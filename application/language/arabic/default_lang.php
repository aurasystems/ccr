<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require 'second_lang.php';


$lang['lang_dashboard'] = "لوحة التحكم";
$lang['lang_email_settings'] = "إعدادات البريد الالكتروني";
$lang['lang_success'] = "Success";
$lang['lang_system_name'] = "System Name";
$lang['lang_system_logo'] = "System Logo";
$lang['lang_front_logo'] = "Front Logo";
$lang['lang_footer_text'] = "Footer Text";
$lang['lang_save'] = "Save";
$lang['lang_cancel'] = "Cancel";
$lang['lang_front_title'] = "Title";
$lang['lang_smtp_host'] = "SMTP Host";
$lang['lang_host_mail'] = "SMTP MAIL";
$lang['lang_smtp_port'] = "SMTP PORT";
$lang['lang_smtp_user'] = "SMTP USER";
$lang['lang_smtp_pass'] = "SMTP PASS";
$lang['lang_basic_info'] = "Basic Information";
$lang['lang_first_name'] = "First Name";
$lang['lang_last_name'] = "Last Name";
$lang['lang_display_name'] = "Display Name";
$lang['lang_phone'] = "Phone";
$lang['lang_email_address'] = "Email";
$lang['lang_address'] = "Address";
$lang['lang_password'] = "كلمة المرور";
$lang['lang_repeat_password'] = "كرر كلمة المرور";
$lang['lang_home'] = "Home";
$lang['lang_login'] = "Login";
$lang['lang_register'] = "Register";
$lang['lang_logout'] = "تسجيل الخروج";
$lang['lang_en'] = "EN";
$lang['lang_ar'] = "AR";
$lang['lang_edit_profile'] = "Edit Profile";
$lang['lang_client_profile'] = "Client Profile";
$lang['lang_user_profile'] = "User Profile";
$lang['lang_facebook'] = "Facebook";
$lang['lang_twitter'] = "Twitter";
$lang['lang_google_plus'] = "Google Plus";
$lang['lang_pinterest'] = "Pinterest";
$lang['lang_flickr'] = "Flickr";
$lang['lang_linkedin'] = "Linkedin";
$lang['lang_social_media'] = "Social Media";
$lang['lang_company_info'] = "Company Info";
$lang['lang_contact_info'] = "Contact Info";
$lang['lang_phone'] = "Phone";
$lang['lang_opening_time'] = "Opening Time";
$lang['lang_user_img'] = "User Image";
$lang['lang_upload_user_img'] = "Upload User Image";
$lang['lang_contact_us'] = "Contact Us";
$lang['lang_name'] = "Name";
$lang['reset_password'] = "Reset Password";
$lang['email_not_found'] = "This email is not found";
$lang['password_recover_success'] = "Password Recover Success";
$lang['lang_pass_reset_link'] = "Send Password Reset Link";
$lang['lang_forget_password'] = "Forgot password?";
$lang['lang_sign_up'] = "New here? Sign up";
$lang['lang_sign_up_now'] = "Sign up";
$lang['lang_sign_in'] = "Sign In";
$lang['lang_back_to_login'] = "Back to login";
$lang['lang_have_account'] = "Already have an account? Sign In";
$lang['lang_send_me_pass'] = "Send me Password";
$lang['lang_terms_conditions'] = "I agree with terms and conditions.";
$lang['lang_users'] = "المستخدمين";
$lang['lang_clients'] = "Clients";
$lang['lang_leave_msg'] = "Leave a message";
$lang['lang_send_msg'] = "Send Message";
$lang['lang_toggle_navi'] = "Toggle navigation";
$lang['lang_menu'] = "Menu";
$lang['lang_language'] = "اللغه";
$lang['lang_english'] = "English";
$lang['lang_espanol'] = "Español";
$lang['lang_francais'] = "Français";
$lang['lang_notifications'] = "الإشعارات";
$lang['lang_view_all_notifications'] = "عرض جميع الإشعارات";
$lang['lang_you_have_no_new_notifications'] = "ليس لديك إشعارات جديدة";
$lang['lang_pending_notifications'] = "الإشعارات الجديدة";
$lang['lang_see_all_msgs'] = "See all messages";
$lang['lang_see_all_notif'] = "عرض جميع الإشعارات";
$lang['lang_account_settings'] = "تعديل الحساب";
$lang['lang_hello_user'] = "مرحبا ";
$lang['lang_system_users'] = "System Users";
$lang['lang_add_user'] = "Add New User";
$lang['lang_add_operator'] = "Add Operator";
$lang['lang_actions'] = "العمليات";
$lang['lang_view_users'] = "All Users";
$lang['lang_edit_user'] = "Edit User";
$lang['lang_delete_user'] = "Delete User";
$lang['lang_confirm_delete_user'] = "هل أنت متأكد من رغبتك في حذف المستخدم؟";
$lang['lang_ban'] = "Ban";
$lang['lang_active'] = "Active";
$lang['lang_#'] = "#";
$lang['lang_first'] = "First";
$lang['lang_last'] = "Last";
$lang['lang_display'] = "Display";
$lang['lang_action'] = "Action";
$lang['lang_receive_msg'] = "To receive a new password, enter your email address below.";
$lang['lang_thank_msg'] = "Thank You. A password reset link was sent to you. Please, check your inbox.";
$lang['lang_email_sent'] = "Thank You. Your message has been sent successfully.";
$lang['lang_go_to_login'] = "Go To Login";
$lang['lang_register_success'] = "You have been registered successfully. We will activate your account soon. Thank you.";
$lang['lang_edit_client'] = "Edit Client";
/////////////////////////////////////////////////////
$lang['lang_arabic'] = "العربية";
$lang['name'] = "الإسم";
$lang['level_name'] = "أسم الصلاحيه";
$lang['private_access'] = "صلاحيه خاصه";
$lang['access_levels'] = "الصلاحيات";
$lang['permissions'] = "الصلاحيات";
$lang['add'] = "إضافة";
$lang['edit'] = "تعديل";
$lang['widget'] = "إختصارات لوحة التحكم";
$lang['data_deleted_successfully'] = "تم الحذف بنجاح.";
$lang['data_submitted_successfully'] = "تمت العملية بنجاح";
$lang['r_u_sure'] = "هل أنت متأكد من رغبتك في الحذف؟";
$lang['action'] = "الضبط";
$lang['add_level'] = "إضافة صلاحيه";
$lang['edit_level'] = "تعديل صلاحيه";
$lang['save'] = "حفظ";
$lang['submit'] = "حفظ";
$lang['users_assigned_level'] = "One or more user are assigned this access right. Please, reassign them first before deleting this access right";
$lang['permissions'] = "الصلاحيات";

//access levels
$lang['activate'] = "تأكيد ورفض";
$lang['all_access'] = "الدخول الى البيانات";
$lang['view'] = "مشاهدة";
$lang['update'] = "تحديث";
$lang['create'] = "إنشاء";
$lang['delete'] = "حذف";
$lang['companies_limited'] = "الشركات المعينة";
$lang['branches_limited'] = "الفروع المعينة";
$lang['exclude_private'] = "أستبعاد الخاص";
$lang['all'] = "الكل";
$lang['own'] = "الخاص";
$lang['assigned'] = "المخصصه";;

$lang['branch'] = "الفرع";
$lang['select_branch'] = "حدد الفرع";
$lang['branches'] = "الفروع";
$lang['email_settings'] = "إعدادت البريد الالكتروني";
//////////////////////////////
$lang['check_all'] = "تحديد الكل";
$lang['uncheck_all'] = "إلغاء تحديد الكل";
$lang['no_permission'] = "You Don't Have Permission To Access This Area";
$lang['permission_error'] = "Permission Error";
$lang['error'] = "Error";
$lang['heading_missing_param'] = "Missing Parameters";
$lang['error_missing_param'] = "One or More Parameters Are Missing";
$lang['heading_resource'] = "Missing Resource";
$lang['error_resource'] = "This Resource is Non-existent";

$lang['img_could_not_be_uploaded'] = "An Error Occured and The Image Could Not Be Uploaded. Please, try again later";
$lang['category_id'] = "القسم";
$lang['companies'] = "الشركات";
$lang['add_company'] = "إضافة شركه";
$lang['edit_company'] = "تعديل شركه";
$lang['select'] = "أختر";
$lang['logo'] = "Logo";
$lang['category'] = "القسم";
$lang['select_category'] = "حدد القسم";
$lang['branches_assigned_company'] = "One or more branch are assigned this company. Please, reassign them first before deleting this company";
$lang['users_assigned_company'] = "One or more user are assigned this company. Please, reassign them first before deleting this company";

$lang['disable'] = "Disable";
$lang['enable'] = "Enable";

$lang['add_branch'] = "إضافة فرع";
$lang['edit_branch'] = "تعديل فرع";
$lang['company'] = "الشركة";
$lang['select_company'] = "أختر الشركة";
$lang['address'] = "العنوان";
$lang['phone'] = "رقم الهاتف";
$lang['users_assigned_branch'] = "One or more user are assigned this branch. Please, reassign them first before deleting this branch";


// Months Short Name
$lang['month_sh_1'] = "JAN";
$lang['month_sh_2'] = "FEB";
$lang['month_sh_3'] = "MAR";
$lang['month_sh_4'] = "APR";
$lang['month_sh_5'] = "MAY";
$lang['month_sh_6'] = "JUN";
$lang['month_sh_7'] = "JUL";
$lang['month_sh_8'] = "AUG";
$lang['month_sh_9'] = "SEP";
$lang['month_sh_10'] = "OCT";
$lang['month_sh_11'] = "NOV";
$lang['month_sh_12'] = "DEC";

// Months Full Name
$lang['month_1'] = "January";
$lang['month_2'] = "February";
$lang['month_3'] = "March";
$lang['month_4'] = "April";
$lang['month_5'] = "May";
$lang['month_6'] = "June";
$lang['month_7'] = "July";
$lang['month_8'] = "August";
$lang['month_9'] = "September";
$lang['month_10'] = "October";
$lang['month_11'] = "November";
$lang['month_12'] = "December";

// System Backup
$lang['system_backups'] = "Backups";
$lang['system_backup'] = "Backup System";
$lang['backup_now'] = "Backup Now";
$lang['system_restore'] = "Restore Backup";
$lang['database_backed_up_successfully'] = "Database backed up successfully";
$lang['database_restored_successfully'] = "Database restored successfully";
$lang['failed_message'] = "Failed";
$lang['r_u_sure_db_restore'] = "Are You Sure You Want to Restore This Version?";
$lang['restore'] = "Restore";

// System Language
$lang['system_lang'] = "ar-EG";
$lang['lang_code'] = "ar";
$lang['system_dir'] = "rtl";
$lang['lang_name'] = "arabic";
$lang['name_in'] = "الإسم باللغه ";
$lang['description_in'] = "Description In ";
$lang['address_in'] = "Address In ";
$lang['lang_ar'] = "العربيه";
$lang['lang_en'] = "الإنجليزيه";

// Footer Copyrights
$lang['lang_copyrights'] = "جميع الحقوق محفوظة";
$lang['lang_copyrights_reserved'] = "";
$lang['system_name'] = "Cairo Camera Rentals";
$lang['lang_developed'] = "تم التطوير بواسطة";
$lang['aura_systems'] = "Aura Systems";
