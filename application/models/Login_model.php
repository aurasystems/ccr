<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function check_login($email, $password) {
        $this->db->where("email", $email);
        $this->db->where("password", $password);
        $this->db->where("deleted !=", 1);
        $this->db->where("status", 1);
        $client = $this->db->get("clients")->row();
        if ($client) {
            //add client data to session
            $set_data = [
                'client_id' => $client->id,
                'client_email' => $client->email,
                'client_name_en' => $client->n_en,
                'client_name_ar' => $client->n_ar,
                'logged_in' => TRUE
            ];
            $this->session->set_userdata($set_data);
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
