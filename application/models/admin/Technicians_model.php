<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Technicians_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_data() {
        $this->db->select("products.*, loc.n_" . lc() . " as lname");
        $this->db->from('products');
        $this->db->where("products.type", 2);
        $this->db->where("products.deleted !=", 1);
        $this->db->join("locations loc", "loc.id = products.location_id", 'left');
        $query = $this->db->get();
        return $query->result();
    }

    function get_info($id = 0) {
        $this->db->select("products.*");
        $this->db->from('products');
        $this->db->where("deleted !=", 1);
        $this->db->where("id", $id);
        return $this->db->get()->row();
    }

    function get_technician_timetable($technician_id) {
        $this->db->select('*');
        $this->db->where('technician_id', $technician_id);
        $this->db->from('technicians_hours');
        $query = $this->db->get();
        return $query->result();
    }

    function insert_timetable($data) {
        $this->db->insert('technicians_hours', $data);
    }

    function update_timetable($technician_id, $day, $data) {
        $this->db->where('technician_id', $technician_id);
        $this->db->where('day', $day);
        $this->db->update('technicians_hours', $data);
    }

    function delete_timetable($technician_id) {
        $this->db->where("technician_id", $technician_id);
        $this->db->delete("technicians_hours");
    }

}
