<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories_model extends CI_Model {

    private $idf = NULL;
    private $sh = NULL;

    function __construct() {
        parent::__construct();
        $this->idf = get_sys_idf(8);
        $this->sh = 'cat';
    }

    function get_data($dt = []) {
        $count = isset($dt['cn']) && $dt['cn'] ? TRUE : FALSE;
        $pid = isset($dt['p']) ? $dt['p'] : 0;
        $this->db->select($count ? "COUNT($this->sh.id) as count" : $this->sh . ".*");
        $this->db->from($this->idf . ' ' . $this->sh);
        $this->db->where($this->sh . ".parent", $pid);
        $this->db->where($this->sh . ".deleted !=", 1);
        $query = $this->db->get();
        return $count ? $query->row() : $query->result();
    }

    function get_info($id = 0) {
        $this->db->select($this->sh . ".*");
        $this->db->from($this->idf . ' ' . $this->sh);
        $this->db->where($this->sh . ".deleted !=", 1);
        $this->db->where($this->sh . ".id", $id);
        return $this->db->get()->row();
    }

    function check_documents_brand($id) {
        $this->db->where("car_brand", $id);
        $this->db->or_where("sub_brand_model", $id);
        $query = $this->db->get("documents");
        if ($query->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    function get_categories() {
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('parent', 0);
        $this->db->where('deleted !=', 1);
        $parent = $this->db->get();
        $categories = $parent->result();
        $i = 0;
        foreach ($categories as $p_cat) {
            $categories[$i]->sub = $this->sub_categories($p_cat->id);
            $i++;
        }
        return $categories;
    }

    function sub_categories($id) {
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('parent', $id);
        $this->db->where('deleted !=', 1);
        $child = $this->db->get();
        $categories = $child->result();
        $i = 0;
        foreach ($categories as $p_cat) {
            $categories[$i]->sub = $this->sub_categories($p_cat->id);
            $i++;
        }
        return $categories;
    }

}
