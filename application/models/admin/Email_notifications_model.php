<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Email_notifications_model extends CI_Model {

    function send_account_info($email, $new_password) {
        $msg_content = '';
        //get User info
        $this->db->where('email', $email);
        $info = $this->db->get('users')->row();
        $msg_content .= "Dear " . $info->{'n_' . lc()} . " <br>";
        $msg_content .= "Your email is: $email <br>";
        $msg_content .= "Your password is: $new_password <br>";
        $msg_content .= "You can change it after login to your account via "
                . "<a href=" . base_url("Login") . ">" . MY_APP_NAME . "</a>";
        // config email smtp
        $settings = settings('all', 1);
        $config = [
            'protocol' => 'smtp',
            'smtp_host' => $settings->smtp_host, //ssl://smtp.googlemail.com
            'smtp_port' => $settings->smtp_port, // 465 
            'smtp_user' => $settings->smtp_user,
            'smtp_pass' => $settings->smtp_pass,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        ];
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from($settings->host_mail);
        $this->email->to($email);
        $this->email->subject(MY_APP_NAME . ' - New Account Creation');
        $this->email->message($msg_content);
        $this->email->send();
    }
}
