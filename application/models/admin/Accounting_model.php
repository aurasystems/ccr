<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Accounting_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_types()
    {
        $this->db->select('*');
        $this->db->from('accounting_types');
        $this->db->where('parent = ', 0);
        $this->db->where('deleted !=', 1);
        return $this->db->get()->result();
    }

    function get_total_accounting($type)
    {
        
            $this->db->select('id')->from('accounting_types')->where('type', $type)->where('parent = ', 0);
            $this->db->where('deleted !=', 1);
            $categ_id = $this->db->get()->row('id');
            $this->db->select('id')->from('accounting_types')->where('parent', $categ_id);
            $this->db->where('deleted !=', 1);
            $sub_id = $this->db->get()->result();
            $arr = array_column($sub_id, "id");
            $arr[count($arr)] = $type;
            $this->db->select('sum(cost) as cost')->from('transaction')->where_in('type_id', $arr);
            $this->db->where('deleted !=', 1);
            return $this->db->get()->row('cost');
      
    }

    function get_trans_data($is_income = false)
    {
        $result = array();
            $this->db->select('id');
            $this->db->from('accounting_types');
            if($is_income == true)
            $this->db->where('type', 1);
            else
            $this->db->where('type', 2);
            $this->db->where('parent =', 0);
            $this->db->where('deleted !=', 1);
            $categ_id = $this->db->get()->result();
            $arr = array_column($categ_id, "id");
            if(!empty($arr))
            {
            $this->db->select('id')->from('accounting_types')->where_in('parent', $arr);
            $sub_id = $this->db->get()->result();
            $arr = array_column($sub_id, "id");
            if($is_income)
            $arr[count($arr)] = 1;
            else
            $arr[count($arr)] = 2;
                if(!empty($arr))
                {
                    $this->db->select('*');
                    $this->db->from('transaction');
                    $this->db->where('deleted !=', 1);
                    $this->db->where_in('type_id', $arr);
                    $result = $this->db->get()->result();
                }
            }
       
            return $result;

    }

    function get_sub($parent=null, $is_income = false)
    {
        $parent_ids = array();
        if($is_income && !$parent)
        {
            $this->db->select('id');
            $this->db->from('accounting_types');
            $this->db->where('deleted !=', 1);
            $this->db->where('type', 1);
            $arr = $this->db->get()->result();
            $parent_ids = array_column($arr, "id");
        }
        else if(!$is_income && !$parent)
        {
            $this->db->select('id');
            $this->db->from('accounting_types');
            $this->db->where('deleted !=', 1);
            $this->db->where('type', 2);
            $arr = $this->db->get()->result();
            $parent_ids = array_column($arr, "id");
        }
        $this->db->select('*');
        $this->db->from('accounting_types');
        $this->db->where('deleted !=', 1);
        if(!$parent && !empty($parent_ids))
        $this->db->where_in('parent', $parent_ids);
        else
        $this->db->where('parent', $parent);
        return $this->db->get()->result();
    }

    function get_category($id)
    {
        return $this->db->select('*')->from('accounting_types')->where('id', $id)->get()->row();
    }

    function delete_relatedTicket($id)
    {
        $this->db->where('type_id', $id);
        $this->db->update('transaction', ['deleted' => 1]);
    }
    
    function get_table_data($table)
    {
        $this->db->select('products.*')->from($table)->where('products.deleted !=',1);
        $this->db->where('i.deleted !=', 1);
        $this->db->where('i.sold !=', 1);
        $this->db->distinct();
        $this->db->join('product_items i', 'i.product_id = products.id');
        return $this->db->get()->result();
    }

    function get_items_data($product_id, $rent_price)
    {
        //get how many times rented
        //get total cost
        //get total booking
        //get total maintenance
        $i   = 0;
        $data= array();
        $this->db->select('*');
        $this->db->from('product_items');
        $this->db->where('deleted !=', 1);
        $this->db->where('sold !=', 1);
        $this->db->where('product_id', $product_id);
        $items = $this->db->get()->result();
        foreach($items as $one)
        {
            $sum = 0;
            $this->db->select('*');
            $this->db->from('booking_items_delivery');
            $this->db->where('item_id', $one->id);
            $times_rented = $this->db->count_all_results();
            $this->db->select('transaction_id');
            $this->db->from('maintenance');
            $this->db->where('item_id', $one->id);
            $arr         = $this->db->get()->result();
            $times_maint = count($arr);
            $trans_ids   = array_column($arr, "transaction_id");
            if(!empty($trans_ids))
            {
                $this->db->select('sum(cost)');
                $this->db->from('transaction');
                $this->db->where('deleted !=', 1);
                $this->db->where_in('id', $trans_ids);
                $sum = $this->db->get()->row('sum(cost)');
            }
          
            $data[$i]['times_rented']     = $times_rented;
            $data[$i]['SN']               = $one->serial_number;
            $data[$i]['total_rented']     = $times_rented * $rent_price;
            $data[$i]['item_cost']        = $one->cost_price;
            $data[$i]['warranty_company'] = $one->warranty_company;
            $data[$i]['waranty_ED']       = $one->warranty_end_date;
            $data[$i]['times_maint']      = $times_maint;     
            $data[$i]['maint_cost']       = $sum;       
            $i++;
        }
        return $data;

    }

    function get_sup_items_data($product_id, $rent_price)
    {
        //get how many times rented
        //get total cost
        //get total booking
        //get total maintenance
        $i   = 0;
        $data= array();
        $this->db->select('id');
        $this->db->from('products_sup');
        $this->db->where('deleted !=', 1);
        $this->db->where('product_id', $product_id);
        $product_id = $this->db->get()->row('id');
        $this->db->select('*');
        $this->db->from('product_sup_items');
        $this->db->where('deleted !=', 1);
        $this->db->where('product_id', $product_id);
        $items = $this->db->get()->result();
        foreach($items as $one)
        {
            $sum = 0;
            $this->db->select('*');
            $this->db->from('booking_items_delivery');
            $this->db->where('sup_id', $one->id);
            $times_rented = $this->db->count_all_results();
            $this->db->select('transaction_id');
            $this->db->from('maintenance');
            $this->db->where('sup_id', $one->id);
            $arr         = $this->db->get()->result();
            $times_maint = count($arr);
            $trans_ids   = array_column($arr, "transaction_id");
            if(!empty($trans_ids))
            {
                $this->db->select('sum(cost)');
                $this->db->from('transaction');
                $this->db->where('deleted !=', 1);
                $this->db->where_in('id', $trans_ids);
                $sum = $this->db->get()->row('sum(cost)');
            }
          
            $data[$i]['times_rented']     = $times_rented;
            $data[$i]['SN']               = $one->serial_number;
            $data[$i]['total_rented']     = $times_rented * $rent_price;
            $data[$i]['item_cost']        = $one->cost_price;
            $data[$i]['warranty_company'] = $one->warranty_company;
            $data[$i]['waranty_ED']       = $one->warranty_end_date;
            $data[$i]['times_maint']      = $times_maint;     
            $data[$i]['maint_cost']       = $sum;       
            $i++;
        }
        return $data;

    }

    function get_usage_data($product_id)
    {
        //get total usage for this product
        $count_all = 0;
        $this->db->select('*');
        $this->db->from('product_items');
        $this->db->where('product_id', $product_id);
        $this->db->where('deleted !=', 1);
        $this->db->where('sold !=', 1);
        $items = $this->db->get()->result();
        $data['count'] = count($items);
        foreach($items as $item)
        {
            $this->db->select('*');
            $this->db->from('booking_items_delivery');
            $this->db->where('item_id', $item->id);
            $count_rented = $this->db->count_all_results();
            $count_all += $count_rented;
        }
        $data['all_rented'] = $count_all;
        $data['pname']      = $this->db->select('n_en')->from('products')->where('id', $product_id)->get()->row('n_en');
        return $data;
    }

    function get_transaction_data($type_id)
    {
         $this->db->select('id');
         $this->db->from('accounting_types');
         $this->db->where('type', $type_id);
         $this->db->where('deleted !=', 1);
         $ids = $this->db->get()->result();
         $arr = array_column($ids, "id");
         $this->db->select('id');
         $this->db->from('accounting_types');
         $this->db->where('deleted !=', 1);
         $this->db->where_in('parent', $arr);
         $sub_ids = $this->db->get()->result();
         $arr = array_column($sub_ids, "id");
         $arr[count($arr)] = $type_id;
         $this->db->select('transaction.*, a.n_en, a.n_ar, a.type');
         $this->db->from('transaction');
         $this->db->where_in('transaction.type_id', $arr);
         $this->db->join('accounting_types a', 'a.id = transaction.type_id');
         return $this->db->get()->result();
    }

    function get_client_data($trans_data, $type_id)
    {
        $trans_ids = array_column($trans_data, "id");
        if($type_id == 1)
        {
            $this->db->select('booking.*, c.*');
            $this->db->from('booking');
            $this->db->where_in('booking.transaction_id', $trans_ids);
            $this->db->join('clients c', 'c.id = booking.client_id');
        }
        else if($type_id == 2)
        {
            $this->db->select('maintenance.*, p.*');
            $this->db->from('maintenance');
            $this->db->where_in('maintenance.transaction_id', $trans_ids);
            $this->db->join('products p', 'p.id = maintenance.product_id');
        }
        $data = $this->db->get()->result();
        return $data;
    }
}