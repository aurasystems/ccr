<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_data_by_productID($table, $prodID) {
        $this->db->where('product_id', $prodID);
        $this->db->where('deleted !=', 1);
        return $this->db->get($table)->result();
    }

    function get_record_name($table, $id) {
        $this->db->select('n_en');
        $this->db->from($table);
        $this->db->where('id', $id);
        $this->db->where('deleted !=', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->first_row()->n_en;
        }
    }

    function get_record($id, $table) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where("deleted !=", 1);
        if ($table == 'product_items') {
            $this->db->where("sold !=", 1);
        }
        $this->db->where("id =", $id);
        $res = $this->db->get()->result();

        return !empty($res) ? $res : null;
    }

    function delete_record($id, $table) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where("id", $id);
        $result = $this->db->get()->row();
        if ($result) {
            $data = array(
                'deleted' => '1',
            );
            $this->db->where('id', $id);
            return $this->db->update($table, $data);
        } else {
            return false;
        }
    }

    function insert_product($category_id, $brand_id, $data) {
        $this->db->insert('products', $data);
        $last_id = $this->db->insert_id();

        $arr = array(
            'category_id' => $category_id,
            'product_id' => $last_id
        );
        $this->db->insert('product_categories', $arr);

        $arr = array(
            'brand_id' => $brand_id,
            'product_id' => $last_id
        );
        $this->db->insert('product_brands', $arr);
    }

    function products_update($productID, $category_id, $brand_id, $data) {
        $this->db->where('id', $productID);
        $this->db->update('products', $data);

        $arr = array(
            'category_id' => $category_id,
        );
        $this->db->where('product_id', $productID);
        $this->db->update('product_categories', $arr);

        $arr = array(
            'brand_id' => $brand_id,
        );
        $this->db->where('product_id', $productID);
        $this->db->update('product_brands', $arr);
    }

    function get_categories() {

        $this->db->select('*')->from('categories')->where('parent !=', 0)->where('deleted !=', 1);

        $res = $this->db->get()->result();

        return $res;
    }

    function get_brands() {

        $this->db->select('*')->from('brands')->where('deleted !=', 1);

        $res = $this->db->get()->result();

        return $res;
    }

    function get_product_brand($product_id) {
        $this->db->select('product_brands.*');
        $this->db->from('product_brands');
        $this->db->where("product_id", $product_id);
        return $this->db->get()->row();
    }

    function get_product_cat($id) {
        $this->db->select('categories.id, categories.is_camera');
        $this->db->from('categories');
        $this->db->join("product_categories", "product_categories.category_id = categories.id", 'left');
        $this->db->where("product_categories.product_id", $id);
        $this->db->where("categories.deleted !=", 1);
        return $this->db->get()->row();
    }

    function get_info($table, $id = 0) {
        $this->db->select("*");
        $this->db->from($table);
        $this->db->where("deleted !=", 1);
        $this->db->where("id", $id);
        return $this->db->get()->row();
    }

    function get_reviews($id = NULL) {
        $this->db->select("reviews.*, clients.n_" . lc() . " as cname");
        $this->db->from('reviews');
        $this->db->join("products", "products.id = reviews.product_id", 'left');
        $this->db->join("clients", "clients.id = reviews.client_id", 'left');
        $this->db->where("reviews.deleted !=", 1);
        $this->db->where("products.deleted !=", 1);
        $this->db->where("reviews.product_id", $id);
        $query = $this->db->get();
        return $query->result();
    }

    function resizeImage($filename, $width, $height, $is_slider = false) {
        $source_path = FCPATH . '/uploads/products/' . $filename;
        if ($is_slider)
            $target_path = FCPATH . '/uploads/slider/';
        else
            $target_path = FCPATH . '/uploads/thumb/';
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => FALSE,
            'create_thumb' => TRUE,
            'thumb_marker' => '',
            'width' => $width,
            'height' => $height
        );
        $this->load->library('image_lib', $config_manip);
        $this->image_lib->initialize($config_manip);
        $this->image_lib->resize();
        $this->image_lib->clear();
    }

}
