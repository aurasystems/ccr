<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Studios_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_data() {
        $this->db->select("products.*, loc.n_" . lc() . " as lname");
        $this->db->from('products');
        $this->db->where("products.type", 1);
        $this->db->where("products.deleted !=", 1);
        $this->db->join("locations loc", "loc.id = products.location_id", 'left');
        $query = $this->db->get();
        return $query->result();
    }

    function get_info($id = 0) {
        $this->db->select("products.*");
        $this->db->from('products');
        $this->db->where("deleted !=", 1);
        $this->db->where("id", $id);
        return $this->db->get()->row();
    }

}
