<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function check_login($email, $password) {
        $this->db->where("email", $email);
        $this->db->where("password", $password);
        $get_user = $this->db->get("users")->row();
        if ($get_user) {
            //add user data to session
            $new_data = [
                'user_id' => $get_user->id,
                'user_email' => $get_user->email,
                'user_name_en' => $get_user->n_en,
                'user_name_ar' => $get_user->n_ar,
                'user_level_id' => $get_user->level_id,
                'language' => $get_user->language,
                'logged_in' => TRUE
            ];
            $this->session->set_userdata($new_data);
            load_permissions($get_user->level_id);
            load_locations($get_user->id);
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
