<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Datatable_model extends CI_Model {

    var $table = '';
    var $select = '';
    var $filter = [];
    var $join = [];
    var $column_order = []; //set column field database for datatable orderable
    var $column_search = []; //set column field database for datatable searchable 
    var $order = ['id' => 'desc']; // default order 

    public function __construct() {
        parent::__construct();
    }

    private function _get_datatables_query($count = FALSE) {

        //add custom select here
        if ($this->select) {
            if (is_array($this->select)) {
                foreach ($this->select as $sel) {
                    $this->db->select($sel);
                }
            } else {
                $this->db->select($this->select);
            }
        }

        //add custom filter here
        if ($this->filter) {
            if (is_array($this->filter)) {
                foreach ($this->filter as $fl) {
                    if (is_array($fl[1])) {
                        $this->db->where_in($fl[0], $fl[1]);
                    } else {
                        $this->db->where($fl[0], $fl[1] ? $fl[1] : NULL);
                    }
                }
            }
        }

        $this->db->from($this->table);


        if ($this->input->post('search')['value'] && !$count) { // if datatable send POST for search
            $i = 0;
            foreach ($this->column_search as $item) { // loop column 
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
                $i++;
            }
        }

        //add custom join here
        if ($this->join && is_array($this->join)) {
            foreach ($this->join as $join) {
                $this->db->join($join[0], $join[1], $join[2]);
            }
        }

        if (isset($_POST['order']) && $this->column_order) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables() {
        $this->_get_datatables_query();
        if (isset($_POST['length']) && $_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->_get_datatables_query(TRUE);
        return $this->db->count_all_results();
    }

    public function get_filter_list($sel = '', $ord = ['col' => '', 'ord' => 'asc'], $table = '') {
        $table = $table ? $table : $this->table;
        $this->db->select($sel);
        $this->db->from($this->table);
        $this->db->order_by($ord['col'] ? $ord['col'] : $sel, $ord['ord']);
        $query = $this->db->get();
        $result = $query->result();

        $list = [];
        foreach ($result as $row) {
            $list[] = $row->{$sel};
        }
        return $list;
    }

    public function set_table($table = '') {
        $this->table = $table;
    }

    public function set_select($select = '') {
        $this->select = $select;
    }

    public function set_filter($filter = '') {
        $this->filter = $filter;
    }

    public function set_join($join = []) {
        $this->join = $join;
    }

    public function set_column_order($column = []) {
        $this->column_order = $column;
    }

    public function set_column_search($column = []) {
        $this->column_search = $column;
    }

    public function set_order($order = []) {
        $this->order = $order;
    }

}
