<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Survey_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_surveys() {
        $this->db->select('surveys.*');
        $this->db->from('surveys');
        $this->db->where("deleted !=", 1);
        return $this->db->get()->result();
    }

    function get_survey_info($id = 0) {
        $this->db->select("surveys.*");
        $this->db->from('surveys');
        $this->db->where("deleted !=", 1);
        $this->db->where("id", $id);
        return $this->db->get()->row();
    }

    function set_survey_deactivate($id, $data) {
        $this->db->where('id !=', $id);
        return $this->db->update('surveys', $data);
    }

    function get_related_survey_questions($survey_id = 0) {
        $this->db->select('survey_questions.*');
        $this->db->from('survey_questions');
        $this->db->join('surveys', 'survey_questions.survey_id = surveys.id');
        $this->db->where('survey_questions.survey_id', $survey_id);
        $this->db->where("survey_questions.deleted !=", 1);
        $this->db->where("surveys.deleted !=", 1);
        return $this->db->get()->result();
    }

    function get_survey_questions($survey_id = 0) {
        $this->db->select('survey_questions.*');
        $this->db->from('survey_questions');
        $this->db->join('surveys', 'survey_questions.survey_id = surveys.id');
        $this->db->where('survey_questions.survey_id', $survey_id);
        $this->db->where('survey_questions.answer_type !=', 0);
        $this->db->where("surveys.deleted !=", 1);
        return $this->db->get()->result();
    }

    function get_survey_questions_ans($survey_id = 0) {
        $this->db->select('survey_answers.*');
        $this->db->select('survey_questions.question as qName');
        $this->db->from('survey_answers');
        $this->db->join('survey_questions', 'survey_answers.question_id = survey_questions.id');
        $this->db->where('survey_questions.survey_id', $survey_id);
        $this->db->where('survey_answers.answer_type !=', 0);
        return $this->db->get()->result();
    }

    function get_option_group_items($op_id) {
        $this->db->select('options_groups_items.*');
        $this->db->from('options_groups_items');
        $this->db->join("options_groups", "options_groups.id = options_groups_items.op_group_id", 'left');
        $this->db->where("options_groups.deleted !=", 1);
        $this->db->where('options_groups_items.op_group_id', $op_id);
        return $this->db->get()->result();
    }

    function get_two_options_answers($question_id, $type, $value) {
        $this->db->select('survey_answers.*');
        $this->db->from('survey_answers');
        $this->db->where('question_id', $question_id);
        $this->db->where('answer_type', $type);
        $this->db->where('answer_value', $value);
        //$this->db->where('submitted', 1);
        return $this->db->count_all_results();
    }

    function get_survey_clients($survey_id) {
        $this->db->distinct('survey_answers.client_id');
        $this->db->select("survey_answers.client_id as cID, clients.n_" . lc() . " as cName");
        $this->db->from('survey_answers');
        $this->db->join('clients', 'survey_answers.client_id = clients.id', 'left');
        $this->db->join('survey_questions', 'survey_answers.question_id = survey_questions.id');
        $this->db->join('surveys', 'survey_answers.survey_id = surveys.id');
        $this->db->where('survey_questions.survey_id', $survey_id);
        $this->db->where("surveys.deleted !=", 1);
        return $this->db->get()->result();
    }

    function get_client_survey_b_t_submitted($survey_id, $client_id) {
        $this->db->select('survey_client_b_t.*');
        $this->db->from('survey_client_b_t');
        $this->db->where('survey_id', $survey_id);
        $this->db->where('client_id', $client_id);
        $this->db->where('submitted', 1);
        return $this->db->get()->row();
    }

    function get_client_answers($survey_id, $client_id) {
        $this->db->select('survey_answers.*');
        $this->db->select('survey_questions.question');
        $this->db->from('survey_answers');
        $this->db->join('survey_questions', 'survey_answers.question_id = survey_questions.id');
        $this->db->join('surveys', 'survey_answers.survey_id = surveys.id');
        $this->db->where('survey_answers.client_id', $client_id);
        $this->db->where('survey_questions.survey_id', $survey_id);
        $this->db->where("surveys.deleted !=", 1);
        return $this->db->get()->result();
    }

    function get_client_survey_b_t($survey_id, $code) {
        $this->db->select('survey_client_b_t.*');
        $this->db->from('survey_client_b_t');
        $this->db->where('code', $code);
        $this->db->where('survey_id', $survey_id);
        //$this->db->where('submitted', 0);
        return $this->db->get()->row();
    }

    function get_survey_answers($survey_id, $code) {
        $this->db->select('survey_answers.*');
        $this->db->select('survey_questions.survey_id');
        $this->db->select('survey_questions.question');
        $this->db->select('survey_questions.answer_type');
        $this->db->from('survey_answers');
        $this->db->join('survey_questions', 'survey_answers.question_id = survey_questions.id');
        $this->db->where('survey_answers.code', $code);
        $this->db->where('survey_answers.survey_id', $survey_id);
        $this->db->where('survey_answers.submitted', 0);
        return $this->db->get()->result();
    }

    function get_current_survey($survey_id) {
        $this->db->select('surveys.*');
        $this->db->from('surveys');
        $this->db->where('id', $survey_id);
        $active_survey = $this->db->get()->row();
        if ($active_survey) {
            $this->db->select('survey_questions.*');
            $this->db->from('survey_questions');
            $this->db->where('survey_questions.survey_id', $active_survey->id);
            $active_survey->questions = $this->db->get()->result();
        }
        return $active_survey;
    }

    function get_client_survey_basic($survey_id, $code) {
        $this->db->select('survey_client_b_t.*');
        $this->db->from('survey_client_b_t');
        $this->db->where('code', $code);
        $this->db->where('survey_id', $survey_id);
        //$this->db->where('submitted', 0);
        return $this->db->get()->row();
    }

    function get_survey_question_by_code($survey_id, $code, $question_id) {
        $this->db->select('survey_answers.*');
        $this->db->from('survey_answers');
        $this->db->where('survey_answers.code', $code);
        $this->db->where('survey_answers.survey_id', $survey_id);
        $this->db->where('survey_answers.question_id', $question_id);
        return $this->db->get()->row();
    }

    function get_survey_basic_by_client($survey_id, $client_id) {
        $this->db->select('survey_client_b_t.*');
        $this->db->from('survey_client_b_t');
        $this->db->where('client_id', $client_id);
        $this->db->where('survey_id', $survey_id);
        //$this->db->where('submitted', 0);
        return $this->db->get()->row();
    }

    function get_survey_answers_by_client($survey_id, $client_id) {
        $this->db->select('survey_answers.*');
        $this->db->select('survey_questions.survey_id');
        $this->db->select('survey_questions.question');
        $this->db->select('survey_questions.answer_type');
        $this->db->from('survey_answers');
        $this->db->join('survey_questions', 'survey_answers.question_id = survey_questions.id');
        $this->db->where('survey_answers.client_id', $client_id);
        $this->db->where('survey_answers.survey_id', $survey_id);
        //$this->db->where('survey_answers.submitted', 0);
        return $this->db->get()->result();
    }

    function get_survey_question_by_client($survey_id, $client_id, $question_id) {
        $this->db->select('survey_answers.*');
        $this->db->from('survey_answers');
        $this->db->where('survey_answers.client_id', $client_id);
        $this->db->where('survey_answers.survey_id', $survey_id);
        $this->db->where('survey_answers.question_id', $question_id);
        return $this->db->get()->row();
    }

    function get_active_survey() {
        $this->db->select('surveys.*');
        $this->db->from('surveys');
        $this->db->where('active', 1);
        $this->db->where("deleted !=", 1);
        return $this->db->get()->row();
    }

    function get_active_survey_questions($survey_id) {
        $this->db->select('survey_questions.*');
        $this->db->from('survey_questions');
        $this->db->join('surveys', 'survey_questions.survey_id = surveys.id');
        $this->db->where('survey_questions.survey_id', $survey_id);
        $this->db->where("survey_questions.deleted !=", 1);
        $this->db->where("surveys.deleted !=", 1);
        return $this->db->get()->result();
    }
    function get_survey_submitted_count($survey_id, $client_id, $from , $to) {
        $this->db->select('survey_client_b_t.*');
        $this->db->from('survey_client_b_t');
        $this->db->where('client_id', $client_id);
        $this->db->where('survey_id', $survey_id);
        $this->db->where("DATE(timestamp) >=", $from);
        $this->db->where("DATE(timestamp) <=", $to);
        return $this->db->count_all_results();
    }
}
