<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Newsletters_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_data() {
        $this->db->select("newsletters.*");
        $this->db->from('newsletters');
        $this->db->where("deleted !=", 1);
        $query = $this->db->get();
        return $query->result();
    }

    function get_info($id = 0) {
        $this->db->select("newsletters.*");
        $this->db->from('newsletters');
        $this->db->where("deleted !=", 1);
        $this->db->where("id", $id);
        return $this->db->get()->row();
    }

    function set_newsletter_deactivate($id, $data) {
        $this->db->where('id !=', $id);
        return $this->db->update('newsletters', $data);
    }

    function get_newsletter_clients($id) {
        $this->db->select("newsletter_assignees.*, clients.n_" . lc() . " as cname, clients.email");
        $this->db->from('newsletter_assignees');
        $this->db->join("newsletters", "newsletters.id = newsletter_assignees.news_l_id", 'left');
        $this->db->join("clients", "clients.id = newsletter_assignees.client_id", 'left');
        $this->db->where("newsletters.deleted !=", 1);
        $this->db->where('newsletter_assignees.news_l_id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function get_clients_by_search($search_content) {
        $this->db->select("clients.id, clients.n_" . lc() . " as cname");
        $this->db->from('clients');
        $this->db->join("newsletter_assignees", "clients.id = newsletter_assignees.client_id", 'left');
        $this->db->where("("
                . "n_en like '%$search_content%' OR "
                . "n_ar like '%$search_content%' OR "
                . "email like '%$search_content%' OR "
                . "phone like '%$search_content%'"
                . ")");
        $this->db->where('newsletter_assignees.id is null');
        $this->db->where('clients.status', 1);
        $this->db->where("clients.deleted !=", 1);
        $result = $this->db->get()->result();
        return $result;
    }

    function check_newsletter_client($id, $client_id) {
        $this->db->select("newsletter_assignees.*");
        $this->db->from('newsletter_assignees');
        $this->db->where("news_l_id", $id);
        $this->db->where("client_id", $client_id);
        return $this->db->get()->row();
    }

    function delete_all_assignee($id) {
        $this->db->where('news_l_id', $id);
        $this->db->delete('newsletter_assignees');
    }

    function get_active_news() {
        $this->db->select("newsletters.*");
        $this->db->from('newsletters');
        $this->db->where("status", 1);
        $this->db->where("deleted !=", 1);
        return $this->db->get()->row();
    }

}
