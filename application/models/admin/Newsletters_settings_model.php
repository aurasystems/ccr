<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Newsletters_settings_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function update_settings($data) {
        $get_settings = $this->db->get('newsletter_settings')->row();
        if ($get_settings) {
            $this->db->where('id', 1);
            $this->db->update('newsletter_settings', $data);
        } else {
            $this->db->insert('newsletter_settings', $data);
        }
    }

    function get_newsletters_settings() {
        return $this->db->get('newsletter_settings')->row();
    }

}
