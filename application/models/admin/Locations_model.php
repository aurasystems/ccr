<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Locations_model extends CI_Model {

    private $idf = NULL;
    private $sh = NULL;

    function __construct() {
        parent::__construct();
        $this->idf = get_sys_idf(6);
        $this->sh = 'u';
    }

    function get_data($dt = []) {
        $count = isset($dt['cn']) && $dt['cn'] ? TRUE : FALSE;
        $this->db->select($count ? "COUNT($this->sh.id) as count" : $this->sh . ".*");

        $this->db->from($this->idf . ' ' . $this->sh);
        $this->db->where($this->sh . ".deleted !=", 1);
        $query = $this->db->get();
        return $count ? $query->row() : $query->result();
    }

    function get_info($id = 0) {
        $this->db->select($this->sh . ".*");
        $this->db->from($this->idf . ' ' . $this->sh);
        $this->db->where($this->sh . ".deleted !=", 1);
        $this->db->where($this->sh . ".id", $id);
        return $this->db->get()->row();
    }

    function get_location_timetable($location_id) {
        $this->db->select('*');
        $this->db->where('location_id', $location_id);
        $this->db->from('locations_hours');
        $query = $this->db->get();
        return $query->result();
    }

    function insert_timetable($data) {
        $this->db->insert('locations_hours', $data);
    }

    function update_timetable($location_id, $day, $data) {
        $this->db->where('location_id', $location_id);
        $this->db->where('day', $day);
        $this->db->update('locations_hours', $data);
    }

    function delete_timetable($location_id) {
        $this->db->where("location_id", $location_id);
        $this->db->delete("locations_hours");
    }

}
