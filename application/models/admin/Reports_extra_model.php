<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports_extra_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_expenses($SN) {
        $data = array();
        $this->db->select('*');
        $this->db->from('product_items');
        $this->db->where('serial_number', $SN);
        $this->db->where('sold !=', 1);
        $res = $this->db->get()->row(); //search if SN for item
        if (!empty($res)) {
            $data['flag'] = true;
            $data['cost'] = $res->cost_price;
            $data['lost'] = $res->missing == 1 ? 'Yes' : 'No';
            $this->db->select('*');
            $this->db->from('products');
            $this->db->where('id', $res->product_id);
            $data['pname'] = $this->db->get()->row('n_' . lc());
            //get maintenance cost
            $this->db->select('*');
            $this->db->from('maintenance');
            $this->db->where('item_id', $res->id);
            $this->db->where('is_applied', 1);
            $maint = $this->db->get()->result();
            if (!empty($maint)) {
                $trans_id = array_column($maint, "transaction_id");
                $this->db->select('SUM(cost) as sum');
                $this->db->from('transaction');
                $this->db->where_in('id', $trans_id);
                $maint_cost = $this->db->get()->row('sum');
                $data['maint_cost'] = $maint_cost == null ? 0 : $maint_cost;
            } else
                $data['maint_cost'] = 0;
        } else { //search if SN for sup item
            $this->db->select('*');
            $this->db->from('product_sup_items');
            $this->db->where('serial_number', $SN);
            $res = $this->db->get()->row();
            if (!empty($res)) {
                $data['flag'] = true;
                $data['cost'] = $res->cost_price;
                $data['lost'] = $res->missing == 1 ? 'Yes' : 'No';
                $this->db->select('*');
                $this->db->from('products_sup');
                $this->db->where('id', $res->product_id);
                $data['pname'] = $this->db->get()->row('n_' . lc());
                //get maintenance cost
                $this->db->select('*');
                $this->db->from('maintenance');
                $this->db->where('sup_id', $res->id);
                $this->db->where('is_applied', 1);
                $maint = $this->db->get()->result();
                if (!empty($maint)) {
                    $trans_id = array_column($maint, "transaction_id");
                    $this->db->select('SUM(cost) as sum');
                    $this->db->from('transaction');
                    $this->db->where_in('id', $trans_id);
                    $maint_cost = $this->db->get()->row('sum');
                    $data['maint_cost'] = $maint_cost == null ? 0 : $maint_cost;
                } else
                    $data['maint_cost'] = 0;
            } else {
                $data['flag'] = false;
                $data['msg'] = lang('SN_not_exist');
            }
        }
        return $data;
    }

    function get_vouchers($from = NULL, $to = NULL) {
        $this->db->select('*');
        $this->db->from('vouchers');
        $this->db->where('v_from <= ', $from);
        $this->db->where('v_to   >= ', $to);
       
        return $this->db->get()->result();
    }

    function get_total_transaction($SN)
    {
        $num_days = 0;
        $data     = array();
        $product_data = array();
        $hours    = 0;
        $days     = 0;
        $data['num_hrs']   = $hours;
        $this->db->select('*');
        $this->db->from('product_items');
        $this->db->where('serial_number', $SN);
        $this->db->limit(1);
        $row        = $this->db->get()->row();
        if(!empty($row))
        {
        $id             = $row->id;
        $product_id = $row->product_id;
        }
        if(!empty($id))//this SN for item
        {
            $this->db->select('*');
            $this->db->from('products');
            $this->db->where('id', $product_id);
            $product_data = $this->db->get()->row();
            $data['pname'] = $product_data->n_en;
            $this->db->select('*');
            $this->db->from('booking_items_delivery');
            $this->db->where('item_id', $id);
            $deliverd_data     = $this->db->get()->result();
            $data['flag']      = true;
            $data['num_trans'] = count($deliverd_data);
            foreach($deliverd_data as $one)
            {
                $this->db->select('*');
                $this->db->from('booking_items');
                $this->db->where('book_id', $one->book_id);
                $booking_items = $this->db->get()->result();
                foreach($booking_items as $item)
                {
                    if($product_data->rent_term == 1)//day
                    {
                    $days      = createDateRangeArraygivenStamp($item->b_from, $item->b_to);
                    $num_days += count($days);
                    }
                    else // hour
                    {
                        $date1 = $item->b_from;
                        $date2 = $item->b_to;
                        $datediff = (strtotime($date2) - strtotime($date1));
                        $hours +=  floor($datediff / (60 * 60 ));
                    }
                }
            }
            if($product_data->rent_term == 1)
            $data['num_days']  = $num_days;
            else
            {
                if($hours >= 24)
                {
                    $days   = floor($hours / 24);
                    $hours %= 24;
                }
                $data['num_days']  = $days;
                $data['num_hrs']   = $hours;
            }
            $data['SN']  = $SN;
        }
        else
        {
            $this->db->select('*');
            $this->db->from('product_sup_items');
            $this->db->where('serial_number', $SN);
            $this->db->limit(1);
            $row            = $this->db->get()->row();
            if(!empty($row))
            {
            $id             = $row->id;
            $product_sup_id = $row->product_id;
            }
            if(!empty($id))//this SN for sup item
            {
                $this->db->select('*');
                $this->db->from('products_sup');
                $this->db->where('id', $product_sup_id);
                $product_data  = $this->db->get()->row();
                $data['pname'] = $product_data->n_en;
                $this->db->select('*');
                $this->db->from('booking_sup_items_delivery');
                $this->db->where('sup_id', $id);
                $deliverd_data     = $this->db->get()->result();
                $data['flag']      = true;
                $data['num_trans'] = count($deliverd_data);
                foreach($deliverd_data as $one)
                {
                    $this->db->select('*');
                    $this->db->from('booking_items');
                    $this->db->where('book_id', $one->book_id);
                    $booking_items = $this->db->get()->result();
                    foreach($booking_items as $item)
                    {
                        if($product_data->rent_term == 1)//day
                        {
                        $days      = createDateRangeArraygivenStamp($item->b_from, $item->b_to);
                        $num_days += count($days);
                        }
                        else // hour
                        {
                            $date1 = $item->b_from;
                            $date2 = $item->b_to;
                            $datediff = (strtotime($date2) - strtotime($date1));
                            $hours +=  floor($datediff / (60 * 60 ));
                        }
                    }
                }
                if($product_data->rent_term == 1)
                $data['num_days']  = $num_days;
                else
                {
                    if($hours >= 24)
                    {
                        $days   = floor($hours / 24);
                        $hours %= 24;
                    }
                    $data['num_days']  = $days;
                    $data['num_hrs']   = $hours;
                }
                $data['SN']  = $SN;
            }
            else
            $data['flag'] = false;
        }
        return $data;
       
    }

    function get_total_trans_period($from, $to)
    {
        $result = array();
        $this->db->select('product_id');
        $this->db->from('booking_items');
        $this->db->where('b_from >=', $from);
        $this->db->where('b_to <=', $to);
        $this->db->distinct();
        $booking_items = $this->db->get()->result();
        if(!empty($booking_items))
        {
            foreach($booking_items as $item)
            {
                $this->db->select('*');
                $this->db->from('product_items');
                $this->db->where('product_id', $item->product_id);
                $item_data = $this->db->get()->result();
                if(!empty($item_data))//get all items for this product
                {
                    foreach($item_data as $one)
                    {
                        $dt = $this->get_total_transaction($one->serial_number);
                        if($dt['flag'] == true)
                            $result[] = $dt;
                    }
                }
                //check if this product has sup items
                $this->db->select('*');
                $this->db->from('products_sup');
                $this->db->where('product_id', $item->product_id);
                $product_sup_data = $this->db->get()->result();
                if(!empty($product_sup_data))
                {
                    foreach($product_sup_data as $sup_product)
                    {
                        $this->db->select('*');
                        $this->db->from('product_sup_items');
                        $this->db->where('product_id', $sup_product->id);
                        $sup_items = $this->db->get()->result();
                        if(!empty($sup_items))
                        {
                            foreach($sup_items as $sup)
                            {
                                $dt = $this->get_total_transaction($sup->serial_number);
                                if($dt['flag'] == true)
                                $result[] = $dt;
                            }
                        }
                    }
                }
            }
        }
        return $result;
    }

    function get_total_sn_inv($from, $to, $type)
    {
        $num_op = 0;
        $data['total_op'] = 0;
        //get total of sn in inv
        $this->db->select('*');
        if($type == 1)
        {
        $this->db->from('product_items');
        $this->db->where('sold !=', 1);
        }
        else if ($type == 2)
        $this->db->from('product_sup_items');
        $this->db->where('deleted !=', 1);
        $this->db->where('exist', 1);
        $this->db->where('missing !=', 1);
        $this->db->where('damaged !=', 1);
        $data['total_sn_inv'] = $this->db->count_all_results();
        //get total of sn operational between from & to
        $this->db->select('*');
        $this->db->from('booking_items');
        $this->db->where('b_from >=', $from);
        $this->db->where('b_to <=', $to);
        $booking_items = $this->db->get()->result();
        if(!empty($booking_items))
        {
            foreach($booking_items as $item)
            {
                if($type == 1)
                {
                $this->db->select('item_id');
                $this->db->from('booking_items_return');
                $this->db->where('book_id', $item->book_id);
                $this->db->where('product_id', $item->product_id);
                }
                else if ($type == 2)
                {
                $this->db->select('sup_id');
                $this->db->from('booking_sup_items_return');
                $this->db->where('book_id', $item->book_id);
                }
                $this->db->distinct();
                $num_op += $this->db->count_all_results();
            }
            $data['total_op'] = $num_op;
        }
        //get total of sn in maintenance
        $this->db->select('*');
        $this->db->from('maintenance');
        if($type == 1)
        $this->db->where('item_id !=', null);
        else if($type == 2)
        $this->db->where('sup_id !=', null);
        $this->db->where('m_from >=', $from);
        $this->db->where('m_to <=', $to);
        $data['total_main'] = $this->db->count_all_results();
        return $data;

    }

    function get_avg_basket($from, $to)
    {
        $this->db->select('count(*) as c, SUM(total) as sum');
        $this->db->from('booking');
        $this->db->where_in('status', [1,2]);
        $this->db->where('b_from >=', $from);
        $this->db->where('b_to <=', $to);
        $this->db->where('canceled !=', 1);
        $this->db->where('deleted !=', 1);
        $res = $this->db->get()->row();
        $data['total_revenu']   = $res->sum;
        $data['total_booking']  = $res->c;
        $data['avg']            = $res->sum / $res->c;
        return $data;
    }

    function get_total_revenue($from, $to, $for, $filter)
    {
        $data = array();
        $data['total_revenu']  = 0;
        $data['total_booking'] = 0;
        if($for == 1) //filter by item
        {
            $this->db->select('serial_number');
            $this->db->from('product_items');
            $this->db->where('id', $filter);
            $data['name'] = $this->db->get()->row('serial_number');
            $this->db->select('*');
            $this->db->from('booking_items');
            $this->db->where('b_from <=', $from);
            $this->db->where('b_to >=', $to);
            $res = $this->db->get()->result();
            if(!empty($res))
            {
                foreach($res as $one)
                {
                    $this->db->select('*');
                    $this->db->from('booking_items_delivery');
                    $this->db->where('item_id', $filter);
                    $this->db->where('book_id', $one->book_id);
                    $this->db->where('product_id', $one->product_id);
                    $this->db->limit(1);
                    $row = $this->db->get()->row();
                    if(!empty($row))
                    {
                        $data['total_revenu']   += $one->rent_price * $one->quantity;
                        $data['total_booking']  += 1; 
                    }
                }

            }
        }
        else if($for == 2) //filter by brand
        {
            $this->db->select('n_en');
            $this->db->from('brands');
            $this->db->where('id', $filter);
            $data['name'] = $this->db->get()->row('n_en');
            $this->db->select('product_id');
            $this->db->from('product_brands');
            $this->db->where('brand_id', $filter);
            $product_brnads = $this->db->get()->result();
            $product_id = array_column($product_brnads, "product_id");
            if(!empty($product_id))
            {
                $this->db->select('*');
                $this->db->from('booking_items');
                $this->db->where('b_from <=', $from);
                $this->db->where('b_to >=', $to);
                $this->db->where_in('product_id', $product_id);
                $res = $this->db->get()->result();
                if(!empty($res))
                {
                    foreach($res as $one)
                    {
                        $this->db->select('*');
                        $this->db->from('booking_items_delivery');
                        $this->db->where('book_id', $one->book_id);
                        $this->db->where('product_id', $one->product_id);
                        $this->db->limit(1);
                        $row = $this->db->get()->row();
                        if(!empty($row))
                        {
                            $data['total_revenu']   += $one->rent_price * $one->quantity;
                            $data['total_booking']  += 1;
                        }
                    }

                }
            }
        }
        else if($for == 3) //filter by category
        {
            $this->db->select('n_en');
            $this->db->from('categories');
            $this->db->where('id', $filter);
            $data['name'] = $this->db->get()->row('n_en');
           
            //get sub categories for this category
            $this->db->select('id');
            $this->db->from('categories');
            $this->db->where('parent', $filter);
            $sub_categ = $this->db->get()->result();
            $categories = array_column($sub_categ, "id");
            $this->db->select('product_id');
            $this->db->from('product_categories');
            $this->db->where_in('category_id', $categories);
            $product_categoris = $this->db->get()->result();
            $product_id = array_column($product_categoris, "product_id");
            if(!empty($product_id))
            {
                $this->db->select('*');
                $this->db->from('booking_items');
                $this->db->where('b_from <=', $from);
                $this->db->where('b_to >=', $to);
                $this->db->where_in('product_id', $product_id);
                $res = $this->db->get()->result();
                if(!empty($res))
                {
                    foreach($res as $one)
                    {
                        $this->db->select('*');
                        $this->db->from('booking_items_delivery');
                        $this->db->where('book_id', $one->book_id);
                        $this->db->where('product_id', $one->product_id);
                        $this->db->limit(1);
                        $row = $this->db->get()->row();
                        if(!empty($row))
                        {
                            $data['total_revenu']   += $one->rent_price * $one->quantity;
                            $data['total_booking']  += 1;
                        }
                    }

                }
            }
        }
       
        return $data;
    }

    function get_individual_trans($from, $to, $for, $filter)
    {
        $data = array();
        $output = array();
        $data['total_revenu']  = 0;
        $data['total_booking'] = 0;

        if($for == 1) //filter by item
        {
            $this->db->select('serial_number');
            $this->db->from('product_items');
            $this->db->where('id', $filter);
            $data['name'] = $this->db->get()->row('serial_number');
            $this->db->select('*');
            $this->db->from('booking_items');
            $this->db->where('b_from <=', $from);
            $this->db->where('b_to >=', $to);
            $res = $this->db->get()->result();
            if(!empty($res))
            {
                foreach($res as $one)
                {
                    $this->db->select('*');
                    $this->db->from('booking_items_delivery');
                    $this->db->where('item_id', $filter);
                    $this->db->where('book_id', $one->book_id);
                    $this->db->where('product_id', $one->product_id);
                    $this->db->limit(1);
                    $row = $this->db->get()->row();
                    if(!empty($row))
                    {
                        $this->db->select('c.n_en as cname');
                        $this->db->from('booking');
                        $this->db->where('booking.id', $one->book_id);
                        $this->db->join('clients c', 'c.id = booking.client_id');
                        $this->db->limit(1);
                        $data['cname']     = $this->db->get()->row('cname');
                        $data['revenue']   = $one->rent_price * $one->quantity;
                        $output[] = $data;

                    }
                }

            }
        }
        else if($for == 2) //filter by brand
        {
            $this->db->select('n_en');
            $this->db->from('brands');
            $this->db->where('id', $filter);
            $data['name'] = $this->db->get()->row('n_en');
            $this->db->select('product_id');
            $this->db->from('product_brands');
            $this->db->where('brand_id', $filter);
            $product_brnads = $this->db->get()->result();
            $product_id = array_column($product_brnads, "product_id");
            if(!empty($product_id))
            {
                $this->db->select('*');
                $this->db->from('booking_items');
                $this->db->where('b_from <=', $from);
                $this->db->where('b_to >=', $to);
                $this->db->where_in('product_id', $product_id);
                $res = $this->db->get()->result();
                if(!empty($res))
                {
                    foreach($res as $one)
                    {
                        $this->db->select('*');
                        $this->db->from('booking_items_delivery');
                        $this->db->where('book_id', $one->book_id);
                        $this->db->where('product_id', $one->product_id);
                        $this->db->limit(1);
                        $row = $this->db->get()->row();
                        if(!empty($row))
                        {
                            $this->db->select('c.n_en as cname');
                            $this->db->from('booking');
                            $this->db->where('booking.id', $one->book_id);
                            $this->db->join('clients c', 'c.id = booking.client_id');
                            $this->db->limit(1);
                            $data['cname']     = $this->db->get()->row('cname');
                            $data['revenue']   = $one->rent_price * $one->quantity;
                            $output[] = $data;

                           
                        }
                    }

                }
            }
        }
        else if($for == 3) //filter by category
        {
            $this->db->select('n_en');
            $this->db->from('categories');
            $this->db->where('id', $filter);
            $data['name'] = $this->db->get()->row('n_en');
           
            //get sub categories for this category
            $this->db->select('id');
            $this->db->from('categories');
            $this->db->where('parent', $filter);
            $sub_categ = $this->db->get()->result();
            $categories = array_column($sub_categ, "id");
            $this->db->select('product_id');
            $this->db->from('product_categories');
            $this->db->where_in('category_id', $categories);
            $product_categoris = $this->db->get()->result();
            $product_id = array_column($product_categoris, "product_id");
           
            if(!empty($product_id))
            {
                $this->db->select('*');
                $this->db->from('booking_items');
                $this->db->where('b_from <=', $from);
                $this->db->where('b_to >=', $to);
                $this->db->where_in('product_id', $product_id);
                $res = $this->db->get()->result();
                if(!empty($res))
                {
                    foreach($res as $one)
                    {
                        $this->db->select('*');
                        $this->db->from('booking_items_delivery');
                        $this->db->where('book_id', $one->book_id);
                        $this->db->where('product_id', $one->product_id);
                        $this->db->limit(1);
                        $row = $this->db->get()->row();
                        if(!empty($row))
                        {
                            $this->db->select('c.n_en as cname');
                            $this->db->from('booking');
                            $this->db->where('booking.id', $one->book_id);
                            $this->db->join('clients c', 'c.id = booking.client_id');
                            $this->db->limit(1);
                            $data['cname']     = $this->db->get()->row('cname');
                            $data['revenue']   = $one->rent_price * $one->quantity;
                            $output[] = $data;
                        }
                    }

                }
            }
        }
     
        return $output;
    }

    function get_revenue_vs_expense($SN)
    {
        $total_price = 0;
        $cost        = 0;
        $sup_cost    = 0;
        $data        = array();
        $data['sn']  = $SN;
        $this->db->select('id, product_id, cost_price');
        $this->db->from('product_items');
        $this->db->where('serial_number', $SN);
        $res                = $this->db->get()->row();
        if(!empty($res))
        {
            $item_id            = $res->id;
            $product_id         = $res->product_id;
            $data['cost_price'] = $res->cost_price;
            $data['flag'] = true;
            $this->db->select('*');
            $this->db->from('booking_items_delivery');
            $this->db->where('item_id', $item_id);
            $items = $this->db->get()->result();
            if(!empty($items))
            {
                foreach($items as $one)
                {
                    $this->db->select('*');
                    $this->db->from('booking_items');
                    $this->db->where('book_id', $one->book_id);
                    $this->db->where('product_id', $one->product_id);
                    $this->db->limit(1);
                    $row = $this->db->get()->row();
                    if(!empty($row))
                    $total_price += $row->rent_price * $row->quantity;
                }
            }
            $data['total_revenue'] = $total_price;
            $this->db->select('transaction_id');
            $this->db->from('maintenance');
            $this->db->where('item_id', $item_id);
            $transaction_id = $this->db->get()->result();
            if(!empty($transaction_id))
            {
                $trans_id = array_column($transaction_id, "transaction_id");
                $this->db->select('SUM(cost) as cost');
                $this->db->from('transaction');
                $this->db->where_in('id', $trans_id);
                $cost = $this->db->get()->row('cost');
            }
            $data['item_cost'] = $cost;
            $this->db->select('id');
            $this->db->from('products_sup');
            $this->db->where('product_id', $product_id);
            $this->db->where('deleted !=', 1);
            $product_sup = $this->db->get()->result();
            if(!empty($product_sup))
            {
                $product_sup_id = array_column($product_sup, "id");
                $this->db->select('id');
                $this->db->from('product_sup_items');
                $this->db->where_in('product_id', $product_sup_id);
                $this->db->where('deleted !=', 1);
                $sup_id = $this->db->get()->result();
                if(!empty($sup_id))
                {
                    $sup_ids = array_column($sup_id, "id");
                    $this->db->select('transaction_id');
                    $this->db->from('maintenance');
                    $this->db->where_in('sup_id', $sup_ids);
                    $transaction_id = $this->db->get()->result();
                    if(!empty($transaction_id))
                    {
                        $trans_id = array_column($transaction_id, "transaction_id");
                        $this->db->select('SUM(cost) as cost');
                        $this->db->from('transaction');
                        $this->db->where_in('id', $trans_id);
                        $sup_cost = $this->db->get()->row('cost');
                    }
                }
            }
            $data['sup_cost'] = $sup_cost;
        }
        else
        {
            $data['flag'] = false;
            $data['msg']  = lang("this_SN_does_not_exist");
        }
        return $data;
    }

    function get_op_eff($SN)
    {
        $data   = array();
        $today  = date('Y-m-d H:i:s');
        $days   = 0;
        $data['sn']  = $SN;
        $this->db->select('*');
        $this->db->from('product_items');
        $this->db->where('serial_number', $SN);
        $item = $this->db->get()->row();
        if(!empty($item))
        {
            $data['flag'] = true;
            $day_added = $item->timestamp;
            $datediff  = (strtotime($today) - strtotime($day_added));
            $hours     =  floor($datediff / (60 * 60 ));
            $days     += floor($hours / 24);
            $data['days_owned'] = $days;
            $this->db->select('*');
            $this->db->from('booking_items_delivery');
            $this->db->where('item_id', $item->id);
            $booked = $this->db->get()->result();
            $days = 0;
            if(!empty($booked))
            {
                foreach($booked as $one)
                {
                    $this->db->select('*');
                    $this->db->from('booking_items');
                    $this->db->where('book_id', $one->book_id);
                    $this->db->where('product_id', $one->product_id);
                    $this->db->limit(1);
                    $row = $this->db->get()->row();
                    if(!empty($row))
                    {
                        $datediff  = (strtotime($row->b_to) - strtotime($row->b_from));
                        $hours     =  floor($datediff / (60 * 60 ));
                        $days     += floor($hours / 24);
                    }
                }
            }
            $data['days_rented'] = $days;
            $this->db->select('*');
            $this->db->from('maintenance');
            $this->db->where('item_id', $item->id);
            $maint = $this->db->get()->result();
            $days = 0;
            if(!empty($maint))
            {
                foreach($maint as $one)
                {
                    $datediff  = (strtotime($one->m_to) - strtotime($one->m_from));
                    $hours     =  floor($datediff / (60 * 60 ));
                    $days     += floor($hours / 24);
                }
            }
            $data['days_maint'] = $days;
        }
        else
        {
            $data['flag'] = false;
            $data['msg']  = lang("this_SN_does_not_exist");
        }
        return $data;
    }
}
