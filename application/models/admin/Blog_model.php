<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Blog_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_categories() {
        $this->db->select("blog_categories.*");
        $this->db->from('blog_categories');
        $this->db->where("blog_categories.deleted !=", 1);
        $query = $this->db->get();
        return $query->result();
    }

    function get_data() {
        $this->db->select("blog.*, blog_categories.n_" . lc() . " as category, users.n_" . lc() . " as uname");
        $this->db->from('blog');
        $this->db->join("blog_categories", "blog_categories.id = blog.category_id", 'left');
        $this->db->join("users", "users.id = blog.created_by", 'left');
        $this->db->where("blog.deleted !=", 1);
        $query = $this->db->get();
        return $query->result();
    }

    function get_info($id = 0) {
        $this->db->select("blog.*");
        $this->db->from('blog');
        $this->db->where("deleted !=", 1);
        $this->db->where("id", $id);
        return $this->db->get()->row();
    }

    function get_tags($id = 0) {
        $this->db->select("blog_tags.*");
        $this->db->from('blog_tags');
        $this->db->join("blog", "blog.id = blog_tags.blog_id", 'left');
        $this->db->where("blog.deleted !=", 1);
        $this->db->where("blog_tags.blog_id", $id);
        $query = $this->db->get();
        return $query->result();
    }

    function delete_blog_tags($blog_id) {
        $this->db->where("blog_id", $blog_id);
        $this->db->delete('blog_tags');
    }

    function get_comments($blog_id = NULL) {
        $this->db->select("blog_comments.*, clients.n_" . lc() . " as cname");
        $this->db->from('blog_comments');
        $this->db->join("blog", "blog.id = blog_comments.blog_id", 'left');
        $this->db->join("clients", "clients.id = blog_comments.cid", 'left');
        $this->db->where("blog_comments.deleted !=", 1);
        $this->db->where("blog.deleted !=", 1);
        $this->db->where("blog_comments.blog_id", $blog_id);
        $query = $this->db->get();
        return $query->result();
    }

    function resizeImage($filename, $width, $height) {
        $source_path = FCPATH . '/uploads/blogs/' . $filename;
        $target_path = FCPATH . '/uploads/thumb/';
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => FALSE,
            'create_thumb' => TRUE,
            'thumb_marker' => '',
            'width' => $width,
            'height' => $height
        );
        $this->load->library('image_lib', $config_manip);
        $this->image_lib->initialize($config_manip);
        $this->image_lib->resize();
        $this->image_lib->clear();
    }

}
