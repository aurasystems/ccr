<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Voucher_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function delete($voucher_id)
    {
        
            $data = array(
                'deleted' => '1',
            );
            $this->db->where('id', $voucher_id);
            return $this->db->update('vouchers', $data);
      
    }
}
