<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Booking_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_pending_orders() {
        $this->db->select("booking.*, clients.n_" . lc() . " as cname, locations.n_" . lc() . " as lname");
        $this->db->from('booking');
        $this->db->where("booking.status", 0);
        $this->db->where("booking.deleted !=", 1);
        $this->db->join("clients", "clients.id = booking.client_id", 'left');
        $this->db->join("locations", "locations.id = booking.branch", 'left');
        $query = $this->db->get();
        return $query->result();
    }

    function get_approved_orders() {
        $this->db->select("booking.*, clients.n_" . lc() . " as cname, locations.n_" . lc() . " as lname");
        $this->db->from('booking');
        $this->db->group_start();
        $this->db->group_start();
        $this->db->where("booking.status", 1);
        $this->db->group_end();
        $this->db->or_group_start();
        $this->db->where("booking.status", 2);
        $this->db->group_end();
        $this->db->group_end();
        $this->db->where("booking.deleted !=", 1);
        $this->db->join("clients", "clients.id = booking.client_id", 'left');
        $this->db->join("locations", "locations.id = booking.branch", 'left');
        $query = $this->db->get();
        return $query->result();
    }

    function get_declined_orders() {
        $this->db->select("booking.*, clients.n_" . lc() . " as cname, decline_reason.n_" . lc() . " as rname, locations.n_" . lc() . " as lname");
        $this->db->from('booking');
        $this->db->where("booking.status", 3);
        $this->db->where("booking.deleted !=", 1);
        $this->db->join("clients", "clients.id = booking.client_id", 'left');
        $this->db->join("decline_reason", "decline_reason.id = booking.decline_reason", 'left');
        $this->db->join("locations", "locations.id = booking.branch", 'left');
        $query = $this->db->get();
        return $query->result();
    }

    function get_info($id = 0) {
        $this->db->select("products.*");
        $this->db->from('products');
        $this->db->where("deleted !=", 1);
        $this->db->where("id", $id);
        return $this->db->get()->row();
    }

    function booking_items($booking_id) {
        $this->db->select("booking_items.*, products.n_" . lc() . " as pname, products.img as pimg");
        $this->db->from('booking_items');
        $this->db->join("products", "products.id = booking_items.product_id", 'left');
        $this->db->where('booking_items.book_id', $booking_id);
        $query = $this->db->get();
        return $query->result();
    }

    function insert_timetable($data) {
        $this->db->insert('technicians_hours', $data);
    }

    function update_timetable($technician_id, $day, $data) {
        $this->db->where('technician_id', $technician_id);
        $this->db->where('day', $day);
        $this->db->update('technicians_hours', $data);
    }

    function delete_booking_items($table_name, $booking_id, $is_delivery = false) {
        if ($is_deliver == true) {
            $this->db->select('*')->from($table_name);
            $deliver_ids = $this->db->get()->result();
            foreach ($deliver_ids as $one) {
                if ($one->id + $booking_id == $one->book_id) {
                    $this->db->where("book_id", $one->book_id);
                    $this->db->delete($table_name);
                }
            }
        } else {
            $this->db->where("book_id", $booking_id);
            $this->db->delete($table_name);
        }
    }

    function get_distinct_categories() {
        $ids_arr = [];
        $result = [];
        $this->db->distinct('category_id');
        $this->db->select("product_categories.category_id");
        $this->db->from('product_categories');
        $query = $this->db->get();
        $ids = $query->result();
        foreach ($ids as $id) {
            if (!in_array($id->category_id, $ids_arr)) {
                $ids_arr[] = $id->category_id;
            }
        }
        for ($i = 0; $i < count($ids_arr); $i++) {
            $this->db->select("categories.id, categories.n_en");
            $this->db->from('categories');
            $this->db->where("categories.deleted !=", 1);
            $this->db->where("categories.id", $ids_arr[$i]);
            $one = $this->db->get()->row();
            $one->cat_id = $ids_arr[$i];
            $result[] = $one;
        }
        return $result;
    }

    function get_related_product($category_id) {
        $this->db->select("product_categories.*, products.n_" . lc() . " as pname, products.img as pimg");
        $this->db->from('product_categories');
        $this->db->join("products", "products.id = product_categories.product_id");
        $this->db->where('product_categories.category_id', $category_id);
        $this->db->where('products.deleted !=', 1);
        return $this->db->get()->result();
    }

    function get_booking_waiting($product_id = 0) {
        $datetime = date('Y-m-d H:i:s');
        $this->db->select("booking_waiting_list.*, clients.n_" . lc() . " as cname, clients.email, clients.phone, products.n_" . lc() . " as pname");
        $this->db->from('booking_waiting_list');
        $this->db->where("booking_waiting_list.product_id", $product_id);
        $this->db->where("booking_waiting_list.b_from >", $datetime);
        $this->db->where("booking_waiting_list.notified !=", 1);
        $this->db->join("clients", "clients.id = booking_waiting_list.client_id", 'left');
        $this->db->join("products", "products.id = booking_waiting_list.product_id", 'left');
        $this->db->order_by('booking_waiting_list.id');
        $this->db->limit('1');
        return $this->db->get()->row();
    }

    function stock_check($booking_id, $product_id) {
        $desc = '';
        //$arr = [];
        $total = 0;
        $this->db->select("booking.*, locations.n_" . lc() . " as lname");
        $this->db->from('booking');
        $this->db->join("locations", "locations.id = booking.branch", 'left');
        $this->db->where("booking.deleted !=", 1);
        $this->db->where("booking.id", $booking_id);
        $booking = $this->db->get()->row();
        if (!empty($booking)) {
            $this->db->select('*');
            $this->db->from('product_items');
            $this->db->where('product_id', $product_id);
            $this->db->where('location_id', $booking->branch);
            $this->db->where("damaged", 0);
            $this->db->where("exist", 1);
            $this->db->where("deleted !=", 1);
            $this->db->where("sold !=", 1);
            $items = $this->db->count_all_results();
            if ($items > 0) {
                $desc .= '(' . $items . ') ' . lang('available') . ' ' . lang('in_booking_branch') . ' ' . $booking->lname . '<br><br>';
                //$arr[] = ['location' => $booking->lname, 'count' => $items];
                $total = $items;
            } else {
                $desc .= lang('no_items_in_branch') . ' ' . $booking->lname . '<br><br>';
                $this->db->select("locations.id, locations.n_" . lc() . " as lname");
                $this->db->from('locations');
                $this->db->where('id !=', $booking->branch);
                $this->db->where('deleted !=', 1);
                $locations = $this->db->get()->result();
                if (!empty($locations)) {
                    foreach ($locations as $row) {
                        $this->db->select('*');
                        $this->db->from('product_items');
                        $this->db->where('product_id', $product_id);
                        $this->db->where('location_id', $row->id);
                        $this->db->where("damaged", 0);
                        $this->db->where("exist", 1);
                        $this->db->where("deleted !=", 1);
                        $this->db->where("sold !=", 1);
                        $l_items = $this->db->count_all_results();
                        if ($l_items > 0) {
                            $desc .= '(' . $l_items . ') ' . lang('available') . ' ' . lang('in_branch') . ' ' . $row->lname . '<br>';
                            //$arr[] = ['location' => $row->lname, 'count' => $l_items];
                        }
                    }
                }
            }
        }
        //echo $tt;die;
        //print_r($arr);die;
        return $desc;
    }

    function get_booked_data_ordered() {
        $this->db->select('booking.*,c.n_en as cname, l.n_en as lname')->from('booking');
        $this->db->where('booking.deleted !=', 1);
        $this->db->join('clients c', 'c.id = booking.client_id');
        $this->db->join('locations l', 'l.id = booking.branch');
        $this->db->order_by("b_from", "ASE");
        return $this->db->get()->result();
    }

    function get_booked_items_ordered() {
        $this->db->select('booking.*,c.n_en as cname, l.n_en as lname')->from('booking');
        $this->db->where('booking.deleted !=', 1);
        $this->db->join('clients c', 'c.id = booking.client_id');
        $this->db->join('locations l', 'l.id = booking.branch');
        $this->db->order_by("b_from", "ASE");
        return $this->db->get()->result();
    }

    function add_to_delivery_temp($items, $book_id) {
        $this->db->select('*');
        $this->db->from('items_delivery_temp');
        $this->db->where('book_id', $book_id);
        $is_exist = $this->db->get()->result();
        if (!empty($is_exist))
            return;
        foreach ($items as $one) {
            $data = [
                'prep_id' => $one->id,
                'book_id' => $one->book_id,
                'product_id' => $one->product_id,
                'item_id' => $one->item_id,
            ];
            $this->db->insert('items_delivery_temp', $data);
            $temp_delivery_id = $this->db->insert_id();
            $this->db->select('*');
            $this->db->from('products_sup');
            $this->db->where('product_id', $one->product_id);
            $sup_data = $this->db->get()->result();
            if (!empty($sup_data)) {
                foreach ($sup_data as $sup) {
                    $data = [
                        'delivery_id' => $temp_delivery_id,
                        'book_id' => $one->book_id,
                        'product_sup_id' => $sup->id,
                    ];
                    $this->db->insert('sup_items_temp', $data);
                }
            }
        }
    }

    function get_prep_items($booking_id = null) {
        $res = array();
        $this->db->select('booking_items_preparation.*, p.n_en as pname')
                ->from('booking_items_preparation')
                ->where('book_id', $booking_id);
        $this->db->join('products p', 'p.id = booking_items_preparation.product_id');
        $res = $this->db->get()->result();

        return $res;
    }

    function get_delivery_record($book_id, $prep_id, $product_id) {
        $this->db->select('*');
        $this->db->from('booking_items_delivery');
        $this->db->where('book_id', $book_id);
        $this->db->where('prep_id', $prep_id);
        $this->db->where('product_id', $product_id);
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    function get_sup_delivery_record($book_id, $deliver_id, $product_supID) {
        $this->db->select('*');
        $this->db->from('booking_sup_items_delivery');
        $this->db->where('book_id', $book_id);
        $this->db->where('delivery_id', $deliver_id);
        $this->db->where('product_sup_id', $product_supID);
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    function get_delivery_temp($book_id) {
        $this->db->select('*');
        $this->db->from('items_delivery_temp');
        $this->db->where('book_id', $book_id);
        return $this->db->get()->result();
    }

    function get_sup_delivery_temp($book_id, $deliver_id) {
        $this->db->select('*');
        $this->db->from('sup_items_temp');
        $this->db->where('book_id', $book_id);
        $this->db->where('delivery_id', $deliver_id);
        return $this->db->get()->result();
    }

    function get_delivery_sup_items($delivery_id) {
        $this->db->select('*');
        $this->db->from('booking_sup_items_delivery');
        $this->db->where('delivery_id', $delivery_id);
        $this->db->where('deleted !=', 1);
        $this->db->where('sup_id !=', null);
        return $this->db->get()->result();
    }

    function get_client_name($booking_id) {
        $this->db->select('c.n_en')->from('booking')->where('booking.id', $booking_id);
        $this->db->join('clients c', 'c.id = booking.client_id');
        return $this->db->get()->row()->n_en;
    }

    function get_prodID($prep_id) {
        $this->db->select('product_id')->from('booking_items_preparation')
                ->where('id', $prep_id);

        return $this->db->get()->row('product_id');
    }

    function get_prepID($deliver_id) {
        $this->db->select('prep_id')->from('booking_items_delivery')
                ->where('id', $deliver_id);
        return $this->db->get()->row('prep_id');
    }

    function getItemsAvailable_relatedToBookID($booking_id, $table, $is_deliver = false) {
        $data = array();
        $res = array();
        $i = 0;
        if ($is_deliver == true) {
            $this->db->select('*')->from($table);
            $deliver_id = $this->db->get()->result();
            foreach ($deliver_id as $deliver) {
                if ($deliver->id + $booking_id == $deliver->book_id) {
                    $res[$i]['product_id'] = $deliver->product_id;
                    $i++;
                }
            }
            // print_r($res);
        } else if ($is_deliver == false) {
            $this->db->select('product_id')->from($table)
                    ->where('book_id', $booking_id)
                    ->where('item_id', null);
            $res = $this->db->get()->result();
        }
        if (!empty($res)) {
            $products_id = array_column($res, "product_id");
            $uItems = $this->db->select('item_id')->from($table)->where('item_id !=', null)
                            ->where_in('product_id', $products_id)->get()->result();
            $used_items = array_column($uItems, "item_id");
            $this->db->select('id, serial_number as SN')->from('product_items');
            $this->db->where_in('product_id', $products_id);
            if (!empty($used_items))
                $this->db->where_not_in('id', $used_items);
            $this->db->where('deleted !=', 1);
            $this->db->where('sold !=', 1);
            $this->db->where('damaged !=', 1);
            $this->db->where('exist !=', 0);
            $res = $this->db->get()->result();
            if (count($res) > 0) {
                $data['flag'] = true;
                $data['available_items'] = $res;
            } else {
                $data['flag'] = false;
                $data['msg'] = lang("item_doesnt_exist");
            }
        } else {
            $data['flag'] = false;
            $data['msg'] = lang("all_items_assigned_remove");
        }
        return $data;
    }

    function getItemsAvailable_relatedToBookID_delivery($book_id, $table, $scan_num) {
        $this->db->select('*')->from('product_items')
                ->where('serial_number', $scan_num)->where('sold !=', 1)->where('deleted!=', 1)
                ->where('exist', 1)->where('damaged !=', 1)->where('missing !=', 1);
        $items_data = $this->db->get()->row();
        // print_r($items_data);
        if (!empty($items_data)) { // if the scanned number for product
            $this->db->select('*')->from($table)->where('item_id', $items_data->id)->where('book_id', $book_id);
            $used = $this->db->get()->result();
            if (!empty($used)) {
                $data['flag'] = false;
                $data['msg'] = 'this item used for another product';
            } else {
                $data['flag'] = true;
                $data['is_sup'] = 0;
                $data['available_items'] = $items_data;
            }
        } else { //if scanned number of sub product
            $this->db->select('*')->from('product_sup_items')
                    ->where('serial_number', $scan_num)
                    ->where('deleted !=', 1)
                    ->where('missing !=', 1)
                    ->where('damaged !=', 1)
                    ->where('exist', 1);
            $items_sup_data = $this->db->get()->row();
            if (!empty($items_sup_data)) {
                $this->db->select('*')->from('sup_items_temp')->where('sup_id', $items_sup_data->id)
                        ->where('book_id', $book_id);
                $used = $this->db->get()->result();
                if (!empty($used)) {
                    $data['flag'] = false;
                    $data['msg'] = lang("sup_item_used_product");
                } else {
                    $data['flag'] = true;
                    $data['is_sup'] = 1;
                    $data['available_items'] = $items_sup_data;
                }
            } else {
                $data['flag'] = false;
                $data['msg'] = lang("item_doesnt_exist");
            }
        }
        //print_r($data);
        return $data;
    }

    function book_scanned_item_delivery($booking_id, $item_id, $product_id, $table, $is_sup = false) {

        if ($is_sup) {
            $product_supID = $product_id;
            $this->db->select('product_id')->from('products_sup')
                    ->where('id', $product_id);
            $product_id = $this->db->get()->row('product_id');
        }

        if ($is_sup == false) {

            $this->db->select('id');
            $this->db->from($table);
            $this->db->where('product_id', $product_id);
            $this->db->where('book_id', $booking_id);
            $this->db->where('item_id', null);
            $this->db->order_by('id', 'ASC');
            $this->db->limit(1);
            $prep_id = $this->db->get()->row('id');
            if (!empty($prep_id)) {
                $this->db->where('id', $prep_id);
                $this->db->update($table, array('item_id' => $item_id)); //update prep table

                $this->db->where('prep_id', $prep_id);
                $this->db->update('items_delivery_temp', array('item_id' => $item_id)); //update temp delivery table
                return true;
            }
        } else if ($is_sup == true) {
            $this->db->select('id');
            $this->db->from($table);
            $this->db->where('product_id', $product_id);
            $this->db->where('book_id', $booking_id);
            $this->db->limit(1);
            $prep_id = $this->db->get()->row('id');
            $this->db->select('id');
            $this->db->from('items_delivery_temp');
            $this->db->where('prep_id', $prep_id);
            $deliver_id = $this->db->get()->row('id');

            $this->db->select('*');
            $this->db->from('sup_items_temp');
            $this->db->where('product_sup_id', $product_supID);
            $this->db->where('book_id', $booking_id);
            $this->db->where('delivery_id', $deliver_id);
            $this->db->where('sup_id', null);
            $deliver_sup = $this->db->get()->result();
            if (!empty($deliver_sup)) {
                $this->db->where('id', $deliver_sup[0]->id);
                $this->db->update('sup_items_temp', array('sup_id' => $item_id));
            } else
                return false;

            return true;
        }
        return false;

        /* $this->db->select('*')->from($table);
          $deliver_ids = $this->db->get()->result();
          $result = array();
          $i = 0;

          if ($is_sup) {
          $product_supID = $product_id;
          $this->db->select('product_id')->from('products_sup')
          ->where('id', $product_id);
          $product_id = $this->db->get()->row('product_id');
          }
          foreach ($deliver_ids as $one) {

          if (($one->id + $booking_id == $one->book_id) && ($product_id == $one->product_id)) {

          }
          }
          return false; */
    }

    function book_scanned_item($booking_id, $item_id, $table) {

        $this->db->select('product_id')->from('product_items')
                ->where('id', $item_id)->where('sold !=', 1);
        $product_id = $this->db->get()->row('product_id');
        $this->db->where('book_id', $booking_id);
        $this->db->where('product_id', $product_id);
        $this->db->where('item_id', null);
        $res = $this->db->get($table)->result();
        //  print_r($res);
        $prep_id = $res[0]->id;
        $this->db->where('id', $prep_id);
        $this->db->update($table, array('item_id' => $item_id));
        $this->db->select('id')->from($table)
                ->where('book_id', $booking_id)->where('product_id', $product_id);
        return $this->db->get()->row('id');
    }

    function addTodelivery($prep_id, $booking_id, $product_id, $item_id) {
        $this->db->select('id')->from('products_sup')
                ->where('product_id', $product_id);
        $product_supID = $this->db->get()->row('id');
        $this->db->select('id')->from('product_sup_items')
                ->where('product_id', $product_supID);
        $sup_itemID = $this->db->get()->row('id');

        $this->db->select('shutter_count')->from('product_items')
                ->where('id', $item_id)->where('sold !=', 1);
        $shutter_count = $this->db->get()->row('shutter_count');

        $this->db->select('id')->from('booking_items_delivery')
                ->where('prep_id', $prep_id);
        $id = $this->db->get()->row('id');
        if (!empty($id)) {
            $data = array(
                'prep_id' => $prep_id,
                'book_id' => $booking_id + $id,
                'product_id' => $product_id,
                'item_id' => $item_id,
                'sup_id' => $sup_itemID,
                'shutter_count' => $shutter_count,
            );
            $this->db->where('id', $id);
            $this->db->update('booking_items_delivery', $data);
        } else {
            $data = array(
                'prep_id' => $prep_id,
                'product_id' => $product_id,
                'item_id' => $item_id,
                'sup_id' => $sup_itemID,
                'shutter_count' => $shutter_count,
            );
            $this->db->insert('booking_items_delivery', $data);
            $insert_id = $this->db->insert_id();
            $this->db->where('id', $insert_id);
            $this->db->update('booking_items_delivery', array('book_id' => $booking_id + $insert_id));
        }
    }

    function remove_extraSup($book_id, $prep_id, $product_supID) {
        $this->db->select('id');
        $this->db->from('items_delivery_temp');
        $this->db->where('prep_id', $prep_id);
        $this->db->limit(1);
        $deliver_id = $this->db->get()->row('id');
        $this->db->select('id');
        $this->db->from('sup_items_temp');
        $this->db->where('book_id', $book_id);
        $this->db->where('delivery_id', $deliver_id);
        $this->db->where('product_sup_id', $product_supID);
        $this->db->where('sup_id !=', null);
        $this->db->limit(1);
        $this->db->order_by('id', 'DESC');
        $id = $this->db->get()->row('id');
        $this->db->where('id', $id);
        $this->db->delete('sup_items_temp');
    }

    function remove_delivery_sup($deliver_supID) {
        $this->db->where('id', $deliver_supID);
        $this->db->update('booking_sup_items_delivery', ['deleted' => 1, 'sup_id' => null]);
    }

    function getDeliveryData($scan_num) {
        $this->db->select('booking.*, c.n_en as cname, c.cash_limit, c.credit_limit, l.n_en as lname');
        $this->db->from('booking');
        $this->db->join('booking_items_preparation p', 'p.book_id = booking.id');
        $this->db->join('product_items i', 'i.id = p.item_id', 'left');
        $this->db->join('clients c', 'c.id = booking.client_id');
        $this->db->join('locations l', 'l.id = booking.branch');
        $this->db->where('booking.deleted !=', 1);
        $this->db->where('booking.canceled !=', 1);
        $this->db->group_start();
        $this->db->where('booking.id', $scan_num);
        $this->db->or_where('c.n_en', $scan_num);
        $this->db->or_where('c.phone', $scan_num);
        $this->db->or_where('c.national_id', $scan_num);
        $this->db->group_end();
        return $this->db->get()->result();
    }

    /*  function getDeliveryData($scan_num) {
      $this->db->select('*')->from('booking_items_delivery');
      $delivery_data = $this->db->get()->result();
      $result = array();
      foreach ($delivery_data as $one) {
      if (($one->id + $scan_num) == $one->book_id) {//if item scanned is book id
      $this->db->select('booking.*,c.n_en as cname, l.n_en as lname')->from('booking');
      $this->db->where('booking.deleted !=', 1);
      $this->db->where('booking.id', $scan_num);
      $this->db->join('clients c', 'c.id = booking.client_id');
      $this->db->join('locations l', 'l.id = booking.branch');
      $this->db->order_by("b_from", "ASE");
      return $this->db->get()->result();
      }
      }
      //if the number scanned is item id
      $this->db->select('id, book_id')->from('booking_items_delivery')
      ->where('item_id', $scan_num);
      $result = $this->db->get()->row();
      //  print_r($result);
      if (!empty($result)) {
      $book_id = $result->book_id - $result->id;
      $this->db->select('booking.*,c.n_en as cname, l.n_en as lname')->from('booking');
      $this->db->where('booking.deleted !=', 1);
      $this->db->where('booking.id', $book_id);
      $this->db->join('clients c', 'c.id = booking.client_id');
      $this->db->join('locations l', 'l.id = booking.branch');
      $this->db->order_by("b_from", "ASE");
      return $this->db->get()->result();
      }
      return $result;
      } */

    function get_missing_items() {
        $this->db->select("booking_items_return.*, booking.client_id, booking.b_from, booking.b_to, clients.n_" . lc() . " as cname, product_items.serial_number, products.n_" . lc() . " as pname");
        $this->db->from('booking_items_return');
        $this->db->join("booking", "booking.id = booking_items_return.book_id");
        $this->db->join("clients", "clients.id = booking.client_id");
        $this->db->join("product_items", "product_items.id = booking_items_return.item_id");
        $this->db->join("products", "products.id = booking_items_return.product_id");
        $this->db->where('booking_items_return.missing', 1);
        $this->db->where('product_items.sold !=', 1);
        $this->db->where('booking.deleted !=', 1);
        return $this->db->get()->result();
    }

    function get_all_missing_items() {
        $this->db->select("booking_items_return.*");
        $this->db->from('booking_items_return');
        $this->db->where('missing', 1);
        $this->db->where('missing_pen_applied', 0);
        return $this->db->get()->result();
    }

    function get_damaged_items() {
        $this->db->select("booking_items_return.*, booking.client_id, booking.b_from, booking.b_to, clients.n_" . lc() . " as cname, product_items.serial_number, products.n_" . lc() . " as pname");
        $this->db->from('booking_items_return');
        $this->db->join("booking", "booking.id = booking_items_return.book_id");
        $this->db->join("clients", "clients.id = booking.client_id");
        $this->db->join("product_items", "product_items.id = booking_items_return.item_id");
        $this->db->join("products", "products.id = booking_items_return.product_id");
        $this->db->where('booking_items_return.damaged', 1);
        $this->db->where('product_items.sold !=', 1);
        $this->db->where('booking.deleted !=', 1);
        return $this->db->get()->result();
    }

    function get_booking_item($booking_id = NULL, $product_id = NULL) {
        $this->db->select("booking_items.*");
        $this->db->from('booking_items');
        $this->db->join("booking", "booking.id = booking_items.book_id");
        $this->db->where("booking_items.book_id", $booking_id);
        $this->db->where("booking_items.product_id", $product_id);
        $this->db->where("booking.deleted !=", 1);
        return $this->db->get()->row();
    }

    function get_client_items($book_id) {
        $res = array();
        $i = 0;
        $this->db->select('booking_items_delivery.*, p.n_en as pname')->from('booking_items_delivery');
        $this->db->join('products p', 'p.id = booking_items_delivery.product_id');
        $deliver_data = $this->db->get()->result();
        foreach ($deliver_data as $deliver) {
            if ($deliver->id + $book_id == $deliver->book_id) {
                $res[$i]['id'] = $deliver->id;
                $res[$i]['book_id'] = $deliver->book_id;
                $res[$i]['product_id'] = $deliver->product_id;
                $res[$i]['item_id'] = $deliver->item_id;
                $res[$i]['shutter_count'] = $deliver->shutter_count;
                $res[$i]['pname'] = $deliver->pname;
                $i++;
            }
        }
        // print_r($res);
        return $res;
    }

    function calculate_shutter($ret_shutter, $before_shutter, $delivery_id, $book_id, $product_id, $item_id) {
        $this->db->select('shutter_count')->from('product_items');
        $this->db->where('id', $item_id);
        $this->db->where('sold !=', 1);
        $fixed_shutter = $this->db->get()->row('shutter_count');
        $this->db->select('b_from, b_to')->from('booking_items');
        $this->db->where('book_id', $book_id);
        $this->db->where('product_id', $product_id);
        $booking_data = $this->db->get()->row();
        if (!empty($booking_data)) {
            $b_from = $booking_data->b_from;
            $b_to = $booking_data->b_to;
            $days = $this->createDateRangeArray($b_from, $b_to);
            $days_count = count($days);
            $max_shutter = intval($fixed_shutter) * intval($days);
            $curr_shutter = $ret_shutter - $before_shutter;
            if ($curr_shutter > $max_shutter)
                return true;
            else
                return false;
        }
    }

    function createDateRangeArray($strDateFrom = NULL, $strDateTo = NULL, $unix = FALSE, $stamp = 'Y-m-d') {
        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.
        // could test validity of dates here but I'm already doing
        // that in the main script

        $aryRange = [];

        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, $unix ? $iDateFrom : date($stamp, $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                array_push($aryRange, $unix ? $iDateFrom : date($stamp, $iDateFrom));
            }
        }
        return $aryRange;
    }

    function insert_returned_data($table, $data) {
        $this->db->select('*')->from($table);
        $this->db->where('book_id', $data['book_id']);
        $this->db->where('product_id', $data['product_id']);
        $this->db->where('item_id', $data['item_id']);
        // $this->db->where('sup_id', $data['sup_id']);
        $this->db->limit(1);
        $record = $this->db->get()->row();
        if (!empty($record)) {
            $this->db->where('book_id', $data['book_id']);
            $this->db->where('product_id', $data['product_id']);
            $this->db->where('item_id', $data['item_id']);
            //$this->db->where('sup_id', $data['sup_id']);
            $this->db->update($table, ['missing' => $data['missing'], 'damaged' => $data['damaged']]);
            return $record->id;
        } else {
            $this->db->insert($table, $data);
            return $this->db->insert_id();
        }
    }

    function insert_sup_returned_data($table, $data) {
        $this->db->select('*')->from($table);
        $this->db->where('book_id', $data['book_id']);
        $this->db->where('return_id', $data['return_id']);
        $this->db->where('product_sup_id', $data['product_sup_id']);
        $this->db->where('sup_id', $data['sup_id']);
        // $this->db->where('sup_id', $data['sup_id']);
        $this->db->limit(1);
        $record = $this->db->get()->row();
        if (!empty($record)) {
            $this->db->where('id', $record->id);
            $this->db->update($table, ['missing' => $data['missing'], 'damaged' => $data['damaged']]);
            return $record->id;
        } else {
            $this->db->insert($table, $data);
            return $this->db->insert_id();
        }
    }

    function get_ready_clients($book_id = null) {
        $book_ids = array();
        $book_id = array();
        $i = 0;
        if ($book_id == null) {
            $this->db->select('id, book_id');
            $this->db->from('booking_items_delivery');
            $book_delivery = $this->db->get()->result();
            foreach ($book_delivery as $one) {
                $book_ids[$i]['book_id'] = $one->book_id - $one->id;
                $i++;
            }
            $book_id = array_column($book_ids, "book_id");
            if (!empty($book_id)) {
                $this->db->select('*');
                $this->db->from('booking');
                $this->db->where_in('id', $book_id);
                return $this->db->get()->result();
            } else
                return array();
        } else {
            $this->db->select('*');
            $this->db->from('booking');
            $this->db->where('id', $book_id);
            return $this->db->get()->row();
        }
    }

    function client_booked_data($book_id, $client_id) {
        $i = 0;
        $res = array();
        $this->db->select('booking_items_delivery.*, p.n_en as pname')->from('booking_items_delivery');
        $this->db->join('products p', 'p.id = booking_items_delivery.product_id');
        $book_delivery = $this->db->get()->result();
        foreach ($book_delivery as $one) {
            if ($one->id + $book_id == $one->book_id) {
                $qunt = $this->get_quantity($book_id, $one->product_id);
                $res[$i]['id'] = $one->id;
                $res[$i]['book_id'] = $one->book_id;
                $res[$i]['product_id'] = $one->product_id;
                $res[$i]['item_id'] = $one->item_id;
                $res[$i]['sup_id'] = $one->sup_id;
                $res[$i]['shutter_count'] = $one->shutter_count;
                $res[$i]['pname'] = $one->pname;
                if (!empty($qunt))
                    $res[$i]['quantity'] = $qunt[0]->quantity;
                else
                    $res[$i]['quantity'] = '';
                $i++;
            }
        }
        return $res;
    }

    function booked_count($book_id, $client_id) {
        $count = 0;
        $res = array();
        $this->db->select('*')->from('booking_items_delivery');
        $book_delivery = $this->db->get()->result();
        foreach ($book_delivery as $one) {
            if ($one->id + $book_id == $one->book_id) {
                $qunt = $this->get_quantity($book_id, $one->product_id);
                if (!empty($qunt))
                    $count += $qunt[0]->quantity;
            }
        }
        return $count;
    }

    function get_record($table, $col_name, $value, $client_id, $product_supID = null) {
        $this->db->where($col_name, $value);
        $this->db->where('client_id', $client_id);
        if ($product_supID != null)
            $this->db->where('sup_product_id', $product_supID);
        return $this->db->get($table)->row();
    }

    function get_quantity($book_id, $product_id) {
        $this->db->select('quantity')->from('booking_items');
        $this->db->where('book_id', $book_id);
        $this->db->where('product_id', $product_id);
        return $this->db->get()->result();
    }

    function get_bookID() {
        $this->db->select('id')->from('booking');
        $this->db->where('client_id', cid());
        $this->db->where('status', 1);
        $this->db->or_where('status', 2);
        $this->db->where('canceled !=', 1);
        $this->db->where('deleted !=', 1);
        $this->db->order_by('id', "desc");
        $this->db->limit(1);
        return $this->db->get()->row('id');
    }

    function update_transaction($is_paid, $client_id, $book_id) {
        $this->db->select('transaction_id');
        $this->db->from('booking');
        $this->db->where('id', $book_id);
        $trans_id = $this->db->get()->row('transaction_id');
        if (!empty($trans_id)) {
            $this->db->where('id', $trans_id);
            $this->db->update('transaction', ['is_paid' => $is_paid]);
        }
    }

    function get_sup_delivery_tmp($deliver_id, $product_supID, $book_id) {
        $this->db->where('delivery_id', $deliver_id);
        $this->db->where('book_id', $book_id);
        $this->db->where('product_sup_id', $product_supID);
        //$this->db->where('deleted !=', 1);
        return $this->db->get('sup_items_temp')->row();
    }

    function update_sup_delivery($table, $deliver_id, $product_supID, $book_id) {
        $this->db->where('delivery_id', $deliver_id);
        $this->db->where('book_id', $book_id);
        $this->db->where('product_sup_id', $product_supID);
        $this->db->update($table, ['sup_id' => null]);
    }

    function get_missing_acc() {
        $this->db->select("booking_sup_items_return.*, booking.client_id, clients.n_" . lc() . " as cname, product_sup_items.serial_number, products_sup.n_" . lc() . " as pname");
        $this->db->from('booking_sup_items_return');
        $this->db->join("booking", "booking.id = booking_sup_items_return.book_id");
        $this->db->join("clients", "clients.id = booking.client_id");
        $this->db->join("products_sup", "products_sup.id = booking_sup_items_return.product_sup_id");
        $this->db->join("product_sup_items", "product_sup_items.id = booking_sup_items_return.sup_id");
        $this->db->where('booking_sup_items_return.missing', 1);
        $this->db->where('booking.deleted !=', 1);
        return $this->db->get()->result();
    }

    function get_damaged_acc() {
        $this->db->select("booking_sup_items_return.*, booking.client_id, clients.n_" . lc() . " as cname, product_sup_items.serial_number, products_sup.n_" . lc() . " as pname");
        $this->db->from('booking_sup_items_return');
        $this->db->join("booking", "booking.id = booking_sup_items_return.book_id");
        $this->db->join("clients", "clients.id = booking.client_id");
        $this->db->join("products_sup", "products_sup.id = booking_sup_items_return.product_sup_id");
        $this->db->join("product_sup_items", "product_sup_items.id = booking_sup_items_return.sup_id");
        $this->db->where('booking_sup_items_return.damaged', 1);
        $this->db->where('booking.deleted !=', 1);
        return $this->db->get()->result();
    }

}
