<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Brands_model extends CI_Model {

    private $idf = NULL;
    private $sh = NULL;

    function __construct() {
        parent::__construct();
        $this->idf = get_sys_idf(7);
        $this->sh = 'br';
    }

    function get_data($dt = []) {
        $count = isset($dt['cn']) && $dt['cn'] ? TRUE : FALSE;
        $this->db->select($count ? "COUNT($this->sh.id) as count" : $this->sh . ".*");
        $this->db->from($this->idf . ' ' . $this->sh);
        $this->db->where($this->sh . ".deleted !=", 1);
        $query = $this->db->get();
        return $count ? $query->row() : $query->result();
    }

    function get_info($id = 0) {
        $this->db->select($this->sh . ".*");
        $this->db->from($this->idf . ' ' . $this->sh);
        $this->db->where($this->sh . ".deleted !=", 1);
        $this->db->where($this->sh . ".id", $id);
        return $this->db->get()->row();
    }
    function check_documents_brand($id) {
        $this->db->where("car_brand", $id);
        $this->db->or_where("sub_brand_model", $id);
        $query = $this->db->get("documents");
        if ($query->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }
}