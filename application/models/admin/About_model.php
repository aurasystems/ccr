<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class About_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function update_content($data) {
        $this->db->where('type', 1);
        $this->db->update('pages', $data);
    }

    function get_content() {
        $this->db->where('type', 1);
        return $this->db->get('pages')->row();
    }

}
