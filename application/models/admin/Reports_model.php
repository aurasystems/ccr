<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_failed_msgs($from = NULL, $to = NULL) {
        $lc = lc();
        $select = [
            ["fm.client_id"],
            ["fm.phone"],
            ["fm.msg"],
            ["fm.timestamp"],
            ["clients.n_$lc as client"]
        ];
        $filter = [
            ['Date(fm.timestamp) >=', $from],
            ['Date(fm.timestamp) <=', $to]
        ];
        $join = [
            ["clients", "clients.id = fm.client_id", "left"]
        ];
        return ['select' => $select, 'filter' => $filter, 'join' => $join];
    }

    function get_transfered_equips($from = NULL, $to = NULL) {
        $lc = lc();
        $select = [
            ["trns.*"],
            ["products.n_$lc as product"],
            ["product_items.serial_number"],
            ["l1.n_$lc as b1name"],
            ["l2.n_$lc as b2name"],
            ["users.n_$lc as uname"]
        ];
        $filter = [
            ['Date(trns.timestamp) >=', $from],
            ['Date(trns.timestamp) <=', $to],
            ['product_items.sold !=', 1]
        ];
        $join = [
            ["products", "products.id = trns.prod_id", "left"],
            ["product_items", "product_items.id = trns.item_id", "left"],
            ["locations l1", "l1.id = trns.from_branch", "left"],
            ["locations l2", "l2.id = trns.to_branch", "left"],
            ["users", "users.id = trns.tr_by", "left"]
        ];
        return ['select' => $select, 'filter' => $filter, 'join' => $join];
    }

    function get_unique_customers($from = NULL, $to = NULL) {
        $lc = lc();
        $select = [
            ["DISTINCT(booking.client_id)"],
            ["clients.n_$lc as cname"]
        ];
        $filter = [
            ['Date(booking.timestamp) >=', $from],
            ['Date(booking.timestamp) <=', $to]
        ];
        $join = [
            ["clients", "clients.id = booking.client_id", "left"],
            ["booking_items_delivery", "booking.id = booking_items_delivery.book_id", "inner"]
        ];
        return ['select' => $select, 'filter' => $filter, 'join' => $join];
    }

    function get_sold_equip() {
        $lc = lc();
        $select = [
            ["product_items.*"],
            ["products.n_$lc as product"],
            ["locations.n_$lc as bname"]
        ];
        $filter = [
            ['product_items.sold', 1]
        ];
        $join = [
            ["products", "products.id = product_items.product_id", "left"],
            ["locations", "locations.id = product_items.location_id", "left"]
        ];
        return ['select' => $select, 'filter' => $filter, 'join' => $join];
    }

    function get_damaged_equip() {
        $lc = lc();
        $select = [
            ["damaged_items.*"],
            ["clients.n_$lc as client"],
            ["product_items.serial_number as item"]
        ];
        $filter = [
            ['product_items.damaged', 1]
        ];
        $join = [
            ["clients", "clients.id = damaged_items.client_id", "left"],
            ["product_items", "product_items.id = damaged_items.item_id", "left"]
        ];
        return ['select' => $select, 'filter' => $filter, 'join' => $join];
    }

    function get_missing_items($data) {
        $this->db->select("booking_items_return.*, booking.client_id, booking.b_from, booking.b_to, clients.n_" . lc() . " as cname, product_items.serial_number, products.n_" . lc() . " as pname");
        $this->db->from('booking_items_return');
        $this->db->join("booking", "booking.id = booking_items_return.book_id");
        $this->db->join("clients", "clients.id = booking.client_id");
        $this->db->join("product_items", "product_items.id = booking_items_return.item_id");
        $this->db->join("products", "products.id = booking_items_return.product_id");
        $this->db->where('booking_items_return.missing', 1);
        $this->db->where('product_items.sold !=', 1);
        $this->db->where('booking.deleted !=', 1);
        if (!empty($data['date_from']) && !empty($data['date_to'])) {
            $this->db->where('DATE(booking_items_return.timestamp) >=', $data['date_from']);
            $this->db->where('DATE(booking_items_return.timestamp) <=', $data['date_to']);
        }
        return $this->db->get()->result();
    }

    function get_num_of_trans($from = NULL, $to = NULL) {
        $lc = lc();
        $select = [
            ["count(booking.id) as count"]
        ];
        $filter = [
            ['Date(timestamp) >=', $from],
            ['Date(timestamp) <=', $to],
            ['deleted !=', 1]
        ];
        $join = [];
        return ['select' => $select, 'filter' => $filter, 'join' => $join];
    }

    function get_most_selling_items($from = NULL, $to = NULL) {
        $this->db->select("count(booking_items_delivery.item_id) as count, SUM(product_items.cost_price) as revenue, product_items.serial_number");
        $this->db->from('booking_items_delivery');
        $this->db->join("booking", "booking.id = booking_items_delivery.book_id");
        $this->db->join("product_items", "product_items.id = booking_items_delivery.item_id");
        $this->db->where('booking.deleted !=', 1);
        $this->db->where('DATE(booking.timestamp) >=', $from);
        $this->db->where('DATE(booking.timestamp) <=', $to);
        $this->db->group_by('booking_items_delivery.item_id');
        return $this->db->get()->row();
    }

}
