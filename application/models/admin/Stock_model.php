<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Stock_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_data() {
        $this->db->select('*')->from('locations')->where('deleted !=', 1);
        $res = $this->db->get()->result();
        return !empty($res) ? $res : null;
    }

    function get_products($location_id) {
        $this->db->select('products.n_' . lc() . ',products.id, count(product_items.product_id) as NUM')
                ->from('products')->join('product_items', 'products.id = product_items.product_id')->where('products.deleted !=', 1)
                ->where('products.location_id', $location_id)->where('product_items.sold !=', 1)->group_by('product_items.product_id');
        $res = $this->db->get()->result();

        return $res;
    }

    function get_items($table, $location_id, $product_id) {
        $this->db->select('*')->from($table)->where('location_id', $location_id)->where('product_id', $product_id)->where('sold !=', 1);
        $res = $this->db->get()->result();

        return $res;
    }

    function get_locations($table, $id, $loc_id) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('id !=', $loc_id);
        $this->db->where("deleted !=", 1);
        return $this->db->get()->result();
    }

    function get_location_products($location_id = NULL) {
        $ids_arr = [];
        $result = [];
        $this->db->select('product_items.*');
        $this->db->from('product_items');
        $this->db->where('location_id', $location_id);
        $this->db->where("deleted !=", 1);
        $this->db->where("sold !=", 1);
        $products = $this->db->get()->result();
        if (!empty($products)) {
            foreach ($products as $one) {
                if (!in_array($one->product_id, $ids_arr)) {
                    $ids_arr[] = $one->product_id;
                }
            }
        }
        for ($i = 0; $i < count($ids_arr); $i++) {
            $product = $this->get_product($ids_arr[$i]);
            if (!empty($product)) {
                if (!in_array($ids_arr[$i], $result)) {
                    $result[] = $product;
                }
            }
        }
        return $result;
    }

    function get_product($product_id = NULL) {
        $this->db->select("products.id, products.n_" . lc() . " as pname, products.img as pimg");
        $this->db->from('products');
        $this->db->where('id', $product_id);
        $this->db->where('deleted !=', 1);
        return $this->db->get()->row();
    }

}
