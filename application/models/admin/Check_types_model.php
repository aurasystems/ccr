<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Check_types_model extends CI_Model {

    private $idf = NULL;
    private $sh = NULL;

    function __construct() {
        parent::__construct();
        $this->idf = get_sys_idf(3);
        $this->sh = 'u';
    }

    function get_record_name($table, $id) {
        $this->db->select('n_en');
        $this->db->from($table);
        $this->db->where('id', $id);
        $this->db->where('deleted !=', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->first_row()->n_en;
        }
    }

    function delete_items($typeID, $table) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where("type_id", $typeID);
        $result = $this->db->get()->row();
        if ($result) {
            $data = array(
                'deleted' => '1',
            );
            $this->db->where("type_id", $typeID);
            return $this->db->update($table, $data);
        } else {
            return false;
        }
    }

    function get_first_id($table, $typeID) {
        $this->db->select('id');
        $this->db->from($table);
        $this->db->where("type_id", $typeID);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->first_row()->id;
        }
    }

    function get_check_types() {
        $this->db->select("check_types.id,"
                . "check_types.n_" . lc() . " as ctname,"
                . "check_types.run_every,"
                . "check_types.unit,"
                . "check_types.last_run");
        $this->db->from('check_types');
        $this->db->where('deleted !=', 1);
        return $this->db->get()->result();
    }

    function get_check_type_items($type_id) {
        $this->db->select("check_items.id,"
                . "check_items.n_" . lc() . " as ctiname,"
                . "check_items.type_id");
        $this->db->from('check_items');
        $this->db->where('type_id', $type_id);
        $this->db->where('deleted !=', 1);
        return $this->db->get()->result();
    }
    function get_products_wiz_check_type($type_id) {
        $this->db->select("products.id,"
                . "products.n_" . lc() . " as pname,"
                . "products.code,"
                . "products.check_type");
        $this->db->from('products');
        $this->db->where('check_type', $type_id);
        $this->db->where('deleted !=', 1);
        return $this->db->get()->result();
    }
    function get_product_items($product_id) {
        $this->db->select("*");
        $this->db->from('product_items');
        $this->db->where('product_id', $product_id);
        $this->db->where('deleted !=', 1);
        $this->db->where('sold !=', 1);
        return $this->db->get()->result();
    }

}
