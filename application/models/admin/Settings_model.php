<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function update_settings($data) {
        $settings = $this->db->get('settings')->row();
        if ($settings) {
            $this->db->where('id', 1);
            $this->db->update('settings', $data);
        } else {
            $this->db->insert('settings', $data);
        }
    }

    function get_settings() {
        return $this->db->get('settings')->row();
    }

    function get_option_group_items($op_id) {
        $this->db->select('options_groups_items.*');
        $this->db->from('options_groups_items');
        $this->db->join("options_groups", "options_groups.id = options_groups_items.op_group_id", 'left');
        $this->db->where("options_groups.deleted !=", 1);
        $this->db->where('options_groups_items.op_group_id', $op_id);
        return $this->db->get()->result();
    }

    function empty_op_gp_items($op_id) {
        $this->db->where('op_group_id', $op_id);
        $this->db->delete('options_groups_items');
    }

    function get_op_gp_item($op_group_id, $value) {
        $this->db->select('options_groups_items.*');
        $this->db->from('options_groups_items');
        $this->db->where('op_group_id', $op_group_id);
        $this->db->where('value', $value);
        return $this->db->get()->row();
    }

    function get_options_groups() {
        $this->db->select("options_groups.*");
        $this->db->from('options_groups');
        $this->db->where("deleted !=", 1);
        $query = $this->db->get();
        return $query->result();
    }

    function get_op($id = 0) {
        $this->db->select("options_groups.*");
        $this->db->from('options_groups');
        $this->db->where("deleted !=", 1);
        $this->db->where("id", $id);
        return $this->db->get()->row();
    }

}
