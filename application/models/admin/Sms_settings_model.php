<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sms_settings_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function update_settings($data) {
        $get_settings = $this->db->get('sms_settings')->row();
        if ($get_settings) {
            $this->db->where('id', 1);
            $this->db->update('sms_settings', $data);
        } else {
            $this->db->insert('sms_settings', $data);
        }
    }

    function get_sms_settings() {
        return $this->db->get('sms_settings')->row();
    }

}
