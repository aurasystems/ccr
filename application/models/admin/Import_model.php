<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Import_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_data() {
        $this->db->select("attendance.*, users.n_" . lc() . " as uname");
        $this->db->from('attendance');
        $this->db->join("users", "users.id = attendance.user_id", 'left');
        $query = $this->db->get();
        return $query->result();
    }
    function get_user_by_att_id($attendance_id) {
        $this->db->select("users.*");
        $this->db->from('users');
        $this->db->where("attendance_id", $attendance_id);
        return $this->db->get()->row();
    }
}
