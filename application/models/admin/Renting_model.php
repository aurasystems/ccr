<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Renting_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function update_content($data) {
        $this->db->where('type', 3);
        $this->db->update('pages', $data);
    }

    function get_content() {
        $this->db->where('type', 3);
        return $this->db->get('pages')->row();
    }

}
