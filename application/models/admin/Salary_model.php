<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Salary_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_users_salaries($data) {
        $result = [];
        $branch = $data['location'];
        $year_month = $data['year_month'];
        $total_day_hours = 0;
        $branch_info = $this->getBranchInfo($branch);
        $dates_array = $this->getAvailableDates($branch, $year_month);
        if (isset($dates_array) && $dates_array) {
            for ($i = 0; $i < count($dates_array); $i++) {
                $day = date('D', strtotime($dates_array[$i]));
                $dtn = day_to_number($day);
                // calculate hours for each working day
                $day_hours = get_branch_day_hours($branch, $dtn);
                if (isset($day_hours) && $day_hours) {
                    $time1 = strtotime($day_hours->time_from);
                    $time2 = strtotime($day_hours->time_to);
                    $total_day_hours += round(abs($time2 - $time1) / 3600, 2);
                }
            }
            // get users in location
            $UsersInLocation = $this->getUsersInLocation($branch);
            if (isset($UsersInLocation) && $UsersInLocation) {
                foreach ($UsersInLocation as $row) {
                    $userHours = 0;
                    $final_salary = 0;
                    for ($i = 0; $i < count($dates_array); $i++) {
                        // get user attendance for each day
                        $checkin = $this->getUserAttendance($row->user_id, $dates_array[$i], 1);
                        $checkout = $this->getUserAttendance($row->user_id, $dates_array[$i], 2);
                        if (isset($checkin) && $checkin && isset($checkout) && $checkout) {
                            $t1 = strtotime($checkin->attend_date);
                            $t2 = strtotime($checkout->attend_date);
                            $diff = $t2 - $t1;
                            $hours = $diff / ( 60 * 60 );
                            $userHours += $hours;
                        }
                    }
                    // get user info and calculate salary
                    $user_info = $this->getUserInfo($row->user_id);
                    $basic_salary = (!empty($user_info->basic_salary)) ? $user_info->basic_salary : 0;
                    if ($userHours >= $total_day_hours) {
                        $final_salary = $basic_salary;
                    } else {
                        $final_salary = ($basic_salary * $userHours) / $total_day_hours;
                    }
                    if (!in_array($row->user_id, $result)) {
                        $dt = [];
                        $dt['u_id'] = $user_info->id;
                        $dt['u_name'] = $user_info->{'n_' . lc()};
                        $dt['b_id'] = $branch_info->id;
                        $dt['b_name'] = $branch_info->{'n_' . lc()};
                        $dt['b_hours'] = round($total_day_hours, 1);
                        $dt['u_basic_salary'] = $basic_salary;
                        $dt['user_hours'] = round($userHours, 1);
                        $dt['final_salary'] = round($final_salary, 1);
                        $dt['year_month'] = $year_month;
                        array_push($result, $dt);
                    }
                }
            }
        }
        return $result;
    }

    function getAvailableDates($branch, $year_month) {
        $dates_array = [];
        $this->db->select("locations.*");
        $this->db->where("locations.deleted !=", 1);
        $this->db->where("locations.id", $branch);
        $this->db->from("locations");
        $branch_info = $this->db->get()->row();
        if ($branch_info) {
            $from = $year_month . '-01';
            $to = date("Y-m-t", strtotime($from));
            $hours = get_branch_hours($branch);
            if ($hours) {
                $working_days = [];
                $array = createDateRangeArray($from, $to, TRUE);
                foreach ($hours as $one) {
                    if ($one->day_off == 0) {
                        array_push($working_days, number_to_day($one->day));
                    }
                }
                foreach ($array as $d) {
                    if (in_array(date('l', $d), $working_days)) {
                        array_push($dates_array, date('Y-m-d', $d));
                    }
                }
                sort($dates_array);
            }
        }
        return $dates_array;
    }

    function getUsersInLocation($branch = 0) {
        $this->db->distinct('user_id');
        $this->db->select("user_locations.user_id");
        $this->db->from("user_locations");
        $this->db->join("users", "users.id = user_locations.user_id");
        $this->db->where('user_locations.location_id', $branch);
        $this->db->where('user_locations.is_temp', 0);
        $this->db->where("users.deleted !=", 1);
        return $this->db->get()->result();
    }

    function getUserAttendance($user_id, $date, $type) {
        $this->db->select("attendance.*");
        $this->db->from("attendance");
        $this->db->join("users", "users.id = attendance.user_id");
        $this->db->where('attendance.user_id', $user_id);
        $this->db->where('attendance.type', $type);
        $this->db->where('DATE(attendance.attend_date)', $date);
        $this->db->where("users.deleted !=", 1);
        $this->db->order_by("attendance.attend_date");
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    function getUserInfo($id = 0) {
        $this->db->select("users.*, user_payroll.amount as basic_salary");
        $this->db->from("users");
        $this->db->join("user_payroll", "users.id = user_payroll.user_id", 'left');
        $this->db->where('users.id', $id);
        $this->db->where("users.deleted !=", 1);
        return $this->db->get()->row();
    }

    function getBranchInfo($id = 0) {
        $this->db->select("locations.*");
        $this->db->from("locations");
        $this->db->where('id', $id);
        $this->db->where("deleted !=", 1);
        return $this->db->get()->row();
    }

    function get_user_attendance($user_id, $branch_id, $year_month) {
        $result = [];
        $branch_info = $this->getBranchInfo($branch_id);
        $dates_array = $this->getAvailableDates($branch_id, $year_month);
        //print_r($dates_array);die;
        if (isset($dates_array) && $dates_array) {
            for ($i = 0; $i < count($dates_array); $i++) {
                $dayHours = 0;
                $checkin = $this->getUserAttendance($user_id, $dates_array[$i], 1);
                $checkout = $this->getUserAttendance($user_id, $dates_array[$i], 2);
                if (isset($checkin) && $checkin && isset($checkout) && $checkout) {
                    $t1 = strtotime($checkin->attend_date);
                    $t2 = strtotime($checkout->attend_date);
                    $diff = $t2 - $t1;
                    $dayHours = $diff / ( 60 * 60 );
                }
                $dt = [];
                $dt['day'] = $dates_array[$i];
                $dt['checkin'] = ($checkin) ? date('Y-m-d h:i a', strtotime($checkin->attend_date)) : '';
                $dt['checkout'] = ($checkout) ? date('Y-m-d h:i a', strtotime($checkout->attend_date)) : '';
                $dt['dayHours'] = round($dayHours, 1);
                array_push($result, $dt);
            }
        }
        return $result;
    }

}
