<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_model extends CI_Model {

    private $idf = NULL;
    private $sh = NULL;

    function __construct() {
        parent::__construct();
        $this->idf = get_sys_idf(1);
        $this->sh = 'u';
    }

    function get_data($dt = []) {
        $level_id = isset($dt['lv']) && $dt['lv'] ? $dt['lv'] : NULL;
        $company = isset($dt['cm']) && $dt['cm'] ? $dt['cm'] : NULL;
        $branch = isset($dt['br']) && $dt['br'] ? $dt['br'] : NULL;
        $count = isset($dt['cn']) && $dt['cn'] ? TRUE : FALSE;
        $acAll = get_p($this->idf, "a");

        $this->db->select($count ? "COUNT($this->sh.id) as count" : $this->sh . ".*, al.n_" . lc() . " as access_name, up.amount");

        $this->db->from($this->idf . ' ' . $this->sh);
        $this->db->where($this->sh . ".deleted !=", 1);

        if (!$acAll) {
            $this->db->where($this->sh . ".added_by", uid());
        } else if ($acAll == 2) {
                $this->db->join("user_companies uc", "uc.user_id = $this->sh.id");
            $this->db->where_in("uc.company_id", get_companies_access());
        } else if ($acAll == 3) {
            $this->db->join("user_locations ub", "ub.user_id = $this->sh.id");
            $this->db->where_in("ub.location_id", get_branches_access());
        }

        if ($level_id) {
            $this->db->{'where' . (is_array($level_id) ? '_in' : '')}($this->sh . ".level_id", $level_id);
        }

        if ($company) {
            $this->db->{'where' . (is_array($company) ? '_in' : '')}("uc.company_id", $company);
            if ($acAll != 2) {
                $this->db->join("user_companies uc", "uc.user_id = $this->sh.id");
            }
        }

        if ($branch) {
            $this->db->{'where' . (is_array($branch) ? '_in' : '')}("ub.location_id", $branch);
            if ($acAll != 3) {
                $this->db->join("user_locations ub", "ub.user_id = $this->sh.id");
            }
        }
        $this->db->join("access_levels al", "al.id = $this->sh.level_id");
        $this->db->join("user_payroll up", "up.user_id = $this->sh.id");
        $query = $this->db->get();
        return $count ? $query->row() : $query->result();
    }

    function get_info($id = 0) {
        $acAll = get_p($this->idf, "a");
        $this->db->select($this->sh . ".*, al.n_" . lc() . " as access_name, up.amount");
        $this->db->from($this->idf . ' ' . $this->sh);
        $this->db->where($this->sh . ".deleted !=", 1);

        $this->db->where($this->sh . ".id", $id);

        if (!$acAll) {
            $this->db->where($this->sh . ".added_by", uid());
        } else if ($acAll == 2) {
            $this->db->join("user_companies uc", "uc.user_id = $this->sh.id");
            $this->db->where_in("uc.company_id", get_companies_access());
        } else if ($acAll == 3) {
            $this->db->join("user_locations ub", "ub.user_id = $this->sh.id");
            $this->db->where_in("ub.branch_id", get_branches_access());
        }

        $this->db->join("access_levels al", "al.id = $this->sh.level_id");
        $this->db->join("user_payroll up", "up.user_id = $this->sh.id");
        return $this->db->get()->row();
    }

    function get_user_companies($id = 0) {
        return $this->db->where("user_id", $id)->get('user_companies')->result();
    }

    function get_user_branches($id = 0) {
        return $this->db->where("user_id", $id)->get('user_locations')->result();
    }

    function update_user($id = 0, $dt = []) {
        $this->db->where('id', $id)->update($this->idf, $dt);
        if ($this->db->affected_rows() > 0) {
            if (uid() == $id) {
                $new_data = [
                    'user_first_name' => $dt['first_name'],
                    'user_last_name' => $dt['last_name'],
                    'user_display_name' => $dt['display_name']
                ];
                $this->session->set_userdata($new_data);
            }
        }
    }

    function delete_user_companies($id) {
        $this->db->where('user_id', $id)->delete('user_companies');
    }

    function delete_user_branches($id) {
        $this->db->where('user_id', $id)->delete('user_locations');
    }

    function do_system_user_delete($id) {
        $this->db->where('id', $id)->delete($this->idf);
    }
    function get_user_payroll($id = 0) {
        return $this->db->where("user_id", $id)->get('user_payroll')->row();
    }
}
