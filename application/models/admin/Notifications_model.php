<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notifications_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_system_operators() {
        $this->db->from("users");
        $this->db->where_in("users.ntf", [1, 3]);
        $this->db->where("users.deleted !=", 1);
        return $this->db->get()->result();
    }

    function get_customer_operators($customer_id) {
        $this->db->from("users");
        $this->db->join("user_customers", "user_customers.user_id = users.id", "left");
        $this->db->where("users.deleted !=", 1);
        $this->db->group_start();

        $this->db->group_start();
        $this->db->where("user_customers.customer_id", $customer_id);
        $this->db->where("users.ntf", 2);
        $this->db->group_end();

        $this->db->or_group_start();
        $this->db->where("users.ntf", 3);
        $this->db->group_end();

        $this->db->group_end();
        return $this->db->get()->result();
    }

    function insert_notification($dt = []) {
        $data['user_id'] = isset($dt['u']) ? $dt['u'] : 0;
        $data['end_user_id'] = isset($dt['eu']) ? $dt['eu'] : 0;
        $data['type'] = isset($dt['t']) ? $dt['t'] : 0;
        $data['related_id'] = isset($dt['r']) ? $dt['r'] : 0;
        $data['time'] = time();
        $this->db->insert('notifications', $data);
        return $this->db->insert_id();
    }

    function insert_bulk_notification($dt = []) {
        $euser = isset($dt['eu']) && $dt['eu'] ? TRUE : FALSE;
        $user = isset($dt['u']) && $dt['u'] ? TRUE : FALSE;
        $ids = isset($dt['ids']) && $dt['ids'] ? $dt['ids'] : [];
        $type = isset($dt['t']) ? $dt['t'] : 0;
        $related_id = isset($dt['r']) ? $dt['r'] : 0;
        $time = time();
        $insert = [];
        foreach ($ids as $id) {
            $data = [
                'user_id' => $user ? $id : 0,
                'end_user_id' => $euser ? $id : 0,
                'type' => $type,
                'related_id' => $related_id,
                'time' => $time
            ];
            array_push($insert, $data);
        }
        if ($insert) {
            $this->db->insert_batch('notifications', $insert);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function set_seen($ids = 0) {
        if (is_numeric($ids)) {
            $this->db->where('id', $ids);
        } else if (is_array($ids) && $ids) {
            $this->db->where_in('id', $ids);
        }
        $this->db->update('notifications', ['seen' => 1]);
    }

    function notified_before($dt = []) {
        $this->db->select('count(id) as count');
        if (isset($dt['u']))
            $this->db->where('user_id', $dt['u']);
        if (isset($dt['eu']))
            $this->db->where('end_user_id', $dt['eu']);
        if (isset($dt['t']))
            $this->db->where('type', $dt['t']);
        if (isset($dt['r']))
            $this->db->where('related_id', $dt['r']);
        if (isset($dt['us']) && $dt['us'])
            $this->db->where('seen', 0);
        if (isset($dt['s']) && $dt['s'])
            $this->db->where('seen', 1);
        if (isset($dt['tm']))
            $this->db->where('time >=', $dt['tm']);
        $notfications = $this->db->get('notifications')->row();
        return $notfications->count > 0 ? $notfications->count : FALSE;
    }

    function get_notifications($dt = [], $format = FALSE, $set_seen = FALSE) {
        if (isset($dt['u']))
            $this->db->where('user_id', $dt['u']);
        if (isset($dt['eu']))
            $this->db->where('end_user_id', $dt['eu']);
        if (isset($dt['t']))
            $this->db->where('type', $dt['t']);
        if (isset($dt['r']))
            $this->db->where('related_id', $dt['r']);
        if (isset($dt['us']) && $dt['us'])
            $this->db->where('seen', 0);
        if (isset($dt['s']) && $dt['s'])
            $this->db->where('seen', 1);
        if (isset($dt['tm']))
            $this->db->where('time >=', $dt['tm']);
        $this->db->order_by('time', 'desc');
        $notfications = $this->db->get('notifications')->result();
        return $format ? $this->format_notifications($notfications, $set_seen) : $notfications;
    }

    function format_notifications($notfications = [], $set_seen = FALSE) {
        if ($notfications) {
            $ntf_array = [];
            $seen_array = [];
            foreach ($notfications as $ntf) {
                // If seen notifcation seen , Add ids to array
                if ($set_seen) {
                    $seen_array[] = $ntf->id;
                }

                $text = '';
                $url = 'javascript:void(0);';

                switch ($ntf->type) {
                    case 31:
                        $text = lang('payment_passed_due');
                        $url = base_url('Customer_payments');
                        break;
                    case 32:
                        $service_info = $this->Global_model->get_data_by_id('services', $ntf->related_id);
                        if ($service_info) {
                            $text = lang('new_service_added') . ' (' . $service_info->{'name_' . lc()} . ')';
                            $url = base_url('Services');
                        }
                        break;
                    case 33:
                        $branch_info = $this->Global_model->get_data_by_id('locations', $ntf->related_id);
                        if ($branch_info) {
                            $text = lang('new_branch_added') . ' (' . $branch_info->{'n_' . lc()} . ')';
                            $url = base_url('admin/Locations');
                        }
                        break;
                    case 21:
                        $text = lang('billing_cycle_end_ntf');
                        $url = base_url('Customer_payments');
                        break;
                    case 22:
                        $this->load->model('Payment_model');
                        $check_payment = $this->Payment_model->check_customer_payment(['id' => $ntf->related_id]);
                        if ($check_payment) {
                            $text = lang('please_pay_ntf') . $check_payment->amount_due . lang('to_avoid_disable_srv');
                            $url = base_url('Customer_payments');
                        }
                        break;
                    case 23:
                        $text = lang('customer_disabled_ntf');
                        break;
                    case 24:
                        $text = lang('customer_disabled_ntf');
                        $branch_info = $this->Global_model->get_data_by_id('locations', $ntf->related_id);
                        if ($branch_info) {
                            $text = lang('branch_disabled_ntf') . ' (' . $branch_info->{'n_' . lc()} . ')';
                            $url = base_url('admin/Locations/index/disabled');
                        }
                        break;
                    case 40:
                        // Equipment Test
                        $this->load->model('admin/Equipment_test_model', 'equipment');
                        $test_info = $this->equipment->get_equipment_test($ntf->related_id);
                        if ($test_info) {
                            $text = lang('equipment_test') . ' (' . $test_info->ctname . ')';
                            $url = base_url('admin/Equipment_test/get_tests/'.$ntf->related_id);
                        }
                        break;
                }
                if ($text) {
                    $ntfObj = new stdClass();
                    $ntfObj->id = $ntf->id;
                    $ntfObj->text = $text;
                    $ntfObj->url = $url;
                    $ntfObj->time = format_unix($ntf->time);
                    $ntfObj->seen = $set_seen ? 1 : $ntf->seen;
                    $ntf_array[] = $ntfObj;
                }
            }
            // If set notifcation seen , Set notfication seen from array
            if ($set_seen) {
                $this->set_seen($seen_array);
            }
            return $ntf_array;
        }
        return FALSE;
    }

}
