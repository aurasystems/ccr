<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clients_model extends CI_Model {

    private $idf = NULL;
    private $sh = NULL;

    function __construct() {
        parent::__construct();
        $this->idf = get_sys_idf(3);
        $this->sh = 'u';
    }

    function get_waiting_approve($table) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where("deleted !=", 1);
        $this->db->where("status", 0);
        return $this->db->get()->result();
    }

    function get_approved($table) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where("deleted !=", 1);
        $this->db->where("status", 1);
        return $this->db->get()->result();
    }

    function get_unapproved_clients($table) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where("deleted !=", 1);
        $this->db->where("status", 2);
        return $this->db->get()->result();
    }

    function get_data($table) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where("deleted !=", 1);
        return $this->db->get()->result();
    }

    function get_email($id, $table) {
        $this->db->select('email');
        $this->db->from($table);
        $this->db->where("id", $id);
        return $this->db->get()->row()->email;
    }

    function delete_account($id) {
        $this->db->select('email');
        $this->db->from('clients');
        $this->db->where("id", $id);
        $result = $this->db->get()->row();
        if ($result) {
            $data = array(
                'deleted' => '1',
                'email' => null,
                'deleted_email' => $result->email
            );
            $this->db->where('id', $id);
            return $this->db->update('clients', $data);
        } else {
            return false;
        }
    }

    function approve_client($id) {
        $data = array(
            'status' => '1',
            'decline_reason' => '0',
            'added_by' => uid()
        );
        $this->db->where('id', $id);
        return $this->db->update('clients', $data);
    }

    function unapprove_client($id) {
        $data = array(
            'status' => '2');
        $this->db->where('id', $id);
        return $this->db->update('clients', $data);
    }

    function get_record($id, $table) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where("deleted !=", 1);
        $this->db->where("id =", $id);
        $res = $this->db->get()->result();
        return !empty($res) ? $res : null;
    }

    function delete_record($id, $table) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where("id", $id);
        $result = $this->db->get()->row();
        if ($result) {
            $data = array(
                'deleted' => '1',
            );
            $this->db->where('id', $id);
            return $this->db->update($table, $data);
        } else {
            return false;
        }
    }

    function get_bookings($id, $count) {
        $this->db->select($count ? "COUNT(booking.id) as count" : "booking.*, clients.n_" . lc() . " as cname, locations.n_" . lc() . " as lname");
        $this->db->from('booking');
        $this->db->group_start();
        $this->db->group_start();
        $this->db->where("booking.status", 1);
        $this->db->group_end();
        $this->db->or_group_start();
        $this->db->where("booking.status", 2);
        $this->db->group_end();
        $this->db->group_end();
        $this->db->where("booking.deleted !=", 1);
        $this->db->join("clients", "clients.id = booking.client_id", 'left');
        $this->db->join("locations", "locations.id = booking.branch", 'left');
        $this->db->where("booking.client_id", $id);
        $this->db->where("booking.deleted !=", 1);
        return $count ? $this->db->get()->row() : $this->db->get()->result();
    }

    function most_rented($client_id = 0) {
        $this->db->select("COUNT(booking_items.id) as max, booking_items.product_id, products.n_" . lc() . " as pname");
        $this->db->from('booking_items');
        $this->db->join("booking", "booking.id = booking_items.book_id", 'left');
        $this->db->join("products", "products.id = booking_items.product_id", 'left');
        $this->db->where("booking.client_id", $client_id);
        $this->db->where("booking.deleted !=", 1);
        $this->db->group_by("booking_items.product_id");
        $this->db->order_by("max desc");
        $result = $this->db->get()->row();
        return $result;
    }

    function old_bookings($id) {
        $arr = [];
        $today = date('Y-m-d');
        $this->db->select("booking.*, clients.n_" . lc() . " as cname, locations.n_" . lc() . " as lname");
        $this->db->from('booking');
        $this->db->join("clients", "clients.id = booking.client_id", 'left');
        $this->db->join("locations", "locations.id = booking.branch", 'left');
        $this->db->group_start();
        $this->db->group_start();
        $this->db->where("booking.status", 1);
        $this->db->group_end();
        $this->db->or_group_start();
        $this->db->where("booking.status", 2);
        $this->db->group_end();
        $this->db->group_end();
        $this->db->where("DATE(booking.b_from) <", $today);
        $this->db->where("booking.client_id", $id);
        $this->db->where("booking.deleted !=", 1);
        $data = $this->db->get()->result();
        foreach ($data as $row) {
            $this->db->select("booking_items_delivery.*");
            $this->db->from('booking_items_delivery');
            $this->db->where("book_id", $row->id);
            $count = $this->db->count_all_results();
            if ($count > 0) {
                $arr[] = $row;
            }
        }
        return $arr;
    }

    function today_bookings($id) {
        $today = date('Y-m-d');
        $this->db->select("booking.*, clients.n_" . lc() . " as cname, locations.n_" . lc() . " as lname");
        $this->db->from('booking');
        $this->db->group_start();
        $this->db->group_start();
        $this->db->where("booking.status", 1);
        $this->db->group_end();
        $this->db->or_group_start();
        $this->db->where("booking.status", 2);
        $this->db->group_end();
        $this->db->group_end();
        $this->db->where("booking.deleted !=", 1);
        $this->db->join("clients", "clients.id = booking.client_id", 'left');
        $this->db->join("locations", "locations.id = booking.branch", 'left');
        //$this->db->join("booking_items_delivery", "booking.id = booking_items_delivery.book_id");
        $this->db->where("DATE(booking.b_from)", $today);
        $this->db->where("booking.client_id", $id);
        $this->db->where("booking.deleted !=", 1);
        return $this->db->get()->result();
    }

    function coming_bookings($id) {
        $today = date('Y-m-d');
        $this->db->select("booking.*, clients.n_" . lc() . " as cname, locations.n_" . lc() . " as lname");
        $this->db->from('booking');
        $this->db->group_start();
        $this->db->group_start();
        $this->db->where("booking.status", 1);
        $this->db->group_end();
        $this->db->or_group_start();
        $this->db->where("booking.status", 2);
        $this->db->group_end();
        $this->db->group_end();
        $this->db->where("booking.deleted !=", 1);
        $this->db->join("clients", "clients.id = booking.client_id", 'left');
        $this->db->join("locations", "locations.id = booking.branch", 'left');
        //$this->db->join("booking_items_delivery", "booking.id = booking_items_delivery.book_id");
        $this->db->where("DATE(booking.b_from) >", $today);
        $this->db->where("booking.client_id", $id);
        $this->db->where("booking.deleted !=", 1);
        return $this->db->get()->result();
    }

    function no_shows_bookings($id) {
        $arr = [];
        $today = date('Y-m-d');
        $this->db->select("booking.*, clients.n_" . lc() . " as cname, locations.n_" . lc() . " as lname");
        $this->db->from('booking');
        $this->db->join("clients", "clients.id = booking.client_id", 'left');
        $this->db->join("locations", "locations.id = booking.branch", 'left');
        $this->db->group_start();
        $this->db->group_start();
        $this->db->where("booking.status", 1);
        $this->db->group_end();
        $this->db->or_group_start();
        $this->db->where("booking.status", 2);
        $this->db->group_end();
        $this->db->group_end();
        $this->db->where("DATE(booking.b_from) <", $today);
        $this->db->where("booking.client_id", $id);
        $this->db->where("booking.deleted !=", 1);
        $data = $this->db->get()->result();
        foreach ($data as $row) {
            $this->db->select("booking_items_delivery.*");
            $this->db->from('booking_items_delivery');
            $this->db->where("book_id", $row->id);
            $count = $this->db->count_all_results();
            if ($count == 0) {
                $arr[] = $row;
            }
        }
        return $arr;
    }

    function booking_items($booking_id) {
        $this->db->select("booking_items.*, products.n_" . lc() . " as pname, products.img as pimg, locations.n_" . lc() . " as lname");
        $this->db->from('booking_items');
        $this->db->join("products", "products.id = booking_items.product_id", 'left');
        $this->db->join("booking", "booking.id = booking_items.book_id", 'left');
        $this->db->join("locations", "locations.id = booking.branch", 'left');
        $this->db->where('booking_items.book_id', $booking_id);
        $query = $this->db->get();
        return $query->result();
    }

    function get_booking_row($booking_id, $client_id) {
        $this->db->select("*");
        $this->db->from('booking');
        $this->db->where("id", $booking_id);
        $this->db->where("client_id", $client_id);
        $this->db->where("deleted !=", 1);
        $result = $this->db->get()->row();
        return $result;
    }

    function items_with_maintenance($client_id) {
        $this->db->select("damaged_items.*, clients.n_" . lc() . " as cname, product_items.serial_number, maintenance.m_from, maintenance.m_to");
        $this->db->from('damaged_items');
        $this->db->join("maintenance", "maintenance.id = damaged_items.maint_id");
        $this->db->join("clients", "clients.id = damaged_items.client_id");
        $this->db->join("product_items", "product_items.id = damaged_items.item_id");
        $this->db->where('damaged_items.client_id', $client_id);
        $this->db->where('product_items.sold !=', 1);
        $query = $this->db->get();
        return $query->result();
    }

    function get_penalties($client_id) {
        $this->db->select("wallet.id, wallet.booking_id, wallet.item_id, wallet.due_to, wallet.timestamp, sum(wallet.amount) as sum, product_items.serial_number");
        $this->db->from('wallet');
        $this->db->join("booking", "booking.id = wallet.booking_id");
        $this->db->join("product_items", "product_items.id = wallet.item_id");
        $this->db->where('wallet.client_id', $client_id);
        $this->db->where('product_items.sold !=', 1);
        $this->db->group_by("wallet.client_id");
        $this->db->group_by("wallet.booking_id");
        $this->db->group_by("wallet.item_id");
        $this->db->order_by("wallet.id");
        return $this->db->get()->result();
    }

    function get_penalties_history($client_id, $booking_id, $item_id) {
        $this->db->select("wallet.id, wallet.booking_id, wallet.item_id, wallet.due_to, wallet.amount, wallet.timestamp, product_items.serial_number");
        $this->db->from('wallet');
        $this->db->join("booking", "booking.id = wallet.booking_id");
        $this->db->join("product_items", "product_items.id = wallet.item_id");
        $this->db->where('wallet.client_id', $client_id);
        $this->db->where('wallet.booking_id', $booking_id);
        $this->db->where('wallet.item_id', $item_id);
        $this->db->where('product_items.sold !=', 1);
        $this->db->order_by("wallet.id");
        return $this->db->get()->result();
    }

    function get_total_penalties($client_id, $booking_id, $item_id) {
        $this->db->select_sum("amount");
        $this->db->from('wallet');
        $this->db->where('client_id', $client_id);
        $this->db->where('booking_id', $booking_id);
        $this->db->where('item_id', $item_id);
        return $this->db->get()->row()->amount;
    }

    function get_last_booking($id) {
        $this->db->select("booking.timestamp");
        $this->db->from('booking');
        $this->db->where("client_id", $id);
        $this->db->where("deleted !=", 1);
        $this->db->order_by("timestamp", "desc");
        $this->db->limit('1');
        return $this->db->get()->row()->timestamp;
    }

    function get_related_surveys($client_id) {
        $this->db->distinct('survey_id');
        $this->db->select("survey_client_b_t.id, survey_client_b_t.survey_id, surveys.title");
        $this->db->from('survey_client_b_t');
        $this->db->join("surveys", "surveys.id = survey_client_b_t.survey_id");
        $this->db->where('survey_client_b_t.client_id', $client_id);
        $this->db->group_by('survey_client_b_t.survey_id');
        return $this->db->get()->result();
    }

    function get_survey_history($survey_id, $client_id) {
        $this->db->distinct('timestamp');
        $this->db->select("survey_client_b_t.id, survey_client_b_t.timestamp");
        $this->db->from('survey_client_b_t');
        $this->db->join("surveys", "surveys.id = survey_client_b_t.survey_id");
        $this->db->where('survey_client_b_t.client_id', $client_id);
        $this->db->where('survey_client_b_t.survey_id', $survey_id);
        return $this->db->get()->result();
    }

    function get_client_survey_b_t_submitted($id, $survey_id, $client_id) {
        $this->db->select('survey_client_b_t.*');
        $this->db->from('survey_client_b_t');
        $this->db->where('survey_id', $survey_id);
        $this->db->where('client_id', $client_id);
        $this->db->where('id', $id);
        //$this->db->where('submitted', 1);
        return $this->db->get()->row();
    }

    function get_client_answers($id, $survey_id, $client_id) {
        $this->db->select('survey_answers.*');
        $this->db->select('survey_questions.question');
        $this->db->from('survey_answers');
        $this->db->join('survey_questions', 'survey_answers.question_id = survey_questions.id');
        $this->db->join('surveys', 'survey_answers.survey_id = surveys.id');
        $this->db->where('survey_answers.client_id', $client_id);
        $this->db->where('survey_answers.c_b_t', $id);
        $this->db->where('survey_questions.survey_id', $survey_id);
        $this->db->where("surveys.deleted !=", 1);
        return $this->db->get()->result();
    }

    function get_survey_basic_by_client($id) {
        $this->db->select('survey_client_b_t.*');
        $this->db->from('survey_client_b_t');
        $this->db->where('id', $id);
        //$this->db->where('client_id', $client_id);
        //$this->db->where('survey_id', $survey_id);
        //$this->db->where('submitted', 0);
        return $this->db->get()->row();
    }

    function get_survey_answers_by_client($id, $survey_id, $client_id) {
        $this->db->select('survey_answers.*');
        $this->db->select('survey_questions.survey_id');
        $this->db->select('survey_questions.question');
        $this->db->select('survey_questions.answer_type');
        $this->db->from('survey_answers');
        $this->db->join('survey_questions', 'survey_answers.question_id = survey_questions.id');
        $this->db->where('survey_answers.c_b_t', $id);
        $this->db->where('survey_answers.survey_id', $survey_id);
        $this->db->where('survey_answers.client_id', $client_id);
        //$this->db->where('survey_answers.submitted', 0);
        return $this->db->get()->result();
    }

    function get_survey_question_by_client($id, $survey_id, $client_id, $question_id) {
        $this->db->select('survey_answers.*');
        $this->db->from('survey_answers');
        $this->db->where('survey_answers.c_b_t', $id);
        $this->db->where('survey_answers.survey_id', $survey_id);
        $this->db->where('survey_answers.client_id', $client_id);
        $this->db->where('survey_answers.question_id', $question_id);
        return $this->db->get()->row();
    }
    function get_approved_limit_data() {
        $this->db->select("clients.id, clients.n_" . lc() . " as cname, clients.email, clients.phone");
        $this->db->from('clients');
        $this->db->where("deleted !=", 1);
        $this->db->where("status", 1);
        return $this->db->get()->result();
    }
    function transactions($client_id) {
        $this->db->select("transaction.*");
        $this->db->from('transaction');
        $this->db->join("booking", "booking.transaction_id = transaction.id", 'left');
        $this->db->where('booking.client_id', $client_id);
        $this->db->where("booking.deleted !=", 1);
        $query = $this->db->get();
        return $query->result();
    }
}
