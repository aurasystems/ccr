<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Equipment_test_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_equipment_tests() {
        $this->db->select("equipment_test.*, check_types.n_" . lc() . " as ctname, check_types.run_every, check_types.unit, check_types.last_run");
        $this->db->from('equipment_test');
        $this->db->join("check_types", "check_types.id = equipment_test.chk_type_id", 'left');
        $this->db->where("equipment_test.completed", 0);
        $this->db->where("check_types.deleted !=", 1);
        $query = $this->db->get();
        return $query->result();
    }

    function get_completed_tests() {
        $this->db->select("equipment_test.*, check_types.n_" . lc() . " as ctname, check_types.run_every, check_types.unit, check_types.last_run");
        $this->db->from('equipment_test');
        $this->db->join("check_types", "check_types.id = equipment_test.chk_type_id", 'left');
        $this->db->where("equipment_test.completed", 1);
        $this->db->where("check_types.deleted !=", 1);
        $query = $this->db->get();
        return $query->result();
    }

    function get_product_items($test_id) {
        $this->db->distinct();
        $this->db->select('equipment_test_checks.p_item_id as p_item_id');
        $this->db->select('product_items.serial_number as p_serial');
        $this->db->from('equipment_test_checks');
        $this->db->join('product_items', 'product_items.id = equipment_test_checks.p_item_id');
        $this->db->where('equipment_test_checks.test_id', $test_id);
        $this->db->where("product_items.deleted !=", 1);
        $this->db->where("product_items.sold !=", 1);
        return $this->db->get()->result();
    }

    function get_check_type_items($test_id, $pitem_id) {
        $this->db->select("equipment_test_checks.id, equipment_test_checks.chk_t_i_id, equipment_test_checks.is_checked, check_items.n_" . lc() . " as ctitem, users.n_" . lc() . " as uname");
        $this->db->from('equipment_test_checks');
        $this->db->join('check_items', 'check_items.id = equipment_test_checks.chk_t_i_id');
        $this->db->join("users", "users.id = equipment_test_checks.checked_by", 'left');
        $this->db->where('equipment_test_checks.test_id', $test_id);
        $this->db->where('equipment_test_checks.p_item_id', $pitem_id);
        $this->db->where("check_items.deleted !=", 1);
        return $this->db->get()->result();
    }

    function get_equipment_test($id) {
        $this->db->select("equipment_test.*, check_types.n_" . lc() . " as ctname, check_types.run_every, check_types.unit, check_types.last_run");
        $this->db->from('equipment_test');
        $this->db->join("check_types", "check_types.id = equipment_test.chk_type_id", 'left');
        $this->db->where("equipment_test.id", $id);
        $this->db->where("check_types.deleted !=", 1);
        return $this->db->get()->row();
    }

    function check_type_items_checked($test_id) {
        $this->db->select("equipment_test_checks.id, equipment_test_checks.chk_t_i_id, equipment_test_checks.is_checked, check_items.n_" . lc() . " as ctitem");
        $this->db->from('equipment_test_checks');
        $this->db->join('check_items', 'check_items.id = equipment_test_checks.chk_t_i_id');
        $this->db->where('equipment_test_checks.test_id', $test_id);
        $this->db->where('equipment_test_checks.is_checked !=', 1);
        $this->db->where("check_items.deleted !=", 1);
        return $this->db->count_all_results();
    }

}
