<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Access_levels_model extends CI_Model {

    private $idf = NULL;
    private $sh = NULL;

    function __construct() {
        parent::__construct();
        $this->idf = get_sys_idf(2);
        $this->sh = 'al';
    }

    function get_data($dt = []) {
        $count = isset($dt['cn']) && $dt['cn'] ? TRUE : FALSE;
        $type = isset($dt['t']) && $dt['t'] ? $dt['t'] : NULL;

        $this->db->select($count ? "COUNT($this->sh.id) as count" : $this->sh . ".*");

        if ($type) {
            $this->db->where($this->sh . ".private" . ($type == 'prv' ? ' !=' : ''), 1);
        }

        $this->db->from($this->idf . ' ' . $this->sh);
        $query = $this->db->get();
        return $count ? $query->row() : $query->result();
    }

    function get_info($id = 0) {

        $this->db->select($this->sh . ".*");
        $this->db->where($this->sh . ".id", $id);

        $this->db->from($this->idf . ' ' . $this->sh);
        $query = $this->db->get();
        return $query->row();
    }

    function check_level_users($id) {
        $this->db->where("level_id", $id)->where("deleted !=", 1)->limit(2);
        $query = $this->db->get("users");
        return $query->num_rows() > 0 ? TRUE : FALSE;
    }

    function get_level_users($id) {
        return $this->db->where("level_id", $id)->where("deleted !=", 1)
                        ->get("users")->result();
    }

    function get_level_permissions($id) {
        return $this->db->select("lf.*, $this->sh.n_" . lc() . " as level_name")
                        ->where("lf.level_id", $id)
                        ->join($this->idf . ' ' . $this->sh, "lf.level_id = $this->sh.id")
                        ->get("level_functions lf")->row();
    }

    function get_fields($table) {
        return $this->db->list_fields($table);
    }

    function update_permissions($id, $dt) {
        $this->db->where("level_id", $id)
                ->update("level_functions", ['permissions' => serialize($dt)]);
    }

    function delete_functions($id) {
        $this->db->where("level_id", $id)
                ->delete("level_functions");
    }

    function isFieldExist($table, $col, $field) {
        $this->db->where($col, $field);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0)
            return true;
        else
            return false;
    }

}
