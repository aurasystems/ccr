<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Maintenance_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_next_maint()
    {
        $today = date('Y-m-d');
        $this->db->select('*')->from('maintenance');
        $this->db->where('b_from <=', $today);
        return $this->db->get()->result();
    }    

    function get_scan_data($scanned_id)
    {

        $this->db->select('id');
        $this->db->from('products');
        $this->db->where('n_en', $scanned_id);
        $this->db->limit(1);
        $product_id = $this->db->get()->row('id');
        if(!empty($product_id))//for technical/studio
        {
            $this->db->select('maintenance.*, p.n_en as pname')->from('maintenance');
            $this->db->where('maintenance.product_id', $product_id);
            $this->db->join("products p", "p.id = maintenance.product_id");
            $res = $this->db->get()->result();
        }
        else if(empty($product_id))//for item
        {
            $this->db->select('id');
            $this->db->from('product_items');
            $this->db->where('serial_number', $scanned_id);
            $item_id = $this->db->get()->row('id');
            if(!empty($item_id))
            {
                $this->db->select('maintenance.*, i.n_en as pname')->from('maintenance');
                $this->db->where('maintenance.item_id', $item_id);
                $this->db->where('i.sold !=', 1);
                $this->db->join("product_items i", "i.id = maintenance.item_id");
                $res = $this->db->get()->result();
            }
            else if(empty($item_id))//for sup item
            {
                $this->db->select('id');
                $this->db->from('product_sup_items');
                $this->db->where('serial_number', $scanned_id);
                $sup_id = $this->db->get()->row('id');
                if(!empty($sup_id))
                {
                    $this->db->select('maintenance.*, i.n_en as pname')->from('maintenance');
                    $this->db->where('maintenance.sup_id', $sup_id);
                    $this->db->join("product_sup_items i", "i.id = maintenance.sup_id");
                    $res = $this->db->get()->result();
                }
            }
        }
       
        return $res;
    }

    function get_data($id)
    {
        $this->db->select('m_to')->from('maintenance')->where('id', $id);
        return $this->db->get()->row('m_to');
    }

    function check_booked_studioTech($product_id, $m_from, $m_to)
    {
        $days = createDateRangeArray($m_from, $m_to);
        foreach($days as $day)
        {
            $booked       = $this->booked_products($product_id, $day);
            $ava_items    = $this->available_products($product_id);
            $booked_count = count($booked);
            if($booked_count > $ava_items)
            {
                $unavailble_count = $booked_count - $ava_items;
                for($i=1; $i<=$unavailble_count; $i++)
                {
                    $this->db->where('id', $booked[$booked_count-$i]->book_id);
                    $this->db->update('booking', array('canceled' => 1));
                }

            }
        }
    }

    function check_booked_items($item_id, $m_from, $m_to)
    {
        $days = createDateRangeArray($m_from, $m_to);
        
            $this->db->select('product_id')->from('product_items');
            $this->db->where('id', $item_id);
            $this->db->where('sold !=', 1);
            $product_id = $this->db->get()->row('product_id');
            foreach($days as $day)
            {
                $booked       = $this->booked_products($product_id, $day);
                $ava_items    = $this->available_items($product_id);
                $booked_count = count($booked);
                if($booked_count > $ava_items)
                {
                    $unavailble_count = $booked_count - $ava_items;
                    for($i=1; $i<=$unavailble_count; $i++)
                    {
                        $this->db->where('id', $booked[$booked_count-$i]->book_id);
                        $this->db->update('booking', array('canceled' => 1));
                    }
                 
                }
            }

       
    }

   

    function available_products($product_id)
    {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('id', $product_id);
        $this->db->where('under_maintenance != ', 1);
        $this->db->where('deleted !=', 1);
        return $this->db->count_all_results();
    }

    function available_items($product_id)
    {
        $this->db->select('*');
        $this->db->from('product_items');
        $this->db->where('product_id', $product_id);
        $this->db->where('damaged != ', 1);
        $this->db->where('exist', 1);
        $this->db->where('deleted !=', 1);
        $this->db->where('sold !=', 1);
        return $this->db->count_all_results();
    }

    function booked_products($product_id, $day)
    {
        $this->db->select('booking_items.*');
        $this->db->from('booking_items');
        $this->db->group_start();
        $this->db->where('b.status', '1');
        $this->db->or_where('b.status', '2');
        $this->db->group_end();
        $this->db->where('booking_items.product_id', $product_id);
        $this->db->where('booking_items.b_from <=', $day);
        $this->db->where('booking_items.b_to >=', $day);
        $this->db->join('booking b', 'b.id = booking_items.book_id');
        return $this->db->get()->result();
    }

     function createDateRangeArray($strDateFrom = NULL, $strDateTo = NULL, $unix = FALSE, $stamp = 'Y-m-d') {
        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.
        // could test validity of dates here but I'm already doing
        // that in the main script
    
        $aryRange = [];
    
        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));
    
        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, $unix ? $iDateFrom : date($stamp, $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                array_push($aryRange, $unix ? $iDateFrom : date($stamp, $iDateFrom));
            }
        }
        return $aryRange;
    } 
    
    function insert_maint($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    function check_client_penalty($item_id, $maint_id, $is_sup=false, $is_stud=false)
    {
        
        if($is_sup==true)
        {
        $this->db->select("*");
        $this->db->from("booking_sup_items_return");
        $this->db->where('sup_id', $item_id);
        }
        else if($is_stud == true)
        {
        $this->db->select("*");
        $this->db->from("booking_items_return");
        $this->db->where('product_id', $item_id);
        }
        else if($is_sup==false && $is_stud == false)
        {
        $this->db->select("*");
        $this->db->from("booking_items_return");
        $this->db->where('item_id', $item_id);
        }
        $this->db->where('damaged', 1);
        $this->db->limit(1);
        $this->db->order_by('id',"DESC");
        $query = $this->db->get();
        $result = $query->row();

        if(!empty($result))
        {
            $this->db->select('client_id')->from('booking');
            $this->db->where('id', $result->book_id);
            $client_id = $this->db->get()->row('client_id');
            $data = array(
                'maint_id'      => $maint_id,
                'client_id'     => $client_id,
                'cost'          => 0, //too be updated later
            );
            
            if($is_stud==true)
            $data['product_id'] = $item_id;
            else  if($is_sup==true)
            {
            $data['sup_id'] = $item_id;
            $this->db->select('product_id')->from('product_sup_items')->where('id', $item_id);
            $prod_supID = $this->db->get()->row('product_id');
            $this->db->select('product_id')->from('products_sup')->where('id', $prod_supID);
            $product_id = $this->db->get()->row('product_id');
            $data['product_id'] = $product_id;
            }
            else if($is_stud==false && $is_sup == false)
            {
            $data['item_id'] = $item_id;
            $this->db->select('product_id')->from('product_items')->where('id', $item_id)->where('sold !=', 1);
            $product_id = $this->db->get()->row('product_id');
            $data['product_id'] = $product_id;
            }
          
            $this->db->insert('damaged_items', $data);
        }
    }

    function update_cost($table, $maint_id, $cost)
    {
        $this->db->where('maint_id', $maint_id);
        $this->db->update($table, ['cost' => $cost]);
    }

    function is_maint_related_client($maint_id)
    {
        $this->db->select('*')->from('maintenance');
        $this->db->where('id', $maint_id);
        $maint_data = $this->db->get()->row();
        $this->db->select("*");
        $this->db->from("booking_items_return");
        $this->db->where('sup_id', $maint_data->sup_id);
        $this->db->where('product_id', $maint_data->product_id);
        $this->db->where('item_id', $maint_data->item_id);
        $this->db->where('damaged', 1);
        $this->db->limit(1);
        $this->db->order_by('id',"DESC");
       $count = $this->db->count_all_results();
       //print_r($count);
        if($count>0)
        return true;
        else
        return false;
    }

    function update_transaction($cost, $maint_id)
    {
        
            $data = [
            'type_id'       => 3,
            'cost'          => $cost,
            ];
            $this->db->select('transaction_id')->from('maintenance')->where('id', $maint_id);
            $trans_id = $this->db->get()->row('transaction_id');
            if($trans_id == null)
            {
                $this->db->insert('transaction', $data);
                $last_id = $this->db->insert_id();
                $this->db->where('id', $maint_id);
                $this->db->update('maintenance', ['transaction_id' => $last_id]);
            }
            else if($trans_id != null)
            {
                $this->db->where('id', $trans_id);
                $this->db->update('transaction', $data);
            }
        
    }

    function get_delivery_temp_time($today, $table)
    {
        $this->db->select('*');
        $this->db->from($table);
        return $this->db->get()->result();
    }
 

}
