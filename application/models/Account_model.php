<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_booking($client_id)
    {
        $result = array();
        $this->db->select('*');
        $this->db->from('booking');
        $this->db->where('client_id', $client_id);
        $this->db->where('deleted !=', 1);
        $book_id = $this->db->get()->result();
        $book_ids = array_column($book_id, "id");
        if(!empty($book_ids))
        {
        $this->db->select('booking_items.*, p.*, b.status, b.canceled');
        $this->db->from('booking_items');
        $this->db->where_in('book_id',$book_ids);
        $this->db->join('products p', 'p.id = booking_items.product_id');
        $this->db->join('booking b', 'b.id = booking_items.book_id');
        $result = $this->db->get()->result();
        }
        return $result;

    }

}
