<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shop_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function inc_dec_quantity($cart_id, $count = null, $dec = false) {
        $this->db->where('id', $cart_id);
        if ($dec) {
            $this->db->set('quantity', 'quantity-1', FALSE);
            $this->db->set('total_cost', 'total_cost/2', false);
        } else {
            $this->db->set('quantity', 'quantity+1', FALSE);
            $this->db->set('total_cost', 'total_cost*' . $count, false);
        }
        $this->db->update('cart');
    }

    function is_record_exist($id, $table, $col) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where("deleted !=", 1);
        $this->db->where($col, $id);
        $res = $this->db->get()->result();

        return !empty($res) ? true : false;
    }

    function isFieldExist($table, $col, $field, $uid) {
        $this->db->where($col, $field);
        $this->db->where('client_id', $uid);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0)
            return true;
        else
            return false;
    }

    function get_data($id, $table, $col, $is_col_deleted = true) {
        $this->db->select('*');
        $this->db->from($table);
        if ($is_col_deleted == true)
            $this->db->where("deleted !=", 1);
        $this->db->where($col, $id);
        $res = $this->db->get()->result();

        return !empty($res) ? $res : null;
    }

    function get_record($id, $table, $col, $is_col_deleted = true) {
        $this->db->select('*');
        $this->db->from($table);
        if ($is_col_deleted == true)
            $this->db->where("deleted !=", 1);
        $this->db->where($col, $id);
        $res = $this->db->get()->row();

        return !empty($res) ? $res : null;
    }

    function check_items_ava($products_id) {
        foreach ($products_id as $id) { // to check how many items left in product_items 
            $item_count = $this->db->select('id')->from('product_items')->where('product_id', $id)->count_all_results();
            $booked_count = $this->db->select('id')->from('booking_items')->where('product_id', $id)->count_all_results();


            if ($booked_count >= $item_count)
                return false;
        }
        return true;
    }

    function get_cart_root() {
        $this->db->select('*');
        $this->db->from('cart');
        $this->db->where('parent', 0);
        $this->db->where('client_id', cid());
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    /* function is_available($cid, $from_date) {
      $ret = true;
      $this->db->select('product_id');
      $this->db->from('cart');
      $this->db->where("client_id", $cid);
      $res = $this->db->get()->result();

      $products_id = array_column($res, "product_id");


      if ($this->check_items_ava($products_id))
      return true;
      $this->db->select('book_id')->from('booking_items')->where_in('product_id', $products_id);
      $res = $this->db->get()->result();
      $arr = array_column($res, "book_id");

      if (count($arr) > 0) {
      $this->db->select('b_to')->from('booking')->where_in('id', $arr)->where('deleted !=', 1);
      $dates = $this->db->get()->result();
      foreach ($dates as $date) {
      if ($from_date <= $date->b_to) {
      return false;
      }
      }
      return true;
      } else {
      return true;
      }
      } */

    /*  function cart_quant_per_prod($product_id)
      {
      $this->db->select('quantity')->from('cart')->where('client_id',cid())->where('product_id',$product_id);
      $res = $this->db->get()->result();

      $count=0;
      if(count($res)>0)
      {
      $count = $res[0]->quantity;
      }
      return $count;
      }
     */

    function get_prodcts($products_id) {
        $this->db->select('n_en')->from('products')->where_in('id', $products_id)->where('deleted !=', 1);
        $res = $this->db->get()->result();
        //print_r($res);
        return $res;
    }

    function get_total_price() {
        $cart_data = get_cart();
        $total = 0;
        if (!empty($cart_data)) {
            foreach ($cart_data as $one) {
                $this->db->select('*')->from('products')->where('id', $one->product_id);
                $this->db->distinct();
                $product = $this->db->get()->row();
                $total += $one->quantity * $product->rent_price;
            }
        }
        return $total;
    }

    function check_booking($b_from = null, $b_to = null) {//to update the booking table with the latest date
        $new_b_to = $b_to;
        $new_b_from = $b_from;
        $book_data = $this->db->select('*')->from('booking')->where('client_id', cid())->where('deleted !=', 1)->get()->row();
        $client_data = $this->db->select('*')->from('clients')->where('id', cid())->where('deleted !=', 1)->get()->row();
        if (!empty($book_data)) {
            $book_items_data = $this->db->select('*')->from('booking_items')->where('book_id', $book_data->id)->get()->result();
            if (count($book_items_data) > 0) {
                foreach ($book_items_data as $b_data) {
                    if ($b_data->b_from < $b_from) {
                        $new_b_from = $b_data->b_from;
                        $b_from = $new_b_from;
                    }
                    if ($b_data->b_to > $b_to) {
                        $new_b_to = $b_data->b_to;
                        $b_to = $new_b_to;
                    }
                }
            } else {
                $data = array(
                    'client_id' => cid(),
                    'b_from' => $b_from,
                    'b_to' => $b_to,
                    'cash_limit' => $client_data->cash_limit,
                    'credit_limit' => $client_data->credit_limit,
                    'status' => $client_data->status,
                    'decline_reason' => $client_data->decline_reason,
                );
                $this->db->where('client_id', cid());
                $this->db->update('booking', $data);
            }
        } else {
            $data = array(
                'client_id' => cid(),
                'b_from' => $b_from,
                'b_to' => $b_to,
                'cash_limit' => $client_data->cash_limit,
                'credit_limit' => $client_data->credit_limit,
                'status' => $client_data->status,
                'decline_reason' => $client_data->decline_reason,
            );
            $this->db->where('client_id', cid());
            $this->db->insert('booking', $data);
        }

        $data = array(
            'client_id' => cid(),
            'b_from' => $new_b_from,
            'b_to' => $new_b_to,
            'cash_limit' => $client_data->cash_limit,
            'credit_limit' => $client_data->credit_limit,
            'status' => $client_data->status,
            'decline_reason' => $client_data->decline_reason,
        );
        $this->db->where('client_id', cid());
        $this->db->update('booking', $data);

        $book_id = $this->db->select('id')->from('booking')->where('client_id', cid())->get()->row()->id;
        return $book_id;
    }

    function get_cart_booking_data($booking_id = null) {
        $result = array();
        if ($booking_id != null) {
            $this->db->select('product_id');
            $this->db->from('cart');
            $this->db->where('client_id', cid());
            $res = $this->db->get()->result();
            $arr = array_column($res, "product_id");
            if (!empty($arr)) {
                $this->db->select('*')->from('booking_items')->where_in('product_id', $arr)->where('book_id', $booking_id);
                $result = $this->db->get()->result();
            }
        }
        return $result;
    }

    function is_cash_exceeded($cash_limit) {
        $parent_id = $this->db->select('id')->from('cart')->where('client_id', cid())->where('parent', 0)->get()->row()->id;
        $this->db->select('SUM(total_cost) as sum')->from('cart')->where('parent', $parent_id);
        $total = $this->db->get()->row()->sum;
        if ($total > $cash_limit) {
            return true;
        }
        return false;
    }

    function is_credit_exceeded($credit_limit) {
        $parent_id = $this->db->select('id')->from('cart')->where('client_id', cid())->where('parent', 0)->get()->row()->id;
        $this->db->select('SUM(total_cost) as sum')->from('cart')->where('parent', $parent_id);
        $total = $this->db->get()->row()->sum;
        if ($total > $credit_limit) {
            return true;
        }
        return false;
    }

    function add_book_pending_approve() {
        $this->db->select('*');
        $this->db->from('booking');
        $this->db->where('client_id', cid());
        $this->db->where('status', 0);
        $this->db->where('deleted !=', 1);
        $this->db->where('canceled !=', 1);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $res = $this->db->get()->row();
        if (empty($res))
            return false; // to create new booking
        else
            return true;
    }

    function add_book_auto_approve() {
        $this->db->where('client_id', cid());
        $this->db->update('booking', array('status' => 2));
    }

    function get_items_data($book_id, $product_id) {
        $res = '';
        $this->db->select('*')->from('booking_items')->where('book_id', $book_id)->where('product_id', $product_id);
        $res = $this->db->get()->row();
        return $res;
    }

    function get_record_not_deleted($table, $id = null, $col = null) {
        if ($id == null) {
            $this->db->select('*');
            $this->db->from($table);
        } else {
            $this->db->select('*');
            $this->db->from($table);
            $this->db->where($col, $id);
        }
        $this->db->where("deleted !=", 1);
        return $this->db->get()->row();
    }

    function update_booking_branch($branch) {
        $this->db->where('client_id', cid());
        $this->db->update('booking', array('branch' => $branch));
    }

    function get_cart_data() {
        $this->db->select('*');
        $this->db->from('cart');
        $this->db->where('client_id', cid());
        $this->db->where('parent', 0);
        $this->db->limit(1);
        $cart_root = $this->db->get()->row();
        $this->db->select('cart.*, p.rent_price, p.rent_term')->from('cart');
        $this->db->where('cart.parent', $cart_root->id);
        $this->db->join('products as p', 'p.id = cart.product_id');
        // $this->db->join('booking as b', 'b.client_id = cart.client_id');
        return $this->db->get()->result();
    }

    /* function insert_into_prep($table_prep, $data) {
      $this->db->insert($table_prep, $data);
      /*  $prep_id = $this->db->insert_id();
      $data['prep_id'] = $prep_id;
      $this->db->insert($table_deliver, $data);
      $deliver_id = $this->db->insert_id();
      $deliver_bookID = $deliver_id + $data['book_id'];
      $this->db->where('id', $deliver_id);
      $this->db->update($table_deliver, array('book_id' => $deliver_bookID));
      //add in delivery sup if there is supp items for product
      $this->db->select('*');
      $this->db->from('products_sup');
      $this->db->where('product_id', $data['product_id']);
      $this->db->where('deleted !=', 1);
      $products_sup = $this->db->get()->result();
      if(!empty($products_sup))
      {
      foreach($products_sup as $one)
      {
      $dt = [
      'delivery_id' => $deliver_id,
      'book_id' => $data['book_id'],
      'product_sup_id' => $one->id,
      ];
      $this->db->insert('booking_sup_items_delivery', $dt);
      }
      }
      } */

    function update_transaction($total, $trans_id, $book_id, $is_credit = false) {

        $data = [
            'type_id' => 4,
            'cost' => $total,
            'is_paid' => $is_credit == true ? '1' : '2', //1 for paid, 2 for unpaid
        ];
        if ($trans_id == '') {
            $this->db->insert('transaction', $data);
            $last_id = $this->db->insert_id();
            $this->db->where('id', $book_id);
            $this->db->update('booking', ['transaction_id' => $last_id]);
        } else if ($trans_id != '') {
            $this->db->where('id', $trans_id);
            $this->db->update('transaction', $data);
        }
    }

    function coupoun_vaidation($voucher_id) {
        $today = date('Y-m-d');
        $data = array();
        $this->db->select('*');
        $this->db->from('vouchers');
        $this->db->where('voucher_number', $voucher_id);
        $voucher_data = $this->db->get()->row();
        if (!empty($voucher_data)) {
            if ($today >= $voucher_data->v_from && $today <= $voucher_data->v_to) {
                if ($voucher_data->client_id != 0) {
                    if ($voucher_data->client_id == cid()) {//for specific client
                        $this->db->where('client_id', cid());
                        $this->db->where('parent', 0);
                        $this->db->update('cart', ['voucher_id' => $voucher_data->id]);
                        $data['msg'] = lang("coupon_available_for_now");
                        $data['id'] = $voucher_data->id;
                        $data['flag'] = true;
                    } else {
                        $data['msg'] = lang("not_assigned_to_you");
                        $data['flag'] = false;
                    }
                } else if ($voucher_data->client_id == 0) {//if for all clients
                    if ($voucher_data->uses_remain > 0) {
                        $this->db->where('client_id', cid());
                        $this->db->where('parent', 0);
                        $this->db->update('cart', ['voucher_id' => $voucher_data->id]);
                        $data['msg'] = lang("coupon_available_for_now");
                        $data['id'] = $voucher_data->id;
                        $data['flag'] = true;
                    } else { //# of uses is 0
                        $data['msg'] = lang("this_voucher_ended");
                        $data['flag'] = false;
                    }
                }
            } else if ($voucher_data->v_to < $today) {
                $data['msg'] = lang("this_voucher_ended");
                $data['flag'] = false;
            } else if ($voucher_data->v_from > $today) {
                $data['msg'] = lang("this_voucher_not_started");
                $data['flag'] = false;
            }
        } else {
            $data['msg'] = lang("this_voucher_not_exist");
            $data['flag'] = false;
        }
        return $data;
    }

    function update_voucher_validation($voucher_id) {
        $today = date('Y-m-d');
        $data = array();
        $this->db->select('*');
        $this->db->from('vouchers');
        $this->db->where('voucher_number', $voucher_id);
        $voucher_data = $this->db->get()->row();
        if (!empty($voucher_data)) {
            if ($today >= $voucher_data->v_from && $today <= $voucher_data->v_to && $voucher_data->uses_remain > 0) {
                if ($voucher_data->client_id != 0) {
                    if ($voucher_data->client_id == cid()) {//for specific client
                        $data['msg'] = lang("coupon_available_for_now");
                        $data['amount'] = $voucher_data->amount;
                        $data['type'] = $voucher_data->type;
                        $data['id'] = $voucher_data->id;
                        $data['flag'] = true;
                    } else {
                        $data['msg'] = lang("not_assigned_to_you");
                        $data['flag'] = false;
                    }
                } else if ($voucher_data->client_id == 0) {//if for all clients
                    if ($voucher_data->uses_remain > 0) {
                        $this->db->where('id', $voucher_data->id);
                        $this->db->update('vouchers', ['uses_remain' => $voucher_data->uses_remain - 1]);
                        $data['msg'] = lang("coupon_available_for_now");
                        $data['amount'] = $voucher_data->amount;
                        $data['type'] = $voucher_data->type;
                        $data['id'] = $voucher_data->id;
                        $data['flag'] = true;
                    } else { //# of uses is 0
                        $data['msg'] = lang("this_voucher_ended");
                        $data['flag'] = false;
                    }
                }
            } else if ($voucher_data->v_to < $today) {
                $data['msg'] = lang("this_voucher_ended");
                $data['flag'] = false;
            } else if ($voucher_data->v_from > $today) {
                $data['msg'] = lang("this_voucher_not_started");
                $data['flag'] = false;
            }
        } else {
            $data['msg'] = lang("this_voucher_not_exist");
            $data['flag'] = false;
        }
        return $data;
    }

    function update_cart($voucher_id) {
        $this->db->where('client_id', cid());
        $this->db->where('parent', 0);
        $this->db->update('cart', ['voucher_id' => $voucher_id]);
    }

//later used for checkout in booking
    function update_booking($voucher_id, $total, $discount, $book_id) {
        $this->db->where('id', $book_id);
        $this->db->update('booking', ['total' => $total, 'discount' => $discount]);
        $this->db->select('transaction_id');
        $this->db->from('booking');
        $this->db->where('id', $book_id);
        $trans_id = $this->db->get()->row('transaction_id');
        if (!empty($trans_id)) {
            $this->db->where('id', $trans_id);
            $this->db->where('deleted !=', 1);
            $this->db->update('transaction', ['cost' => $total, 'voucher_id' => $voucher_id, 'voucher_amount' => $discount]);
        }
    }

    function update_rating($product_id, $rating) {
        $this->db->select('*');
        $this->db->from('reviews');
        $this->db->where('client_id', cid());
        $this->db->where('product_id', $product_id);
        $res = $this->db->get()->result();
        if (!empty($res)) {
            $this->db->where('client_id', cid());
            $this->db->where('product_id', $product_id);
            $this->db->update('reviews', ['rating' => $rating]);
        } else
            $this->db->insert('reviews', ['rating' => $rating, 'product_id' => $product_id, 'client_id' => cid()]);
    }

    function count_products() {
        $this->db->select("products.*");
        $this->db->from('products');
        $this->db->where("deleted !=", 1);
        return $this->db->count_all_results();
    }

    function get_products_data($limit, $start) {
        $this->db->select("products.*");
        $this->db->from('products');
        $this->db->where("deleted !=", 1);
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
    }

    function get_table_info($table, $id = 0) {
        $this->db->select("*");
        $this->db->from($table);
        $this->db->where("deleted !=", 1);
        $this->db->where("id", $id);
        return $this->db->get()->row();
    }

    function get_cart_quantity($product_id, $b_from, $b_to) {
        $quantity = 0;
        $this->db->select('*');
        $this->db->from('cart');
        $this->db->where('client_id', cid());
        $this->db->where('parent', 0);
        $this->db->limit(1);
        $cart_root = $this->db->get()->row();
        if (!empty($cart_root)) {
            $this->db->select('*');
            $this->db->from('cart');
            $this->db->where('parent', $cart_root->id);
            $this->db->where('b_from <=', $b_from);
            $this->db->where('b_to >=', $b_to);
            $this->db->where('product_id', $product_id);
            $quantity = $this->db->count_all_results();
        }
        return $quantity;
    }

    function update_cart_voucher($voucher_id) {
        $this->db->where('id', $voucher_id);
        $this->db->update('cart', ['voucher_id' => NULL]);
    }

    function check_waiting_list($cid, $product_id, $from, $to) {
        $this->db->select("*");
        $this->db->from('booking_waiting_list');
        $this->db->where("client_id", $cid);
        $this->db->where("product_id", $product_id);
        $this->db->where("b_from", $from);
        $this->db->where("b_to", $to);
        return $this->db->get()->row();
    }

}
