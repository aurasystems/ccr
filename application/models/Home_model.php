<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_featured_prod($categ_id = null) {
        $sub_cat_id = 0;
        $res = array();
        $products_id = array();

        if ($categ_id == null || $categ_id == -1) {
            $this->db->select('*')->from('products')->where('featured', 1);
            $res = $this->db->get()->result();
        } else if ($categ_id != null && $categ_id != -1) {
            $this->db->select('id')->from('categories')->where('parent', $categ_id);
            $res = $this->db->get();

            if ($res->num_rows() > 0) {
                $sub_cat_id = $res->row()->id;

                $this->db->select('product_id')->from('product_categories')->where('category_id', $sub_cat_id);
                $products_id = $this->db->get()->result();

                if (count($products_id) > 0) {
                    $arr = array_column($products_id, "product_id");
                    $this->db->select('*')->from('products')->where_in('id', $arr)->where('featured', 1);
                    $res = $this->db->get()->result();
                }
            }
        }

        return $res;
    }

    function count_featured_products() {
        $this->db->select("products.*");
        $this->db->from('products');
        $this->db->where("featured", 1);
        $this->db->where("deleted !=", 1);
        return $this->db->count_all_results();
    }

    function get_featured_products($limit, $start) {
        $this->db->select("products.*");
        $this->db->from('products');
        $this->db->where("featured", 1);
        $this->db->where("deleted !=", 1);
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
    }

}
