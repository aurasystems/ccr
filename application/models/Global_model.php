<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Global_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function global_insert($table_name, $data) {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    function global_update($table_name, $id, $data) {
        if (is_array($id)) {
            $this->db->where_in('id', $id);
        } else {
            $this->db->where('id', $id);
        }
        return $this->db->update($table_name, $data);
    }

    function global_update_by_colName($table_name, $col_name, $field, $data) {
        if (is_array($field)) {
            $this->db->where_in($col_name, $field);
        } else {
            $this->db->where($col_name, $field);
        }
        return $this->db->update($table_name, $data);
    }

    function get_data_by_id($table_name, $id) {
        $this->db->where('id', $id);
        return $this->db->get($table_name)->row();
    }

    function get_table_count($table_name) {
        $this->db->from($table_name);
        return $this->db->count_all_results();
    }

    function get_table_data($table_name) {
        return $this->db->get($table_name)->result();
    }

    function get_all($table_name) {
        return $this->db->get($table_name)->result();
    }

    function get_all_active($table_name) {
        $this->db->where('deleted !=', 1);
        return $this->db->get($table_name)->result();
    }

    function get_all_non_disabled($table_name) {
        $this->db->where('deleted !=', 1);
        $this->db->where('disabled !=', 1);
        return $this->db->get($table_name)->result();
    }

    function check_email($table_name, $email) {
        $this->db->where("email", $email);
        $get_data = $this->db->get($table_name);
        if ($get_data->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    function reset_password($table_name, $email, $new_password) {
        $this->db->where('email', $email);
        $this->db->update($table_name, ['password' => md5($new_password)]);
        $this->send_new_password($table_name, $email, $new_password);
    }

    function send_reset_link($email) {
        $msg_content = '';
        //get Client info
        $this->db->where('email', $email);
        $info = $this->db->get("users")->row();
        $token = $info->id . "_" . substr($info->password, 0, 5);
        $msg_content .= "Dear " . $info->{'n_' . lc()} . " <br>";
        $msg_content .= "You are receiving this email because you requested a password reset. Please use the following link to reset your password: <br>"
                . "<a href=" . base_url("Login/reset_my_password/$token") . ">" . base_url("Login/reset_my_password/$token") . "</a> <br>"
                . "If it wasn't you, please, ignore this email. <br>";
        // config email smtp
        $settings = $this->db->get('email_settings')->row();
        if (!empty($settings)) {
            $config = [
                'protocol' => 'smtp',
                'smtp_host' => $settings->smtp_host, //ssl://smtp.googlemail.com
                'smtp_port' => $settings->smtp_port, // 465 
                'smtp_user' => $settings->smtp_user,
                'smtp_pass' => $settings->smtp_pass,
                'mailtype' => 'html',
                'charset' => 'iso-8859-1'
            ];
            $this->load->library('email');
            $this->email->initialize($config);
            $this->email->from($settings->host_mail);
            $this->email->to($email);
            $this->email->subject('Password Reset Link');
            $this->email->message($msg_content);
            $this->email->send();
        }
    }

    function global_delete($table_name, $id) {
        $this->db->where('id', $id);
        $this->db->delete($table_name);
    }

    function isFieldExist($table, $col, $field) {
        $this->db->where($col, $field);
        $this->db->where("deleted !=", 1);
        if ($table == 'product_items') {
            $this->db->where("sold !=", 1);
        }
        $query = $this->db->get($table);
        if ($query->num_rows() > 0)
            return true;
        else
            return false;
    }

    function get_data_not_deleted($table, $id = null, $col = null) {
        if ($id == null) {
            $this->db->select('*');
            $this->db->from($table);
        } else {
            $this->db->select('*');
            $this->db->from($table);
            $this->db->where($col, $id);
        }
        $this->db->where("deleted !=", 1);
        return $this->db->get()->result();
    }

    function get_data_by_colID($table, $colID, $ID) {
        $this->db->where($colID, $ID);
        $this->db->where('deleted !=', 1);
        if ($table == 'product_items') {
            $this->db->where("sold !=", 1);
        }
        return $this->db->get($table)->result();
    }

    function get_total($table, $col = null, $id = null) {
        $this->db->select('count(*)');
        $query = $this->db->get($table);
        $this->db->where('deleted !=', 1);
        if ($col != null && $id != null)
            $this->db->where($col, $id);

        $cnt = $query->row_array();
        return $cnt['count(*)'];
    }

    function get_active_data($table) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where("deleted !=", 1);
        return $this->db->get()->result();
    }

}
