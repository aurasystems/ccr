<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Blog_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_data($category = NULL, $limit, $start) {
        $this->db->select("blog.*, blog_categories.n_" . lc() . " as category, users.n_" . lc() . " as uname");
        $this->db->from('blog');
        $this->db->join("blog_categories", "blog_categories.id = blog.category_id", 'left');
        $this->db->join("users", "users.id = blog.created_by", 'left');
        $this->db->where("blog.deleted !=", 1);
        $this->db->where("blog.status", 1);
        if (!empty($category)) {
            $this->db->where("blog.category_id", $category);
        }
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
    }

    function get_info($id) {
        $this->db->select("blog.*, blog_categories.n_" . lc() . " as category, users.n_" . lc() . " as uname");
        $this->db->from('blog');
        $this->db->join("blog_categories", "blog_categories.id = blog.category_id", 'left');
        $this->db->join("users", "users.id = blog.created_by", 'left');
        $this->db->where("blog.deleted !=", 1);
        $this->db->where("blog.status", 1);
        $this->db->where("blog.id", $id);
        return $this->db->get()->row();
    }

    function get_tags($id) {
        $this->db->select("blog_tags.*");
        $this->db->from('blog_tags');
        $this->db->join("blog", "blog.id = blog_tags.blog_id", 'left');
        $this->db->where("blog.deleted !=", 1);
        $this->db->where("blog.status", 1);
        $this->db->where("blog_tags.blog_id", $id);
        $query = $this->db->get();
        return $query->result();
    }

    function get_blog_by_tag($tag = NULL, $limit, $start) {
        $this->db->select("blog.*, blog_categories.n_" . lc() . " as category, users.n_" . lc() . " as uname");
        $this->db->from('blog');
        $this->db->join("blog_categories", "blog_categories.id = blog.category_id", 'left');
        $this->db->join("users", "users.id = blog.created_by", 'left');
        $this->db->where("blog.deleted !=", 1);
        $this->db->where("blog.status", 1);
        if (!empty($tag)) {
            $this->db->where('(blog.title LIKE \'%' . $tag . '%\' OR blog.content LIKE \'%' . $tag . '%\')', NULL, FALSE);
        }
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
    }

    function count_blog_by_tag($tag = NULL) {
        $this->db->select("blog.*, blog_categories.n_" . lc() . " as category, users.n_" . lc() . " as uname");
        $this->db->from('blog');
        $this->db->join("blog_categories", "blog_categories.id = blog.category_id", 'left');
        $this->db->join("users", "users.id = blog.created_by", 'left');
        $this->db->where("blog.deleted !=", 1);
        $this->db->where("blog.status", 1);
        if (!empty($tag)) {
            $this->db->where('(blog.title LIKE \'%' . $tag . '%\' OR blog.content LIKE \'%' . $tag . '%\')', NULL, FALSE);
        }
        return $this->db->count_all_results();
    }

    function get_categories() {
        $this->db->select("blog_categories.*");
        $this->db->from('blog_categories');
        $this->db->where("blog_categories.deleted !=", 1);
        $query = $this->db->get();
        return $query->result();
    }

    function count_blog_by_category($category = NULL) {
        $this->db->select("blog.*");
        $this->db->from('blog');
        $this->db->join("blog_categories", "blog_categories.id = blog.category_id");
        $this->db->where("blog.deleted !=", 1);
        $this->db->where("blog.status", 1);
        $this->db->where("blog.category_id", $category);
        return $this->db->count_all_results();
    }

    function get_all_tags() {
        $this->db->distinct('tag');
        $this->db->select("blog_tags.tag");
        $this->db->from('blog_tags');
        $query = $this->db->get();
        return $query->result();
    }

    function count_comments($blog_id = NULL) {
        $this->db->select("blog_comments.*");
        $this->db->from('blog_comments');
        $this->db->join("blog", "blog.id = blog_comments.blog_id");
        $this->db->where("blog_comments.deleted !=", 1);
        $this->db->where("blog.deleted !=", 1);
        $this->db->where("blog_comments.blog_id", $blog_id);
        return $this->db->count_all_results();
    }

    function get_comments($blog_id = NULL) {
        $this->db->select("blog_comments.*, clients.n_" . lc() . " as cname");
        $this->db->from('blog_comments');
        $this->db->join("blog", "blog.id = blog_comments.blog_id", 'left');
        $this->db->join("clients", "clients.id = blog_comments.cid", 'left');
        $this->db->where("blog_comments.deleted !=", 1);
        $this->db->where("blog.deleted !=", 1);
        $this->db->where("blog_comments.blog_id", $blog_id);
        $query = $this->db->get();
        return $query->result();
    }

    function count_blogs($category = NULL) {
        $this->db->select("blog.*, blog_categories.n_" . lc() . " as category, users.n_" . lc() . " as uname");
        $this->db->from('blog');
        $this->db->join("blog_categories", "blog_categories.id = blog.category_id", 'left');
        $this->db->join("users", "users.id = blog.created_by", 'left');
        $this->db->where("blog.deleted !=", 1);
        $this->db->where("blog.status", 1);
        if (!empty($category)) {
            $this->db->where("blog.category_id", $category);
        }
        return $this->db->count_all_results();
    }

}
