<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function search_products_by($search_wiz = NULL, $text) {
        $this->db->select('products.*');
        $this->db->from('products');
        $this->db->where("("
                . "n_en like '%$text%' OR "
                . "n_ar like '%$text%' OR "
                . "overview like '%$text%' OR "
                . "description like '%$text%' OR "
                . "specifications like '%$text%'"
                . ")");
        if (!empty($search_wiz)) {
            if ($search_wiz == 1) {
                $this->db->where("type", 1);
            } elseif ($search_wiz == 2) {
                $this->db->where("type", 2);
            } elseif ($search_wiz == 3) {
                $this->db->where("type", 3);
            }
        }
        $this->db->where("deleted !=", 1);
        $result = $this->db->get()->result();
        return $result;
    }

}
