<?php $settings = settings('all', 3); ?>
<footer class="site-footer">
    <div class="footer-top">
        <div class="section-padding">
            <div class="container">
                <div class="row">

                    <div class="col-md-4 col-sm-6">
                        <div class="widget widget_about_us">
                            <?php if (!empty($settings->logo)) { ?>
                                <a class="footer-logo" href="<?= base_url() ?>Home"><img style="width: 45%;" src="<?= base_url() ?>uploads/settings/<?= $settings->logo ?>" alt="Site Logo"></a>
                            <?php } ?>
                            <div class="widget-details">
                                <p class="description">

                                </p><!-- /.description -->
                                <address>
                                    <?= (!empty($settings->address)) ? $settings->address : '' ?>
                                </address>
                                <span><?= (!empty($settings->phone)) ? $settings->phone : '' ?></span>
                                <span><a href="<?= (!empty($settings->email)) ? 'mailto:' . $settings->email : '' ?>"><?= (!empty($settings)) ? $settings->email : '' ?></a></span>
                            </div><!-- /.widget-details -->
                        </div><!-- /.widget -->
                    </div>

                    <div class="col-md-2 col-sm-6">
                        <div class="widget widget_useful_links">
                            <h4 class="widget-title">Useful links</h4>
                            <div class="widget-details">
                                <?php if (!empty($settings) && $settings->show_about) { ?>
                                    <span><i class="ti-control-record"></i><a href="<?= base_url('About') ?>"><?= lang('about_us') ?></a></span>
                                <?php } ?>
                                <?php if (!empty($settings) && $settings->show_privacy) { ?>
                                    <span><i class="ti-control-record"></i><a href="<?= base_url('Privacy') ?>"><?= lang('privacy_policy') ?></a></span>
                                <?php } ?>
                                <?php if (!empty($settings) && $settings->show_renting) { ?>
                                    <span><i class="ti-control-record"></i><a href="<?= base_url('Renting') ?>"><?= lang('how_to_rent') ?></a></span>
                                <?php } ?>
                            </div><!-- /.widget-details -->
                        </div><!-- /.widget -->
                    </div>

                    <div class="col-md-2 col-sm-6">
                        <div class="widget widget_shop_links">
                            <h4 class="widget-title">Shops</h4>
                            <div class="widget-details">
                                <span><i class="ti-control-record"></i><a href="<?= base_url() ?>Shop"><?= lang('shop') ?></a></span>
                                <span><i class="ti-control-record"></i><a href="<?= base_url() ?>Shop/cart"><?= lang('Shop_cart') ?></a></span>
                                <span><i class="ti-control-record"></i><a href="<?= base_url() ?>Checkout"><?= lang('checkout') ?></a></span>                
                            </div><!-- /.widget-details -->
                        </div><!-- /.widget -->
                    </div>

                    <div class="col-md-4 col-sm-6 hidden">
                        <div class="widget widget_instagram_feed">
                            <h4 class="widget-title">Instagram Photos</h4>
                            <div class="widget-details">
                                <a href="#"><img src="<?= base_url() ?>images/instagram/1.png" alt="Instagram Image"></a>
                                <a href="#"><img src="<?= base_url() ?>images/instagram/2.png" alt="Instagram Image"></a>
                                <a href="#"><img src="<?= base_url() ?>images/instagram/3.png" alt="Instagram Image"></a>
                                <a href="#"><img src="<?= base_url() ?>images/instagram/4.png" alt="Instagram Image"></a>
                                <a href="#"><img src="<?= base_url() ?>images/instagram/5.png" alt="Instagram Image"></a>
                                <a href="#"><img src="<?= base_url() ?>images/instagram/6.png" alt="Instagram Image"></a>
                                <a href="#"><img src="<?= base_url() ?>images/instagram/7.png" alt="Instagram Image"></a>
                                <a href="#"><img src="<?= base_url() ?>images/instagram/8.png" alt="Instagram Image"></a>
                            </div><!-- /.widget-details -->
                        </div><!-- /.widget -->
                    </div>

                </div>
            </div>
        </div>
    </div><!-- /.footer-top -->

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 text-left">
                    <div class="copyright">
                        © <a href="http://aura-sys.com"><?= lang('aura_sys') ?></a> <?= date('Y') ?> | <?= lang('developed_by') ?> <a href="http://aura-sys.com"><?= lang('aura_sys') ?></a>
                    </div><!-- /.copyright -->
                </div>
                <div class="col-sm-5 text-right hidden">
                    <div class="payment-list">
                        <a href="#"><img src="<?= base_url() ?>images/payment/1.png" alt="Payment Logo"></a>
                        <a href="#"><img src="<?= base_url() ?>images/payment/2.png" alt="Payment Logo"></a>
                        <a href="#"><img src="<?= base_url() ?>images/payment/3.png" alt="Payment Logo"></a>
                        <a href="#"><img src="<?= base_url() ?>images/payment/4.png" alt="Payment Logo"></a>
                        <a href="#"><img src="<?= base_url() ?>images/payment/5.png" alt="Payment Logo"></a>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.footer-bottom -->
</footer>