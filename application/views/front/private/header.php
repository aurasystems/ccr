<?php
$items = get_cart();
$settings = settings('all', 3);
//$book = get_record('client_id', cid(), 'booking');
$total = get_total_count(); 
?>
<header id="masthead" class="masthead">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 top-left text-left">
                    <p class="top-contact">
                        <i class="ti-email"></i><span><a href="<?= (!empty($settings->email)) ? 'mailto:'.$settings->email : '' ?>"><?= (!empty($settings)) ? $settings->email : '' ?></a></span>
                        <i class="ti-mobile"></i><span><?= (!empty($settings->phone)) ? $settings->phone : '' ?></span>
                    </p><!-- /.top-contact -->
                </div><!-- /.top-left -->
                <div class="col-sm-7 top-right text-right">
                    <!-- Currencies -->
                    <!-- /.Currencies -->
                    <!-- Lang -->
                    <!-- /.Lang -->
                    <div class="wish-list hidden">
                        <a href="#" class="current-title">Wishlist</a>
                        <span class="count">0</span>
                        <span><i class="ti-heart"></i></span>
                    </div>
                    <div class="my-account dropdown">
                        <?php if ((cid() != "")) { ?>
                            <a href="#"><?= lang('my_account') ?><i class="ti-user"></i></a>
                            <ul class="unsorted-list">
                                <li><a href="<?= base_url('Account/profile') ?>">My Profile</a></li>
                                <li><a href="<?= base_url('Account/rent_history') ?>">Rent History</a></li>
                                <li class="hidden"><a href="#">My Wishlist</a></li>
                                <li class="hidden"><a href="#">My Cart</a></li>
                                <li><a href="<?= base_url() ?>Checkout">Checkout</a></li>
                                <li><a href="<?= base_url('Account/logout') ?>"><?= lang('logout') ?></a></li>
                            </ul>
                        <?php } else { ?>
                            <a href="#"><?= lang('register_login') ?><i class="ti-user"></i></a>
                            <ul class="unsorted-list">
                                <li><a href="<?= base_url('Login') ?>"><?= lang('login') ?></a></li>
                                <li><a href="<?= base_url('Register') ?>"><?= lang('register') ?></a></li>
                            </ul>
                        <?php } ?>
                    </div><!-- /.my-account -->
                </div><!-- /.top-right -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.header-top -->
    <div class="header-middle">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <?php if(!empty($settings->logo)) { ?>
                    <h1><a class="navbar-brand hidden-xs" href="<?= base_url() ?>Home"><img style="width: 50%;" src="<?= base_url() ?>uploads/settings/<?= $settings->logo ?>" alt="Site Logo"></a></h1>
                    <?php } ?>
                </div>
                <div class="col-sm-7">
                    <div class="top-search-form">
                        <form action="<?= base_url() ?>Search" method="post" class="menu-form">
                            <?= csrf() ?>
                            <fieldset> 
                                <select name="category" id="category">
                                    <option value="all"><?= lang('all') ?></option>
                                    <option value="1"><?= lang('studios') ?></option>
                                    <option value="2"><?= lang('technicians') ?></option>
                                    <option value="3"><?= lang('gears') ?></option>
                                </select>
                            </fieldset>
                            <input autocomplete="off" type="text" id="search_products" name="keyword" placeholder="<?= lang('search_for') ?>" class="form-control" data-search="true">
                            <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                        </form>
                        <div id="data-container" class="search_container"></div>
                    </div><!-- /.top-search-form -->
                </div>
                <div id="update_cart">
                            <div  class="col-sm-2">	
                                  <div class="shop-cart">             	
                                    <a class="cart-control" href="#" title="View your shopping cart">	
                                      <i class="ti-bag"></i>	
                                      <span class="count" id="total-count"><?=get_quantity(cid(), null, true)?></span>	
                                      <span>Cart - </span>
                                      <span class="amount" id="price_total" ><?= $total ?></span>	
                                    </a><!-- /.cart-control -->	
                                    <div class="cart-items">	
                                      <div  class="widget_shopping_cart_content">	
                                      <?php  $total =0; foreach($items as $item){ 	
                                          $product = get_product_details($item->product_id);
                                          $quant= get_quantity(cid(), $item->id);
                                           ?>	
                                        <div  class="cart-top">
                                        <div class="item media">
                                            <button onclick="delete_item('<?= $item->id ?>','<?=$product->id?>')"  class="btn" ><i class="fa fa-close"></i></button>
                                            <div class="item-thumbnail media-left">
                                                <img src="<?= base_url() ?>/uploads/thumb/<?= $product->img ?>" class="item-img img-responsive" style="width: 165px; height: auto;" alt="Item Thimbnail">
                                            </div><!-- /.item-thumbnail -->
                                            <div class="item-details media-body">
                                                <div class="rating"><input type="hidden" class="rating-tooltip-manual" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="5"/></div><!-- /.rating -->
                                                <h4  class="item-title"><a class="name" href="#"><?= $product->n_en ?></a></h4><!-- /.item-title -->
                                                <div class="item-price">
                                                    <div class="item-price">
                                                        <span class="currency">EGP&nbsp;</span>
                                                        <span class="price"><?= $product->rent_price ?></span> 
                                                        <span class="item-count" id="qunt<?=$product->id?>"><?= $quant ?></span><!-- /.item-count -->
                                                    </div><!-- /.item-price -->
                                                </div><!-- /.item-price -->
                                            </div><!-- /.item-details -->
                                        </div><!-- /.item -->
                                    </div><!-- /.cart-top -->
                                    <?php $total += $quant * $product->rent_price; } ?>
                                    <div class="cart-middle">
                                    <button onclick="delete_item(null, -1)" class="btn pull-left"><i class="ti-trash"></i> Empty Cart</button>
                                    <div class="price-total pull-right">
                                      <span>Sub total:</span>
                                      <span class="price"><?= 'EGP '.$total ?></span>
                                    </div><!-- /.price-total -->
                                  </div><!-- /.cart-middle -->
                                <div class="cart-bottom">
                                    <a href="<?= base_url() ?>Shop/cart" class="btn pull-left"><i class="icons icon-basket-loaded"></i> View Cart</a>
                                    <a href="<?= base_url() ?>Checkout" class="btn pull-right">Checkout</a>
                                </div><!-- /.cart-bottom -->
                            </div><!-- /.widget_shopping_cart_content -->
                     </div>
                        </div><!-- /.scart-items -->
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.header-middle -->
    <div class="header-bottom">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand visible-xs" href="./">
                    <img src="<?= base_url() ?>images/logo.png" alt="Site Logo">
                </a><!-- /.navbar-brand -->
            </div>
            <nav id="main-menu" class="menu collapse navbar-collapse pull-left">
                <ul class="nav navbar-nav num-list-top li">
                    <li class="menu-item menu-item-has-children active">
                        <a href="<?= base_url() ?>Home"><i class="fa fa-home"></i><?= ' ' ?>Home</a>
                    </li>
                    <li class="menu-item menu-item-has-children menu-item-has-mega-menu">
                        <a href="<?= base_url() ?>Shop">Shop</a>
                    </li>
                    <li class="menu-item menu-item-has-children menu-item-has-mega-menu">
                        <a href="<?= base_url() ?>Shop/cart">Shopping Cart</a>
                    </li> 
                    <li class="menu-item menu-item-has-children menu-item-has-mega-menu">
                        <a href="<?= base_url() ?>Checkout">Checkout</a>
                    </li>
                    <?php if(!empty($settings) && $settings->show_about) { ?>
                    <li class="menu-item menu-item-has-children menu-item-has-mega-menu">
                        <a href="<?= base_url() ?>About">About Us</a>
                    </li>
                    <?php } ?>
                    <li class="menu-item menu-item-has-children menu-item-has-mega-menu">
                        <a href="<?= base_url() ?>Contact">Contact Us</a>
                    </li>
                    <?php if(!empty($settings) && $settings->show_blog) { ?>
                    <li class="menu-item menu-item-has-children menu-item-has-mega-menu">
                        <a href="<?= base_url() ?>Blog">Blogs</a>
                    </li>
                    <?php } ?>
                    <li class="menu-item menu-item-has-children hidden">
                        <a href="#">Pages</a>
                        <ul class="sub-menu">
                            <li><a href="<?= base_url() ?>Faq">Faq</a></li>
                            <?php if(cid()==''){ ?>
                            <li><a href="<?= base_url() ?>Login">Login</a></li>
                            <li><a href="<?= base_url() ?>Register">Register</a></li>  
                            <?php } ?>  
                            <li><a href="<?= base_url() ?>Our_Services">Service</a></li>
                        </ul>
                    </li>
                </ul><!-- /.navbar-nav -->
            </nav><!-- /.navbar-collapse -->
            <div class="menu-social pull-right">
                <a target="_blank" href="<?= (!empty($settings->insta_url)) ? $settings->insta_url : '#' ?>"><i class="ti-instagram"></i></a>
                <a target="_blank" href="<?= (!empty($settings->fb_url)) ? $settings->fb_url : '#' ?>"><i class="ti-facebook"></i></a>
            </div><!-- /.menu-social -->
        </div><!-- /.container -->
    </div><!-- /.header-bottom -->
</header><!-- /#masthead -->

