<title><?= MY_APP_NAME ?> | <?= $title ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

<meta name="description" content="Cairo Camera Rentals">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" href="apple-touch-icon.png">

<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/themify-icons.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/et-line-icons.css">  
<link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/slick.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/magnific-popup.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/style_theme.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/custom.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.carousel.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/themes.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/home/home-11.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/shop/cart.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/shop/shop.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/shop/shop-details.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/blog/classic-01.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/blog/blog-single.css">
<link href="<?=base_url()?>assets/plugins/dropzone/dropzone.min.css" rel="stylesheet">
    <script src="<?=base_url()?>assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <!--[if lte IE 7]><script src="lte-ie7.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="assets/js/vendor/html5-3.6-respond-1.4.2.min.js"></script><![endif]-->

