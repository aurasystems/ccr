<section class="subscribe-03 background-bg hidden" data-image-src="<?= base_url() ?>images/home07/subscribe.jpg">
    <div class="container">
        <div class="subscribe-details">
            <div class="row">
                <div class="col-sm-5">
                    <div class="section-top">
                        <h3 class="section-title"><?= lang('stay_up_to_date') ?> <span></span></h3><!-- /.section-title -->
                    </div>
                </div>
                <div class="col-sm-7">
                    <form class="subscribe-form" action="#">
                        <input class="form-control" type="email" placeholder="myemail@email.com">
                        <input class="btn btn-subscribe" type="submit" value="Subscribe">
                    </form>
                </div>
            </div><!-- /.row -->
        </div><!-- /.subscribe-details -->
    </div><!-- /.section-padding -->
</section>