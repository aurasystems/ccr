<div id="scroll-to-top" class="scroll-to-top">
    <i class="fa fa-angle-double-up"></i>    
</div>


<script src="<?= base_url() ?>assets/js/plugins.js"></script>
<script src="<?= base_url() ?>assets/js/main.js"></script>
<script src="<?= base_url() ?>assets/js/bootstrap-rating.min.js"></script>
<script src="<?= base_url() ?>assets/js/jquery.lwtCountdown-1.0.js"></script>
<script src="<?= base_url() ?>assets/js/jquery.js"></script>
<script src="<?= base_url() ?>assets/js/jquery-ui.min.js"></script>
<script>
    $('#search_products').keyup(function () {
        var search_query = $(this).val();
        var form_data = {
            term: search_query,
            search_wiz: $('#category').val(),
            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
        }
        var resp_data_format = "";
        if (search_query != '') {
            $.ajax({
                url: "<?= base_url() ?>Search/search_products",
                data: form_data,
                method: "post",
                dataType: "json",
                success: function (response) {
                    for (var i = 0; i < response.length; i++) {
                        resp_data_format = resp_data_format + "<li class='search_item'><a href='<?= base_url() ?>Shop/item_details/" + response[i]['id'] + "'>" + response[i]['n_en'] + "</a></li><br>";
                    }
                    ;
                    $("#data-container").html(resp_data_format);
                }
            });
        } else {
            $("#data-container").html('');
        }
    });


    function delete_item_old(id, product_id) {
        window.location = "<?= base_url() ?>Cart/delete_cart/" + id + "/"+ product_id;
    }
    function start_end_date(product_id)
    {
    window.location = "<?= base_url() ?>Shop/check_availabilty/" + product_id;
    }
    function update_rating(product_id)
    {
        var postData = {
            product_id: product_id,
            rating: $('#product_rating'+product_id).val(),
            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
        };
        $.ajax({
                url: "<?= base_url() ?>Shop/add_rating",
                data: postData,
                method: "post",
                dataType: "json",
                success: function (){
                   
                }
            });

    }
    $(function(){
        $('.num-list-top li a').click(function(){
            $('.num-list-top li.active').removeClass('active');
            $(this).addClass('active');
        });
    });
      /*  function b_ava_check(){
            var product_id = $("#prod-id").val();
            var b_to = $('#date-type').val();
            var postData = {
            product_id: product_id,
            b_to: b_to,
            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
        };
        $.ajax({
            url: "<?= base_url(); ?>Shop/check_ava",
            method: "POST",
            data: postData,
            success: function(data) {
               if(data==true)
               {
                $('#available-alert').css("display","none");
                $('#available-alert').css("display","block");
                setTimeout(function () {
                    $('#available-alert').css("display","none");
                }, 5000);
                        $.ajax({
                            url: "<?= base_url(); ?>Shop/add_cart",
                            method: "POST",
                            data: postData,
                            success: function (data) {
                                $('#update_cart').html(data);
                            }
                });
               }
               else if(data==false)
               {
               $('#available-alert').css("display","none");
               $('#unavailable-alert').css("display","block");
               setTimeout(function () {
                $('#unavailable-alert').css("display","none");
                }, 5000);
               }
            }
        });
       } */


</script>