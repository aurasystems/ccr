<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/pages/register.css">
    </head>
    <body class="demo-page">
        <?php $this->load->view('front/private/header'); ?>
        <section class="page-name-sec page-name-sec-01">
            <div class="section-padding">
                <div class="container">
                    <h3 class="page-title"><?= lang('my_profile') ?></h3><!-- /.page-title -->

                    <div class="row">
                        <div class="col-sm-7">
                            <p class="description">
                                
                            </p><!-- /.description -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.page-name-sec -->

        <section class="login-section">
            <div class="section-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="sign-in bg-gray sign-up billing-table">
                                <h2 class="title"><?= lang('update_info') ?> <span></span></h2><!-- /.title -->
                                <form class="signup-form" id="signup-form" action="<?= base_url('Account/update_info') ?>" method="post">
                                    <?= csrf() ?>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="n_en" value="<?= !empty($one->n_en) ? $one->n_en : '' ?>" class="form-control" placeholder="<?= lang('name_en') ?>" required>
                                                <?php echo form_error('n_en'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="n_ar" value="<?= !empty($one->n_ar) ? $one->n_ar : '' ?>" class="form-control" placeholder="<?= lang('name_ar') ?>" required>  
                                                <?php echo form_error('n_ar'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="email" name="email" value="<?= !empty($one->email) ? $one->email : '' ?>" class="form-control" placeholder="<?= lang('email') ?>" required autocomplete="off">
                                                <?php echo form_error('email'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="phone" value="<?= !empty($one->phone) ? $one->phone : '' ?>" class="form-control" placeholder="<?= lang('phone') ?>" required>
                                                <?php echo form_error('phone'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="national_id" value="<?= !empty($one->national_id) ? $one->national_id : '' ?>" class="form-control" placeholder="<?= lang('national_id') ?>" required>
                                                <?php echo form_error('national_id'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="address_en" value="<?= !empty($one->address_en) ? $one->address_en : '' ?>" class="form-control" placeholder="<?= lang('address_en') ?>">
                                                <?php echo form_error('address_en'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="password" name="password" class="form-control" placeholder="<?= lang('password') ?>">
                                                <?php echo form_error('password'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="password" name="confirm_password" class="form-control" placeholder="<?= lang('confirm_password') ?>">
                                                <?php echo form_error('confirm_password'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="submit-input">
                                        <input type="submit" class="btn" value="<?= lang('save') ?>">
                                    </p>
                                </form>
                            </div><!-- /.sign-up -->
                        </div>
                    </div><!-- /.row -->
                </div><!--/.container-->
            </div><!-- /.section-padding -->
        </section><!--/.login-section-->

        <?php $this->load->view('front/private/subscribe'); ?>
        <?php $this->load->view('front/private/footer'); ?>
        <?php $this->load->view('front/private/footer_js'); ?>
    </body>
</html>