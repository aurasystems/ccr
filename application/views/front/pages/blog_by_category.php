<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
        <link rel="stylesheet" href="assets/css/pages/about-01.css">
    </head>
    <body class="blog page">
        <?php $this->load->view('front/private/header'); ?>
        <section class="page-name-sec page-name-sec-01">
            <div class="section-padding">
                <div class="container">
                    <h3 class="page-title"><?= $title . ' (' . $category->{'n_' . lc()} . ')' ?></h3><!-- /.page-title -->

                    <div class="row">
                        <div class="col-sm-7">
                            <p class="description">
                                
                            </p><!-- /.description -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.page-name-sec -->

        <section class="blog-posts left-sidebar">
            <div class="section-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 pull-right">
                            <?php
                            if (!empty($blogs)) {
                                foreach ($blogs as $blog) {
                                    ?>
                                    <article class="post type-post format-standard">
                                        <?php if (!empty($blog->img)) { ?>
                                            <div class="entry-thumbnail">
                                                <img src="<?= base_url() ?>uploads/blogs/<?= $blog->img ?>" alt="<?= lang('blog') ?>">
                                            </div>
                                        <?php } ?>
                                        <div class="post-content media">
                                            <div class="entry-meta media-left">
                                                <time datetime="<?= date('Y-m-d', strtotime($blog->create_time)) ?>"><span class="date"><?= date('d', strtotime($blog->create_time)) ?></span> <span class="month"><?= date('M', strtotime($blog->create_time)) ?></span></time>
                                            </div><!-- /.entry-meta -->

                                            <div class="entry-content media-body">
                                                <h2 class="entry-title"><?= $blog->title ?></h2><!-- /.entry-title -->

                                                <div class="meta-info">
                                                    <span class="author"><i class="ti-user"></i><?= $blog->uname ?></span>
                                                    <span class="cat-links"><i class="ti-tag"></i><?= $blog->category ?></span>
                                                </div><!-- /.meta-info -->

                                                <div class="description">
                                                    <p><?= substr($blog->content, 0, 200) ?></p>
                                                </div><!-- /.description -->

                                                <a href="<?= base_url() ?>Blog/get_details/<?= $blog->id ?>" class="btn">More <i class="ti-arrow-right"></i></a>
                                            </div><!-- /.entry-content -->
                                        </div><!-- /.content -->
                                    </article><!--/.post-->

                                    <?php
                                }
                            }
                            ?>

                            <div class="pagination">
                                <?= $links ?>
                            </div><!--/.pagination-->

                        </div>



                        <div class="col-md-4 pull-left">
                            <aside class="sidebar">
                                <div class="widget widget_categories">
                                    <h2 class="widget-title">Categories <span></span></h2><!-- /.widget-title -->
                                    <div class="widget-details">
                                        <ul>
                                            <?php
                                            if (!empty($categories)) {
                                                foreach ($categories as $cat) {
                                                    ?>
                                                    <li><a href="<?= base_url() ?>Blog/get_blog_by_category/<?= $cat->id ?>"><?= $cat->{'n_' . lc()} ?><span class="count"><?= $cat->count ?></span></a></li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div><!-- /.widget-details -->
                                </div><!--/.widget-->

                                <div class="widget widget_tag_cloud">
                                    <h2 class="widget-title">Tags <span></span></h2><!-- /.widget-title -->
                                    <div class="widget-details">
                                        <div class="tagcloud">
                                            <?php
                                            if(!empty($tags)) {
                                                foreach($tags as $tag) {
                                            ?>
                                            <a href="<?= base_url() ?>Blog/get_blog_by_tag/<?= $tag->tag ?>"><?= $tag->tag ?></a>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </div><!--/.tagcloud-->
                                    </div><!-- /.widget-details -->
                                </div><!--/.widget-->

                            </aside><!--/.sidebar-->
                        </div>
                    </div><!--/.row-->
                </div><!--/.container-->
            </div><!-- /.section-padding -->
        </section><!-- /.blog-posts -->
        <?php $this->load->view('front/private/subscribe'); ?>
        <?php $this->load->view('front/private/footer'); ?>
        <?php $this->load->view('front/private/footer_js'); ?>
    </body>
</html>
