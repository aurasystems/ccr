<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/pages/register.css">
    </head>
    <body class="demo-page">
        <?php $this->load->view('front/private/header'); ?>
        <section class="page-name-sec page-name-sec-01">
            <div class="section-padding">
                <div class="container">
                    <h3 class="page-title"><?= lang('results_for') . ' ' . $keyword ?></h3><!-- /.page-title -->
                    <div class="row">
                        <div class="col-sm-7">

                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.page-name-sec -->

        <section class="shop-contents">
            <div class="section-padding">
                <div class="container">
                    <div class="shop-products">
                        <div class="tab-content">
                            <?php if (!empty($results)) { ?>
                                <?php foreach ($results as $r) { ?>
                                    <div class="col-md-3">
                                        <div class="item">
                                            <?php
                                            if (!empty($r->img)) {
                                                $image = 'uploads/products/'.$r->img;
                                            }
                                            else {
                                                $image = 'images/home08/featured/1.jpg';
                                            }
                                        ?>
                                        <div class="item-thumbnail">
                                            <a class="fancybox" href="<?= base_url().$image ?>">
                                                <img src="<?= base_url().$image ?>" alt="Item Thumbnail">
                                            </a>
                                        </div><!-- /.item-thumbnail -->
                                        <div class="item-content">
                                            <div class="buttons">
                                            <button type="submit" onclick="start_end_date(<?= $r->id ?>)">Add to cart<i class="fa fa-shopping-cart"></i></button>                                            
                                            </div><!-- /.buttons -->
                                            <h3 class="item-title text-center"><a href="<?= base_url() ?>Shop/item_details/<?= $r->id ?>"><?= $r->{'n_' . lc()} ?></a></h3><!-- /.item-title -->
                                            <div class="item-price display_block text-center">
                                                <span class=""><?= lang('egp') ?></span>
                                                <span class="price"><?= $r->rent_price ?></span>
                                            </div><!-- /.item-price -->
                                        </div><!-- /.item-content -->
                                    </div><!-- /.item -->
                                </div>
                                <?php } ?>
                            <?php } else { ?>
                                <h2 class="text-center"><?= lang('no_result_found') ?></h2>
                            <?php } ?>
                        </div><!-- /.tab-content -->
                    </div><!-- /.shop-products -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.shop-contents -->
        <?php $this->load->view('front/private/subscribe'); ?>
        <?php $this->load->view('front/private/footer'); ?>
        <?php $this->load->view('front/private/footer_js'); ?>
    </body>
</html>