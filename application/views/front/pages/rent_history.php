<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/pages/register.css">
    </head>
    <body class="demo-page">
        <?php $this->load->view('front/private/header'); ?>
        <section class="page-name-sec page-name-sec-01">
            <div class="section-padding">
                <div class="container">
                    <h3 class="page-title"><?= lang('rent_history') ?></h3><!-- /.page-title -->

                    <div class="row">
                        <div class="col-sm-7">
                            <p class="description">
                                
                            </p><!-- /.description -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.page-name-sec -->
 <!-- Begin Ecommerce Wish List -->
 <br>
 <br>
 <section class="section-ecommerce container">
      <div class="wishlist clearfix">
        <div class="row">
          <div class="col-md-12">
            <p class="m-b-30">Here you will find all your <?= lang('rent_history') ?>.</p>
            <div class="table-responsive shopping-cart-table">
          
              <table class="table">
                <thead>
                  <tr>
                    <th>
                      <?=lang("quantity")?>
                    </th>
                    <th class="text-center">
                      <?=lang("img")?>
                    </th>
                    <th class="text-center">
                      <?=lang("product_details")?>
                    </th>
                    <th class="text-center">
                      <?=lang("from")?>
                    </th>
                    <th class="text-center">
                      <?=lang("to")?>
                    </th>
                    <th class="text-center">
                      <?=lang("status")?>
                    </th>
                    <th class="text-right">
                      <?=lang("price")?>
                    </th>
                  </tr>
                </thead>
                <tbody>
                <div style="border-top-style: solid; border-width: thin; ">
                <?php $i=0; foreach($booking as $one){?>
                  <tr>
                  <td><?=$one->quantity?></td>
                    <td class="text-center">
                      <a class="fancybox" href="<?= base_url() ?>uploads/products/<?= $one->img?>">
                      <img src="<?=base_url()?>/uploads/products/<?=$one->img?>" alt="<?=$one->img?>" width="auto" height="60">
                      </a>
                    </td>
                    <td class="text-center">
                      <strong href="ecommerce-product-detail.html"><?=$one->n_en?></strong>
                    </td>
                    <td class="text-center">
                      <?=$one->b_from?>
                    </td>
                    <td class="text-center">
                    <?=$one->b_from?>
                    </td>
                    <td class="text-center">
                    <?php if($one->canceled == 1)
                          echo lang("canceled");
                          else if($one->status == 1 || $one->status == 2)
                          echo lang("approved");
                          else if($one->status == 0)
                          echo lang("pending") ?>
                    </td>
                    <td class="text-right">
                    <strong>EGP</strong>&nbsp;<span class="item-total"><?=$one->rent_price?></span>
                    </td>
                    <td><?php if($can_cancel[$i] == true){?>
                      <div class="btn-group">
                      <button title="<?=lang("cancel")?>" onclick="cancel_booking('<?=$one->book_id?>')" type="button" class="close" aria-label="Close"><i style="color:red" class="fa fa-times"></i></button>
                      </div>
                    <?php } ?></td>
                  </tr>
                <?php $i++; } ?>
                </div>
                </tbody>
              </table>
              <!--penalty section--->
              <hr>
              <p class="m-b-30">Your rent penalty</p>
              <table class="table">
                <thead>
                  <tr>
                    <th>
                      <?=lang("#")?>
                    </th>
                    <th class="text-center">
                      <?=lang("description")?>
                    </th>
                    <th class="text-center">
                      <?=lang("cost")?>
                    </th>
                   
                  </tr>
                </thead>
                <div style="border-top-style: solid; border-width: thin; ">
                <tbody>
                <?php $penalty = get_client_penalty();
                  $i = 0;
                  foreach($penalty as $one){?>
                  <tr>
                  <td><?=$i++?></td>
                  <td class="text-center"><strong><?=$one->pname?></strong></td>
                  <td class="text-center"><?=$one->cost?></td>
                  </tr>
                <?php } ?>
                </div>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Ecommerce Wish List -->
    <?php $this->load->view('front/private/subscribe'); ?>
    <?php $this->load->view('front/private/footer'); ?>
    <?php $this->load->view('front/private/footer_js'); ?>
    <script>
        function cancel_booking(book_id)
            {
                var conf = confirm("<?= lang("r_u_sure_cancel") ?>");
                    if (conf) {
                        window.location = "<?= base_url() ?>Account/cancel_booking/" + book_id;
                    }
            }
            
    </script>
</body>
</html>
