<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
    </head>
    <body class="demo-page">

        <?php $categories = get_categories(); ?>
        <?php $this->load->view('front/private/header'); ?>
        <section class="banner banner-11 text-left background-bg" data-image-src="<?= base_url() ?>images/home11/banner.jpg">
            <div class="container">
                <div class="row">
                    <div class="banner-top">
                        <div class="col-sm-3">
                            <h3 class="banner-title"><?= lang('shop_by_categ') ?></h3>
                            <div class="shop-category">
                            <ul class="category-menu">
                           <?= $menu ?>
                            </ul>
                            </div><!-- /.shop-category -->
                        </div>
                        <div class="col-sm-6">
                            <div id="banner-slider" class="banner-slider carousel slide background-bg" data-image-src="<?= base_url() ?>images/home11/slider/1.jpg">
                                <ol class="carousel-indicators">
                                    <li data-target="#banner-slider" data-slide-to="0" class="active"></li>
                                    <li data-target="#banner-slider" data-slide-to="1"></li>
                                    <li data-target="#banner-slider" data-slide-to="2"></li>
                                </ol>
                               
                                <div class="carousel-inner">
                                    
                                    <?php $n = 1; $slider = get_slider();
                                if(!empty($slider)){
                                    foreach($slider as $slide){ ?>

                                    <div class="item <?=$n==1? 'active':''?>">
                                        <div class="col-sm-6">
                                        <img class="banner-image" src="<?= base_url() ?>uploads/slider/<?=$slide->img?>" alt="Slider Image">
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="slider-content">
                                                <h4 class="top-title">Brand New</h4>
                                                <h2 class="main-title"><?=$slide->{'n_' . lc()} ?></h2><!-- /.banner-title -->
                                                <h3 hidden class="sub-title">Rose Gold</h3><!-- /.banner-sub-title --> 
                                                <a href="<?= base_url() ?>Shop" class="btn">Shop now</a><!-- /.btn -->
                                            </div><!-- /.slider-content -->
                                        </div>
                                    </div><!-- /.item -->
                                    <?php $n++; }
                                    }?>
                                </div>
                               
                            </div><!-- /#banner-slider -->
                        </div>
                        <div class="col-sm-3">
                            <h3 class="banner-title"><?= lang('top_seller') ?></h3><!-- /.banner-title -->
                            <div id="top-sell-slider" class="top-sell-slider carousel slide text-center">
                                <ol class="carousel-indicators">
                                    <li data-target="#top-sell-slider" data-slide-to="0" class="active"></li>
                                    <li data-target="#top-sell-slider" data-slide-to="1"></li>
                                    <li data-target="#top-sell-slider" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <?php $top_seller = get_top_seller(); 
                                    if(!empty($top_seller)){
                                        for($i = 0; $i<count($top_seller); $i+=2){?>
                                    <div class="item active">
                                        <div class="product-item">
                                            <div class="item-thumbnail">
                                                <img src="<?= base_url() ?>uploads/thumb/<?=$top_seller[$i]->img?>" alt="Item Thumbnail">
                                                <?php if(cid()!=''){?>
                                                <div class="add-cart"><a onclick="start_end_date(<?=$top_seller[$i]->id?>)" class="btn">Add to cart</a></div><!-- /.add-cart -->
                                                <?php }?>
                                            </div><!-- /.item-thumbnail -->
                                            <div class="item-details">
                                                <h3 class="item-title"><?=$top_seller[$i]->{'n_' . lc()} ?></h3><!-- /.item-title -->
                                                <div class="item-price"><span class="currency">EGP</span><span class="price"><?=$top_seller[$i]->rent_price?></span></div><!-- /.item-price -->
                                            </div><!-- /.item-details -->
                                        </div><!-- /.product-item -->
                                        <?php if($i+1 < count($top_seller)) {?>
                                        <div class="product-item">
                                            <div class="item-thumbnail">
                                            <img src="<?= base_url() ?>uploads/thumb/<?=$top_seller[$i+1]->img?>" alt="Item Thumbnail">
                                            <?php if(cid()!=''){?>
                                                <div class="add-cart"><a onclick="start_end_date(<?=$top_seller[$i]->id?>)" class="btn">Add to cart</a></div><!-- /.add-cart -->
                                            <?php }?>
                                            </div><!-- /.item-thumbnail -->
                                            <div class="item-details">
                                            <h3 class="item-title"><?=$top_seller[$i+1]->{'n_' . lc()} ?></h3><!-- /.item-title -->
                                            <div class="item-price"><span class="currency">EGP</span><span class="price"><?=$top_seller[$i+1]->rent_price?></span></div><!-- /.item-price -->
                                            </div><!-- /.item-details -->
                                        </div><!-- /.product-item -->
                                        <?php }?>
                                    </div><!-- /.item -->
                                    <?php }
                                    }?>
                                </div>
                            </div><!-- /#top-sell-slider -->
                        </div>
                    </div><!-- /.banner-top -->

                </div>
            </div><!-- /.container -->
        </section><!-- /.banner -->
        <!-- Featured -->
        <section class="featured featured-11 text-center">
            <div class="section-padding">
                <div class="container">
                    <div class="row">
                        <div class="section-top">
                            <h2 class="section-title"><?= lang('featured') ?><span></span></h2>
                        </div><!-- /.section-top -->
                        <ul class="filter">
                            <li><a class="active" href="" data-filter="*">All</a></li>
                            <?php foreach($categories as $category) {?>
                            <li><a   href="" data-filter=".cat-<?=$category->id?>"><?= $category->n_en?></a></li>
                            <?php } ?>
                        </ul>
                        <div class="featured-sorting">
                            <?php 
                             $categories = get_categories();
                            foreach($categories as $cat) {
                            $products = get_featured_products($cat->id);
                            if($products!=null)
                            {
                            foreach($products as $product) {
                            ?>
                            <div class="item col-md-3 col-sm-4 cat-<?=$cat->id?>" >
                                <div class="item-thumbnail">
                                   <a class="fancybox" href="<?= base_url() ?>uploads/products/<?= $product->img ?>">
                                        <img class="img" src="<?= base_url() ?>uploads/products/<?= $product->img ?>" alt="<?= $product->img ?>">   </a>
                                </div><!-- /.item-thumbnail -->
                                <div class="item-content">
                                <?php if ((cid() != "")) { ?>
                                    <div class="buttons">
                                    <button type="submit" onclick="start_end_date(<?=$product->id?>)">Add to cart<i class="fa fa-shopping-cart"></i></button>
                                        
                                    </div><!-- /.buttons -->
                                <?php } ?>
                                    <h3 class="item-title"><a href="<?= base_url()?>Shop/item_details/<?=$product->id?>"><?= $product->n_en ?></a></h3><!-- /.item-title -->
                                    <div class="item-price">
                                        <span class="currency">EGP&nbsp;</span>
                                        <span class="price"><?= $product->rent_price ?></span>
                                    </div><!-- /.item-price -->
                                    
                                    <?php if(cid() != '')
                                    {?>
                                    <div class="rating"><input type="hidden" id="product_rating<?=$product->id?>" value="<?=get_rating($product->id)?>"  onchange="update_rating(<?=$product->id?>)" class="rating-tooltip-manual" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="5"/></div><!-- /.rating -->
                                    <?php }?>
                                </div><!-- /.item-content -->
                            </div><!-- /.item -->
                            <?php
                            }
                        }
                   }
                         ?>
                        </div><!-- /.featured-sorting -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.featured -->
        <!-- Deal -->
        
        <!-- /.deal -->
        <!-- Trending -->
        <section class="trending-03 hidden">
            <div class="section-padding">
                <div class="container">
                    <h2 class="section-title"><?= lang('trending') ?> <span></span></h2><!-- /.section-title -->
                    <div class="section-details">
                        <div class="row">
                           <div class="trending-slider-03 text-center">
                                <div class="item">
                                    <div class="item-thumbnail"><a class="fancybox" href="<?= base_url() ?>images/home08/featured/2.jpg"><img src="<?= base_url() ?>images/home08/featured/2.jpg" alt="Item Thumbnail"></a></div><!-- /.item-thumbnail -->
                                    <div class="item-content">
                                        <div class="buttons">
                                            <button class="add-to-cart">Add to cart<i class="fa fa-shopping-cart"></i></button>
                                            <button class="wish-list"><i class="fa fa-heart"></i></button>
                                        </div><!-- /.buttons -->
                                        <h3 class="item-title"><a href="#">Product name here</a></h3><!-- /.item-title -->
                                        <div class="item-price">
                                            <span class="currency">$</span>
                                            <span class="price">49.00</span>
                                        </div><!-- /.item-price -->
                                        <div class="rating"><input type="hidden" class="rating-tooltip-manual" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="5"/></div><!-- /.rating -->
                                    </div><!-- /.item-content -->
                                </div><!-- /.item -->
                                <div class="item">
                                    <div class="item-thumbnail"><a class="fancybox" href="<?= base_url() ?>images/home08/featured/3.jpg"><img src="<?= base_url() ?>images/home08/featured/3.jpg" alt="Item Thumbnail"></a></div><!-- /.item-thumbnail -->
                                    <div class="item-content">
                                        <div class="buttons">
                                            <button class="add-to-cart">Add to cart<i class="fa fa-shopping-cart"></i></button>
                                            <button class="wish-list"><i class="fa fa-heart"></i></button>
                                        </div><!-- /.buttons -->
                                        <h3 class="item-title"><a href="#">Product name here</a></h3><!-- /.item-title -->
                                        <div class="item-price">
                                            <span class="currency">$</span>
                                            <span class="price">49.00</span>
                                        </div><!-- /.item-price -->
                                        <div class="rating"><input type="hidden" class="rating-tooltip-manual" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="5"/></div><!-- /.rating -->
                                    </div><!-- /.item-content -->
                                </div><!-- /.item -->
                                <div class="item">
                                    <div class="item-thumbnail"><a class="fancybox" href="<?= base_url() ?>images/home08/featured/4.jpg"><img src="<?= base_url() ?>images/home08/featured/4.jpg" alt="Item Thumbnail"></a></div><!-- /.item-thumbnail -->
                                    <div class="item-content">
                                        <div class="buttons">
                                            <button class="add-to-cart">Add to cart<i class="fa fa-shopping-cart"></i></button>
                                            <button class="wish-list"><i class="fa fa-heart"></i></button>
                                        </div><!-- /.buttons -->
                                        <h3 class="item-title"><a href="#">Product name here</a></h3><!-- /.item-title -->
                                        <div class="item-price">
                                            <span class="currency">$</span>
                                            <span class="price">49.00</span>
                                        </div><!-- /.item-price -->
                                        <div class="rating"><input type="hidden" class="rating-tooltip-manual" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="5"/></div><!-- /.rating -->
                                    </div><!-- /.item-content -->
                                </div><!-- /.item -->
                                <div class="item">
                                    <div class="item-thumbnail"><a class="fancybox" href="<?= base_url() ?>images/home08/featured/5.jpg"><img src="<?= base_url() ?>images/home08/featured/5.jpg" alt="Item Thumbnail"></a></div><!-- /.item-thumbnail -->
                                    <div class="item-content">
                                        <div class="buttons">
                                            <button class="add-to-cart">Add to cart<i class="fa fa-shopping-cart"></i></button>
                                            <button class="wish-list"><i class="fa fa-heart"></i></button>
                                        </div><!-- /.buttons -->
                                        <h3 class="item-title"><a href="#">Product name here</a></h3><!-- /.item-title -->
                                        <div class="item-price">
                                            <span class="currency">$</span>
                                            <span class="price">49.00</span>
                                        </div><!-- /.item-price -->
                                        <div class="rating"><input type="hidden" class="rating-tooltip-manual" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="5"/></div><!-- /.rating -->
                                    </div><!-- /.item-content -->
                                </div><!-- /.item -->
                                <div class="item">
                                    <div class="item-thumbnail"><a class="fancybox" href="<?= base_url() ?>images/home08/featured/6.jpg"><img src="<?= base_url() ?>images/home08/featured/6.jpg" alt="Item Thumbnail"></a></div><!-- /.item-thumbnail -->
                                    <div class="item-content">
                                        <div class="buttons">
                                            <button class="add-to-cart">Add to cart<i class="fa fa-shopping-cart"></i></button>
                                            <button class="wish-list"><i class="fa fa-heart"></i></button>
                                        </div><!-- /.buttons -->
                                        <h3 class="item-title"><a href="#">Product name here</a></h3><!-- /.item-title -->
                                        <div class="item-price">
                                            <span class="currency">$</span>
                                            <span class="price">49.00</span>
                                        </div><!-- /.item-price -->
                                        <div class="rating"><input type="hidden" class="rating-tooltip-manual" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="5"/></div><!-- /.rating -->
                                    </div><!-- /.item-content -->
                                </div><!-- /.item -->
                                <div class="item">
                                    <div class="item-thumbnail"><a class="fancybox" href="<?= base_url() ?>images/home08/featured/7.jpg"><img src="<?= base_url() ?>images/home08/featured/7.jpg" alt="Item Thumbnail"></a></div><!-- /.item-thumbnail -->
                                    <div class="item-content">
                                        <div class="buttons">
                                            <button class="add-to-cart">Add to cart<i class="fa fa-shopping-cart"></i></button>
                                            <button class="wish-list"><i class="fa fa-heart"></i></button>
                                        </div><!-- /.buttons -->
                                        <h3 class="item-title"><a href="#">Product name here</a></h3><!-- /.item-title -->
                                        <div class="item-price">
                                            <span class="currency">$</span>
                                            <span class="price">49.00</span>
                                        </div><!-- /.item-price -->
                                        <div class="rating"><input type="hidden" class="rating-tooltip-manual" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="5"/></div><!-- /.rating -->
                                    </div><!-- /.item-content -->
                                </div><!-- /.item -->
                                <div class="item">
                                    <div class="item-thumbnail"><a class="fancybox" href="<?= base_url() ?>images/home08/featured/8.jpg"><img src="<?= base_url() ?>images/home08/featured/8.jpg" alt="Item Thumbnail"></a></div><!-- /.item-thumbnail -->
                                    <div class="item-content">
                                        <div class="buttons">
                                            <button class="add-to-cart">Add to cart<i class="fa fa-shopping-cart"></i></button>
                                            <button class="wish-list"><i class="fa fa-heart"></i></button>
                                        </div><!-- /.buttons -->
                                        <h3 class="item-title"><a href="#">Product name here</a></h3><!-- /.item-title -->
                                        <div class="item-price">
                                            <span class="currency">$</span>
                                            <span class="price">49.00</span>
                                        </div><!-- /.item-price -->
                                        <div class="rating"><input type="hidden" class="rating-tooltip-manual" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="5"/></div><!-- /.rating -->
                                    </div><!-- /.item-content -->
                                </div><!-- /.item -->
                                <div class="item">
                                    <div class="item-thumbnail"><a class="fancybox" href="<?= base_url() ?>images/home08/featured/9.jpg"><img src="<?= base_url() ?>images/home08/featured/9.jpg" alt="Item Thumbnail"></a></div><!-- /.item-thumbnail -->
                                    <div class="item-content">
                                        <div class="buttons">
                                            <button class="add-to-cart">Add to cart<i class="fa fa-shopping-cart"></i></button>
                                            <button class="wish-list"><i class="fa fa-heart"></i></button>
                                        </div><!-- /.buttons -->
                                        <h3 class="item-title"><a href="#">Product name here</a></h3><!-- /.item-title -->
                                        <div class="item-price">
                                            <span class="currency">$</span>
                                            <span class="price">49.00</span>
                                        </div><!-- /.item-price -->
                                        <div class="rating"><input type="hidden" class="rating-tooltip-manual" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="5"/></div><!-- /.rating -->
                                    </div><!-- /.item-content -->
                                </div><!-- /.item -->
                            </div><!-- /.trending-slider -->
                        </div><!-- /.row -->
                    </div><!-- /.section-details -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.trending -->
        <!-- Other Banners -->
        <section class="other-banners other-banners-09 hidden">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="item">
                            <a href="#">
                                <img src="<?= base_url() ?>images/home08/others/4.jpg" alt="Banner Image">
                                <div class="item-details">
                                    <h3 class="item-title text-center">
                                        <span>Up to 45% off</span> on iphone 6
                                    </h3><!-- /.item-title -->
                                </div><!-- /.item-details -->
                            </a>
                        </div><!-- /.item -->
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="item">
                                    <a href="#">
                                        <img src="<?= base_url() ?>images/home08/others/1.jpg" alt="Banner Image">
                                        <div class="item-details">
                                            <h3 class="item-title">
                                                <span>Microsoft</span> surface pro 3
                                            </h3><!-- /.item-title -->
                                        </div><!-- /.item-details -->
                                    </a>
                                </div><!-- /.item -->
                            </div>
                            <div class="col-sm-6">
                                <div class="item">
                                    <a href="#">
                                        <img src="<?= base_url() ?>images/home08/others/2.jpg" alt="Banner Image">
                                        <div class="item-details">
                                            <h3 class="item-title text-center">
                                                <span>Stereo</span> headphone <span>collect now</span>
                                            </h3><!-- /.item-title -->
                                        </div><!-- /.item-details -->
                                    </a>
                                </div><!-- /.item -->
                            </div>
                            <div class="col-sm-6">
                                <div class="item">
                                    <a href="#">
                                        <img src="<?= base_url() ?>images/home08/others/3.jpg" alt="Banner Image">
                                        <div class="item-details">
                                            <h3 class="item-title">
                                                <span>-50% discounts</span> on video cams
                                            </h3><!-- /.item-title -->
                                        </div><!-- /.item-details -->
                                    </a>
                                </div><!-- /.item -->
                            </div>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.other-banners -->
        <!-- start  time modal      -->
        <div class="modal bd-example-modal-lg fade" id="myModal-start" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                        <div class="modal-header panel-header bg-primary">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title  " id="myModalLabel" value=''><?= lang("select_start_date") ?></h4>
                        </div>
                        <div class="modal-body">
                        <form method="POST" action="<?= base_url() ?>Shop/b_start_d" class="form-horizontal" role="form">
                                <?= csrf() ?>
                            <div class="form-group">
                            <label class="col-sm-4 control-label" for="n_en"><?= lang('start_date') ?> : <span class="text-red">*</span></label>
                                <div class="col-sm-7">
                                <input type="text" id="datepicker-from" autocomplete="off" name="b_from"  class="input" value="" placeholder="<?= lang('select_start_date') ?>" required/>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                            </div>
                        </form>
                        </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->                                    
        <!-- start  time modal-->
        <!--termination time modal-->
        <div class="modal bd-example-modal-lg fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                        <div class="modal-header panel-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="panel-title"  value=''><?= lang("duration") ?></h4>
                        </div>
                    <div class="modal-body">
                        <form method="POST" action="" class="form-horizontal" role="form">
                        <?= csrf() ?>
                        <div class="form-group">
                        <label class="col-sm-4 control-label" for="n_en"><?= lang('choose_end_d') ?> : <span class="text-red">*</span></label>
                            <div class="col-sm-7">
                                <input hidden type="text" name="id" id="prod-id"  class="input" value="" />
                                <input  type="text" autocomplete="off" name="b_to"  id="date-type" class="input" value="" placeholder="<?= lang('select_end_date') ?>" required/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="button" class="btn" onclick="b_ava_check()"  value="<?= lang("check_avaibility") ?>">
                            </div>
                            <div class="alert alert-success" role="alert" style="display:none;" id="available-alert"> 
                            <i class="icon ti-info-alt"></i><strong><?= lang("available").lang("added_cart")?></strong>
                            </div>
                            <div class="alert alert-danger" role="alert" style="display:none;" id="unavailable-alert"> 
                            <i class="icon ti-info-alt"></i><strong><?= lang("unavailable")?></strong>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!--termination time modal-->
        <!-- Top Rated -->
        <section class="top-rated bg-gray text-center">
            <div class="section-padding">
                <div class="container">
                    <div class="section-top">
                        <h2 class="section-title"><?= lang('top_rated') ?><span></span></h2><!-- /.section-title -->
                    </div><!-- /.section-top -->
                    <div class="row">
                        <div class="top-rated-slider owl-carousel owl-theme">
                            <?php $top_rated = get_top_rated();
                                if(!empty($top_rated)){
                                    foreach($top_rated as $top){ ?>
                            <div class="item">
                                <?php if(!empty($top->img)) { ?>
                                <div class="col-md-6">
                                    <div class="item-thumbnail">
                                        <img src="<?= base_url() ?>uploads/thumb/<?= $top->img ?>" alt="Item Thumbnail">
                                        <span hidden class="ribbon sale">Sale</span><!-- /.ribbon -->
                                        <div class="item-inner">
                                            <a class="fancybox" href="<?= base_url() ?>uploads/thumb/<?= $top->img ?>"><i class="fa fa-search"></i> <span> Quick View</span></a>
                                        </div><!-- /.item-inner -->
                                    </div><!-- /.item-thumbnail -->
                                </div>
                                <?php } ?>
                                <div class="col-md-6">
                                    <div class="item-details">
                                        <h3 class="item-title"><a href="<?= base_url() ?>Shop/item_details/<?= $top->id ?>"><?=$top->{'n_' . lc()}?></a></h3><!-- /.item-title -->
                                        <div class="item-price">
                                            <span class="currency">EGP&nbsp;</span><!-- /.currency -->
                                            <span class="price"><?=$top->rent_price?></span><!-- /.price -->
                                        </div><!-- /.item-price -->
                                        <p class="description" style="min-height: 100px;">
                                            <?= substr($top->description, 0, 100) ?>
                                        </p><!-- /.description -->
                                        <?php if(cid()!=''){?>
                                        <a onclick="start_end_date(<?=$top->id?>)" class="btn">Add to cart</a><!-- /.btn -->
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                            <?php }
                            }?>
                        </div><!-- /.top-rated-slider -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.top-rated -->
        <!-- Clients Logo -->
        <section class="clients-logo clients-logo-11 text-center hidden">
            <div class="section-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="item"><a href="#"><img src="<?= base_url() ?>images/brand/1.png" alt="Brand Logo"></a></div><!-- /.item -->
                        </div>
                        <div class="col-sm-3">
                            <div class="item"><a href="#"><img src="<?= base_url() ?>images/brand/2.png" alt="Brand Logo"></a></div><!-- /.item -->
                        </div>
                        <div class="col-sm-3">
                            <div class="item"><a href="#"><img src="<?= base_url() ?>images/brand/3.png" alt="Brand Logo"></a></div><!-- /.item -->
                        </div>
                        <div class="col-sm-3">
                            <div class="item"><a href="#"><img src="<?= base_url() ?>images/brand/4.png" alt="Brand Logo"></a></div><!-- /.item -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section>
        <?php $this->load->view('front/private/subscribe'); ?>
        <?php $this->load->view('front/private/footer'); ?>
        <?php $this->load->view('front/private/footer_js'); ?>
    </body>
</html>
