<!doctype html>
<head>
    <?php $this->load->view('front/private/head'); ?>
    <link rel="stylesheet" href="assets/css/shop/checkout.css">
</head>
<body>
    <?php $this->load->view('front/private//header'); ?>           
    <section class="page-name-sec page-name-sec-01">
        <div class="section-padding">
            <div class="container">
                <h3 class="page-title"><?= lang("checkout") ?></h3><!-- /.page-title -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.section-padding -->
</section><!-- /.page-name-sec -->
<section class="checkout-section">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <?= flash_success() ?>
                <?= flash_error() ?>
                <div class="col-md-7">
                    <div class="methods">
                        <?php $c_voucher = get_cart_voucher(cid()); ?>
                        <?php if (cid() == '') { ?>
                            <div class="returning-customer">
                                <h4 class="checkout-title"><?= lang('login') ?></h4>
                                <form class="sign-in-form" id="sign-in-form" action="<?= base_url('Login/do_login') ?>" method="post">
                                    <?= csrf() ?>
                                    <div class="form-group">
                                        <input type="email" name="email" id="user_login" class="input" value="" placeholder="<?= lang('email') ?>" required/>
                                        <?php echo form_error('email'); ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" id="user_pass" class="input" value="" placeholder="<?= lang('password') ?>" required/>
                                        <?php echo form_error('password'); ?>
                                    </div>
                                    <p class="form-input">
                                        <input type="submit" class="btn" value="<?= lang('sign_in') ?>" />
                                    </p>
                                    <p class="checkbox pull-left">
                                        <input name="rememberme" type="checkbox" class="rememberme" value="<?= lang('rem_me') ?>"/> 
                                        <?= lang('rem_me') ?>
                                        <a href="#" class="pull-right" title="<?= lang('recover_your_pass') ?>"><?= lang('forgot_pass') ?></a>
                                    </p>
                                </form>
                            </div><!-- /.returning-customer -->
                        <?php } ?>
                    </div><!-- /.methods -->
                </div>
                <?php $sub_total = 0; ?>
                <div class="col-md-5">
                    <h3 class="title"><?= lang('have_cop') ?></h3>
                    <?php
                    if (empty($c_voucher)) {
                        $url = 'Checkout/coupon_validation';
                    } else {
                        $url = 'Checkout/remove_voucher/'.$c_voucher->id;
                    }
                    ?>
                    <form class="checkout_coupon" method="post" action="<?= base_url().$url ?>">
                        <?= csrf() ?>
                        <p class="form-row form-row-first">
                            <input type="text" name="coupon_code" class="input-text" placeholder="<?= lang("enter_code") ?>" id="coupon_code" value="">
                            <input type="text" name="sub_total" value="<?= $sub_total ?>" hidden>
                            <span class="text-red"><?php echo form_error('coupon_number'); ?></span>
                        </p>
                        <?php if (empty($c_voucher)) { ?>
                            <p class="form-row form-row-last">
                                <input type="submit" class="button btn btn-red" name="apply_coupon" value="Apply">
                            </p>
                        <?php } else { ?>
                            <p class="form-row form-row-last">
                                <input type="submit" class="button btn btn-red" name="apply_coupon" value="Remove">
                            </p>
                        <?php } ?>
                    </form>   
                    <?php if (cid() != '') { ?>           
                        <div id="order_review" class="woocommerce-checkout-review-order">
                            <h3 class="title">Your Order</h3>
                            <table class="cart-table">
                                <thead>
                                    <tr>
                                        <th class="product-name">Product</th>
                                        <th class="product-total pull-right">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($items as $item) {
                                        $product = get_product_details($item->product_id);
                                        ?>
                                        <tr class="cart_item">
                                            <td class="product-name order-item">
                                                <a class="fancybox pull-left" href="<?= base_url() ?>uploads/products/<?= $product->img ?>">
                                                    <img width="100px" height="auto" src="<?= base_url() ?>uploads/products/<?= $product->img ?>" alt="<?= $product->img ?>">
                                                    <h3 class="item-title"><?= $product->n_en ?><span class="count"> x <?php
                                                            $quatitiy = get_quatitiy($item->id);
                                                            echo $quatitiy;
                                                            ?></span></h3>              
                                                </a>
                                            </td>
                                            <td class="product-total pull-right">
                                                <span class="amount"><?php
                                                    $sub_to_each = $product->rent_price * $quatitiy;
                                                    echo $sub_to_each;
                                                    ?></span>  
                                            </td>
                                        </tr>
                                        <?php
                                        $sub_total += $item->total_cost;
                                    }
                                    ?>
                                </tbody>

                                <tfoot>
                                    <tr class="cart-subtotal">
                                        <th>Subtotal</th>
                                        <td class="pull-right"><span class="amount">LE <?= $sub_total ?></span></td>
                                    </tr>
                                    <tr class="cart-subtotal">
                                        <th>Voucher</th>
                                        <td class="pull-right"><span class="amount">LE <?= (!empty($c_voucher)) ? $c_voucher->amount : 0 ?></span></td>
                                    </tr>
                                    <tr class="order-total">
                                        <th>Order Total</th>
                                        <td class="pull-right"><strong><span class="amount">LE <?= ($sub_total - ((!empty($c_voucher)) ? $c_voucher->amount : 0)) ?></span></strong> </td>
                                    </tr>
                                </tfoot>

                            </table>

                            <div class="payment-method">
                                <?php if (!empty($items)) { ?>
                                    <form action="<?= base_url() ?>Checkout/check_all" class="payment-form" method="POST">
                                    <?php } ?>
                                    <?php if (empty($items)) { ?>
                                        <form action="<?= base_url() ?>Checkout" class="payment-form" method="POST">
                                        <?php } ?>
                                        <?= csrf() ?>
                                        <div class="cheque-method ">
                                            <label > <?= lang("pick_up") . lang("location") ?></label>
                                            <select name="location_id" class="form-control" >
                                                <?php foreach ($locations as $loc) { ?>
                                                    <option value="<?= $loc->id ?>" ><?= $loc->n_en ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <?php if (!empty($settings) && ($settings->booking_cash_limit == 1) && ($settings->booking_credit_limit == 0)) { ?>
                                            <div class="cheque-method ">
                                                <label > <?= lang("cash") ?></label>
                                                <input  type="checkbox" value='' name="cash" required>
                                            </div>
                                        <?php } ?>
                                        <?php if (!empty($settings) && ($settings->booking_credit_limit == 1) && ($settings->booking_cash_limit == 0)) { ?>
                                            <div class="cheque-method ">
                                                <label ><?= lang("credit_card") ?></label>
                                                <input type="checkbox" value='' name="credit" required>
                                            </div>
                                        <?php } ?>
                                        <?php if (!empty($settings) && ($settings->booking_cash_limit == 1) && ($settings->booking_credit_limit == 1)) { ?>
                                            <div class="cheque-method">
                                                <label > <?= lang("cash") ?></label>
                                                <input checked  type="radio" value='1' name="payment_method">
                                            </div>
                                            <div class="cheque-method">
                                                <label ><?= lang("credit_card") ?></label>
                                                <input type="radio" value='2' name="payment_method">
                                            </div>
                                        <?php } ?>
                                        <div class="agreement">
                                            <input type="checkbox" name="accept" value="" id="accept" required>
                                            <label  for="accept">I have read and accept the <a href="<?= base_url() ?>Privacy"><?= lang('term_and_cond') ?></a></label>
                                        </div><!-- /.agreement -->
                                        <input type="submit" class="btn" name="submit" value="Place your order">
                                    </form><!-- /.payment-form -->
                            </div><!-- /.payment-method -->
                        </div><!-- /.order_review -->
                    <?php } ?>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.section-padding -->
</section><!-- /.checkout-section -->
<?php $this->load->view('front/private/subscribe'); ?>
<?php $this->load->view('front/private/footer'); ?>
<?php $this->load->view('front/private/footer_js'); ?>
</body>
</html>


