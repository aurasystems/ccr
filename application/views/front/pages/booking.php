<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/jquery.timepicker.min.css">
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"rel = "stylesheet">
        <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
       <style>
.ui-datepicker {
    width: 300px;
    height: 300px;
    margin: 5px auto 0;
    font: 12pt Arial, sans-serif;
}

.ui-datepicker table {
        width: 100%;
}

.ui-datepicker-header {
    background: lightgray;
    color: lightgray;
    font-family:'Times New Roman';
    border-width: 0 0 0 0;
    border-style: none;
    border-color: #111;
}

.ui-datepicker-title {
    text-align: center;
    font-size: 15px;
}

.ui-datepicker-prev {
    float: left;
    cursor: pointer;
    background-position: center -30px;
}

.ui-datepicker-next {
    float: right;
    cursor: pointer;
    background-position: center 0px;
}

.ui-datepicker thead {
    background-color: #f7f7f7;
}

.ui-datepicker th {
    background-color:#808080;
    text-transform: uppercase;
    font-size: 8pt;
    color: #666666;
}

.ui-datepicker td span, .ui-datepicker td a {
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 30px;
    line-height: 30px;
    color: #ffffff;
}

.ui-datepicker-calendar .ui-state-default {
      background: linear-gradient(#999999, #737373);
      color:#ffffff;
      height:40px;
      width:40px;
}

.ui-datepicker-calendar .ui-state-hover {
    background: #33adff;
    color: #FFFFFF;
}

.ui-datepicker-calendar .ui-state-active {
    background: #33adff;
    -webkit-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
    -moz-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
    box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
    color: #e0e0e0;
    text-shadow: 0px 1px 0px #4d7a85;
    border: 1px solid #55838f;
    position: relative;
    margin: -1px;
}
#ui-datepicker-div{
    z-index:2000 !important;
}

.ui-datepicker-unselectable .ui-state-default {
    background: lightgray;
    color: #000;
}
       </style>
    </head>
    <body>
        <?php $this->load->view('front/private/header'); ?>
        <section class="page-name-sec page-name-sec-01">
            <div class="section-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="" style="">
                                <?php if ($reserve == true) { ?>
                                    <form method="POST" enctype="Multipart/form-data" action="<?= base_url() ?>Shop/add_waiting_list/<?= $product_id ?>/<?= $rent_term ?>" class="form-horizontal" role="form">
                                    <?php } ?>
                                    <?php if ($reserve == false) { ?>
                                        <form method="POST" enctype="Multipart/form-data" action="" class="form-horizontal" role="form">
                                        <?php } ?>
                                        <?= csrf() ?>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"><?= lang('quantity') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-2">
                                                <?php echo form_input(['type' => 'number', 'min' => 1, 'id' => 'quantity', 'style' => 'width:80px;', 'name' => 'quantity', 'class' => 'form-control', 'value' => 1, 'placeholder' => lang('quantity'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('quantity'); ?></span>
                                            </div>
                                        </div>
                                        <?php if (!empty($product) && $product->rent_term == 1) { // day ?>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label"><?= lang('b_from') ?> : <span class="text-red">*</span></label>
                                                <div class="col-sm-2">
                                                    <?php echo form_input(['id' => 'b_from', 'value' => date('Y-m-d'), 'type' => 'text', 'name' => 'b_from', 'style' => 'width:280px;', 'autocomplete' => 'off', 'class' => 'form-control datetimepicker datepicker', 'placeholder' => lang('b_from'), 'required' => 'required']); ?>
                                                    <span class="text-red"><?php echo form_error('b_from'); ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label"><?= lang('b_to') ?> : <span class="text-red">*</span></label>
                                                <div class="col-sm-2">
                                                    <?php echo form_input(['id' => 'b_to', 'value' => date('Y-m-d'), 'type' => 'text', 'name' => 'b_to', 'style' => 'width:280px;', 'autocomplete' => 'off', 'class' => 'form-control datetimepicker datepicker', 'placeholder' => lang('b_to'), 'required' => 'required']); ?>
                                                    <span class="text-red"><?php echo form_error('b_to'); ?></span>
                                                </div>
                                            </div>
                                        <?php } elseif (!empty($product) && $product->rent_term == 2) { // hour ?>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label"><?= lang('date') ?> : <span class="text-red">*</span></label>
                                                <div class="col-sm-2">
                                                    <?php echo form_input(['id' => 'date', 'type' => 'date', 'name' => 'date', 'style' => 'width:280px;', 'autocomplete' => 'off', 'class' => 'form-control date-picker', 'value' => set_value('date'), 'placeholder' => lang('date'), 'required' => 'required']); ?>
                                                    <span class="text-red"><?php echo form_error('date'); ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label"><?= lang('b_from') ?> : <span class="text-red">*</span></label>
                                                <div class="col-sm-2">
                                                    <?php echo form_input(['id' => 'start', 'type' => 'text', 'name' => 'b_time_from', 'style' => 'width:280px;', 'autocomplete' => 'off', 'class' => 'form-control', 'value' => set_value('b_from'), 'placeholder' => lang('b_from'), 'required' => 'required']); ?>
                                                    <span class="text-red"><?php echo form_error('b_time_from'); ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label"><?= lang('b_to') ?> : <span class="text-red">*</span></label>
                                                <div class="col-sm-2">
                                                    <?php echo form_input(['id' => 'end', 'type' => 'text', 'name' => 'b_time_to', 'style' => 'width:280px;', 'autocomplete' => 'off', 'class' => 'form-control', 'value' => set_value('b_to'), 'placeholder' => lang('b_to'), 'required' => 'required']); ?>
                                                    <span class="text-red"><?php echo form_error('b_time_to'); ?></span>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php if ($reserve == false) { ?>
                                            <div class="col-sm-12 text-center pull-right" style="width:80px;" >
                                                <p class="form-row form-row-last">
                                                    <input style="padding: 0 !important;" type="submit" class="button btn btn-red" value="<?= lang("save") ?>">
                                                </p>
                                            </div> 
                                        <?php } ?>                                               
                                        <?php if ($reserve == true) { ?>
                                            <div class="col-sm-5 text-center pull-right" style="width:80px;">
                                                <input style="padding: 0 !important;" type="submit" class="btn btn-primary " value="<?= lang("add_to_waiting_list") ?>">
                                            </div>
                                            <div class="col-sm-7 text-center pull-right" style="width:auto;">
                                                <input style="line-height: 3;" onclick="choose_another_date(<?= $product_id ?>)"  type="button" class="btn btn-primary " value="<?= lang("different_date") ?>">
                                            </div>
                                        <?php } ?>                                             
                                    </form>
                            </div><!-- /.sign-in -->
                        </div>
                    </div>
                </div><!-- /.row -->

            </div><!--/.container-->
        </div><!-- /.section-padding -->
    </section><!--/.login-section-->
    <?php $this->load->view('front/private/subscribe'); ?>
    <?php $this->load->view('front/private/footer'); ?>
    <?php $this->load->view('front/private/footer_js'); ?>
    <script>
        function waiting_list(product_id, b_from, b_to)
        {
            window.location = "<?= base_url() ?>Shop/add_waiting_list/" + product_id + '/' + b_from + '/' + b_to;

        }
        function choose_another_date(product_id)
        {
            window.location = "<?= base_url() ?>Shop/check_availabilty/" + product_id;

        }
    </script>
    <script src="<?= base_url() ?>assets/js/jquery.timepicker.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#start, #end').timepicker({
                timeFormat: 'h:mm p',
                interval: 30,
                startTime: '10:00',
                dynamic: true,
                dropdown: true,
                scrollbar: true,
                onSelect: function (dateText, inst) {
                jQuery("#datepickerdepone").datepicker('option', "minDate", dateText); 
            }
            });
        });
    </script>

<script>

$("#b_from").datepicker({
    onSelect: function() {
        var minDate = $('#b_from').datepicker('getDate');
        $("#b_to").datepicker("change", { minDate: minDate });
    }
});

$("#b_to").datepicker({
    onSelect: function() {
        var maxDate = $('#b_to').datepicker('getDate');
        $("#b_from").datepicker("change", { maxDate: maxDate });
    },
    orientation: "bottom",
});
          
      </script>
</body>
</html>