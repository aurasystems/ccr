<!doctype html>
<head>
    <?php $this->load->view('front/private/head'); ?>
    <link rel="stylesheet" href="assets/css/pages/about-01.css">
</head>
<body>
    <?php $this->load->view('front/private/header'); ?>
    <section class="page-name-sec page-name-sec-01">
        <div class="section-padding">
            <div class="container">
                <h3 class="page-title"><?= $title ?></h3><!-- /.page-title -->

                <div class="row">
                    <div class="col-sm-7">
                        <p class="description">

                        </p><!-- /.description -->
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.page-name-sec -->



    <section class="intro">
        <div class="section-padding">
            <div class="container">
                <?= !empty($about) ? $about->content : '' ?>
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.intro -->

    <?php $this->load->view('front/private/subscribe'); ?>
    <?php $this->load->view('front/private/footer'); ?>
    <?php $this->load->view('front/private/footer_js'); ?>
</body>
</html>
