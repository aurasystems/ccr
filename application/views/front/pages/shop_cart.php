<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
    </head>
<body>
<?php $this->load->view('front/private/header'); ?>          
  <section class="page-name-sec page-name-sec-01">
    <div class="section-padding">
      <div class="container">
        <h3 class="page-title">Shopping Cart</h3><!-- /.page-title -->
        <div class="row">
          <div class="col-sm-7">
            <p class="description">
                
            </p><!-- /.description -->
          </div>
        </div><!-- /.row -->
      </div><!-- /.container -->
    </div><!-- /.section-padding -->
  </section><!-- /.page-name-sec -->
  <section class="cart-section">
    <div class="section-padding">
      <div class="container">
        <div class="per-order-items">
        <?= flash_success() ?>
        <?= flash_error() ?>
        <div class="alert alert-danger" role="alert" style="display:none;" id="booking-error"> 
            <i class="icon ti-na"></i><strong  id="checkout-err"></strong>
          </div>
          <table class="cart-table">
            <tbody>
              <tr class="order-head">
                <th>PRODUCTS</th>
                <th>RENT PRICE</th>
                <th>QUANTITY</th>
                <th>SUBTOTAL</th>
              </tr>
              <tr id="detail_cart">
                <?php $subtotal = 0; foreach($items as $item){ 
                  $product = get_product_details($item->product_id);?>
                <td class="order-item">
                  <button onclick="delete_item('<?= $item->id ?>','<?=$product->id?>')" class="del"><i class="ti-trash"></i></button>
                  <div class="item-thumbnail">
                    <img src="<?=base_url()?>/uploads/thumb/<?= $product->img ?>" alt="images">
                  </div><!-- /.item-thumbnail -->
                  <div class="item-details">
                    <h3 class="item-title"><?= $product->n_en ?></h3>
                    <div class="product-meta">
                    </div><!-- /.product-meta -->
                  </div><!-- /.item-details -->
                </td>
                <td class="unit-price"><span class="currency">$</span><span class="price"><?= $product->rent_price ?></span></td>
                <?php $quant = get_quantity(cid(), $item->id)?>
                <td class="order-count">
                  <div class="cart-counter">
                    <button onclick='decrement("<?=$item->id?>","<?=$product->id?>","<?=$product->rent_price?>")' class="item-minus"><i class="ti-minus"></i></button>
                    <span class="item-count" id="number<?=$product->id?>"><?= $quant?></span>
                    <button onclick='increment("<?=$item->id?>","<?=$product->id?>","<?=$product->rent_price?>")' class="item-plus"><i class="ti-plus"></i></button>
                  </div><!-- /.cart-counter -->
                </td>
                <td class="total-price"><span class="currency">$</span><span class="price" id="total-perQuant<?=$product->id?>"><?= $quant*$product->rent_price ?></span></td>
              </tr>
                <?php $subtotal += $quant * $product->rent_price; }?>
            </tbody>
          </table><!-- /.cart-table -->
          <div class="cart-buttons">
            <button onclick="delete_item(null, -1)" class="clear-cart">Clear shopping cart</button>
            <button  onclick="go_shop()" class="continue pull-right">Continue shopping</button>
          </div><!-- /.cart-buttons -->
        </div><!-- /.per-order-items -->
            <div class="billing-order col-md-4 col-sm-6 pull-right">
              <div class="order-cost">
                <ul>
                  <li>
                    <div class="bill-name">Sub-total</div>
                    <div class="amount"><span class="currency">$</span><span id="sub-total" class="count"><?php  echo $subtotal;?></span></div>
                  </li>
                  <li>
                    <div class="bill-name total">Order Total</div>
                    <div class="amount total"><span class="currency">$</span><span id="total_price" class="count"><?= $subtotal ?></span></div>
                  </li>
                </ul>
                <a class="btn" href="<?= base_url() ?>Checkout/checkBeforePayment/1">Proceed to checkout</a>
                
              </div><!-- /.order-cost -->
            </div><!-- /.billing-order -->
          </div><!-- /.row -->
        </div><!-- /.billing-table -->
      </div><!-- /.contaier -->
    </div><!-- /.section-padding -->
  </section><!-- /.cart-section -->
  <?php $this->load->view('front/private/subscribe'); ?>
<?php $this->load->view('front/private/footer'); ?>
<?php $this->load->view('front/private/footer_js'); ?>


<script>

      function increment(id, product_id, rent_prict)
            {
            var postData = {
                      cart_id: id,
                      product_id: product_id,
                      <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                    };
            $.ajax({
                url : "<?= base_url()?>Shop/increment",
                method : "POST",
                dataType: "json",
                data : postData,
                success: function(data){
                  $('#number'+product_id).html(data.count);
                  $('#sub-total').html(data.sub_total);
                  $('#total_price').html(data.sub_total+30);
                  $('#price_total').html(data.sub_total);
                  $('#booking-error').css('display','none');

                  if(data.not_ava == false)
                  {
                  $('#total-perQuant'+product_id).html(data.count*rent_prict);
                  }
                  if(data.not_ava == true)
                  {
                  $('#booking-error').css('display','block');
                  $('#checkout-err').html(data.msg);
                  }
                
                }
              });
                        
            }

            function decrement(id, product_id,rent_prict)
            {
                
            var postData = {
                     cart_id: id,
                     product_id: product_id,
                     <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                   };
           $.ajax({
               url : "<?= base_url()?>Shop/decrement",
               method : "POST",
               dataType: "json",
               data : postData,
               success: function(data){
                 $('#number'+product_id).html(data.count);
                 $('#sub-total').html(data.sub_total);
                 $('#total-perQuant'+product_id).html(data.count*rent_prict);
                 $('#total_price').html(data.sub_total+30);
                 $('#price_total').html(data.sub_total);
                 $('#booking-error').css('display','none');
               }
             });
                        
            }
            function go_shop()
            {
              window.location = "<?= base_url() ?>Shop";
            }
        
</script>

</body>
</html>