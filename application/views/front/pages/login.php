<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/pages/register.css">
    </head>
    <body>
        <?php $this->load->view('front/private/header'); ?>
        <section class="page-name-sec page-name-sec-01">
            <div class="section-padding">
                <div class="container">
                    <h3 class="page-title"><?= lang('login') ?></h3><!-- /.page-title -->

                    <div class="row">
                        <div class="col-sm-7">
                            <p class="description">
                            </p><!-- /.description -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.page-name-sec -->
        <section class="login-section">
            <div class="section-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="sign-in bg-gray">
                                <h2 class="title"><?= lang('have_account') ?></h2>
                                <form class="sign-in-form" id="sign-in-form" action="<?= base_url('Login/do_login') ?>" method="post">
                                    <?= csrf() ?>
                                    <div class="form-group">
                                        <input type="email" name="email" id="user_login" class="input" value="" placeholder="<?= lang('email') ?>" required/>
                                        <?php echo form_error('email'); ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" id="user_pass" class="input" value="" placeholder="<?= lang('password') ?>" required/>
                                        <?php echo form_error('password'); ?>
                                    </div>
                                    <p class="form-input">
                                        <input type="submit" class="btn" value="<?= lang('sign_in') ?>" />
                                    </p>
                                    <p class="checkbox pull-left">
                                        <a href="<?= base_url() ?>Login/reset_password" class="pull-right" title="<?= lang('recover_your_pass') ?>"><?= lang('forgot_pass') ?></a>
                                    </p>
                                </form>
                            </div><!-- /.sign-in -->
                        </div>
                    </div>
                </div><!-- /.row -->
            </div><!--/.container-->
        </div><!-- /.section-padding -->
    </section><!--/.login-section-->
    <?php $this->load->view('front/private/subscribe'); ?>
    <?php $this->load->view('front/private/footer'); ?>
    <?php $this->load->view('front/private/footer_js'); ?>
</body>
</html>