<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
        <link rel="stylesheet" href="<?= base_url()?>assets/css/shop/shop-details.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/pages/register.css">
    </head>
    <body>
        <?php $this->load->view('front/private/header'); ?>
        <?php $item = get_record('id', $product_id, 'products');
        $cat_n = get_cat_name($item[0]->id);
        ?>
        <section class="page-name-sec page-name-sec-01">
            <div class="section-padding">
                <div class="container">
                    <h3 class="page-title"><?= lang('shop_details') ?></h3><!-- /.page-title -->

                    <div class="row">
                        <div class="col-sm-7">
                            <p class="description">
                                
                            </p><!-- /.description -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.page-name-sec -->
        <div id="success_alrt"><?= flash_success() ?></div>
        <?= flash_error() ?>
        <section class="shop-contents">
            <div class="section-padding">
                <div class="container">
                    <div class="product-details">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="item-gallery vertical">                    
                                    <div class="tabs">
                                        <div role="tabpanel" class="tabpanel">
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane in active" id="item1">
                                                    <img src="<?= base_url() ?>/uploads/products/<?= $item[0]->img ?>" alt="Product Image"> 
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="item2">
                                                    <img src="<?= base_url() ?>/uploads/products/<?= $item[0]->img ?>" alt="Product Image"> 
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="item3">
                                                    <img src="<?= base_url() ?>/uploads/products/<?= $item[0]->img ?>" alt="Product Image"> 
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="item4">
                                                    <img src="<?= base_url() ?>/uploads/products/<?= $item[0]->img ?>" alt="Product Image"> 
                                                </div>
                                            </div><!-- /.tab-content -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#item1" aria-controls="item1" role="tab" data-toggle="tab" aria-expanded="true"><img src="<?= base_url() ?>/uploads/products/<?= $item[0]->img ?>" alt="Product Image"></a></li>
                                                <li role="presentation" class=""><a href="#item2" aria-controls="item2" role="tab" data-toggle="tab" aria-expanded="false"><img src="<?= base_url() ?>/uploads/products/<?= $item[0]->img ?>" alt="Product Image"></a></li>
                                                <li role="presentation" class=""><a href="#item3" aria-controls="item3" role="tab" data-toggle="tab" aria-expanded="true"><img src="<?= base_url() ?>/uploads/products/<?= $item[0]->img ?>" alt="Product Image"></a></li>
                                                <li role="presentation" class=""><a href="#item4" aria-controls="item4" role="tab" data-toggle="tab" aria-expanded="true"><img src="<?= base_url() ?>/uploads/products/<?= $item[0]->img ?>" alt="Product Image"></a></li>
                                            </ul><!-- /.nav-tabs -->

                                        </div>
                                    </div><!-- /.item-gallery -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="about-product">
                                    <h3 class="item-title"><?= $item[0]->n_en ?></h3>
                                    <div class="top-meta">
                                        <a href="#rev_count"><?php $rev_count = get_reviews_total($item[0]->id); echo $rev_count;?> reviews
                                        <?php if(cid()!= '') { ?>
                                        <a href="#review">Write a review</a>
                                        <?php } ?>
                                    </div><!-- /.top-meta -->
                                    <div class="item-price">
                                        <div class="current-price"><span class="currency">EGP</span><span class="price"><?=$item[0]->rent_price?></span></div><!-- /.current-price -->
<!--     
                                        <div class="previous-price"><span class="currency">$</span><span class="price">79.00</span></div>
 -->                                    </div><!-- /.item-price -->
                              <!--       <div class="alert alert-danger" role="alert" style="display:none;" id="booking-error"> 
                                        <i class="icon ti-na"></i><strong  id="checkout-err"></strong>
                                    </div>
                                    <div class="cart-counter">
                                    <button onclick='increment("<?=$item[0]->id?>","<?=$item[0]->rent_price?>")' class="item-plus"><i class="ti-plus"></i></button>
                                    <span class="item-count" id="number<?=$item[0]->id?>">1</span>
                                        <button onclick='decrement("<?=$item[0]->id?>","<?=$item[0]->rent_price?>")' class="item-minus"><i class="ti-minus"></i></button>
                                    </div>-->

                                   <!--  <p class="description">
                                        <?= $item[0]->description ?>
                                    </p> /.short-description --> 
                                    <?php if ((cid() != "")) { ?>
                                    <div class="buttons">
                                    <button type="submit" onclick="start_end_date(<?=$item[0]->id?>)">Add to cart<i class="fa fa-shopping-cart"></i></button>
                                    </div>
                                    <?php } ?>
                                    <div class="product-meta">
                                        <span class="meta-id"><?=lang("category")?> : <span class="meta-about"><a href="#"><?= $cat_n ?></a></span></span>
                                        <!-- <span class="meta-id">Tags : <span class="meta-about"><a href="#">Mockup</a>,<a href="#">Design</a>,<a href="#">Photoshop</a></span></span> -->
                                    </div><!-- /.product-meta -->
                                </div><!-- /.about-product -->
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="product-tabs" id="rev_count">
                            <div class="tabs">
                                <div role="tabpanel">

                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class=""><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab" aria-expanded="true"><?=lang("desc")?></a></li>
                                        <li role="presentation" class="active"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab" aria-expanded="false"><?=lang("review")?> (<span class="count"><?=$rev_count?></span>)</a></li>
                                        <!-- <li hidden role="presentation" class=""><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab" aria-expanded="true"><?=lang("tags")?> (<span class="count"><?=$rev_count?></span>)</a></li> -->
                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade" id="tab1">
                                            <p class="description">
                                                <strong><?=$item[0]->description?></strong>
                                                <br><br>
                                              </p><!-- /.description -->
                                        </div><!-- /.tab-pane -->

                                        <div role="tabpanel" class="tab-pane fade in active" id="tab2">
                                        <?php
                                        $reviews = get_reviews($item[0]->id);
                                        $n = 1;
                                        if(!empty($reviews)){
                                         foreach($reviews as $one){
                                             if($n % 2 != 0) {?>
                                        
                                            <div class="review parent media">
                                                <div hidden class="author-avatar media-left">
                                                    <img src="images/shop/avatar/1.jpg" alt="Author Avatar">
                                                </div><!-- /.author-avatar -->
                                            
                                                <div class="review-top media-body">
                                                    <h4 class="author-name">
                                                        <a href="#"><?=$one->{'n_' . lc()}?></a>
                                                    </h4><!-- /.author-name -->

                                                    <div class="meta-info">
                                                        <span>
                                                            <time datetime="2016-06-06 21:33"><?=$one->timestamp?></time>
                                                        </span>
                                                    </div><!-- /.meta-info -->
                                                    <?php if(cid() != ''){?>
                                                    <div class="rating">
                                                        <input type="hidden"  value="<?=$one->rating?>" class="rating-tooltip-manual" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="5"/>
                                                    </div><!-- /.rating -->
                                                    <?php }?>
                                                </div><!-- /.review-top -->
                                                <p class="description">
                                                <?=$one->description?>
                                                </p> 
                                        <?php $n++;} else if($n % 2 == 0){?>
                                                <div class="review children media">
                                                    <div hidden class="author-avatar media-left">
                                                        <img src="images/shop/avatar/2.jpg" alt="Author Avatar">
                                                    </div><!-- /.author-avatar -->

                                                    <div class="review-top media-body">
                                                        <h4 class="author-name">
                                                        <a href="#"><?=$one->{'n_' . lc()}?></a>
                                                        </h4><!-- /.author-name -->

                                                        <div class="meta-info">
                                                            <span>
                                                            <time datetime="2016-06-06 21:33"><?=$one->timestamp?></time>
                                                            </span>
                                                        </div><!-- /.meta-info -->

                                                        <div class="rating">
                                                        <input type="hidden" value="<?=$one->rating?>" class="rating-tooltip-manual" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="5"/>
                                                        </div><!-- /.rating -->
                                                    </div><!-- /.review-top -->
                                                    <p class="description">
                                                    <?=$one->description?>
                                                    </p> 
                                                </div><!-- /.review -->
                                        <?php $n++;}
                                        }
                                    }?>
                                            </div><!-- /.review -->
                                        </div><!-- /.tab-pane -->

                                        <div role="tabpanel" class="tab-pane fade" id="tab3">

                                        </div><!-- /.tab-pane -->
                                    </div><!-- /.tab-content -->
                                </div><!-- /.tab-panel -->
                            </div><!-- /.tabs -->
                        </div><!-- /.product-tabs --> 
                    </div><!-- /.product-details -->
                    <?php if(cid()!= '') { ?>
                    <div id="review" class="add-review">
                        <h3 class="title"><?=lang("add_review")?></h3><!-- /.title -->
                        <div class="review-form">
                            <form action="<?=base_url()?>/Shop/add_review/<?=$item[0]->id?>" method="post" class="wpcf7-form">  
                            <?= csrf() ?>                                 
                                <p>
                                    <span class="wpcf7-form-control-wrap">
                                        <label for="message"><?=lang("ur_rev")?>*</label>
                                        <span class="rating">
                                            <label><?=lang("ur_rating")?></label>
                                            <input type="hidden" name="rating" class="rating-tooltip-manual" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="5"/>
                                        </span><!-- /.rating -->
                                        <textarea id="message" name="message" class="wpcf7-form-control" required ></textarea>
                                    </span>
                                </p>
                                <p class="choose-table-form"> 
                                    <input type="submit" value="Submit Now" class="wpcf7-form-control wpcf7-submit" /> 
                                </p>
                            </form>
                        </div><!-- /.review-form -->
                    <?php } ?>
                    </div><!-- /.add-review -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.shop-contents -->
        <section class="related-products text-center">
            <div class="section-padding">
                <div class="container">
                    <div class="section-top text-center">
                        <h2 class="section-title"><?=lang("related_prod")?> <span></span></h2>
                    </div><!-- /.section-top -->
                    <div id="related-slider" class="related-slider">
                    <?php $related_prod = get_related_products($item[0]->id);
                    if(!empty($related_prod)){
                            foreach($related_prod as $product){?>
                        <div class="item">
                        <a class="fancybox" href="<?= base_url() ?>uploads/products/<?= $product->img?>">
                                <div class="item-thumbnail">
                                <img  src="<?= base_url() ?>uploads/thumb/<?= $product->img?>" alt="<?= $product->img ?>">
                                    <span hidden class="ribbon sale">Sale</span>
                                </div>
                            </a>
                            <div class="item-details">
                                <h3 class="item-title tit"><a href="#"><?=$product->{'n_' . lc()}?></a></h3><!-- /.item-title -->
                                <div class="item-price">
                                    <span class="currency">EGP&nbsp;</span><!-- /.currency -->
                                    <span class="price"><?=$product->rent_price?></span><!-- /.price -->
                                </div><!-- /.item-price -->
                                <?php if(cid()!=''){ ?>
                                <a onclick ="start_end_date(<?=$item[0]->id?>)" class="btn">Add to cart</a><!-- /.btn -->
                                <?php } ?>
                            </div>
                        </div>
                            <?php }
                            }?>
                    </div><!-- /#related-slider -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.related-products -->
        <?php $this->load->view('front/private/subscribe'); ?>
        <?php $this->load->view('front/private/footer'); ?>
        <?php $this->load->view('front/private/footer_js'); ?>
        <script>
                    setTimeout(function () {                
                            $('#success_alrt').css('display', 'none');
                    }, 5000);

        </script>
        
    </body>

</html>