<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/pages/register.css">
    </head>
    <body class="demo-page">
        <?php $this->load->view('front/private/header'); ?>
        <section class="page-name-sec page-name-sec-01">
            <div class="section-padding">
                <div class="container">
                    <h3 class="page-title"><?= lang('register_login') ?></h3><!-- /.page-title -->

                    <div class="row">
                        <div class="col-sm-7">
                            <p class="description">
                                
                            </p><!-- /.description -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.page-name-sec -->
        <section class="login-section">
            <div class="section-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="sign-in bg-gray sign-up billing-table">
                                <h2 class="title"><?= lang('create_new_account') ?></h2>
                                <form class="sign-in-form" id="sign-in-form" action="<?= base_url('Register/validation') ?>" method="post">
                                    <?= csrf() ?>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="n_en" class="form-control" value="<?= set_value('n_en') ?>" placeholder="<?= lang("name_in") . lang("lang_en") . lang('*') ?>" required>
                                                <?php echo form_error('n_en'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="n_ar" class="form-control" value="<?= set_value('n_ar') ?>" placeholder="<?= lang("name_in") . lang("lang_ar") . lang('*') ?>" required>
                                                <?php echo form_error('n_ar'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="phone" class="form-control" value="<?= set_value('phone') ?>" placeholder="<?= lang('phone') . lang('*') ?>" required>
                                                <?php echo form_error('phone'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="email" name="email" class="form-control" value="<?= set_value('email') ?>" placeholder="<?= lang('email') . lang('*') ?>" required autocomplete="off">
                                                <?php echo form_error('email'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="national_id" class="form-control" value="<?= set_value('national_id') ?>" placeholder="<?= lang('national_id') . lang('*') ?>" required>
                                                <?php echo form_error('national_id'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="address_en" class="form-control" value="<?= set_value('address_en') ?>" placeholder="<?= lang('address_en') . lang('*') ?>" required>
                                                <?php echo form_error('address_en'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="address_ar" class="form-control" value="<?= set_value('address_ar') ?>" placeholder="<?= lang('address_ar') . lang('*') ?>" required>
                                                <?php echo form_error('address_ar'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select name="gender" class="form-input">
                                                    <option value=""><?= lang('gender') ?></option>
                                                    <option value="1"><?= lang('male') ?></option>
                                                    <option value="2"><?= lang('female') ?></option>
                                                </select>
                                                <?php echo form_error('gender'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="password" name="password" class="form-control" placeholder="<?= lang('password') . lang('*') ?>" required>
                                                <?php echo form_error('password'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="password" name="confirm_password" class="form-control" placeholder="<?= lang('confirm_password') . lang('*') ?>" required>
                                                <?php echo form_error('confirm_password'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select id="type" name="type" class="form-input">
                                                    <option value=""> <?= lang('type') ?></option>
                                                    <option value="1"><?= lang('Corporation') ?></option>
                                                    <option value="2"><?= lang('indiv') ?></option>
                                                </select>
                                                <?php echo form_error('type'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6" id="corp_name_div" style="display: none;">
                                            <div class="form-group">
                                                <input id="corp_name" name="corp_n" type="text" class="form-control" placeholder="<?= lang('please_ntr_corp_n') ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <p class="checkbox pull-left">
                                        <input name="term_cond" type="checkbox" value="1"/> 
                                        I have read and accept the <a href="<?=base_url()?>Privacy"> <?= lang('term_and_cond') ?></a>
                                    </p>
                                    <p class="form-input">
                                        <input type="submit" class="btn" value="<?= lang('sign_up') ?>" />
                                    </p>
                                </form>
                            </div><!-- /.sign-in -->
                        </div>
                    </div>
                </div><!-- /.row -->
            </div><!--/.container-->
        </div><!-- /.section-padding -->
    </section><!--/.login-section-->
    <?php $this->load->view('front/private/subscribe'); ?>
    <?php $this->load->view('front/private/footer'); ?>
    <?php $this->load->view('front/private/footer_js'); ?>
    <script>
        $(document).ready(function(){
            $("#corp_name_div").hide();
        });
        $("#type").change(function () {
            var value = $(this).val();
            if (value == 1) {
                $("#corp_name_div").show();
            } else {
                $("#corp_name_div").hide();
            }
        });
    </script>
</body>
</html>