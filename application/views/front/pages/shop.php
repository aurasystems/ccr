<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
        <link rel="stylesheet" href="assets/css/shop/shop.css">
    </head>
    <body class="shop-no-sidebar">
        <?php $this->load->view('front/private/header'); ?>
        <section class="page-name-sec page-name-sec-01">
            <div class="section-padding">
                <div class="container">
                    <h3 class="page-title"><?= lang('shop') ?></h3><!-- /.page-title -->

                    <div class="row">
                        <div class="col-sm-6">
                            <p class="description">
                                
                            </p><!-- /.description -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.page-name-sec -->
        <section class="shop-contents">
            <div class="section-padding">
                <div class="container">
                    <div class="shop-products">
                        <div class="row">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in text-center" id="grid">
                                    <?php
                                    //$products = get_products();
                                    if (!empty($products)) {
                                        foreach ($products as $product) {
                                            ?>
                                            <div class="col-sm-3">
                                                <div class="item">
                                                    <?php
                                                    if(!empty($product->img)) {
                                                        $image = 'uploads/products/'.$product->img;
                                                        $thumb = 'uploads/thumb/'.$product->img;
                                                    }
                                                    else {
                                                        $image = 'images/p_default.jpg';
                                                        $thumb = 'images/p_default.jpg';
                                                    }
                                                    ?>
                                                    <div class="item-thumbnail">
                                                        <a class="fancybox" href="<?= base_url().$image ?>">
                                                            <img  src="<?= base_url().$thumb ?>" alt="<?= $thumb ?>">
                                                        </a>
                                                    </div><!-- /.item-thumbnail -->
                                                    <div class="item-content">
                                                        <?php if ((cid() != "")) { ?>
                                                            <div class="buttons">
                                                                <button type="submit" onclick="start_end_date(<?= $product->id ?>)">Add to cart<i class="fa fa-shopping-cart"></i></button>
                                                            </div><!-- /.buttons -->
                                                        <?php } ?>
                                                        <h3 class="item-title"><a href="<?= base_url() ?>Shop/item_details/<?= $product->id ?>"><?= $product->n_en ?></a></h3><!-- /.item-title -->
                                                        <div class="item-price">
                                                            <span class="currency">EGP&nbsp;</span>
                                                            <span class="price"><?= $product->rent_price ?></span>
                                                        </div><!-- /.item-price -->
                                                            <?php if(cid() != '')
                                                            {?>
                                                            <div class="rating"><input type="hidden" id="product_rating<?=$product->id?>" value="<?=get_rating($product->id)?>"  onchange="update_rating(<?=$product->id?>)" class="rating-tooltip-manual" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="5"/></div><!-- /.rating -->
                                                            <?php }?>
                                                    </div><!-- /.item-content -->
                                                </div><!-- /.item -->
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div><!-- /.tab-pane -->
                            </div><!-- /.tab-content -->
                        </div><!-- /.row -->
                    </div><!-- /.shop-products -->

                    <div class="pagination pagination-02 text-center">
                        <?= $links ?>
                    </div><!-- /.pagination -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.shop-contents -->
        <?php $this->load->view('front/private/subscribe'); ?>
        <?php $this->load->view('front/private/footer'); ?>
        <?php $this->load->view('front/private/footer_js'); ?>
        <script type="text/javascript">
            $(document).ready(function ($) {
                "use strict";
                $('.rating-tooltip-manual').rating({
                    extendSymbol: function () {
                        var title;
                        $(this).tooltip({
                            container: 'body',
                            placement: 'bottom',
                            trigger: 'manual',
                            title: function () {
                                return title;
                            }
                        });
                        $(this).on('rating.rateenter', function (e, rate) {
                            title = rate;
                            $(this).tooltip('show');
                        })
                                .on('rating.rateleave', function () {
                                    $(this).tooltip('hide');
                                });
                    }
                });

                /*-------- Filter By Price -----------*/

                $("#slider-range").slider({
                    range: true,
                    min: 0,
                    max: 500,
                    values: [75, 300],
                    slide: function (event, ui) {
                        $("#amount").val("$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ]);
                    }
                });
                $("#amount").val("$" + $("#slider-range").slider("values", 0) +
                        " - $" + $("#slider-range").slider("values", 1));

            });
        </script>
    </body>
</html>