<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/pages/register.css">
    </head>
    <body>
        <?php $this->load->view('front/private/header'); ?>
        <section class="page-name-sec page-name-sec-01">
            <div class="section-padding">
                <div class="container">
                    <h3 class="page-title"><?= (!empty($title)) ? $title : '' ?></h3><!-- /.page-title -->

                    <div class="row">
                        <div class="col-sm-7">
                            <p class="description">
                            </p><!-- /.description -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.page-name-sec -->
        <section class="login-section">
            <div class="section-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="sign-in bg-gray">
                                <h2 class="title"><?= (!empty($title)) ? $title : '' ?></h2>
                                <?php echo form_open(base_url("Login/reset_my_password/$token")); ?>
                                <div class="append-icon m-b-20">
                                    <?php echo form_input(['type' => 'password', 'id' => 'password', 'name' => 'password', 'class' => 'form-control form-white email', 'placeholder' => 'New Password', 'required' => 'required']); ?>
                                    <?php echo form_error('password'); ?>
                                </div>
                                <?php echo form_submit(['class' => 'btn btn-lg btn-danger btn-block ladda-button', 'data-style' => 'expand-left', 'value' => lang('lang_save')]); ?>
                                <p class="checkbox text-center">
                                    <a href="<?= base_url('Login') ?>"><?= lang('lang_back_to_login') ?></a>
                                </p>
                                <?php echo form_close(); ?>
                            </div><!-- /.sign-in -->
                        </div>
                    </div>
                </div><!-- /.row -->
            </div><!--/.container-->
        </div><!-- /.section-padding -->
    </section><!--/.login-section-->
    <?php $this->load->view('front/private/subscribe'); ?>
    <?php $this->load->view('front/private/footer'); ?>
    <?php $this->load->view('front/private/footer_js'); ?>
</body>
</html>