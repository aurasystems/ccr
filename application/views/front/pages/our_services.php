<!doctype html>
<head>
<?php $this->load->view('front/private/head'); ?>
<link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.carousel.css">
</head>
<body>
<?php $this->load->view('front/private/header'); ?>
  <section class="page-name-sec page-name-sec-01">
    <div class="section-padding">
      <div class="container">
        <h3 class="page-title">Our services</h3><!-- /.page-title -->

        <div class="row">
          <div class="col-sm-7">
            <p class="description">
                
            </p><!-- /.description -->
          </div>
        </div><!-- /.row -->
      </div><!-- /.container -->
    </div><!-- /.section-padding -->
  </section><!-- /.page-name-sec -->




  <section class="core-services text-center">
    <div class="section-padding">
      <div class="container">
        <div class="row">
          <div class="section-top">
            <h2 class="section-title">Core Services of Shopaholic<span></span></h2>
          </div><!-- /.section-top -->

          <p class="section-description">
            Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent 
          </p><!-- /.section-description -->

          <div class="section-details">
            <div class="col-md-4 col-sm-6">
              <div class="item media">
                <div class="item-icon media-left">
                  <span class="icon-tools"></span>
                </div><!-- /.item-icon -->
                <div class="item-details media-body">
                  <h3 class="item-title">Designing</h3><!-- /.item-title -->
                  <p class="description">
                    Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum
                  </p><!-- /.description -->
                </div><!-- /.item-details -->
              </div><!-- /.item -->
            </div>

            <div class="col-md-4 col-sm-6">
              <div class="item media">
                <div class="item-icon media-left">
                  <span class="icon-picture"></span>
                </div><!-- /.item-icon -->
                <div class="item-details media-body">
                  <h3 class="item-title">Photography</h3><!-- /.item-title -->
                  <p class="description">
                    Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum
                  </p><!-- /.description -->
                </div><!-- /.item-details -->
              </div><!-- /.item -->
            </div>

            <div class="col-md-4 col-sm-6">
              <div class="item media">
                <div class="item-icon media-left">
                  <span class="icon-beaker"></span>
                </div><!-- /.item-icon -->
                <div class="item-details media-body">
                  <h3 class="item-title">Development</h3><!-- /.item-title -->
                  <p class="description">
                    Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum
                  </p><!-- /.description -->
                </div><!-- /.item-details -->
              </div><!-- /.item -->
            </div>

            <div class="col-md-4 col-sm-6">
              <div class="item media">
                <div class="item-icon media-left">
                  <span class="icon-strategy"></span>
                </div><!-- /.item-icon -->
                <div class="item-details media-body">
                  <h3 class="item-title">Branding</h3><!-- /.item-title -->
                  <p class="description">
                    Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum
                  </p><!-- /.description -->
                </div><!-- /.item-details -->
              </div><!-- /.item -->
            </div>

            <div class="col-md-4 col-sm-6">
              <div class="item media">
                <div class="item-icon media-left">
                  <span class="icon-globe"></span>
                </div><!-- /.item-icon -->
                <div class="item-details media-body">
                  <h3 class="item-title">E-Commerce</h3><!-- /.item-title -->
                  <p class="description">
                    Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum
                  </p><!-- /.description -->
                </div><!-- /.item-details -->
              </div><!-- /.item -->
            </div>

            <div class="col-md-4 col-sm-6">
              <div class="item media">
                <div class="item-icon media-left">
                  <span class="icon-lifesaver"></span>
                </div><!-- /.item-icon -->
                <div class="item-details media-body">
                  <h3 class="item-title">24x7 Support</h3><!-- /.item-title -->
                  <p class="description">
                    Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum
                  </p><!-- /.description -->
                </div><!-- /.item-details -->
              </div><!-- /.item -->
            </div>

          </div><!-- /.section-details -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </div><!-- /.section-padding -->
  </section><!-- /.core-services -->
  



  <section class="more-services bg-gray">
    <div class="section-padding">
      <div class="container">
        <div class="item">
          <div class="col-sm-6">
            <div class="item-details">
              <h3 class="item-title">Strategies That Work</h3><!-- /.item-title -->
              <p class="description">
                This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
              </p><!-- /.description -->
              <a href="#" class="btn">Learn more</a>
            </div><!-- /.item-details -->
          </div>

          <div class="col-sm-6">
            <div class="item-image">
              <img src="images/service/1.jpg" alt="Service Image">
            </div><!-- /.item-image -->
          </div>
        </div><!-- /.item -->

        <div class="item">
          <div class="col-sm-6">
            <div class="item-image">
              <img src="images/service/2.jpg" alt="Service Image">
            </div><!-- /.item-image -->
          </div>

          <div class="col-sm-6">
            <div class="item-details">
              <h3 class="item-title">Reinventing excellence</h3><!-- /.item-title -->
              <p class="description">
                This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
              </p><!-- /.description -->
              <a href="#" class="btn">Learn more</a>
            </div><!-- /.item-details -->
          </div>
        </div><!-- /.item -->
      </div><!-- /.container -->
    </div><!-- /.section-padding -->
  </section><!-- /.more-services -->




  <section class="inspire parallax-style background-bg text-center" data-image-src="images/service/parallax.jpg">
    <div class="overlay">
      <div class="section-padding">
        <div class="container">
          <h3 class="sub-title">Credibly envisioneer cooperative leadership</h3><!-- /.sub-title -->
          <h2 class="main-title">Be Inspired & Start Work With Us</h2><!-- /.main-title -->
          <div class="buttons">
            <a href="#" class="btn">Buy theme</a>
            <a href="#" class="btn">Contact us</a>
          </div><!-- /.buttons -->
        </div><!-- /.container -->
      </div><!-- /.section-padding -->
    </div><!-- /.overlay -->
  </section><!-- /.inspire -->




  <section class="features features-12">
    <div class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src="images/service/feature.png" alt="Feature Image">
          </div>  
          <div class="col-md-6">
            <div class="features-details">
              <h3 class="title">We give you the endless possibilities to express yourself to the world</h3><!-- /.title -->
              <p class="description">
                This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
              </p><!-- /.description -->

              <div class="feature-items">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="item media">
                      <div class="item-icon media-left">
                        <span class="icon-tools"></span>
                      </div><!-- /.item-icon -->
                      <div class="item-details media-body">
                        <h3 class="item-title">Clean & modern design</h3><!-- /.item-title -->
                      </div><!-- /.item-details -->
                    </div><!-- /.item -->
                  </div>

                  <div class="col-sm-6">
                    <div class="item media">
                      <div class="item-icon media-left">
                        <span class="icon-mobile"></span>
                      </div><!-- /.item-icon -->
                      <div class="item-details media-body">
                        <h3 class="item-title">Unique & fully responsive</h3><!-- /.item-title -->
                      </div><!-- /.item-details -->
                    </div><!-- /.item -->
                  </div>

                  <div class="col-sm-6">
                    <div class="item media">
                      <div class="item-icon media-left">
                        <span class="icon-profile-male"></span>
                      </div><!-- /.item-icon -->
                      <div class="item-details media-body">
                        <h3 class="item-title">Supported by expert team</h3><!-- /.item-title -->
                      </div><!-- /.item-details -->
                    </div><!-- /.item -->
                  </div>

                  <div class="col-sm-6">
                    <div class="item media">
                      <div class="item-icon media-left">
                        <span class="icon-heart"></span>
                      </div><!-- /.item-icon -->
                      <div class="item-details media-body">
                        <h3 class="item-title">Designed with love</h3><!-- /.item-title -->
                      </div><!-- /.item-details -->
                    </div><!-- /.item -->
                  </div>
                </div><!-- /.row -->
              </div><!-- /.feature-items -->
            </div><!-- /.features-details -->
          </div>
        </div><!-- /.row -->
      </div><!-- /.container -->
    </div><!-- /.section-padding -->
  </section><!-- /.inspire -->





  <section class="testimonials testimonial-13 bg-gray">
    <div class="section-padding">
      <div class="container">
        <div id="testimonial-slider" class="testimonial-slider carousel slide">
          <ol class="carousel-indicators">
            <li data-target="#testimonial-slider" data-slide-to="0" class=""></li>
            <li data-target="#testimonial-slider" data-slide-to="1" class=""></li>
            <li data-target="#testimonial-slider" data-slide-to="2" class="active"></li>
          </ol>

          <div class="carousel-inner text-center">
            <div class="item active">
              <div class="client-avatar"><img src="images/service/avatar.jpg" alt="Client-avatar"></div><!-- /.client-avatar -->
              <p class="description">
                He seemed to brace himself for a great effort, and then started on the queerest rigmarole. I didn't get hold of it at first, and I had to stop and ask him questions
              </p><!-- /.description -->
              <div class="client-details">
                <span class="name">Randy Dixon</span><!-- /.name -->
                <span class="designation"><a href="#">www.google.com</a></span><!-- /.designation -->
              </div><!-- /.client-details -->
            </div><!-- /.item -->

            <div class="item">
              <div class="client-avatar"><img src="images/service/avatar.jpg" alt="Client-avatar"></div><!-- /.client-avatar -->
              <p class="description">
                He was an American and started to see the world. He wrote a bit, and acted as war correspondent for a Chicago paper, and spent a year or two in South-Eastern Europe.
              </p><!-- /.description -->
              <div class="client-details">
                <span class="name">Charles Woods</span><!-- /.name -->
                <span class="designation"><a href="#">www.google.com</a></span><!-- /.designation -->
              </div><!-- /.client-details -->
            </div><!-- /.item -->

            <div class="item">
              <div class="client-avatar"><img src="images/service/avatar.jpg" alt="Client-avatar"></div><!-- /.client-avatar -->
              <p class="description">
                He had played about with politics, he told me, at first for the interest of them, and then because he couldn't help himself. I read him as a sharp, restless fellow
              </p><!-- /.description -->
              <div class="client-details">
                <span class="name">Brenda Rogers</span><!-- /.name -->
                <span class="designation"><a href="#">www.google.com</a></span><!-- /.designation -->
              </div><!-- /.client-details -->
            </div><!-- /.item -->
          </div><!-- /.carousel-inner -->
        </div><!-- /.testimonial-slider -->

        <div class="clients-logo">
          <div class="col-sm-3">
            <div class="item bg-gray"><a href="#"><img src="images/brand/1.png" alt="Brand Logo"></a></div><!-- /.item -->
          </div>
          <div class="col-sm-3">
            <div class="item bg-gray"><a href="#"><img src="images/brand/2.png" alt="Brand Logo"></a></div><!-- /.item -->
          </div>
          <div class="col-sm-3">
            <div class="item bg-gray"><a href="#"><img src="images/brand/3.png" alt="Brand Logo"></a></div><!-- /.item -->
          </div>
          <div class="col-sm-3">
            <div class="item bg-gray"><a href="#"><img src="images/brand/4.png" alt="Brand Logo"></a></div><!-- /.item -->
          </div>
        </div><!-- /.client-logo -->

      </div><!-- /.container -->
    </div><!-- /.section-padding -->
  </section><!-- /.testimonials -->
  <?php $this->load->view('front/private/subscribe'); ?>
  <?php $this->load->view('front/private/footer'); ?>
  <?php $this->load->view('front/private/footer_js'); ?>
</body>
</html>