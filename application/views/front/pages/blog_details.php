<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
        <link rel="stylesheet" href="assets/css/pages/about-01.css">
    </head>
    <body class="blog page single classic">
        <?php $this->load->view('front/private/header'); ?>
        <section class="page-name-sec page-name-sec-01">
            <div class="section-padding">
                <div class="container">
                    <h3 class="page-title"><?= (!empty($blog->title)) ? $blog->title : '' ?></h3><!-- /.page-title -->

                    <div class="row">
                        <div class="col-sm-7">
                            <p class="description">
                                
                            </p><!-- /.description -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.page-name-sec -->

        <section class="blog-posts">
            <div class="section-padding">
                <div class="container">
                    <article class="post type-post format-standard">
                        <?php if (!empty($blog->img)) { ?>
                            <div class="entry-thumbnail">
                                <img src="<?= base_url() ?>uploads/blogs/<?= $blog->img ?>" alt="<?= lang('blog') ?>">
                            </div>
                        <?php } ?>
                        <div class="post-content media">
                            <div class="entry-meta media-left">
                                <time datetime="<?= (!empty($blog->create_time)) ? date('Y-m-d', strtotime($blog->create_time)) : '' ?>"><span class="date"><?= (!empty($blog->create_time)) ? date('d', strtotime($blog->create_time)) : '' ?></span> <span class="month"><?= (!empty($blog->create_time)) ? date('M', strtotime($blog->create_time)) : '' ?></span></time>
                            </div><!-- /.entry-meta -->

                            <div class="entry-content media-body">
                                <h2 class="entry-title">
                                    <?= (!empty($blog->title)) ? $blog->title : '' ?>
                                </h2><!-- /.entry-header -->

                                <div class="meta-info">
                                    <span class="author"><i class="ti-user"></i><a href="#"><?= (!empty($blog->uname)) ? $blog->uname : '' ?></a></span>
                                    <span class="cat-links"><i class="ti-tag"></i><?= (!empty($blog->category)) ? $blog->category : '' ?></span>
                                </div><!-- /.meta-info -->

                                <p class="description">
                                    <?= (!empty($blog->content)) ? $blog->content : '' ?>
                                </p>

                                <div class="post-bottom">
                                    <?php if (!empty($tags)) { ?>
                                        <div class="post-tag">
                                            <?php foreach ($tags as $tag) { ?>
                                                <a class="blog_tag" href="<?= base_url() ?>Blog/get_blog_by_tag/<?= $tag->tag ?>"><?= $tag->tag ?></a>
                                            <?php } ?>
                                        </div><!-- /.post-tag -->
                                    <?php } ?>
                                </div><!-- /.post-bottom -->
                                <div class="comments">
                                    <?php if (!empty(cid())) { ?>
                                        <h2 class="title"><?= lang('comments') ?>: <span class="count"><?= (!empty($count_comments)) ? $count_comments : 0 ?></span></h2>
                                        <ol class="comment-list">
                                            <?php
                                            if(!empty($comments)) {
                                                foreach($comments as $comment) {
                                            ?>
                                            <li class="comment parent media">
                                                <div class="comment-item">
                                                    <div class="author-avatar media-left">
                                                        <img src="<?= base_url() ?>images/blog/comment/1.jpg" alt="Comment Authors">
                                                    </div><!-- /.author-avatar -->
                                                    <div class="comment-body media-body">
                                                        <div class="comment-metadata">
                                                            <span class="author"><a href="#"><?= $comment->cname ?></a></span>
                                                            <span class="time"><time><?= date('Y-m-d h:i a', strtotime($comment->timestamp)) ?></time> </span>
                                                        </div><!-- /.comment-metadata -->
                                                    </div><!--/.comment-body-->
                                                    <p class="description">
                                                        <?= $comment->comment ?>
                                                    </p>
                                                </div><!-- /.comment-item -->
                                            </li>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </ol><!-- /.comment-list -->

                                        <div class="respond">
                                            <?= flash_success() ?>
                                            <?= flash_error() ?>
                                            <h2 class="title"><?= lang('add_your_comment') ?></h2>
                                            <form action="<?= base_url('Blog/add_comment/' . $blog_id) ?>" method="post" class="comment-form">
                                                <?= csrf() ?>
                                                <textarea id="comment" class="form-control" name="comment" placeholder="Comment*" rows="8" required></textarea>
                                                <span class="text-red"><?php echo form_error('comment'); ?></span>
                                                <input class="btn" type="submit" value="Submit Now">
                                            </form><!-- /.comment-form -->
                                        </div><!-- /.comment-respond -->
                                    <?php } else { ?>
                                        <span class="title"><a href="<?= base_url() ?>Login"><?= lang('login') ?></a><?= ' ' . lang('to_view_comm') ?></span>
                                        <?php } ?>
                                </div><!-- /.comment-area -->
                            </div><!-- /.entry-content -->        
                        </div><!-- /.post-content -->
                    </article><!--/.post-->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.blog-posts -->

        <?php $this->load->view('front/private/subscribe'); ?>
        <?php $this->load->view('front/private/footer'); ?>
        <?php $this->load->view('front/private/footer_js'); ?>
    </body>
</html>
