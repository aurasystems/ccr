<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="Balance icon" href="<?=base_url()?>assets/images/favicon.png" type="image/png">
        <title><?= MY_APP_NAME ?> | <?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="themes-lab" name="author" />
        <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/ui.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
    </head>
    <body class="account" data-page="login">
        <div class="container" id="login-block">
            <div class="row">
                        <div class="col-lg-12 ">
                            <div class="panel" style="margin-top: 100px;">
                                <div class="panel-content">
                                    <?php if ($this->session->flashdata('warning')) { ?>
                                        <div class="row">
                                            <div class="col-md-12">                                        
                                                <div class="alert alert-block alert-warning fade in text-center">
                                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                                    <?= $this->session->flashdata('warning') ?>                                                
                                                </div>                                       
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                                            <?php echo form_open('admin/Login/do_account_verification', $attributes); ?>
                                            <div class="form-group">
                                                <?php echo form_label($this->lang->line('barcode').': <span class="text-red">*</span>','barcode',$label_att); ?>
                                                <div class="col-sm-8">
                                                    <?php echo form_input(array('name' => 'barcode', 'class'=>'form-control','value' => set_value('barcode') , 'placeholder'=>'Barcode','required'=>'required')); ?>
                                                    <span class="text-red"><?php echo form_error('barcode'); ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <?php echo form_label($this->lang->line('national_id').': <span class="text-red">*</span>','national_id',$label_att); ?>
                                                <div class="col-sm-8">
                                                    <?php echo form_input(array('name' => 'national_id', 'class'=>'form-control','value' => set_value('national_id') , 'placeholder'=>'National ID','required'=>'required')); ?>
                                                    <span class="text-red"><?php echo form_error('national_id'); ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <?php echo form_label($this->lang->line('email').': <span class="text-red">*</span>','capacity',$label_att); ?>
                                                <div class="col-sm-8">
                                                    <?php echo form_input(array('name' => 'email', 'class'=>'form-control','value' => set_value('email') , 'placeholder'=>'Email','required'=>'required')); ?>
                                                    <span class="text-red"><?php echo form_error('email'); ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <?php echo form_label($this->lang->line('phone').': <span class="text-red">*</span>','phone',$label_att); ?>
                                                <div class="col-sm-8">
                                                    <?php echo form_input(array('name' => 'phone', 'class'=>'form-control','value' => set_value('phone') , 'placeholder'=>'Phone','required'=>'required')); ?>
                                                    <span class="text-red"><?php echo form_error('phone'); ?></span>
                                                </div>
                                            </div>
                                            <div class="row mrgn-top-5">
                                                <div class="col-sm-8 col-sm-offset-4">
                                                    <div class="pull-left">
                                                        <?php echo form_submit(array('type' => 'submit', 'value' => $this->lang->line('verify'), 'class'=>'btn btn-success')); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/gsap/main-gsap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/backstretch/backstretch.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.js"></script>
        <?php $this->load->view('admin/private/scripts/login_js'); ?>
    </body>
</html>