<!doctype html>
<head>
    <?php $this->load->view('front/private/head'); ?>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/pages/contact.css">
</head>
<body>
    <?php $this->load->view('front/private//header'); ?>
    <section class="page-name-sec page-name-sec-01">
        <div class="section-padding">
            <div class="container">
                <h3 class="page-title">Contact Us</h3><!-- /.page-title -->

                <div class="row">
                    <div class="col-sm-7">
                        <p class="description">
                            
                        </p><!-- /.description -->
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.page-name-sec -->
    <section class="contact-section text-center">
        <div class="section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="item">
                            <div class="item-icon"><span class="icon-phone"></span></div><!-- /.item-icon -->
                            <div class="item-details">
                                <h4 class="item-title"><?= lang('phone') ?></h4><!-- /.item-title -->
                                <span class="details">
                                    <?= (!empty($settings->phone)) ? $settings->phone : '' ?>
                                </span><!-- /.details -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="item">
                            <div class="item-icon"><span class="icon-map-pin"></span></div><!-- /.item-icon -->
                            <div class="item-details">
                                <h4 class="item-title"><?= lang('address') ?></h4><!-- /.item-title -->
                                <span class="details">
                                    <?= (!empty($settings->address)) ? $settings->address : '' ?>
                                </span><!-- /.details -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="item">
                            <div class="item-icon"><span class="icon-envelope"></span></div><!-- /.item-icon -->
                            <div class="item-details">
                                <h4 class="item-title"><?= lang('email') ?></h4><!-- /.item-title -->
                                <span class="details">
                                    <a href="<?= (!empty($settings->email)) ? 'mailto:' . $settings->email : '' ?>"><?= (!empty($settings)) ? $settings->email : '' ?></a>
                                </span><!-- /.details -->
                            </div>
                        </div>
                    </div>
                </div><!-- /.row -->

                <div class="section-padding">
                    <div class="section-top">
                        <h2 class="section-title"><?= lang('give_msg') ?><span></span></h2><!-- /.section-title -->
                    </div><!-- /.section-top -->

                    <p class="section-description">

                    </p><!-- /.section-description -->

                    <form action="<?= base_url() ?>Contact/send" method="post" class="wpcf7-form contact-form">
                        <?= csrf() ?>
                        <div class="contact-input-fields">
                            <p>
                                <span class="wpcf7-form-control-wrap">
                                    <label for="name">Your Name*</label>
                                    <input type="text" id="name" name="name" value="" class="wpcf7-form-control" required="">
                                </span>
                            </p>
                            <p>
                                <span class="wpcf7-form-control-wrap">
                                    <label for="email">Your Email*</label>
                                    <input type="email" id="email" name="email" value="" class="wpcf7-form-control" required="">
                                </span>
                            </p>
                            <p>
                                <span class="wpcf7-form-control-wrap">
                                    <label for="url">Your URL</label>
                                    <input type="url" id="url" name="url" value="" class="wpcf7-form-control">
                                </span>
                            </p>
                        </div><!-- /.contact-input-fields -->

                        <p>
                            <span class="wpcf7-form-control-wrap">
                                <label for="message">Your Message*</label>
                                <textarea id="message" name="message" class="wpcf7-form-control" required=""></textarea>
                            </span>
                        </p>

                        <p class="choose-table-form"> 
                            <input type="submit" value="Submit Now" class="wpcf7-form-control wpcf7-submit"> 
                        </p>
                    </form><!-- /.contact-form -->
                </div><!-- /.section-padding -->

                <div id="google-map">
                    <div class="map-container">
                        <div id="googleMaps" class="google-map-container"></div>
                    </div><!-- /.map-container -->
                </div><!-- /#google-map-->

            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.contact-section -->
    <?php $this->load->view('front/private/subscribe'); ?>
    <?php $this->load->view('front/private/footer'); ?>
    <?php $this->load->view('front/private/footer_js'); ?>

    <script type="text/javascript">

        jQuery(document).ready(function ($) {
            "use strict";


            /*----------- Google Map - with support of gmaps.js ----------------*/
            function isMobile() {
                return ('ontouchstart' in document.documentElement);
            }

            function init_gmap() {
                if (typeof google == 'undefined')
                    return;
                var options = {
                    center: {lat: -37.834812, lng: 144.963055},
                    zoom: 15,
                    mapTypeControl: true,
                    mapTypeControlOptions: {
                        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                    },
                    navigationControl: true,
                    scrollwheel: false,
                    streetViewControl: true,
                    styles: [{"featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{"color": "#444444"}]}, {"featureType": "landscape", "elementType": "all", "stylers": [{"color": "#f2f2f2"}]}, {"featureType": "poi", "elementType": "all", "stylers": [{"visibility": "off"}]}, {"featureType": "road", "elementType": "all", "stylers": [{"saturation": -100}, {"lightness": 45}]}, {"featureType": "road.highway", "elementType": "all", "stylers": [{"visibility": "simplified"}]}, {"featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"featureType": "transit", "elementType": "all", "stylers": [{"visibility": "off"}]}, {"featureType": "water", "elementType": "all", "stylers": [{"color": "#cdcdcd"}, {"visibility": "on"}]}]
                }

                if (isMobile()) {
                    options.draggable = false;
                }

                $('#googleMaps').gmap3({
                    map: {
                        options: options
                    },
                    marker: {
                        latLng: [-37.834812, 144.963055],
                        options: {icon: 'images/map-icon.png'}
                    }
                });
            }

            init_gmap();

        });
    </script>



</body>
</html>