<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
    </head>
    <body>
        <?php $this->load->view('front/private/header'); ?>
        <section class="page-name-sec page-name-sec-01">
            <div class="section-padding">
                <div class="container">
                    <h3 class="page-title"><?= $title ?></h3><!-- /.page-title -->

                    <div class="row">
                        <div class="col-sm-7">
                            <p class="description">

                            </p><!-- /.description -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.page-name-sec -->
        <section class="shop-contents">
            <div class="section-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 pull-right">
                            <div class="shop-products">
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in text-center" id="grid">
                                        <?php
                                        if (!empty($items)) {
                                            foreach ($items as $item) {
                                        ?>
                                            <div class="col-sm-4">
                                                <div class="item">
                                                    <?php if(!empty($item->img)) { ?>
                                                    <div class="item-thumbnail">
                                                        <a class="fancybox" href="<?= base_url() ?>uploads/products/<?= $item->img ?>">
                                                            <img  src="<?= base_url() ?>uploads/thumb/<?= $item->img ?>" alt="<?= $item->img ?>">
                                                        </a>
                                                    </div><!-- /.item-thumbnail -->
                                                    <?php } ?>
                                                    <div class="item-content">
                                                        <?php if (cid() != "") { ?>
                                                            <div class="buttons">
                                                                <button type="submit" onclick="start_end_date(<?= $item->id ?>)">Add to cart<i class="fa fa-shopping-cart"></i></button>
                                                            </div><!-- /.buttons -->
                                                        <?php } ?>
                                                        <h3 class="item-title"><a href="<?= base_url() ?>Shop/item_details/<?= $item->id ?>"><?= $item->n_en ?></a></h3><!-- /.item-title -->
                                                        <div class="item-price">
                                                            <span class="currency">EGP&nbsp;</span>
                                                            <span class="price"><?= $item->rent_price ?></span>
                                                        </div><!-- /.item-price -->
                                                        <?php if(cid() != ''){ ?>
                                                            <div class="rating"><input type="hidden" id="product_rating<?= $item->id ?>" value="<?=get_rating($item->id)?>"  onchange="update_rating(<?= $item->id ?>)" class="rating-tooltip-manual" data-filled="fa fa-star" data-empty="fa fa-star-o" data-fractions="5"/></div><!-- /.rating -->
                                                        <?php }?>
                                                    </div><!-- /.item-content -->
                                                </div><!-- /.item -->
                                            </div>
                                        <?php
                                            }
                                        }
                                        ?>

                                    </div><!-- /.tab-pane -->
                                </div><!-- /.tab-content -->
                            </div><!-- /.shop-products -->

                            <!-- /.pagination -->
                        </div>
                        <div class="col-md-4 clearfix">
                            <aside class="sidebar left-sidebar pull-left">
                                <div class="widget widget_brand_names">
                                    <h3 class="heading-title">Brands</h3><!-- /.heading-title -->
                                    <div class="widget-details">
                                        <?php
                                        $brands = get_brands();
                                        $all_count = brand_prod_count();
                                        ?>
                                        <ul>
                                            <?php
                                            if(!empty($brands)) {
                                                foreach ($brands as $brand) {
                                                    $total_count = brand_prod_count($brand->id);
                                            ?>
                                            <li><a href="<?= base_url() ?>Shop/brand/<?= $brand->id ?>"><?= $brand->n_en ?> <span class="count pull-right"><?= (!empty($total_count)) ? $total_count : 0 ?></span></a></li>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div><!-- /.widget-details -->
                                </div><!-- /.widget -->
                            </aside><!-- /.sidebar -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </section><!-- /.shop-contents -->
        <?php $this->load->view('front/private/subscribe'); ?>
        <?php $this->load->view('front/private/footer'); ?>
        <?php $this->load->view('front/private/footer_js'); ?>
        <script type="text/javascript">
            "use strict";
            $('.rating-tooltip-manual').rating({
                extendSymbol: function () {
                    var title;
                    $(this).tooltip({
                        container: 'body',
                        placement: 'bottom',
                        trigger: 'manual',
                        title: function () {
                            return title;
                        }
                    });
                    $(this).on('rating.rateenter', function (e, rate) {
                        title = rate;
                        $(this).tooltip('show');
                    })
                            .on('rating.rateleave', function () {
                                $(this).tooltip('hide');
                            });
                }
            });
            /*-------- Filter By Price -----------*/
            $("#slider-range").slider({
                range: true,
                min: 0,
                max: 500,
                values: [75, 300],
                slide: function (event, ui) {
                    $("#amount").val("$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ]);
                }
            });
            $("#amount").val("$" + $("#slider-range").slider("values", 0) +
                    " - $" + $("#slider-range").slider("values", 1));
        </script>
    </body>
</html>