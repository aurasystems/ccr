<div class="page-head text-center">
    <div class="blog-main-slider" style="background-image:url('<?= base_url() ?>assets/planet/img/top_bg.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <h2><?= (!empty($title)) ? $title : '' ?></h2>
        </div>
    </div>
</div>