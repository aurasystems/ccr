<!--========================================
        Quick view
        ===========================================-->
<div class="quickViewWrap xv-hide">
    <div class="quickView">
        <?php if (cid()) { ?>
            <ul class="quickActions clearfix">
                <li><a href="<?= base_url('Account/profile') ?>"><?= lang('my_profile') ?></a></li>
                <li><a href="<?= base_url('Account/rent_history') ?>"><?= lang('rent_history') ?></a></li>
                <li><a href="<?= base_url() ?>Account/logout"><?= lang('logout') ?></a></li>
            </ul>
        <?php } else { ?>
            <ul class="quickActions clearfix">
                <li><a href="<?= base_url() ?>Login"><i class="fa fa-user"></i><?= lang('login') ?></a></li>
                <li><a href="#"><i class="fa fa-lock"></i><?= lang('register') ?></a></li>
            </ul>
        <?php } ?>
    </div>
</div>