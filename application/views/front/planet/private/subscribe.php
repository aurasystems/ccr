<div class="container hidden">
    <div class="xv-sign-up text-center pt-20 pb-60">
        <h3>Newsletter Subscription</h3>
        <form class="sign-up-form">
            <i class="fa fa-envelope"></i>
            <input type="text" placeholder="Coming Soon!" disabled="disabled">
            <button type="submit" disabled="disabled"><?= lang('subscribe') ?></button>
        </form><!--signup-form-->
    </div><!--xv-signup-->
</div><!--container-->