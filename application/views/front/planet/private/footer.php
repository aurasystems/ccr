<?php $settings = settings('all', 3); ?>
<footer class="doc-footer footer-dark">
    <div class="footer-content" style="background-image:url('<?= base_url() ?>assets/planet/img/top_bg.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="contact">
                        <?php if (!empty($settings)) { ?>
                            <a class="ft-logo pb-20" href="<?= base_url() ?>"><img src="<?= base_url() ?>uploads/settings/<?= $settings->logo ?>" alt="CCR"></a>
                        <?php } ?>
                    </div><!--content-->
                </div><!--column-3-->
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <h4 class="no-mar">Information</h4>
                    <?php if (!empty($settings)) { ?>
                        <ul class="no-preIcon customeIconList pt-30">
                            <li>
                                <i class="fa fa-phone"></i><?= $settings->phone ?>
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i>
                                <a href="mailto:<?= $settings->email ?>"><?= $settings->email ?></a>
                            </li>
                            <li>
                                <i class="fa fa-map-marker"></i><?= $settings->address ?>
                            </li>
                        </ul>
                    <?php } ?>
                </div><!--column-3-->
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <h4 class="no-mar">Pages</h4>
                    <ul class="mt-25">
                        <li><a href="<?= base_url('Home') ?>"><?= lang('home') ?></a></li>
                        <li><a href="<?= base_url('Shop') ?>"><?= lang('shop') ?></a></li>
                        <li><a href="<?= base_url('Cart') ?>"><?= lang('cart') ?></a></li>
                        <?php if (!empty($settings) && $settings->show_about) { ?>
                            <li><a href="<?= base_url('About') ?>"><?= lang('about_us') ?></a></li>
                        <?php } ?>
                        <li><a href="<?= base_url('Contact') ?>"><?= lang('contact_us') ?></a></li>
                        <?php if (!empty($settings) && $settings->show_blog) { ?>
                            <li><a href="<?= base_url('Blog') ?>"><?= lang('blog') ?></a></li>
                        <?php } ?>
                    </ul>
                </div><!--column-3-->
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <h4 class="no-mar">Links</h4>
                    <ul class="mt-25">
                        <li><a href="<?= base_url('Privacy') ?>">Privacy & Policy</a></li>
                        <li><a href="<?= base_url('Renting') ?>">How To Rent?</a></li>
                    </ul>
                </div><!--column-3-->
            </div><!--row-->
        </div><!--container-->
    </div><!--footer-content-->
    <div class="copy-rights color-white">
        <div class="container">
            <div class="inner">
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-sm-6 col-xs-12">
                        <span>Copyright &copy; <a style="color: #4f7594;" href="http://aura-sys.com"><?= lang('aura_sys') ?></a>. All Right Reserved</span>
                    </div><!--column-6-->

                </div><!--row-->
            </div><!--inner-->
        </div><!--container-->
    </div><!--copy-rights-->
</footer><!--doc-footer-->
<script src="<?= base_url() ?>assets/planet/js/jquery.js"></script>
<script src="<?= base_url() ?>assets/planet/js/ajaxify.min.js"></script>
<script src="<?= base_url() ?>assets/planet/js/jquery.easing-1.3.pack.js"></script>
<script src="<?= base_url() ?>assets/planet/js/jquery.countdown.min.js"></script>
<script src="<?= base_url() ?>assets/planet/js/jquery.waitforimages.js"></script>
<script src="<?= base_url() ?>assets/planet/js/owl.carousel.min.js"></script>
<script src="<?= base_url() ?>assets/planet/js/masonry.pkgd.min.js"></script>
<script src="<?= base_url() ?>assets/planet/js/main.js"></script>
<script>
    $('#search_products').keyup(function () {
        var search_query = $(this).val();
        var form_data = {
            term: search_query,
            search_wiz: $('#category').val(),
<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
        }
        var resp_data_format = "";
        if (search_query != '') {
            $.ajax({
                url: "<?= base_url() ?>Search/search_products",
                data: form_data,
                method: "post",
                dataType: "json",
                success: function (response) {
                    for (var i = 0; i < response.length; i++) {
                        resp_data_format = resp_data_format + "<li class='search_item'><a href='<?= base_url() ?>Shop/item_details/" + response[i]['id'] + "'>" + response[i]['n_en'] + "</a></li><br>";
                    }
                    ;
                    $("#data-container").html(resp_data_format);
                }
            });
        } else {
            $("#data-container").html('');
        }
    });
    function delete_item(id, product_id) {
        window.location = "<?= base_url() ?>Cart/delete_cart/" + id + "/" + product_id;
    }
    function start_end_date(product_id) {
        window.location = "<?= base_url() ?>Shop/check_availabilty/" + product_id;
    }
    function update_rating(product_id) {
        var postData = {
            product_id: product_id,
            rating: $('#product_rating' + product_id).val(),
<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
        };
        $.ajax({
            url: "<?= base_url() ?>Shop/add_rating",
            data: postData,
            method: "post",
            dataType: "json",
            success: function () {

            }
        });

    }
</script>