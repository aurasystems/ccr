<aside class="sidebar">
    <section class="widget widget-category pb-30" id="secondary-left">
        <header>
            <h3 style="padding-bottom: 10px;"><?= lang('categories') ?></h3>
        </header><!--Widget header-->
        <aside class="widget widget_product_categories">
            <?= $cats; ?>
        </aside>

    </section><!--widget-->
    <nav class="secondary-nav">
        <header>
            <h3 style="color: #5fb7ea;"><?= lang('brands') ?></h3>
        </header><!--Widget header-->
        <ul>
            <?php
            if (!empty($brands)) {
                foreach ($brands as $brand) {
            ?>
                    <li><a href="<?= base_url() ?>Shop/brand/<?= $brand->id ?>"><?= $brand->{'n_' . lc()} ?></a></li>
            <?php
                }
            }
            ?>
        </ul>
    </nav><!--secondary nav-->
</aside>  