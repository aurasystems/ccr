<?php
$settings = settings('all', 3);
$items = get_cart();
?>
<header class="doc-header header-light">
    <div class="header-top" style="background-image:url('<?= base_url() ?>assets/planet/img/top_bg.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-7">

                </div>
                <div class="col-xs-12 col-md-5">
                    <ul class="top-nav text-right">
                        <?php
                        if (!cid()) {
                            ?>
                            <li><a href="<?= base_url() ?>Login"><i class="fa fa-user"></i><?= lang('login') ?></a></li>
                            <li><a href="https://bit.ly/client-registration-form"><i class="fa fa-lock"></i><?= lang('register') ?></a></li>
                            <?php
                        } else {
                            ?>
                            <li class="parent"><a href="#"><?= $this->session->userdata('client_name_' . lc()) ?></a>
                                <ul>
                                    <li class="text-left"><a href="<?= base_url('Account/profile') ?>"><?= lang('my_profile') ?></a></li>
                                    <li class="text-left"><a href="<?= base_url('Account/rent_history') ?>"><?= lang('rent_history') ?></a></li>
                                    <li class="text-left"><a href="<?= base_url() ?>Account/logout"><?= lang('logout') ?></a></li>
                                </ul>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div><!--top header-->
    <div class="header-bottom">
        <div class="container">
            <?php if (!empty($settings)) { ?>
                <a class="logo" href="<?= base_url() ?>">
                    <img src="<?= base_url() ?>uploads/settings/<?= $settings->logo ?>" width="200" height="" alt="CCR"/>
                </a>
            <?php } ?>
            <nav>
                <a class="nav-triger" href="#">
                    <img src="<?= base_url() ?>assets/planet/img/basic/menu55.png" alt="">
                </a>
                <ul class="main_menu">
                    <li class="home-dropdown <?= (!empty($active) && $active == 'home') ? 'active' : '' ?>"><a href="<?= base_url('Home') ?>"><?= lang('home') ?></a></li>
                    <li class="<?= (!empty($active) && $active == 'shop') ? 'active' : '' ?>"><a href="<?= base_url('Shop') ?>"><?= lang('shop') ?></a></li>
                    <li class="<?= (!empty($active) && $active == 'cart') ? 'active' : '' ?>"><a href="<?= base_url('Cart') ?>"><?= lang('cart') ?></a></li>
                    <?php if (!empty($settings) && $settings->show_about) { ?>
                    <li class="<?= (!empty($active) && $active == 'about') ? 'active' : '' ?>"><a href="<?= base_url('About') ?>"><?= lang('about_us') ?></a></li>
                    <?php } ?>
                    <li class="<?= (!empty($active) && $active == 'contact') ? 'active' : '' ?>"><a href="<?= base_url('Contact') ?>"><?= lang('contact_us') ?></a></li>
                    <?php if (!empty($settings) && $settings->show_blog) { ?>
                    <li class="<?= (!empty($active) && $active == 'blog') ? 'active' : '' ?>"><a href="<?= base_url('Blog') ?>"><?= lang('blog') ?></a></li>
                    <?php } ?>
                </ul>
                <a href="#" class="openCart" style="background: url('<?= base_url() ?>assets/planet/img/basic/bag21.png') no-repeat center center #fdb913;"></a>
                <div class="table-responsive cart-calculations text-center">
                    <table class="table">
                        <tbody class="shadow-around">
                            <?php
                            $subtotal = 0;
                            if (!empty(cid())) {
                                if (!empty($items)) {
                                    foreach ($items as $item) {
                                        $product = get_product_details($item->product_id);
                                        if (!empty($product)) {
                                            $quant = get_quantity(cid(), $item->id);
                                            ?>
                                            <tr class="table-body">
                                                <td><figure><img src="<?= base_url() ?>uploads/thumb/<?= $product->img ?>" alt="" width="130" height="93" /></figure></td>
                                                <td>
                                                    <div class="cart-wrappper">
                                                        <h6 style="color: #4f7594;"><?= $product->{'n_' . lc()} ?></h6>
                                                    </div>                             
                                                </td>
                                                <td>
                                                    <span class="cart-content">Quantity:</span>
                                                    <span class="cart-price"><?= $quant ?></span>
                                                </td>
                                                <td>
                                                    <span class="cart-content">Unit Price:</span>
                                                    <span class="cart-price"><?= lang('egp') ?> <?= $product->rent_price ?></span>
                                                </td>
                                                <td>
                                                    <span class="cart-content">Sub Price:</span>
                                                    <span class="cart-price"><?= lang('egp') ?> <?= $quant * $product->rent_price ?></span>
                                                </td>
                                                <td>
                                                    <a onclick="delete_item('<?= $item->id ?>', '<?= $product->id ?>')" class="cart-action" href="javascript:void(0);"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                            $subtotal += $quant * $product->rent_price;
                                        }
                                    }
                                    ?>
                                    <tr class="table-body style">
                                        <td></td>
                                        <td>
                                            <a href="<?= base_url('Cart') ?>" class="btn-cart btn-rectangle">View Cart</a>
                                            <a href="<?= base_url('Checkout') ?>" class="btn-cart btn-rectangle">Checkout</a>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td class="style">TOTAL:<span class="colpink" style="color: #fdb913;"><?= lang('egp') ?> <?= $subtotal ?></span></td>
                                        <td></td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr class="table-body style">
                                    <td><h4 class="title"><a href="<?= base_url() ?>Login"><?= lang('login') ?></a><?= ' ' . lang('to_view_cart') ?></h4></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </nav>
        </div>
    </div><!--header Bottom-->
</header><!--document Header-->
<!--========================================
Search
===========================================-->
<div class="social-search style shadow-around style-wide">
    <div class="container">
        <div class="social">
            <ul>
                <li><span><?= (!empty($settings)) ? lang('phone') . ': ' . $settings->phone : '' ?></span></li>
                <li><a target="_blank" href="<?= (!empty($settings)) ? $settings->fb_url : '#' ?>"><i class="fa fa-facebook" style="background-color: #3b5998;"></i></a></li>
                <li><a target="_blank" href="<?= (!empty($settings)) ? $settings->insta_url : '#' ?>"><i class="fa fa-instagram"style="background-color: #910101;"></i></a></li>
            </ul><!--ul-->
        </div><!--social-->
        <form class="search-form" method="post" action="<?=  base_url('Search')?>">
            <?= csrf() ?>
            <input type="text" autocomplete="off" id="search_products" name="keyword" placeholder="<?= lang('search') ?>"data-search="true">
            <div  class="search_container"><ul id="data-container"></ul></div>
        </form><!--search-form-->
    </div><!--container-->
</div><!--social-search-->