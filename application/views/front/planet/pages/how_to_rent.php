<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
<!--<![endif]-->
<?php $this->load->view('front/planet/private/head'); ?>

<body class="home">
    <?php $this->load->view('front/planet/private/quick_view'); ?>
    <!--========================================     
    Header
    ===========================================-->
    <?php $this->load->view('front/planet/private/header'); ?>
    <!--=======Page Content Area=========-->
    <main id="pageContentArea">
        <?php $this->load->view('front/planet/private/page_head'); ?>
        <div class="page-contentArea pt-50 pb-50">
            <div class="container">
                <div class="col-xs-12 col-sm-8 col-md-9">
                    <div class="container">
                        <?= !empty($renting) ? $renting->content : '' ?>
                    </div>
                </div>
            </div>
            <!--conten Coloumn-->
        </div>
        <!--Main row-->
        <!--page-contentArea-->
    </main>

    <?php $this->load->view('front/planet/private/footer'); ?>

</body>
</html>