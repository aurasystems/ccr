<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
<!--<![endif]-->
<?php $this->load->view('front/planet/private/head'); ?>

<body class="home">
    <!--========================================     
    Header
    ===========================================-->
    <?php $this->load->view('front/planet/private/header'); ?>


    <!--=======Page Content Area=========-->
    <main id="pageContentArea">
    <div class="page-head text-center">
            <div class="blog-main-slider" style="background-image:url('assets/planet/img/b14.jpg');">
                <div class="overlay"></div>
                <div class="container">
                </div>
              <!--container-->

            </div>
        </div>
        <div class="page-contentArea pt-50 pb-50">

            <div class="container">
                
            </div>
            <!--container-->

        </div>
        <!--page-contentArea-->
    </main>

    <?php $this->load->view('front/planet/private/footer'); ?>

</body>
</html>