<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
    <!--<![endif]-->
    <?php $this->load->view('front/planet/private/head'); ?>
    <body class="home">
        <?php $this->load->view('front/planet/private/quick_view'); ?>
        <!--========================================     
        Header
        ===========================================-->
        <?php $this->load->view('front/planet/private/header'); ?>
        <!--=======Page Content Area=========-->   
        <main id="pageContentArea">
            <!--========================================
            Page Head
            ===========================================-->
            <?php $this->load->view('front/planet/private/page_head'); ?>
            <!--========================================
            cart content
            ===========================================-->
            <div class="container">
                <?php if (!empty(cid())) { ?>
                    <?php $c_voucher = get_cart_voucher(cid()); ?>
                    <div class="xv-cart pt-60">
                        <?= flash_success() ?>
                        <?= flash_error() ?>
                        <div class="xv-cart-top pb-45">
                            <div class="table-responsive cart-cal  text-center">
                                <div class="alert alert-danger" role="alert" style="display:none;" id="booking-error"> 
                                    <i class="icon ti-na"></i><strong  id="checkout-err"></strong>
                                </div>
                                <table class="table">
                                    <thead>
                                        <tr class="table-head">
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Sub Price</th>
                                        </tr>
                                    </thead>
                                    <tbody class="shadow-around">
                                        <?php
                                        $subtotal = 0;
                                        if (!empty($items)) {
                                            foreach ($items as $item) {
                                                $product = get_product_details($item->product_id);
                                                if (!empty($product)) {
                                                    $quant = get_quantity(cid(), $item->id);
                                                    ?>
                                                    <tr class="table-body">
                                                        <td><figure><img src="<?= base_url() ?>uploads/thumb/<?= $product->img ?>" alt="" width="171" height="97"/></figure></td>
                                                        <td>
                                                            <div class="cart-wrappper text-left">
                                                                <h6 style="color: #4f7594;"><?= $product->{'n_' . lc()} ?></h6>
                                                            </div>                             
                                                        </td> 
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>
                                                            <?= $quant ?>
                                                        </td>
                                                        <td><span class="cart-price"><?= lang('egp') ?> <?= $product->rent_price ?></span></td>
                                                        <td><span class="cart-price"><?= lang('egp') ?> <?= $quant * $product->rent_price ?></span></td>
                                                    </tr>
                                                    <?php
                                                    $subtotal += $quant * $product->rent_price;
                                                }
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>

                            </div>
                        </div><!--xv-cart-top-->
                        <div class="xv-accordian pt-15 pb-55">
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-md-offset-1">
                                    <?php
                                    $sub_total = 0;
                                    if (!empty($items)) {
                                        foreach ($items as $item) {
                                            $product = get_product_details($item->product_id);
                                            if (!empty($product)) {
                                                $quatitiy = get_quatitiy($item->id);
                                            }
                                            $sub_total += $item->total_cost;
                                        }
                                    }
                                    if (empty($c_voucher)) {
                                        $url = 'Checkout/coupon_validation';
                                    } else {
                                        $url = 'Checkout/remove_voucher/' . $c_voucher->id;
                                    }
                                    ?>
                                    <form class="checkout_coupon" method="post" action="<?= base_url() . $url ?>">
                                        <?= csrf() ?>
                                        <div class="accordian-pane active">
                                            <div class="accordian-form text-left">
                                                <div class="accordian-form-wrap">
                                                    <label><strong>HAVE A VOUCHER?</strong></label>
                                                    <input type="text" name="coupon_code" class="input-text" placeholder="<?= lang("enter_code") ?>" id="coupon_code" value="" required="required">
                                                    <input type="text" name="sub_total" value="<?= $sub_total ?>" class="hidden">
                                                    <span class="text-red"><?php echo form_error('coupon_number'); ?></span>
                                                </div>
                                                <div class="accordian-form-button">
                                                    <?php if (empty($c_voucher)) { ?>
                                                        <p class="form-row form-row-last">
                                                            <input type="submit" class="btn btn-accordian btn-blue" name="apply_coupon" value="Apply">
                                                        </p>
                                                    <?php } else { ?>
                                                        <p class="form-row form-row-last">
                                                            <input type="submit" class="btn btn-accordian btn-blue" name="apply_coupon" value="Remove">
                                                        </p>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <?php if (!empty($items)) { ?>
                                        <form action="<?= base_url() ?>Checkout/check_all" class="payment-form" method="POST">
                                        <?php } ?>
                                        <?php if (empty($items)) { ?>
                                            <form action="<?= base_url() ?>Checkout" class="payment-form" method="POST">
                                            <?php } ?>
                                            <?= csrf() ?>
                                            <table class="table">
                                                <tr class="Sub-Total">
                                                    <th>Sub Total</th>
                                                    <td><?= lang('egp') ?> <?= $sub_total ?></td>
                                                </tr>
                                                <tr class="Promotion-Discount">
                                                    <th>VOUCHER</th>
                                                    <td><?= lang('egp') ?> <?= (!empty($c_voucher)) ? $c_voucher->amount : 0 ?></td>
                                                </tr>
                                                <tr class="Total">
                                                    <th>TOTAL</th>
                                                    <td style="color: #616161;"><?= lang('egp') ?> <?= ($sub_total - ((!empty($c_voucher)) ? $c_voucher->amount : 0)) ?></td>
                                                </tr>
                                                <tr class="Promotion-Discount">
                                                    <th>LOCATION</th>
                                                    <td>
                                                        <select name="location_id" class="">
                                                            <?php foreach ($locations as $loc) { ?>
                                                                <option value="<?= $loc->id ?>" ><?= $loc->n_en ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr class="Promotion-Discount">
                                                    <th>PAYMENT</th>
                                                    <td>
                                                        <?php if (!empty($settings) && $settings->booking_cash_limit) { ?>
                                                            <input  type="radio" value='1' name="payment_method" required>
                                                            <label > <?= lang("cash") ?></label>
                                                        <?php } ?>
                                                        <?php if (!empty($settings) && $settings->booking_credit_limit) { ?>
                                                            <input type="radio" value='2' name="payment_method" required>
                                                            <label ><?= lang("credit_card") ?></label>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr class="Continue-Shopping">
                                                    <th></th>
                                                    <td>
                                                        <input type="checkbox" name="accept" value="" id="accept" required>
                                                        <label  for="accept">I have read and accept the <a style="color: #616161;" href="<?= base_url() ?>Privacy"><?= lang('term_and_cond') ?></a></label>
                                                    </td>
                                                <tr class="Continue-Shopping">
                                                    <th></th>
                                                    <td><input type="submit" class="btn btn-accordian btn-blue" name="submit" value="Place your order"></td>
                                                </tr>
                                            </table>
                                        </form>
                                </div>
                            </div>    
                        </div>
                        <?php
                    } else {
                        ?>
                        <h4 class="title text-center"><a href="<?= base_url() ?>Login"><?= lang('login') ?></a><?= ' ' . lang('to_view_cart') ?></h4>

                    </div>
                    <?php
                }
                ?>
            </div><!--xv-cart-->
        </main>
        <?php $this->load->view('front/planet/private/footer'); ?>

    </body>
</html>