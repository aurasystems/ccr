<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
    <!--<![endif]-->
    <?php $this->load->view('front/planet/private/head'); ?>
    <body class="home">
        <?php $this->load->view('front/planet/private/quick_view'); ?>
        <!--========================================
            Header
            ===========================================-->
        <?php $this->load->view('front/planet/private/header'); ?>
        <!--=======Page Content Area=========-->
        <main id="pageContentArea">
            <?php $this->load->view('front/planet/private/page_head'); ?>
            <div class="container">
                <div class="product-overview pt-50 pb-50">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <div class="xv-product-slides grid-view products" data-thumbnail="figure .xv-superimage" data-product=".xv-product-unit">
                                <header class="sec-heading text-center">
                                    <h2><span><?= lang('search_results') ?></span></h2>
                                </header>
                                <!--header-->
                                <div class="row">
                                    <?php
                                    if (!empty($items)) {
                                        foreach ($items as $item) {
                                            if (!empty($item->img)) {
                                                $image = base_url() . 'uploads/thumb/' . $item->img;
                                            } else {
                                                $image = base_url() . 'uploads/products/default_product.png';
                                            }
                                    ?>
                                        <div class="xv-product-unit">
                                            <div class="xv-product shadow-around">
                                                <figure>
                                                    <a href="<?= base_url() ?>Shop/item_details/<?= $item->id ?>"><img class="xv-superimage" src="<?= $image ?>" alt="" width="280" height="265" /></a>
                                                </figure>
                                                <div class="xv-product-content">
                                                    <h3><a href="<?= base_url() ?>Shop/item_details/<?= $item->id ?>"><?= substr($item->{'n_' . lc()}, 0, 20) ?></a></h3>
                                                    <div class="xv-rating stars-5"></div>
                                                    <span class="xv-price"><?= lang('egp') ?> <?= $item->rent_price ?></span>
                                                </div>
                                                <!--xv-product-content-->
                                            </div>
                                            <!--xv-product(list-view)-->
                                        </div>
                                    <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <!--pagination-->
                        </div>
                    </div>
                    <!--row-->
                </div>
                <!--product overview-->
            </div>
            <!--container-->
        </main>
        <!--pageContentArea ends-->
        <!--========================================
            Footer
            ===========================================-->
        <?php $this->load->view('front/planet/private/footer'); ?>
    </body>
</html>