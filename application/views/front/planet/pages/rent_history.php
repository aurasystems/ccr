<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
    <!--<![endif]-->
    <?php $this->load->view('front/planet/private/head'); ?>

    <body class="home">
        <?php $this->load->view('front/planet/private/quick_view'); ?>
        <!--========================================     
        Header
        ===========================================-->
        <?php $this->load->view('front/planet/private/header'); ?>
        <!--=======Page Content Area=========-->
        <main id="pageContentArea">
            <?php $this->load->view('front/planet/private/page_head'); ?>
            <div class="container">
                <div class="page-contentArea pt-50 pb-50">
                    <div class="xv-cart pt-60">
                        <div class="xv-cart-top pb-45">
                            <div class="table-responsive cart-cal text-center">
                                <table class="table">
                                    <thead>
                                        <tr class="table-head">
                                            <th><?= lang("quantity") ?></th>
                                            <th style="text-align: center !important;"><?= lang("img") ?></th>
                                            <th><?= lang("product_details") ?></th>
                                            <th><?= lang("from") ?></th>
                                            <th><?= lang("to") ?></th>
                                            <th><?= lang("status") ?></th>
                                            <th><?= lang("price") ?></th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="shadow-around">
                                        <?php
                                        $i = 0;
                                        foreach ($booking as $one) {
                                            ?>
                                            <tr class="table-body">
                                                <td><?= $one->quantity ?></td>
                                                <td>
                                                    <figure> <a href="<?= base_url() ?>uploads/products/<?= $one->img ?>">
                                                            <img src="<?= base_url() ?>/uploads/products/<?= $one->img ?>" alt="" width="90px" height="90px"></a>
                                                    </figure>
                                                </td>
                                                <td><strong href="ecommerce-product-detail.html"><?= $one->n_en ?></strong></td>
                                                <td><?= $one->b_from ?></td>
                                                <td><?= $one->b_from ?></td>
                                                <td>
                                                    <?php
                                                    if ($one->canceled == 1)
                                                        echo lang("canceled");
                                                    else if ($one->status == 1 || $one->status == 2)
                                                        echo lang("approved");
                                                    else if ($one->status == 0)
                                                        echo lang("pending")
                                                        ?>
                                                </td>
                                                <td><strong>EGP</strong>&nbsp;<span class="item-total"><?= $one->rent_price ?></span></td>
                                                <td>
                                                    <ul class="cart-action">
                                                        <?php if ($can_cancel[$i] == true) { ?>
                                                            <li> <button title="<?= lang("cancel") ?>" onclick="cancel_booking('<?= $one->book_id ?>')" type="button" class="btn-cart btn-square style" aria-label="Close"><i style="color:red" class="fa fa-times"></i></button></li>
                                                        </ul>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--container-->
                </div>
                <!--page-contentArea-->
            </div>
            <!--container-->
        </main>
        <?php $this->load->view('front/planet/private/footer'); ?>
    </body>
</html>