<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
    <!--<![endif]-->
    <?php $this->load->view('front/planet/private/head'); ?>

    <body class="home">
        <?php $this->load->view('front/planet/private/quick_view'); ?>
        <!--========================================     
        Header
        ===========================================-->
        <?php $this->load->view('front/planet/private/header'); ?>
        <!--=======Page Content Area=========-->
        <main id="pageContentArea">
            <?php $this->load->view('front/planet/private/page_head'); ?>
            <div class="container">
                <div class="page-contentArea pt-50 pb-50">
                    <section class="login-section">
                        <div class="section-padding">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8">
                                        <?= flash_success() ?>
                                        <?= flash_error() ?>
                                        <div class="sign-in bg-gray sign-up billing-table">
                                            <h2 class="title"><?= lang('update_info') ?> <span></span></h2><!-- /.title -->
                                            <form class="signup-form" id="signup-form" action="<?= base_url('Account/update_info') ?>" method="post">
                                                <?= csrf() ?>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="n_en" value="<?= !empty($one->n_en) ? $one->n_en : '' ?>" class="form-control" placeholder="<?= lang('name_en') ?>" required>
                                                            <?php echo form_error('n_en'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="n_ar" value="<?= !empty($one->n_ar) ? $one->n_ar : '' ?>" class="form-control" placeholder="<?= lang('name_ar') ?>" required>
                                                            <?php echo form_error('n_ar'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="email" name="email" value="<?= !empty($one->email) ? $one->email : '' ?>" class="form-control" placeholder="<?= lang('email') ?>" required autocomplete="off">
                                                            <?php echo form_error('email'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="phone" value="<?= !empty($one->phone) ? $one->phone : '' ?>" class="form-control" placeholder="<?= lang('phone') ?>" required>
                                                            <?php echo form_error('phone'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="national_id" value="<?= !empty($one->national_id) ? $one->national_id : '' ?>" class="form-control" placeholder="<?= lang('national_id') ?>" required>
                                                            <?php echo form_error('national_id'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="address_en" value="<?= !empty($one->address_en) ? $one->address_en : '' ?>" class="form-control" placeholder="<?= lang('address_en') ?>">
                                                            <?php echo form_error('address_en'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="password" name="password" class="form-control" placeholder="<?= lang('password') ?>">
                                                            <?php echo form_error('password'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="password" name="confirm_password" class="form-control" placeholder="<?= lang('confirm_password') ?>">
                                                            <?php echo form_error('confirm_password'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="submit-input">
                                                    <input type="submit" name="submit" value="Save" class="btn-blue btn">				
                                                </p>
                                            </form>
                                        </div><!-- /.sign-up -->
                                    </div>
                                </div><!-- /.row -->
                            </div>
                            <!--/.container-->
                        </div><!-- /.section-padding -->
                    </section>
                    <!--/.profile-section-->
                </div>
            </div>
            <!--container-->
        </main>
        <?php $this->load->view('front/planet/private/footer'); ?>

    </body>

</html>