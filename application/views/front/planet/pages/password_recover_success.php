<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
    <!--<![endif]-->
    <?php $this->load->view('front/planet/private/head'); ?>

    <body class="home">
        <?php $this->load->view('front/planet/private/quick_view'); ?>
        <!--========================================     
        Header
        ===========================================-->
        <?php $this->load->view('front/planet/private/header'); ?>
        <!--=======Page Content Area=========-->
        <main id="pageContentArea">
            <?php $this->load->view('front/planet/private/page_head'); ?>
            <div class="page-contentArea pt-50 pb-50">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <h2><?= lang('lang_thank_msg') ?></h2>
                            <a href="<?= base_url('Login') ?>"><?= lang('lang_back_to_login') ?></a>
                        </div>
                    </div>
                </div>
                <!--orderContent-->
            </div>
            <!--page-contentArea-->
        </main>
        <?php $this->load->view('front/planet/private/footer'); ?>
    </body>
</html>