<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
    <!--<![endif]-->
    <?php $this->load->view('front/planet/private/head'); ?>
    <body class="home">
        <?php $this->load->view('front/planet/private/quick_view'); ?>
        <!--========================================
        Header
        ===========================================--> 
        <?php $this->load->view('front/planet/private/header'); ?>
        <div class="clearfix"></div>   
        <main id="pageContentArea">

            <!--========================================
            Product Details
            ===========================================-->

            <div class="page-head style text-center">
                <div class="blog-main-slider" style="background-image:url('<?= base_url() ?>assets/planet/img/b3.jpg'); no-repeat">
                    <div class="overlay"></div>
                    <div class="container">
                        <h2>SINGLE PRODUCT</h2>
                        <p>Aenean sollicitudin, nec sagittis sem lorem quist bibe dum auctor.</p>
                    </div>
                </div>
            </div>

            <!--========================================
            single product details
            ===========================================-->

            <div class="container">
                <div class="single-product-detail pt-60 pb-30">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="product-slider-wrap text-center shadow-around">
                                <div class="product-slide">
                                    <figure>
                                        <img src="<?= base_url() ?>assets/planet/img/single-product-mac.png" alt="">
                                    </figure>
                                </div><!--product-slide-->
                                <div class="product-slider styleDetail">
                                    <div class="detail-slider">
                                        <div class="owl-carousel slider_controls5"  data-dots="false" data-prev="fa fa-angle-left" data-next="fa fa-angle-right" data-margin="20" data-slides="4" data-slides-md="3" data-slides-sm="1" data-loop="false" data-nav="true">
                                            <figure class="border-around">
                                                <a href="#"><img src="<?= base_url() ?>assets/planet/img/detail-product-slider-1.png" alt=""></a>
                                            </figure>

                                            <figure class="border-around">
                                                <a href="#"><img src="<?= base_url() ?>assets/planet/img/detail-product-slider-1.png" alt=""></a>
                                            </figure>


                                            <figure class="border-around">
                                                <a href="#"><img src="<?= base_url() ?>assets/planet/img/detail-product-slider-1.png" alt=""></a>
                                            </figure>
                                            <figure class="border-around">
                                                <a href="#"><img src="<?= base_url() ?>assets/planet/img/detail-product-slider-1.png" alt=""></a>
                                            </figure>
                                            <figure class="border-around">
                                                <a href="#"><img src="<?= base_url() ?>assets/planet/img/detail-product-slider-1.png" alt=""></a>
                                            </figure>
                                        </div>                        

                                    </div><!--product-slider-->
                                </div>
                            </div><!--column-6-->

                        </div><!--column-6-->
                        <div class="col-xs-12 col-md-6">
                            <div class="single-product-overview">
                                <h2>Apple New Macbook 2015</h2>
                                <div class="xv-rating stars-5"></div>
                                <ul class="review">
                                    <li>15 Review(s)&emsp;</li>
                                    <li><a href="#">Make a Review</a></li>
                                </ul>
                                <p class="no-mar">This is New Macbook 2015. Proin gravida nibh vel velit auctor aliquet. Aenean sollici tudin, nec sagittis sem lorem quis bibe dum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit gravida nibh vel velit.</p>
                                <ul class="product-description mt-35 mb-35">
                                    <li><span>Availability</span>: Available in Stock</li>
                                    <li><span>Product Code</span>: CwT4a</li>
                                    <li><span>Manufacturer</span>: Apple Inc</li>
                                    <li><span>Review Points</span>: 25 Points</li> 
                                </ul>
                                <div class="cart-options">
                                    <a href="#" class="price"><span>$2500</span> <del>$7000</del></a>
                                    <ul class="cart-buttons mt-45 clearfix">
                                        <li class="btn-color-opt">

                                            <div class="custome-select">
                                                <b class="fa fa-caret-down"></b>
                                                <span>Select a Color</span>
                                                <select>
                                                    <option>Select a color</option>
                                                    <option>Red</option>
                                                    <option>Black</option>
                                                    <option>Grey</option>
                                                </select>
                                            </div>

                                        </li>
                                        <li>
                                            <div class="quantity-control">
                                                <span class="btn-cart btn-square btn-plus btn-qty"><i class="fa fa-plus"></i></span>
                                                <input type="text" data-invalid="Enter valid quantity" data-maxalert="Maximum limit reached" data-max="10" data-minalert="Minimum limit reached" data-min="1" value="2">
                                                <span class="btn-cart btn-square btn-minus btn-qty"><i class="fa fa-minus"></i></span>                      
                                            </div>
                                        </li>
                                        <li>
                                            <a href="#" class="btn-cart btn-square">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="btn-cart btn-square">
                                                <i class="fa fa-exchange"></i>
                                            </a>
                                        </li>
                                    </ul><!--cart-buttons-->
                                    <a class="btn btn-continue">ADD TO CART</a>
                                </div><!--cart-options-->
                            </div><!--single-product-overview-->
                        </div><!--column-6-->
                    </div><!--single-product-detail-->
                    <div class="tabs-pane mt-60">
                        <ul class="tab-sections">
                            <li class="active"><a href="#tab01" class="btn-cart">Description</a></li>
                            <li><a href="#tab02" class="btn-cart">Specification</a></li>
                            <li><a href="#tab03" class="btn-cart">Return Info</a></li>
                            <li><a href="#tab04" class="btn-cart">Review</a></li>
                        </ul>
                        <div class="tab-panels">
                            <div id="tab01" class="tab-content active">
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, nec sagittis sem lorem quis bibe dum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a nec sagittis sem sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, nec sagittis sem lorem quis bibe dum auctor.
                                </p>
                                <ul>
                                    <li>5.5-inch (diagonal) LED-backlit widescreen Multi-Touch</li>
                                    <li>A8 chip with 64-bit architecture</li>
                                    <li>1080p HD video recording (30 fps or 60 fps)</li>
                                    <li>M8 motion coprocessor</li>
                                    <li>Full sRGB standard</li>
                                    <li>Slo-mo video (120 fps or 240 fps)</li>
                                    <li>Touch ID fingerprint identity sensor built into the Home button</li>
                                </ul>
                            </div><!--tab content-->
                            <div id="tab02" class="tab-content">
                                <ul>
                                    <li>5.5-inch (diagonal) LED-backlit widescreen Multi-Touch</li>
                                    <li>A8 chip with 64-bit architecture</li>
                                    <li>1080p HD video recording (30 fps or 60 fps)</li>
                                    <li>M8 motion coprocessor</li>
                                    <li>Full sRGB standard</li>
                                    <li>Slo-mo video (120 fps or 240 fps)</li>
                                    <li>Touch ID fingerprint identity sensor built into the Home button</li>
                                </ul>
                            </div><!--tab content-->
                            <div id="tab03" class="tab-content">
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, nec sagittis sem lorem quis bibe dum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a nec sagittis sem sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, nec sagittis sem lorem quis bibe dum auctor.
                                </p>
                                <ul>
                                    <li>5.5-inch (diagonal) LED-backlit widescreen Multi-Touch</li>
                                    <li>A8 chip with 64-bit architecture</li>
                                    <li>1080p HD video recording (30 fps or 60 fps)</li>
                                    <li>M8 motion coprocessor</li>
                                    <li>Full sRGB standard</li>
                                    <li>Slo-mo video (120 fps or 240 fps)</li>
                                    <li>Touch ID fingerprint identity sensor built into the Home button</li>
                                </ul>
                            </div><!--tab content-->
                            <div id="tab04" class="tab-content">
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, nec sagittis sem lorem quis bibe dum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a nec sagittis sem sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, nec sagittis sem lorem quis bibe dum auctor.
                                </p>
                            </div><!--tab content-->
                        </div>

                    </div><!--tabs pane-->
                </div><!--container-->
            </div>

            <!--========================================
            related products
            ===========================================--> 

            <div class="container">
                <div class="xv-featured pt-25 ">
                    <header class="sec-heading text-center">
                        <div class="category-wrap style">
                            <span class="categorise"><i class="fa fa-bars"></i>All Categories<i class="fa style fa-caret-down"></i></span>
                            <ul class="cat-dropDown">
                                <li><a href="#">Smart Phones</a></li>
                                <li><a href="#">Tablets</a></li>
                                <li><a href="#">Cameras</a></li>
                                <li><a href="#">Headphones</a></li>
                                <li><a href="#">Gaming Gear</a></li>
                            </ul>
                        </div>
                        <h2><span>Related Product</span></h2>
                        <span>for Apple New Macbook 2015</span>
                    </header><!--header-->
                    <div class="xv-product-slides mt-25 mb-25">
                        <div class="owl-carousel slider_controls1"  data-dots="false" data-prev="fa fa-angle-left" data-next="fa fa-angle-right" data-slides="4" data-slides-md="2" data-slides-sm="1" data-margin="24" data-loop="true" data-nav="true">
                            <div class="xv-product style shadow-around">
                                <figure>
                                    <a href="#"><img  class="xv-superimage" src="<?= base_url() ?>assets/planet/img/featured-img-1.png" alt=""/></a>
                                    <figcaption>
                                        <ul>
                                            <li><a data-qv-tab="#qvt-wishlist"  class="btn-cart flytoQuickView btn-square btn-blue" href="#"><i class="fa fa-heart"></i></a></li>
                                            <li><a data-qv-tab="#qvt-compare" class="btn-cart flytoQuickView btn-square btn-blue" href="#"><i class="fa fa-exchange"></i></a></li>
                                            <li><a class="btn-cart btn-square btn-blue" href="#"><i class="fa fa-expand"></i></a></li>
                                        </ul>
                                    </figcaption>
                                </figure><!--figure-->
                                <div class="xv-product-content">
                                    <h3><a href="detail1.html">Smartphone Apple iPhone 5 64GB</a></h3>
                                    <ul class="color-opt">
                                        <li><a href="#">White</a></li>
                                        <li><a href="#">Black</a></li>
                                        <li><a href="#">Gold</a></li>
                                    </ul>
                                    <div class="xv-rating stars-5"></div>
                                    <span class="xv-price">$250</span>
                                    <a data-qv-tab="#qvt-cart" href="#" class="product-buy flytoQuickView">Buy</a>
                                </div>
                            </div><!--xv-product-->
                            <div class="xv-product style shadow-around">
                                <figure>
                                    <a href="#"><img  class="xv-superimage" src="<?= base_url() ?>assets/planet/img/featured-img-2.png" alt=""/></a>
                                    <figcaption>
                                        <ul>
                                            <li><a data-qv-tab="#qvt-wishlist"  class="btn-cart flytoQuickView btn-square btn-blue" href="#"><i class="fa fa-heart"></i></a></li>
                                            <li><a data-qv-tab="#qvt-compare" class="btn-cart flytoQuickView btn-square btn-blue" href="#"><i class="fa fa-exchange"></i></a></li>
                                            <li><a class="btn-cart btn-square btn-blue" href="#"><i class="fa fa-expand"></i></a></li>
                                        </ul>
                                    </figcaption>
                                </figure><!--figure-->
                                <div class="xv-product-content">
                                    <h3><a href="detail1.html">Smartphone Apple iPhone 5 64GB</a></h3>
                                    <ul class="color-opt">
                                        <li><a href="#">White</a></li>
                                        <li><a href="#">Black</a></li>
                                        <li><a href="#">Gold</a></li>
                                    </ul>
                                    <div class="xv-rating stars-5"></div>
                                    <span class="xv-price">$250</span>
                                    <a data-qv-tab="#qvt-cart" href="#" class="product-buy flytoQuickView">Buy</a>
                                </div>
                            </div><!--xv-product-->
                            <div class="xv-product style shadow-around">
                                <figure>
                                    <a href="#"><img  class="xv-superimage" src="<?= base_url() ?>assets/planet/img/featured-img-3.png" alt=""/></a>
                                    <figcaption>
                                        <ul>
                                            <li><a data-qv-tab="#qvt-wishlist"  class="btn-cart flytoQuickView btn-square btn-blue" href="#"><i class="fa fa-heart"></i></a></li>
                                            <li><a data-qv-tab="#qvt-compare" class="btn-cart flytoQuickView btn-square btn-blue" href="#"><i class="fa fa-exchange"></i></a></li>
                                            <li><a class="btn-cart btn-square btn-blue" href="#"><i class="fa fa-expand"></i></a></li>
                                        </ul>
                                    </figcaption>
                                </figure><!--figure-->
                                <div class="xv-product-content">
                                    <h3><a href="detail1.html">Smartphone Apple iPhone 5 64GB</a></h3>
                                    <ul class="color-opt">
                                        <li><a href="#">White</a></li>
                                        <li><a href="#">Black</a></li>
                                        <li><a href="#">Gold</a></li>
                                    </ul>
                                    <div class="xv-rating stars-5"></div>
                                    <span class="xv-price">$250</span>
                                    <a data-qv-tab="#qvt-cart" href="#" class="product-buy flytoQuickView">Buy</a>
                                </div>
                            </div><!--xv-product-->
                            <div class="xv-product style shadow-around">
                                <figure>
                                    <a href="#"><img  class="xv-superimage" src="<?= base_url() ?>assets/planet/img/featured-img-4.png" alt=""/></a>
                                    <figcaption>
                                        <ul>
                                            <li><a data-qv-tab="#qvt-wishlist"  class="btn-cart flytoQuickView btn-square btn-blue" href="#"><i class="fa fa-heart"></i></a></li>
                                            <li><a data-qv-tab="#qvt-compare" class="btn-cart flytoQuickView btn-square btn-blue" href="#"><i class="fa fa-exchange"></i></a></li>
                                            <li><a class="btn-cart btn-square btn-blue" href="#"><i class="fa fa-expand"></i></a></li>
                                        </ul>
                                    </figcaption>
                                </figure><!--figure-->
                                <div class="xv-product-content">
                                    <h3><a href="detail1.html">Smartphone Apple iPhone 5 64GB</a></h3>
                                    <ul class="color-opt">
                                        <li><a href="#">White</a></li>
                                        <li><a href="#">Black</a></li>
                                        <li><a href="#">Gold</a></li>
                                    </ul>
                                    <div class="xv-rating stars-5"></div>
                                    <span class="xv-price">$250</span>
                                    <a data-qv-tab="#qvt-cart" href="#" class="product-buy flytoQuickView">Buy</a>
                                </div>
                            </div><!--xv-product-->
                            <div class="xv-product style shadow-around">
                                <figure>
                                    <a href="#"><img  class="xv-superimage" src="<?= base_url() ?>assets/planet/img/featured-img-1.png" alt=""/></a>
                                    <figcaption>
                                        <ul>
                                            <li><a data-qv-tab="#qvt-wishlist"  class="btn-cart flytoQuickView btn-square btn-blue" href="#"><i class="fa fa-heart"></i></a></li>
                                            <li><a data-qv-tab="#qvt-compare" class="btn-cart flytoQuickView btn-square btn-blue" href="#"><i class="fa fa-exchange"></i></a></li>
                                            <li><a class="btn-cart btn-square btn-blue" href="#"><i class="fa fa-expand"></i></a></li>
                                        </ul>
                                    </figcaption>
                                </figure><!--figure-->
                                <div class="xv-product-content">
                                    <h3><a href="detail1.html">Smartphone Apple iPhone 5 64GB</a></h3>
                                    <ul class="color-opt">
                                        <li><a href="#">White</a></li>
                                        <li><a href="#">Black</a></li>
                                        <li><a href="#">Gold</a></li>
                                    </ul>
                                    <div class="xv-rating stars-5"></div>
                                    <span class="xv-price">$250</span>
                                    <a data-qv-tab="#qvt-cart" href="#" class="product-buy flytoQuickView">Buy</a>
                                </div>
                            </div><!--xv-product-->
                            <div class="xv-product style shadow-around">
                                <figure>
                                    <a href="#"><img  class="xv-superimage" src="<?= base_url() ?>assets/planet/img/featured-img-2.png" alt=""/></a>
                                    <figcaption>
                                        <ul>
                                            <li><a data-qv-tab="#qvt-wishlist"  class="btn-cart flytoQuickView btn-square btn-blue" href="#"><i class="fa fa-heart"></i></a></li>
                                            <li><a data-qv-tab="#qvt-compare" class="btn-cart flytoQuickView btn-square btn-blue" href="#"><i class="fa fa-exchange"></i></a></li>
                                            <li><a class="btn-cart btn-square btn-blue" href="#"><i class="fa fa-expand"></i></a></li>
                                        </ul>
                                    </figcaption>
                                </figure><!--figure-->
                                <div class="xv-product-content">
                                    <h3><a href="detail1.html">Smartphone Apple iPhone 5 64GB</a></h3>
                                    <ul class="color-opt">
                                        <li><a href="#">White</a></li>
                                        <li><a href="#">Black</a></li>
                                        <li><a href="#">Gold</a></li>
                                    </ul>
                                    <div class="xv-rating stars-5"></div>
                                    <span class="xv-price">$250</span>
                                    <a data-qv-tab="#qvt-cart" href="#" class="product-buy flytoQuickView">Buy</a>
                                </div>
                            </div><!--xv-product-->
                            <div class="xv-product style shadow-around">
                                <figure>
                                    <a href="#"><img  class="xv-superimage" src="<?= base_url() ?>assets/planet/img/featured-img-3.png" alt=""/></a>
                                    <figcaption>
                                        <ul>
                                            <li><a data-qv-tab="#qvt-wishlist"  class="btn-cart flytoQuickView btn-square btn-blue" href="#"><i class="fa fa-heart"></i></a></li>
                                            <li><a data-qv-tab="#qvt-compare" class="btn-cart flytoQuickView btn-square btn-blue" href="#"><i class="fa fa-exchange"></i></a></li>
                                            <li><a class="btn-cart btn-square btn-blue" href="#"><i class="fa fa-expand"></i></a></li>
                                        </ul>
                                    </figcaption>
                                </figure><!--figure-->
                                <div class="xv-product-content">
                                    <h3><a href="detail1.html">Smartphone Apple iPhone 5 64GB</a></h3>
                                    <ul class="color-opt">
                                        <li><a href="#">White</a></li>
                                        <li><a href="#">Black</a></li>
                                        <li><a href="#">Gold</a></li>
                                    </ul>
                                    <div class="xv-rating stars-5"></div>
                                    <span class="xv-price">$250</span>
                                    <a data-qv-tab="#qvt-cart" href="#" class="product-buy flytoQuickView">Buy</a>
                                </div>
                            </div><!--xv-product-->
                            <div class="xv-product style shadow-around">
                                <figure>
                                    <a href="#"><img  class="xv-superimage" src="<?= base_url() ?>assets/planet/img/featured-img-4.png" alt=""/></a>
                                    <figcaption>
                                        <ul>
                                            <li><a data-qv-tab="#qvt-wishlist"  class="btn-cart flytoQuickView btn-square btn-blue" href="#"><i class="fa fa-heart"></i></a></li>
                                            <li><a data-qv-tab="#qvt-compare" class="btn-cart flytoQuickView btn-square btn-blue" href="#"><i class="fa fa-exchange"></i></a></li>
                                            <li><a class="btn-cart btn-square btn-blue" href="#"><i class="fa fa-expand"></i></a></li>
                                        </ul>
                                    </figcaption>
                                </figure><!--figure-->
                                <div class="xv-product-content">
                                    <h3><a href="detail1.html">Smartphone Apple iPhone 5 64GB</a></h3>
                                    <ul class="color-opt">
                                        <li><a href="#">White</a></li>
                                        <li><a href="#">Black</a></li>
                                        <li><a href="#">Gold</a></li>
                                    </ul>
                                    <div class="xv-rating stars-5"></div>
                                    <span class="xv-price">$250</span>
                                    <a data-qv-tab="#qvt-cart" href="#" class="product-buy flytoQuickView">Buy</a>
                                </div>
                            </div><!--xv-product-->
                        </div><!--owl carousel-->
                    </div>
                </div><!--xv-featured-->
            </div><!--container-->   
            <!--========================================
            sign up
            ===========================================-->
            <?php $this->load->view('front/planet/private/subscribe'); ?>
        </main> <!--pageContentArea ends-->   
        <!--========================================
        Footer
        ===========================================-->
        <?php $this->load->view('front/planet/private/footer'); ?>
        <!--=================================
        </body>
        </html>
