<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
<!--<![endif]-->
<?php $this->load->view('front/planet/private/head'); ?>

<body class="home">
    <?php $this->load->view('front/planet/private/quick_view'); ?>
    <!--========================================     
    Header
    ===========================================-->
    <?php $this->load->view('front/planet/private/header'); ?>
    <!--=======Page Content Area=========-->
    <main id="pageContentArea">
        <?php $this->load->view('front/planet/private/page_head'); ?>
        <div class="page-contentArea pt-50 pb-50">
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        <?= flash_success() ?>
                        <?= flash_error() ?>
                        <h2>Give a message</h2>
                        <form action="<?= base_url() ?>Contact/send" method="post">
                            <?= csrf() ?>
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" placeholder="Full Name *" name="name" required="required">
                                </div>
                                <!--column-->
                                <div class="col-md-4">
                                    <input type="email" placeholder="Email *" name="email" required="required">
                                </div>
                                <!--column-->
                                <div class="col-md-4">
                                    <input type="text" placeholder="Your URL" name="Your URL">
                                </div>
                                <!--column-->
                            </div>
                            <!--row-->
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea name="message" placeholder="Your Message *" required="required"></textarea>
                                </div>
                                <!--column-->
                            </div>
                            <!--row-->
                            <button class="btn btn-send btn-pink"><i class="fa fa-paper-plane"></i>
                                Send</button>
                        </form>
                        <!--Form-->
                    </div>

                </div>
            </div>
            <!--orderContent-->

        </div>
        <!--page-contentArea-->
    </main>

    <?php $this->load->view('front/planet/private/footer'); ?>

</body>

</html>