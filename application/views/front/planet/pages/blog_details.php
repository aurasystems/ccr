<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
    <!--<![endif]-->
    <?php $this->load->view('front/planet/private/head'); ?>
    <body class="home">
        <?php $this->load->view('front/planet/private/quick_view'); ?>
        <!--========================================     
        Header
        ===========================================-->
        <?php $this->load->view('front/planet/private/header'); ?>
        <!--=======Page Content Area=========-->
        <main id="pageContentArea">
            <?php $this->load->view('front/planet/private/page_head'); ?>
            <div class="page-contentArea pt-50 pb-50">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-3">
                            <nav class="secondary-nav">
                                <header>
                                    <h3 style="color: #5fb7ea;"><?= lang('categories') ?></h3>
                                </header><!--Widget header-->
                                <ul>
                                    <?php
                                    if (!empty($categories)) {
                                        foreach ($categories as $cat) {
                                            ?>
                                            <li><a href="<?= base_url() ?>Blog/get_blog_by_category/<?= $cat->id ?>"><span class="p-count"><?= $cat->count ?></span><?= $cat->{'n_' . lc()} ?></a></li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </nav><!--secondary nav-->
                            <aside class="sidebar">
                                <section class="widget widget_tag_cloud">
                                    <header>
                                        <h3><?= lang('tags') ?></h3>
                                    </header><!--Widget header-->
                                    <div class="widget-content">
                                        <?php
                                        if (!empty($tags)) {
                                            foreach ($tags as $tag) {
                                                ?>
                                                <a href="<?= base_url() ?>Blog/get_blog_by_tag/<?= $tag->tag ?>"><?= $tag->tag ?></a>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </section><!--widget-->
                            </aside>
                        </div><!--sidebar Column-->
                        <!--sidebar Column-->
                        <div class="col-xs-12 col-sm-8 col-md-9">
                            <div class="articleWrap">
                                <article class="xv-article">
                                    <?php
                                    if (!empty($blog->img)) {
                                        $image = base_url() . 'uploads/thumb/' . $blog->img;
                                    } else {
                                        $image = base_url() . 'uploads/blogs/default_blog.png';
                                    }
                                    ?>
                                    <figure>
                                        <img src="<?= $image ?>" alt="">
                                    </figure><!--fig-->
                                    <div class="xv-art-content mb-60 shadow-around">
                                        <header>
                                            <h2><?= (!empty($blog->title)) ? $blog->title : '' ?></h2>
                                            <time datetime="<?= date('Y-m-d', strtotime($blog->create_time)) ?>"><?= lang('post_by') ?> <?= $blog->uname ?> <span class="date"><?= date('d', strtotime($blog->create_time)) ?></span> <span class="month"><?= date('M', strtotime($blog->create_time)) ?></span></time>
                                        </header><!--header-->
                                        <p><?= (!empty($blog->content)) ? $blog->content : '' ?></p>
                                    </div><!--article content-->

                                </article><!--article-->
                            </div><!--articleWrap-->
                            <?php if (!empty(cid())) { ?>
                                <div class="commentWrap shadow-around">
                                    <div class="comments">
                                        <h2><?= (!empty($count_comments)) ? $count_comments : 0 ?> <?= lang('comments') ?></h2>
                                        <?php
                                        if (!empty($comments)) {
                                            foreach ($comments as $comment) {
                                                ?>
                                                <div class="comment">
                                                    <figure>
                                                        <img src="<?= base_url() ?>assets/planet/img/commrnt01.png" alt="">
                                                    </figure>
                                                    <div class="pad" style="margin-bottom: 5%;">
                                                        <h4><?= $comment->cname ?></h4>
                                                        <div class="date-stamp"><?= date('Y-m-d h:i a', strtotime($comment->timestamp)) ?></div>
                                                        <p><?= $comment->comment ?></p>

                                                    </div>
                                                </div><!--comment-->
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div><!--comments-->
                                    <div class="comment-form">
                                        <?= flash_success() ?>
                                        <?= flash_error() ?>
                                        <h2>Leave a Reply</h2>
                                        <form action="<?= base_url('Blog/add_comment/' . $blog_id) ?>" method="post">
                                            <?= csrf() ?>
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <textarea id="comment" class="form-control" name="comment" placeholder="Comment:" required="required"></textarea>
                                                    <span class="text-red"><?php echo form_error('comment'); ?></span>
                                                </div>
                                            </div>
                                            <input class="btn" id="submit1" type="submit" value="Submit comment">
                                        </form>
                                    </div><!--reply-->
                                </div>
                            <?php } else { ?>
                                <h4 class="title"><a href="<?= base_url() ?>Login"><?= lang('login') ?></a><?= ' ' . lang('to_view_comm') ?></h4>
                                <?php } ?>
                        </div><!--conten Coloumn-->
                    </div>
                    <!--Main row-->
                </div>
                <!--container-->
            </div>
            <!--page-contentArea-->
        </main>
        <?php $this->load->view('front/planet/private/footer'); ?>
    </body>
</html>