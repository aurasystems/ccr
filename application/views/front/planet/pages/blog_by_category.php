<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
    <!--<![endif]-->
    <?php $this->load->view('front/planet/private/head'); ?>
    <body class="home">
        <?php $this->load->view('front/planet/private/quick_view'); ?>
        <!--========================================     
        Header
        ===========================================-->
        <?php $this->load->view('front/planet/private/header'); ?>
        <!--=======Page Content Area=========-->
        <main id="pageContentArea">
            <?php $this->load->view('front/planet/private/page_head'); ?>
            <div class="page-contentArea pt-50 pb-50">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-3">
                            <nav class="secondary-nav">
                                <header>
                                    <h3 style="color: #5fb7ea;"><?= lang('categories') ?></h3>
                                </header><!--Widget header-->
                                <ul>
                                    <?php
                                    if (!empty($categories)) {
                                        foreach ($categories as $cat) {
                                            ?>
                                            <li><a href="<?= base_url() ?>Blog/get_blog_by_category/<?= $cat->id ?>"><span class="p-count"><?= $cat->count ?></span><?= $cat->{'n_' . lc()} ?></a></li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </nav><!--secondary nav-->
                            <aside class="sidebar">
                                <section class="widget widget_tag_cloud">
                                    <header>
                                        <h3><?= lang('tags') ?></h3>
                                    </header><!--Widget header-->
                                    <div class="widget-content">
                                        <?php
                                        if (!empty($tags)) {
                                            foreach ($tags as $tag) {
                                                ?>
                                                <a href="<?= base_url() ?>Blog/get_blog_by_tag/<?= $tag->tag ?>"><?= $tag->tag ?></a>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </section><!--widget-->
                            </aside>
                        </div><!--sidebar Column-->
                        <!--sidebar Column-->
                        <?php
                        if (!empty($blogs)) {
                            ?>
                            <div class="col-xs-12 col-sm-8 col-md-9">
                                <div class="articleWrap">
                                    <?php
                                    foreach ($blogs as $blog) {
                                        if (!empty($blog->img)) {
                                            $image = base_url() . 'uploads/thumb/' . $blog->img;
                                        } else {
                                            $image = base_url() . 'uploads/blogs/default_blog.png';
                                        }
                                        ?>
                                        <article class="xv-article">
                                            <?php if (!empty($blog->img)) { ?>
                                                <figure>
                                                    <a href="<?= base_url() ?>Blog/get_details/<?= $blog->id ?>"><img src="<?= $image ?>" alt=""></a>
                                                </figure>
                                            <?php } ?>
                                            <!--fig-->
                                            <div class="xv-art-content mb-60 shadow-around">
                                                <header>
                                                    <a href="<?= base_url() ?>Blog/get_details/<?= $blog->id ?>">
                                                        <h2><?= $blog->title ?></h2>
                                                    </a>
                                                    <time datetime="<?= date('Y-m-d', strtotime($blog->create_time)) ?>"><?= lang('post_by') ?> <?= $blog->uname ?> <span class="date"><?= date('d', strtotime($blog->create_time)) ?></span> <span class="month"><?= date('M', strtotime($blog->create_time)) ?></span></time>
                                                </header>
                                                <!--header-->
                                                <p><?= substr($blog->content, 0, 200) ?></p>
                                            </div>
                                            <!--article content-->
                                        </article>
                                    <?php } ?>
                                    <!--article-->
                                </div>
                                <!--articleWrap-->
                                <div class="pagination text-center">
                                    <?= $links ?>
                                </div>
                                <!--pagination-->
                            </div>
                            <?php
                        }
                        ?>
                        <!--conten Coloumn-->
                    </div>
                    <!--Main row-->
                </div>
                <!--container-->
            </div>
            <!--page-contentArea-->
        </main>
        <?php $this->load->view('front/planet/private/footer'); ?>
    </body>
</html>