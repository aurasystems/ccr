<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
    <!--<![endif]-->
    <?php $this->load->view('front/planet/private/head'); ?>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/jquery.timepicker.min.css">
    <style>

        .ui-timepicker-standard {
            z-index: 1000 !important;
        }
    </style>
    <body class="home">
        <?php $this->load->view('front/planet/private/quick_view'); ?>
        <!--========================================     
        Header
        ===========================================-->
        <?php $this->load->view('front/planet/private/header'); ?>
        <!--=======Page Content Area=========-->
        <main id="pageContentArea">
            <?php $this->load->view('front/planet/private/page_head'); ?>
            <div class="page-contentArea pt-50 pb-50">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <?php if ($reserve == true) { ?>
                                <form method="POST" enctype="Multipart/form-data" action="<?= base_url() ?>Shop/add_waiting_list/<?= $product_id ?>/<?= $rent_term ?>" class="form-horizontal" role="form">
                                <?php } else if ($reserve == false) { ?>
                                    <form method="POST" enctype="Multipart/form-data" action="" class="form-horizontal" role="form">
                                    <?php } ?>
                                    <?= csrf() ?>
                                    <div class="form-group">
                                        <div class="accordian-form-wrap">
                                            <label><?= lang('quantity') ?> : <span class="text-red">*</span></label>
                                            <?php if ($reserve == true) { ?>
                                                <?php echo form_input(['type' => 'number', 'min' => 1, 'id' => 'quantity', 'name' => 'quantity', 'class' => '', 'value' => 1, 'placeholder' => lang('quantity'), 'required' => 'required', 'readonly' => 'readonly']); ?>
                                            <?php } else if ($reserve == false) { ?>
                                                <?php echo form_input(['type' => 'number', 'min' => 1, 'id' => 'quantity', 'name' => 'quantity', 'class' => '', 'value' => 1, 'placeholder' => lang('quantity'), 'required' => 'required']); ?>
                                            <?php } ?>
                                            <span class="text-red"><?php echo form_error('quantity'); ?></span>
                                        </div>
                                    </div>
                                    <?php if (!empty($product) && $product->rent_term == 1) { // day ?>
                                        <div class="form-group">
                                            <div class="accordian-form-wrap">
                                                <label><?= lang('b_from') ?> : <span class="text-red">*</span></label>
                                                <?php if ($reserve == true) { ?>
                                                    <?php echo form_input(['id' => 'b_from', 'value' => date('Y-m-d'), 'type' => 'date', 'name' => 'b_from', 'style' => 'margin-bottom:0;', 'autocomplete' => 'off', 'class' => 'date-picker', 'placeholder' => lang('b_from'), 'required' => 'required', 'readonly' => 'readonly']); ?>
                                                <?php } else if ($reserve == false) { ?>
                                                    <?php echo form_input(['id' => 'b_from', 'value' => date('Y-m-d'), 'type' => 'date', 'name' => 'b_from', 'style' => 'margin-bottom:0;', 'autocomplete' => 'off', 'class' => 'date-picker', 'placeholder' => lang('b_from'), 'required' => 'required']); ?>
                                                <?php } ?>
                                                <span class="text-red"><?php echo form_error('b_from'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="accordian-form-wrap">
                                                <label><?= lang('b_to') ?> : <span class="text-red">*</span></label>
                                                <?php if ($reserve == true) { ?>
                                                    <?php echo form_input(['id' => 'b_to', 'value' => date('Y-m-d'), 'type' => 'date', 'name' => 'b_to', 'style' => 'margin-bottom:0;', 'autocomplete' => 'off', 'class' => 'date-picker', 'placeholder' => lang('b_to'), 'required' => 'required', 'readonly' => 'readonly']); ?>
                                                <?php } else if ($reserve == false) { ?>
                                                    <?php echo form_input(['id' => 'b_to', 'value' => date('Y-m-d'), 'type' => 'date', 'name' => 'b_to', 'style' => 'margin-bottom:0;', 'autocomplete' => 'off', 'class' => 'date-picker', 'placeholder' => lang('b_to'), 'required' => 'required']); ?>
                                                <?php } ?>
                                                <span class="text-red"><?php echo form_error('b_to'); ?></span>
                                            </div>
                                        </div>
                                    <?php } elseif (!empty($product) && $product->rent_term == 2) { // hour ?>
                                        <div class="form-group">
                                            <div class="accordian-form-wrap">
                                                <label><?= lang('date') ?> : <span class="text-red">*</span></label>
                                                <?php if ($reserve == true) { ?>
                                                    <?php echo form_input(['id' => 'date', 'type' => 'date', 'name' => 'date', 'autocomplete' => 'off', 'class' => 'date-picker', 'value' => set_value('date'), 'placeholder' => lang('date'), 'required' => 'required', 'readonly' => 'readonly']); ?>
                                                <?php } else if ($reserve == false) { ?>
                                                    <?php echo form_input(['id' => 'date', 'type' => 'date', 'name' => 'date', 'autocomplete' => 'off', 'class' => 'date-picker', 'value' => set_value('date'), 'placeholder' => lang('date'), 'required' => 'required']); ?>
                                                <?php } ?>
                                                <span class="text-red"><?php echo form_error('date'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="accordian-form-wrap">
                                                <label><?= lang('b_from') ?> : <span class="text-red">*</span></label>
                                                <?php if ($reserve == true) { ?>
                                                    <?php echo form_input(['id' => 'start', 'type' => 'text', 'name' => 'b_time_from', 'style' => 'margin-bottom:0;', 'autocomplete' => 'off', 'class' => '', 'value' => set_value('b_from'), 'placeholder' => lang('b_from'), 'required' => 'required', 'readonly' => 'readonly']); ?>
                                                <?php } else if ($reserve == false) { ?>
                                                    <?php echo form_input(['id' => 'start', 'type' => 'text', 'name' => 'b_time_from', 'style' => 'margin-bottom:0;', 'autocomplete' => 'off', 'class' => '', 'value' => set_value('b_from'), 'placeholder' => lang('b_from'), 'required' => 'required']); ?>
                                                <?php } ?>
                                                <span class="text-red"><?php echo form_error('b_time_from'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="accordian-form-wrap">
                                                <label><?= lang('b_to') ?> : <span class="text-red">*</span></label>
                                                <?php if ($reserve == true) { ?>
                                                    <?php echo form_input(['id' => 'end', 'type' => 'text', 'name' => 'b_time_to', 'style' => 'margin-bottom:0;', 'autocomplete' => 'off', 'class' => '', 'value' => set_value('b_to'), 'placeholder' => lang('b_to'), 'required' => 'required', 'readonly' => 'readonly']); ?>
                                                <?php } else if ($reserve == false) { ?>
                                                    <?php echo form_input(['id' => 'end', 'type' => 'text', 'name' => 'b_time_to', 'style' => 'margin-bottom:0;', 'autocomplete' => 'off', 'class' => '', 'value' => set_value('b_to'), 'placeholder' => lang('b_to'), 'required' => 'required']); ?>
                                                <?php } ?>
                                                <span class="text-red"><?php echo form_error('b_time_to'); ?></span>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($reserve == false) { ?>
                                        <div class="col-sm-12 text-center">
                                            <div class="">
                                                <input type="submit" class="btn btn-accordian btn-blue" value="<?= lang("save") ?>">
                                            </div>
                                        </div> 
                                    <?php } ?>                                               
                                    <?php if ($reserve == true) { ?>
                                        <div class="col-sm-6 text-center">
                                            <div class="">
                                                <input type="submit" class="btn btn-accordian btn-blue" value="<?= lang("add_to_waiting_list") ?>">
                                            </div>
                                        </div> 
                                        <div class="col-sm-6 text-center">
                                            <div class="">
                                                <input onclick="choose_another_date(<?= $product_id ?>)" type="button" class="btn btn-accordian btn-blue" value="<?= lang("different_date") ?>">
                                            </div>
                                        </div>
                                    <?php } ?>                                             
                                </form>
                        </div>
                    </div>
                </div>
                <!--orderContent-->
            </div>
            <!--page-contentArea-->
        </main>
        <?php $this->load->view('front/planet/private/footer'); ?>
        <script src="<?= base_url() ?>assets/js/jquery.timepicker.min.js"></script>
        <script>
        function waiting_list(product_id, b_from, b_to) {
            window.location = "<?= base_url() ?>Shop/add_waiting_list/" + product_id + '/' + b_from + '/' + b_to;
        }
        function choose_another_date(product_id) {
            window.location = "<?= base_url() ?>Shop/check_availabilty/" + product_id;
        }
        </script>
        <script>
            $(document).ready(function () {
                $('#start, #end').timepicker({
                    timeFormat: 'h:mm p',
                    interval: 30,
                    startTime: '10:00',
                    dynamic: true,
                    dropdown: true,
                    scrollbar: true,
                    onSelect: function (dateText, inst) {
                        $("#datepickerdepone").datepicker('option', "minDate", dateText);
                    }
                });
            });
        </script>
    </body>
</html>