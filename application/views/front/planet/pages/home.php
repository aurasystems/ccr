<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
    <!--<![endif]-->
    <?php $this->load->view('front/planet/private/head'); ?>
    <body class="home">
        <?php $this->load->view('front/planet/private/quick_view'); ?>
        <!--========================================
        Header
        ===========================================--> 
        <?php $this->load->view('front/planet/private/header'); ?>
        <div class="clearfix"></div>   

        <!--========================================
        Slider Area
        ===========================================-->
        <div class="xv-slider-wrap style2 pt-60 pb-60">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-md-offset-2">
                        <div class="xv-slider">
                            <div class="owl-carousel slider_controls1 style smSlider"  data-dots="false" data-prev="fa fa-angle-left" data-next="fa fa-angle-right" data-slides="1" data-slides-md="1" data-slides-sm="1" data-margin="0" data-loop="true" data-nav="true">
                                <?php
                                if (!empty($sliders)) {
                                    foreach ($sliders as $slide) {
                                        ?>
                                        <div class="xv-slide" style="background-image:url(<?= base_url() ?>uploads/slider/<?= $slide->img ?>);">
                                            
                                        </div><!--xv-slide-->
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div><!--xv-slider-->
                    </div>

                </div><!--row-->
            </div><!--container-->
        </div><!--slider Wrap-->
        <!--=======Page Content Area=========-->   
        <main id="pageContentArea">
            <!--=================================
            new items
            =================================-->
            <div class="container">
                <div class="product-overview pt-50 pb-50">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 hidden-xs">
                            <?php $this->load->view('front/planet/private/aside'); ?>
                        </div>
                        <div class="col-xs-12 col-sm-9">  
                            <div class="xv-product-slides grid-view products" data-thumbnail="figure .xv-superimage"  data-product=".xv-product-unit">
                                <header class="sec-heading text-center">
                                    <h2><span>Featured</span></h2>
                                </header><!--header-->
                                <div class="row">
                                    <?php
                                    if (!empty($products)) {
                                        foreach ($products as $product) {
                                            if (!empty($product->img)) {
                                                $image = base_url() . 'uploads/thumb/' . $product->img;
                                            } else {
                                                $image = base_url() . 'uploads/products/default_product.png';
                                            }
                                            ?>
                                            <div class="xv-product-unit">
                                                <div class="xv-product shadow-around">
                                                    <figure>
                                                        <a href="<?= base_url() ?>Shop/item_details/<?= $product->id ?>"><img class="xv-superimage" src="<?= $image ?>" alt="" width="280" height="265" /></a>
                                                    </figure>
                                                    <div class="xv-product-content">
                                                        <h3><a href="<?= base_url() ?>Shop/item_details/<?= $product->id ?>"><?= substr($product->{'n_' . lc()}, 0, 20) ?></a></h3>
                                                        <div class="xv-rating stars-5"></div>
                                                        <span class="xv-price"><?= lang('egp') ?> <?= $product->rent_price ?></span>
                                                    </div><!--xv-product-content-->
                                                </div><!--xv-product(list-view)-->
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="pagination text-center pt-10 pb-05">
                                <?= $links ?>
                            </div>
                        </div>
                    </div><!--row-->
                </div><!--product overview-->
            </div><!--container-->
            <!--========================================
            sign up
            ===========================================-->
            <?php $this->load->view('front/planet/private/subscribe'); ?>
        </main> <!--pageContentArea ends-->   
        <!--========================================
        Footer
        ===========================================-->
        <?php $this->load->view('front/planet/private/footer'); ?>
        <!--=================================
        </body>
        </html>
