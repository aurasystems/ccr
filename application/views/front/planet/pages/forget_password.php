<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
    <!--<![endif]-->
    <?php $this->load->view('front/planet/private/head'); ?>

    <body class="home">
        <?php $this->load->view('front/planet/private/quick_view'); ?>
        <!--========================================     
        Header
        ===========================================-->
        <?php $this->load->view('front/planet/private/header'); ?>
        <!--=======Page Content Area=========-->
        <main id="pageContentArea">
            <?php $this->load->view('front/planet/private/page_head'); ?>
            <div class="page-contentArea pt-50 pb-50">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <h2><?= (!empty($title)) ? $title : '' ?></h2>
                            <form method="post" action="<?= base_url('Login/do_reset_password') ?>" id="login">
                                <?= csrf() ?>
                                <p class="text-center"> <img src="<?= base_url() ?>assets/planet/img/basic/padlock106.png" alt=""></p>
                                <input type="text" name="email" placeholder="<?= lang('email') ?>" id="email" required="required">
                                <input type="submit" name="submit" value="<?= lang('sign_in') ?>" class="btn-blue btn">				
                            </form>
                            <!--Form-->
                        </div>
                    </div>
                </div>
                <!--orderContent-->
            </div>
            <!--page-contentArea-->
        </main>
        <?php $this->load->view('front/planet/private/footer'); ?>
    </body>
</html>