<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
    <!--<![endif]-->
    <?php $this->load->view('front/planet/private/head'); ?>

    <body class="home">
        <?php $this->load->view('front/planet/private/quick_view'); ?>
        <!--========================================     
        Header
        ===========================================-->
        <?php $this->load->view('front/planet/private/header'); ?>
        <!--=======Page Content Area=========-->
        <main id="pageContentArea">
            <?php $this->load->view('front/planet/private/page_head'); ?>
            <div class="container">
                <div class="single-product-detail pt-60 pb-30">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="product-slider-wrap text-center shadow-around">
                                <div class="product-slide">
                                    <?php
                                    if (!empty($product->img)) {
                                        $image = base_url() . 'uploads/thumb/' . $product->img;
                                    } else {
                                        $image = base_url() . 'uploads/products/default_product.png';
                                    }
                                    ?>
                                    <figure>
                                        <img src="<?= $image ?>" width="486" height="279" alt="">
                                    </figure>
                                </div>
                                <!--product-slide
                                <div class="product-slider styleDetail">
                                    <div class="detail-slider">
                                        <div class="owl-carousel slider_controls5" data-dots="false" data-prev="fa fa-angle-left" data-next="fa fa-angle-right" data-margin="20" data-slides="4" data-slides-md="3" data-slides-sm="1" data-loop="false" data-nav="true">
                                            <figure class="border-around">
                                                <a href="#"><img src="<?= base_url() . "uploads/products/" . $product->img ?>" alt=""></a>
                                            </figure>
    
                                            <figure class="border-around">
                                                <a href="#"><img src="<?= base_url() . "uploads/products/" . $product->img ?>" alt=""></a>
                                            </figure>
    
    
                                            <figure class="border-around">
                                                <a href="#"><img src="<?= base_url() . "uploads/products/" . $product->img ?>" alt=""></a>
                                            </figure>
                                            <figure class="border-around">
                                                <a href="#"><img src="<?= base_url() . "uploads/products/" . $product->img ?>" alt=""></a>
                                            </figure>
                                            <figure class="border-around">
                                                <a href="#"><img src="<?= base_url() . "uploads/products/" . $product->img ?>" alt=""></a>
                                            </figure>
                                        </div>
                                    </div>
                                </div>-->
                            </div>
                            <!--column-6-->
                        </div>
                        <!--column-6-->
                        <div class="col-xs-12 col-md-6">
                            <div class="single-product-overview">
                                <h2><?= $product->n_en ?></h2>
                                <?php if (!empty($product->description)) { ?>
                                    <p class="no-mar" style="margin-top: 30px;"><?= $product->description ?></p>
                                <?php } ?>
                                <ul class="product-description mt-35 mb-35">
                                    <li><span><?= lang("category") ?></span>: <?= $cat_n ?></li>
                                </ul>
                                <div class="cart-options">
                                    <span class="xv-price" style="color: #fdb913; font-size: 24px;"><?= lang('egp') ?> <?= $product->rent_price ?> </span>
                                    <ul class="cart-buttons mt-45 clearfix">
                                        <li>
                                        </li>
                                    </ul>
                                    <!--cart-buttons-->
                                    <?php if (cid() != '') { ?>
                                    <a href="<?= base_url("Shop/check_availabilty/$product->id") ?>" class="btn btn-continue"><?= lang('add_to_cart') ?></a>
                                    <?php } else { ?>
                                    <h4 class="title text-center"><a href="<?= base_url() ?>Login"><?= lang('login') ?></a><?= ' ' . lang('to_add_cart') ?></h4>
                                    <?php } ?>
                                </div>
                                <!--cart-options-->
                            </div>
                            <!--single-product-overview-->
                        </div>
                        <!--column-6-->
                    </div>
                    <!--single-product-detail-->
                    <div class="tabs-pane mt-60">
                        <ul class="tab-sections">
                            <li class="active"><a href="#tab01" class="btn-cart">Overview</a></li>
                            <li><a href="#tab02" class="btn-cart">Specification</a></li>
                            <li><a href="#tab03" class="btn-cart">Resources</a></li>
                            <li><a href="#tab04" class="btn-cart">Description</a></li>
                            <li><a href="#tab06" class="btn-cart">Review</a></li>
                        </ul>
                        <div class="tab-panels">
                            <div id="tab01" class="tab-content active">
                                <?= $product->overview ?>
                            </div>
                            <!--tab content-->
                            <div id="tab02" class="tab-content">
                                <?= $product->specifications ?>
                            </div>
                            <!--tab content-->
                            <div id="tab03" class="tab-content">
                                <?= $product->resources ?>
                            </div>
                            <!--tab content-->
                            <div id="tab04" class="tab-content">
                                <?= $product->description ?>
                            </div>
                            <!--tab content-->
                            <div id="tab06" class="tab-content">
                                <?php if (!empty(cid())) { ?>
                                    <div class="commentWrap shadow-around">
                                        <div class="comments">
                                            <h2><?= (!empty($rev_count)) ? $rev_count : 0 ?> <?= lang('reviews') ?></h2>
                                            <?php
                                            if (!empty($reviews)) {
                                                foreach ($reviews as $review) {
                                                    ?>
                                                    <div class="comment">
                                                        <figure>
                                                            <img src="<?= base_url() ?>assets/planet/img/commrnt01.png" alt="">
                                                        </figure>
                                                        <div class="pad" style="margin-bottom: 5%;">
                                                            <h4><?= $review->client_name_en ?></h4>
                                                            <div class="date-stamp"><?= date('Y-m-d h:i a', strtotime($review->timestamp)) ?></div>
                                                            <p><?= $review->description ?></p>

                                                        </div>
                                                    </div><!--comment-->
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div><!--comments-->
                                        <div class="comment-form">
                                            <?= flash_success() ?>
                                            <?= flash_error() ?>
                                            <h2>Leave a Reply</h2>
                                            <form action="<?= base_url('Shop/add_review/' . $product->id) ?>" method="post">
                                                <?= csrf() ?>
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <textarea id="comment" class="form-control" name="message" placeholder="Review:" required="required"></textarea>
                                                        <span class="text-red"><?php echo form_error('message'); ?></span>
                                                    </div>
                                                </div>
                                                <input class="btn" id="submit1" type="submit" value="Submit Now">
                                            </form>
                                        </div><!--reply-->
                                    </div>
                                <?php } else { ?>
                                    <h4 class="title"><a href="<?= base_url() ?>Login"><?= lang('login') ?></a><?= ' ' . lang('to_review') ?></h4>
                                <?php } ?>
                            </div>
                            <!--tab content-->
                        </div>
                    </div>
                    <!--tabs pane-->
                </div>
                <!--container-->
            </div>
            <!--========================================
related products
===========================================-->
            <div class="container">
                <div class="xv-featured pt-25 ">
                    <header class="sec-heading text-center">
                        <h2><span>Related Product</span></h2>
                    </header>
                    <!--header-->
                    <div class="xv-product-slides mt-25 mb-25">
                        <div class="owl-carousel slider_controls1" data-dots="false" data-prev="fa fa-angle-left" data-next="fa fa-angle-right" data-slides="4" data-slides-md="2" data-slides-sm="1" data-margin="24" data-loop="true" data-nav="true">
                            <?php
                            if (!empty($related_prods)) {
                                foreach ($related_prods as $product) {
                                    ?>
                                    <div class="xv-product style shadow-around">
                                        <?php
                                        if (!empty($product->img)) {
                                            $image = base_url() . 'uploads/thumb/' . $product->img;
                                        } else {
                                            $image = base_url() . 'uploads/products/default_product.png';
                                        }
                                        ?>
                                        <figure>
                                            <a href="<?= base_url() ?>Shop/item_details/<?= $product->id ?>"><img class="xv-superimage" src="<?= $image ?>" width="280" height="265" alt="" /></a>
                                        </figure>
                                        <!--figure-->
                                        <div class="xv-product-content">
                                            <h3><a href="<?= base_url() ?>Shop/item_details/<?= $product->id ?>"><?= substr($product->{'n_' . lc()}, 0, 20) ?></a></h3>
                                            <span class="xv-price"><?= lang('egp') ?> <?= $product->rent_price ?> </span>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                            <!--xv-product-->
                        </div>
                        <!--owl carousel-->
                    </div>
                </div>
                <!--xv-featured-->
            </div>
            <!--container-->
            <!--page-contentArea-->
        </main>
        <?php $this->load->view('front/planet/private/footer'); ?>
    </body>
</html>