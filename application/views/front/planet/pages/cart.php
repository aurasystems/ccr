<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
    <!--<![endif]-->
    <?php $this->load->view('front/planet/private/head'); ?>
    <body class="home">
        <?php $this->load->view('front/planet/private/quick_view'); ?>
        <!--========================================     
        Header
        ===========================================-->
        <?php $this->load->view('front/planet/private/header'); ?>
        <!--=======Page Content Area=========-->   
        <main id="pageContentArea">
            <!--========================================
            Page Head
            ===========================================-->
            <?php $this->load->view('front/planet/private/page_head'); ?>
            <!--========================================
            cart content
            ===========================================-->
            <div class="container">
                <?php if (!empty(cid())) { ?>
                    <div class="xv-cart pt-60">
                        <div class="xv-cart-top pb-45">
                            <div class="table-responsive cart-cal  text-center">
                                <div class="alert alert-danger" role="alert" style="display:none;" id="booking-error"> 
                                    <i class="icon ti-na"></i><strong  id="checkout-err"></strong>
                                </div>
                                <table class="table">
                                    <thead>
                                        <tr class="table-head">
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Sub Price</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="shadow-around">
                                        <?php
                                        $subtotal = 0;
                                        if (!empty($items)) {
                                            foreach ($items as $item) {
                                                $product = get_product_details($item->product_id);
                                                if (!empty($product)) {
                                                    $quant = get_quantity(cid(), $item->id);
                                                    ?>
                                                    <tr class="table-body">
                                                        <td><figure><img src="<?= base_url() ?>uploads/thumb/<?= $product->img ?>" alt="" width="171" height="97"/></figure></td>
                                                        <td>
                                                            <div class="cart-wrappper text-left">
                                                                <h6 style="color: #4f7594;"><?= $product->{'n_' . lc()} ?></h6>
                                                            </div>                             
                                                        </td> 
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>
                                                            <div class="quantity-control">
                                                                <button onclick='increment("<?= $item->id ?>", "<?= $product->id ?>", "<?= $product->rent_price ?>")' class="btn-cart btn-square btn-plus btn-qty"><i class="fa fa-plus"></i></button>
                                                                <input id="number<?= $product->id ?>" type="text" value="<?= $quant ?>" data-min="1" data-minalert="Minimum limit reached" data-invalid="Enter valid quantity">
                                                                <button onclick='decrement("<?= $item->id ?>", "<?= $product->id ?>", "<?= $product->rent_price ?>")' class="btn-cart btn-square btn-minus btn-qty"><i class="fa fa-minus"></i></button>
                                                            </div>
                                                        </td>
                                                        <td><span class="cart-price"><?= lang('egp') ?> <?= $product->rent_price ?></span></td>
                                                        <td><span class="cart-price count" id="total_price"><?= lang('egp') ?> <?= $quant * $product->rent_price ?></span></td>
                                                        <td>
                                                            <ul class="cart-action">
                                                                <li><a href="javascript:void(0);" onclick="delete_item('<?= $item->id ?>', '<?= $product->id ?>')" class="btn-cart btn-square btn-blue"><i class="fa fa-trash-o"></i></a></li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $subtotal += $quant * $product->rent_price;
                                                }
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>

                            </div>
                        </div><!--xv-cart-top-->
                        <div class="xv-accordian pt-15 pb-55">
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-md-offset-1">

                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <table class="table">
                                        <tr class="Sub-Total">
                                            <th>Sub Total</th>
                                            <td><span id="sub-total" class="count"><?= lang('egp') ?> <?= $subtotal ?></span></td>
                                        </tr>
                                        <tr class="Total">
                                            <th>TOTAL</th>
                                            <td style="color: #616161;"><?= lang('egp') ?> <span id="final_total" class="count"><?= $subtotal ?></span></td>
                                        </tr>
                                        <tr class="Continue-Shopping">
                                            <th><button onclick="go_shop()" class="btn btn-send btn-pink">Continue Shopping</button></th>
                                            <td><a href="<?= base_url('Checkout') ?>" class="btn btn-send btn-pink">CHECKOUT</a></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>    
                        </div>
                        <?php
                    } else {
                        ?>
                        <h4 class="title text-center"><a href="<?= base_url() ?>Login"><?= lang('login') ?></a><?= ' ' . lang('to_view_cart') ?></h4>
                    </div>
                    <?php
                }
                ?>
            </div><!--xv-cart-->
        </main>
        <?php $this->load->view('front/planet/private/footer'); ?>
        <script>
            function increment(id, product_id, rent_prict) {
                var postData = {
                    cart_id: id,
                    product_id: product_id,
<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                };
                $.ajax({
                    url: "<?= base_url() ?>Cart/increment",
                    method: "POST",
                    dataType: "json",
                    data: postData,
                    success: function (data) {
                        $('#number' + product_id).val(data.count);
                        $('#sub-total').html(data.sub_total);
                        $('#total_price').html(data.sub_total);
                        $('#final_total').html(data.sub_total);
                        $('#booking-error').css('display', 'none');
                        if (data.not_ava == false) {
                            $('#total-perQuant' + product_id).html(data.count * rent_prict);
                        }
                        if (data.not_ava == true) {
                            $('#booking-error').css('display', 'block');
                            $('#checkout-err').html(data.msg);
                        }
                    }
                });
            }
            function decrement(id, product_id, rent_prict) {
                var postData = {
                    cart_id: id,
                    product_id: product_id,
<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                };
                $.ajax({
                    url: "<?= base_url() ?>Cart/decrement",
                    method: "POST",
                    dataType: "json",
                    data: postData,
                    success: function (data) {
                        $('#number' + product_id).val(data.count);
                        $('#sub-total').html(data.sub_total);
                        $('#total-perQuant' + product_id).html(data.count * rent_prict);
                        $('#total_price').html(data.sub_total);
                        $('#final_total').html(data.sub_total);
                        $('#booking-error').css('display', 'none');
                    }
                });
            }
            function go_shop() {
                window.location = "<?= base_url() ?>Shop";
            }
        </script>
    </body>
</html>