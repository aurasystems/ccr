<?php $lang_ = $this->session->userdata('language') ?>
<?php $lang_ = $this->session->userdata('language') ?>
<style>

@media screen and (min-width: 1024px) {
  .sidebar-collapsed .topbar {
    margin-left: <?= $lang_ == 'english'?'-50':''?>;
  }
}

</style>

<!-- BEGIN TOPBAR -->
<div  style="margin-right:<?=$lang_ == 'arabic'? '100px':'';?>" class="topbar" id="top_bar">
    <div class="header-left">
        <div class="topnav" id ="mySidebar">
            <a class="menutoggle" href="#" onclick="close_open()" data-toggle="sidebar-collapsed"><span class="menu__handle"><span><?= lang('lang_menu') ?></span></span></a>
        </div>
    </div>
    <div class="header-right">
        <ul class="header-menu nav navbar-nav">
            <!-- BEGIN LANGUAGE DROPDOWN -->
            <li class="dropdown" id="language-header">
                <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <i class="icon-globe"></i>
                    <span><?= lang('lang_language') ?></span>
                </a>
                <ul class="dropdown-menu">
                    <?php
                    $languages = [];
                    $dfLang = lang('lang_name');
                    $lang['arabic'] = ['name' => 'عربي', 'url' => 'arabic', 'image' => 'Egypt.png'];
                    $lang['english'] = ['name' => 'ENGLISH', 'url' => 'english', 'image' => 'usa.png'];
                    ?>
                    <?php
                    foreach ($lang as $k => $v) :
                        if ($k !== $dfLang) {
                            ?>
                            <li>
                                <a href="<?= site_url('admin/Lang/' . $lang[$k]['url']) ?>"><img
                                        src="<?= base_url() ?>assets/images/flags/<?= $lang[$k]['image'] ?>"
                                        alt="<?= $lang[$k]['name'] ?>">
                                    <span><?= $lang[$k]['name'] ?></span></a>
                            </li>
                            <?php
                        }
                    endforeach;
                    ?>
                    <li>
                        <a href="javascript:void(0)"><img
                                src="<?= base_url() ?>assets/images/flags/<?= $lang[$dfLang]['image'] ?>"
                                alt="<?= $lang[$dfLang]['name'] ?>">
                            <span><?= $lang[$dfLang]['name'] ?></span></a>
                    </li>
                </ul>
            </li>
            <!-- END LANGUAGE DROPDOWN -->
            <!-- BEGIN NOTIFICATION DROPDOWN -->
            <li class="dropdown" id="notifications-header">
                <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <i class="icon-bell"></i>
                    <span class="badge badge-danger badge-header ntf_w_count" style="top: 8px;"></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="dropdown-header clearfix">
                        <p class="pull-left"><span class="ntf_w_count"></span> <?= lang('lang_pending_notifications') ?></p>
                    </li>
                    <li>
                        <ul class="dropdown-menu-list withScroll" data-height="220" id="ntf_w_body">

                        </ul>
                    </li>
                    <li class="dropdown-footer clearfix hidden">
                        <a href="<?= base_url('admin/Notifications') ?>"
                           class="pull-left"><?= lang('lang_see_all_notif') ?></a>
                        <a href="<?= base_url('admin/Notifications') ?>" class="pull-right">
                            <i class="icon-bell"></i>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- END NOTIFICATION DROPDOWN -->
            <!-- BEGIN USER DROPDOWN -->
            <li class="dropdown" id="user-header">
                <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <span class="username"><?= lang('lang_hello_user') ?> <?= $this->session->userdata('user_name_' . lc()) ?></span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<?= base_url('admin/Dashboard/account_settings') ?>"><i class="icon-settings"></i><span><?= lang('lang_account_settings') ?></span></a>
                    </li>
                    <li>
                        <a href="<?= base_url('admin/Dashboard/Logout') ?>"><i class="icon-logout"></i><span><?= lang('lang_logout') ?></span></a>
                    </li>
                </ul>
            </li>
            <!-- END USER DROPDOWN -->
        </ul>
    </div>
    <!-- header-right -->
</div>
<!-- END TOPBAR -->
<script>

function close_open() {
    <?php if($lang_=='arabic'){?>
      
 // document.getElementById("top_bar").style.width = "0";
  if(document.getElementById("top_bar").style.marginRight == "100px")
  {
  document.getElementById("top_bar").style.marginRight = "-140";
  var myObj = document.getElementsByClassName('page-content');
        for(var i=0; i<myObj.length; i++){
        myObj[i].style.marginRight = "-190";
    }
  }
  else
  {
  document.getElementById("top_bar").style.marginRight = "100"
  var myObj = document.getElementsByClassName('page-content');
        for(var i=0; i<myObj.length; i++){
        myObj[i].style.marginRight = "0";
    }
  }
    <?php }?>
}
</script>