<a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a> 
<script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.11.1.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/gsap/main-gsap.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/jquery-cookies/jquery.cookies.min.js"></script> <!-- Jquery Cookies, for theme -->
<script src="<?= base_url() ?>assets/plugins/jquery-block-ui/jquery.blockUI.min.js"></script> <!-- simulate synchronous behavior when using AJAX -->
<script src="<?= base_url() ?>assets/plugins/translate/jqueryTranslator.min.js"></script> <!-- Translate Plugin with JSON data -->
<script src="<?= base_url() ?>assets/plugins/bootbox/bootbox.min.js"></script> <!-- Modal with Validation -->
<script src="<?= base_url() ?>assets/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script> <!-- Custom Scrollbar sidebar -->
<script src="<?= base_url() ?>assets/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script> <!-- Show Dropdown on Mouseover -->
<script src="<?= base_url() ?>assets/plugins/charts-sparkline/sparkline.min.js"></script> <!-- Charts Sparkline -->
<script src="<?= base_url() ?>assets/plugins/retina/retina.min.js"></script> <!-- Retina Display -->
<script src="<?= base_url() ?>assets/plugins/select2/select2.min.js"></script> <!-- Select Inputs -->
<script src="<?= base_url() ?>assets/plugins/icheck/icheck.min.js"></script> <!-- Checkbox & Radio Inputs -->
<script src="<?= base_url() ?>assets/plugins/backstretch/backstretch.min.js"></script> <!-- Background Image -->
<script src="<?= base_url() ?>assets/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script> <!-- Animated Progress Bar -->
<script src="<?= base_url() ?>assets/plugins/charts-chartjs/Chart.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/dropzone/dropzone.min.js"></script>  <!-- Upload Image & File in dropzone -->
<script src="<?= base_url() ?>assets/js/builder.js"></script> <!-- Theme Builder -->
<script src="<?= base_url() ?>assets/js/sidebar_hover.js"></script> <!-- Sidebar on Hover -->
<script src="<?= base_url() ?>assets/js/widgets/notes.js"></script> <!-- Notes Widget -->
<script src="<?= base_url() ?>assets/js/quickview.js"></script> <!-- Chat Script -->
<script src="<?= base_url() ?>assets/js/pages/search.js"></script> <!-- Search Script -->
<script src="<?= base_url() ?>assets/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script> <!-- Select Inputs -->
<!--<script src="<?= base_url() ?>assets/plugins/ion-sound/js/ion.sound.min.js"></script>  Sound Script -->
<script>
var csrf_name = "<?= $this->security->get_csrf_token_name()?>";
var csrf_hash = '<?= $this->security->get_csrf_hash() ?>';
    function get_notifications() {
        $.ajax({
            url: '<?= base_url('admin/Notifications/get_ajax') ?>',
            type: "POST",
            data: {
                <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: "JSON",
            success: function (data) {
                if (data.status == 'success') {
                    var past_count = parseInt($(".ntf_w_count").html());
//                if (past_count && past_count < data.count) {
//                    ion.sound.play("glass");
//                }
                    $(".ntf_w_count").html(data.count);
                    $("#ntf_w_body").html(data.html);
                } else {
                    window.location = '<?= base_url('admin/Login') ?>';
                }
            }
        });
    }

    function set_seen(ntf_id, url) {
        $.ajax({
            url: '<?= base_url('admin/Notifications/set_seen') ?>',
            type: "POST",
            data: {
                ntf_id: ntf_id,
<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: "JSON",
            success: function (success) {
                window.location = url;
            }
        });
    }

    $(document).ready(function () {
//        ion.sound({
//            sounds: [{name: "glass"}],
//            volume: 0.5,
//            path: "<?= base_url() ?>assets/plugins/ion-sound/sounds/",
//            preload: true
//        });
        setTimeout(function () {
           // get_notifications();
        }, 500);
    });

</script>
<!-- Main Plugin Initialization Script -->
<?php $this->load->view('admin/private/plugins_js'); ?>
<?php $this->load->view('admin/private/application_script'); ?><!-- Main Application Script -->

