<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="admin-themes-lab">
    <meta name="author" content="themes-lab">
    <title><?= MY_APP_NAME ?> | <?= $title ?></title>
    <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/theme.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/ui.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/custom.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/plugins/dropzone/dropzone.min.css" rel="stylesheet">
    <script src="<?=base_url()?>assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>