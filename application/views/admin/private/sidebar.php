<!-- BEGIN SIDEBAR -->

<div  class="sidebar">
    <div style="height:90px;" class="logopanel">
        <h1>
        <?php $settings = settings('all', 3);
             if(!empty($settings->logo)) { ?>
             <h1>
                    <img style="width: 50%; height: 50px;" src="<?= base_url() ?>uploads/settings/<?= $settings->logo ?>" alt="Site Logo">
            </h1>
        <?php } ?>
        </h1>
    </div>
    <div style="margin-top:40px;"  class="sidebar-inner">
        <ul  class="nav nav-sidebar">
            <!-- For Dashboard -->
            <li class=" nav <?= sidebar_parent("dashboard") ?>">
                <a href="<?= base_url('admin/Dashboard') ?>"><i class="icon-home"></i><span data-translate="dashboard"><?= lang('lang_dashboard') ?></span></a>
            </li>
            <!-- For Accounts/Access levels -->
            <?php if (or_pb(["users", "access_levels", "payroll"])) { ?>
                <li class="nav-parent <?= sidebar_parent(["users", "add_user", "access_levels", "payroll"]) ?>">
                    <a href="#"><i class="fa fa-users"></i><span data-translate="Users"><?= lang('account_management') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <?php if (get_p("users", "v")) { ?>
                            <li class="<?= sidebar_active("users") ?>"><a href="<?= base_url() ?>admin/User"><?= lang('accounts') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("users", "c")) { ?>
                            <li class="<?= sidebar_active("add_user") ?>"><a href="<?= base_url() ?>admin/User/add"><?= lang('add_account') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("payroll", "v")) { ?>
                            <li class="<?= sidebar_active("payroll") ?>"><a href="<?= base_url() ?>admin/Salary"><?= lang('payroll') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("access_levels", "v")) { ?>
                            <li class="<?= sidebar_active("access_levels") ?>"><a href="<?= base_url() ?>admin/Access_levels"><?= lang('access_rights') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <!-- For Utilities -->
            <?php if (or_pb(["settings", "email_settings", "sms_settings", "newsletters_settings", "locations", "brands", "categories", "decline_reasons", "check_types", "technicians_types", "blog_categories", "questions_types"])) { ?>
                <li class="nav-parent <?= sidebar_parent(["settings", "email_settings", "sms_settings", "newsletters_settings", "locations", "brands", "categories", "decline_reasons", "check_types", "technicians_types", "blog_categories", "questions_types"]) ?>">
                    <a href="#"><i class="fa fa-database"></i><span data-translate="Utilities"><?= lang('utilities') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <?php if (get_p("settings", "v")) { ?>
                            <li class="<?= sidebar_active("settings") ?>"><a href="<?= base_url('admin/Settings') ?>"><?= lang('settings') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("email_settings", "v")) { ?>
                            <li class="<?= sidebar_active("email_settings") ?>"><a href="<?= base_url('admin/Email_settings') ?>"><?= lang('email_settings') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("sms_settings", "v")) { ?>
                            <li class="<?= sidebar_active("sms_settings") ?>"><a href="<?= base_url('admin/Sms_settings') ?>"><?= lang('sms_settings') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("newsletters_settings", "v")) { ?>
                            <li class="<?= sidebar_active("newsletters_settings") ?>"><a href="<?= base_url('admin/Newsletters_settings') ?>"><?= lang('newsletters_settings') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("locations", "v")) { ?>
                            <li class="<?= sidebar_active("locations") ?>"><a href="<?= base_url('admin/Locations') ?> "><?= lang('locations') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("brands", "v")) { ?>
                            <li class="<?= sidebar_active("brands") ?>"><a href="<?= base_url('admin/Brands') ?>"><?= lang('brands') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("categories", "v")) { ?>
                            <li class="<?= sidebar_active("categories") ?>"><a href="<?= base_url('admin/Categories') ?>"><?= lang('categories') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("decline_reasons", "v")) { ?>
                            <li class="<?= sidebar_active("decline_reasons") ?>"><a href="<?= base_url('admin/Decline_reasons') ?>"><?= lang('decline_reasons') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("check_types", "v")) { ?>
                            <li class="<?= sidebar_active("check_types") ?>"><a href="<?= base_url('admin/Check_types') ?>"><?= lang('check_types') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("technicians_types", "v")) { ?>
                            <li class="<?= sidebar_active("technicians_types") ?>"><a href="<?= base_url('admin/Technicians_types') ?>"><?= lang('technicians_types') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("blog_categories", "v")) { ?>
                            <li class="<?= sidebar_active("blog_categories") ?>"><a href="<?= base_url('admin/Blog/categories') ?>"><?= lang('blog_categories') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("questions_types", "v")) { ?>
                            <li class="<?= sidebar_active("questions_types") ?>"><a href="<?= base_url('admin/Settings/questions_types') ?>"><?= lang('questions_types') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <!-- For Clients -->
            <?php if (get_p("clients", "v")) { ?>
                <li class="nav-parent <?= sidebar_parent(["clients", "add_client", "edit_client", "approved_clients", "unapproved_clients", "waiting_approve"]) ?>">
                    <a href="#"><i class="fa fa-users"></i><span data-translate="Settings"><?= lang('clients') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <?php if (get_p("clients", "c")) { ?>
                            <li class="<?= sidebar_active("add_client") ?>"><a href="<?= base_url() ?>admin/Clients/add_client"><?= lang('add_client') ?></a></li>
                        <?php } ?>
                        <li class="<?= sidebar_active("waiting_approve") ?>"><a href="<?= base_url() ?>admin/Clients/waiting_approve"><?= lang('waiting_approve') ?></a></li>
                        <li class="<?= sidebar_active("approved_clients") ?>"><a href="<?= base_url() ?>admin/Clients/approved"><?= lang('approved') ?></a></li>
                        <li class="<?= sidebar_active("unapproved_clients") ?>"><a href="<?= base_url() ?>admin/Clients/unapproved"><?= lang('unapproved') ?></a></li>
                    </ul>
                </li>
            <?php } ?>
            <!-- For Vouchers -->
            <?php if (get_p("vouchers", "v")) { ?>
                <li class="nav-parent <?= sidebar_parent(["vouchers", "add_voucher"]) ?>">
                    <a href="#"><i class="fa fa-gift"></i><span data-translate="Settings"><?= lang('vouchers') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <?php if (get_p("vouchers", "c")) { ?>
                            <li class="<?= sidebar_active("add_voucher") ?>"><a href="<?= base_url() ?>admin/Vouchers/add"><?= lang('add') ?></a></li>
                        <?php } ?>
                        <li class="<?= sidebar_active("vouchers") ?>"><a href="<?= base_url() ?>admin/Vouchers"><?= lang('vouchers') ?></a></li>
                    </ul>
                </li>
            <?php } ?>
            <!-- For Products -->
            <?php if (get_p("products", "v") || get_p("products", "c")) { ?>
                <li class="nav-parent <?= sidebar_parent(["products", "add_product", "edit_product"]) ?>">
                    <a href="#"><i class="fa fa-sitemap"></i><span data-translate="Products"><?= lang('products') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <?php if (get_p("products", "c")) { ?>
                            <li class="<?= sidebar_active("add_product") ?>"><a href="<?= base_url() ?>admin/Products/add"><?= lang('add_product') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("products", "v")) { ?>
                            <li class="<?= sidebar_active("products") ?>"><a href="<?= base_url() ?>admin/Products"><?= lang('products') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <!-- For Studio -->
            <?php if (get_p("studios", "v") || get_p("studios", "c")) { ?>
                <li class="nav-parent <?= sidebar_parent(["studios", "add_studio", "edit_studio"]) ?>">
                    <a href="#"><i class="fa fa-building"></i><span data-translate="Studios"><?= lang('studios') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <?php if (get_p("studios", "c")) { ?>
                            <li class="<?= sidebar_active("add_studio") ?>"><a href="<?= base_url() ?>admin/Studios/add"><?= lang('add_studio') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("studios", "v")) { ?>
                            <li class="<?= sidebar_active("studios") ?>"><a href="<?= base_url() ?>admin/Studios"><?= lang('studios') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <!-- For Stock -->
            <?php if (get_p("stock", "v") || get_p("stock", "c")) { ?>
                <li class="nav-parent <?= sidebar_parent(["stock"]) ?>">
                    <a href="#"><i class="fa fa-cubes"></i><span data-translate="stock"><?= lang('stock') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <?php if (get_p("stock", "v")) { ?>
                            <li class="<?= sidebar_active("stock") ?>"><a href="<?= base_url() ?>admin/Stock"><?= lang('stock') ?></a></li>
                        <?php } ?>

                    </ul>
                </li>
            <?php } ?>
            <!-- For Technicians -->
            <?php if (get_p("technicians", "v") || get_p("technicians", "c")) { ?>
                <li class="nav-parent <?= sidebar_parent(["technicians", "add_technician", "edit_technician"]) ?>">
                    <a href="#"><i class="fa fa-users"></i><span data-translate="Studios"><?= lang('technicians') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <?php if (get_p("technicians", "c")) { ?>
                            <li class="<?= sidebar_active("add_technician") ?>"><a href="<?= base_url() ?>admin/Technicians/add"><?= lang('add_technician') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("technicians", "v")) { ?>
                            <li class="<?= sidebar_active("technicians") ?>"><a href="<?= base_url() ?>admin/Technicians"><?= lang('technicians') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <!-- For Booking -->
            <?php if (get_p("booking", "v")) { ?>
                <li class="nav-parent <?= sidebar_parent(["booking", "approved_bookings", "pending_bookings", "declined_bookings"]) ?>">
                    <a href="#"><i class="fa fa-money"></i><span data-translate="Booking"><?= lang('booking_requests') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li class="<?= sidebar_active("pending_bookings") ?>"><a href="<?= base_url() ?>admin/Booking/pending_bookings"><?= lang('pending_bookings') ?></a></li>
                        <li class="<?= sidebar_active("approved_bookings") ?>"><a href="<?= base_url() ?>admin/Booking/approved_bookings"><?= lang('approved_bookings') ?></a></li>
                        <li class="<?= sidebar_active("declined_bookings") ?>"><a href="<?= base_url() ?>admin/Booking/declined_bookings"><?= lang('declined_bookings') ?></a></li>
                    </ul>
                </li>
            <?php } ?>
            <!--For Booking Preparation & Missing Items-->
            <?php if (or_pb(["booking_preparation", "missing_items", "damaged_items", "missing_acc", "damaged_acc"])) {
                ?>
                <li class="nav-parent <?= sidebar_parent(["booking_preparation", "booking_delivery", "booking_return", "missing_items", "damaged_items", "missing_acc", "damaged_acc"]) ?>">
                    <a href="#"><i class="fa fa-cart-arrow-down"></i><span data-translate="booking_preparation"><?= lang('booking_management') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <?php if (get_p("booking_preparation", "v")) { ?>
                            <li class="<?= sidebar_active("booking_preparation") ?>"><a href="<?= base_url('admin/Booking_preparation') ?>"><?= lang('booking_preparation') ?></a></li>
                            <li class="<?= sidebar_active("booking_delivery") ?>"><a href="<?= base_url('admin/Booking_preparation/delivery') ?>"><?= lang('booking_delivery') ?></a></li>
                            <li class="<?= sidebar_active("booking_return") ?>"><a href="<?= base_url('admin/Booking_preparation/return') ?>"><?= lang('booking_return') ?></a></li>
                            <!-- <li class="<?= sidebar_active("invoice_printing") ?>"><a href="<?= base_url('admin/Booking_preparation/invoice') ?>"><?= lang('invoice_printing') ?></a></li> -->
                        <?php } ?>
                        <?php if (get_p("missing_items", "v")) { ?>
                            <li class="<?= sidebar_active("missing_items") ?>"><a href="<?= base_url('admin/Booking/missing_items') ?>"><?= lang('missing_items') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("damaged_items", "v")) { ?>
                            <li class="<?= sidebar_active("damaged_items") ?>"><a href="<?= base_url('admin/Booking/damaged_items') ?>"><?= lang('damaged_items') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("missing_acc", "v")) { ?>
                            <li class="<?= sidebar_active("missing_acc") ?>"><a href="<?= base_url('admin/Booking/missing_acc') ?>"><?= lang('missing_acc') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("damaged_acc", "v")) { ?>
                            <li class="<?= sidebar_active("damaged_acc") ?>"><a href="<?= base_url('admin/Booking/damaged_acc') ?>"><?= lang('damaged_acc') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <!-- For Accounting -->
            <?php if (or_pb(["accounting"])) {
                ?>
                <li class="nav-parent <?= sidebar_parent(["accounting", "transaction", "types", "add_transaction", "add_type", "products_stat", "products_usage"]) ?>">
                    <a href="#"><i class="fa fa-line-chart"></i><span data-translate="accounting"><?= lang('accounting') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li class="<?= sidebar_active("types") ?>"><a href="<?= base_url('admin/Accounting/types') ?>"><?= lang('types') ?></a></li>
                        <?php if (get_p("accounting", "c")) { ?>
                            <li class="<?= sidebar_active("add_type") ?>"><a href="<?= base_url('admin/Accounting/add') ?>"><?= lang('add_type') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("accounting", "v")) { ?>
                            <li class="<?= sidebar_active("accounting") ?>"><a href="<?= base_url('admin/Accounting') ?>"><?= lang('accounting') ?></a></li>
                            <li class="<?= sidebar_active("transaction") ?>"><a href="<?= base_url('admin/Accounting/transaction') ?>"><?= lang('transaction') ?></a></li>
                            <li class="<?= sidebar_active("products_stat") ?>"><a href="<?= base_url('admin/Accounting/products') ?>"><?= lang('products_stat') ?></a></li>
                            <li class="<?= sidebar_active("products_usage") ?>"><a href="<?= base_url('admin/Accounting/products_usage') ?>"><?= lang('products_usage') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("accounting", "c")) { ?>
                            <li class="<?= sidebar_active("add_transaction") ?>"><a href="<?= base_url('admin/Accounting/add_transaction') ?>"><?= lang('add_transaction') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <!-- For Equipment Test & Maintenance -->
            <?php if (or_pb(["equipment_test", "maintenance"])) { ?>
                <li class="nav-parent <?= sidebar_parent(["equipment_test", "com_equipment_test", "add_maintenance", "maintenance"]) ?>">
                    <a href="#"><i class="fa fa-wrench"></i><span data-translate="Equipment Test & Maintenance"><?= lang('test_maintenance') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <?php if (get_p("equipment_test", "v")) { ?>
                            <li class="<?= sidebar_active("equipment_test") ?>"><a href="<?= base_url() ?>admin/Equipment_test"><?= lang('current_test') ?></a></li>
                            <li class="<?= sidebar_active("com_equipment_test") ?>"><a href="<?= base_url() ?>admin/Equipment_test/completed"><?= lang('com_equip_test') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("maintenance", "c")) { ?>
                            <li class="<?= sidebar_active("add_maintenance") ?>"><a href="<?= base_url() ?>admin/Maintenance/add"><?= lang('add_maintenance') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("maintenance", "v")) { ?>
                            <li class="<?= sidebar_active("maintenance") ?>"><a href="<?= base_url() ?>admin/Maintenance"><?= lang('maintenance') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <!-- For Front-End Pages -->
            <?php if (get_p("blog", "v") || get_p("about_page", "v") || get_p("privacy_page", "v") || get_p("how_to_rent_page", "v")) { ?>
                <li class="nav-parent <?= sidebar_parent(["blog", "add_blog", "edit_blog", "about_page", "privacy_page", "how_to_rent_page"]) ?>">
                    <a href="#"><i class="fa fa-sitemap"></i><span data-translate="Front-End Pages"><?= lang('front_end') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <?php if (get_p("blog", "v")) { ?>
                            <li class="<?= sidebar_active("blog") ?>"><a href="<?= base_url() ?>admin/Blog"><?= lang('blog') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("about_page", "v")) { ?>
                            <li class="<?= sidebar_active("about_page") ?>"><a href="<?= base_url() ?>admin/About"><?= lang('about_page') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("privacy_page", "v")) { ?>
                            <li class="<?= sidebar_active("privacy_page") ?>"><a href="<?= base_url() ?>admin/Privacy"><?= lang('privacy_page') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("how_to_rent_page", "v")) { ?>
                            <li class="<?= sidebar_active("how_to_rent_page") ?>"><a href="<?= base_url() ?>admin/Renting"><?= lang('how_to_rent_page') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <!-- For Newsletters Pages -->
            <?php if (get_p("newsletters", "v") || get_p("newsletters", "c")) { ?>
                <li class="nav-parent <?= sidebar_parent(["newsletters", "add_newsletter", "newsletter_settings"]) ?>">
                    <a href="#"><i class="fa fa-sitemap"></i><span data-translate="Newsletter"><?= lang('newsletters') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <?php if (get_p("newsletters", "c")) { ?>
                            <li class="<?= sidebar_active("add_newsletter") ?>"><a href="<?= base_url() ?>admin/Newsletters/add"><?= lang('add_newsletter') ?></a></li>
                        <?php } ?>
                        <li class="<?= sidebar_active("newsletters") ?>"><a href="<?= base_url() ?>admin/Newsletters"><?= lang('newsletter') ?></a></li>
                    </ul>
                </li>
            <?php } ?>

            <!-- For Attendance Import -->
            <?php if (get_p("import", "v") || get_p("import", "c")) { ?>
                <li class="nav-parent <?= sidebar_parent(["import", "import_data"]) ?>">
                    <a href="#"><i class="fa fa-database"></i><span data-translate="Studios"><?= lang('attendance') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <?php if (get_p("import", "c")) { ?>
                            <li class="<?= sidebar_active("import_data") ?>"><a href="<?= base_url() ?>admin/Import/import"><?= lang('import') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("import", "v")) { ?>
                            <li class="<?= sidebar_active("import") ?>"><a href="<?= base_url() ?>admin/Import"><?= lang('attendance') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <!-- For Reports -->
            <?php if (get_p("equip_rep", "v") || get_p("acc_rep", "v") || get_p("ecomm_rep", "v") || get_p("failed_msgs_rpt", "v")) { ?>
                <li class="nav-parent <?= sidebar_parent(["equip_rep", "acc_rep", "ecomm_rep", "failed_msgs_rpt"]) ?>">
                    <a href="#"><i class="fa fa-files-o"></i><span data-translate="Reports"><?= lang('reports') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <?php if (get_p("equip_rep", "v")) { ?>
                            <li class="<?= sidebar_active("equip_rep") ?>"><a href="<?= base_url('admin/Reports/equip_rep') ?>"><?= lang('equip_rep') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("acc_rep", "v")) { ?>
                            <li class="hidden <?= sidebar_active("acc_rep") ?>"><a href="<?= base_url('admin/Reports/acc_rep') ?>"><?= lang('acc_rep') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("ecomm_rep", "v")) { ?>
                            <li class="<?= sidebar_active("ecomm_rep") ?>"><a href="<?= base_url('admin/Reports/ecomm_rep') ?>"><?= lang('ecomm_rep') ?></a></li>
                        <?php } ?>
                        <?php if (get_p("failed_msgs_rpt", "v")) { ?>
                            <li class="<?= sidebar_active("failed_msgs_rpt") ?>"><a href="<?= base_url('admin/Reports/failed_msgs_rpt') ?>"><?= lang('failed_msgs_rpt') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <?php if (get_p("surveys", "v")) { ?>
                <!-- For Surveys -->
                <li class=" nav <?= sidebar_parent("surveys") ?>">
                    <a href="<?= base_url('admin/Survey') ?>"><i class="icon-feed"></i><span data-translate="Surveys"><?= lang('surveys') ?></span></a>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
<!-- END SIDEBAR -->