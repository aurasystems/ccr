<li>
    <a href="javascript:void(0);" onclick="set_seen(<?= $ntf->id ?>, '<?= $ntf->url ?>');">
        <i class="fa fa-star p-r-10 f-18 c-orange"></i>
        <?= $ntf->text ?>
        <span class="dropdown-time"><?= get_time_ago($ntf->time) ?></span>
    </a>
</li>