<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>"> 
        <div>
            <section>
                <?php $this->load->view('admin/private/sidebar'); ?>
                <div class="main-content">
                    <?php $this->load->view('admin/private/topbar'); ?>
                    <!-- BEGIN PAGE CONTENT -->
                    <div class="page-content page-thin">
                        <?= flash_success() ?>
                        <?= flash_error() ?>
                        <?php
                        $branches = '';
                        foreach ($locations as $location) {
                            $branches .= '<option value="' . $location->id . '">' . $location->n_en . '</option>';
                        }
                        ?>
                        <!--/////////////////-->
                        <div class="panel panel-default no-bd">
                            <div class="panel-header bg-primary">
                                <h2 class="panel-title"><?= lang("location") ?></h2>
                            </div>
                            <div class="panel-body bg-white">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <form method="POST" action="" class="form-horizontal" role="form">
                                            <?= csrf() ?>  
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" for="Select_branch"><?= lang('Select_branch') ?> : <span class="text-red">*</span></label>
                                                <div class="col-sm-7">
                                                    <select id='sel' name="city" class="form-control" style="width:350px">
                                                        <?= $branches ?>
                                                    </select>
                                                </div>
                                            </div>   
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label"><?= lang('temp') ?> : <span class="text-red">*</span></label>
                                                <div  class="col-sm-7">
                                                    <input type="button" onclick="no()" value="<?= lang("no") ?>"></input>
                                                    <input type="button" onclick="yes()" value="<?= lang("yes") ?>"></input>
                                                    <input hidden class='tmp' value='0' type='text' name="temp">

                                                    <div hidden  class="col-sm-3 ">
                                                        <label class="control-label"><?= lang("yes") ?></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="hidden_fields" style="display:none;">
                                                <div name='start' class=" form-group">
                                                    <label class="col-sm-4 control-label" for="start_date"><?= lang('start_date') ?> : <span class="text-red">*</span></label>
                                                    <div class="col-sm-3">
                                                        <input  class="form-control date-picker" type="text" id="start_date" name="start_date" >
                                                        <span  class="text-red"><?php echo form_error('start_date'); ?></span>
                                                    </div>
                                                </div>
                                                <div name='end'  class="form-group">
                                                    <label class="col-sm-4 control-label" for="end_date"><?= lang('end_date') ?> : <span class="text-red">*</span></label>
                                                    <div class="col-sm-3">
                                                        <input class="form-control  date-picker" type="text" id="end_date" name="end_date">
                                                        <span class="text-red"><?php echo form_error('end_date'); ?></span>
                                                    </div>
                                                </div>
                                            </div>         

                                            <div class="col-sm-12 text-center">
                                                <input  type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <table class="table table-dynamic">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th><?= lang('location') ?></th>
                                                    <th><?= lang('is_temp') ?></th>
                                                    <th><?= lang('from') ?></th>
                                                    <th><?= lang('to') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $i = 1;
                                                foreach ($user_location as $ulocation) {
                                                    ?>
                                                    <tr>
                                                        <td><?= $i++ ?></td>
                                                        <td><?= $ulocation->lname ?></td>
                                                        <td><?= ($ulocation->is_temp) ? '<span class="text-success p-l-20"><i class="fa fa-check fa-1x"></i></span>' : '<span class="text-success p-l-20"><i class="fa fa-close fa-1x"></i></span>' ?></td>
                                                        <td><?= ($ulocation->start_date != NULL) ? date('Y-m-d', strtotime($ulocation->start_date)) : '' ?></td>
                                                        <td><?= ($ulocation->end_date != NULL) ? date('Y-m-d', strtotime($ulocation->end_date)) : '' ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--//////////////////-->
                        <?php $this->load->view('admin/private/copyright'); ?>
                    </div>
                    <!-- END PAGE CONTENT -->
                </div>
            </section>
            <?php $this->load->view('admin/private/search'); ?>
            <?php $this->load->view('admin/private/preloader'); ?>
            <?php $this->load->view('admin/private/footer'); ?>
            <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
            <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
            <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
            <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
            <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
            <script type="text/javascript">
            function yes() {
                $('#hidden_fields').css('display', 'block');
                $('.tmp').val('1');
            }
            function no() {
                $('#hidden_fields').css('display', 'none');
                $('.tmp').val('0');
            }
            /*   $('#radio_container input:radio').click(function() {
             if ($(this).val() === '1') {
             $('#hidden_fields').css('display','block');
             } else if ($(this).val() === '2') {
             $('#hidden_fields').css('display','none');
             } 
             }); */
            </script>
    </body>
</html>