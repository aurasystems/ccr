<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= lang("add_account") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php echo form_open_multipart('admin/User/add', $attributes); ?>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="attendance_id"><?= lang('attendance_id') ?> : <span class="text-red">*</span></label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(['id' => 'attendance_id', 'name' => 'attendance_id', 'class' => 'form-control', 'value' => set_value('attendance_id'), 'required' => 'required']); ?>
                                            <span class="text-red"><?php echo form_error('attendance_id'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="n_en"><?= lang('name_in') . lang("lang_en") ?> : <span class="text-red">*</span></label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(['id' => 'n_en', 'name' => 'n_en', 'class' => 'form-control', 'value' => set_value('n_en'), 'required' => 'required']); ?>
                                            <span class="text-red"><?php echo form_error('name'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="n_ar"><?= lang('name_in') . lang("lang_ar") ?> : <span class="text-red">*</span></label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(['id' => 'n_ar', 'name' => 'n_ar', 'class' => 'form-control', 'value' => set_value('n_ar'), 'required' => 'required']); ?>
                                            <span class="text-red"><?php echo form_error('n_ar'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('email') ?> : <span class="text-red">*</span></label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(['type' => 'email', 'id' => 'email', 'name' => 'email', 'class' => 'form-control', 'value' => set_value('email'), 'required' => 'required']); ?>
                                            <span class="text-red"><?php echo form_error('email'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('password') ?> : <span class="text-red">*</span></label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(['type' => 'password', 'id' => 'password', 'name' => 'password', 'class' => 'form-control', 'value' => set_value('password'), 'required' => 'required']); ?>
                                            <span class="text-red"><?php echo form_error('password'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('password_confirm') ?> : <span class="text-red">*</span></label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(['type' => 'password', 'id' => 'password_confirm', 'name' => 'password_confirm', 'class' => 'form-control', 'value' => set_value('password_confirm'), 'required' => 'required']); ?>
                                            <span class="text-red"><?php echo form_error('password_confirm'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('access_right') ?> : <span class="text-red">*</span></label>
                                        <div class="col-sm-7">
                                            <select name='access_level' id='access_level' data-search='true' class='form-control'>
                                                <option value=''><?= lang('set_access_level') ?></option>
                                                <?php foreach ($access_levels as $one) { ?>
                                                    <option value='<?= $one->id ?>' <?= $this->input->post('access_level') == $one->id ? 'selected' : '' ?>><?= $one->{'n_' . lc()} ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-red"><?php echo form_error('access_level'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="job_start_date"><?= lang('job_start_date') ?> :</label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(['id' => 'datepicker', 'name' => 'job_start_date', 'class' => 'date-picker form-control', 'value' => set_value('job_start_date')]); ?>
                                            <span class="text-red"><?php echo form_error('job_start_date'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="salary"><?= lang('salary') ?> :</label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(['type' => 'number','min' => 0, 'id' => 'salary', 'name' => 'salary', 'class' => 'form-control', 'value' => set_value('salary')]); ?>
                                            <span class="text-red"><?php echo form_error('salary'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="notes"><?= lang('notes') ?> : </label>
                                        <div class="col-sm-7">
                                            <?php echo form_textarea(['id' => 'notes', 'name' => 'notes', 'class' => 'form-control', 'placeholder' => lang('notes'), 'rows' => '5', 'cols' => '15']); ?>
                                            <span class="text-red"><?php echo form_error('notes'); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 text-center">
                                        <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script>
            $(document).ready(function () {
                
            });
        </script>
    </body>
</html>