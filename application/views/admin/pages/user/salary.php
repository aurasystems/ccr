<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="row">
                                <div class="col-md-12">   
                                    <div id="check_alert" class="preview active" style="display: none;">
                                        <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= lang("payroll") ?></h2>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label><?= lang('location') ?></label>
                                                <select name="location" id="location" class="form-control" data-search="true">
                                                    <option value=""><?= lang('select_location') ?></option>
                                                    <?php
                                                    if (isset($locations) && $locations) {
                                                        foreach ($locations as $location) {
                                                            ?>
                                                            <option value="<?= $location->id ?>"><?= $location->{'n_'.lc()} ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label><?= lang('month') ?></label>
                                                <input type="text" autocomplete="off" name="year_month" id="datepicker" class="b-datepicker form-control" placeholder="<?= lang('year_month') ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                            <button class="btn btn-sm btn-dark" onclick="generate_report()"><?= lang('calculate') ?></button>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="filter-left">
                                                <table id="tableContents" class="table table-dynamic table-striped table-tools">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th><?= lang('name') ?></th>
                                                            <th><?= lang('basic_salary') ?></th>
                                                            <th><?= lang('location') ?></th>
                                                            <th><?= lang('loc_working_hours') ?></th>
                                                            <th><?= lang('act_working_hours') ?></th>
                                                            <th><?= lang('deserved_salary') ?></th>
                                                            <th><?= lang('year_month') ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/plugins/datatables/dataTables.tableTools.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>
            $("#datepicker").bootstrapDatepicker( {
                autoclose: true,
                format: "yyyy-mm",
                viewMode: "months", 
                minViewMode: "months",
                endDate: new Date()
            });
            function generate_report() {
                var tableContents = $('#tableContents').DataTable();
                tableContents.clear().draw();
                var location = $("#location").val();
                var year_month = $("#datepicker").val();
                if(location == '' || year_month == '') {
                    $("#check_msg").html('<?=$this->lang->line('spec_loc_month')?>');
                    $("#check_alert").slideDown();
                }
                else {
                    $("#check_alert").slideUp();
                    var postData = {
                        location: location,
                        year_month: year_month,
                        <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                    };
                    $('.loader-overlay').removeClass('loaded');
                    $.ajax({
                        url: '<?= base_url('admin/Salary/get_users_salaries') ?>',
                        type: "POST",
                        dataType: "json",
                        data: postData,
                        success: function (result) {
                            if (result == false) {
                                $('.loader-overlay').addClass('loaded');
                                tableContents.clear().draw();
                            } else {
                                var i = 0;
                                $.each(result, function (index, data) {
                                    i++;
                                    tableContents.row.add([
                                        i,
                                        '<a href="<?= base_url() ?>admin/Salary/get_user_attendance/' + data.u_id +'/' + data.b_id + '/' + data.year_month + '">' + data.u_name + '</a>',
                                        data.u_basic_salary,
                                        data.b_name,
                                        data.b_hours,
                                        data.user_hours,
                                        data.final_salary,
                                        data.year_month
                                    ]);
                                });
                                $('.loader-overlay').addClass('loaded');
                                tableContents.draw();
                            }
                        }
                    });
                }
            }
        </script>
    </body>
</html>