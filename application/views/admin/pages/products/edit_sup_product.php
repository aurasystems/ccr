<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <?php
                    $types = '';
                    foreach ($check_types as $type) {
                        if (!$empty) {
                            if ($item[0]->check_type == $type->id)
                                $types .= '<option value="' . $type->id . '" selected>' . $type->n_en . '</option>';
                            else
                                $types .= '<option value="' . $type->id . '">' . $type->n_en . '</option>';
                        } else
                            $types .= '<option value="' . $type->id . '">' . $type->n_en . '</option>';
                    }
                    $val_n = '';
                    $val_y = '';
                    $stud = '';
                    $tech = '';
                    $gear = '';
                    $hour = '';
                    $day = '';
                    if (!$empty) {
                        if ($item[0]->rent_term == 1) {
                            $hour = 'notselected';
                            $day = 'selected';
                        } else if ($item[0]->rent_term == 2) {
                            $hour = 'selected';
                            $day = 'notselected';
                        }
                        if ($item[0]->featured == 1) {
                            $val_y = true;
                            $val_n = false;
                        } else {
                            $val_y = false;
                            $val_n = true;
                        }
                    }
                    ?>
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= $title ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form method="POST" action="" class="form-horizontal" role="form" enctype="multipart/form-data">
                                        <?= csrf() ?>
                                        <?php if(!empty($item[0]->img)) { ?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('img') ?> :</label>
                                            <div class="col-sm-3">
                                                <img class="img-md img-thumbnail" src="<?= base_url() ?>uploads/products/<?= $item[0]->img ?>">
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('img') ?> :</label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(array('type' => 'file', 'id' => 'userfile', 'name' => 'userfile', 'class' => 'form-control', 'size' => 20)); ?>
                                                <span class="text-red"><?php echo form_error('img'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="featured"><?= lang('featured') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-2">
                                                <label class="control-label"><?= lang('yes') ?></label>
                                                    <?php echo form_radio('featured', 1, $val_y, set_checkbox('featured', 1), "id='yes'"); ?>
                                            </div>
                                            <div class="col-sm-2">
                                                <label class="control-label"><?= lang('no') ?></label>
                                                <?php echo form_radio('featured', 0, $val_n, set_checkbox('featured', 0), "id='no'"); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="rent_term"><?= lang('rent_term') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <select name="rent_term" class="form-control" style="width:350px">
                                                    <option value='2' <?= $hour ?>><?= lang('hr') ?></option>
                                                    <option value='1' <?= $day ?>><?= lang('day') ?></option>
                                                </select>
                                            </div>
                                        </div>   
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="check_type"><?= lang('check_type') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <select name="check_type" class="form-control" style="width:350px">
                                                <?= $types ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="n_en"><?= lang('name_in') . lang("lang_en") ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['value' => !$empty ? $item[0]->n_en : '', 'id' => 'n_en', 'name' => 'n_en', 'class' => 'form-control', 'placeholder' => lang('name_in') . lang('lang_en'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('n_en'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="n_ar"><?= lang('name_in') . lang("lang_ar") ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['value' => !$empty ? $item[0]->n_ar : '', 'id' => 'n_ar', 'name' => 'n_ar', 'class' => 'form-control', 'placeholder' => lang('name_in') . lang('lang_ar'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('n_ar'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="code"><?= lang('code') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['value' => !$empty ? $item[0]->code : '', 'id' => 'code', 'name' => 'code', 'class' => 'form-control', 'placeholder' => lang('code'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('code'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="rent_price"><?= lang('rent_price') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['value' => !$empty ? $item[0]->rent_price : '', 'type' => 'number', 'id' => 'rent_price', 'name' => 'rent_price', 'class' => 'form-control', 'placeholder' => lang('rent_price'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('rent_price'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="resources"><?= lang('resources') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-7">
                                                <?php echo form_input(['value' => !$empty ? $item[0]->resources : '', 'id' => 'resources', 'name' => 'resources', 'class' => 'form-control', 'placeholder' => lang('resources'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('resources'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="overview"><?= lang('overview') ?> : </label>
                                            <div class="col-sm-7">
                                                <?php echo form_textarea(['value' => !$empty ? $item[0]->overview : '', 'id' => 'overview', 'name' => 'overview', 'class' => 'form-control', 'placeholder' => lang('overview'), 'rows' => '5', 'cols' => '15']); ?>
                                                <span class="text-red"><?php echo form_error('overview'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="description"><?= lang('description') ?> : </label>
                                            <div class="col-sm-7">
                                                <?php echo form_textarea(['value' => !$empty ? $item[0]->description : '', 'id' => 'description', 'name' => 'description', 'class' => 'form-control', 'placeholder' => lang('description'), 'rows' => '5', 'cols' => '15']); ?>
                                                <span class="text-red"><?php echo form_error('description'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="specifications"><?= lang('specifications') ?> :</label>
                                            <div class="col-sm-7">
                                                <?php echo form_textarea(['value' => !$empty ? $item[0]->specifications : '', 'id' => 'specifications', 'name' => 'specifications', 'class' => 'form-control', 'placeholder' => lang('specifications'), 'rows' => '3', 'cols' => '15']); ?>
                                                <span class="text-red"><?php echo form_error('specifications'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->

    </body>
</html>