<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $one->{'n_' . lc()} . ' ' ?><?= lang("accessories") ?></h2>
                                </div>
                                <div class="panel-body">
                                    <?php if (get_p("products", "c")) { ?>
                                        <div class="text-center">
                                            <a class="btn btn-primary" href="<?= base_url() ?>admin/Products/add_accessory/<?= $id ?>"><?= lang("add_accessory") ?></a>
                                        </div>
                                    <?php } ?>

                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('name') ?></th>
                                                <th><?= lang('count') ?></th>
                                                <th><?= lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($accessories as $accessory) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $accessory->name ?></td>
                                                    <td><?= $accessory->count ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a title="<?= lang("edit") ?>" href="<?= base_url() ?>admin/Products/edit_accessory/<?= $accessory->id ?>/<?= $id ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                                                            <a onclick="delete_accessory(<?= $accessory->id . ',' . $id ?>);" title="<?= lang("delete") ?>" href="javascript:void(0);" data-id="<?= $accessory->id ?>" class="delete btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                                        </div>
                                                    </td>

                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tbody>

                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
            function delete_accessory(accessory_id, product_id) {
                if (accessory_id != '' && product_id != '') {
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url('admin/Products/delete_accessory/') ?>" + accessory_id + '/' + product_id;
                    }
                }
            }
        </script>
    </body>
</html>