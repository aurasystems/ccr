<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?=$product_name.' ' ?><?= lang("items") ?></h2>
                                </div>
                                <div class="panel-body">
                                    <?php if (get_p("products", "c")) { ?>
                                        <div class="text-center">
                                            <a class="btn btn-primary" href="<?= base_url() ?>admin/Products/add_item/<?= $this->uri->segment($this->uri->total_segments()) ?>"><?= lang("add_item") ?></a>
                                        </div>
                                    <?php } ?>
                                    
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                            <th>#</th>
                                                <th><?= lang('serial_number') ?></th>
                                                <th><?= lang('cost_price') ?></th>
                                                <th><?= lang('sold') ?></th>
                                                <th><?= lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach($items as $item) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $item->serial_number?></td>
                                                    <td><?= $item->cost_price?></td>
                                                    <td><?= $item->sold == 1 ? '<i class="fa fa-check"></i>' : '' ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <?php
                                                            if ($item->sold == 0) {
                                                                ?>
                                                                <a onclick="mark_as_sold(<?= $item->id.','.$this->uri->segment($this->uri->total_segments()) ?>);" href="javascript:void(0);" title="<?= lang("mark_as_sold") ?>" href="#" data-id="<?= $item->id ?>" class="btn btn-sm btn-warning"><i class="fa fa-close"></i></a>
                                                            <?php
                                                            }
                                                            ?>
                                                            <a title="<?= lang("edit") ?>" href="<?= base_url() ?>admin/Products/edit_item/<?= $item->id ?>/<?= $this->uri->segment($this->uri->total_segments()) ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                                                            <a title="<?= lang("delete") ?>" href="#" data-id="<?= $item->id ?>" class="delete btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                                        </div>
                                                    </td>

                                                </tr>
                                                <?php
                                            }
                                            ?>
                                          
                                        </tbody>
                                        
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>




        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
            $(document).ready(function () {
                $(".delete").click(function (e) {
                    e.preventDefault();
                    var id = $(this).attr("data-id");
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url('admin/Products/delete_item/') ?>" + id +"/" + <?= $this->uri->segment($this->uri->total_segments()) ?>;
                    }
                });
            });
            function mark_as_sold(id, product) {
                if (id != '') {
                    var conf = confirm("<?= lang("mark_item_as_sold") ?>");
                    if (conf) {
                        window.location = "<?= base_url('admin/Products/mark_item_as_sold/') ?>" + id + '/' + product;
                    }
                }
            }
        </script>
    </body>
</html>