<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <?php
                    $types = '';
                    $location='';
                    $category ='';
                    $brand = '';

                    foreach ($check_types as $type)
                        $types .= '<option value="' . $type->id . '">' . $type->n_en . '</option>';
                    foreach ($locations as $loc)
                        $location .= '<option value="'.$loc->id.'" >'.$loc->n_en.'</option>';
                    foreach ($brands as $bran)
                        $brand .= '<option value="'.$bran->id.'" >'.$bran->n_en.'</option>';
                    foreach ($categories as $cat)
                        $category .= '<option value="'.$cat->id.'" >'.$cat->n_en.'</option>';
                    ?>
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= lang("add_product") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form method="POST" action="" class="form-horizontal" role="form" enctype="multipart/form-data">
                                        <?= csrf() ?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('img') ?> :</label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(array('type' => 'file', 'id' => 'userfile', 'name' => 'userfile', 'class' => 'form-control', 'size' => 20)); ?>
                                                <span class="text-red"><?php echo form_error('img'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="featured"><?= lang('featured') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-2">
                                                <label class="m-r-20">
                                                    <input type="radio" name="featured" value="1" data-radio="iradio_minimal-blue" checked="checked"> <?= lang('yes') ?>
                                                </label>
                                            </div>
                                            <div class="col-sm-2">
                                                <label class="m-r-20">
                                                    <input type="radio" name="featured" value="0" data-radio="iradio_minimal-blue"> <?= lang('no') ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="slider"><?= lang('slider') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-2">
                                                <label class="m-r-20">
                                                    <input type="radio" name="slider" value="1" data-radio="iradio_minimal-blue" checked="checked"> <?= lang('yes') ?>
                                                </label>
                                            </div>
                                            <div class="col-sm-2">
                                                <label class="m-r-20">
                                                    <input type="radio" name="slider" value="0" data-radio="iradio_minimal-blue"> <?= lang('no') ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="rent_term"><?= lang('rent_term') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <select name="rent_term" class="form-control" style="width:350px" data-search="true">
                                                    <option value='2'><?= lang('hr') ?></option>
                                                    <option value='1'><?= lang('day') ?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="check_type"><?= lang('check_type') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <select name="check_type" class="form-control" style="width:350px" data-search="true">
                                                    <?= $types ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="category"><?= lang('category') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <select name="category" class="form-control" style="width:350px" data-search="true">
                                                    <?= $category ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="brand"><?= lang('brand') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <select name="brand" class="form-control" style="width:350px" data-search="true">
                                                    <?= $brand ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="n_en"><?= lang('name_in') . lang("lang_en") ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['id' => 'n_en', 'name' => 'n_en', 'class' => 'form-control', 'placeholder' => lang('name_in') . lang('lang_en'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('n_en'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="n_ar"><?= lang('name_in') . lang("lang_ar") ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['id' => 'n_ar', 'name' => 'n_ar', 'class' => 'form-control', 'placeholder' => lang('name_in') . lang('lang_ar'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('n_ar'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="code"><?= lang('code') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['id' => 'code', 'name' => 'code', 'class' => 'form-control', 'placeholder' => lang('code'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('code'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="rent_price"><?= lang('rent_price') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['type' => 'number', 'id' => 'rent_price', 'name' => 'rent_price', 'class' => 'form-control', 'placeholder' => lang('rent_price'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('rent_price'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="resources"><?= lang('resources') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-7">
                                                <?php echo form_textarea(['id' => 'resources', 'name' => 'resources', 'class' => 'form-control cke-editor', 'placeholder' => lang('resources'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('resources'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="overview"><?= lang('overview') ?> : </label>
                                            <div class="col-sm-7">
                                                <?php echo form_textarea(['id' => 'overview', 'name' => 'overview', 'class' => 'form-control cke-editor', 'placeholder' => lang('overview'), 'rows' => '5', 'cols' => '15']); ?>
                                                <span class="text-red"><?php echo form_error('overview'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="description"><?= lang('description') ?> : </label>
                                            <div class="col-sm-7">
                                                <?php echo form_textarea(['id' => 'description', 'name' => 'description', 'class' => 'form-control cke-editor', 'placeholder' => lang('description'), 'rows' => '5', 'cols' => '15']); ?>
                                                <span class="text-red"><?php echo form_error('description'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="specifications"><?= lang('specifications') ?> :</label>
                                            <div class="col-sm-7">
                                                <?php echo form_textarea(['id' => 'specifications', 'name' => 'specifications', 'class' => 'form-control cke-editor', 'placeholder' => lang('specifications'), 'rows' => '3', 'cols' => '15']); ?>
                                                <span class="text-red"><?php echo form_error('specifications'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <input type="submit" class="btn btn-primary" value="<?= lang("create") ?>">
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/cke-editor/ckeditor.js"></script> <!-- Advanced HTML Editor -->
        <script src="<?= base_url() ?>assets/plugins/cke-editor/adapters/adapters.min.js"></script>

    </body>
</html>