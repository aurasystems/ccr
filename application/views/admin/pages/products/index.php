<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= lang("products") ?></h2>
                                </div>
                                <div class="panel-body">
                                    <?php if (get_p("products", "c")) { ?>
                                        <div class="text-center">
                                            <a class="btn btn-primary" href="<?= base_url() ?>admin/Products/add"><?= lang("add_product") ?></a>
                                        </div>
                                    <?php } ?>
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('name') ?></th>
                                                <th><?= lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            $n = 0;
                                            foreach ($products as $product) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $product->{'n_' . lc()} ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a title="<?= lang("reviews") ?>" href="<?= base_url() ?>admin/Products/reviews/<?= $product->id ?>" class="btn btn-sm btn-primary"><i class="fa fa-star"></i></a>
                                                            <a title="<?= lang("view_items") ?>" href="<?= base_url() ?>admin/Products/items/<?= $product->id ?>" class="btn btn-sm btn-primary"><i class="fa fa-list-ul"></i></a>
                                                            <a title="<?= lang("sup_items") ?>" href="<?= base_url() ?>admin/Products/supplementary/<?= $product->id ?>" class="btn btn-sm btn-info"><i class="fa fa-sitemap"></i></a>
                                                            <?php if (get_p("products", "u")) { ?>
                                                            <a title="<?= lang("edit") ?>" href="<?= base_url() ?>admin/Products/edit_product/<?= $product->id ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                                                            <?php } ?>
                                                            <?php if (get_p("products", "u")) { ?>
                                                            <a onclick="delete_product(<?= $product->id ?>);" title="<?= lang("delete") ?>" href="javascript:void(0);" data-id="<?= $product->id ?>" class="delete btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                $n++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>
            $(document).ready(function () {
//                $(".delete").click(function (e) {
//                    e.preventDefault();
//                    var id = $(this).attr("data-id");
//                    var conf = confirm("<?= lang("r_u_sure") ?>");
//                    if (conf) {
//                        window.location = "<?= base_url('admin/Products/delete_product/') ?>" + id;
//                    }
//                });
            });
            function delete_product(product_id) {
                if (product_id != '') {
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url('admin/Products/delete_product/') ?>" + product_id;
                    }
                }
            }
        </script>
    </body>
</html>