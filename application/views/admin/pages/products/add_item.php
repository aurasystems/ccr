<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= $product_name ?>  <?= lang("add_item") ?> </h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form method="POST" action="" class="form-horizontal" role="form">
                                        <?= csrf() ?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="location"><?= lang('location') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <select name="location_id" class="form-control" data-search="true">
                                                    <option value=''><?= lang('select_location') ?></option>
                                                    <?php foreach ($locations as $one) { ?>
                                                        <option value='<?= $one->id ?>'><?= $one->{'n_' . lc()} ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="text-red"><?php echo form_error('location'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="serial_number"><?= lang('serial_number') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['id' => 'serial_number', 'name' => 'serial_number', 'class' => 'form-control', 'placeholder' => lang('serial_number'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('serial_number'); ?></span>
                                            </div>
                                        </div>
                                        <?php if(!empty($product_cat) && $product_cat->is_camera == 1) { ?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="shutter_count"><?= lang('shutter_count') ?> :</label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['type' => 'number', 'min' => 0, 'id' => 'shutter_count', 'name' => 'shutter_count', 'class' => 'form-control', 'value' => set_value('shutter_count'), 'placeholder' => lang('shutter_count')]); ?>
                                                <span  class="text-red"><?php echo form_error('shutter_count'); ?></span>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="cost_price"><?= lang('cost_price') ?> :</label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['type' => 'number', 'min' => 1, 'id' => 'cost_price', 'name' => 'cost_price', 'class' => 'form-control', 'placeholder' => lang('cost_price')]); ?>
                                                <span class="text-red"><?php echo form_error('cost_price'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="warranty_company"><?= lang('warranty_company') ?> : </label>
                                            <div class="col-sm-3">
                                                <?php echo form_textarea(['id' => 'warranty_company', 'name' => 'warranty_company', 'class' => 'form-control', 'placeholder' => lang('warranty_company'), 'rows' => '5', 'cols' => '15']); ?>
                                                <span class="text-red"><?php echo form_error('warranty_company'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="warranty_end_date"><?= lang('warranty_end_date') ?> : </label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['id' => 'date', 'name' => 'warranty_end_date', 'class' => 'form-control date-picker', 'value' => set_value('warranty_end_date'), 'placeholder' => lang('warranty_end_date')]); ?>
                                                <span  class="text-red"><?php echo form_error('warranty_end_date'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <input type="submit" class="btn btn-primary" value="<?= lang("create") ?>">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
    </body>
</html>