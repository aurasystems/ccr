<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12 portlets">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="alert alert-danger" role="alert" style="display:none;" id="error"> 
                            <i class="icon ti-na"></i><strong  id="err-msg"></strong>
                            </div>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                <label for="by_SN"><?=lang("by_SN")?></label>
                                <div style="text-align:left;">
                                        <input style="width: 25%;" class="form-control"  value='' placeholder="<?= lang("scan_SN") ?>" type="text" name="" id="scan">
                                    </div>
                                    <label for="by_SN"><?=lang("by_date")?></label>
                                    <div class="row">
                                        <div class="col-md-12">   
                                            <div id="check_alert" class="preview active" style="display: none;">
                                                <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><?= lang('from') ?></label>
                                                <input dir="ltr" autocomplete="off" type="text" name="date_from" id="date_from" class="form-control date-picker text-center" value="<?= $from ?>" placeholder="<?= lang('date_from') ?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><?= lang('to') ?></label>
                                                <input dir="ltr" autocomplete="off" type="text" name="date_to" id="date_to" class="form-control date-picker text-center" value="<?= $to ?>" placeholder="<?= lang('date_to') ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                            <button class="btn btn-sm btn-dark" onclick="generate_report()"><?= lang('generate') ?></button>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <table id="tableContents" class="table table-striped table-tools">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center"><?= lang('lang_#') ?></th>
                                                        <th class="text-center"><?= lang('product_name') ?></th>
                                                        <th class="text-center"><?= lang('SN') ?></th>
                                                        <th class="text-center"><?= lang('num_trans') ?></th>
                                                        <th class="text-center"><?= lang('num_days') ?></th>
                                                        <th class="text-center"><?= lang('num_hours') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center" id="update">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/plugins/datatables/dataTables.tableTools.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script type="text/javascript">
            $("#scan").change(function () {
        $('#error').css('display', 'none');
            var scan_value = $(this).val();
            $.ajax({
                url: '<?= base_url('admin/Reports_extra/gen_total_number_of_rental_rpt_scan') ?>',
                type: "POST",
                data: {
                    scan_SN: scan_value,
                <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: "JSON",
                success: function (data) {
                    $("tbody").children().remove()  
                    if(data.flag==false)
                    {
                        $('#error').css('display', 'block');
                        $('#err-msg').html(data.msg);
                        setTimeout(function(){ $('#error').css('display', 'none'); }, 3000);

                    }   
                    else
                    {          
                    var trHTML = '';
                        trHTML +=
                                '<tr><td>' + 1 +
                                '</td><td>' + data.pname +
                                '</td><td>' + data.SN +
                                '</td><td>' + data.num_trans +
                                '</td><td>' + data.num_days +
                                '</td><td>' + data.num_hrs +
                        '</td></tr>';
                    $('#update').append(trHTML);
                    }
                }
            });
        });


                function update_table(){
                  
                    $.ajax({
                    "url": "<?php echo site_url('admin/Reports_extra/gen_total_number_of_rental_rpt') ?>",
                        type: "POST",
                        data: {
                            date_from : $("#date_from").val(),
                            date_to : $("#date_to").val(),
                            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                            },
                    dataType: "JSON",
                        success: function(data){    
                            $("tbody").children().remove()               
                                var trHTML = '';
                                var n = 1;
                                $.each(data, function (i, o){
                                trHTML += 
                                        '<tr><td>' + n++ +
                                        '</td><td >' + o.pname +
                                        '</td><td >' + o.SN +
                                        '</td><td>' + o.num_trans +
                                        '</td><td>' + o.num_days +
                                        '</td><td>' + o.num_hrs +                                      
                                       '</td></tr>';
                                });
                                $('#update').append(trHTML);
                        }
                    });
                }
        
            function generate_report() {
                var tableContents = $('#tableContents').DataTable();
                tableContents.clear().draw();
                var date_from = $("#date_from").val();
                var date_to = $("#date_to").val();
                if ((date_from == null && date_to == null) || (date_from == null && date_to != null) || (date_from != null && date_to == null)) {
                    $("#check_msg").html('<?= lang('start_end_date_check_full') ?>');
                    $("#check_alert").slideDown();
                } else if (date_from > date_to) {
                    $("#check_msg").html('<?= lang('start_end_date_check') ?>');
                    $("#check_alert").slideDown();
                } else {
                    $("#check_alert").slideUp();
                    update_table();
                }
            }
        </script>
    </body>
</html>