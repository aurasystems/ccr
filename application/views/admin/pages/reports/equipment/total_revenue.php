<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12 portlets">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                    <div class="col-md-4">
                                        <label ><?= lang('report_for') ?></label>
                                            <div class="form-group">
                                                <select name="for" id="for" style="width : 40%">
                                                    <option value="1"><?=lang("item")?></option>
                                                    <option value="2"><?=lang("brand")?></option>
                                                    <option value="3"><?=lang("category")?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div id="items" class="col-md-4">
                                        <label ><?= lang('item') ?></label>
                                            <div class="form-group">
                                                <select name="item" id="item" style="width : 40%;">
                                                <?php $SN = get_items_products();
                                                if(!empty($SN)){
                                                foreach($SN as $item){ ?>
                                                    <option value="<?=$item->id?>"><?=$item->serial_number?></option>
                                                <?php }
                                                }?>
                                                </select>
                                            </div>
                                        </div>
                                        <div id="brands" class="col-md-4" style="display:none;">
                                        <label ><?= lang('brand') ?></label>
                                            <div class="form-group">
                                                <select name="brand" id="brand" style="width : 40%; ">
                                                <?php $brands = get_brands();
                                                if(!empty($brands)){
                                                foreach($brands as $brand){ ?>
                                                    <option value="<?=$brand->id?>"><?=$brand->{'n_' . lc()}?></option>
                                                <?php }
                                                }?>
                                                </select>
                                            </div>
                                        </div>
                                        <div id="category" class="col-md-4" style="display:none;">
                                        <label ><?= lang('category') ?></label>
                                            <div class="form-group">
                                            <select name="categ" id="categ" style="width : 40%;">
                                                <?php $categ = get_categories();
                                                if(!empty($categ)){
                                                foreach($categ as $cat){ ?>
                                                    <option value="<?=$cat->id?>"><?=$cat->{'n_' . lc()}?></option>
                                                <?php }
                                                }?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">   
                                            <div id="check_alert" class="preview active" style="display: none;">
                                                <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><?= lang('date_from') ?></label>
                                                <input dir="ltr" autocomplete="off" type="text" name="date_from" id="date_from" class="form-control date-picker text-center" value="<?= $from ?>" placeholder="<?= lang('date_from') ?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><?= lang('date_to') ?></label>
                                                <input dir="ltr" autocomplete="off" type="text" name="date_to" id="date_to" class="form-control date-picker text-center" value="<?= $to ?>" placeholder="<?= lang('date_to') ?>" />
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                            <button class="btn btn-sm btn-dark" onclick="generate_report()"><?= lang('generate') ?></button>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <table id="tableContents" class="table table-striped table-tools">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center"><?= lang('lang_#') ?></th>
                                                        <th class="text-center"><?= lang('name') ?></th>
                                                        <th class="text-center"><?= lang('#booking') ?></th>
                                                        <th class="text-center"><?= lang('total_revenue') ?></th>
                                                        <th class="text-center"><?= lang("indiv_trans") ?></th>

                                                    </tr>
                                                </thead>
                                                <tbody class="text-center" id="update">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/plugins/datatables/dataTables.tableTools.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script type="text/javascript">
                $("#for").change(function(){
                var id = $(this).val();

                    switch (id){
                        case '1':  $('#category').css('display', 'none');
                        $('#brands').css('display', 'none');
                        $('#items').css('display', 'block');
                    break;
                        case '2': $('#items').css('display', 'none');
                        $('#category').css('display', 'none');
                        $('#brands').css('display', 'block');
                    break;
                        case '3': $('#items').css('display', 'none');
                        $('#brands').css('display', 'none');
                        $('#category').css('display', 'block');
                    break;
                    
                    }
                });
           
                function update_table(){
                            var date_from =  $("#date_from").val();
                            var date_to   = $("#date_to").val();
                            var filter_for       = $("#for").val();
                            var item      = $("#item").val();
                            var brand     = $("#brand").val();
                            var categ     = $("#categ").val();
                    $.ajax({
                    "url": "<?php echo site_url('admin/Reports_extra/gen_total_revenue_sn_rpt') ?>",
                        type: "POST",
                        data: {
                            date_from : date_from,
                            date_to :   date_to,
                            for :       filter_for,
                            item :      item,
                            brand :     brand,
                            categ :     categ,
                            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                            },
                    dataType: "JSON",
                        success: function(data){    
                            $("tbody").children().remove()               
                                var trHTML = '';
                                var n = 1;
                               // $.each(data, function (i, o){
                                trHTML += 
                                        '<tr><td>' + n +
                                        '</td><td>' + data.name +
                                        '</td><td >' + data.total_booking +
                                        '</td><td>' + data.total_revenu +
                                        '</td><td> <a title="<?= lang("show") ?>" href="<?= base_url() ?>admin/Reports_extra/indivdual_transaction/' + date_from +'/' + date_to +'/' + data.for +'/' + data.filter +'"  class="btn btn-sm btn-primary"><i class="fa fa-list-ul"></i></a>'

                                       '</td></tr>';
                             //   });
                                $('#update').append(trHTML);
                        }
                    });
                }

            function generate_report() {
                var tableContents = $('#tableContents').DataTable();
                tableContents.clear().draw();
                var date_from = $("#date_from").val();
                var date_to = $("#date_to").val();
                if ((date_from == null && date_to == null) || (date_from == null && date_to != null) || (date_from != null && date_to == null)) {
                    $("#check_msg").html('<?= lang('start_end_date_check_full') ?>');
                    $("#check_alert").slideDown();
                } else if (date_from > date_to) {
                    $("#check_msg").html('<?= lang('start_end_date_check') ?>');
                    $("#check_alert").slideDown();
                } else {
                    $("#check_alert").slideUp();
                    update_table();
                }
            }
        </script>
    </body>
</html>