<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th><?= lang('title') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><a href="<?= base_url('admin/Reports_extra/total_revenue_sn_rpt') ?>"><?= lang('total_revenue_s_n') ?></a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="<?= base_url('admin/Reports_extra/total_number_of_rental_rpt') ?>"><?= lang('total_trans_num_s_n') ?></a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="<?= base_url('admin/Reports_extra/total_expenses_rpt') ?>"><?= lang('total_expensed_s_n') ?></a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="<?= base_url('admin/Reports_extra/profit_anal_s_n_rpt') ?>"><?= lang('profit_anal_s_n') ?></a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="<?= base_url('admin/Reports_extra/op_eff_rpt') ?>"><?= lang('op_eff') ?></a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="<?= base_url('admin/Reports_extra/total_s_n_inv_rpt') ?>"><?= lang('total_s_n_inv') ?></a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="<?= base_url('admin/Reports/sold_equip') ?>"><?= lang('sold_equip') ?></a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="<?= base_url('admin/Reports/theft_equip') ?>"><?= lang('theft_equip') ?></a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="<?= base_url('admin/Reports/damaged_equip') ?>"><?= lang('damaged_equip') ?></a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="<?= base_url('admin/Reports/equipment_transfer_rpt') ?>"><?= lang('equip_transfer') ?></a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
            $(document).ready(function () {

            });
        </script>
    </body>
</html>