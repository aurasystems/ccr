<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12 portlets">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="alert alert-danger" role="alert" style="display:none;" id="error"> 
                            <i class="icon ti-na"></i><strong  id="err-msg"></strong>
                            </div>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                
                                    <div class="row">
                                        <div class="col-md-12">   
                                            <div id="check_alert" class="preview active" style="display: none;">
                                                <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <label for="by_SN"><?=lang("SN")?></label>
                                <div style="text-align:left;">
                                        <input style="width: 25%;" class="form-control"  value='' placeholder="<?= lang("scan_SN") ?>" type="text" name="" id="scan">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                            <button class="btn btn-sm btn-dark" onclick="generate_report()"><?= lang('generate') ?></button>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <table id="tableContents" class="table table-striped table-tools">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center"><?= lang('lang_#') ?></th>
                                                        <th class="text-center"><?= lang('SN') ?></th>
                                                        <th class="text-center"><?= lang('total_days_owned') ?></th>
                                                        <th class="text-center"><?= lang('total_days_rented') ?></th>
                                                        <th class="text-center"><?= lang('total_days_maint') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center" id="update">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/plugins/datatables/dataTables.tableTools.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script type="text/javascript">
                function update_table(){
                  
                    $.ajax({
                    "url": "<?php echo site_url('admin/Reports_extra/gen_op_eff_rpt') ?>",
                        type: "POST",
                        data: {
                            SN     : $("#scan").val(),
                            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                            },
                    dataType: "JSON",
                        success: function(data){    
                            $("tbody").children().remove()               
                                var trHTML = '';
                                var n = 1;
                              //  $.each(data, function (i, o){
                                if(data.flag == true)
                                  {
                                trHTML += 
                                        '<tr><td>' + n++ +
                                        '</td><td >' + data.sn +
                                        '</td><td>' + data.days_owned +
                                        '</td><td>' + data.days_rented +
                                        '</td><td>' + data.days_maint +    
                                       '</td></tr>';
                           //     });
                                $('#update').append(trHTML);
                                  }
                                  else if(data.flag == false)
                                  {
                                    $("#check_msg").html(data.msg);
                                    $("#check_alert").slideDown();
                                  }
                        }
                    });
                }
        
            function generate_report() {
                var tableContents = $('#tableContents').DataTable();
                tableContents.clear().draw();
                var date_from = $("#date_from").val();
                var date_to = $("#date_to").val();
                var scan = $("#scan").val();
                if(scan == ''){
                    $("#check_msg").html('<?= lang('must_enter_SN') ?>');
                    $("#check_alert").slideDown();
                } else {
                    $("#check_alert").slideUp();
                    update_table();
                }
            }
        </script>
    </body>
</html>