<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12 portlets">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="check_alert" class="preview active" style="display: none;">
                                                <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><?= lang('date_from') ?></label>
                                                <input dir="ltr" autocomplete="off" type="text" name="date_from" id="date_from" class="form-control date-picker text-center" placeholder="<?= lang('date_from') ?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><?= lang('date_to') ?></label>
                                                <input dir="ltr" autocomplete="off" type="text" name="date_to" id="date_to" class="form-control date-picker text-center" placeholder="<?= lang('date_to') ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                            <button class="btn btn-sm btn-dark" onclick="generate_report()"><?= lang('generate') ?></button>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <table id="tableContents" class="table table-striped table-tools">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center"><?= lang('lang_#') ?></th>
                                                        <th class="text-center"><?= lang('client') ?></th>
                                                        <th class="text-center"><?= lang('product') ?></th>
                                                        <th class="text-center"><?= lang('serial_number') ?></th>
                                                        <th class="text-center"><?= lang('booking_from') ?></th>
                                                        <th class="text-center"><?= lang('booking_to') ?></th>
                                                        <th class="text-center"><?= lang('return_date') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/plugins/datatables/dataTables.tableTools.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script type="text/javascript">
            function generate_report() {
                var tableContents = $('#tableContents').DataTable();
                tableContents.clear().draw();
                var date_from = $("#date_from").val();
                var date_to = $("#date_to").val();
                if ((date_from == null && date_to != null) || (date_from != null && date_to == null)) {
                    $("#check_msg").html('<?= lang('start_end_date_check_full') ?>');
                    $("#check_alert").slideDown();
                } else if (date_from > date_to) {
                    $("#check_msg").html('<?= lang('start_end_date_check') ?>');
                    $("#check_alert").slideDown();
                } else {
                    $("#check_alert").slideUp();
                    var postData = {
                        date_from : date_from,
                        date_to : date_to,
                        <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>',
                    };
                    $('.loader-overlay').removeClass('loaded');
                    $.ajax({
                        url: '<?= base_url() ?>admin/Reports/gen_theft_equip_rpt',
                        type: "POST",
                        dataType: "json",
                        data: postData,
                        success: function(result){
                            $('#msg_div').hide();
                            $('#warning_div').hide();
                            if(result == false) {
                                $('.loader-overlay').addClass('loaded');
                                tableContents.clear().draw();
                            }
                            else {
                                var i = 1;
                                for($i=0; $i < result.length; $i++) {
                                    tableContents.row.add([
                                        i,
                                        '<a href="<?= base_url() ?>admin/Clients/dashboard/'+ result[$i]['client_id'] +'">'+ result[$i]['cname'] +'</a>',
                                        result[$i]['pname'],
                                        result[$i]['serial_number'],
                                        result[$i]['b_from'],
                                        result[$i]['b_to'],
                                        result[$i]['return_time']
                                    ]);
                                    $('.loader-overlay').addClass('loaded');
                                    tableContents.draw();
                                    i++;
                                }
                            }
                        }
                    });
                }
            }
        </script>
    </body>
</html>