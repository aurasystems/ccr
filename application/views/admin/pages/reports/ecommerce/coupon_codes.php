<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12 portlets">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">   
                                            <div id="check_alert" class="preview active" style="display: none;">
                                                <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><?= lang('date_from') ?></label>
                                                <input dir="ltr" autocomplete="off" type="text" name="date_from" id="date_from" class="form-control date-picker text-center" value="<?= $from ?>" placeholder="<?= lang('date_from') ?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><?= lang('date_to') ?></label>
                                                <input dir="ltr" autocomplete="off" type="text" name="date_to" id="date_to" class="form-control date-picker text-center" value="<?= $to ?>" placeholder="<?= lang('date_to') ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                            <button class="btn btn-sm btn-dark" onclick="generate_report()"><?= lang('generate') ?></button>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <table id="tableContents" class="table table-striped table-tools">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center"><?= lang('lang_#') ?></th>
                                                        <th class="text-center"><?= lang('voucher_number') ?></th>
                                                        <th class="text-center"><?= lang('amount') ?></th>
                                                        <th class="text-center"><?= lang('v_from') ?></th>
                                                        <th class="text-center"><?= lang('v_to') ?></th>
                                                        <th class="text-center"><?= lang('type') ?></th>
                                                        <th class="text-center"><?= lang('client') ?></th>
                                                        <th class="text-center"><?= lang('uses_total') ?></th>
                                                        <th class="text-center"><?= lang('uses_remain') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center" id="update">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/plugins/datatables/dataTables.tableTools.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script type="text/javascript">
           
                function update_table(){
                  
                    $.ajax({
                    "url": "<?php echo site_url('admin/Reports_extra/gen_coupon_codes_rpt') ?>",
                        type: "POST",
                        data: {
                            date_from : $("#date_from").val(),
                            date_to : $("#date_to").val(),
                            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                            },
                    dataType: "JSON",
                        success: function(data){    
                            $("tbody").children().remove()               
                                var trHTML = '';
                                var n = 1;
                                var type = '';
                                var client = '';
                                $.each(data, function (i, o){
                                    if(o.type == 1)
                                        type = 'amount';
                                    else
                                        type = 'percentage';
                                    if(o.client_id == 0)
                                        client = 'All Clients';
                                    else 
                                    {
                                        get_client(o.client_id, function(d) {
                                            client = d; //console.log(d);
                                            });
                                        //client = get_client(o.client_id);
                                    }
                                trHTML += 
                                        '<tr><td>' + n++ +
                                        '</td><td >' + o.voucher_number +
                                        '</td><td>' + o.amount +
                                        '</td><td>' + o.v_from +
                                        '</td><td>' + o.v_to +                                      
                                        '</td><td>' + type +
                                        '</td><td>' + client +
                                        '</td><td>' + o.uses_total +
                                        '</td><td>' + o.uses_total +
                                       '</td></tr>';
                                });
                                $('#update').append(trHTML);
                        }
                    });
                }

                function get_client(client_id, callback)
                {
                    var client_name = '';
                    $.ajax({
                    "url": "<?php echo site_url('admin/Reports_extra/get_client_name') ?>",
                        type: "POST",
                        async: false,
                        data: {
                            client_id : client_id,
                            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                            },
                            
                    dataType: "JSON",
                        success: function(data){  
                            client_name = data;
                            callback(client_name); 
                        }
                    }); 
                }
        
            function generate_report() {
                var tableContents = $('#tableContents').DataTable();
                tableContents.clear().draw();
                var date_from = $("#date_from").val();
                var date_to = $("#date_to").val();
                if ((date_from == null && date_to == null) || (date_from == null && date_to != null) || (date_from != null && date_to == null)) {
                    $("#check_msg").html('<?= lang('start_end_date_check_full') ?>');
                    $("#check_alert").slideDown();
                } else if (date_from > date_to) {
                    $("#check_msg").html('<?= lang('start_end_date_check') ?>');
                    $("#check_alert").slideDown();
                } else {
                    $("#check_alert").slideUp();
                    update_table();
                }
            }
        </script>
    </body>
</html>