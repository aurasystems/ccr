<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= lang("edit_location") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php echo form_open_multipart("admin/Locations/edit/$id", $attributes); ?>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="n_en"><?= lang('name_in') . lang('lang_en') ?> : <span class="text-red">*</span></label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(['id' => 'n_en', 'name' => 'n_en', 'class' => 'form-control', 'value' => !empty($one->n_en) ? $one->n_en : '', 'required' => 'required']); ?>
                                            <span class="text-red"><?php echo form_error('n_en'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="n_ar"><?= lang('name_in') . lang('lang_ar') ?> : <span class="text-red">*</span></label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(['id' => 'n_ar', 'name' => 'n_ar', 'class' => 'form-control', 'value' => !empty($one->n_ar) ? $one->n_ar : '', 'required' => 'required']); ?>
                                            <span class="text-red"><?php echo form_error('n_ar'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('lat') ?> :</label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(['id' => 'lat', 'name' => 'lat', 'class' => 'form-control', 'value' => !empty($one->lat) ? $one->lat : '']); ?>
                                            <span class="text-red"><?php echo form_error('lat'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('lon') ?> :</label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(['id' => 'lon', 'name' => 'lon', 'class' => 'form-control', 'value' => !empty($one->lon) ? $one->lon : '']); ?>
                                            <span class="text-red"><?php echo form_error('lon'); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 text-center">
                                        <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script>
            $(document).ready(function () {
                
            });
        </script>
    </body>
</html>