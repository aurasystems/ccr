<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= lang("locations") ?></h2>
                                </div>
                                <div class="panel-body">
                                    <?php if (get_p("locations", "c")) { ?>
                                        <div class="text-center">
                                            <a class="btn btn-primary" href="<?= base_url() ?>admin/Locations/add"><?= lang("add_location") ?></a>
                                        </div>
                                    <?php } ?>
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('name') ?></th>
                                                <th><?= lang('lang_actions') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($locations as $location) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $location->{'n_' . lc()} ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a title="<?= lang("timetable") ?>" href="<?= base_url() ?>admin/Locations/timetable/<?= $location->id ?>" class="btn btn-sm btn-primary"><i class="fa fa-clock-o"></i></a>
                                                            <?php if (get_p("locations", "u")) { ?>
                                                                <a title="<?= lang("edit") ?>" href="<?= base_url() ?>admin/Locations/edit/<?= $location->id ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                                                            <?php } ?>
                                                            <?php if (get_p("locations", "d")) { ?>
                                                                <a title="<?= lang("delete") ?>" href="#" data-id="<?= $location->id ?>" class="delete btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
            $(document).ready(function () {
                $(".delete").click(function (e) {
                    e.preventDefault();
                    var id = $(this).attr("data-id");
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url('admin/Locations/delete/') ?>" + id;
                    }
                });
            });
        </script>
    </body>
</html>