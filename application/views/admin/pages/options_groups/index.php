<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">   
                            <div id="check_alert" class="preview active" style="display: none;">
                                <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <?php if (get_p("questions_types", "c")) { ?>
                                        <div class="text-center">
                                            <a class="btn btn-primary" href="<?= base_url('admin/Settings/add_question_type') ?>"><?= lang("add_question_type") ?></a>
                                        </div>
                                    <?php } ?>
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th><?= $this->lang->line('#') ?></th>
                                                <th><?= $this->lang->line('name') ?></th>
                                                <th><?= $this->lang->line('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($options_groups) {
                                                $i = 1;
                                                foreach ($options_groups as $op) {
                                                    ?>
                                                    <tr>
                                                        <td><?= $i ?></td>
                                                        <td><?= $op->name ?></td>
                                                        <td>
                                                            <div class="btn-group">
                                                                <?php if (get_p("questions_types", "u")) { ?>
                                                                    <a href="<?= base_url() ?>admin/Settings/edit_question_type/<?= $op->id ?>" class="edit btn btn-sm btn-dark"><i class="fa fa-edit"></i></a>
                                                                <?php } ?>
                                                                <?php if (get_p("questions_types", "d")) { ?>
                                                                    <a onclick="delete_question_type(<?= $op->id ?>);" href="javascript:void(0);" data-id="<?= $op->id ?>" class="delete btn btn-sm btn-danger"title="<?= lang("delete") ?>"><i class="fa fa-trash" ></i></a>
                                                            <?php } ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>
            function delete_question_type(id) {
                if (id != '') {
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url() ?>admin/Settings/delete_question_type/" + id;
                    }
                }
            }
        </script>
    </body>
</html>