<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= lang("email_sett") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <?php echo form_open_multipart('admin/Email_settings/update', $attributes); ?>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('lang_smtp_host') ?> :</label>
                                        <div class="col-sm-7">
                                            <input value="<?= !empty($email_settings->smtp_host) ? $email_settings->smtp_host : '' ?>" type="text" name="smtp_host" class="form-control"/>
                                            <span class="text-red"><?php echo form_error('smtp_host'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('lang_host_mail') ?> :</label>
                                        <div class="col-sm-7">
                                            <input value="<?= !empty($email_settings->host_mail) ? $email_settings->host_mail : '' ?>" type="text" name="host_mail" class="form-control"/>
                                            <span class="text-red"><?php echo form_error('host_mail'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('lang_smtp_port') ?> :</label>
                                        <div class="col-sm-7">
                                            <input value="<?= !empty($email_settings->smtp_port) ? $email_settings->smtp_port : '' ?>" type="number" name="smtp_port" class="form-control"/>
                                            <span class="text-red"><?php echo form_error('smtp_port'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('lang_smtp_user') ?> :</label>
                                        <div class="col-sm-7">
                                            <input value="<?= !empty($email_settings->smtp_user) ? $email_settings->smtp_user : '' ?>" type="text" name="smtp_user" class="form-control"/>
                                            <span class="text-red"><?php echo form_error('smtp_user'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('lang_smtp_pass') ?> :</label>
                                        <div class="col-sm-7">
                                            <input value="<?= !empty($email_settings->smtp_pass) ? $email_settings->smtp_pass : '' ?>" type="text" name="smtp_pass" class="form-control"/>
                                            <span class="text-red"><?php echo form_error('smtp_pass'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('prefix_text') ?> :</label>
                                        <div class="col-sm-7">
                                            <textarea class="form-control" name="prefix_text" rows="5"><?= !empty($email_settings->prefix_text) ? $email_settings->prefix_text : '' ?></textarea>
                                            <span class="text-red"><?php echo form_error('prefix_text'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('postfix_text') ?> :</label>
                                        <div class="col-sm-7">
                                            <textarea class="form-control" name="postfix_text" rows="5"><?= !empty($email_settings->postfix_text) ? $email_settings->postfix_text : '' ?></textarea>
                                            <span class="text-red"><?php echo form_error('postfix_text'); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 text-center">
                                        <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
    </body>
</html>