<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= lang("settings") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php echo form_open_multipart('admin/Settings/update', $attributes); ?>
                                    <?php if(!empty($settings->logo)) { ?>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('logo') ?> :</label>
                                        <div class="col-sm-4">
                                            <img src="<?= base_url() ?>uploads/settings/<?= $settings->logo ?>" alt="Site Logo" class="img-responsive thumbnail" width="100" >
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('logo') ?> :</label>
                                        <div class="col-sm-4">
                                            <input type="file" name="userfile" id="userfile" size="20" class="form-control"/>
                                            <?php echo form_error('userfile'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('notification_type') ?> :</label>
                                        <div class="col-sm-7">
                                            <label class="m-r-20">
                                                <input type="radio" name="notification_type" value="0" data-radio="iradio_minimal-blue" <?= ($settings->notification_type == 0) ? 'checked' : '' ?>> <?= lang('no') ?>
                                            </label>
                                            <label class="m-r-20">
                                                <input type="radio" name="notification_type" value="1" data-radio="iradio_minimal-blue" <?= ($settings->notification_type == 1) ? 'checked' : '' ?>> <?= lang('email') ?>
                                            </label>
                                            <label class="m-r-20">
                                                <input type="radio" name="notification_type" value="2" data-radio="iradio_minimal-blue" <?= ($settings->notification_type == 2) ? 'checked' : '' ?>> <?= lang('sms') ?>
                                            </label>
                                            <label class="m-r-20">
                                                <input type="radio" name="notification_type" value="3" data-radio="iradio_minimal-blue" <?= ($settings->notification_type == 3) ? 'checked' : '' ?>> <?= lang('both') ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('booking_cash_limit') ?> :</label>
                                        <div class="col-sm-7">
                                            <label class="m-r-20">
                                                <input type="radio" name="booking_cash_limit" value="0" data-radio="iradio_minimal-blue" <?= ($settings->booking_cash_limit == 0) ? 'checked' : '' ?>> <?= lang('no') ?>
                                            </label>
                                            <label class="m-r-20">
                                                <input type="radio" name="booking_cash_limit" value="1" data-radio="iradio_minimal-blue" <?= ($settings->booking_cash_limit == 1) ? 'checked' : '' ?>> <?= lang('yes') ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('booking_credit_limit') ?> :</label>
                                        <div class="col-sm-7">
                                            <label class="m-r-20">
                                                <input type="radio" name="booking_credit_limit" value="0" data-radio="iradio_minimal-blue" <?= ($settings->booking_credit_limit == 0) ? 'checked' : '' ?>> <?= lang('no') ?>
                                            </label>
                                            <label class="m-r-20">
                                                <input type="radio" name="booking_credit_limit" value="1" data-radio="iradio_minimal-blue" <?= ($settings->booking_credit_limit == 1) ? 'checked' : '' ?>> <?= lang('yes') ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('show_blog') ?> :</label>
                                        <div class="col-sm-7">
                                            <label class="m-r-20">
                                                <input type="radio" name="show_blog" value="0" data-radio="iradio_minimal-blue" <?= ($settings->show_blog == 0) ? 'checked' : '' ?>> <?= lang('no') ?>
                                            </label>
                                            <label class="m-r-20">
                                                <input type="radio" name="show_blog" value="1" data-radio="iradio_minimal-blue" <?= ($settings->show_blog == 1) ? 'checked' : '' ?>> <?= lang('yes') ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('show_about') ?> :</label>
                                        <div class="col-sm-7">
                                            <label class="m-r-20">
                                                <input type="radio" name="show_about" value="0" data-radio="iradio_minimal-blue" <?= ($settings->show_about == 0) ? 'checked' : '' ?>> <?= lang('no') ?>
                                            </label>
                                            <label class="m-r-20">
                                                <input type="radio" name="show_about" value="1" data-radio="iradio_minimal-blue" <?= ($settings->show_about == 1) ? 'checked' : '' ?>> <?= lang('yes') ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('show_privacy') ?> :</label>
                                        <div class="col-sm-7">
                                            <label class="m-r-20">
                                                <input type="radio" name="show_privacy" value="0" data-radio="iradio_minimal-blue" <?= ($settings->show_privacy == 0) ? 'checked' : '' ?>> <?= lang('no') ?>
                                            </label>
                                            <label class="m-r-20">
                                                <input type="radio" name="show_privacy" value="1" data-radio="iradio_minimal-blue" <?= ($settings->show_privacy == 1) ? 'checked' : '' ?>> <?= lang('yes') ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('show_renting') ?> :</label>
                                        <div class="col-sm-7">
                                            <label class="m-r-20">
                                                <input type="radio" name="show_renting" value="0" data-radio="iradio_minimal-blue" <?= ($settings->show_renting == 0) ? 'checked' : '' ?>> <?= lang('no') ?>
                                            </label>
                                            <label class="m-r-20">
                                                <input type="radio" name="show_renting" value="1" data-radio="iradio_minimal-blue" <?= ($settings->show_renting == 1) ? 'checked' : '' ?>> <?= lang('yes') ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('send_survey_type') ?> :</label>
                                        <div class="col-sm-7">
                                            <label class="m-r-20">
                                                <input type="radio" name="send_survey_type" value="0" data-radio="iradio_minimal-blue" <?= ($settings->send_survey_type == 0) ? 'checked' : '' ?>> <?= lang('month') ?>
                                            </label>
                                            <label class="m-r-20">
                                                <input type="radio" name="send_survey_type" value="1" data-radio="iradio_minimal-blue" <?= ($settings->send_survey_type == 1) ? 'checked' : '' ?>> <?= lang('quarter') ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('no_show_penalty') ?> :</label>
                                        <div class="col-sm-4">
                                            <input min="1" value="<?= !empty($settings->no_show_penalty) ? $settings->no_show_penalty : '' ?>" type="number" name="no_show_penalty" class="form-control"/>
                                            <span class="text-red"><?php echo form_error('no_show_penalty'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('free_cancellation_time_limit') ?> :</label>
                                        <div class="col-sm-4">
                                            <input min="1" value="<?= !empty($settings->free_cancellation_time_limit) ? $settings->free_cancellation_time_limit : '' ?>" type="number" name="free_cancellation_time_limit" class="form-control"/>
                                            <span class="text-red"><?php echo form_error('free_cancellation_time_limit'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('time_for_missing') ?> :</label>
                                        <div class="col-sm-4">
                                            <input min="1" value="<?= !empty($settings->time_for_missing) ? $settings->time_for_missing : '' ?>" type="number" name="time_for_missing" class="form-control"/>
                                            <span class="text-red"><?php echo form_error('time_for_missing'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('phone') ?> :</label>
                                        <div class="col-sm-4">
                                            <input value="<?= !empty($settings->phone) ? $settings->phone : '' ?>" type="text" name="phone" class="form-control"/>
                                            <span class="text-red"><?php echo form_error('phone'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('email') ?> :</label>
                                        <div class="col-sm-4">
                                            <input value="<?= !empty($settings->email) ? $settings->email : '' ?>" type="email" name="email" class="form-control"/>
                                            <span class="text-red"><?php echo form_error('email'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('address') ?> :</label>
                                        <div class="col-sm-4">
                                            <input value="<?= !empty($settings->address) ? $settings->address : '' ?>" type="text" name="address" class="form-control"/>
                                            <span class="text-red"><?php echo form_error('address'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('lon') ?> :</label>
                                        <div class="col-sm-4">
                                            <input value="<?= !empty($settings->longitude) ? $settings->longitude : '' ?>" type="text" name="longitude" class="form-control"/>
                                            <span class="text-red"><?php echo form_error('longitude'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('lat') ?> :</label>
                                        <div class="col-sm-4">
                                            <input value="<?= !empty($settings->latitude) ? $settings->latitude : '' ?>" type="text" name="latitude" class="form-control"/>
                                            <span class="text-red"><?php echo form_error('latitude'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('fb_url') ?> :</label>
                                        <div class="col-sm-4">
                                            <input value="<?= !empty($settings->fb_url) ? $settings->fb_url : '' ?>" type="text" name="fb_url" class="form-control"/>
                                            <span class="text-red"><?php echo form_error('fb_url'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= lang('insta_url') ?> :</label>
                                        <div class="col-sm-4">
                                            <input value="<?= !empty($settings->insta_url) ? $settings->insta_url : '' ?>" type="text" name="insta_url" class="form-control"/>
                                            <span class="text-red"><?php echo form_error('insta_url'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="app_reg_text"><?= lang('app_reg_text') ?> : </label>
                                        <div class="col-sm-4">
                                            <?php echo form_textarea(['id' => 'app_reg_text', 'name' => 'app_reg_text', 'class' => 'form-control', 'value' => !empty($settings->app_reg_text) ? $settings->app_reg_text : '', 'placeholder' => lang('app_reg_text'), 'rows' => '5', 'cols' => '15']); ?>
                                            <span class="text-red"><?php echo form_error('app_reg_text'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="unapp_reg_text"><?= lang('unapp_reg_text') ?> : </label>
                                        <div class="col-sm-4">
                                            <?php echo form_textarea(['id' => 'unapp_reg_text', 'name' => 'unapp_reg_text', 'class' => 'form-control', 'value' => !empty($settings->unapp_reg_text) ? $settings->unapp_reg_text : '', 'placeholder' => lang('unapp_reg_text'), 'rows' => '5', 'cols' => '15']); ?>
                                            <span class="text-red"><?php echo form_error('unapp_reg_text'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="app_order_text"><?= lang('app_order_text') ?> : </label>
                                        <div class="col-sm-4">
                                            <?php echo form_textarea(['id' => 'app_order_text', 'name' => 'app_order_text', 'class' => 'form-control', 'value' => !empty($settings->app_order_text) ? $settings->app_order_text : '', 'placeholder' => lang('app_order_text'), 'rows' => '5', 'cols' => '15']); ?>
                                            <span class="text-red"><?php echo form_error('app_order_text'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="order_app_text"><?= lang('unapp_order_text') ?> : </label>
                                        <div class="col-sm-4">
                                            <?php echo form_textarea(['id' => 'unapp_order_text', 'name' => 'unapp_order_text', 'class' => 'form-control', 'value' => !empty($settings->unapp_order_text) ? $settings->unapp_order_text : '', 'placeholder' => lang('unapp_order_text'), 'rows' => '5', 'cols' => '15']); ?>
                                            <span class="text-red"><?php echo form_error('unapp_order_text'); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 text-center">
                                        <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
    </body>
</html>