<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('username') ?></th>
                                                <th><?= lang('location') ?></th>
                                                <th><?= lang('cash_limit') ?></th>
                                                <th><?= lang('credit_limit') ?></th>
                                                <th><?= lang('total_price') ?></th>
                                                <th><?= lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($booked_items as $one) {
                                                ?>
                                                <?php if($i==1){ ?>
                                                <tr class="success">
                                                <?php } ?>
                                                <?php if($i>1){ ?>
                                                <tr>
                                                <?php } ?>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $one->cname ?></td>
                                                    <td><?= $one->lname ?></td>
                                                    <td><?= $one->cash_limit ?></td>
                                                    <td><?= $one->credit_limit ?></td>
                                                    <td><?= $one->total ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a title="<?= lang("view_items") ?>" href="<?= base_url() ?>admin/Booking_preparation/items/<?= $one->id ?>" class="btn btn-sm btn-primary"><i class="fa fa-list-ul"></i></a>
                                                            <?php if (get_p("booking_preparation", "d")) { ?>
                                                            <a onclick="delete_booking(<?= $one->id ?>);" href="javascript:void(0);" data-id="<?= $one->id ?>" class="delete btn btn-sm btn-danger"title="delete client"><i class="fa fa-trash" ></i></a>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>
            function delete_booking(id) {
                if (id != '') {
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url('admin/Booking/delete_booking/') ?>" + id + '/1';
                    }
                }
            }
          
        </script>
    </body>
</html>