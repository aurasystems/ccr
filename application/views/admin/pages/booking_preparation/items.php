<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-danger" role="alert" style="display:none;" id="error"> 
                                <i class="icon ti-na"></i><strong  id="err-msg"></strong>
                            </div>
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?php echo $bookCN . ' '; ?><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <div style="text-align:center;">
                                        <input style="width: 25%;" class="form-control" data-Bookid="<?= $book_id ?>" value='' placeholder="<?= lang("scan_SN") ?>" type="text" name="" id="scan">
                                    </div>

                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('product_name') ?></th>
                                                <th><?= lang('assigned_item_SN') ?></th>
                                                <th><?= lang('assign_diff_item_SN') ?></th>
                                            </tr>
                                        </thead>
                                        <div >
                                            <tbody id="update" >
                                                <?php
                                                if(!empty($items) && !$returned) {
                                                    $i = 1;
                                                    foreach ($items as $one) {
                                                    ?>
                                                    <tr>
                                                        <td><?= $i++ ?></td>
                                                        <td><?= $one->pname ?></td>
                                                        <td id="update-SN<?= $one->id ?>"><?php
                                                            $SN = get_SN($one->item_id);
                                                            echo $SN;
                                                            ?></td>
                                                        <td>
                                                            <div class="btn-group">
                                                                <select data-search="true" style="width:200px" data-Bookid="<?= $book_id ?>" data-prodID=<?= $one->product_id ?> data-prepID="<?= $one->id ?>" name="item-<?= $one->id ?>" class="dropD">
                                                                    <option  selected  value="<?= lang("choose_option") ?>" disabled><?= lang("choose_option") ?></option>
                                                                    <?php 
                                                                    $related_items = getItemsPerproduct('booking_items_preparation', $one->product_id, $book_id);
                                                                    foreach ($related_items as $item) {
                                                                        ?>
                                                                        <option  value="<?= $item->id ?>"><?= $item->serial_number ?>(<?php $loc = get_branch_by_id($item->location_id); echo $loc->{'n_' . lc()}?>)</option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div class="btn-group" style='display:<?= $SN == null ? 'none' : '' ?>'>
                                                                <button title="<?= lang("remove_assigned_item") ?>" onclick="removeAssignItem('<?= $book_id ?>', '<?= $one->product_id ?>', '<?= $one->id ?>')" type="button" class="close" aria-label="Close"><i style="color:red" class="fa fa-times"></i></button>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </div>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->

    </body>
    <script>
                                                                function init_drop() {
                                                                    setTimeout(function () {
                                                                        $('select').each(function () {
                                                                            function format(state) {
                                                                                var state_id = state.id;
                                                                                if (!state_id)
                                                                                    return state.text; // optgroup
                                                                                var res = state_id.split("-");
                                                                                if (res[0] == 'image') {
                                                                                    if (res[2])
                                                                                        return "<img class='flag' src='<?= base_url() ?>assets/images/flags/" + res[1].toLowerCase() + "-" + res[2].toLowerCase() + ".png' style='width:27px;padding-right:10px;margin-top: -3px;'/>" + state.text;
                                                                                    else
                                                                                        return "<img class='flag' src='<?= base_url() ?>assets/images/flags/" + res[1].toLowerCase() + ".png' style='width:27px;padding-right:10px;margin-top: -3px;'/>" + state.text;
                                                                                } else {
                                                                                    return state.text;
                                                                                }
                                                                            }
                                                                            $(this).select2({
                                                                                formatResult: format,
                                                                                formatSelection: format,
                                                                                placeholder: $(this).data('placeholder') ? $(this).data('placeholder') : '',
                                                                                allowClear: $(this).data('allowclear') ? $(this).data('allowclear') : true,
                                                                                minimumInputLength: $(this).data('minimumInputLength') ? $(this).data('minimumInputLength') : -1,
                                                                                minimumResultsForSearch: $(this).data('search') ? 1 : -1,
                                                                                dropdownCssClass: $(this).data('style') ? 'form-white' : '',
                                                                            });
                                                                        });

                                                                    }, 200);
                                                                }
                                                                function change_drop(id, prepID, prodID, item_id) {
                                                                    $.ajax({
                                                                        url: '<?= base_url('admin/Booking_preparation/add_items') ?>',
                                                                        type: "POST",
                                                                        data: {
                                                                            book_id: id,
                                                                            prepID: prepID,
                                                                            item_id: item_id,
                                                                            prodID: prodID,
                                                                            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                                                                        },
                                                                        dataType: "JSON",
                                                                        success: function (data) {
                                                                            $("#update").html(data);
                                                                            init_drop();
                                                                            $(".dropD").change(function () {
                                                                                var id = $(this).attr("data-Bookid");
                                                                                var prepID = $(this).attr("data-prepID");
                                                                                var prodID = $(this).attr("data-prodID");
                                                                                var item_id = $(this).val();
                                                                                change_drop(id, prepID, prodID, item_id);
                                                                            });
                                                                        },
                                                                        //  error: function(ts) { alert(ts.responseText) }
                                                                    });
                                                                }
                                                                // First init
                                                                $(".dropD").change(function () {
                                                                    var id = $(this).attr("data-Bookid");
                                                                    var prepID = $(this).attr("data-prepID");
                                                                    var prodID = $(this).attr("data-prodID");
                                                                    var item_id = $(this).val();
                                                                    change_drop(id, prepID, prodID, item_id);
                                                                });
                                                                $("#scan").change(function () {
                                                                    var book_id = $(this).attr("data-Bookid");
                                                                    var scan_value = $(this).val();
                                                                    $.ajax({
                                                                        url: '<?= base_url('admin/Booking_preparation/scan') ?>',
                                                                        type: "POST",
                                                                        data: {
                                                                            book_id: book_id,
                                                                            scan_SN: scan_value,
                                                                <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                                                                        },
                                                                        dataType: "JSON",
                                                                        success: function (data) {
                                                                            if (data.flag == true)
                                                                            {
                                                                                $("#update").html(data.options);
                                                                                init_drop();
                                                                                $('#update-SN' + data.prep_id).html(data.scan_value);
                                                                            } else if (data.flag == false)
                                                                            {
                                                                                $('#error').css('display', 'block');
                                                                                $('#err-msg').html(data.msg);
                                                                                setTimeout(function () {
                                                                                    $('#error').css('display', 'none');
                                                                                }, 3000);
                                                                            }
                                                                        }
                                                                    });
                                                                });

                                                                function removeAssignItem(book_id, prod_id, prep_id)
                                                                {
                                                                    var conf = confirm("<?= lang("r_u_sure") ?>");
                                                                    if (conf) {
                                                                        window.location = "<?= base_url() ?>admin/Booking_preparation/delete_item/" + book_id + '/' + prod_id + '/' + prep_id;
                                                                    }
                                                                }
    </script>
</html>