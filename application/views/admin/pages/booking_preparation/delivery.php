<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-danger" role="alert" style="display:none;" id="error"> 
                                <i class="icon ti-na"></i><strong  id="err-msg"></strong>
                            </div>
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <div style="text-align:center;">
                                        <input style="width: 25%;" class="form-control" value='' placeholder="<?= lang("scan_num") ?>" type="text" name="" id="scan">
                                    </div>
                                

                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('username') ?></th>
                                                <th><?= lang('location') ?></th>
                                                <th><?= lang('cash_limit') ?></th>
                                                <th><?= lang('credit_limit') ?></th>
                                                <th><?= lang('total_price') ?></th>
                                                <th><?= lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody id="update" >


                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->

    </body>

    <script>
        $("#scan").change(function () {
            var scan_value = $(this).val();
            $.ajax({
                url: '<?= base_url('admin/Booking_preparation/delivery_scan') ?>',
                type: "POST",
                data: {
                    scan_SN: scan_value,
<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: "JSON",
                success: function (data) {
                    //alert('suc');
                    $("tbody").children().remove()               
                    var trHTML = '';
                    var n = 1;
                    $.each(data, function (i, o) {
                        trHTML +=
                                '<tr><td>' + n++ +
                                '</td><td>' + o.cname +
                                '</td><td>' + o.lname +
                                '</td><td>' + o.cash_limit +
                                '</td><td>' + o.credit_limit +
                                '</td><td>' + o.total +
                                '</td><td> <a title="<?= lang("view_items") ?>" href="<?= base_url() ?>admin/Booking_preparation/delivery_items/' + o.id + '" class="btn btn-sm btn-primary"><i class="fa fa-list-ul"></i></a>'
                        '</td></tr>';
                    });
                    $('#update').append(trHTML);
                }
            });
        });

    </script>
</html>