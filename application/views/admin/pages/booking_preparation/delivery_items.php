<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                        <div class="alert alert-danger" role="alert" style="display:none;" id="error"> 
                            <i class="icon ti-na"></i><strong  id="err-msg"></strong>
                        </div>
                        <div class="alert alert-success" role="alert" style="display:none;" id="success"> 
                            <i class="icon ti-na"></i><strong  id="succ-msg"></strong>
                        </div>
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?php echo $cname.' ';?><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                <div style="text-align:center;">
                                    <input  data-Bookid="<?= $book_id ?>" value='' placeholder="<?=lang("scan_SN")?>" type="text" name="" id="scan">
                                    </div>
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('product_name') ?></th>
                                                <th><?= lang('assigned_item_SN') ?></th>
                                                <th><?= lang('assign_diff_item_SN') ?></th>
                                                <th><?= lang('shutter_count') ?></th>
                                                <th><?= lang("sup_items") ?></th>
                                            </tr>
                                        </thead>
                                        <div >
                                        <tbody id="update" >
                                            <?php
                                            $i = 1;

                                            foreach ($items as $one) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $one->pname ?></td>
                                                    <td id="update-SN<?=$one->id?>"><?php $SN = get_SN($one->item_id); echo $SN;?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                        <select data-search="true" style="width:200px" data-Bookid="<?=$book_id?>" data-prodID=<?=$one->product_id?> data-cname="<?=$cname?>" data-isSup=0 data-deliverID="<?=$one->id?>" name="item-<?=$one->id?>" class="dropD">
                                                        <option  selected  value="<?=lang("choose_option")?>" disabled><?=lang("choose_option")?></option>
                                                        <?php $related_items = getItemsPerproduct('booking_items_delivery', $one->product_id, $book_id, true);                                                          
                                                           foreach($related_items as $item){ ?>
                                                                 <option  value="<?=$item->id?>"><?=$item->serial_number?>(<?php $loc = get_branch_by_id($item->location_id); echo $loc->{'n_' . lc()}?>)</option>
                                                            <?php }?>
                                                        </select>
                                                        </div>
                                                        <div class="btn-group" style='display:<?=$SN==null? 'none':''?>'>
                                                        <button title="<?=lang("remove_assigned_item")?>" onclick="removeAssignItem('<?=$book_id?>','<?=$one->product_id?>','<?=$one->id?>', '<?=$cname?>')" type="button" class="close" aria-label="Close"><i style="color:red" class="fa fa-times"></i></button>
                                                        </div>
                                                    </td>
                                                   <td>
                                                   <?php $is_cam = is_camera($one->product_id);
                                                   if($is_cam==1)
                                                   {
                                                       $shutter_count = get_shutter_count($one->id); ?>
                                                   <input type="text" class="shutter_c" data-prev="<?=$shutter_count?>" data-id="<?=$one->id?>" value=<?=$shutter_count?>>
                                                   <?php }?>
                                                     </td>
                                                   <td style=" justify-content: space-between;">
                                                   <fieldset>
                                                   <?php $supItems = get_sup_items($one->product_id);
                                                            if(!empty($supItems)){
                                                                ?>
                                                            <?php          
                                                                foreach($supItems as $itemSup)
                                                                { 
                                                                    $count_of_sup = assignedSups_perSupProduct($book_id, $one->id, $itemSup->id);
                                                                    $SN_sup='';
                                                                    $delivery_sup = get_sup_delivery($one->id, $itemSup->id, $book_id);
                                                                    ?>                                                                
                                                                    <p><strong><?=lang('product_name')?>: </strong><?= $itemSup->n_en ?>  <i class="fa fa-times"></i> <?=$count_of_sup?></p>
                                                                    <?php if(!empty($delivery_sup)){ ?>
                                                                    <p id="updateSup-SN<?=$delivery_sup->sup_id?>"> <strong><?=lang('assigned_item_SN')?>: </strong> <?php $SN_sup=''; if($delivery_sup->sup_id!=null){ $SN_sup = get_sup_SN($delivery_sup->sup_id); echo $SN_sup; }else echo '';?></p>
                                                                    <?php }
                                                                    else {?>
                                                                    <p id="updateSup-SN<?=$delivery_sup->sup_id?>"> <strong><?=lang('assigned_item_SN')?>: </strong></p>
                                                                    <?php }?>
                                                                    <p>
                                                                        <div class="btn-group">
                                                                        <select data-search="true" style="width:200px" data-Bookid="<?=$book_id?>" data-prodID=<?=$one->product_id?> data-cname="<?=$cname?>" data-isSup=1 data-deliverID="<?=$one->id?>" name="item-<?=$one->id?>" class="dropD">
                                                                        <option  selected  value="<?=lang("choose_option")?>" disabled><?=lang("choose_option")?></option>
                                                                        <?php $related_sup_items = getSupItemsPerproduct('booking_items_delivery', $one->product_id, $book_id, $itemSup->id); 
                                                                        if(!empty($related_sup_items)){
                                                                        foreach($related_sup_items as $item_sup){?>
                                                                                <option  value="<?=$item_sup->id?>"><?=$item_sup->serial_number?></option>
                                                                        <?php }?>
                                                                        
                                                                   <?php } ?>
                                                                        </select>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                        <div>
                                                                        <?php if($count_of_sup > 1){ ?>
                                                                            <button style="opacity: 0.5;" title="<?=lang("remove_extra_sup")?>" onclick="remove_extra('<?=$book_id?>','<?=$one->id?>', '<?=$itemSup->id?>')" type="button" class="close" aria-label="Close"><i style="color:red; opacity: 4.5;" class="fa fa-minus"></i></button>
                                                                        <?php }?>
                                                                        <?php if(!empty($related_sup_items) && $SN_sup != null){
                                                                          ?>
                                                                    <button style="opacity: 0.5;" title="<?=lang("extra_sup")?>" onclick="add_extra('<?=$book_id?>','<?=$one->id?>', '<?=$itemSup->id?>','<?= end($related_sup_items)->id ?>')" type="button" class="close" aria-label="Close"><i style="color:green" class="fa fa-plus"></i></button>
                                                                        <?php }?>
                                                                       
                                                                    </div>
                                                                        <div id="update_supSN<?=$one->id?>" class="btn-group" style='display:<?=$SN_sup==null? 'none':''?>'>
                                                                        <button title="<?=lang("remove_assigned_item")?>" onclick="removeAssignSupItem('<?=$book_id?>','<?=$one->product_id?>','<?=$one->id?>', '<?=$itemSup->id?>')" type="button" class="close" aria-label="Close"><i style="color:red" class="fa fa-times"></i></button>
                                                                        </div>
                                                                    </p>
                                                                        <?php }?>
                                                                        <?php }?>
                                                                        </fieldset>

                                                   </td>
                                                </tr>
                                               
                                                                        
                                                <?php
                                                
                                            }
                                            ?>
                                           
                                                                        
                                        </tbody>
                                        </div>
                                    </table>
                                    <div class="text-center">
                                    <button type="button" class="btn btn-success" onclick="ready_to_deliver('<?=$book_id?>')"><?=lang('ready_to_deliver')?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
       
    </body>
    <script >

                
                function init_drop() {
                      setTimeout(function () {
                          $('select').each(function () {
                              function format(state) {
                                  var state_id = state.id;
                                  if (!state_id)
                                      return state.text; // optgroup
                                  var res = state_id.split("-");
                                  if (res[0] == 'image') {
                                      if (res[2])
                                          return "<img class='flag' src='<?= base_url() ?>assets/images/flags/" + res[1].toLowerCase() + "-" + res[2].toLowerCase() + ".png' style='width:27px;padding-right:10px;margin-top: -3px;'/>" + state.text;
                                      else
                                          return "<img class='flag' src='<?= base_url() ?>assets/images/flags/" + res[1].toLowerCase() + ".png' style='width:27px;padding-right:10px;margin-top: -3px;'/>" + state.text;
                                  } else {
                                      return state.text;
                                  }
                              }
                              $(this).select2({
                                  formatResult: format,
                                  formatSelection: format,
                                  placeholder: $(this).data('placeholder') ? $(this).data('placeholder') : '',
                                  allowClear: $(this).data('allowclear') ? $(this).data('allowclear') : true,
                                  minimumInputLength: $(this).data('minimumInputLength') ? $(this).data('minimumInputLength') : -1,
                                  minimumResultsForSearch: $(this).data('search') ? 1 : -1,
                                  dropdownCssClass: $(this).data('style') ? 'form-white' : '',
                              });
                          });
                      }, 200);
                  }
                $(".shutter_c").change(function(){
                    var id         = $(this).attr("data-id");
                    var prev_count = $(this).attr("data-prev");
                    var curr_count = $(this).val();
                    //alert(prev_count);

                    if(curr_count < prev_count)
                    {
                        $('#error').css('display', 'block');
                        $('#err-msg').html("<?=lang("choose_bigger_shutter")?>");
                        setTimeout(function () {                
                            $('#error').css('display', 'none');
                    }, 5000);
                    }
                    else
                    {
                        $.ajax({
                        url: '<?= base_url('admin/Booking_preparation/update_shutter_count') ?>',
                        type: "POST",
                        data: {
                            prep_id: id,
                            new_count: curr_count,
                            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                            },
                        dataType: "JSON",
                        success: function(data){
                            $(this).val(curr_count);
                            $('#success').css('display', 'block');
                            $('#succ-msg').html("<?=lang("shutter_success")?>");
                            setTimeout(function () {                
                                $('#success').css('display', 'none');
                        }, 5000);
                        },
                    });
                    }                    
                });

                function change_drop(id, prep_id, prodID, item_id, is_sup, cname) {
                    //alert(deliverID);
                    $.ajax({
                        url: '<?= base_url('admin/Booking_preparation/add_delivery_items') ?>',
                        type: "POST",
                        data: {
                            book_id: id,
                            prep_id: prep_id,
                            item_id: item_id,
                            prodID: prodID,
                            is_sup: is_sup,
                            cname: cname,
                            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                            },
                        dataType: "JSON",
                        success: function(data){
                            $("#update").html(data);
                            init_drop();
                            $(".dropD").change(function () {
                                var id = $(this).attr("data-Bookid");
                                var deliverID = $(this).attr("data-deliverID");
                                var prodID = $(this).attr("data-prodID");
                                var is_sup = $(this).attr("data-isSup");
                                var item_id = $(this).val();
                                var cname = $(this).attr("data-cname");
                              change_drop(id, deliverID, prodID, item_id, is_sup, cname);
                          });
                        }
                });
                }
                $( ".dropD").change(function() {
                var id = $(this).attr("data-Bookid");
                var deliverID = $(this).attr("data-deliverID");
                var prodID = $(this).attr("data-prodID");
                var is_sup = $(this).attr("data-isSup");
                var item_id = $(this).val();
                var cname = $(this).attr("data-cname");
                change_drop(id, deliverID, prodID, item_id, is_sup, cname);
            });

           
            $( "#scan" ).change(function() {
                var book_id = $(this).attr("data-Bookid");
                var scan_value = $(this).val();
                $.ajax({
                url: '<?= base_url('admin/Booking_preparation/delivery_items_scan') ?>',
                type: "POST",
                data: {
                    book_id: book_id,
                    scan_SN: scan_value,
                    <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                    },
               dataType: "JSON",
                success: function(data){
                    if(data.flag==true)
                    {
                        $("#update").html(data.options);
                        init_drop();
                    }
                    else if(data.flag==false)
                    {   
                        $('#error').css('display', 'block');
                        $('#err-msg').html(data.msg);
                        setTimeout(function () {                
                            $('#error').css('display', 'none');
                    }, 5000);
                    }
                }                
                });
            });

            function removeAssignItem(book_id, prod_id, prep_id, cname)
            {
                var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url() ?>admin/Booking_preparation/delete_deliver_item/" + book_id +'/' + prod_id + '/' + prep_id;
                    }
            }
            
            function removeAssignSupItem(book_id, prod_id, prep_id, product_supID)
            {
                var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                      //  alert(deliver_id);
                        window.location = "<?= base_url() ?>admin/Booking_preparation/delete_sup_item/" + book_id +'/' + prod_id + '/' + prep_id +'/' + product_supID;
                    }
            }

            function add_extra(book_id, prep_id, product_supID, sup_id)
            {
                window.location = "<?= base_url() ?>admin/Booking_preparation/add_extra_sup/" + book_id +'/' + prep_id + '/' + product_supID +'/' + sup_id;

            }
            function remove_extra(book_id, prep_id, product_supID)
            {
                window.location = "<?= base_url() ?>admin/Booking_preparation/remove_extra_sup/" + book_id +'/' + prep_id + '/' + product_supID ;
            }

            function ready_to_deliver(book_id)
            {
                window.location = "<?= base_url() ?>admin/Booking_preparation/ready_to_deliver/" + book_id  ;

            }
</script>
</html>