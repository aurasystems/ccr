<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <?php if (get_p($this->idf, "c")) { ?>
                                        <div class="text-center">
                                            <a class="btn btn-primary" href="<?= base_url('admin/'.ucfirst($this->idf) . '/add') ?>"><?= lang("add_category") ?></a>
                                        </div>
                                    <?php } ?>
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('name') ?></th>
                                                <th><?= lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach (${$this->idf} as $one) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $one->{'n_' . lc()} ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <?php if (get_p($this->idf, "u")) { ?>
                                                                <a title="<?= lang("edit") ?>" href="<?= base_url('admin/'.ucfirst($this->idf) . '/edit/' . $one->id) ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                                                            <?php } ?>
                                                            <?php if (get_p($this->idf, "v")) { ?>
                                                                <a title="<?= lang("sub_brands") ?>" href="<?= base_url('admin/'.ucfirst($this->idf) . '/sub/' . $one->id) ?>" class="btn btn-sm btn-success"><i class="fa fa-child"></i></a>
                                                            <?php } ?>
                                                            <?php if (get_p($this->idf, "d")) { ?>
                                                                <a title="<?= lang("delete") ?>" href="javascript:void(0);" onclick="del(<?= $one->id ?>);" class="delete btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
            function del (id) {
                var conf = confirm("<?= lang("r_u_sure") ?>");
                if (conf) {
                    window.location = "<?= base_url('admin/'.$this->idf . '/delete/') ?>" + id;
                }
            }
        </script>
    </body>
</html>