<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= $title ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form method="POST" enctype="Multipart/form-data" action="" class="form-horizontal" role="form">
                                        <?= csrf() ?>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('is_camera') ?></label>
                                            <div class="col-sm-2">
                                                <input type="checkbox" name="is_camera" value="1" <?= checked($one->is_camera) ?> />
                                                <span class="text-red"><?= form_error('is_camera') ?></span>
                                            </div>
                                        </div>
                                        <?php if(!empty($one->img)) { ?>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('img') ?> :</label>
                                            <div class="col-sm-2">
                                                <img class="img-md img-thumbnail" src="<?= base_url() ?>uploads/categories/<?= $one->img ?>">
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('img') ?> :</label>
                                            <div class="col-sm-2">
                                                <?php echo form_input(array('type' => 'file', 'id' => 'userfile', 'name' => 'userfile', 'class' => 'form-control', 'size' => 20)); ?>
                                                <span class="text-red"><?php echo form_error('nationality'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('parent') ?> :</label>
                                            <div class="col-sm-2">
                                                <select name='parent' id='parent' data-search='true' class='form-control'>
                                                    <option value=''><?= lang('no_parent') ?></option>
                                                    <?php
                                                    foreach ($parents as $parent) {
                                                        if (($one->parent == 0 && $one->id != $parent->id) || ($one->parent != 0)) {
                                                            ?>
                                                            <option value='<?= $parent->id ?>' <?= $parent->id == $one->parent ? 'selected' : '' ?>><?= $parent->{'n_' . lc()} ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <span class="text-red"><?php echo form_error('nationality'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('name_en') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-2">
                                                <?php echo form_input(['id' => 'n_en', 'name' => 'n_en', 'class' => 'form-control', 'value' => !empty($one->n_en) ? $one->n_en : '', 'placeholder' => lang('name_en'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('n_en'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('name_ar') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-2">
                                                <?php echo form_input(['id' => 'n_ar', 'name' => 'n_ar', 'class' => 'form-control', 'value' => !empty($one->n_ar) ? $one->n_ar : '', 'placeholder' => lang('name_ar'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('n_ar'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('description') ?> :</label>
                                            <div class="col-sm-2">
                                                <?php echo form_textarea(array('name' => 'description', 'class' => 'form-control', 'rows' => '5', 'cols' => '10', 'value' => !empty($one->description) ? $one->description : '')); ?>
                                                <span class="text-red"><?php echo form_error('description'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
    </body>
</html>