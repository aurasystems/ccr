<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= $title ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form method="POST" enctype="Multipart/form-data" action="" class="form-horizontal" role="form">
                                        <?= csrf() ?>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('voucher_name') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-4">
                                                <?php echo form_input(['id' => 'voucher_number', 'name' => 'voucher_number', 'class' => 'form-control', 'value' => set_value('voucher_number'), 'placeholder' => lang('voucher_number'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('voucher_number'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('voucher_am') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-4">
                                                <?php echo form_input(['id' => 'voucher_am', 'name' => 'amount', 'class' => 'form-control', 'value' => set_value('voucher_am'), 'placeholder' => lang('voucher_am'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('voucher_am'); ?></span>
                                            </div>
                                        </div>
                                        
                                        <div id="clients" class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('to') ?> :</label>
                                            <div class="col-sm-4">
                                                <select name='client_id' data-search='true' id="sel" class='form-control'>
                                                    <option value='0'><?= lang('all')?>&nbsp;<?=lang('clients') ?></option>
                                                    <?php
                                                    $clients = get_clients();
                                                    foreach ($clients as $one) {
                                                        ?>
                                                        <option value='<?= $one->id ?>'><?= $one->{'n_' . lc()} ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <span class="text-red"><?php echo form_error('client'); ?></span>
                                            </div>
                                        </div>
                                          <div id="type" class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('type') ?>:<span class="text-red">*</span></label>
                                                <div class="col-sm-2">
                                                    <?php echo form_radio('type', 1,true, set_checkbox('type', 1), "id='amount'"); ?>
                                                    <label class="control-label"><?= lang('amount') ?></label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <?php echo form_radio('type', 2, set_checkbox('type', 2), "id='percentage'"); ?>
                                                    <label class="control-label "><?= lang('percentage') ?></label>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="v_from"><?= lang('v_from') ?> : <span class="text-red">*</span> </label>
                                            <div class="col-sm-3">
                                            <input  autocomplete="off" name= 'v_from' class='form-control date-picker' placeholder='<?=lang('from') ?>' required>
                                            <span class="text-red"><?php echo form_error('v_from'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="v_to"><?= lang('v_to') ?> : <span class="text-red">*</span> </label>
                                            <div class="col-sm-3">
                                            <input  autocomplete="off" name= 'v_to' class='form-control date-picker' placeholder='<?=lang('to') ?> 'required>
                                                <span class="text-red"><?php echo form_error('v_to'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="number_uses"><?= lang('number_uses') ?> :</label>
                                            <div class="col-sm-3">
                                            <input  type='number' name= 'uses_total' class='form-control' placeholder='<?=lang('number_uses') ?>'>
                                                <span class="text-red"><?php echo form_error('uses_total'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
    </body>
    <script>

    </script>
</html>