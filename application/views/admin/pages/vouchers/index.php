<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/css/pages/accounting.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?=$title?></h2>
                                </div>
                                <div class="panel-body">
                                <?php if (get_p("products", "c")) { ?>
                                        <div class="text-center">
                                            <a class="btn btn-primary" href="<?= base_url() ?>admin/Vouchers/add"><?= lang("add_voucher") ?></a>
                                        </div>
                                    <?php } ?>
                                    <table class="table">
                                        <thead>
                                        <tr>
                                                <th>#</th>
                                                <th><?= lang('number') ?></th>
                                                <th><?= lang('from') ?></th>
                                                <th><?= lang('to') ?></th>
                                                <th><?= lang('amount') ?></th>
                                                <th><?= lang('clients') ?></th>
                                                <th><?= lang('uses') ?></th>
                                                <th><?= lang('remaining') ?></th>
                                                <th><?= lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i=1;
                                         foreach($vouchers as $one){ ?>
                                         <tr>
                                      <td><?=$i++?></td>
                                      <td><?=$one->voucher_number?></td>
                                      <td><?=$one->v_from?></td>
                                      <td><?=$one->v_to?></td>
                                      <td><?=$one->amount?></td>
                                      <td><?php if($one->client_id==0) echo 'All clients'; else {$res = get_record('id', $one->client_id,'clients'); echo $res[0]->n_en;}?></td>
                                      <td><?=$one->uses_total?></td>
                                      <td><?=$one->uses_remain?></td>
                                      <td>
                                      <?php if (get_p("vouchers", "u")) { ?>
                                        <a title="<?= lang("edit") ?>" href="<?= base_url() ?>admin/Vouchers/edit/<?=$one->id?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                                        <a onclick="delete_voucher(<?= $one->id ?>)" title="<?= lang("delete") ?>" class="delete btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                        <?php } ?>
                                        </td>
                                        </tr>
                                      <?php }?>
                                    </tbody>
                                    </table>
                                    

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <!--Creates the popup body-->
        <div class="modal fade" id="modal-topfull" tabindex="-1" role="dialog" aria-hidden="true" >
        <div class="modal-dialog modal-topfull" style="max-width: 500px;margin: auto;">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
            </div>
            <div class="modal-body" >
            <label for=""><?=lang("please_enter_client_penalty")?></label>
                <p class="font-nothing f-20"><input type="text" name="cost" class="cost"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-embossed" data-dismiss="modal">Save changes</button>
            </div>
            </div>
        </div>
        </div>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
       
    </body>

<script>

    function delete_voucher(product_id) {
                if (product_id != '') {
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url('admin/Vouchers/delete/') ?>" + product_id;
                    }
                }
            }

</script>
</html>