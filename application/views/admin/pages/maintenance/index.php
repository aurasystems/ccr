<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                        <div class="alert alert-danger" role="alert" style="display:none;" id="error"> 
                            <i class="icon ti-na"></i><strong  id="err-msg"></strong>
                        </div>
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?=$title?></h2>
                                </div>
                                <div class="panel-body">
                                <div style="text-align:center;">
                                    <input style="width: 25%;" class="form-control m-b-20" value='' placeholder="<?=lang("scan_sn_or_stud_tech")?>" type="text" name="" id="scan">
                                    </div>
                         
                                    <table class="table">
                                        <thead>
                                        <tr>
                                                <th>#</th>
                                                <th><?= lang('name') ?></th>
                                                <th><?= lang('m_from') ?></th>
                                                <th><?= lang('m_to') ?></th>
                                                <th><?= lang('applied') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody id="update" >

                                    </tbody>
                                    </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <!--Creates the popup body-->
        <div class="modal fade" id="modal-topfull" tabindex="-1" role="dialog" aria-hidden="true" >
        <div class="modal-dialog modal-topfull" style="max-width: 500px;margin: auto;">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
            </div>
            <div class="modal-body" >
            <label for=""><?=lang("please_enter_total_cost")?></label>
                <p class="font-nothing f-20"><input type="text" name="cost" class="cost"></p>
                <div  class="c_cost">
                <label for=""><?=lang("please_enter_client_penalty")?></label>
                <p class="font-nothing f-20"><input type="text" name="client_cost" class="client_cost"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-embossed" data-dismiss="modal">Save changes</button>
            </div>
            </div>
        </div>
        </div>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>
            $(document).ready(function () {
                
            });
            $( "#scan" ).change(function() {
                var scan_value = $(this).val();
                $.ajax({
                    url: '<?= base_url('admin/Maintenance/scan') ?>',
                    type: "POST",
                    data: {
                        scan_value: scan_value,
                        <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                        },
                    dataType: "JSON",
                    success: function(data){
                        $("tbody").children().remove()               
                        var trHTML = '';
                        var n = 1;
                        $.each(data, function (i, o){
                            var chk = '';
                            var chk_no ='';
                            if(o.is_applied==1)
                                chk = 'green';
                            else
                                chk_no = 'green';
                        trHTML += 
                                '<tr><td>' + n++ +
                                '</td><td>' + o.pname +
                                '</td><td><input id="date_from-'+o.id+'"  autocomplete="off" name= "m_from" class="form-control date-picker" onchange="change_dates('+o.id+','+o.product_id+', '+o.item_id+')"  value="'+ o.m_from +'">' +
                                '</td><td><input id="date_to-'+o.id+'" autocomplete="off" name= "m_to" class="form-control date-picker" onchange="change_dates('+o.id+','+o.product_id+', '+o.item_id+')"  value="'+ o.m_to +'">' +
                                '</td><td><input type="button" value="Yes" id="applied-'+o.id+'" onclick="get_cost('+o.id+','+o.product_id+', '+o.item_id+', '+o.sup_id+', '+o.m_to+')" name="chk-applied" style="color:black;background-color:'+chk+'"><input type="button" value="No" id="not_applied-'+o.id+'" onclick="not_applied('+o.id+','+o.product_id+', '+o.item_id+', '+o.sup_id+')" name="chk-applied" style="color:black;background-color:'+chk_no+'">'
                            '</td></tr>';
                        });
                        $('#update').append(trHTML);
                        datepicker();
                    }
                });
            });
            function change_dates(id, product_id, item_id) {
                var m_from = $('#date_from-'+id).val();
                var m_to   = $('#date_to-'+id).val();
                $.ajax({
                    url: '<?= base_url('admin/Maintenance/update_dates') ?>',
                    type: "POST",
                    data: {
                        id: id,
                        m_from: m_from,
                        m_to: m_to,
                        product_id: product_id,
                        item_id: item_id,
                        <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                        },
                   dataType: "JSON",
                    success: function(data){    
                    }
                    });
            }
            function get_cost(id, product_id, item_id, sup_id, m_to, maint_id) {
                $.ajax({
                    url: '<?= base_url('admin/Maintenance/get_damaged') ?>',
                    type: "POST",
                    data: {
                        id: id,
                        <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                        },
                  // dataType: "JSON",
                    success: function(data){
                        if(data==true)
                        {
                            $('.c_cost').css('display', 'block');
                        }    
                        else
                        $('.c_cost').css('display', 'none');

                        $('.modal').modal('show'); 
                        $('.modal').on('hidden.bs.modal', function (e) {
                            applied(id, product_id, item_id, sup_id, m_to);            
                        })     


                    }
                    });


            }
            function applied(id, product_id, item_id, sup_id, m_to) {
                var cost = $('.cost').val();
                var client_cost = $('.client_cost').val();
                $("#applied-"+id).css('background-color', 'green');
                $("#not_applied-"+id).css('background-color', 'white');
                $.ajax({
                    url: '<?= base_url('admin/Maintenance/update_applied') ?>',
                    type: "POST",
                    data: {
                        id: id,
                        product_id: product_id,
                        item_id: item_id,
                        sup_id: sup_id,
                        m_to: m_to,
                        cost: cost,
                        client_cost: client_cost,
                        <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                        },
                   dataType: "JSON",
                    success: function(data){    
                    }
                });
            }
            function not_applied(id, product_id, item_id, sup_id) {
                $("#applied-"+id).css('background-color', 'white');
                $("#not_applied-"+id).css('background-color', 'green');
                $.ajax({
                     url: '<?= base_url('admin/Maintenance/update_not_applied') ?>',
                     type: "POST",
                     data: {
                         id: id,
                         product_id: product_id,
                         item_id: item_id,
                         sup_id: sup_id,
                         <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                         },
                    dataType: "JSON",
                     success: function(data){    
                     }
                 });  
             }
        </script>
    </body>
</html>