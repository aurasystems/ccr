<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= lang("add_maintenance") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form enctype="multipart/form-data" method="POST" action="<?= base_url() ?>admin/Maintenance/add" class="form-horizontal" role="form">
                                        <?= csrf() ?>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="maintanance_type"><?= lang('maintanance_type') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <select id="maintanance_type" name="maintanance_type" class="form-control " data-search="true" required>
                                                    <option value=''><?= lang('maintanance_type') ?></option>
                                                        <option value='1'><?=lang("studio")?>/<?=lang("technician")?></option>
                                                        <option value='2'><?=lang("items")?></option>
                                                        <option value='3'><?=lang("sup_items")?></option>
                                                </select>
                                                <span class="text-red"><?php echo form_error('maintanance_type'); ?></span>
                                            </div>
                                        </div>
                                        <div id="tec_stud" class="form-group" style="display:none">
                                            <label class="col-sm-2 control-label" for="product"><?= lang('choose_item') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <select name="tec_stud" class="form-control" data-search="true" >
                                                    <option value=''><?= lang('choose_item') ?></option>
                                                        <?php $products = get_studio_technicans();
                                                        if(!empty($products)){
                                                        foreach($products as $product){ ?>
                                                       <option value=<?=$product->id?>><?=$product->n_en?></option>
                                                        <?php }
                                                    }?>
                                                </select>
                                                <span class="text-red"><?php echo form_error('tec_stud'); ?></span>
                                            </div>
                                        </div>
                                        <div id="items" class="form-group" style="display:none">
                                            <label class="col-sm-2 control-label" for="product"><?= lang('choose_item') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <select name="item" class="form-control" data-search="true" >
                                                    <option value=''><?= lang('choose_item') ?></option>
                                                        <?php $products = get_items_products();
                                                        if(!empty($products)){
                                                        foreach($products as $product){ ?>
                                                       <option value=<?=$product->id?>><?=$product->serial_number?></option>
                                                        <?php }
                                                    }?>
                                                </select>
                                                <span class="text-red"><?php echo form_error('item'); ?></span>
                                            </div>
                                        </div>
                                        <div id="sup_items" class="form-group" style="display:none">
                                            <label class="col-sm-2 control-label" for="product"><?= lang('choose_item') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <select name="sup_item" class="form-control" data-search="true" >
                                                    <option value=''><?= lang('choose_item') ?></option>
                                                        <?php $products = get_all_sup();
                                                        if(!empty($products)){
                                                        foreach($products as $product){ ?>
                                                       <option value=<?=$product->id?>><?=$product->serial_number?></option>
                                                        <?php }
                                                    }?>
                                                </select>
                                                <span class="text-red"><?php echo form_error('item'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="m_from"><?= lang('m_from') ?> : <span class="text-red">*</span> </label>
                                            <div class="col-sm-3">
                                            <input  autocomplete="off" name= 'm_from' class='form-control date-picker' placeholder=<?=lang('m_from') ?>required>
                                            <span class="text-red"><?php echo form_error('m_from'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="m_to"><?= lang('m_to') ?> : <span class="text-red">*</span> </label>
                                            <div class="col-sm-3">
                                            <input  autocomplete="off" name= 'm_to' class='form-control date-picker' placeholder=<?=lang('m_to') ?>required>
                                                <span class="text-red"><?php echo form_error('m_to'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->

    </body>
    <script>
        $( "#maintanance_type").change(function() {
       var m_t = $('#maintanance_type').val();
        if(m_t == 1)
        {
        $('#sup_items').css('display','none');
        $('#items').css('display','none');
        $('#tec_stud').css('display','block');
        }
        else if(m_t == 2)
        {
        $('#sup_items').css('display','none');
        $('#tec_stud').css('display','none');
        $('#items').css('display','block');
        }
        else if(m_t == 3)
        {
        $('#tec_stud').css('display','none');
        $('#items').css('display','none');
        $('#sup_items').css('display','block');
        }
    });

    </script>
</html>