<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= lang("attendance_import") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row" id="clock_alerts_div" style="display: none;">
                                <div class="alert alert-block alert-warning fade in text-center" style="background-color: #f2dede !important; color: #b94a48 !important;">
                                    <?= lang('select_file') ?>
                                </div>
                            </div>
                            <?php if (isset($error)): ?>
                                <div class="row upload_error">
                                    <div class="col-md-12 alert alert-danger text-center">
                                        <?php echo $error; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($this->session->flashdata('warning')) { ?>                     
                                <div class="alert alert-block alert-warning fade in text-center" style="background-color: #f2dede !important; color: #b94a48 !important;">
                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                    <?= $this->session->flashdata('warning') ?>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('msg')) { ?>
                                <div class="row">
                                    <div class="col-md-12">                                        
                                        <div class="alert alert-block alert-success fade in">
                                            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                            <?= $this->session->flashdata('msg') ?>
                                        </div>                                       
                                    </div>
                                </div>
                            <?php } ?>
                            <?php echo form_open_multipart('admin/Import/import_data', $attributes); ?>
                            <div class="col-md-3">
                                <div class="file">
                                    <div class="option-group">
                                        <span class="file-button btn-primary"><?= lang('select_file') ?></span>
                                        <input type="file" onchange="document.getElementById('uploader').value = this.value;" name="userfile" class="custom-file" aria-required="true">
                                        <input type="text" readonly="" placeholder="no file selected" id="uploader" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <input type="submit" id="submit_btn" class="btn btn-success" onclick="submit_form()" value="<?= lang("upload") ?>">
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
            $(document).ready(function () {
                $('#submit_btn').remove('disabled', true);
            });
            function submit_form() {
                $('#submit_btn').prop('disabled', true);
                $("#import_form").submit();
            }
        </script>
    </body>
</html>