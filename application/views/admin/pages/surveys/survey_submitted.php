<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    <script src="<?= base_url() ?>assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body class="<?= b_style() ?>">
        <section class="bg_white">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row text-center">
                    <h2><strong><?= $submitted_survey ?></strong></h2>
                </div>
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/cke-editor/ckeditor.js"></script> <!-- Advanced HTML Editor -->
        <script src="<?= base_url() ?>assets/plugins/cke-editor/adapters/adapters.min.js"></script>
    </body>
</html>