<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">   
                            <div id="check_alert" class="preview active" style="display: none;">
                                <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <table class="table">
                                        <tbody>
                                            <?php
                                            if (!empty($survey_questions)) {
                                                foreach ($survey_questions as $ques_ans) {
                                                    $final_total = 0;
                                                    $multi_final_total = 0;
                                                    $multi_final_avg = 0;
                                                    $op_gp_items = $this->Survey_model->get_option_group_items($ques_ans->answer_type);
                                                    if (count($op_gp_items) == 2) {
                                                        for ($i = 0; $i < count($op_gp_items); $i++) {
                                                            $two_op_ans = $this->Survey_model->get_two_options_answers($ques_ans->id, $op_gp_items[$i]->op_group_id, $op_gp_items[$i]->value);
                                                            $final_total += $two_op_ans;
                                                            $rating_avg = '';
                                                        }
                                                    } elseif (count($op_gp_items) > 2) {
                                                        for ($i = 0; $i < count($op_gp_items); $i++) {
                                                            $multi_op_ans = $this->Survey_model->get_two_options_answers($ques_ans->id, $op_gp_items[$i]->op_group_id, $op_gp_items[$i]->value);
                                                            $multi_final_avg += $multi_op_ans * $op_gp_items[$i]->value;
                                                            $multi_final_total += $multi_op_ans;
                                                        }
                                                        if ($multi_final_total > 0) {
                                                            $rating_avg = '(' . (($multi_final_avg / $multi_final_total)) . ')';
                                                        } else {
                                                            $rating_avg = 0;
                                                        }
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <h3><strong><?= $ques_ans->question ?></strong></h3>
                                                            <?php if ($rating_avg != '') { ?>
                                                                <h4><strong><?= lang('avg') . ' : ' . $rating_avg ?></strong></h4>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                            <?php
                                                            // check type and loop for answers result
                                                            if (count($op_gp_items) == 2) {
                                                                for ($i = 0; $i < count($op_gp_items); $i++) {
                                                                    $two_op_ans = $this->Survey_model->get_two_options_answers($ques_ans->id, $op_gp_items[$i]->op_group_id, $op_gp_items[$i]->value);
                                                                    if ($final_total > 0) {
                                                                        $two_op_percent = round(($two_op_ans / $final_total) * 100);
                                                                    } else {
                                                                        $two_op_percent = 0;
                                                                    }
                                                                    ?>
                                                            <tr>
                                                                <td>
                                                                    <span><?= $op_gp_items[$i]->name ?></span>
                                                                    <div class="progress progress-bar-large">
                                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= $two_op_percent ?>" aria-valuemin="0" aria-valuemax="<?= $final_total ?>" style="width: <?= $two_op_percent ?>%">
                                                                            <span class="sr-only"><?= $two_op_percent ?></span> <?= $two_op_percent ?>%
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                            }
                                                        } elseif (count($op_gp_items) > 2) {
                                                            for ($i = 0; $i < count($op_gp_items); $i++) {
                                                                $multi_op_ans = $this->Survey_model->get_two_options_answers($ques_ans->id, $op_gp_items[$i]->op_group_id, $op_gp_items[$i]->value);
                                                                if ($multi_final_total > 0) {
                                                                    $rate_ans_percent = round(($multi_op_ans / $multi_final_total) * 100);
                                                                } else {
                                                                    $rate_ans_percent = 0;
                                                                }
                                                                ?>
                                                            <tr>
                                                                <td>
                                                                    <span><?= $op_gp_items[$i]->name ?></span>
                                                                    <div class="progress progress-bar-large">
                                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= $rate_ans_percent ?>" aria-valuemin="0" aria-valuemax="<?= $multi_final_total ?>" style="width: <?= $rate_ans_percent ?>%">
                                                                            <span class="sr-only"><?= $rate_ans_percent ?></span> <?= $rate_ans_percent ?>%
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>

        </script>
    </body>
</html>