<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">   
                            <div id="check_alert" class="preview active" style="display: none;">
                                <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <?php if (get_p("surveys", "c")) { ?>
                                    <div class="col-md-12 text-center">
                                        <div id="catg_alert" class="preview active" style="display: none;">
                                            <div class="alert alert-danger media fade in text-center" id='catg_msg'>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <fieldset>
                                            <legend class="edit_legend"><?= lang('add_question') ?></legend>
                                            <div class="col-md-1">
                                                <label><?= lang('question') ?> <span class="text-red">*</span></label>
                                            </div>
                                            <div class="col-md-6">
                                                <input class="form-control" name='question' id='question' placeholder="<?= lang('question') ?>"> 
                                            </div>
                                            <div class="col-md-2">
                                                <select name="answer_type" id="answer_type" class="form-control" data-style="white">
                                                    <option value="0"><?= $this->lang->line('long_ans') ?></option>
                                                    <?php
                                                    if ($options_groups) {
                                                        foreach($options_groups as $op) {
                                                    ?>
                                                            <option value="<?= $op->id ?>"><?= $op->name ?></option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <button class="btn btn-dark" onclick="add_new_question()">
                                                    <i class="fa fa-plus-circle"></i> <?= $this->lang->line('add') ?>
                                                </button>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <?php } ?>
                                    <hr>
                                    <div class="row">
                                        <h3 style="font-weight: bold; color: #00448e;"><?= lang('questions') ?></h3>
                                        <?php if (!empty($survey_questions)) { ?>
                                            <div class="col-md-12">
                                                <table class="table table-striped text-center">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center"><?= lang('#') ?></th>
                                                            <th class="text-center"><?= lang('question') ?></th>
                                                            <th class="text-center"><?= lang('type') ?></th>
                                                            <th class="text-center"><?= lang('actions') ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $i = 1;
                                                        foreach ($survey_questions as $question) {
                                                            if ($question->answer_type == 1) {
                                                                $yes_no = 'selected';
                                                                $rating = '';
                                                                $long_ans = '';
                                                            } elseif ($question->answer_type == 2) {
                                                                $yes_no = '';
                                                                $rating = 'selected';
                                                                $long_ans = '';
                                                            } elseif ($question->answer_type == 3) {
                                                                $yes_no = '';
                                                                $rating = '';
                                                                $long_ans = 'selected';
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td><?= $i++ ?></td>
                                                                <td><input class="form-control" id='ques_<?= $question->id ?>' disabled="" value='<?= $question->question ?>'></td>
                                                                <td>
                                                                    <select name="edit_type" id="ans_type_<?= $question->id ?>" class="form-control" disabled="disabled">
                                                                        <?php
                                                                        if ($question->answer_type == 0) {
                                                                            $long_ans = 'selected';
                                                                        } else {
                                                                            $long_ans = '';
                                                                        }
                                                                        ?>
                                                                        <option value="0" <?= $long_ans ?>><?= lang('long_ans') ?></option>
                                                                        <?php
                                                                        if ($options_groups) {
                                                                            foreach ($options_groups as $op) {
                                                                                if ($op->id == $question->answer_type) {
                                                                                    $other = 'selected';
                                                                                } else {
                                                                                    $other = '';
                                                                                }
                                                                                ?>
                                                                                <option value="<?= $op->id ?>" <?= $other ?>><?= $op->name ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <div class="btn-group">
                                                                        <?php if (get_p("surveys", "u")) { ?>
                                                                        <a href="javascript:void(0);" class="btn btn-dark btn-sm" id='edit_btn_<?= $question->id ?>' onclick="edit_question(<?= $question->id ?>)">
                                                                            <i class="fa fa-edit"></i>
                                                                        </a>
                                                                        <?php } ?>
                                                                        <a href="javascript:void(0);" class="btn btn-dark btn-sm" id='update_btn_<?= $question->id ?>' style="display: none" onclick="update_question(<?= $question->id ?>)">
                                                                            <i class="fa fa-floppy-o"></i>
                                                                        </a>
                                                                        <?php if (get_p("surveys", "d")) { ?>
                                                                        <a onclick="delete_question(<?= $survey_id.','.$question->id ?>);" href="javascript:void(0);" data-id="<?= $question->id ?>" class="delete btn btn-sm btn-danger"title="<?= lang("delete") ?>"><i class="fa fa-trash" ></i></a>
                                                                        <?php } ?>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <?php
                                            } else {
                                            ?>
                                            <div class="col-md-12 text-center">
                                                <h4 style="font-weight: bold;"><?= lang('no_question') ?></h4>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>
            function add_new_question() {
                var survey_id = <?= $survey_id ?>;
                var question = $("#question").val();
                var answer_type = $("#answer_type").val();
                if (question != '' && answer_type != '') {
                    $('.loader-overlay').removeClass('loaded');
                    $.ajax({
                        url: '<?= base_url() ?>admin/Survey/add_survey_question',
                        type: "POST",
                        data: {
                            survey_id: survey_id,
                            question: question,
                            answer_type: answer_type,
                            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        dataType: "JSON",
                        success: function (result) {
                            if (result.flag == true) {
                                location.reload();
                            }
                            else {
                                $("#catg_msg").html(result.msg);
                                $("#catg_alert").slideDown();
                            }
                            $('.loader-overlay').addClass('loaded');
                        }
                    });
                }
                else {
                    $("#catg_msg").html('<?=$this->lang->line('add_ques_check')?>');
                    $("#catg_alert").slideDown();
                }
            }
            function edit_question($id) {
                $("#ques_" + $id).removeAttr('disabled');
                $("#ans_type_" + $id).removeAttr('disabled');
                $("#edit_btn_" + $id).slideUp();
                $("#update_btn_" + $id).slideDown();
            }
            function update_question($id) {
                var ques_val = $("#ques_" + $id).val();
                var ans_type = $("#ans_type_" + $id).val();
                if (ques_val != '' && ans_type != '') {
                    $('.loader-overlay').removeClass('loaded');
                    $.ajax({
                        url: '<?= base_url() ?>admin/Survey/edit_survey_question',
                        type: "POST",
                        data: {
                            id: $id,
                            ques_val: ques_val,
                            ans_type: ans_type,
                            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        dataType: "JSON",
                        success: function (result) {
                            location.reload();
                        }
                    });
                }
            }
            function delete_question(survey_id, id) {
                if (survey_id != '' && id != '') {
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url() ?>admin/Survey/delete_question/" + survey_id +"/"+ id;
                    }
                }
            }
        </script>
    </body>
</html>