<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= $title ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form method="POST" enctype="Multipart/form-data" action="" class="form-horizontal" role="form">
                                        <?= csrf() ?>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('title') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(array('id' => 'title', 'name' => 'title', 'class' => 'form-control', 'value' => (!empty($survey_data->title)) ? $survey_data->title : '', 'placeholder' => lang('title'), 'required' => 'required')); ?>
                                                <span class="text-red"><?php echo form_error('title'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('count_to_send') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(array('type' => 'number', 'min' => 1, 'id' => 'count_to_send', 'name' => 'count_to_send', 'class' => 'form-control', 'value' => (!empty($survey_data->count)) ? $survey_data->count : '', 'placeholder' => lang('count_to_send'), 'required' => 'required')); ?>
                                                <span class="text-red"><?php echo form_error('count_to_send'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('description') ?> :</label>
                                            <div class="col-sm-6">
                                                <?php echo form_textarea(array('name' => 'description', 'class' => 'form-control cke-editor', 'rows' => '5', 'cols' => '10', 'value' => (!empty($survey_data->description)) ? $survey_data->description : '')); ?>
                                                <span class="text-red"><?php echo form_error('description'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/cke-editor/ckeditor.js"></script> <!-- Advanced HTML Editor -->
        <script src="<?= base_url() ?>assets/plugins/cke-editor/adapters/adapters.min.js"></script>

        <script>

        </script>
    </body>
</html>