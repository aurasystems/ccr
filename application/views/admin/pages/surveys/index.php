<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">   
                            <div id="check_alert" class="preview active" style="display: none;">
                                <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <?php if (get_p("surveys", "c")) { ?>
                                        <div class="text-center">
                                            <a class="btn btn-primary" href="<?= base_url('admin/Survey/add_survey') ?>"><?= lang("add_survey") ?></a>
                                        </div>
                                    <?php } ?>
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th><?= $this->lang->line('#') ?></th>
                                                <th><?= $this->lang->line('title') ?></th>
                                                <th><?= $this->lang->line('count_to_send') ?></th>
                                                <th><?= $this->lang->line('rat_quest_ans') ?></th>
                                                <th><?= $this->lang->line('status') ?></th>
                                                <th><?= $this->lang->line('datetime') ?></th>
                                                <th><?= $this->lang->line('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($surveys) {
                                                $i = 1;
                                                foreach ($surveys as $survey) {
                                                    ?>
                                                    <tr>
                                                        <td><?= $i ?></td>
                                                        <td>
                                                            <a href="<?= base_url() ?>admin/Survey/get_questions/<?= $survey->id ?>">
                                                                <?= $survey->title ?>
                                                            </a>
                                                        </td>
                                                        <td><?= $survey->count ?></td>
                                                        <td>
                                                            <a href="<?= base_url() ?>admin/Survey/show_answers/<?= $survey->id ?>" class="edit btn btn-sm btn-success"><?= $this->lang->line('answers') ?></a>
                                                        </td>
                                                        <td><?= ($survey->active == 1) ? '<span class="text-success"><i class="fa fa-check fa-1x"></i></span>' : '' ?></td>
                                                        <td><?= date('Y-m-d h:i a', strtotime($survey->timestamp)) ?></td>
                                                        <td>
                                                            <div class="btn-group">
                                                                <?php if (get_p("surveys", "u")) { ?>
                                                                <a href="<?= base_url() ?>admin/Survey/edit_survey/<?= $survey->id ?>" class="edit btn btn-sm btn-dark">
                                                                    <i class="fa fa-edit"></i>
                                                                 </a>
                                                                <?php } ?>
                                                                <?php
                                                                if (get_p("surveys", "e")) {
                                                                    if ($survey->active == 0) {
                                                                        ?>
                                                                        <a onclick="active_survey(<?= $survey->id ?>);" href="javascript:void(0);" title="<?= lang("activate") ?>" href="#" data-id="<?= $survey->id ?>" class="btn btn-sm btn-success"><i class="fa fa-check"></i></a>
                                                                        <?php
                                                                    } elseif($survey->active == 1) {
                                                                        ?>
                                                                        <a onclick="deactivate_survey(<?= $survey->id ?>);" href="javascript:void(0);" title="<?= lang("deactivate") ?>" href="#" data-id="<?= $survey->id ?>" class="btn btn-sm btn-warning"><i class="fa fa-close"></i></a>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                                <?php if (get_p("surveys", "d")) { ?>
                                                                    <a onclick="delete_survey(<?= $survey->id ?>);" href="javascript:void(0);" data-id="<?= $survey->id ?>" class="delete btn btn-sm btn-danger"title="<?= lang("delete") ?>"><i class="fa fa-trash" ></i></a>
                                                                <?php } ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>
            function delete_survey(id) {
                if (id != '') {
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url() ?>admin/Survey/delete_survey/" + id;
                    }
                }
            }
            function active_survey(id) {
                if (id != '') {
                    var conf = confirm("<?= lang("act_this") ?>");
                    if (conf) {
                        window.location = "<?= base_url('admin/Survey/active_survey/') ?>" + id;
                    }
                }
            }
            function deactivate_survey(id) {
                if (id != '') {
                    var conf = confirm("<?= lang("deact_this") ?>");
                    if (conf) {
                        window.location = "<?= base_url('admin/Survey/deactivate_survey/') ?>" + id;
                    }
                }
            }
        </script>
    </body>
</html>