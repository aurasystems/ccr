<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <?php $this->load->view('admin/pages/invoice_printing/cam_js'); ?>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12 portlets">
                            <div class="row">
                                <div class="col-md-12">   
                                    <div id="check_alert" class="preview active" style="display: none;">
                                        <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?php $client = get_record('id',$client_id,'clients'); echo $client[0]->{'n_' . lc()} . ' (' . $title . ')' ?></h2>
                                </div>
                                <div class="panel-content">
                                    <?php //$this->load->view('admin/pages/clients/tab'); ?>
                                    <div class="tab-content" >
                                        <div class="tab-pane fade active in">
                                            <div class="row invoice-page">
                                                <div class="col-md-12 p-t-0"  id="printableArea">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="pull-left">
                                                                <?php if (!empty($settings->logo)) { ?>
                                                                    <img style="width: 50%;" src="<?= base_url() ?>uploads/settings/<?= $settings->logo ?>" class="img-responsive0" alt="CCR">
                                                                <?php } ?>
                                                                <address>
                                                                    <p class="width-300 m-t-10"><strong><?= lang('system_name') ?></strong></p>
                                                                    <p class="width-300"><?= (!empty($settings->address)) ? $settings->address : '' ?></p>
                                                                    <p class="width-300"><?= (!empty($settings)) ? $settings->email : '' ?></p>
                                                                    <abbr title="Phone"></abbr><?= (!empty($settings->phone)) ? $settings->phone : '' ?>
                                                                </address>
                                                            </div>
                                                            <div class="pull-right" style="margin-top: 150px;">
                                                                <address>
                                                                    <p class="width-300 m-t-10"><strong><?= (!empty($client)) ? $client[0]->{'n_'. lc()} : '' ?></strong></p>
                                                                    <p class="width-300"><?= (!empty($client)) ? $client[0]->{'address_'. lc()} : '' ?></p>
                                                                    <p class="width-300"><?= (!empty($client)) ? $client[0]->email : '' ?></p>
                                                                    <abbr title="Phone"></abbr> <?= (!empty($client)) ? $client[0]->phone : '' ?>
                                                                </address>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-12 m-t-20 m-b-20">
                                                                    <p><strong><?= lang('booking_from') ?>:</strong> <span><?= date('Y-m-d h:i a', strtotime($booking[0]->b_from)) ?></span></p>
                                                                    <p><strong><?= lang('booking_to') ?>:</strong> <span><?= date('Y-m-d h:i a', strtotime($booking[0]->b_to)) ?></span></p>
                                                                </div>
                                                            </div>
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width:65px" class="unseen text-center"><?= lang('qty') ?></th>
                                                                        <th class="text-left"><?= lang('name') ?></th>
                                                                        <th class="text-left"><?= lang('serial_number') ?></th>
                                                                        <th class="text-left"><?= lang('sup_items') ?></th>
                                                                        <th class="text-left"><?= lang('shutter_count') ?></th>
                                                                        <th class="text-left"><?= lang('date_from') ?></th>
                                                                        <th class="text-left"><?= lang('date_to') ?></th>
                                                                        <th style="width:145px" class="text-right"><?= lang('renting_price') ?></th>
                                                                        <th style="width:95px" class="text-right"><?= lang('tot') ?></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    if ($booking_items) {
                                                                        $subtotal = 0;
                                                                        $total = 0;
                                                                        foreach ($booking_items as $item) {
                                                                            $booking_item = get_from_to(($item['book_id']-$item['id']), $item['product_id']);
                                                                            $b_from = '';
                                                                            $b_to   = '';
                                                                            $flag   = false;
                                                                            if(!empty($booking_item))
                                                                            {
                                                                                $b_from = $booking_item->b_from;
                                                                                $b_to   = $booking_item->b_to;
                                                                                $flag=true;
                                                                            }
                                                                            ?>
                                                                            <tr class="item-row">
                                                                                <td class="delete-wpr">
                                                                                    <p class="qty text-center"><?= $item['quantity'] ?></p>
                                                                                </td>
                                                                                <td>
                                                                                    <div class="text-primary">
                                                                                        <p><strong><?= $item['pname'] ?></strong></p>
                                                                                    </div>
                                                                                </td>
                                                                                <td><?= get_SN($item['item_id']) ?></td>
                                                                                <td><?php $sup_items = get_delivery_sup_items($item['id']);
                                                                                if(!empty($sup_items)){
                                                                                foreach($sup_items as $one)
                                                                                    { ?>
                                                                                    <p><?=get_sup_SN($one->sup_id)?><button title="<?=lang("remove_sup")?>" onclick="remove_sup('<?=$one->id?>','<?=$book_id?>', '<?=$client_id?>')" type="button" class="close" aria-label="Close"><i style="color:red" class="fa fa-minus"></i></button></p>
                                                                                    <?php 
                                                                                    }
                                                                                } ?>
                                                                                 </td>
                                                                                <td><?=$item['shutter_count'] ?></td>
                                                                                <td>
                                                                                    <p><strong><?= date('Y-m-d h:i a', strtotime($b_from)) ?></strong></p>
                                                                                </td>
                                                                                <td>
                                                                                    <p><strong><?= date('Y-m-d h:i a', strtotime($b_to)) ?></strong></p>
                                                                                </td>
                                                                                <td>
                                                                                    <p class="text-right cost"><?php $rent_p = $flag?$booking_item->rent_price: 0; echo lang('egp') . ' ' . $rent_p; ?></p>
                                                                                </td>
                                                                                <?php $total= $flag?($booking_item->rent_price * $booking_item->quantity) : 0;
                                                                                $subtotal += $total; ?>
                                                                                <td class="text-right price"><?= lang('egp') . ' ' . ($total) ?></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                        <tr>
                                                                            <td colspan="6" rowspan="4"></td>
                                                                            <td class="text-right"><strong>Subtotal</strong></td>
                                                                            <td colspan="4" class="text-right" id="subtotal"><?= lang('egp') . ' ' . $subtotal ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="text-right no-border"><strong>Discount</strong></td>
                                                                            <td colspan="4" class="text-right"><?= $booking[0]->discount .' '. lang('egp') ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="text-right no-border">
                                                                                <div><strong>Total</strong></div>
                                                                            </td>
                                                                            <td colspan="4" class="text-right" id="total"><?=$subtotal - $booking[0]->discount .' '. lang('egp')?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4"> <strong> <?=lang("client")?>&nbsp;<?=lang("Signature") ?></strong></td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                               
                                                <div class="col-sm-4 pull-right">
                                                            <select style="width:50vh" name="" id="action">
                                                                    <option value="0"><?=lang("choose_action")?></option>
                                                                    <option value="1"><?=lang("deliverd&paid")?></option>
                                                                    <option value="2"><?=lang("deliverd&unpaid")?></option>
                                                            </select>
                                                            </div>
                                                            </div>
                                                    <div class="col-sm-4">
                                                    </div>
                                                    <div class="alert alert-success" role="alert" style="display:none;" id="success"> 
                                                    <i class="icon ti-na"></i><strong  id="succ-msg"></strong>
                                                </div>
                                                <fieldset class="pull-right" style="vertical-align:center; height:150px">
                                                        <button onclick="printDiv('printableArea')" value="Print Invoice" class="print-button"><span  class="print-icon"></span></button>
                                                </fieldset >
                                                <fieldset class="pull-right" style="vertical-align:center; height:150px;">
                                                <form action="" method="post" enctype="multipart/form-data">
                                                <?= csrf() ?>
                                                        <input class="btn btn-outline-secondary" type="button" onclick="show_hide_capture(1)" name="userfile" style=" width:80px; height:30px;" value=<?=lang("scan")?> >
                                                        </form>
                                                </fieldset>
                                                <fieldset class="pull-right" style="vertical-align:center; height:150px">
                                                <form action="<?=base_url()?>admin/Booking_preparation/upload_invoice/<?=$book_id?>/<?=$client_id?>" method="post" enctype="multipart/form-data">
                                                <?= csrf() ?>
                                                <div class="form-group">
                                                        <input type="file" name="userfile" id="userfile" size="20" class="form-control"/>
                                                    <?php echo form_error('userfile'); ?>
                                                </div>
                                                <div class="col-sm-12 text-center">
                                                    <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                                </div>
                                                </form>
                                                </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <div class="modal fade" id="capture_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
           <div class="modal-dialog modal-lg" role="document" style="width: 630px">
               <div class="modal-content">
               <form action="" method="post" enctype="multipart/form-data">
                        <?= csrf() ?>
                   <div class="modal-body">
                       <div id="camera_wrapper" class="">
                           <div id="camera"></div>
                           <br />                                
                       </div>
                   </div>
                   <div class="modal-footer">

                       <button type="button" class="btn btn-dark" id="capture_btn">
                           <i class="fa fa-camera"></i>
                       </button>
                   </div>
                   </form>
               </div>
           </div>
       </div>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script type="text/javascript">
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
        $(function () {

               //give the php file path
               webcam.set_api_url('<?= base_url() ?>admin/Booking_preparation/capture_img');
               webcam.set_swf_url('<?= base_url() ?>assets/js/webcam/webcam.swf');//flash file (SWF) file path
               webcam.set_quality(100); // Image quality (1 - 100)
               webcam.set_shutter_sound(true, '<?= base_url() ?>assets/js/webcam/shutter.mp3'); // play shutter click sound
               var camera = $('#camera');
               camera.html(webcam.get_html(600, 460)); //generate and put the flash embed code on page

               $('#capture_btn').click(function () {
                   $('.loader-overlay').removeClass('loaded');
                   //take snap
                   webcam.snap();
                   $('#show_saved_img').html('<h3>Please Wait...</h3>');
               });
               //after taking snap call show image
               webcam.set_hook('onComplete', function (img) {
                   $('#show_saved_img').html('<img class="img-thumbnail" src="<?= base_url() ?>uploads/invoice_scan/' + img + '">');
                   $("#img_cam").val(img);
                   //reset camera for the next shot
                   webcam.reset();
                   $("#capture_modal").modal('hide');
                   $('.loader-overlay').addClass('loaded');
               });
              

           });
           function show_hide_capture($val) {
               if ($val == 0) {
                   $("#upld_img_div").slideDown();
                   $("#img_cam_div").slideUp();
               }
               else {
                   $("#upld_img_div").slideUp();
                   $("#img_cam_div").slideDown();
                   $("#capture_modal").modal('show');
               }
           }

           function remove_sup(sup_deliver_id, book_id, client_id)
           {
            window.location = "<?= base_url() ?>admin/Booking_preparation/remove_sup/" + sup_deliver_id + '/' + book_id + '/' + client_id;
           }

           $('#action').change(function(){
            var val = $(this).val();
            $.ajax({
                    url: '<?= base_url('admin/Booking_preparation/update_delivery_action') ?>',
                    type: "POST",
                    data: {
                        is_paid: val,
                        client_id: <?=$client_id?>,
                        book_id: <?=$book_id?>,
                        <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                        },
                    success: function(data){ 
                     $('#success').css('display', 'block');  
                     $('#succ-msg').html('Action Received..!');
                     setTimeout(function () {                
                            $('#success').css('display', 'none');
                    }, 5000);
                    }
                });
            

           });

</script>
    </body>
</html>