<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('client') ?></th>
                                                <th><?= lang('product') ?></th>
                                                <th><?= lang('serial_number') ?></th>
                                                <th><?= lang('booking_from') ?></th>
                                                <th><?= lang('booking_to') ?></th>
                                                <th><?= lang('add_to_maintenance') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($items as $one) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><a href="<?= base_url('admin/Clients/dashboard/' . $one->client_id) ?>"><?= $one->cname ?></a></td>
                                                    <td><?= $one->pname ?></td>
                                                    <td><?= $one->serial_number ?></td>
                                                    <td><?= date('Y-m-d h:i a', strtotime($one->b_from)) ?></td>
                                                    <td><?= date('Y-m-d h:i a', strtotime($one->b_to)) ?></td>
                                                    <td>
                                                        <?php
                                                        if (get_p("damaged_items", "u")) {
                                                            if($one->added_maintenance == 0) {
                                                        ?>
                                                        <div class="btn-group">
                                                            <a onclick="add_to_maintenance(<?= $one->id ?>, <?= $one->product_id ?>, <?= $one->item_id ?>)" data-toggle="modal" data-target="#maintenance_modal" href="javascript:void(0);" data-id="<?= $one->id ?>" class="btn btn-sm btn-success" title="<?= lang("add_to_maintenance") ?>"><i class="fa fa-plus"></i></a>
                                                        </div>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <!-- Maintenance modal -->
        <div class="modal bd-example-modal-lg fade" id="maintenance_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?= $this->lang->line('add_to_maintenance') ?></h4>
                    </div>
                    <form method="POST" action="<?= base_url() ?>admin/Booking/add_to_maintenance" class="form-horizontal" role="form">
                        <?= csrf() ?>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="n_en"><?= lang('from') ?> : <span class="text-red">*</span></label>
                                <div class="col-sm-7">
                                    <?php echo form_input(['id' => 'm_from', 'name' => 'm_from', 'class' => 'form-control date-picker', 'placeholder' => lang('from'), 'required' => 'required']); ?>
                                    <span class="text-red"><?php echo form_error('m_from'); ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="n_en"><?= lang('to') ?> : <span class="text-red">*</span></label>
                                <div class="col-sm-7">
                                    <?php echo form_input(['id' => 'm_to', 'name' => 'm_to', 'class' => 'form-control date-picker', 'placeholder' => lang('to'), 'required' => 'required']); ?>
                                    <span class="text-red"><?php echo form_error('m_to'); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="return_id" id="return_id" />
                            <input type="hidden" name="product_id" id="product_id" />
                            <input type="hidden" name="item_id" id="item_id" />
                            <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!-- /.Maintenance modal -->
        <!-- END PAGE CONTENT -->
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>
            function add_to_maintenance(id, product_id, item_id){
                $("#return_id").val(id);
                $("#product_id").val(product_id);
                $("#item_id").val(item_id);
                $("#maintenance_modal").show();
            }
        </script>
    </body>
</html>