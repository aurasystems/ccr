<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?=$cname ?>&nbsp;<?=   $title ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form method="POST" action="<?=base_url()?>admin/Booking_preparation/check_return/<?=$book_id?>">
                                        <?= csrf() ?>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <th><input type="checkbox" name="all" id="all" ><span id="all_label"><?= lang('check_all') ?></span></th>
                                                <th class="text-center"><?= lang('items') ?></th>
                                                <th class="text-center"><?= lang('sup_items') ?></th>
                                                <th class="text-center"><?= lang('missing') ?></th>
                                                <th class="text-center"><?= lang('damaged') ?></th>
                                                <th class="text-center"><?= lang('shutter_count') ?></th>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if(!empty($items) && !$returned) {
                                                        $i = 1;
                                                        foreach($items as $one){
                                                        ?>
                                                        <tr>
                                                        <td><?=$i++?></td>
                                                        <td class="text-center"><?php
                                                            if($one['item_id']!= null)
                                                            echo get_SN($one['item_id']);
                                                            else if($one['item_id']==null && $one['product_id']!=null)
                                                            echo $one['pname'];
                                                             ?></td>
                                                             <td></td>
                                                        <td class="text-center">
                                                            <?php if($one['item_id'] != null){ ?>
                                                         <input  type="checkbox" name="missing-<?=$one['id']?>" value="1"/>
                                                            <?php }?>
                                                            </td>
                                                            <td class="text-center">
                                                            <?php if($one['item_id'] != null || $one['product_id'] != null){ ?>
                                                                    <input type="checkbox" name="damaged-<?=$one['id']?>" value="1"    />
                                                            <?php }?>
                                                            </td>
                                                            <td class="text-center">
                                                            <?php if($one['shutter_count']!= null) {?>
                                                                    <input type="text" name="ret_shutter_count-<?=$one['id']?>" value=""    />
                                                            <?php }?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $delivery_sup = get_delivery_sup_items($one['id']);
                                                                if(!empty($delivery_sup)){
                                                                    foreach($delivery_sup as $sup){
                                                             ?>
                                                            <tr>
                                                            <td><?=$i++?></td>
                                                            <td class="text-center"><?=get_sup_SN($sup->sup_id)?></td>
                                                            <td class="text-center"><i class="fa fa-check" aria-hidden="true"></i></td>
                                                            <td class="text-center">
                                                            <input  type="checkbox" name="missing-<?=$sup->sup_id?>" value="1"    />
                                                            </td>
                                                            <td class="text-center">
                                                                    <input type="checkbox" name="damaged-<?=$sup->sup_id?>" value="1"    />
                                                            </td>
                                                            <td class="text-center"></td>
                                                            </tr>
                                                            <?php 
                                                                    }
                                                                }
                                                            ?>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php if(!empty($items) && !$returned) { ?>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary m-b-40"><?= lang('save') ?></button>
                                        </div>
                                        <?php } ?>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script>
            $(document).ready(function () {
                $('#all').on('ifChanged', function (event) {
                    if ($(this).prop('checked'))
                    {
                        $('input').iCheck('check');
                        var label = "<?= lang('uncheck_all') ?>";
                        $('#all_label').html(label);
                    } else {
                        $('input').iCheck('uncheck');
                        var label = "<?= lang('check_all') ?>";
                        $('#all_label').html(label);
                    }
                });
               
            });
            function update_ret(id)
                {
                    alert(id);
                }
        </script>
    </body>
</html>