<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('client') ?></th>
                                                <th><?= lang('product') ?></th>
                                                <th><?= lang('serial_number') ?></th>
                                                <th><?= lang('booking_from') ?></th>
                                                <th><?= lang('booking_to') ?></th>
                                                <th><?= lang('return_date') ?></th>
                                                <th><?= lang('set_as_returned') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($items as $one) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><a href="<?= base_url('admin/Clients/dashboard/' . $one->client_id) ?>"><?= $one->cname ?></a></td>
                                                    <td><?= $one->pname ?></td>
                                                    <td><?= $one->serial_number ?></td>
                                                    <td><?= date('Y-m-d h:i a', strtotime($one->b_from)) ?></td>
                                                    <td><?= date('Y-m-d h:i a', strtotime($one->b_to)) ?></td>
                                                    <td><?= date('Y-m-d h:i a', strtotime($one->return_time)) ?></td>
                                                    <td>
                                                        <?php if (get_p("missing_items", "u")) { ?>
                                                        <div class="btn-group">
                                                            <a onclick="set_returned(<?= $one->id ?>);" title="<?= lang("returned") ?>" href="javascript:void(0);" data-id="<?= $one->id ?>" class="btn btn-sm btn-success"><i class="fa fa-check"></i></a>
                                                        </div>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <!-- END PAGE CONTENT -->
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
            function set_returned(id) {
                if (id != '') {
                    var conf = confirm("<?= lang("set_returned") ?>");
                    if (conf) {
                        window.location = "<?= base_url() ?>admin/Booking/set_item_returned/" + id;
                    }
                }
            }
        </script>
    </body>
</html>