<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $technician->{'n_' . lc()}.' ' ?><?= lang("timetable") ?></h2>
                                </div>
                                <div class="panel-body">
                                    <div class="panel-content">
                                        <?php if (empty($hours)) { ?>
                                            <form class="form-horizontal ui-sortable" role="form" method="post" action="<?php echo base_url() ?>admin/Technicians/insert_timetable/<?= $technician_id ?>" id='form'>
                                                <?= csrf() ?>
                                                <?php
                                                $days = [
                                                    '1' => "monday",
                                                    '2' => "tuesday",
                                                    '3' => "wednesday",
                                                    '4' => "thursday",
                                                    '5' => "friday",
                                                    '6' => "saturday",
                                                    '7' => "sunday",
                                                ];
                                                ?>
                                                <div class="row">
                                                    <label class="col-sm-12"> <h4><?= lang('sunday') ?> </h4></label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-1 control-label"><?= lang('from') ?> :</label>
                                                    <div class="col-sm-2">
                                                        <input class="form-control timepicker" id="time_from7" name="time_from7" type="text" value="" />
                                                    </div>
                                                    <label class="col-sm-1 control-label edit_align"><?= lang('to') ?> :</label>
                                                    <div class="col-sm-2">
                                                        <input class="form-control timepicker" id="time_to7" name="time_to7" type="text" value="" />
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label class=" control-label edit_m_t"><?= lang('off') ?></label>
                                                        <input class="form-control day_off" i="7" id="day_off7" name="day_off7" type="checkbox" value="1" />
                                                    </div>
                                                </div>
                                                <?php
                                                for ($i = 1; $i <= 6; $i++) {
                                                    ?>
                                                    <div class="row">
                                                        <label class="col-sm-12"> <h4><?= lang($days[$i]) ?> </h4></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-1 control-label"><?= lang('from') ?> :</label>
                                                        <div class="col-sm-2">
                                                            <input class="form-control timepicker" id="time_from<?= $i ?>" name="time_from<?= $i ?>" type="text"  value="" />
                                                        </div>
                                                        <label class="col-sm-1 control-label edit_align"><?= lang('to') ?> :</label>
                                                        <div class="col-sm-2">
                                                            <input class="form-control timepicker" id="time_to<?= $i ?>" name="time_to<?= $i ?>" type="text" value=""  />
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label class=" control-label edit_m_t"><?= lang('off') ?></label>
                                                            <input i="<?= $i ?>" class="form-control day_off" id="day_off<?= $i ?>" name="day_off<?= $i ?>" type="checkbox" value="1" />
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="text-center">
                                                    <input class="btn btn-primary" type="submit" value="<?= lang('save') ?>"/>
                                                </div>
                                            </form>
                                            <?php
                                        } else {
                                            ?>
                                            <form class="form-horizontal ui-sortable" role="form" method="post" action="<?php echo base_url() ?>admin/Technicians/edit_timetable/<?= $technician_id ?>" id='form'>
                                                <?= csrf() ?>
                                                <?php
                                                $days = [
                                                    '1' => "monday",
                                                    '2' => "tuesday",
                                                    '3' => "wednesday",
                                                    '4' => "thursday",
                                                    '5' => "friday",
                                                    '6' => "saturday",
                                                    '7' => "sunday",
                                                ];
                                                ?>

                                                <div class="row">
                                                    <label class="col-sm-12"> <h4><?= lang('sunday') ?> </h4></label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-1 control-label"><?= lang('from') ?> :</label>
                                                    <div class="col-sm-2">
                                                        <input class="form-control timepicker" id="time_from7" name="time_from7" type="text"  value="<?= date("h:i a", strtotime($hours[7]->time_from)) ?>" />
                                                    </div>
                                                    <label class="col-sm-1 control-label edit_align"><?= lang('to') ?> :</label>
                                                    <div class="col-sm-2">
                                                        <input class="form-control timepicker" id="time_to7" name="time_to7" type="text" value="<?= date("h:i a", strtotime($hours[7]->time_to)) ?>"  />
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label class=" control-label edit_m_t"><?= lang('off') ?></label>
                                                        <input i="7" <?= $hours[7]->day_off == 1 ? 'checked' : '' ?> class="form-control day_off" id="day_off7" name="day_off7" type="checkbox" value="1" />
                                                    </div>
                                                </div>
                                                <?php
                                                for ($i = 1; $i <= 6; $i++) {
                                                    ?>
                                                    <div class="row">
                                                        <label class="col-sm-12"> <h4><?= lang($days[$i]) ?> </h4></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-1 control-label"><?= lang('from') ?> :</label>
                                                        <div class="col-sm-2">
                                                            <input class="form-control timepicker" id="time_from<?= $i ?>" name="time_from<?= $i ?>" type="text" value="<?= date("h:i a", strtotime($hours[$i]->time_from)) ?>"  />
                                                        </div>
                                                        <label class="col-sm-1 control-label edit_align"><?= lang('to') ?> :</label>
                                                        <div class="col-sm-2">
                                                            <input class="form-control timepicker" id="time_to<?= $i ?>" name="time_to<?= $i ?>" type="text" value="<?= date("h:i a", strtotime($hours[$i]->time_to)) ?>"  />
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label class=" control-label edit_m_t"><?= lang('off') ?></label>
                                                            <input i="<?= $i ?>" <?= $hours[$i]->day_off == 1 ? 'checked' : '' ?> class="form-control day_off" id="day_off<?= $i ?>" name="day_off<?= $i ?>" type="checkbox" value="1" />
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="text-center">
                                                    <input class="btn btn-primary" type="submit" value="<?= lang('save') ?>"/>
                                                </div>
                                            </form>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script>
            $(document).ready(function () {
                $.each($('.day_off'), function (i, val) {
                    var i = $(this).attr('i');
                    if ($(this).prop('checked')) {
                        $("#time_from" + i).val('');
                        $("#time_to" + i).val('');
                        $("#time_from" + i).prop('disabled', true);
                        $("#time_to" + i).prop('disabled', true);
                    }
                });
                $('.day_off').on('ifChecked', function () {
                    var i = $(this).attr('i');
                    $("#time_from" + i).val('');
                    $("#time_to" + i).val('');
                    $("#time_from" + i).prop('disabled', true);
                    $("#time_to" + i).prop('disabled', true);

                });
                $('.day_off').on('ifUnchecked', function () {
                    var i = $(this).attr('i');
                    $("#time_from" + i).val('');
                    $("#time_to" + i).val('');
                    $("#time_from" + i).prop('disabled', false);
                    $("#time_to" + i).prop('disabled', false);
                });
            });
        </script>
    </body>
</html>