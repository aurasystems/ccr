<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= $level_name ?>&nbsp;<?= lang("permissions") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php
                                    $disabled = '';
                                    if (!get_p("access_levels", "u")) {
                                        $disabled = 'disabled';
                                    }
                                    ?>
                                    <form method="POST" action="">
                                        <?= csrf() ?>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <th><input type="checkbox" name="all" id="all" <?= $disabled ?> ><span id="all_label"><?= lang('check_all') ?></span></th>
                                                <th style="text-align:<?= lc() == 'ar' ? 'right' : 'left' ?>"><?= lang('all_access') ?></th>
                                                <th class="text-center"><?= lang('view') ?></th>
                                                <th class="text-center"><?= lang('create') ?></th>
                                                <th class="text-center"><?= lang('update') ?></th>
                                                <th class="text-center"><?= lang('activate') ?></th>
                                                <th class="text-center"><?= lang('delete') ?></th>
                                                <th class="text-center"><?= lang('widget') ?></th>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($permissions as $module => $perms) {
                                                        ?>
                                                        <tr>
                                                            <td><?= lang("$module"); ?></td>
                                                            <td style="text-align:<?= lc() == 'ar' ? 'right' : 'left' ?>">
                                                                <?php
                                                                if ($perms["a"] != 200) {
                                                                    if (in_array($module, ['users'])) {
                                                                        $own_label = lang('own');
                                                                        $all_label = lang('all');
                                                                        $assigned_label = lang('assigned');
                                                                        switch ($module) {
                                                                            case 'users':
                                                                                $limited_label = lang('branches_limited');
                                                                                break;
                                                                        }
                                                                        ?>
                                                                        <label>
                                                                            <input type="radio" name="<?= "{$module}_a" ?>" value="0"
                                                                                   data-radio="iradio_minimal-blue" <?= checked($perms["a"], 0) ?> <?= $disabled ?>> <?= $own_label ?>
                                                                        </label>
                                                                        <label>
                                                                            <input type="radio" name="<?= "{$module}_a" ?>" value="1"
                                                                                   data-radio="iradio_minimal-blue" <?= checked($perms["a"], 1) ?> <?= $disabled ?>> <?= $all_label ?>
                                                                        </label>
                                                                        <label>
                                                                            <input type="radio" name="<?= "{$module}_a" ?>" value="3"
                                                                                   data-radio="iradio_minimal-blue" <?= checked($perms["a"], 3) ?> <?= $disabled ?>> <?= $limited_label ?>
                                                                        </label>
                                                                        <?php
                                                                    } else if (in_array($module, ['access_levels'])) {
                                                                        $own_label = lang('own');
                                                                        //$limited_label = lang('companies_limited');
                                                                        switch ($module) {
                                                                            case 'access_levels':
                                                                                $limited_label = lang('exclude_private');
                                                                                break;
                                                                        }
                                                                        ?>
                                                                        <label>
                                                                            <input type="radio" name="<?= "{$module}_a" ?>" value="0"
                                                                                   data-radio="iradio_minimal-blue" <?= checked($perms["a"], 0) ?> <?= $disabled ?>> <?= $own_label ?>
                                                                        </label>
                                                                        <label>
                                                                            <input type="radio" name="<?= "{$module}_a" ?>" value="1"
                                                                                   data-radio="iradio_minimal-blue" <?= checked($perms["a"], 1) ?> <?= $disabled ?>> <?= lang('all') ?>
                                                                        </label>
                                                                        <label>
                                                                            <input type="radio" name="<?= "{$module}_a" ?>" value="2"
                                                                                   data-radio="iradio_minimal-blue" <?= checked($perms["a"], 2) ?> <?= $disabled ?>> <?= $limited_label ?>
                                                                        </label>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                <?php } else { ?>
                                                                    <?= disabled_input("{$module}_a") ?>
                                                                <?php } ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <?php if ($perms["v"] != 200) { ?>
                                                                    <input type="checkbox" name="<?= "{$module}_v" ?>" value="1" <?= checked($perms["v"]) ?> <?= $disabled ?>  />
                                                                <?php } else { ?>
                                                                    <?= disabled_input("{$module}_v") ?>
                                                                <?php } ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <?php if ($perms["c"] != 200) { ?>
                                                                    <input type="checkbox" name="<?= "{$module}_c" ?>" value="1" <?= checked($perms["c"]) ?> <?= $disabled ?>  />
                                                                <?php } else { ?>
                                                                    <?= disabled_input("{$module}_c") ?>
                                                                <?php } ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <?php if ($perms["u"] != 200) { ?>
                                                                    <input type="checkbox" name="<?= "{$module}_u" ?>" value="1" <?= checked($perms["u"]) ?> <?= $disabled ?>  />
                                                                <?php } else { ?>
                                                                    <?= disabled_input("{$module}_u") ?>
                                                                <?php } ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <?php if ($perms["e"] != 200) { ?>
                                                                    <input type="checkbox" name="<?= "{$module}_e" ?>" value="1" <?= checked($perms["u"]) ?> <?= $disabled ?>  />
                                                                <?php } else { ?>
                                                                    <?= disabled_input("{$module}_e") ?>
                                                                <?php } ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <?php if ($perms["d"] != 200) { ?>
                                                                    <input type="checkbox" name="<?= "{$module}_d" ?>" value="1" <?= checked($perms["d"]) ?> <?= $disabled ?>  />
                                                                <?php } else { ?>
                                                                    <?= disabled_input("{$module}_d") ?>
                                                                <?php } ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <?php if ($perms["w"] != 200) { ?>
                                                                    <input type="checkbox" name="<?= "{$module}_w" ?>" value="1" <?= checked($perms["w"]) ?> <?= $disabled ?>  />
                                                                <?php } else { ?>
                                                                    <?= disabled_input("{$module}_w") ?>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="text-center">
                                            <button <?= $disabled ?> type="submit" class="btn btn-primary m-b-40"><?= lang('save') ?></button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script>
            $(document).ready(function () {
                $('#all').on('ifChanged', function (event) {
                    if ($(this).prop('checked'))
                    {
                        $('input').iCheck('check');
                        var label = "<?= lang('uncheck_all') ?>";
                        $('#all_label').html(label);
                    } else {
                        $('input').iCheck('uncheck');
                        var label = "<?= lang('check_all') ?>";
                        $('#all_label').html(label);
                    }
                });
            });
        </script>
    </body>
</html>