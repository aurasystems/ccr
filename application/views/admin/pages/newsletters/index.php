<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <?php if (get_p("newsletters", "c")) { ?>
                                        <div class="text-center">
                                            <a class="btn btn-primary" href="<?= base_url() ?>admin/Newsletters/add"><?= lang("add_newsletter") ?></a>
                                        </div>
                                    <?php } ?>
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('subject') ?></th>
                                                <th><?= lang('datetime') ?></th>
                                                <th><?= lang('run_every') ?></th>
                                                <th><?= lang('last_run') ?></th>
                                                <th><?= lang('active') ?></th>
                                                <th><?= lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            $n = 0;
                                            foreach ($data as $one) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $one->subject ?></td>
                                                    <td><?= date('Y-m-d h:i a', strtotime($one->timestamp)) ?></td>
                                                    <td><?php echo $one->run_every;
                                            if ($one->unit == 1 && $one->run_every > 1)
                                                echo ' hours';
                                            else if ($one->unit == 2 && $one->run_every > 1)
                                                echo ' days';
                                            else if ($one->unit == 3 && $one->run_every > 1)
                                                echo ' weeks';
                                            else if ($one->unit == 1 && $one->run_every == 1)
                                                echo ' hour';
                                            else if ($one->unit == 2 && $one->run_every == 1)
                                                echo ' day';
                                            else if ($one->unit == 3 && $one->run_every == 1)
                                                echo ' week';
                                                ?></td>
                                                    <td><?php
                                                        if ($last_rn[$n]['day'] > 0) {
                                                            if ($last_rn[$n]['day'] == 1) {
                                                                echo $last_rn[$n]['day'] . ' day ' . $last_rn[$n]['hour'] . ' hours ago';
                                                            } else if ($last_rn[$n]['day'] > 1) {
                                                                echo $last_rn[$n]['day'] . ' days ' . $last_rn[$n]['hour'] . ' hours ago';
                                                            }
                                                        } else if ($last_rn[$n]['hour'] < 23 && $last_rn[$n]['hour'] >= 1) {
                                                            if ($last_rn[$n]['hour'] == 1) {
                                                                echo $last_rn[$n]['hour'] . ' hour ' . $last_rn[$n]['minute'] . ' minute ago';
                                                            } else if ($last_rn[$n]['hour'] > 1) {
                                                                echo $last_rn[$n]['hour'] . ' hours ' . $last_rn[$n]['minute'] . ' minute ago';
                                                            }
                                                        } else
                                                            echo $last_rn[$n]['minute'] . ' minutes ago';
                                                        ?>
                                                    </td>
                                                    <td><?= $one->status == 1 ? '<i class="fa fa-check"></i>' : '<i class="fa fa-close"></i>' ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a title="<?= lang("assignees") ?>" href="<?= base_url() ?>admin/Newsletters/assignees/<?= $one->id ?>" class="btn btn-sm btn-primary"><i class="fa fa-list-ul"></i></a>
                                                            <?php
                                                            if (get_p("newsletters", "e")) {
                                                                if ($one->status == 0) {
                                                                    ?>
                                                                    <a onclick="set_status(<?= $one->id ?>, 1);" href="javascript:void(0);" title="<?= lang("activate_this") ?>" href="#" data-id="<?= $one->id ?>" class="btn btn-sm btn-success"><i class="fa fa-check"></i></a>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <a onclick="set_status(<?= $one->id ?>, 0);" href="javascript:void(0);" title="<?= lang("deactivate") ?>" href="#" data-id="<?= $one->id ?>" class="btn btn-sm btn-warning"><i class="fa fa-close"></i></a>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                            <?php if (get_p("blog", "u")) { ?>
                                                                <a href="<?= base_url() ?>admin/Newsletters/edit/<?= $one->id ?>" class="btn btn-sm btn-warning" title="<?= lang("edit") ?>"><i class="fa fa-edit"></i></a>
                                                            <?php } ?>    
                                                            <?php if (get_p("blog", "d")) { ?>
                                                                <a onclick="delete_newsletter(<?= $one->id ?>);" href="javascript:void(0);" data-id="<?= $one->id ?>" class="delete btn btn-sm btn-danger"title="<?= lang("delete") ?>"><i class="fa fa-trash" ></i></a>
                                                                <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                $n++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
            function set_status(id, flag) {
                if (id != '') {
                    var conf;
                    if (flag) {
                        conf = confirm("<?= lang("act_this_news") ?>");
                    }
                    else {
                        conf = confirm("<?= lang("deact_this_news") ?>");
                    }
                    if (conf) {
                        window.location = "<?= base_url('admin/Newsletters/set_status/') ?>" + id + '/' + flag;
                    }
                }
            }
            function delete_newsletter(id) {
                if (id != '') {
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url() ?>admin/Newsletters/delete_newsletter/" + id;
                    }
                }
            }
            $(document).ready(function () {

            });
        </script>
    </body>
</html>