<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= $title ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form method="POST" action="" class="form-horizontal" role="form" enctype="multipart/form-data">
                                        <?= csrf() ?>
                                        <?php if (!empty($newsletter->img)) { ?>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label"><?= lang('img') ?> : </label>
                                                <div class="col-sm-3">
                                                    <img class="img-md img-thumbnail" src="<?= base_url() ?>uploads/newsletters/<?= $newsletter->img ?>">
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('img') ?> : </label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(array('type' => 'file', 'id' => 'userfile', 'name' => 'userfile', 'class' => 'form-control', 'size' => 20)); ?>
                                                <span class="text-red"><?php echo form_error('img'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="title"><?= lang('title') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['id' => 'subject', 'name' => 'subject', 'value' => (!empty($newsletter->subject)) ? $newsletter->subject : '', 'class' => 'form-control', 'placeholder' => lang('subject'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('subject'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="content"><?= lang('content') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-6">
                                                <?php echo form_textarea(['id' => 'mail_body', 'name' => 'mail_body', 'value' => (!empty($newsletter->mail_body)) ? $newsletter->mail_body : '', 'class' => 'form-control cke-editor', 'placeholder' => lang('content'), 'rows' => '5', 'cols' => '15', 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('mail_body'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="run_every"><?= lang('run_every') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-2">
                                                <?php echo form_input(['type' => 'number', 'min' => 1, 'id' => 'run_every', 'name' => 'run_every', 'value' => (!empty($newsletter->run_every)) ? $newsletter->run_every : '', 'class' => 'form-control', 'placeholder' => lang('run_every'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('run_every'); ?></span>
                                            </div>
                                            <div class="col-sm-2">
                                                <label class="control-label">
                                                    <?php echo form_radio('unit', 1, $newsletter->unit == 1 ? true : false, set_checkbox('unit', 1), "id='hour'"); ?>
                                                    <?= lang('hr') ?>
                                                </label>
                                            </div>
                                            <div class="col-sm-2">
                                                <label class="control-label">
                                                    <?php echo form_radio('unit', 2, $newsletter->unit == 2 ? true : false, set_checkbox('unit', 2), "id='day'"); ?>
                                                    <?= lang('day') ?>
                                                </label>
                                            </div>
                                            <div class="col-sm-2">
                                                <label class="control-label">
                                                    <?php echo form_radio('unit', 3, $newsletter->unit == 3 ? true : false, set_checkbox('unit', 3), "id='week'"); ?>
                                                    <?= lang('week') ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/cke-editor/ckeditor.js"></script> <!-- Advanced HTML Editor -->
        <script src="<?= base_url() ?>assets/plugins/cke-editor/adapters/adapters.min.js"></script>
        <script>
            $(document).ready(function () {

            });
        </script>
    </body>
</html>