<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <h3 class="text-center"><strong><?= lang('assign_to_all') ?></strong></h3>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="row">
                                                <div class="col-md-6 col-md-offset-3">
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <?php echo form_open('admin/Newsletters/add_assignees/' . $id, $attributes); ?>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label" for="assignees"><?= lang('assignees') ?> :</label>
                                                                <div class="col-sm-9">
                                                                    <input name="assignees[]" id="assignees" class="form-control" data-search="true" required>
                                                                    <span class="text-red"><?php echo form_error('assignees'); ?></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 text-center">
                                                                <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                                            </div>
                                                            <?php echo form_close(); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            
                                            <div class="row">
                                                <div class="col-lg-12 portlets">
                                                    <a style="margin: 0 auto; display: table;" onclick="delete_all_assignee(<?= $id ?>);" href="javascript:void(0);" data-id="<?= $id ?>" class="delete btn btn-sm btn-danger"title="<?= lang("delete_all") ?>"><?= lang("delete_all") ?></a>
                                                    <table class="table table-dynamic">
                                                        <thead>
                                                            <tr>
                                                                <th><?= $this->lang->line('#') ?></th>
                                                                <th><?= $this->lang->line('client') ?></th>
                                                                <th><?= $this->lang->line('lang_action') ?></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $i = 1;
                                                            foreach ($clients as $client) {
                                                                ?>
                                                                <tr>
                                                                    <td><?= $i++ ?></td>
                                                                    <td><?= $client->cname ?></td>
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <?php if (get_p("newsletters", "d")) { ?>
                                                                                <a onclick="delete_assignee(<?= $client->id . ',' . $client->news_l_id ?>);" href="javascript:void(0);" data-id="<?= $client->id ?>" class="delete btn btn-sm btn-danger"title="<?= lang("delete") ?>"><i class="fa fa-trash" ></i></a>
                                                                                <?php } ?>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
            function delete_assignee(id, news_id) {
                if (id != '' && news_id != '') {
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url() ?>admin/Newsletters/delete_assignee/" + id + '/' + news_id;
                    }
                }
            }
            function delete_all_assignee(id) {
                if (id != '') {
                    var conf = confirm("<?= lang("del_all_assignees") ?>");
                    if (conf) {
                        window.location = "<?= base_url() ?>admin/Newsletters/delete_all_assignee/" + id;
                    }
                }
            }
            $("#assignees").select2({
                placeholder: "<?php echo $this->lang->line('assignees'); ?> ",
                minimumInputLength: 3,
                allowClear: true,
                multiple: true,
                escapeMarkup: function (markup) {
                    return markup;
                },
                ajax: {
                    url: '<?= base_url() ?>admin/Newsletters/search_client_name',
                    dataType: 'json',
                    type: "POST",
                    quietMillis: 50,
                    data: function (term) {
                        return {
                            term: term,
                            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                        };
                    },
                    results: function (data) {
                        var results = [];
                        if (data != false) {
                            $.each(data, function (index, item) {
                                results.push({
                                    id: item.id,
                                    text: item.cname
                                });
                            });
                        } else {
                            results = [];
                        }
                        return {
                            results: results
                        };
                    }
                }
            });
        </script>
    </body>
</html>