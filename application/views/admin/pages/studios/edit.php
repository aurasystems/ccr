<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= lang("edit_studio") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form enctype="multipart/form-data" method="POST" action="<?= base_url() ?>admin/Studios/edit/<?= $id ?>" class="form-horizontal" role="form">
                                        <?= csrf() ?>
                                        <?php if(!empty($one->img)) { ?>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('img') ?> :</label>
                                            <div class="col-sm-2">
                                                <img class="img-md img-thumbnail" src="<?= base_url() ?>uploads/studios/<?= $one->img ?>">
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('img') ?> :</label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(array('type' => 'file', 'id' => 'userfile', 'name' => 'userfile', 'class' => 'form-control', 'size' => 20)); ?>
                                                <span class="text-red"><?php echo form_error('img'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="n_en"><?= lang('name_in') . lang("lang_en") ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['id' => 'n_en', 'name' => 'n_en', 'class' => 'form-control', 'value' => !empty($one->n_en) ? $one->n_en : '', 'placeholder' => lang('type_in') . lang('lang_en'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('n_en'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="n_ar"><?= lang('name_in') . lang("lang_ar") ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['id' => 'n_ar', 'name' => 'n_ar', 'class' => 'form-control', 'value' => !empty($one->n_ar) ? $one->n_ar : '', 'placeholder' => lang('type_in') . lang('lang_ar'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('n_ar'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="code"><?= lang('code') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['id' => 'code', 'name' => 'code', 'class' => 'form-control', 'value' => !empty($one->code) ? $one->code : '', 'placeholder' => lang('code'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('code'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="rent_price"><?= lang('rent_price') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['type' => 'number', 'id' => 'rent_price', 'name' => 'rent_price', 'class' => 'form-control', 'value' => !empty($one->rent_price) ? $one->rent_price : '', 'placeholder' => lang('rent_price'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('rent_price'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="location"><?= lang('location') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <select name="location" class="form-control" data-search="true" required>
                                                    <option value=''><?= lang('select_location') ?></option>
                                                    <?php foreach ($locations as $location) { ?>
                                                        <option value='<?= $location->id ?>' <?= $one->location_id == $location->id ? 'selected' : '' ?>><?= $location->{'n_' . lc()} ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="text-red"><?php echo form_error('location'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="rent_term"><?= lang('rent_term') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <select name="rent_term" data-search="true" class="form-control">
                                                    <option value='1' <?= $one->rent_term == 1 ? 'selected' : '' ?>><?= lang('day') ?></option>
                                                    <option value='2' <?= $one->rent_term == 2 ? 'selected' : '' ?>><?= lang('hr') ?></option>
                                                </select>
                                                <span class="text-red"><?php echo form_error('rent_term'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="overview"><?= lang('overview') ?> : </label>
                                            <div class="col-sm-7">
                                                <?php echo form_textarea(['id' => 'overview', 'name' => 'overview', 'class' => 'form-control', 'value' => !empty($one->overview) ? $one->overview : '', 'placeholder' => lang('overview'), 'rows' => '5', 'cols' => '15']); ?>
                                                <span class="text-red"><?php echo form_error('overview'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="description"><?= lang('description') ?> : </label>
                                            <div class="col-sm-7">
                                                <?php echo form_textarea(['id' => 'description', 'name' => 'description', 'class' => 'form-control', 'value' => !empty($one->description) ? $one->description : '', 'placeholder' => lang('description'), 'rows' => '5', 'cols' => '15']); ?>
                                                <span class="text-red"><?php echo form_error('description'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->

    </body>
</html>