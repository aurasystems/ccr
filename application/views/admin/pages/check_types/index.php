<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= lang("check_types") ?></h2>
                                </div>
                                <div class="panel-body">
                                    <?php if (get_p("users", "c")) { ?>
                                        <div class="text-center">
                                            <a class="btn btn-primary" href="<?= base_url() ?>admin/Check_types/add_type"><?= lang("add_type") ?></a>
                                        </div>
                                    <?php } ?>
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('name') ?></th>
                                                <th><?= lang('run_every') ?></th>
                                                <th><?= lang('last_run') ?></th>
                                                <th><?= lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            $n = 0;
                                            foreach ($types as $type) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $type->{'n_' . lc()} ?></td>
                                                    <td><?php echo $type->run_every;
                                            if ($type->unit == 1 && $type->run_every > 1)
                                                echo ' hours';
                                            else if ($type->unit == 2 && $type->run_every > 1)
                                                echo ' days';
                                            else if ($type->unit == 3 && $type->run_every > 1)
                                                echo ' weeks';
                                            else if ($type->unit == 1 && $type->run_every == 1)
                                                echo ' hour';
                                            else if ($type->unit == 2 && $type->run_every == 1)
                                                echo ' day';
                                            else if ($type->unit == 3 && $type->run_every == 1)
                                                echo ' week';
                                                ?></td>
                                                    <td><?php
                                                        if ($last_rn[$n]['day'] > 0) {
                                                            if ($last_rn[$n]['day'] == 1) {
                                                                echo $last_rn[$n]['day'] . ' day ' . $last_rn[$n]['hour'] . ' hours ago';
                                                            } else if ($last_rn[$n]['day'] > 1) {
                                                                echo $last_rn[$n]['day'] . ' days ' . $last_rn[$n]['hour'] . ' hours ago';
                                                            }
                                                        } else if ($last_rn[$n]['hour'] < 23 && $last_rn[$n]['hour'] >= 1) {
                                                            if ($last_rn[$n]['hour'] == 1) {
                                                                echo $last_rn[$n]['hour'] . ' hour ' . $last_rn[$n]['minute'] . ' minute ago';
                                                            } else if ($last_rn[$n]['hour'] > 1) {
                                                                echo $last_rn[$n]['hour'] . ' hours ' . $last_rn[$n]['minute'] . ' minute ago';
                                                            }
                                                        } else
                                                            echo $last_rn[$n]['minute'] . ' minutes ago';
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a title="<?= lang("view_items") ?>" href="<?= base_url() ?>admin/Check_types/items/<?= $type->id ?>" class="btn btn-sm btn-primary"><i class="fa fa-list-ul"></i></a>
                                                            <a title="<?= lang("edit") ?>" href="<?= base_url() ?>admin/Check_types/edit_type/<?= $type->id ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                                                            <a title="<?= lang("delete") ?>" href="#" data-id="<?= $type->id ?>" class="delete btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                $n++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
<?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
            $(document).ready(function () {
                $(".delete").click(function (e) {
                    e.preventDefault();
                    var id = $(this).attr("data-id");
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url('admin/Check_types/delete_type/') ?>" + id;
                    }
                });
            });
        </script>
    </body>
</html>