<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= lang("add_type") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                <form method="POST" action="" class="form-horizontal" role="form">
                                        <?= csrf() ?>
                                        <div class="form-group">
                                        <label class="col-sm-4 control-label" for="n_en"><?= lang('type_in') . lang("lang_en") ?> : <span class="text-red">*</span></label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(['value' => $type[0]->n_en!=null? $type[0]->n_en: '', 'id' => 'n_en', 'name' => 'n_en', 'class' => 'form-control', 'placeholder' => lang('type_in').lang('lang_en') , 'required' => 'required']); ?>
                                            <span class="text-red"><?php echo form_error('n_en'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="n_ar"><?= lang('type_in') . lang("lang_ar") ?> : <span class="text-red">*</span></label>
                                        <div class="col-sm-7">
                                            <?php echo form_input([ 'value' => $type[0]->n_en!=null? $type[0]->n_ar: '', 'id' => 'n_ar', 'name' => 'n_ar', 'class' => 'form-control', 'placeholder' => lang('type_in').lang('lang_ar'), 'required' => 'required']); ?>
                                            <span class="text-red"><?php echo form_error('n_ar'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="n_ar"><?= lang('run_every') ?> : <span class="text-red">*</span></label>
                                        <div class="col-sm-2">
                                            <?php echo form_input([ 'value' => $type[0]->run_every!=null? $type[0]->run_every: '', 'type' => 'number' ,'id' => 'run_every', 'name' => 'run_every', 'class' => 'form-control', 'placeholder' => lang('run_every'), 'required' => 'required']); ?>
                                            <span class="text-red"><?php echo form_error('run_every'); ?></span>
                                        </div>
                                        <div class="col-sm-2">
                                        <label class="control-label"><?= lang('hr') ?></label>
                                                <?php echo form_radio(  'unit', 1,$type[0]->unit==1? true:false, set_checkbox('unit', 1), "id='hour'"); ?>
                                                </div>
                                            <div class="col-sm-2">
                                        <label class="control-label"><?= lang('day') ?></label>
                                                <?php echo form_radio('unit', 2,$type[0]->unit==2? true:false, set_checkbox('unit', 2), "id='day'"); ?>
                                            </div>
                                            <div class="col-sm-2">
                                        <label class="control-label"><?= lang('week') ?></label>
                                                <?php echo form_radio('unit', 3,$type[0]->unit==3? true:false, set_checkbox('unit', 3), "id='week'"); ?>
                                              </div>
                                    </div>
                                    <div class="col-sm-12 text-center">
                                            <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
       
    </body>
</html>