<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= MY_APP_NAME ?> | <?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="themes-lab" name="author" />
        <link rel="shortcut icon" href="<?= base_url() ?>assets/img/favicon.png">
        <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/ui.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
    </head>
    <body class="account" data-page="login">
        <!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <div class="account-wall">
                        <i class="user-img icons-faces-users-03"></i>
                        <div class="row editErrorMsg">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                        </div>
                        <?php echo form_open('admin/Login/do_reset_password'); ?>
                        <div class="append-icon m-b-20">
                            <?php echo form_input(['type' => 'email', 'id' => 'email', 'name' => 'email', 'class' => 'form-control form-white email', 'placeholder' => 'Email', 'required' => 'required']); ?>
                            <i class="icon-lock"></i>
                            <?php echo form_error('email'); ?>
                        </div>

                        <?php echo form_submit(['class' => 'btn btn-lg btn-danger btn-block ladda-button', 'data-style' => 'expand-left', 'value' => lang('lang_pass_reset_link')]); ?>
                        <div class="text-center">
                            <a href="<?= base_url('admin/Login') ?>"><?= lang('lang_back_to_login') ?></a>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/gsap/main-gsap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/backstretch/backstretch.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.js"></script>
        <?php $this->load->view('admin/private/scripts/login_js'); ?>
    </body>
</html>