<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">   
                            <div id="check_alert" class="preview active" style="display: none;">
                                <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $one->ctname . ' ' . $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <?php
                                    $i = 1;
                                    $class_collapse = '';
                                    $collapsed_in = 'in';
                                    ?>
                                    <?php
                                    if (!empty($result)) {
                                        $i = 1;
                                        $collapse = TRUE;
                                        ?>
                                        <div id="accordion" class="panel-group">
                                            <?php
                                            foreach ($result as $k => $v) {
                                                $k_data = explode("-", $k);

                                                if ($collapse == TRUE) {
                                                    $class_collapse = '';
                                                    $collapsed_in = 'in';
                                                } else {
                                                    $class_collapse = 'collapsed';
                                                    $collapsed_in = '';
                                                }
                                                ?>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4>
                                                            <a href="#collapseOne_<?= $i ?>" data-toggle="collapse" style="font-weight: bold;" class="<?= $class_collapse ?>">
                                                                <?= $k_data[1] ?>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div class="panel-collapse collapse <?= $collapsed_in ?>" id="collapseOne_<?= $i ?>">
                                                        <div class="panel-content panel-body">
                                                            <!-- Loop for check type items -->
                                                            <form enctype="multipart/form-data" method="POST" action="<?= base_url() ?>admin/Equipment_test/equipment_checked" class="form-horizontal" role="form">
                                                                <?= csrf() ?>
                                                                <?php for ($j = 0; $j < count($v); $j++) { ?>
                                                                    <div class="form-group">
                                                                        <label class="col-sm-2 control-label" for="chkitem_<?= $v[$j]->id ?>" style="top: -5;"><?= $v[$j]->ctitem ?></label>
                                                                        <?php if (!$v[$j]->is_checked) { ?>
                                                                            <div class="col-sm-3">
                                                                                <?php echo form_checkbox(array('id' => 'chkitem_' . $v[$j]->id, 'name' => 'chkitem_' . $v[$j]->id, 'data-checkbox' => 'icheckbox_flat-blue', 'value' => 1)); ?>
                                                                            </div>
                                                                        <?php } else { ?>
                                                                            <span class="text-success p-l-20"><i class="fa fa-check fa-1x"></i></span>
                                                                            <?php } ?>
                                                                            <?php if ($v[$j]->is_checked) { ?>
                                                                            <label><span><?= lang('checked_by').' '.$v[$j]->uname ?></span></label>
                                                                            <?php } ?>
                                                                    </div>
                                                                <?php } ?>
                                                                <input type="hidden" name="test" value="<?= $id ?>" />
                                                                <input type="hidden" name="pro_item_id" value="<?= $k_data[0] ?>" />
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                $i++;
                                                $collapse = FALSE;
                                            }
                                            ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>
            $(document).ready(function () {

            });
        </script>
    </body>
</html>