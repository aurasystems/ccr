<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12 portlets">
                            <div class="row">
                                <div class="col-md-12">   
                                    <div id="check_alert" class="preview active" style="display: none;">
                                        <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $client->{'n_' . lc()}.' ('.$title.')' ?></h2>
                                </div>
                                <div class="panel-content">
                                    <?php $this->load->view('admin/pages/clients/tab'); ?>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in">
                                            <div class="row">
                                                <table class="table table-dynamic">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th><?= lang('location') ?></th>
                                                            <th><?= lang('cash_limit') ?></th>
                                                            <th><?= lang('credit_limit') ?></th>
                                                            <th><?= lang('total_price') ?></th>
                                                            <th><?= lang('invoice') ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $i = 1;
                                                        foreach ($bookings as $one) {
                                                            ?>
                                                            <tr>
                                                                <td><?= $i++ ?></td>
                                                                <td><?= $one->lname ?></td>
                                                                <td><?= $one->cash_limit ?></td>
                                                                <td><?= $one->credit_limit ?></td>
                                                                <td><?= $one->total ?></td>
                                                                <td><a title="<?= lang("invoice") ?>" href="<?= base_url() ?>admin/Clients/invoice/<?= $one->id.'/'.$client_id ?>" class="btn btn-sm btn-primary"><i class="fa fa-eye-slash"></i></a></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>

        </script>
    </body>
</html>