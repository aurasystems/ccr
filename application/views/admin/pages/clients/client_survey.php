<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">   
                            <div id="check_alert" class="preview active" style="display: none;">
                                <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $client->{'n_' . lc()} . ' ' . $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <?php if ($active_survey && count($survey_answers) > 0) { ?>
                                            <div class="row text-center">
                                                <h2><strong><?= $active_survey->title ?></strong></h2>
                                                <?php if ($active_survey->description != '' || $active_survey->description != NULL) { ?>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <p><?= $active_survey->description ?></p>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <hr>
                                            <?php if (count($survey_answers) > 0) { ?>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                                        <?php echo form_open('admin/Clients/submit_survey/' . $id . '/' . $survey_id . '/' . $client_id, $attributes); ?>
                                                        <?php
                                                        $counter = 1;
                                                        foreach ($survey_answers as $question) {
                                                            if ($question->answer_type != 0) {
                                                                $op_gp_items = $this->Survey_model->get_option_group_items($question->answer_type);
                                                                ?>
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <p><strong><?= $counter++ . '- ' . $question->question ?></strong></p>
                                                                        <input type="hidden" value="<?= $question->question_id ?>" />
                                                                        <?php
                                                                        $flag = TRUE;
                                                                        $flag_2 = TRUE;
                                                                        if (count($op_gp_items) == 2) {
                                                                            // yes or no answer
                                                                            ?>
                                                                            <div class="col-sm-12 edit_padding_l_r">
                                                                                <?php
                                                                                for ($i = 0; $i < count($op_gp_items); $i++) {
                                                                                    if ($flag == TRUE) {
                                                                                        $checked = 'checked';
                                                                                    } else {
                                                                                        $checked = '';
                                                                                    }
                                                                                    ?>
                                                                                    <div class="col-sm-1">
                                                                                        <label>
                                                                                            <?php echo form_radio(array('name' => $question->question_id, 'value' => $op_gp_items[$i]->value . ',' . $op_gp_items[$i]->name, 'class' => 'icheck', 'data-radio' => 'iradio_square-aero', 'checked' => $checked)); ?> <?= $op_gp_items[$i]->name ?>
                                                                                        </label>
                                                                                    </div>
                                                                                    <?php
                                                                                    $flag = FALSE;
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                            <?php
                                                                        } elseif (count($op_gp_items) > 2) {
                                                                            // rating answer
                                                                            ?>
                                                                            <div class="col-sm-12 edit_padding_l_r">
                                                                                <?php
                                                                                for ($i = 0; $i < count($op_gp_items); $i++) {
                                                                                    if ($flag_2 == TRUE) {
                                                                                        $checked_2 = 'checked';
                                                                                    } else {
                                                                                        $checked_2 = '';
                                                                                    }
                                                                                    ?>
                                                                                    <div class="col-sm-1">
                                                                                        <label>
                                                                                            <?php echo form_radio(array('name' => $question->question_id, 'value' => $op_gp_items[$i]->value . ',' . $op_gp_items[$i]->name, 'class' => 'icheck', 'data-radio' => 'iradio_square-aero', 'checked' => $checked_2)); ?> <?= $op_gp_items[$i]->name ?>
                                                                                        </label>
                                                                                    </div>
                                                                                    <?php
                                                                                    $flag_2 = FALSE;
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <p><strong><?= $counter++ . '- ' . $question->question ?></strong></p>
                                                                        <input type="hidden" value="<?= $question->question_id ?>" />
                                                                        <div class="col-sm-12 edit_padding_l_r">
                                                                            <?php echo form_textarea(array('name' => $question->question_id, 'class' => 'form-control form-white', 'value' => (!empty($question->answer_name)) ? $question->answer_name : '')); ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <?php
                                                            }
                                                            ?>
                                                            <?php
                                                        }
                                                        ?>
                                                        <div class="row mrgn-top-5">
                                                            <div class="col-sm-7 col-sm-offset-5">
                                                                <?php echo form_submit(array('type' => 'submit', 'value' => $this->lang->line('submit'), 'class' => 'btn btn-success')); ?>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="row text-center">
                                                    <h2><strong><?= $this->lang->line('submitted_survey') ?></strong></h2>
                                                </div>
                                            <?php } ?>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="row text-center">
                                                <h2><strong><?= $this->lang->line('submitted_survey') ?></strong></h2>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script type="text/javascript">

        </script>
    </body>
</html>