<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12 portlets">
                            <div class="row">
                                <div class="col-md-12">   
                                    <div id="check_alert" class="preview active" style="display: none;">
                                        <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $client->{'n_' . lc()}.' ('.$title.')' ?></h2>
                                </div>
                                <div class="panel-content">
                                    <?php $this->load->view('admin/pages/clients/tab'); ?>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in">
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <h2 style="font-weight: bold; color: #00448e;"><?= $client->{'n_' . lc()} ?></h2>
                                                    <h4 style="font-weight: bold;"><?= (!empty($last_booking)) ? lang('last_booking').' : '.date('Y-m-d g:i a', strtotime($last_booking)) : '' ?></h4>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-xlg-2 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="panel custom_panel">
                                                        <div class="panel-content widget-info">
                                                            <div class="row">
                                                                <div class="left">
                                                                    <i class="fa fa-star bg-dark"></i>
                                                                </div>
                                                                <div class="right">
                                                                    <p class=""><?= (!empty($most_rented)) ? $most_rented->pname : '' ?></p>
                                                                    <p class="text"><?= lang('most_rented') ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xlg-2 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="panel custom_panel">
                                                        <div class="panel-content widget-info">
                                                            <div class="row">
                                                                <div class="left">
                                                                    <i class="fa fa-cart-arrow-down bg-dark"></i>
                                                                </div>
                                                                <div class="right">
                                                                    <p class="number countup" data-from="0" data-to="<?= (!empty($bookings_count)) ? $bookings_count->count : 0 ?>">0</p>
                                                                    <p class="text"><?= lang('num_of_bookings') ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>

        </script>
    </body>
</html>