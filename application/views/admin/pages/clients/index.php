<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <?php
                            $decline_reasons = '';
                            foreach ($reasons as $reason) {
                                $decline_reasons .= '<option value="' . $reason->id . '">' . $reason->n_en . '</option>';
                            }
                            ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= lang("clients") ?></h2>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('username') ?></th>
                                                <th><?= lang('email') ?></th>
                                                <th><?= lang('type') ?></th>
                                                <th><?= lang('status') ?></th>
                                                <th><?= lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($clients_data as $one) {
                                                $c_l = $one->cash_limit;
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><a href="<?= base_url('Admin/Clients/crm/' . $one->id) ?>"><?= $one->{'n_' . lc()} ?></a></td>
                                                    <td><?= $one->email ?></td>
                                                    <td><?= $one->type == 1 ? lang('corp') : lang('indiv');' <i class="fa fa-user"></i>' ?></td>
                                                    <td><?= $one->status == 1 ? lang('approved') : lang('unapproved') ?>  </td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a data-name=<?= $one->n_en ?>  data-toggle="<?= $one->status ? '' : 'modal' ?>" data-target="<?= $one->status ? '' : '#myModal' ?>" href="#" id='approve_limit' data-status=<?= $one->status ?> data-id= <?= $one->id ?> class="appr btn btn-sm btn-success" title="<?= $one->status ? lang("unapprove_client") : lang("approve_client") ?>"><i   class="<?= $one->status ? 'fa fa-user' : 'fa fa-user-plus' ?>"></i>
                                                            <a onclick="decline_client(<?= $one->id ?>)" id="decline" data-toggle="modal" data-target="#decline_modal" href="javascript:void(0);" data-id="<?= $one->id ?>" class="btn btn-sm btn-danger" title="<?= lang("decline") ?>"><i class="fa fa-close"></i></a>
                                                            <a href="<?= base_url() ?>admin/Clients/edit/<?= $one->id ?>" class="btn btn-sm btn-warning" title="edit client"><i class="fa fa-edit"></i></a>
                                                            <a onclick="delete_client(<?= $one->id ?>);" href="javascript:void(0);" data-id="<?= $one->id ?>" class="delete btn btn-sm btn-danger"title="delete client"><i class="fa fa-trash" ></i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <!-- Decline modal -->
        <div class="modal bd-example-modal-lg fade" id="decline_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?= $this->lang->line('add_decline_reason') ?></h4>
                    </div>
                    <form method="POST" action="<?= base_url() ?>admin/Clients/decline_client" class="form-horizontal" role="form">
                        <?= csrf() ?>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="n_en"><?= lang('decline_reasons') ?> : <span class="text-red">*</span></label>
                                <div class="col-sm-7">
                                    <select name='reason' id='reason' data-search='true' class='form-control'>
                                        <option value=''><?= lang('select_d_r') ?></option>
                                        <?php
                                        foreach ($decline_reasons as $reason) {
                                            ?>
                                            <option value='<?= $reason->id ?>'><?= $reason->{'n_' . lc()} ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <span class="text-red"><?php echo form_error('reason'); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="client_id" id="client_id" />
                            <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!-- /.Decline modal -->
        <!-- Client limits modal -->
        <div class="modal bd-example-modal-lg fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><strong><?= $this->lang->line('client_limit') ?></strong></h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?= base_url() ?>admin/Clients/limits" class="form-horizontal" role="form">
                            <?= csrf() ?>
                            <?php echo form_input(['value' => '', 'id' => 'id_hidden', 'type' => 'hidden', 'name' => 'id']); ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="n_en"><?= lang('cash_limit') ?> : <span class="text-red">*</span></label>
                                <div class="col-sm-7">
                                    <?php echo form_input(['value' => '', 'id' => 'cash_limit', 'name' => 'cash_limit', 'class' => 'form-control', 'placeholder' => lang('cash_limit'), 'required' => 'required']); ?>
                                    <span class="text-red"><?php echo form_error('cash_limit'); ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="n_ar"><?= lang('credit_limit') ?> : <span class="text-red">*</span></label>
                                <div class="col-sm-7">
                                    <?php echo form_input(['value' => '', 'id' => 'credit_limit', 'name' => 'credit_limit', 'class' => 'form-control', 'placeholder' => lang('credit_limit'), 'required' => 'required']); ?>
                                    <span class="text-red"><?php echo form_error('credit_limit'); ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= lang('flag_1') ?></label>
                                <div class="col-sm-7">
                                    <label class="control-label"><?= lang("yes") ?></label>
                                    <?php echo form_radio('flag_1', 0, true, set_checkbox('flag_1', 0), "id='flag_1_y'"); ?>
                                    <label class="control-label"><?= lang("no") ?></label>
                                    <?php echo form_radio('flag_1', 1, set_checkbox('flag_1', 1), "id='flag_1_n'"); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= lang('flag_2') ?></label>
                                <div class="col-sm-7">
                                    <label class="control-label"><?= lang("yes") ?></label>
                                    <?php echo form_radio('flag_2', 0, true, set_checkbox('flag_2', 0), "id='flag_2_n'"); ?>
                                    <label class="control-label"><?= lang("no") ?></label>
                                    <?php echo form_radio('flag_2', 1, set_checkbox('flag_2', 1), "id='flag_2_n'"); ?>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- Client limits modal -->
        <!-- END PAGE CONTENT -->
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
            function decline_client(client_id){
                $("#client_id").val(client_id);
                $("#decline_modal").show();
            }
            function delete_client(client_id) {
                if (client_id != '') {
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url() ?>admin/Clients/delete_client/" + client_id;
                    }
                }
            }
            $(document).ready(function () {
//                $("#decline").click(function (e) {
//                    e.preventDefault();
//                    var id = $(this).attr("data-id");
//                    $("#client_id").val(id);
//                });
                $(document).on("click", ".appr", function () {
                    var id = $(this).attr("data-id");
                    var name = $(this).attr("data-name");
                    $("#id_hidden").val(id);
                    $("#client_name").html(name);

                });
//                $(".delete").click(function (e) {
//                    e.preventDefault();
//                    var id = $(this).attr("data-id");
//                    var conf = confirm("<?= lang("r_u_sure") ?>");
//                    if (conf) {
//                        window.location = "<?= base_url() ?>admin/Clients/delete_client/" + id;
//                    }
//                });
                    $(".appr").click(function (e) {
                        e.preventDefault();
                        var id = $(this).attr("data-id");
                        var status = $(this).attr("data-status");
                        if (status == 1) {
                            var conf = confirm("<?= lang("r_u_sure_cl_unapprove") ?>");
                            if (conf) {
                                window.location = "<?= base_url() ?>admin/Clients/unapprove_client/" + id;
                            }
                        }
                    });
//                $('.decline').click(function(e)
//                {
//                    var id = $(this).attr("data-id");      
//                    $('#cl' + id ).toggle('slow');
//                    e.preventDefault();
//                });

                var buttons = document.querySelectorAll('.dec_reas');
                for (var i = 0; i < buttons.length; i++) {
                    var self = buttons[i];
                    self.addEventListener('click', function (event) {
                        event.preventDefault();
                        var id = $(this).attr("data-id");
                        var value = $('#reason_' + id).val();
                        var conf = confirm("<?= lang("r_u_sure_cl_decline") ?>");
                        if (conf) {
                            window.location = "<?= base_url() ?>admin/Clients/decline_client/" + id + '/' + value;
                        }
                    });
                }
            });
        </script>
    </body>
</html>