<ul class="nav nav-tabs nav-primary">
    <li class="<?= ($active == 'dashboard') ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Clients/dashboard/<?= $client_id ?>"><?= lang('dashboard') ?></a></li>
    <li class="<?= ($active == 'old_bookings') ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Clients/old_bookings/<?= $client_id ?>"><?= lang('old_bookings') ?></a></li>
    <li class="<?= ($active == 'today_bookings') ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Clients/today_bookings/<?= $client_id ?>"><?= lang('today_bookings') ?></a></li>
    <li class="<?= ($active == 'coming_bookings') ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Clients/coming_bookings/<?= $client_id ?>"><?= lang('coming_bookings') ?></a></li>
    <li class="<?= ($active == 'no_shows') ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Clients/no_shows/<?= $client_id ?>"><?= lang('no_shows') ?></a></li>
    <li class="<?= ($active == 'maintenance') ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Clients/items_with_maintenance/<?= $client_id ?>"><?= lang('maintenance_on_rented_items') ?></a></li>
    <?php if (get_p("wallet", "v")) { ?>
    <li class="<?= ($active == 'wallet') ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Clients/wallet/<?= $client_id ?>"><?= lang('penalties') ?></a></li>
    <?php } ?>
    <li class="<?= ($active == 'surveys') ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Clients/surveys/<?= $client_id ?>"><?= lang('surveys') ?></a></li>
    <li class="<?= ($active == 'transactions') ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Clients/transactions/<?= $client_id ?>"><?= lang('transactions') ?></a></li>
</ul>