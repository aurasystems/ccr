<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('username') ?></th>
                                                <th><?= lang('email') ?></th>
                                                <th><?= lang('type') ?></th>
                                                <th><?= lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($clients_data as $one) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><a href="<?= base_url('admin/Clients/dashboard/' . $one->id) ?>"><?= $one->{'n_' . lc()} ?></a></td>
                                                    <td><?= $one->email ?></td>
                                                    <td><?= $one->type == 1 ? lang('corp') : lang('indiv');' <i class="fa fa-user"></i>' ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a onclick="approve_client(<?= $one->id ?>);" title="<?= lang("approve") ?>" href="javascript:void(0);" data-id="<?= $one->id ?>" class="btn btn-sm btn-success"><i class="fa fa-check"></i></a>
                                                            <a href="<?= base_url() ?>admin/Clients/edit/<?= $one->id ?>" class="btn btn-sm btn-warning" title="edit client"><i class="fa fa-edit"></i></a>
                                                            <a onclick="delete_client(<?= $one->id ?>);" href="javascript:void(0);" data-id="<?= $one->id ?>" class="delete btn btn-sm btn-danger"title="delete client"><i class="fa fa-trash" ></i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <!-- END PAGE CONTENT -->
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
            function decline_client(client_id){
                $("#client_id").val(client_id);
                $("#decline_modal").show();
            }
            function delete_client(client_id) {
                if (client_id != '') {
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url() ?>admin/Clients/delete_client/" + client_id + '/2';
                    }
                }
            }
            function approve_client(client_id) {
                if (client_id != '') {
                    var conf = confirm("<?= lang("approve_this_client") ?>");
                    if (conf) {
                        window.location = "<?= base_url() ?>admin/Clients/approve_this_client/" + client_id;
                    }
                }
            }
            $(document).ready(function () {
                $(document).on("click", ".appr", function () {
                    var id = $(this).attr("data-id");
                    var name = $(this).attr("data-name");
                    $("#id_hidden").val(id);
                    $("#client_name").html(name);

                });
//                $(".delete").click(function (e) {
//                    e.preventDefault();
//                    var id = $(this).attr("data-id");
//                    var conf = confirm("<?= lang("r_u_sure") ?>");
//                    if (conf) {
//                        window.location = "<?= base_url() ?>admin/Clients/delete_client/" + id;
//                    }
//                });
                    $(".appr").click(function (e) {
                        e.preventDefault();
                        var id = $(this).attr("data-id");
                        var status = $(this).attr("data-status");
                        if (status == 1) {
                            var conf = confirm("<?= lang("r_u_sure_cl_unapprove") ?>");
                            if (conf) {
                                window.location = "<?= base_url() ?>admin/Clients/unapprove_client/" + id;
                            }
                        }
                    });
//                $('.decline').click(function(e)
//                {
//                    var id = $(this).attr("data-id");      
//                    $('#cl' + id ).toggle('slow');
//                    e.preventDefault();
//                });

//                var buttons = document.querySelectorAll('.dec_reas');
//                for (var i = 0; i < buttons.length; i++) {
//                    var self = buttons[i];
//                    self.addEventListener('click', function (event) {
//                        event.preventDefault();
//                        var id = $(this).attr("data-id");
//                        var value = $('#reason_' + id).val();
//                        var conf = confirm("<?= lang("r_u_sure_cl_decline") ?>");
//                        if (conf) {
//                            window.location = "<?= base_url() ?>admin/Clients/decline_client/" + id + '/' + value;
//                        }
//                    });
//                }
            });
        </script>
    </body>
</html>