<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= lang("edit_client") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form method="POST" action="" class="form-horizontal" role="form">
                                        <?= csrf() ?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="n_en"><?= lang('name_in') . lang("lang_en") ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['value' => $flag ? $client_data[0]->n_en : '', 'id' => 'n_en', 'name' => 'n_en', 'class' => 'form-control', 'placeholder' => lang('name_in') . lang('lang_en'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('n_en'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="n_ar"><?= lang('name_in') . lang("lang_ar") ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['value' => $flag ? $client_data[0]->n_ar : '', 'id' => 'n_ar', 'name' => 'n_ar', 'class' => 'form-control', 'placeholder' => lang('name_in') . lang('lang_ar'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('n_ar'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('email') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['value' => $flag ? $client_data[0]->email : '', 'type' => 'email', 'id' => 'email', 'name' => 'email', 'class' => 'form-control', 'placeholder' => lang('email'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('email'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('national_id') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['value' => $flag ? $client_data[0]->national_id : '', 'type' => 'number', 'id' => 'national_id', 'name' => 'national_id', 'class' => 'form-control', 'placeholder' => lang('national_id'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('national_id'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('birthday') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['value' => $flag ? $client_data[0]->birthday : '', 'id' => 'birthday', 'name' => 'birthday', 'class' => 'form-control date-picker', 'placeholder' => lang('birthday'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('birthday'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('address_en') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['value' => $flag ? $client_data[0]->address_en : '', 'type' => 'text', 'id' => 'address_en', 'name' => 'address_en', 'class' => 'form-control', 'placeholder' => lang('address_en') . lang('lang_en'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('address_en'); ?></span>
                                            </div>
                                        </div>            
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('address_ar') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['value' => $flag ? $client_data[0]->address_ar : '', 'type' => 'text', 'id' => 'address_ar', 'name' => 'address_ar', 'class' => 'form-control', 'placeholder' => lang('address_ar') . lang('lang_ar'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('address_ar'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('phone') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['value' => $flag ? $client_data[0]->phone : '', 'type' => 'text', 'id' => 'phone', 'name' => 'phone', 'class' => 'form-control', 'placeholder' => lang('phone'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('phone'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('gender') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <select name='gender' id='gender' data-search='true' class='form-control'>
                                                    <option value=''><?= lang('select_gender') ?></option>
                                                    <?php foreach (get_genders() as $one) { ?>
                                                        <option value='<?= $one['id'] ?>' <?= $client_data[0]->gender == $one['id'] ? 'selected' : '' ?>><?= $one[lc()] ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="text-red"><?php echo form_error('gender'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('cash_limit') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['value' => $flag ? $client_data[0]->cash_limit : '', 'type' => 'number', 'id' => 'cash_limit', 'name' => 'cash_limit', 'class' => 'form-control', 'placeholder' => lang('cash_limit'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('cash_limit'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('credit_limit') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['value' => $flag ? $client_data[0]->credit_limit : '', 'type' => 'number', 'id' => 'credit_limit', 'name' => 'credit_limit', 'class' => 'form-control', 'placeholder' => lang('credit_limit'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('credit_limit'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('password') ?> :</label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['type' => 'password', 'id' => 'password', 'name' => 'password', 'class' => 'form-control', 'placeholder' => lang('password')]); ?>
                                                <span class="text-red"><?php echo form_error('password'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('password_confirm') ?> :</label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['type' => 'password', 'id' => 'confirm_password', 'name' => 'confirm_password', 'class' => 'form-control', 'placeholder' => lang('password_confirm')]); ?>
                                                <span class="text-red"><?php echo form_error('confirm_password'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('type') ?></label>
                                            <div class="col-sm-3">
                                                <select id="type" name="type" data-search='true' class='form-control'>
                                                    <option value=""> <?= lang('type') ?></option>
                                                    <option value="1" <?= $client_data[0]->type == 1 ? 'selected' : '' ?>><?= lang('Corporation') ?></option>
                                                    <option value="2" <?= $client_data[0]->type == 2 ? 'selected' : '' ?>><?= lang('indiv') ?></option>
                                                </select>
                                                <span class="text-red"><?php echo form_error('type'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group" id="corp_name_div" style="display: none;">
                                            <label class="col-sm-3 control-label"><?= lang('specify_corp_name') ?></label>
                                            <div class="col-sm-3">
                                                <input value="<?= $flag ? $client_data[0]->corp_n : '' ?>" id="corp_name" name="corp_n" type="text" class="form-control" placeholder="<?= lang('please_ntr_corp_n') ?>" >
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script>
            $(document).ready(function () {
                <?php if($client_data[0]->type == 1) { ?>
                     $("#corp_name_div").show();   
                <?php } else { ?>
                    $("#corp_name_div").hide();
                <?php } ?>
            });
            $("#type").change(function () {
                var value = $(this).val();
                if (value == 1) {
                    $("#corp_name_div").show();
                } else {
                    $("#corp_name_div").hide();
                }
            });
        </script>
    </body>
</html>