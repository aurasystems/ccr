<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12 portlets">
                            <div class="row">
                                <div class="col-md-12">   
                                    <div id="check_alert" class="preview active" style="display: none;">
                                        <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $client->{'n_' . lc()} . ' (' . $title . ')' ?></h2>
                                </div>
                                <div class="panel-content">
                                    <?php $this->load->view('admin/pages/clients/tab'); ?>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in">
                                            <div class="row">
                                                <table class="table table-dynamic">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th><?= lang('item') . ' (' . lang('serial_number') . ')' ?></th>
                                                            <th><?= lang('amount') ?></th>
                                                            <th><?= lang('reason') ?></th>
                                                            <th><?= lang('added') ?></th>
                                                            <th><?= lang('history') ?></th>
                                                            <th><?= lang('action') ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $i = 1;
                                                        foreach ($penalties as $one) {
                                                            ?>
                                                            <tr>
                                                                <td><?= $i++ ?></td>
                                                                <td><?= $one->serial_number ?></td>
                                                                <td><?= $one->sum ?></td>
                                                                <td><?= $one->due_to == 1 ? lang('damaged') : lang('missing') ?></td>
                                                                <td><?= date('Y-m-d h:i a', strtotime($one->timestamp)) ?></td>
                                                                <td><a class="btn btn-sm btn-primary" title="<?= lang("history") ?>" href="<?= base_url('admin/Clients/penalties_history/' . $client_id.'/'.$one->booking_id.'/'.$one->item_id) ?>"><?= lang('history') ?></a></td>
                                                                <td>
                                                                    <?php
                                                                    if (get_p("wallet", "u")) {
                                                                        if($one->sum != 0) {
                                                                    ?>
                                                                    <div class="btn-group">
                                                                        <a onclick="settle_amount(<?= $client_id ?>, <?= $one->booking_id ?>, <?= $one->item_id ?>, <?= $one->sum ?>, <?= $one->due_to ?>);" title="<?= lang("settle_penalty") ?>" href="javascript:void(0);" data-id="<?= $one->id ?>" class="btn btn-sm btn-success"><?= lang('settle_penalty') ?></a>
                                                                    </div>
                                                                    <?php
                                                                        }
                                                                        else {
                                                                            echo lang('settled');
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>
            function settle_amount(client_id, booking_id, item_id, sum, due_to) {
                if (client_id == '' || booking_id == '' || item_id == '' || sum == '' || due_to == '') {
                    $("#check_msg").html('<?= $this->lang->line('error_occured') ?>');
                    $("#check_alert").slideDown();
                } 
                else {
                    var conf = confirm("<?= lang("settle_this") ?>");
                    if(conf) {
                        $("#check_alert").slideUp();
                        $('.loader-overlay').removeClass('loaded');
                        $.ajax({
                            url: '<?= base_url() ?>admin/Clients/settle_penalty',
                            type: "POST",
                            data: {
                                client_id: client_id,
                                booking_id: booking_id,
                                item_id: item_id,
                                sum: sum,
                                due_to: due_to,
                                <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                            },
                            dataType: "JSON",
                            success: function (result) {
                                if(result == false) {
                                    $("#check_msg").html('<?= $this->lang->line('error_occured') ?>');
                                    $("#check_alert").slideDown();
                                    $('.loader-overlay').addClass('loaded');
                                }
                                else {
                                    location.reload();
                                }
                            }
                        });
                    }
                }
            }
        </script>
    </body>
</html>