<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12 portlets">
                            <div class="row">
                                <div class="col-md-12">   
                                    <div id="check_alert" class="preview active" style="display: none;">
                                        <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $client->{'n_' . lc()} . ' (' . $title . ')' ?></h2>
                                </div>
                                <div class="panel-content">
                                    <?php //$this->load->view('admin/pages/clients/tab'); ?>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in">
                                            <div class="row invoice-page">
                                                <div class="col-md-12 p-t-0">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="pull-left">
                                                                <?php if (!empty($settings->logo)) { ?>
                                                                    <img style="width: 50%;" src="<?= base_url() ?>uploads/settings/<?= $settings->logo ?>" class="img-responsive0" alt="CCR">
                                                                <?php } ?>
                                                                <address>
                                                                    <p class="width-300 m-t-10"><strong><?= lang('system_name') ?></strong></p>
                                                                    <p class="width-300"><?= (!empty($settings->address)) ? $settings->address : '' ?></p>
                                                                    <p class="width-300"><?= (!empty($settings)) ? $settings->email : '' ?></p>
                                                                    <abbr title="Phone"></abbr><?= (!empty($settings->phone)) ? $settings->phone : '' ?>
                                                                </address>
                                                            </div>
                                                            <div class="pull-right" style="margin-top: 150px;">
                                                                <address>
                                                                    <p class="width-300 m-t-10"><strong><?= (!empty($client)) ? $client->{'n_'. lc()} : '' ?></strong></p>
                                                                    <p class="width-300"><?= (!empty($client)) ? $client->{'address_'. lc()} : '' ?></p>
                                                                    <p class="width-300"><?= (!empty($client)) ? $client->email : '' ?></p>
                                                                    <abbr title="Phone"></abbr> <?= (!empty($client)) ? $client->phone : '' ?>
                                                                </address>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-12 m-t-20 m-b-20">
                                                                    <p><strong><?= lang('booking_from') ?>:</strong> <span><?= date('Y-m-d h:i a', strtotime($booking->b_from)) ?></span></p>
                                                                    <p><strong><?= lang('booking_to') ?>:</strong> <span><?= date('Y-m-d h:i a', strtotime($booking->b_to)) ?></span></p>
                                                                </div>
                                                            </div>
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width:65px" class="unseen text-center"><?= lang('qty') ?></th>
                                                                        <th class="text-left"><?= lang('desc') ?></th>
                                                                        <th class="text-left"><?= lang('date_from') ?></th>
                                                                        <th class="text-left"><?= lang('date_to') ?></th>
                                                                        <th style="width:145px" class="text-right"><?= lang('renting_price') ?></th>
                                                                        <th style="width:95px" class="text-right"><?= lang('tot') ?></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    if ($booking_items) {
                                                                        $subtotal = 0;
                                                                        $total = 0;
                                                                        foreach ($booking_items as $item) {
                                                                            $subtotal = $item->rent_price * $item->quantity;
                                                                            $total += $subtotal;
                                                                            ?>
                                                                            <tr class="item-row">
                                                                                <td class="delete-wpr">
                                                                                    <p class="qty text-center"><?= $item->quantity ?></p>
                                                                                </td>
                                                                                <td>
                                                                                    <div class="text-primary">
                                                                                        <p><strong><?= $item->pname ?></strong></p>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    <p><strong><?= date('Y-m-d h:i a', strtotime($item->b_from)) ?></strong></p>
                                                                                </td>
                                                                                <td>
                                                                                    <p><strong><?= date('Y-m-d h:i a', strtotime($item->b_to)) ?></strong></p>
                                                                                </td>
                                                                                <td>
                                                                                    <p class="text-right cost"><?= lang('egp') . ' ' . $item->rent_price ?></p>
                                                                                </td>
                                                                                <td class="text-right price"><?= lang('egp') . ' ' . ($item->rent_price * $item->quantity) ?></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                        <tr>
                                                                            <td colspan="4" rowspan="4"></td>
                                                                            <td class="text-right"><strong>Subtotal</strong></td>
                                                                            <td class="text-right" id="subtotal"><?= lang('egp') . ' ' . $total ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="text-right no-border"><strong>Discount</strong></td>
                                                                            <td class="text-right"><?= lang('egp') . ' ' . $booking->discount ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="text-right no-border">
                                                                                <div><strong>Total</strong></div>
                                                                            </td>
                                                                            <td class="text-right" id="total"><?= lang('egp') . ' ' . ($total - $booking->discount) ?></td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>

        </script>
    </body>
</html>