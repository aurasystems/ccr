<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                        <div class="alert alert-danger" role="alert" style="display:none;" id="error"> 
                            <i class="icon ti-na"></i><strong  id="err-msg"></strong>
                        </div>
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?=$title?></h2>
                                </div>
                                <div class="panel-body">
                                <div style="">
                                    <select style="text-align:center; width: 25%;" class="form-control m-b-20" name="" id="type">
                                    <option value="0"><?=lang("select_type")?></option>
                                    <option value="1"><?=lang("income")?></option>
                                    <option value="2"><?=lang("expense")?></option>
                                    </select>
                                </div>
                         
                                    <table class="table">
                                        <thead>
                                        <tr>
                                                <th>#</th>
                                                <th><?= lang('name') ?></th>
                                                <th id="name"><?= lang('client') ?></th>
                                                <th><?= lang('paid') ?></th>
                                                <th><?= lang('desc') ?></th>
                                                <th id="v_num"><?= lang('voucher_number') ?></th>
                                                <th id="v_am"><?= lang('voucher_am') ?></th>
                                                <th><?= lang('cost') ?></th>
                                                <th><?= lang('tot') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody id="update" >

                                    </tbody>
                                    </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <!--Creates the popup body-->
        <div class="modal fade" id="modal-topfull" tabindex="-1" role="dialog" aria-hidden="true" >
        <div class="modal-dialog modal-topfull" style="max-width: 500px;margin: auto;">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
            </div>
            <div class="modal-body" >
            <label for=""><?=lang("please_enter_total_cost")?></label>
                <p class="font-nothing f-20"><input type="text" name="cost" class="cost"></p>
                <div  class="c_cost">
                <label for=""><?=lang("please_enter_client_penalty")?></label>
                <p class="font-nothing f-20"><input type="text" name="client_cost" class="client_cost"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-embossed" data-dismiss="modal">Save changes</button>
            </div>
            </div>
        </div>
        </div>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
       
    </body>

<script>
        $( "#type" ).change(function() {
                var type    = $(this).val();
                var is_paid = '';
                var desc    = '';
                var v_id    = '';
                var flag    = false;
                if(type == 1)
                {
                $("#name").html('<?=lang('client')?>');
                $('#v_num').css('display','');
                $('#v_am').css('display','');
                }
                else if(type == 2)
                {
                $("#name").html('<?=lang('product')?>');
                $('#v_num').css('display','none');
                $('#v_am').css('display','none');
                }
                $.ajax({
                url: '<?= base_url('admin/Accounting/fetch_transaction') ?>',
                type: "POST",
                data: {
                    type: type,
                    <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                    },
               dataType: "JSON",
                success: function(data){  
                    $("tbody").children().remove()               
                    var trHTML = '';
                    var n = 1;
                    var v_id = '';
                    var flag = false;
                    
                    $.each(data['trans'], function (i, o){
                        v_id = '';
                        if(o.paid == 1)
                        is_paid = '<?=lang("paid")?>';
                        else
                        is_paid = '<?=lang('unpaid')?>';
                        if(o.description!=null) desc = o.description;
                        if(type == 1)
                        {
                        if(o.voucher_id!=null)
                        {
                            $.ajax({
                                url: '<?= base_url('admin/Accounting/get_voucher_code') ?>',
                                type: "POST",
                                data: {
                                    voucher_id: o.voucher_id,
                                    <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                                    },
                                success: function(data){ 
                                    v_id  =  data;
                                    $('#v_code-'+o.voucher_id).html(v_id);
                                }
                            });
                        }
                        }
                    trHTML += 
                            '<tr><td>'  + n++ +
                            '</td><td>'     + o.n_en +
                            '</td>';
                            $.each(data['fetch'], function (k, n){
                                if(o.id == n.transaction_id)
                                {
                    trHTML +=    '<td>' + n.n_en +
                                    '</td>';
                                    flag = true;
                                }
                            });
                            if(!flag)
                            {
                    trHTML +=    '<td></td>';
                            }
                            else
                            flag = false;
                            var total = +o.cost + +o.voucher_amount;
                    trHTML +=
                            '</td><td>'     + is_paid +
                            '</td><td>'     + desc +
                            '</td>';
                            if(type == 1)
                            {
                    trHTML +=
                            '<td id="v_code-'+o.voucher_id+'">'  + v_id +
                            '</td><td>'     + o.voucher_amount +
                            '</td>';
                            }
                    trHTML +=
                           '<td>'     + o.cost +
                            '</td><td>'     + total +
                        '</td></tr>';
                    });
                $('#update').append(trHTML);
                }
            });
        });
</script>
</html>