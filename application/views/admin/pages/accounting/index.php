<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/css/pages/accounting.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?=$title?></h2>
                                </div>
                                <div class="panel-body">
                               
                                <div class="top">
                                        <div class="budget">
                                        <div class="budget__title">
                                        </div>

                                        <div class="budget__income clearfix">
                                            <div class="budget__income--text">Income</div>
                                            <div class="right">
                                            <div class="budget__income--value">+&nbsp;<?=$income?></div>
                                            <div class="budget__income--percentage">&nbsp;</div>
                                            </div>
                                        </div>

                                        <div class="budget__expenses clearfix">
                                            <div class="budget__expenses--text">Expenses</div>
                                            <div class="right clearfix">
                                            <div class="budget__expenses--value">-&nbsp;<?=$expense?></div>
                                            <div class="budget__expenses--percentage"><?php if($income>0)
                                            {echo round(($expense/$income)*100);} else echo -1;?>%</div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <table class="table">
                                        <thead>
                                        <tr>
                                                <th>#</th>
                                                <th class="income__title" ><?= lang('income') ?></th>
                                                <th></th>
                                                <th class="expenses__title"><?= lang('expense') ?></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1; $n=0; 
                                        if(count($income_dt) >= count($expense_dt))
                                        {   $len=count($expense_dt);
                                            foreach($income_dt as $one){
                                            $accounting =  get_accounting($one->type_id); ?>
                                            <tr>
                                            <td><?=$i++?></td>
                                            <td><?=$accounting->n_en?></td>
                                            <td class="inc_item__value">+&nbsp;<?=$one->cost?></td>
                                            <?php if($n<$len)
                                            {$accounting =  get_accounting($expense_dt[$n]->type_id);?>
                                            <td><?=$accounting->n_en?></td>
                                            <td class="exp_item__value">-&nbsp;<?=$expense_dt[$n]->cost?></td>
                                            <?php $n++; }?>
                                            </tr>
                                            <?php }
                                        }
                                        else if(count($expense_dt) >= count($income_dt))
                                        {   $len=count($income_dt);
                                            foreach($expense_dt as $one){
                                                $accounting =  get_accounting($one->type_id); ?>
                                                <tr>
                                                <td><?=$i++?></td>
                                                <?php if($n<$len)
                                                {$accounting =  get_accounting($income_dt[$n]->type_id);?>
                                                <td><?=$accounting->n_en?></td>
                                                <td class="inc_item__value">+&nbsp;<?=$one->cost?></td>
                                                <?php $n++; } else {?>
                                                    <td></td>
                                                    <td></td>
                                                <?php }?>
                                                <td><?=$accounting->n_en?></td>
                                                <td class="exp_item__value">-&nbsp;<?=$one->cost?></td>
                                                </tr>
                                                <?php }
                                        }?>
                                    </tbody>
                                    </table>
                                    

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <!--Creates the popup body-->
        <div class="modal fade" id="modal-topfull" tabindex="-1" role="dialog" aria-hidden="true" >
        <div class="modal-dialog modal-topfull" style="max-width: 500px;margin: auto;">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
            </div>
            <div class="modal-body" >
            <label for=""><?=lang("please_enter_client_penalty")?></label>
                <p class="font-nothing f-20"><input type="text" name="cost" class="cost"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-embossed" data-dismiss="modal">Save changes</button>
            </div>
            </div>
        </div>
        </div>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
       
    </body>

<script>


</script>
</html>