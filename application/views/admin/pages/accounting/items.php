<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/css/pages/accounting_items.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('serial_number') ?></th>
                                                <th><?= lang('times_rented') ?>(#)</th>
                                                <th><?= lang('total_rented') ?>(LE)</th>
                                                <th><?= lang('item_cost') ?>(LE)</th>
                                                <th><?= lang('warranty_company') ?></th>
                                                <th><?= lang('waranty_ED') ?></th>
                                                <th><?= lang('times_maint') ?>(#)</th>
                                                <th><?= lang('maint_cost') ?>(LE)</th>
                                                <th><?= lang('statistics') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            if(!empty($items)){
                                            foreach($items as $item) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $item['SN'] ?></td>
                                                    <td><?= $item['times_rented'] ?></td>
                                                    <td class="income__title"><?= $item['total_rented'] ?></td>
                                                    <td class="expenses__title"><?= $item['item_cost'] ?></td>
                                                    <td><?= $item['warranty_company'] ?></td>
                                                    <td><?= $item['waranty_ED'] ?></td>
                                                    <td><?= $item['times_maint'] ?></td>
                                                    <td class="expenses__title"><?= $item['maint_cost'] ?></td>
                                                    <td>
                                                    <div class="top">
                                                        <div class="budget">
                                                        <div class="budget__income clearfix">
                                                            <div class="right">
                                                            <div class="budget__income--value">+&nbsp;<?=$item['total_rented']?></div>
                                                            <div class="budget__income--percentage">&nbsp;</div>
                                                            </div>
                                                        </div>
                                                        <div class="budget__expenses clearfix">
                                                            <div class="right clearfix">
                                                            <div class="budget__expenses--value">-&nbsp;<?=($item['maint_cost'] + $item['item_cost'])?></div>
                                                            <div class="budget__expenses--percentage"><?php if($item['total_rented']>0)
                                                            {echo round((($item['maint_cost'] + $item['item_cost'])/$item['total_rented'])*100);} else echo -1;?>%</div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>

                                                    </td>
                                                </tr>
                                                <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>
        </script>
    </body>
</html>