<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= $title ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form method="POST" enctype="Multipart/form-data" action="" class="form-horizontal" role="form">
                                        <?= csrf() ?>
                                        <div id="type" class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('type') ?> :<span class="text-red">*</span></label>
                                            <div class="col-sm-2">
                                                <select name='type' data-search='true' id="sel" class='form-control'>
                                                    <?php
                                                    foreach ($types as $type) {
                                                        ?>
                                                        <option value='<?= $type->type ?>' <?=$type->id==1? 'selected':''?>><?= $type->{'n_' . lc()} ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <span class="text-red"><?php echo form_error('parent'); ?></span>
                                            </div>
                                        </div>
                                        <div id="income" class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('sub_type')?> :</label>
                                            <div class="col-sm-2">
                                                <select name='sub_n' data-search='true' id="sel" class='form-control'>
                                                <option value='0' selected><?= lang('select_sub') ?></option>
                                                    <?php
                                                    foreach ($sub_income as $one) {
                                                        echo $one->id.'hello';?>
                                                        
                                                        <option value='<?= $one->id ?>'><?= $one->{'n_' . lc()} ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <span class="text-red"><?php echo form_error('sub'); ?></span>
                                            </div>
                                        </div>
                                        <div id="expense" class="form-group" style="display:none">
                                            <label class="col-sm-2 control-label"><?= lang('sub_type') ?> :</label>
                                            <div class="col-sm-2">
                                                <select name='sub_e' data-search='true' id="sel" class='form-control'>
                                                    <option value='0' selected><?= lang('select_sub') ?></option>
                                                    <?php
                                                    foreach ($sub_expense as $one) {
                                                        echo $one->id; ?>
                                                        <option value='<?= $one->id ?>'><?= $one->{'n_' . lc()} ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <span class="text-red"><?php echo form_error('sub'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="description"><?= lang('description') ?> : </label>
                                            <div class="col-sm-2">
                                                <?php echo form_textarea(['id' => 'description', 'name' => 'description', 'class' => 'form-control', 'placeholder' => lang('description'), 'rows' => '5', 'cols' => '5']); ?>
                                                <span class="text-red"><?php echo form_error('description'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?= lang('cost') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-2">
                                                <?php echo form_input(['type' => 'number' , 'id' => 'cost', 'name' => 'cost', 'class' => 'form-control', 'value' => set_value('cost'), 'placeholder' => lang('cost')]); ?>
                                                <span class="text-red"><?php echo form_error('cost'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
    </body>
    <script>

        $('#type').change(function(){

            var value = $('#sel').val();
            if(value == 1)
            {
                $('#expense').css('display', 'none');
                $('#income').css('display', 'block');
            }
            else if(value == 2)
            {
                $('#income').css('display', 'none');
                $('#expense').css('display', 'block');
            }
            else
            {
                $('#income').css('display', 'none');
                $('#expense').css('display', 'none');
            }
        });

    </script>
</html>