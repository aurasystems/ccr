<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/magnific/magnific-popup.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/hover-effects/hover-effects.min.css" rel="stylesheet">
    </head>
    <style>
    figure.effect-zoe figcaption{
        height:9em;
        opacity:0.7;
    }
    </style>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= lang("add_product_booking") . ' ' . lang('#') . $id ?></h2>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12 portlets">
                                            <div class="panel panel-transparent">
                                                <div class="panel-content">
                                                    <div class="portfolioFilter text-center m-b-20">
                                                        <a href="#" data-filter="*" class="current">All Categories</a>
                                                        <?php foreach ($categories as $category) { ?>
                                                        <a href="#" data-filter=".c_<?= $category->id ?>"><?= $category->n_en ?></a>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="portfolioContainer grid">
                                                        <?php
                                                        foreach ($categories as $category) {
                                                            $products = get_related_product($category->cat_id);
                                                            foreach($products as $product) {
                                                        ?>
                                                        <figure onclick="add_booking_info(<?= $id ?>, <?= $product->product_id ?>)" class="c_<?= $category->id ?> effect-zoe">
                                                            <img src="<?= base_url() ?>uploads/products/<?= $product->pimg ?>" alt="<?= $product->pimg ?>"/>
                                                            <figcaption >
                                                                <h2 style="highet:200px;"><strong><?= $product->pname ?></strong></h2>
                                                            </figcaption>
                                                        </figure>
                                                        
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/magnific/jquery.magnific-popup.min.js"></script>  <!-- Image Popup -->
        <script src="<?= base_url() ?>assets/plugins/isotope/isotope.pkgd.min.js"></script>  <!-- Filter & sort magical Gallery -->
        <script>
            function add_booking_info(booking_id, product_id){
                window.location = "<?= base_url('admin/Booking/booking_check_product/') ?>" + booking_id + '/' + product_id;
            }
            $(document).ready(function () {
                $(".delete").click(function (e) {
                    e.preventDefault();
                    var id = $(this).attr("data-id");
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url('admin/Booking/delete_booking_item/') ?>" + id + '/' + <?= $id ?>;
                    }
                });
                var $container = $('.portfolioContainer');
                $container.isotope();
                $('.portfolioFilter a').click(function () {
                    $('.portfolioFilter .current').removeClass('current');
                    $(this).addClass('current');
                    var selector = $(this).attr('data-filter');
                    $container.isotope({
                        filter: selector
                    });
                    return false;
                });
            });
        </script>
    </body>
</html>