<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= lang("booking_items").' '. lang('#').$id ?></h2>
                                </div>
                                <div class="panel-body">
                                    <?php if($one->status != 3) { ?>
                                    <div class="text-center">
                                        <a class="btn btn-primary" href="<?= base_url() ?>admin/Booking/booking_add_product/<?= $id ?>"><?= lang("add_product_booking") ?></a>
                                    </div>
                                    <?php } ?>
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                            <th>#</th>
                                                <th><?= lang('name') ?></th>
                                                <th><?= lang('from') ?></th>
                                                <th><?= lang('to') ?></th>
                                                <th><?= lang('cost_price') ?></th>
                                                <th><?= lang('quantity') ?></th>
                                                <th><?= lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach($items as $item) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $item->pname ?></td>
                                                    <td>
                                                        <input class="form-control datetimepicker" id='b_from_<?= $item->id ?>' disabled="" value='<?= $item->b_from ?>'>
                                                    </td>
                                                    <td>
                                                        <input class="form-control datetimepicker" id='b_to_<?= $item->id ?>' disabled="" value='<?= $item->b_to ?>'>
                                                    </td>
                                                    <td><?= $item->rent_price ?></td>
                                                    <td>
                                                        <input type="number" min="1" class="form-control" id='b_qty_<?= $item->id ?>' disabled="" value='<?= $item->quantity ?>'>
                                                    </td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <?php if($item->b_from > $now && $one->status != 3) { ?>
                                                            <a title="<?= lang("stock_check") ?>" href="javascript:void(0);" data-id="<?= $item->id ?>" class="btn btn-sm btn-primary" onclick="stock_check(<?= $id ?>, <?= $item->product_id ?>)"><i class="fa fa-check-circle"></i></a>
                                                            <?php } ?>
                                                            <?php if (get_p("booking", "u")) { ?>
                                                            <a href="javascript:void(0);" class="btn btn-dark btn-sm" id='edit_btn_<?= $item->id ?>' onclick="edit_booking_date_to(<?= $item->id ?>)">
                                                                <i class="fa fa-edit"></i>
                                                            </a>
                                                            <?php } ?>
                                                            <a href="javascript:void(0);" class="btn btn-dark btn-sm" id='update_btn_<?= $item->id ?>' style="display: none" onclick="update_booking_date_to(<?= $item->id ?>)">
                                                                <i class="fa fa-floppy-o"></i>
                                                            </a>
                                                            <?php if (get_p("booking", "d")) { ?>
                                                            <a title="<?= lang("delete") ?>" href="#" data-id="<?= $item->id ?>" class="delete btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <div class="modal bd-example-modal-lg fade" id="result_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?= lang('ava_items_in_stock') ?></h4>
                    </div>
                    <div class="modal-body">
                        <h3 class="items_count text-center"></h3>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>
            $(document).ready(function () {
                $(".delete").click(function (e) {
                    e.preventDefault();
                    var id = $(this).attr("data-id");
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url('admin/Booking/delete_booking_item/') ?>" + id + '/' + <?= $id ?>;
                    }
                });
            });
            function edit_booking_date_to($id) {
                $("#b_from_" + $id).removeAttr('disabled');
                $("#b_to_" + $id).removeAttr('disabled');
                $("#b_qty_" + $id).removeAttr('disabled');
                $("#edit_btn_" + $id).slideUp();
                $("#update_btn_" + $id).slideDown();
            }
            function update_booking_date_to($id) {
                var b_from = $("#b_from_" + $id).val();
                var b_to = $("#b_to_" + $id).val();
                var b_qty = $("#b_qty_" + $id).val();
                if ((b_from == '' && b_to == '') || (b_from == '' && b_to != '') || (b_from != '' && b_to == '') || (b_qty <= 0) || (b_qty == '')) {
                    $("#check_msg").html('<?= $this->lang->line('check_booking_dates_qty') ?>');
                    $("#check_alert").slideDown();
                } else if (b_from >= b_to) {
                    $("#check_msg").html('<?= $this->lang->line('check_dates') ?>');
                    $("#check_alert").slideDown();
                }
                else {
                    $("#check_alert").slideUp();
                    $('.loader-overlay').removeClass('loaded');
                    $.ajax({
                        url: '<?= base_url() ?>admin/Booking/update_booking_date_to',
                        type: "POST",
                        data: {
                            id: $id,
                            b_from: b_from,
                            b_to: b_to,
                            b_qty: b_qty,
                            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        dataType: "JSON",
                        success: function (result) {
                            location.reload();
                        }
                    });
                }
            }
            function stock_check(booking_id, product_id) {
                $(".items_count").html('');
                if (booking_id != '' && product_id != '') {
                    $('.loader-overlay').removeClass('loaded');
                    $.ajax({
                        url: '<?= base_url() ?>admin/Booking/stock_check',
                        type: "POST",
                        dataType: "JSON",
                        data: {
                            booking_id: booking_id,
                            product_id: product_id,
                            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        success: function (result) {
                            $(".items_count").append(result);
                            $('#result_modal').modal('show');
                            $('.loader-overlay').addClass('loaded');
                        }
                    });
                }
            }
        </script>
    </body>
</html>