<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">   
                            <div id="check_alert" class="preview active" style="display: none;">
                                <div class="alert alert-block alert-warning fade in text-center" id="check_msg">
                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('username') ?></th>
                                                <th><?= lang('location') ?></th>
                                                <th><?= lang('cash_limit') ?></th>
                                                <th><?= lang('credit_limit') ?></th>
                                                <th><?= lang('total_price') ?></th>
                                                <th><?= lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($pending as $one) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $one->cname ?></td>
                                                    <td><?= $one->lname ?></td>
                                                    <td><?= $one->cash_limit ?></td>
                                                    <td><?= $one->credit_limit ?></td>
                                                    <td><?= $one->total ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a title="<?= lang("view_items") ?>" href="<?= base_url() ?>admin/Booking/booking_items/<?= $one->id ?>" class="btn btn-sm btn-primary"><i class="fa fa-list-ul"></i></a>
                                                            <?php if (get_p("booking", "e")) { ?>
                                                            <a id="approve" title="<?= lang("approve") ?>" href="#" data-id="<?= $one->id ?>" class="btn btn-sm btn-success"><i class="fa fa-check"></i></a>
                                                            <a id="decline" data-toggle="modal" data-target="#decline_modal" href="#" data-id="<?= $one->id ?>" class="btn btn-sm btn-danger" title="<?= lang("decline") ?>"><i class="fa fa-close"></i></a>
                                                            <?php } ?>
                                                            <?php if (get_p("booking", "d")) { ?>
                                                            <a onclick="delete_booking(<?= $one->id ?>);" href="javascript:void(0);" data-id="<?= $one->id ?>" class="delete btn btn-sm btn-danger"title="delete client"><i class="fa fa-trash" ></i></a>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <div class="modal bd-example-modal-lg fade" id="decline_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?= $this->lang->line('add_decline_reason') ?></h4>
                    </div>
                    <form method="POST" action="<?= base_url() ?>admin/Booking/decline_order" class="form-horizontal" role="form">
                        <?= csrf() ?>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="n_en"><?= lang('decline_reasons') ?> : <span class="text-red">*</span></label>
                                <div class="col-sm-7">
                                    <select name='reason' id='reason' data-search='true' class='form-control'>
                                        <option value=''><?= lang('select_d_r') ?></option>
                                        <?php
                                        foreach ($decline_reasons as $reason) {
                                            ?>
                                            <option value='<?= $reason->id ?>'><?= $reason->{'n_' . lc()} ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <span class="text-red"><?php echo form_error('reason'); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="book_id" id="book_id" />
                            <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>
            function delete_booking(id) {
                if (id != '') {
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url('admin/Booking/delete_booking/') ?>" + id + '/0';
                    }
                }
            }
            $(document).ready(function () {
//                $(".delete").click(function (e) {
//                    e.preventDefault();
//                    var id = $(this).attr("data-id");
//                    var conf = confirm("<?= lang("r_u_sure") ?>");
//                    if (conf) {
//                        window.location = "<?= base_url('admin/Booking/delete_booking/') ?>" + id + '/0';
//                    }
//                });
                $("#decline").click(function (e) {
                    e.preventDefault();
                    var id = $(this).attr("data-id");
                    $("#book_id").val(id);
                });
                $("#approve").click(function (e) {
                    e.preventDefault();
                    var id = $(this).attr("data-id");
                    var conf = confirm("<?= lang("approve_this") ?>");
                    if (conf) {
                        window.location = "<?= base_url('admin/Booking/approve_order/') ?>" + id;
                    }
                });
            });
            function edit_booking_date_to($id) {
                $("#b_from_" + $id).removeAttr('disabled');
                $("#b_to_" + $id).removeAttr('disabled');
                $("#edit_btn_" + $id).slideUp();
                $("#update_btn_" + $id).slideDown();
            }
            function update_booking_date_to($id) {
                var b_from = $("#b_from_" + $id).val();
                var b_to = $("#b_to_" + $id).val();
                if ((b_from == '' && b_to == '') || (b_from == '' && b_to != '') || (b_from != '' && b_to == '')) {
                    $("#check_msg").html('<?= $this->lang->line('specify_b_date_to') ?>');
                    $("#check_alert").slideDown();
                } else if (b_from >= b_to) {
                    $("#check_msg").html('<?= $this->lang->line('check_dates') ?>');
                    $("#check_alert").slideDown();
                }
                else {
                    $("#check_alert").slideUp();
                    $('.loader-overlay').removeClass('loaded');
                    $.ajax({
                        url: '<?= base_url() ?>admin/Booking/update_booking_date_to',
                        type: "POST",
                        data: {
                            id: $id,
                            b_from: b_from,
                            b_to: b_to,
                            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        dataType: "JSON",
                        success: function (result) {
                            if(result == false) {
                                $("#check_msg").html('<?= $this->lang->line('error_occured') ?>');
                                $("#check_alert").slideDown();
                                $('.loader-overlay').addClass('loaded');
                            }
                            else {
                                location.reload();
                            }
                        }
                    });
                }
            }
        </script>
    </body>
</html>