<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= lang("add_product_booking") . ' ' . lang('#') . $booking_id ?></h2>
                                </div>
                                <div class="panel-body bg-white">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <form method="POST" enctype="Multipart/form-data" action="<?= base_url() ?>admin/Booking/check_product_availability/<?= $booking_id ?>/<?= $product_id ?>" class="form-horizontal" role="form">
                                                <?= csrf() ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"><?= lang('quantity') ?> : <span class="text-red">*</span></label>
                                                    <div class="col-sm-2">
                                                        <?php echo form_input(['type' => 'number', 'min' => 1, 'id' => 'quantity', 'name' => 'quantity', 'class' => 'form-control', 'value' => 1, 'placeholder' => lang('quantity'), 'required' => 'required']); ?>
                                                        <span class="text-red"><?php echo form_error('quantity'); ?></span>
                                                    </div>
                                                </div>
                                                <?php if (!empty($product) && $product->rent_term == 1) { // day ?>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label"><?= lang('b_from') ?> : <span class="text-red">*</span></label>
                                                        <div class="col-sm-2">
                                                            <?php echo form_input(['id' => 'b_from','autocomplete' => 'off', 'name' => 'b_from', 'class' => 'form-control datetimepicker datepicker', 'value' => date('Y-m-d'), 'placeholder' => lang('b_from'), 'required' => 'required']); ?>
                                                            <span class="text-red"><?php echo form_error('b_from'); ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label"><?= lang('b_to') ?> : <span class="text-red">*</span></label>
                                                        <div class="col-sm-2">
                                                            <?php echo form_input(['id' => 'b_to','autocomplete' => 'off', 'name' => 'b_to', 'class' => 'form-control datetimepicker datepicker', 'value' => date('Y-m-d'), 'placeholder' => lang('b_to'), 'required' => 'required']); ?>
                                                            <span class="text-red"><?php echo form_error('b_to'); ?></span>
                                                        </div>
                                                    </div>
                                                <?php } elseif (!empty($product) && $product->rent_term == 2) { // hour ?>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label"><?= lang('date') ?> : <span class="text-red">*</span></label>
                                                        <div class="col-sm-2">
                                                            <?php echo form_input(['id' => 'date', 'name' => 'date', 'class' => 'form-control date-picker', 'value' => set_value('date'), 'placeholder' => lang('date'), 'required' => 'required']); ?>
                                                            <span class="text-red"><?php echo form_error('date'); ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label"><?= lang('b_from') ?> : <span class="text-red">*</span></label>
                                                        <div class="col-sm-2">
                                                            <?php echo form_input(['id' => 'start', 'name' => 'b_time_from', 'class' => 'form-control', 'value' => set_value('b_from'), 'placeholder' => lang('b_from'), 'required' => 'required']); ?>
                                                            <span class="text-red"><?php echo form_error('b_time_from'); ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label"><?= lang('b_to') ?> : <span class="text-red">*</span></label>
                                                        <div class="col-sm-2">
                                                            <?php echo form_input(['id' => 'end', 'name' => 'b_time_to', 'class' => 'form-control', 'value' => set_value('b_to'), 'placeholder' => lang('b_to'), 'required' => 'required']); ?>
                                                            <span class="text-red"><?php echo form_error('b_time_to'); ?></span>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="col-sm-12 text-center">
                                                    <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>
            $(document).ready(function () {
                $('#start').timepicker({
                    isRTL: $('body').hasClass('rtl') ? true : false,
                    //timeFormat: $(this).attr('data-format', 'am-pm') ? 'hh:mm tt' : 'HH:mm'
                });
                $('#end').timepicker({
                    isRTL: $('body').hasClass('rtl') ? true : false,
                    //timeFormat: $(this).attr('data-format', 'am-pm') ? 'hh:mm tt' : 'HH:mm'
                });
            });

            
$("#b_from").datepicker({
    onSelect: function() {
        var minDate = $('#b_from').datepicker('getDate');
        $("#b_to").datepicker("change", { minDate: minDate });
    }
});

$("#b_to").datepicker({
    onSelect: function() {
        var maxDate = $('#b_to').datepicker('getDate');
        $("#b_from").datepicker("change", { maxDate: maxDate });
    }
});
          
        </script>
    </body>
</html>