<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= lang("blog") ?></h2>
                                </div>
                                <div class="panel-body">
                                    <?php if (get_p("blog", "c")) { ?>
                                        <div class="text-center">
                                            <a class="btn btn-primary" href="<?= base_url() ?>admin/Blog/add"><?= lang("add_blog") ?></a>
                                        </div>
                                    <?php } ?>
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('category') ?></th>
                                                <th><?= lang('title') ?></th>
                                                <th><?= lang('created_by') ?></th>
                                                <th><?= lang('datetime') ?></th>
                                                <th><?= lang('published') ?></th>
                                                <th><?= lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($blog as $one) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $one->category ?></td>
                                                    <td><?= $one->title ?></td>
                                                    <td><?= $one->uname ?></td>
                                                    <td><?= date('Y-m-d h:i a', strtotime($one->create_time)) ?></td>
                                                    <td><?= $one->status == 1 ? '<i class="fa fa-check"></i>' : '<i class="fa fa-close"></i>' ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a title="<?= lang("show_comments") ?>" href="<?= base_url() ?>admin/Blog/show_comments/<?= $one->id ?>" class="btn btn-sm btn-primary"><i class="fa fa-list-ul"></i></a>
                                                            <?php
                                                            if (get_p("blog", "e")) {
                                                                if ($one->status == 0) {
                                                                    ?>
                                                                    <a onclick="publish_blog(<?= $one->id ?>);" href="javascript:void(0);" title="<?= lang("publish") ?>" href="#" data-id="<?= $one->id ?>" class="btn btn-sm btn-success"><i class="fa fa-check"></i></a>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <a onclick="unpublish_blog(<?= $one->id ?>);" href="javascript:void(0);" title="<?= lang("unpublish") ?>" href="#" data-id="<?= $one->id ?>" class="btn btn-sm btn-warning"><i class="fa fa-close"></i></a>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                            <?php if (get_p("blog", "u")) { ?>
                                                            <a href="<?= base_url() ?>admin/Blog/edit/<?= $one->id ?>" class="btn btn-sm btn-warning" title="<?= lang("edit") ?>"><i class="fa fa-edit"></i></a>
                                                            <?php } ?>    
                                                            <?php if (get_p("blog", "d")) { ?>
                                                                <a onclick="delete_blog(<?= $one->id ?>);" href="javascript:void(0);" data-id="<?= $one->id ?>" class="delete btn btn-sm btn-danger"title="<?= lang("delete") ?>"><i class="fa fa-trash" ></i></a>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
            function publish_blog(id) {
                if (id != '') {
                    var conf = confirm("<?= lang("publish_this") ?>");
                    if (conf) {
                        window.location = "<?= base_url('admin/Blog/publish_blog/') ?>" + id;
                    }
                }
            }
            function unpublish_blog(id) {
                if (id != '') {
                    var conf = confirm("<?= lang("unpublish_this") ?>");
                    if (conf) {
                        window.location = "<?= base_url('admin/Blog/unpublish_blog/') ?>" + id;
                    }
                }
            }
            function delete_blog(id) {
                if (id != '') {
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url() ?>admin/Blog/delete_blog/" + id;
                    }
                }
            }
            $(document).ready(function () {
                
            });
        </script>
    </body>
</html>