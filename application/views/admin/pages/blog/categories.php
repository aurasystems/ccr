<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= lang("blog_categories") ?></h2>
                                </div>
                                <div class="panel-body">
                                    <?php if (get_p("blog_categories", "c")) { ?>
                                        <div class="text-center">
                                            <a onclick="add_blog_cat()" id="decline" data-toggle="modal" data-target="#category_modal" href="javascript:void(0);" data-id="" class="btn btn-primary" title="<?= lang("add_blog_category") ?>"><?= lang("add_blog_category") ?></a>
                                        </div>
                                    <?php } ?>
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= lang('name_en') ?></th>
                                                <th><?= lang('name_ar') ?></th>
                                                <th><?= lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($categories as $one) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td>
                                                        <input class="form-control" id='n_en_<?= $one->id ?>' disabled="" value='<?= $one->n_en ?>'>
                                                    </td>
                                                    <td>
                                                        <input class="form-control" id='n_ar_<?= $one->id ?>' disabled="" value='<?= $one->n_ar ?>'>
                                                    </td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <?php if (get_p("blog_categories", "u")) { ?>
                                                            <a href="javascript:void(0);" class="btn btn-dark btn-sm" id='edit_btn_<?= $one->id ?>' onclick="edit_category(<?= $one->id ?>)">
                                                                <i class="fa fa-edit"></i>
                                                            </a>
                                                            <?php } ?>
                                                            <a href="javascript:void(0);" class="btn btn-dark btn-sm" id='update_btn_<?= $one->id ?>' style="display: none" onclick="update_category(<?= $one->id ?>)">
                                                                <i class="fa fa-floppy-o"></i>
                                                            </a>
                                                            <a onclick="delete_category(<?= $one->id ?>);" href="javascript:void(0);" data-id="<?= $one->id ?>" class="delete btn btn-sm btn-danger"title="delete client"><i class="fa fa-trash" ></i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <!-- Category modal -->
        <div class="modal bd-example-modal-lg fade" id="category_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?= $this->lang->line('add_blog_category') ?></h4>
                    </div>
                    <form method="POST" action="<?= base_url() ?>admin/Blog/add_category" class="form-horizontal" role="form">
                        <?= csrf() ?>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="n_en"><?= lang('name_en') ?> : <span class="text-red">*</span></label>
                                <div class="col-sm-7">
                                    <?php echo form_input(['id' => 'n_en', 'name' => 'n_en', 'class' => 'form-control', 'placeholder' => lang('name_en'), 'required' => 'required']); ?>
                                    <span class="text-red"><?php echo form_error('n_en'); ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="n_ar"><?= lang('name_ar') ?> : <span class="text-red">*</span></label>
                                <div class="col-sm-7">
                                    <?php echo form_input(['id' => 'n_ar', 'name' => 'n_ar', 'class' => 'form-control', 'placeholder' => lang('name_ar'), 'required' => 'required']); ?>
                                    <span class="text-red"><?php echo form_error('n_ar'); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="return_id" id="return_id" />
                            <input type="hidden" name="product_id" id="product_id" />
                            <input type="hidden" name="item_id" id="item_id" />
                            <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!-- /.Category modal -->
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
            function delete_category(id) {
                if (id != '') {
                    var conf = confirm("<?= lang("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url() ?>admin/Blog/delete_category/" + id;
                    }
                }
            }
            function add_blog_cat(){
                $("#category_modal").show();
            }
            function edit_category($id) {
                $("#n_en_" + $id).removeAttr('disabled');
                $("#n_ar_" + $id).removeAttr('disabled');
                $("#edit_btn_" + $id).slideUp();
                $("#update_btn_" + $id).slideDown();
            }
            function update_category($id) { // cont here............
                var n_en = $("#n_en_" + $id).val();
                var n_ar = $("#n_ar_" + $id).val();
                if ((n_en == '' || n_ar == '')) {
                    $("#check_msg").html('<?= $this->lang->line('fields_required') ?>');
                    $("#check_alert").slideDown();
                }
                else {
                    $("#check_alert").slideUp();
                    $('.loader-overlay').removeClass('loaded');
                    $.ajax({
                        url: '<?= base_url() ?>admin/Blog/update_category',
                        type: "POST",
                        data: {
                            id: $id,
                            n_en: n_en,
                            n_ar: n_ar,
                            <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        dataType: "JSON",
                        success: function (result) {
                            location.reload();
                        }
                    });
                }
            }
            $(document).ready(function () {
//                $(".delete").click(function (e) {
//                    e.preventDefault();
//                    var id = $(this).attr("data-id");
//                    var conf = confirm("<?= lang("r_u_sure") ?>");
//                    if (conf) {
//                        window.location = "<?= base_url('admin/Technicians/delete_technician/') ?>" + id;
//                    }
//                });
            });
        </script>
    </body>
</html>