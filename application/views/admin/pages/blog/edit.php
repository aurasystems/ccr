<html lang="<?= lang('system_lang') ?>" dir="<?= lang('system_dir') ?>">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="<?= b_style() ?>">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= $title ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form method="POST" action="" class="form-horizontal" role="form" enctype="multipart/form-data">
                                        <?= csrf() ?>
                                        <?php if(!empty($blog->img)) { ?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('img') ?> : </label>
                                            <div class="col-sm-3">
                                                <img class="img-md img-thumbnail" src="<?= base_url() ?>uploads/blogs/<?= $blog->img ?>">
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('img') ?> : </label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(array('type' => 'file', 'id' => 'userfile', 'name' => 'userfile', 'class' => 'form-control', 'size' => 20)); ?>
                                                <span class="text-red"><?php echo form_error('img'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('category') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <select name='category' id='category' data-search='true' class='form-control'>
                                                    <option value=''><?= lang('specify_category') ?></option>
                                                    <?php foreach ($categories as $one) { ?>
                                                    <option value='<?= $one->id ?>' <?= ($one->id == $blog->category_id) ? 'selected' : '' ?>><?= $one->{'n_'. lc()} ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="text-red"><?php echo form_error('category'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="title"><?= lang('title') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['id' => 'title', 'name' => 'title', 'value' => (!empty($blog->title)) ? $blog->title : '' , 'class' => 'form-control', 'placeholder' => lang('title'), 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('title'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="content"><?= lang('content') ?> : <span class="text-red">*</span></label>
                                            <div class="col-sm-6">
                                                <?php echo form_textarea(['id' => 'content', 'name' => 'content', 'value' => (!empty($blog->content)) ? $blog->content : '', 'class' => 'form-control', 'placeholder' => lang('content'), 'rows' => '5', 'cols' => '15', 'required' => 'required']); ?>
                                                <span class="text-red"><?php echo form_error('content'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="title"><?= lang('tags') ?> : </label>
                                            <div class="col-sm-3">
                                                <?php echo form_input(['id' => 'tags', 'name' => 'tags', 'class' => 'select-tags form-control', 'value' => (!empty($tags)) ? $tags : '']); ?>
                                                <span class="text-red"><?php echo form_error('tags'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <input type="submit" class="btn btn-primary" value="<?= lang("save") ?>">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script>
            $(document).ready(function () {
                
            });
        </script>
    </body>
</html>