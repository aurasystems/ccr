<?php

function construct_init($no_log = NULL) {
    $CI = &get_instance();
    if (!$no_log) {
        if (!$CI->session->userdata("user_id")) {
            redirect("admin/Login");
        }
    }
    if (!empty($CI->session->userdata('language'))) {
        $CI->lang->load('default', $CI->session->userdata('language') == 'english' ? 'default' : $CI->session->userdata('language'));
    } else {
        $CI->lang->load('default');
    }
}

function construct_init_front($no_log = NULL) {
    $CI = &get_instance();
    if (!$no_log) {
        if (!$CI->session->userdata("client_id")) {
            redirect("Login");
        }
    }
    if (!empty($CI->session->userdata('language'))) {
        $CI->lang->load('default', $CI->session->userdata('language') == 'english' ? 'default' : $CI->session->userdata('language'));
    } else {
        $CI->lang->load('default');
    }
}

function b_style() {
    return 'fixed-topbar fixed-sidebar theme-sdtl color-default ' . lang('system_dir');
}

function settings($key = NULL, $type = 1) {
    $CI = &get_instance();
    switch ($type) {
        case 1:
            $settings = $CI->session->userdata('email_settings');
            if (!$settings) {
                $CI->load->model('admin/Email_settings_model');
                $settings = $CI->Email_settings_model->get_email_settings();
                if ($settings) {
                    $CI->session->set_userdata("email_settings", $settings);
                }
            }
            break;
        case 2:
            $settings = $CI->session->userdata('sms_settings');
            if (!$settings) {
                $CI->load->model('admin/Sms_settings_model');
                $settings = $CI->Sms_settings_model->get_sms_settings();
                if ($settings) {
                    $CI->session->set_userdata("sms_settings", $settings);
                }
            }
            break;
        case 3:
            $settings = $CI->session->userdata('settings');
            if (!$settings) {
                $CI->load->model('admin/Settings_model');
                $settings = $CI->Settings_model->get_settings();
                if ($settings) {
                    $CI->session->set_userdata("settings", $settings);
                }
            }
            break;
        case 4:
            $settings = $CI->session->userdata('newsletters_settings');
            if (!$settings) {
                $CI->load->model('admin/Newsletters_settings_model');
                $settings = $CI->Newsletters_settings_model->get_newsletters_settings();
                if ($settings) {
                    $CI->session->set_userdata("newsletters_settings", $settings);
                }
            }
            break;
    }
    $val = isset($settings->$key) ? $settings->$key : ($settings && $key == 'all' ? $settings : '');
    return $val;
}

function get_sys_idf($type = 0) {
    $m = [1 => 'users', 2 => 'access_levels', 3 => 'clients', 4 => 'email_settings', 5 => 'settings', 6 => 'locations', 7 => 'brands', 8 => 'categories', 9 => 'studios', 10 => 'technicians_types'];
    return !empty($m[$type]) ? $m[$type] : FALSE;
}

function get_data_list($dt = []) {
    $access = [];
    $c = isset($dt['c']) && $dt['c'] ? $dt['c'] : 0;
    $m = ucfirst(get_sys_idf($c));
    if ($c && $m) {
        $CI = &get_instance();
        $CI->load->model('admin/' . $m . '_model', $m);
        $access = $CI->{$m}->get_data($dt);
    }
    return $access;
}

function get_single_date($dt = []) {
    $data = NULL;
    $c = isset($dt['c']) && $dt['c'] ? $dt['c'] : 0;
    $id = isset($dt['i']) && $dt['i'] ? $dt['i'] : 0;
    $chk = isset($dt['chk']) && $dt['chk'] ? TRUE : FALSE;
    $m = ucfirst(get_sys_idf($c));
    if ($id && $c && $m) {
        $CI = &get_instance();
        $CI->load->model('admin/' . $m . '_model', $m);
        $data = $CI->{$m}->get_info($id);
    }
    if ($chk && !$data) {
        redirect(base_url("admin/General/error/resource"));
    }
    return $data;
}

function lc() {
    $CI = &get_instance();
    return $CI->lang->line('lang_code');
}

function uid() {
    $CI = &get_instance();
    return $CI->session->userdata("user_id");
}

function lang($keyword = '') {
    return get_instance()->lang->line($keyword);
}

function to_date($date, $custom = NULL) {
    $format = "Y-m-d H:i:s";
    if ($custom) {
        $format = $custom;
    }
    return date($format, strtotime($date));
}

function selected($value, $compare) {
    if ($value == $compare) {
        return "selected";
    }
    return "";
}

function valid_mobile($mobile) {
    if (strlen($mobile) == 11 && $mobile[0] === "0") {
        return TRUE;
    }
    return FALSE;
}

function valid_date($date) {
    if (!strtotime($date)) {
        return FALSE;
    }
    return True;
}

function form_group($field, $edit = NULL, $date = "", $label = "", $required = "", $type = "text", $arrtibutes = '') {
    $CI = &get_instance();
    $value = $CI->input->post($field);
    if ($edit) {
        $value = $edit;
    }
    $form_group = " <div class='form-group'>
                        <label class='col-sm-4 control-label'>" . ($label ? $label : $CI->lang->line($field)) . ($required ? ' <span class="text-red">*</span>' : '') . "</label>
                        <div class='col-sm-8'>
                            <input type='$type' name='$field' id='$field' class='form-control $date' placeholder='" . ($label ? $label : $CI->lang->line($field)) . "' value='" . $value . "' $required $arrtibutes>
                            <span class='c-red'>" . form_error($field) . "</span>
                        </div>
                    </div>";
    return $form_group;
}

function form_group_file($field, $edit = NULL, $label = "", $required = "", $arrtibutes = '') {
    $CI = &get_instance();
    $value = $CI->input->post($field);
    if ($edit) {
        $value = $edit;
    }
    $form_group = " <div class='form-group'>
                        <label class='col-sm-4 control-label'>" . ($label ? $label : $CI->lang->line($field)) . "</label>
                        <div class='col-sm-8'>
                            <input type='file' name='$field' id='$field' class='form-control' placeholder='" . ($label ? $label : $CI->lang->line($field)) . "' value='" . $value . "' $required $arrtibutes>
                            <span class='c-red'>" . form_error($field) . "</span>
                        </div>
                    </div>";
    return $form_group;
}

function form_group_select($field, $options, $option_name = NULL, $edit = NULL, $mutiple = "", $label = "", $required = "", $arrtibutes = '') {
    $CI = &get_instance();
    if ($option_name == NULL) {
        $option_name = "name";
    }
    $value = $CI->input->post($field);
    if ($edit) {
        $value = $edit;
    }
    $form_group = "<div class='form-group'>
                    <label class='col-sm-4 control-label'>" . ($label ? $label : $CI->lang->line($field)) . ($required ? ' <span class="text-red">*</span>' : '') . "</label>
                    <div class='col-sm-8'>
                        <select $mutiple name='$field' id='$field' data-search='true' class='form-control' $required $arrtibutes>";
    foreach ($options as $one) {
        $form_group .= "<option " . selected($value, is_object($one) ? $one->id : (is_array($one) ? $one['id'] : '')) .
                " value='" . (is_object($one) ? $one->id : (is_array($one) ? $one['id'] : '')) . "'>" . (is_object($one) ? $one->{$option_name} : (is_array($one) ? $one[$option_name] : '')) . "</option>";
    }
    $form_group .= "</select>
                        <span class='c-red'>" . form_error($field) . "</span>
                    </div>
                </div>";
    return $form_group;
}

function form_group_text($field, $edit = NULL, $label = "", $required = "", $arrtibutes = '') {
    $CI = &get_instance();
    $value = $CI->input->post($field);
    if ($edit) {
        $value = $edit;
    }
    $form_group = " <div class='form-group'>
                        <label class='col-sm-4 control-label'>" . ($label ? $label : $CI->lang->line($field)) . ($required ? ' <span class="text-red">*</span>' : '') . "</label>
                        <div class='col-sm-8'>
                        <textarea rows='5' name='$field' id='$field' class='form-control' placeholder='" . ($label ? $label : $CI->lang->line($field)) . "' $required $arrtibutes>$value</textarea>
                            <span class='c-red'>" . form_error($field) . "</span>
                        </div>
                    </div>";
    return $form_group;
}

function disabled_input($name = '', $value = 5) {
    if (!$value)
        $value = 5;
    $input = '<input type="hidden" name="' . $name . '" value="' . $value . '"/>';
    return $input;
}

function form_inputs($data = [], $pfx = '', $psfx = '', $item = NULL, $form_edit = FALSE, $disabled = FALSE, $readonly = FALSE) {
    $inputs = '';
    foreach ($data as $dt) {
        if (!empty($dt)) {
            $type = !empty($dt['t']) ? $dt['t'] : 1;
            $field = !empty($dt['f']) ? $dt['f'] : NULL;
            $edit = !empty($dt['e']) ? $dt['e'] : NULL;
            $date = !empty($dt['d']) ? 'date-picker' : '';
            $label = !empty($dt['l']) ? $dt['l'] : '';
            $required = !empty($dt['r']) ? $dt['r'] : '';
            $arrtibutes = !empty($dt['a']) ? $dt['a'] : '';
            $itype = !empty($dt['it']) ? $dt['it'] : 'text';
            $mutiple = !empty($dt['mp']) ? $dt['mp'] : '';
            $options = !empty($dt['op']) ? $dt['op'] : [];
            $option_name = !empty($dt['opn']) ? $dt['opn'] : NULL;

            if (!$edit && $item && $form_edit && $field) {
                if (is_object($item) && !empty($item->{$field})) {
                    $edit = $item->{$field};
                } else if (is_array($item) && !empty($item[$field])) {
                    $edit = $item[$field];
                }
            }

            if ($disabled)
                $arrtibutes .= ' disabled';
            if ($readonly)
                $arrtibutes .= 'readonly';

            $custom = !empty($dt['c']) ? $dt['c'] : '';
            $ipfx = !empty($dt['pfx']) ? $dt['pfx'] : $pfx;
            $ipsfx = !empty($dt['psfx']) ? $dt['psfx'] : $psfx;

            $inputs .= $ipfx;
            switch ($type) {
                case 1:
                    $inputs .= form_group($field, $edit, $date, $label, $required, $itype, $arrtibutes);
                    break;
                case 2:
                    $inputs .= form_group_file($field, $edit, $label, $required, $arrtibutes);
                    break;
                case 3:
                    $inputs .= form_group_select($field, $options, $option_name, $edit, $mutiple, $label, $required, $arrtibutes);
                    break;
                case 4:
                    $inputs .= form_group_text($field, $edit, $label, $required, $arrtibutes);
                    break;
                case 5:
                    $inputs .= disabled_input($field, $edit);
                    break;
                case 6:
                    $inputs .= $custom;
                    break;
            }
            $inputs .= $ipsfx;
        }
    }
    return $inputs;
}

function get_placeholder($value = '', $key = '', $title = '') {
    return [['id' => $value, $key . '_' . lc() => $title]];
}

function flash_success() {
    $CI = &get_instance();
    $flash = "";
    if ($CI->session->flashdata("success")) {
        $flash .= "<div class='row'>
                <div class='col-md-12'>
                    <div class='alert alert-block alert-success fade in'>
                        
                        <p></p><h4><i class='fa fa-check'></i> " . $CI->session->flashdata("success") . "</h4><p></p>
                    </div>
                </div>
            </div>
    ";
    }
    return $flash;
}

function flash_error() {
    $CI = &get_instance();
    $flash = "";
    if ($CI->session->flashdata("error")) {
        $flash .= "<div class='row'>
                <div class='col-md-12'>
                    <div class='alert alert-block alert-danger fade in'>
                        <p></p><h4><i class='fa fa-times'></i> " . $CI->session->flashdata("error") . "</h4><p></p>
                    </div>
                </div>
            </div>
    ";
    }
    return $flash;
}

function handle_file($field, $custom_config = NULL) {
//    $data=array(
//        "field"=>"the name of the file field",
//        "custom_config"=>"array of custom config",
//    );
//    $config['upload_path'] = './uploads/settings/back';
//    $config['allowed_types'] = 'gif|jpg|png|jpeg';
//    $config['max_size'] = '10000';
//    $config['max_width'] = '5000';
//    $config['max_height'] = '5000';
    $CI = &get_instance();
    $response['status'] = "no_file";
    if ($_FILES[$field]['name'] != "") {
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = "*";
        if ($custom_config) {
            $config = $custom_config;
        }
        $CI->load->library("upload", $config);
        if ($CI->upload->do_upload($field)) {
            $file_data = $CI->upload->data();
            $response['status'] = "success";
            $response['file_name'] = $file_data['file_name'];
        } else {
            $response['status'] = "fail";
            $response['error'] = $CI->upload->display_errors();
            $CI->session->set_flashdata("error", $response['error']);
            redirect(current_url());
        }
    }
    return $response;
}

function ids($arr = [], $id = 'id') {
    $n_arr = [];
    foreach ($arr as $v) {
        if (is_object($v) && isset($v->{$id})) {
            $n_arr[] = $v->{$id};
        } else {
            if (isset($v[$id])) {
                $n_arr[] = $v[$id];
            }
        }
    }
    return $n_arr;
}

function pd($value) {
    echo "<pre>";
    print_r($value);
    echo "</pre>";
    die;
}

function p($value) {
    echo "<pre>";
    print_r($value);
    echo "</pre>";
}

function checked($value = NULL, $compare = 1) {
    if ($value == $compare) {
        return "checked";
    }
    return "";
}

function in_content($key, $array) {
    if (!empty($array[$key])) {
        return $array[$key];
    }
    return "";
}

function sidebar_active($module) {
    $CI = &get_instance();
    $active = $CI->session->userdata("sidebar_active") == $module ? 'active' : '';
    return $active;
}

function sidebar_parent($module) {
    $CI = &get_instance();
    if (in_array($CI->session->userdata("sidebar_active"), is_array($module) ? $module : [$module])) {
        return "active";
    }
    return "";
}

function csrf() {
    $CI = &get_instance();
    $name = $CI->security->get_csrf_token_name();
    $value = $CI->security->get_csrf_hash();
    return "<input type='hidden' name='$name' value='$value' >";
}

function set_active($active) {
    $CI = &get_instance();
    $CI->session->set_userdata("sidebar_active", $active);
}

function check_param($param, $error = NULL) {
    if (!$param) {
        redirect(base_url("admin/General/error/$error"));
    }
}

function notify($data = []) {
    // Each notify type has different type num
    // Types : [40 => For equipment test] fill with type num...
    if ($data) {
        $CI = &get_instance();
        $CI->load->model('admin/Notifications_model');
        $notify = $CI->Notifications_model->insert_notification($data);
        return $notify;
    }
    return FALSE;
}

function notify_all($data = []) {
    if ($data) {
        $CI = &get_instance();
        $CI->load->model('admin/Notifications_model');
        $notify = $CI->Notifications_model->insert_bulk_notification($data);
        return $notify;
    }
    return FALSE;
}

function notified_before($data = []) {
    if ($data) {
        $CI = &get_instance();
        $CI->load->model('admin/Notifications_model');
        $notified_before = $CI->Notifications_model->notified_before($data);
        return $notified_before;
    }
    return FALSE;
}

function read_notifications($data = [], $format = FALSE, $set_seen = FALSE) {
    if ($data) {
        $CI = &get_instance();
        $CI->load->model('admin/Notifications_model');
        $notifications = $CI->Notifications_model->get_notifications($data, $format, $set_seen);
        return $notifications;
    }
    return FALSE;
}

function set_seen($ids = 0) {
    if ($ids) {
        $CI = &get_instance();
        $CI->load->model('admin/Notifications_model');
        $CI->Notifications_model->set_seen($ids);
        return TRUE;
    }
    return FALSE;
}

function format_unix($time = 0, $format = 'd-m-Y H:i') {
    if ($time) {
        return date($format, $time);
    }
    return FALSE;
}

function get_time_ago($date = '') {
    if ($date == '' || $date == NULL)
        return FALSE;
    $CI = &get_instance();
    $CI->load->helper('date');

    // Declare timestamps
    $last = new DateTime($date);
    $now = new DateTime(date('Y-m-d h:i:s', time()));

    // Find difference
    $interval = $last->diff($now);

    // Store in variable to be used for calculation etc
    $years = (int) $interval->format('%Y');
    $months = (int) $interval->format('%m');
    $days = (int) $interval->format('%d');
    $hours = (int) $interval->format('%H');
    $minutes = (int) $interval->format('%i');


    if ($years > 0) {
        return $years . ' Years ' . $months . ' Months ' . $days . ' Days ' . $hours . ' Hours ' . $minutes . ' minutes ago.';
    } else if ($months > 0) {
        return $months . ' Months ' . $days . ' Days ' . $hours . ' Hours ' . $minutes . ' minutes ago.';
    } else if ($days > 0) {
        return $days . ' Days ' . $hours . ' Hours ' . $minutes . ' minutes ago.';
    } else if ($hours > 0) {
        return $hours . ' Hours ' . $minutes . ' minutes ago.';
    } else {
        return $minutes . ' minutes ago.';
    }
}

function get_genders($type = 0) {
    $genders = [
        1 => ['id' => 1, 'en' => 'Male', 'ar' => 'ذكر'],
        2 => ['id' => 2, 'en' => 'Female', 'ar' => 'أنثى']
    ];
    return $type ? (!empty($genders[$type][lc()]) ? $genders[$type][lc()] : []) : $genders;
}

function cid() {
    $CI = &get_instance();
    return $CI->session->userdata("client_id");
}

function get_branch_hours($branch_id) {
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->where('location_id', $branch_id);
    $CI->db->from('locations_hours');
    $query = $CI->db->get();
    return $query->result();
}

function createDateRangeArray($strDateFrom = NULL, $strDateTo = NULL, $unix = FALSE, $stamp = 'Y-m-d') {
    // takes two dates formatted as YYYY-MM-DD and creates an
    // inclusive array of the dates between the from and to dates.
    // could test validity of dates here but I'm already doing
    // that in the main script

    $aryRange = [];

    $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
    $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

    if ($iDateTo >= $iDateFrom) {
        array_push($aryRange, $unix ? $iDateFrom : date('Y-m-d', $iDateFrom)); // first entry
        while ($iDateFrom < $iDateTo) {
            $iDateFrom += 86400; // add 24 hours
            array_push($aryRange, $unix ? $iDateFrom : date('Y-m-d', $iDateFrom));
        }
    }
    return $aryRange;
}

function number_to_day($d = NULL) {
    $dn = FALSE;
    if (!$d) {
        return FALSE;
    }
    $days = [1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday',];
    if (isset($days[$d])) {
        $dn = $days[$d];
    }
    return $dn;
}

function day_to_number($d = NULL) {
    $dn = FALSE;
    if (!$d) {
        $d = date('D');
    }
    $days = ['Mon' => 1, 'Tue' => 2, 'Wed' => 3, 'Thu' => 4, 'Fri' => 5, 'Sat' => 6, 'Sun' => 7];
    if (isset($days[$d])) {
        $dn = $days[$d];
    }
    return $dn;
}

function get_branch_day_hours($branch_id, $dtn) {
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->where('location_id', $branch_id);
    $CI->db->where('day', $dtn);
    $CI->db->where("day_off !=", 1);
    $CI->db->from('locations_hours');
    $query = $CI->db->get();
    return $query->row();
}

function send_sms($phone_num = NULL, $msg = NULL) {
    if (!empty($phone_num) && !empty($msg)) {
        $CI = &get_instance();
        $sms_sett = $CI->db->get('sms_settings')->row();
        $msg = urlencode($msg);
        return CallAPI('https://smsmisr.com/api/webapi/?Username=' . $sms_sett->username . '&password=' . $sms_sett->password . '&language=1&sender=' . $sms_sett->sender_id . '&Mobile=' . $phone_num . '&message=' . $msg);
    } else {
        return FALSE;
    }
}

function CallAPI($url) {
    $headers = array(
        "cache-control: no-cache",
        "content-type: application/xml",
    );
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, array());
    curl_setopt($curl, CURLOPT_VERBOSE, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($curl);
    if ($error = curl_errno($curl)) {
        return FALSE;
    }
    curl_close($curl);
    return TRUE;
}

function send_email($data) {
    $CI = &get_instance();
    $email_settings = $CI->db->get("email_settings")->row();
    if (!empty($email_settings)) {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $email_settings->smtp_host, //ssl://smtp.googlemail.com
            'smtp_port' => $email_settings->smtp_port, // 465 
            'smtp_user' => $email_settings->smtp_user,
            'smtp_pass' => $email_settings->smtp_pass,
            'mailtype' => 'html'
        );
        $CI->load->library('email');
        $CI->email->initialize($config);
        $CI->email->from($email_settings->host_mail);
        $CI->email->to($data['to']);
        if (!empty($data['subject'])) {
            $CI->email->subject($data['subject']);
        }
        $CI->email->message($data['message']);
        $sent = $CI->email->send();
//                $err = $CI->email->print_debugger();
//                $err = json_encode($err) . "\n";
    }
}

function guest_email($data) {
    $CI = &get_instance();
    $email_settings = $CI->db->get("email_settings")->row();
    if (!empty($email_settings)) {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $email_settings->smtp_host, //ssl://smtp.googlemail.com
            'smtp_port' => $email_settings->smtp_port, // 465 
            'smtp_user' => $email_settings->smtp_user,
            'smtp_pass' => $email_settings->smtp_pass,
            'mailtype' => 'html'
        );
        $CI->load->library('email');
        $CI->email->initialize($config);
        $CI->email->from($data['from']);
        $CI->email->to($data['to']);
        if (!empty($data['subject'])) {
            $CI->email->subject($data['subject']);
        }
        $CI->email->message($data['message']);
        $sent = $CI->email->send();
//                $err = $CI->email->print_debugger();
//                $err = json_encode($err) . "\n";
    }
}

function get_hours($from, $to) {
    $timestamp1 = strtotime($from);
    $timestamp2 = strtotime($to);
    return abs($timestamp2 - $timestamp1) / (60 * 60);
}

function send_active_survey($client_id = NULL) {
    $CI = &get_instance();
    check_param($client_id);
    $client_data = $CI->Global_model->get_data_by_id('clients', $client_id);
    check_param($client_data, "resource");
    $settings = settings('all', 3);
    if (!empty($settings)) {
        $send_survey_type = $settings->send_survey_type;
        if (!$send_survey_type) {
            // per month
            $from = date('Y-m-01');
            $to = date('Y-m-t', strtotime($from));
        } else {
            // per quarter
            $month = date('m');
            $number = ceil($month / 3);
            switch ($number) {
                case 1:
                    // Quarter 1
                    $from = date('Y-01-01');
                    $to = date('Y-03-t', strtotime(date('Y-03-01')));
                    break;
                case 2:
                    // Quarter 2
                    $from = date('Y-04-01');
                    $to = date('Y-06-t', strtotime(date('Y-06-01')));
                    break;
                case 3:
                    // Quarter 3
                    $from = date('Y-07-01');
                    $to = date('Y-09-t', strtotime(date('Y-09-01')));
                    break;
                case 4:
                    // Quarter 4
                    $from = date('Y-10-01');
                    $to = date('Y-12-t', strtotime(date('Y-12-01')));
                    break;
            }
        }
    } else {
        $from = date('Y-m-01');
        $to = date('Y-m-t', strtotime($from));
    }
    $msg = '';
    $CI->db->select('surveys.*');
    $CI->db->from('surveys');
    $CI->db->where('active', 1);
    $CI->db->where("deleted !=", 1);
    $survey = $CI->db->get()->row();
    if (!empty($survey)) {
        // Get count of submitted survey for client
        $CI->db->select('survey_client_b_t.*');
        $CI->db->from('survey_client_b_t');
        $CI->db->where('survey_id', $survey->id);
        $CI->db->where('client_id', $client_id);
        $CI->db->where("DATE(timestamp) >=", $from);
        $CI->db->where("DATE(timestamp) <=", $to);
        $count_survey = $CI->db->count_all_results();
        if ($count_survey < $survey->count) {
            $generated_code = randomString();
            $CI->db->select('survey_questions.*');
            $CI->db->from('survey_questions');
            $CI->db->join('surveys', 'survey_questions.survey_id = surveys.id');
            $CI->db->where('survey_questions.survey_id', $survey->id);
            $CI->db->where("survey_questions.deleted !=", 1);
            $CI->db->where("surveys.deleted !=", 1);
            $survey_questions = $CI->db->get()->result();
            if (!empty($survey_questions)) {
                // insert into survey_client_b_t
                $survey_client_b_t = array(
                    'code' => $generated_code,
                    'survey_id' => $survey->id,
                    'client_id' => $client_id
                );
                $insderted_id = $CI->Global_model->global_insert('survey_client_b_t', $survey_client_b_t);
                if ($insderted_id) {
                    foreach ($survey_questions as $question) {
                        $data = array(
                            'c_b_t' => $insderted_id,
                            'code' => $generated_code,
                            'survey_id' => $survey->id,
                            'client_id' => $client_id,
                            'question_id' => $question->id,
                            'answer_type' => $question->answer_type
                        );
                        $CI->Global_model->global_insert('survey_answers', $data);
                    }
                    $msg .= "Dear $client_data->n_en <br>";
                    $msg .= 'Please answer this survey. <a href="' . base_url() . 'Survey/get_survey/' . $survey->id . '/' . $generated_code . '" title="Survey"> ' . lang("new_survey") . ' </a>';
                    $data = [
                        'to' => $client_data->email,
                        'subject' => lang("new_survey"),
                        'message' => $msg
                    ];
                    email_survey_to_client($data);
                }
            }
        }
    }
}

function email_survey_to_client($data) {
    $CI = &get_instance();
    $email_settings = $CI->db->get("email_settings")->row();
    if (!empty($email_settings)) {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $email_settings->smtp_host, //ssl://smtp.googlemail.com
            'smtp_port' => $email_settings->smtp_port, // 465 
            'smtp_user' => $email_settings->smtp_user,
            'smtp_pass' => $email_settings->smtp_pass,
            'mailtype' => 'html'
        );
        $CI->load->library('email');
        $CI->email->initialize($config);
        $CI->email->from($email_settings->host_mail);
        $CI->email->to($data['to']);
        if (!empty($data['subject'])) {
            $CI->email->subject($data['subject']);
        }
        $CI->email->message($data['message']);
        $sent = $CI->email->send();
    }
}

function randomString($length = 25) {
    $str = "";
    $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
        $rand = mt_rand(0, $max);
        $str .= $characters[$rand];
    }
    return $str;
}

function send_reset_link($email) {
    $CI = &get_instance();
    $msg_content = '';
    //get Client info
    $CI->db->where('email', $email);
    $info = $CI->db->get("clients")->row();
    $token = $info->id . "_" . substr($info->password, 0, 5);
    $msg_content .= "Dear " . $info->{'n_' . lc()} . " <br>";
    $msg_content .= "You are receiving this email because you requested a password reset. Please use the following link to reset your password: <br>"
            . "<a href=" . base_url("Login/reset_my_password/$token") . ">" . base_url("Login/reset_my_password/$token") . "</a> <br>"
            . "If it wasn't you, please, ignore this email. <br>";
    // config email smtp
    $settings = $CI->db->get('email_settings')->row();
    if (!empty($settings)) {
        $config = [
            'protocol' => 'smtp',
            'smtp_host' => $settings->smtp_host, //ssl://smtp.googlemail.com
            'smtp_port' => $settings->smtp_port, // 465 
            'smtp_user' => $settings->smtp_user,
            'smtp_pass' => $settings->smtp_pass,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        ];
        $CI->load->library('email');
        $CI->email->initialize($config);
        $CI->email->from($settings->host_mail);
        $CI->email->to($email);
        $CI->email->subject('Password Reset Link');
        $CI->email->message($msg_content);
        $CI->email->send();
    }
}
