<?php

function search_document($dt = []) {
    $query = !empty($dt['q']) ? $dt['q'] : NULL;
    if ($query) {
        $CI = &get_instance();
        $CI->db->select('id,record_number');

        $CI->db->like('record_number', $query);
        $CI->db->or_like('document_number', $query);

        $CI->db->from(get_sys_idf(8));
        $fetch = $CI->db->get();
        return $fetch->result();
    }
    return FALSE;
}

function search_client($dt = []) {
    $query = !empty($dt['q']) ? $dt['q'] : NULL;
    if ($query) {
        $CI = &get_instance();
        $CI->db->select('*');


        $CI->db->group_start();
        $CI->db->like('record_number', $query);

        $CI->db->or_group_start();
        $CI->db->or_like('national_id', $query);
        $CI->db->or_like('passport_number', $query);
        $CI->db->or_like('n_en', $query);
        $CI->db->or_like('n_ar', $query);
        $CI->db->where('type', 1);
        $CI->db->group_end();
        $CI->db->or_group_start();
        $CI->db->or_like('commercial_registry_number', $query);
        $CI->db->or_like('entity_name', $query);
        $CI->db->where('type', 2);
        $CI->db->group_end();

        $CI->db->group_end();

        $CI->db->from(get_sys_idf(5));
        $fetch = $CI->db->get();
        return $fetch->result();
    }
    return FALSE;
}

function get_client_documents($dt = []) {
    $id = !empty($dt['id']) ? $dt['id'] : NULL;
    if ($id) {
        $CI = &get_instance();
        $company = get_companies_access();
        $company = $company ? $company[0] : NULL;
        $CI->Global_model->global_insert('search_history', ['client_id' => $id, 'user_id' => uid(), 'company_id' => $company, 'timestamp' => date('Y-m-d H:i:s')]);
        $CI->db->select('dc.*, companies.n_' . lc() . ' as company_name, users.n_' . lc() . ' as maker');
        $CI->db->{'where' . (is_array($id) ? '_in' : '')}('dc.client_id', $id);
        $CI->db->from(get_sys_idf(8) . ' dc');

        $CI->db->join("companies", "companies.id = dc.company_id", "left");
        $CI->db->join("users", "users.id = dc.added_by", "left");
        $fetch = $CI->db->get();
        return $fetch->result();
    }
    return FALSE;
}

function get_document_claim($dt = []) {
    $id = !empty($dt['r']) ? $dt['r'] : NULL;
    if ($id) {
        $CI = &get_instance();
        $CI->db->select('cl.*,users.n_' . lc() . ' as maker');
        $CI->db->{'where' . (is_array($id) ? '_in' : '')}('cl.record_number', $id);
        $CI->db->from(get_sys_idf(9) . ' cl');
        $CI->db->join("users", "users.id = cl.added_by", "left");
        $fetch = $CI->db->get();
        return $fetch->result();
    }
    return FALSE;
}

function get_search_history($id = 0, $days = 20) {
    if ($id) {
        $search_time = date('Y-m-d H:i:s', strtotime('-' . $days . 'days', strtotime(date('Y-m-d H:i:s'))));
        $CI = &get_instance();
        $CI->db->select('sh.timestamp,users.n_' . lc() . ' as maker,companies.n_' . lc() . ' as company');
        $CI->db->{'where' . (is_array($id) ? '_in' : '')}('sh.client_id', $id);
        $CI->db->where('sh.timestamp >=', $search_time);
        $CI->db->from('search_history sh');
        $CI->db->join("users", "users.id = sh.user_id", "left");
        $CI->db->join("companies", "companies.id = sh.company_id", "left");
        $CI->db->order_by("sh.timestamp", "desc");
        $fetch = $CI->db->get();
        return $fetch->result();
    }
    return FALSE;
}
