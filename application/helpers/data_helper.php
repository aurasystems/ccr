<?php

function get_date() {  //helper
    date_default_timezone_set('Africa/Cairo');
    return date('Y-m-d H:i:s');
}

function get_last_run($data) {
    date_default_timezone_set('Africa/Cairo');
    $cur_time = date('Y-m-d H:i:s');
    //  echo $data['types'][2]->last_run.'c'.$cur_time;
    $data_time = [];
    $i = 0;
    foreach ($data['types'] as $dt) {

        $diff = abs(strtotime($cur_time) - strtotime($data['types'][$i]->last_run));
        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
        $weeks = $days / 7;
        $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
        $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
        $seconds = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));

        $data_time[$i] = [
            'week' => $weeks,
            'day' => $days,
            'hour' => $hours,
            'minute' => $minutes,
            'second' => $seconds
        ];

        // echo $hours;
        // echo $data_time[$i]['day']."hr ";

        $i++;
    }
    return $data_time;
}

function get_branch($location_id, $table) {
    $CI = &get_instance();
    $CI->db->select('n_' . lc())->from($table)->where('id = ', $location_id)->where('deleted !=', 1);

    $res = $CI->db->get()->row('n_' . lc());

    return $res;
}

function get_avaiable_products($branch_id = null) {
    $CI = &get_instance();

    $count = 0;

    $count += $CI->db->select('*')->from('products')->where('location_id', $branch_id)->where('deleted !=', 1)->count_all_results();

    return $count;
}

/* function get_avaiable_products( $branch_id=null,$product_id=null,$is_num_prod=false)
  {
  $CI = &get_instance();
  $res_1='';
  $res_2='';
  $count=0;

  if($product_id==null && $branch_id==null)
  {
  $CI->db->select('id,location_id')->from('products');
  $res = $CI->db->get()->result();
  foreach($res as $one)
  {
  $count += $CI->db->select('*')->from('product_items')->where('product_id',$one->id)->count_all_results();
  }
  $CI->db->select('id,product_id')->from('products_sup');
  $res = $CI->db->get()->result();
  foreach($res as $one)
  {
  $count += $CI->db->select('*')->from('product_sup_items')->where('product_id',$one->id)->count_all_results();
  }
  }
  else if($branch_id!=null)
  {

  $CI->db->select('id')->from('products')->where('location_id',$branch_id);
  $res_1 = $CI->db->get()->result();
  if($is_num_prod==true)
  {
  $count += $CI->db->select('*')->from('products')->where('location_id',$branch_id)->count_all_results();
  }
  else if($is_num_prod==false)
  {
  foreach($res_1 as $one)
  {

  $count += $CI->db->select('*')->from('product_items')->where('product_id',$one->id)->count_all_results();
  $count += $CI->db->select('id')->from('products_sup')->where('product_id',$one->id)->count_all_results();

  $CI->db->select('id')->from('products_sup')->where('product_id',$one->id);
  $res_2 = $CI->db->get()->result();
  foreach($res_2 as $one_2)
  {
  $count += $CI->db->select('*')->from('product_sup_items')->where('product_id',$one_2->id)->count_all_results();
  }

  }
  }
  }
  else if($product_id!=null)
  {


  $count += $CI->db->select('*')->from('product_items')->where('product_id',$product_id)->count_all_results();
  $CI->db->select('id')->from('products_sup')->where('product_id',$product_id);
  $res = $CI->db->get()->result();
  foreach($res as $one)
  {
  $count += $CI->db->select('*')->from('product_sup_items')->where('product_id',$one->id)->count_all_results();
  }

  }

  return $count;

  } */

function get_products($branch_id = null) {

    $CI = &get_instance();

    $res = array();
    $res_2 = array();

    if ($branch_id != null)
        $CI->db->select('*')->from('products')->where('location_id', $branch_id)->where('deleted !=', 1);
    else
        $CI->db->select('*')->from('products')->where('deleted !=', 1);

    $res = $CI->db->get()->result();


    return $res;
}

function get_categories($sub = false, $parent_id = null) {

    $CI = &get_instance();
    if ($sub == true)
        $CI->db->select('*')->from('categories')->where('parent =', $parent_id)->where('deleted !=', 1);
    else
        $CI->db->select('*')->from('categories')->where('parent =', 0)->where('deleted !=', 1);

    $res = $CI->db->get()->result();

    return $res;
}

function get_sub_categories($category_id) {
    $CI = &get_instance();

    $ret = array();

    $CI->db->select('product_id')->from('product_categories')->where('category_id', $category_id);
    $CI->db->distinct();
    $res = $CI->db->get()->result();
    $arr = array_column($res, "product_id");


    if (count($arr) > 0) {
        $CI->db->select('brand_id')->from('product_brands')->where_in('product_id', $arr);
        $CI->db->distinct();
        $res = $CI->db->get()->result();
        $arr = array_column($res, "brand_id");
        if (count($arr) > 0) {
            $CI->db->select('*')->from('brands')->where_in('id', $arr)->where('deleted !=', 1);
            $ret = $CI->db->get()->result();
            return $ret;
        } else
            return $ret;
    } else
        return $ret;
}

function get_avaiable_items($product_id = NULL, $location_id = NULL) {
    $CI = &get_instance();
    $count = 0;
    $count += $CI->db->select('*')->from('product_items')->where('product_id', $product_id)->where('location_id', $location_id)->where('deleted !=', 1)->where('sold !=', 1)->count_all_results();
    return $count;
}

function get_brands($category_id = null) {
    $CI = &get_instance();
    $res = array();
    $ret = array();
    if ($category_id == null) {
        $CI->db->select('*')->from('brands')->where('deleted !=', 1);
        $ret = $CI->db->get()->result();
    } else if ($category_id != null) {
        $CI->db->select('product_id')->from('product_categories')->where('category_id', $category_id);
        $CI->db->distinct();
        $res = $CI->db->get()->result();
        $arr = array_column($res, "product_id");
        if (count($arr) > 0) {
            $CI->db->select('brand_id')->from('product_brands')->where_in('product_id', $arr);
            $CI->db->distinct();
            $ret = $CI->db->get()->result();
            $arr = array_column($ret, "brand_id");
            if (count($arr) > 0) {
                $CI->db->select('*')->from('brands')->where_in('id', $arr)->where('deleted !=', 1);
                $ret = $CI->db->get()->result();
            }
        }
    }
    return $ret;
}

function get_record($field, $id, $table, $is_deleted = true) {
    $CI = &get_instance();
    if ($is_deleted == false)
        $CI->db->select('*')->from($table)->where($field, $id);
    else
        $CI->db->select('*')->from($table)->where($field, $id)->where('deleted !=', 1);
    $res = $CI->db->get()->result();

    return $res;
}

function get_cat_name($product_id) {
    $CI = &get_instance();

    $cat_n = '';

    $CI->db->select('category_id');
    $CI->db->where('product_id', $product_id);
    $res = $CI->db->get('product_categories');
    if ($res->num_rows() > 0) {
        $CI->db->select('n_en,parent')->from('categories')->where('id', $res->row()->category_id);
        $parent = $CI->db->get()->row()->parent;
        if ($parent != 0)
            $cat_n = $CI->db->select('n_en')->from('categories')->where('id', $parent)->get()->row()->n_en;
        else
            $cat_n = $CI->db->get()->row()->n_en;
    }

    return $cat_n;
}

function get_cat_product($id, $is_brand = false) {
    $CI = &get_instance();
    if ($is_brand == true) {
        if ($id == 0)
            $CI->db->select('product_id')->from('product_brands');
        else
            $CI->db->select('product_id')->from('product_brands')->where('brand_id =', $id);
        $CI->db->distinct();
        $res = $CI->db->get()->result();
        $ret = array();
    } else if ($is_brand == false) {
        $CI->db->select('product_id')->from('product_categories')->where('category_id =', $id);
        $CI->db->distinct();
        $res = $CI->db->get()->result();
        $ret = array();
    }
    $arr = array_column($res, "product_id");
    if (count($arr) > 0) {
        $CI->db->select('*')->from('products')->where_in('id', $arr)->where('deleted !=', 1);
        $ret = $CI->db->get()->result();
    }
    return $ret;
}

function fetch_cat_product($category_id = NULL, $count = FALSE, $limit = NULL, $start = NULL) {
    $ret = [];
    $CI = &get_instance();
    $CI->db->select('product_id')->from('product_categories')->where('category_id =', $category_id);
    $CI->db->distinct();
    if ($count) {
        return $CI->db->count_all_results();
    } else {
        $res = $CI->db->get()->result();
        //print_r($res);die;
        $arr = array_column($res, "product_id");
        //print_r($arr);die;
        if (count($arr) > 0) {
            $CI->db->select('*')->from('products');
            $CI->db->where_in('id', $arr);
            $CI->db->where('deleted !=', 1);
            if (!empty($limit) && !empty($start)) {
                $CI->db->limit($limit, $start);
            }
            $ret = $CI->db->get()->result();
        }
        return $ret;
    }
}

function fetch_brand_product($brand_id = NULL, $count = FALSE, $limit = NULL, $start = NULL) {
    $ret = [];
    $CI = &get_instance();
    $CI->db->select('product_id')->from('product_brands')->where('brand_id =', $brand_id);
    $CI->db->distinct();
    if ($count) {
        return $CI->db->count_all_results();
    } else {
        $res = $CI->db->get()->result();
        $arr = array_column($res, "product_id");
        if (count($arr) > 0) {
            $CI->db->select('*')->from('products');
            $CI->db->where_in('id', $arr);
            $CI->db->where('deleted !=', 1);
            if (!empty($limit) && !empty($start)) {
                $CI->db->limit($limit, $start);
            }
            $ret = $CI->db->get()->result();
        }
        return $ret;
    }
}

function brand_prod_count($brand_id = null) {
    $CI = &get_instance();
    $ret = array();
    $arr = array();
    $count = 0;
    if ($brand_id == null) {
        $CI->db->select('product_id')->from('product_brands');
        $CI->db->distinct();
        $ret = $CI->db->get()->result();
        $arr = array_column($ret, "product_id");
    } else if ($brand_id != null) {
        $CI->db->select('product_id')->from('product_brands')->where('brand_id', $brand_id);
        $CI->db->distinct();
        $ret = $CI->db->get()->result();
        $arr = array_column($ret, "product_id");
    }
    $count = $CI->db->select('*')->from('products')->where_in('id', $arr)->where('deleted !=', 1)->count_all_results();

    return $count;
}

function get_cart() {
    $CI = &get_instance();
    $cart = array();
    $CI->db->select('*')->from('cart')->where('client_id', cid());
    $CI->db->where('parent', 0);
    $CI->db->limit(1);
    $res = $CI->db->get()->row();
    if (!empty($res)) {
        $CI->db->select('*');
        $CI->db->from('cart');
        $CI->db->where('parent', $res->id);
        $cart = $CI->db->get()->result();
    }
    return $cart;
}

function get_product_details($product_id) {
    $CI = &get_instance();
    $CI->db->select('id,n_en,img,rent_price, rent_term')->from('products')->where('id', $product_id)->where('deleted != ', 1);
    $res = $CI->db->get()->row();
    return $res;
}

function get_quantity($client_id, $cart_id = null, $is_number = false) {
    $CI = &get_instance();
    $quantity = array();
    $count = 0;
    if ($is_number == true) {
        if ($cart_id == null) {
            $CI->db->select('id');
            $CI->db->from('cart');
            $CI->db->where('parent', 0);
            $CI->db->where('client_id', $client_id);
            $CI->db->limit(1);
            $cart_id = $CI->db->get()->row('id');
        }
        $count = $CI->db->select('product_id')->from('cart')->where('parent', $cart_id)->count_all_results();
        return $count;
    } else if ($is_number == false) {
        $CI->db->select('quantity')->from('cart')->where('id', $cart_id);
        $quantity = $CI->db->get()->row('quantity');
    }

    return $quantity;
}

function delete_cart($cart_id = null) {
    $CI = &get_instance();

    if ($cart_id != null)
        $CI->db->where('id', $cart_id);
    else {
        $CI->db->select('*');
        $CI->db->from('cart');
        $CI->db->where('parent', 0);
        $CI->db->where('client_id', cid());
        $CI->db->limit(1);
        $cart_root = $CI->db->get()->row();
        if (!empty($cart_root)) {
            $CI->db->where('id', $cart_root->id);
            $CI->db->delete('cart');
            $CI->db->where('parent', $cart_root->id);
            return $CI->db->delete('cart');
        } else
            return;
    }
    return $CI->db->delete('cart');
}

function get_total_count() {
    $CI = &get_instance();
    $count = 0;
    $quantity = array();
    $products = array();
    $CI->db->select('id');
    $CI->db->from('cart');
    $CI->db->where('parent', 0);
    $CI->db->where('client_id', cid());
    $CI->db->limit(1);
    $parent = $CI->db->get()->row('id');
    $CI->db->select('product_id')->from('cart')->where('parent', $parent);
    $products = $CI->db->get()->result();
    $arr = array_column($products, "product_id");
    if (count($arr) > 0) {
        $CI->db->select('id,rent_price')->from('products')->where_in('id', $arr)->where('deleted != ', 1);
        $products = $CI->db->get()->result();
        $CI->db->select('quantity, product_id')->from('cart')->where('parent', $parent);
        $quantity = $CI->db->get()->result();

        $n = 0;

        foreach ($products as $product) {
            $count += $product->rent_price * $quantity[$n]->quantity;
            $n++;
        }
    }
    return $count;
}

function get_related_product($category_id) {
    $CI = &get_instance();
    $ret = array();
    $CI->db->select("product_categories.*, products.n_" . lc() . " as pname, products.img as pimg");
    $CI->db->from('product_categories');
    $CI->db->join("products", "products.id = product_categories.product_id", 'left');
    $CI->db->where('product_categories.category_id', $category_id);
    $CI->db->where('products.deleted !=', 1);
    return $CI->db->get()->result();
}

function checkAvailability($location = NULL, $product, $quantity, $b_from, $b_to) {
    $CI = &get_instance();
    $result = [];
    if ($b_from != '' && $b_to != '' && $quantity != '' && $quantity > 0) {
        // first check if this product is exists in selected branch
        $getItemsByLocation = getItemsByLocation($product, $location);
        if ($getItemsByLocation > 0) {
            $diff = abs(strtotime($b_to) - strtotime($b_from));
            $years = floor($diff / (365 * 60 * 60 * 24));
            $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
            $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
            $days = $days + 1;
            // Get all bookings for this product during b_from and b_to
            $getProductBookings = getProductBookings($product, $b_from, $b_to);
            if (!empty($getProductBookings)) {
                // Bookings found for this product during selected date range
                $count = 0;
                for ($i = 1; $i <= $days; $i++) {
                    // loop for each day of posted date ranges and check count and compare with posted quantity
                    foreach ($getProductBookings as $booking) {
                        if ($booking->b_from <= $b_from && $booking->b_to >= $b_from) {
                            $count = $count + 1;
                        }
                    }
                    // Later we will check for maintenance...
                    $undamagedProducts = getUndamagedProducts($product, $location);
                    if ($undamagedProducts <= $count || $quantity > ($undamagedProducts - $count)) {
                        $msg = lang('day') . ' (' . $b_from . ') ' . lang("req_qty_not_available");
                        $result = ['flag' => FALSE, 'msg' => $msg];
                        return $result;
                    }
                    $b_from = date("Y-m-d H:i:s", strtotime("$b_from +1 day"));
                    $count = 0;
                }
                $msg = lang("product_added_to_booking");
                $result = ['flag' => TRUE, 'msg' => $msg];
                return $result;
            } else {
                // No bookings found
                // Later we will check for maintenance...
                $undamagedProducts = getUndamagedProducts($product, $location);
                if ($quantity > $undamagedProducts) {
                    $msg = lang('only') . ' (' . $undamagedProducts . ') ' . lang("prod_available");
                    $result = ['flag' => FALSE, 'msg' => $msg];
                    return $result;
                } else {
                    $msg = lang("product_added_to_booking");
                    $result = ['flag' => TRUE, 'msg' => $msg];
                    return $result;
                }
            }
        } else {
            // Product not exists in selected location
            $msg = lang("product_not_found");
            $result = ['flag' => FALSE, 'msg' => $msg];
            return $result;
        }
    } else {
        // Fill in all posted fields
        $msg = lang("fill_in_all_fields");
        $result = ['flag' => FALSE, 'msg' => $msg];
        return $result;
    }
}

function checkAvailability_old($location = null, $product, $quantity, $b_from, $b_to) {
    $CI = &get_instance();
    $result = [];
    if ($location == null) {
        if ($b_from != '' && $b_to != '' && $quantity != '' && $quantity > 0) {
            // first check if this product is exists in selected branch
            $getItemsByLocation = getItemsByLocation($product);
            if ($getItemsByLocation > 0) {
                $diff = abs(strtotime($b_to) - strtotime($b_from));
                $years = floor($diff / (365 * 60 * 60 * 24));
                $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
                $days = $days + 1;
                // Get all bookings for this product during b_from and b_to
                $getProductBookings = getProductBookings($product, $b_from, $b_to);
                if (!empty($getProductBookings)) {
                    // Bookings found for this product during selected date range
                    $count = 0;
                    for ($i = 1; $i <= $days; $i++) {
                        // loop for each day of posted date ranges and check count and compare with posted quantity
                        foreach ($getProductBookings as $booking) {
                            if ($booking->b_from <= $b_from && $booking->b_to >= $b_from) {
                                $count = $count + 1;
                            }
                        }
                        // Later we will check for maintenance...
                        $undamagedProducts = getUndamagedProducts($product);
                        if ($undamagedProducts <= $count || $quantity > ($undamagedProducts - $count)) {
                            $msg = lang('day') . ' (' . $b_from . ') ' . lang("req_qty_not_available");
                            $result = ['flag' => FALSE, 'msg' => $msg];
                            return $result;
                        }
                        $b_from = date("Y-m-d H:i:s", strtotime("$b_from +1 day"));
                        $count = 0;
                    }
                    $msg = lang("product_added_to_booking");
                    $result = ['flag' => TRUE, 'msg' => $msg];
                    return $result;
                } else {
                    // No bookings found
                    // Later we will check for maintenance...
                    $undamagedProducts = getUndamagedProducts($product);
                    if ($quantity > $undamagedProducts) {
                        $msg = lang('only') . ' (' . $undamagedProducts . ') ' . lang("prod_available");
                        $result = ['flag' => FALSE, 'msg' => $msg];
                        return $result;
                    } else {
                        $msg = lang("product_added_to_booking");
                        $result = ['flag' => TRUE, 'msg' => $msg];
                        return $result;
                    }
                }
            } else {
                // Product not exists in selected location
                $msg = lang("product_not_found_all_stock");
                $result = ['flag' => FALSE, 'msg' => $msg];
                return $result;
            }
        } else {
            // Fill in all posted fields
            $msg = lang("fill_in_all_fields");
            $result = ['flag' => FALSE, 'msg' => $msg];
            return $result;
        }
    } else if ($location != null) {
        if ($b_from != '' && $b_to != '' && $location != '' && $quantity != '' && $quantity > 0) {
            // first check if this product is exists in selected branch
            $getItemsByLocation = getItemsByLocation($product, $location);
            if ($getItemsByLocation > 0) {
                $diff = abs(strtotime($b_to) - strtotime($b_from));
                $years = floor($diff / (365 * 60 * 60 * 24));
                $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
                $days = $days + 1;
                // Get all bookings for this product during b_from and b_to
                $getProductBookings = getProductBookings($product, $b_from, $b_to);
                if (!empty($getProductBookings)) {
                    // Bookings found for this product during selected date range
                    $count = 0;
                    for ($i = 1; $i <= $days; $i++) {
                        // loop for each day of posted date ranges and check count and compare with posted quantity
                        foreach ($getProductBookings as $booking) {
                            if ($booking->b_from <= $b_from && $booking->b_to >= $b_from) {
                                $count = $count + 1;
                            }
                        }
                        // Later we will check for maintenance...
                        $undamagedProducts = getUndamagedProducts($product, $location);
                        if ($undamagedProducts <= $count || $quantity > ($undamagedProducts - $count)) {
                            $msg = lang('day') . ' (' . $b_from . ') ' . lang("req_qty_not_available");
                            $result = ['flag' => FALSE, 'msg' => $msg];
                            return $result;
                        }
                        $b_from = date("Y-m-d H:i:s", strtotime("$b_from +1 day"));
                        $count = 0;
                    }
                    $msg = lang("product_added_to_booking");
                    $result = ['flag' => TRUE, 'msg' => $msg];
                    return $result;
                } else {
                    // No bookings found
                    // Later we will check for maintenance...
                    $undamagedProducts = getUndamagedProducts($product, $location);
                    if ($quantity > $undamagedProducts) {
                        $msg = lang('only') . ' (' . $undamagedProducts . ') ' . lang("prod_available");
                        $result = ['flag' => FALSE, 'msg' => $msg];
                        return $result;
                    } else {
                        $msg = lang("product_added_to_booking");
                        $result = ['flag' => TRUE, 'msg' => $msg];
                        return $result;
                    }
                }
            } else {
                // Product not exists in selected location
                $msg = lang("product_not_found");
                $result = ['flag' => FALSE, 'msg' => $msg];
                return $result;
            }
        } else {
            // Fill in all posted fields
            $msg = lang("fill_in_all_fields");
            $result = ['flag' => FALSE, 'msg' => $msg];
            return $result;
        }
    }
}

function getLocations() {
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('locations');
    $CI->db->where('deleted !=', 1);
    return $CI->db->get()->result();
}

function getItemsByLocation($product, $location = NULL) {
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('product_items');
    $CI->db->where('product_id', $product);
    if ($location != NULL)
        $CI->db->where('location_id', $location);
    $CI->db->where("deleted !=", 1);
    $CI->db->where("sold !=", 1);
    return $CI->db->count_all_results();
}

function get_product_by_id($id) {
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('products');
    $CI->db->where('id', $id);
    $CI->db->where("deleted !=", 1);
    return $CI->db->get()->row();
}

function getProductBookings($product, $b_from, $b_to) {
    $CI = &get_instance();
    $CI->db->select('booking_items.*');
    $CI->db->from('booking_items');
    $CI->db->where('booking_items.product_id', $product);
    $CI->db->group_start();
    $CI->db->where('b.status', '1');
    $CI->db->or_where('b.status', '2');
    $CI->db->group_end();
    $CI->db->group_start();
    $CI->db->group_start();
    $CI->db->where('booking_items.b_from >=', $b_from);
    $CI->db->where('booking_items.b_from <=', $b_to);
    $CI->db->group_end();
    $CI->db->or_group_start();
    $CI->db->where('booking_items.b_to >=', $b_from);
    $CI->db->where('booking_items.b_to <=', $b_to);
    $CI->db->group_end();
    $CI->db->or_group_start();
    $CI->db->where('booking_items.b_from >=', $b_from);
    $CI->db->where('booking_items.b_to <=', $b_to);
    $CI->db->group_end();
    $CI->db->or_group_start();
    $CI->db->where('booking_items.b_from <=', $b_from);
    $CI->db->where('booking_items.b_to >=', $b_to);
    $CI->db->group_end();
    $CI->db->group_end();
    $CI->db->join('booking b', 'b.id = booking_items.book_id');
    $res = $CI->db->get()->result();
    // print_r($res);
    return $res;
}

function get_quatitiy($cart_id = null) {
    $CI = &get_instance();
    if ($cart_id != null) {
        $CI->db->select('quantity')->from('cart')->where('id', $cart_id);
        return $CI->db->get()->row('quantity');
    }
    return null;
}

function getUndamagedProducts($product, $location = NULL) {
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('product_items');
    $CI->db->where('product_id', $product);
    if ($location != NULL)
        $CI->db->where('location_id', $location);
    $CI->db->where("damaged", 0);
    $CI->db->where("missing", 0);
    $CI->db->where("exist", 1);
    $CI->db->where("deleted !=", 1);
    $CI->db->where("sold !=", 1);
    return $CI->db->count_all_results();
}

function get_featured_products($categ_id = null) {
    $CI = &get_instance();
    $sub_cat_id = 0;
    $final = array();
    $products_id = array();
    if ($categ_id == null) {
        $CI->db->select('*')->from('products')->where('featured', 1)->where('deleted !=', 1);
        $final = $CI->db->get()->result();
    } else if ($categ_id != null) {
        $CI->db->select('id')->from('categories')->where('parent', $categ_id);
        $res = $CI->db->get();
        if ($res->num_rows() > 0) {
            $sub_cat_id = $res->row()->id;
            $CI->db->select('product_id')->from('product_categories')->where('category_id', $sub_cat_id);
            $products_id = $CI->db->get()->result();
            if (count($products_id) > 0) {
                $arr = array_column($products_id, "product_id");
                $CI->db->select('*')->from('products')->where_in('id', $arr)->where('featured', 1)->where('deleted !=', 1);
                $final = $CI->db->get()->result();
            }
        }
    }
    return !empty($final) ? $final : null;
}

function count_featured_products($count, $limit = NULL, $start = NULL) {
    $ret = [];
    $CI = &get_instance();
    $CI->db->select('id')->from('products')->where('featured', 1)->where('deleted !=', 1);
    $CI->db->distinct();
    if ($count) {
        return $CI->db->count_all_results();
    } else {
        $res = $CI->db->get()->result();
        $arr = array_column($res, "id");
        if (count($arr) > 0) {
            $CI->db->select('*')->from('products');
            $CI->db->where_in('id', $arr);
            if (!empty($limit) && !empty($start)) {
                $CI->db->limit($limit, $start);
            }
            $ret = $CI->db->get()->result();
        }
        return $ret;
    }
}

function check_type_items_checked($test_id = NULL, $pitem_id = NULL) {
    $CI = &get_instance();
    $CI->db->select("equipment_test_checks.id, equipment_test_checks.chk_t_i_id, equipment_test_checks.is_checked, check_items.n_" . lc() . " as ctitem");
    $CI->db->from('equipment_test_checks');
    $CI->db->join('check_items', 'check_items.id = equipment_test_checks.chk_t_i_id');
    $CI->db->where('equipment_test_checks.test_id', $test_id);
    $CI->db->where('equipment_test_checks.p_item_id', $pitem_id);
    $CI->db->where('equipment_test_checks.is_checked !=', 1);
    $CI->db->where("check_items.deleted !=", 1);
    return $CI->db->count_all_results();
}

function assignedSups_perSupProduct($book_id, $prep_id, $product_SupID) {
    $CI = &get_instance();
    $CI->db->select('id');
    $CI->db->from('items_delivery_temp');
    $CI->db->where('prep_id', $prep_id);
    $CI->db->limit(1);
    $deliver_id = $CI->db->get()->row('id');
    $CI->db->select('*');
    $CI->db->from('sup_items_temp');
    $CI->db->where('product_sup_id', $product_SupID);
    $CI->db->where('book_id', $book_id);
    $CI->db->where('delivery_id', $deliver_id);
    $CI->db->where('sup_id !=', null);
    return $CI->db->count_all_results();
}

function getItemsPerproduct($table, $product_id, $book_id, $is_delivery = false) {
    $CI = &get_instance();

    $CI->db->select('*')->from('booking_items');
    $CI->db->where('book_id', $book_id);
    $CI->db->where('product_id', $product_id);
    $book_data = $CI->db->get()->result();

    $days = array();
    $ava_items = array();
    $deliv_bookID = '';
    if (!empty($book_data)) {

        $ava_items = available_items($product_id);

        foreach ($book_data as $date) {
            $days = array();
            if ($date->rent_term == 1) {
                $days = createDateRangeArraygivenStamp($date->b_from, $date->b_to); // get the time range for this product
                $days[0] = $date->b_from;
                $days[count($days) - 1] = $date->b_to;
            } else if ($date->rent_term == 2) {
                $days[0] = $date->b_from;
                $days[1] = $date->b_to;
            }
            foreach ($days as $day) {
                $booked_prod = booked_products($product_id, $day);
                $ava_count = count($ava_items);
                $booked_count = count($booked_prod);
                if ($booked_count > 0) {

                    foreach ($booked_prod as $one) {
                        /*    if ($is_delivery == true) {
                          $CI->db->select('*')->from($table);
                          $delivery_items = $CI->db->get()->result();
                          foreach ($delivery_items as $item) {
                          if (($item->id + $one->book_id == $item->book_id) && ($item->product_id == $one->product_id))
                          $deliv_bookID = $item->book_id;
                          }
                          $CI->db->select('item_id')->from($table)->where('book_id', $deliv_bookID)->where('product_id', $one->product_id);
                          } else */
                        $CI->db->select('item_id')->from($table)->where('book_id', $one->book_id)->where('product_id', $one->product_id);
                        $CI->db->where('item_id !=', null);
                        $used_item = $CI->db->get()->result();
                        if (!empty($used_item)) {
                            foreach ($used_item as $item) {// for($i =0 ; $i<count($ava_items); $i++)
                                $i = 0;
                                foreach ($ava_items as $ava) {
                                    if ($ava->id == $item->item_id)
                                        unset($ava_items[$i]);
                                    $i++;
                                }
                            }
                        }
                        //get current used items in temp
                        if ($is_delivery == true) {
                            $CI->db->select('item_id')->from('items_delivery_temp')->where('book_id', $one->book_id)->where('product_id', $one->product_id);
                            $CI->db->where('item_id !=', null);
                            $used_item = $CI->db->get()->result();

                            if (!empty($used_item)) {
                                foreach ($used_item as $item) {// for($i =0 ; $i<count($ava_items); $i++)
                                    $i = 0;
                                    foreach ($ava_items as $ava) {
                                        if ($ava->id == $item->item_id)
                                            unset($ava_items[$i]);
                                        $i++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return $ava_items;
}

function available_items($product_id) {

    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('product_items');
    $CI->db->where('product_id', $product_id);
    $CI->db->where('damaged != ', 1);
    $CI->db->where('exist', 1);
    $CI->db->where('missing !=', 1);
    $CI->db->where('deleted !=', 1);
    $CI->db->where('sold !=', 1);
    return $CI->db->get()->result();
}

function booked_products($product_id, $day) {
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('booking_items');
    $CI->db->where('product_id = ', $product_id);
    $CI->db->where('b_from <=', $day);
    $CI->db->where('b_to >=', $day);
    $res = $CI->db->get()->result();

    for ($i = 0; $i < count($res); $i++) {
        $CI->db->select('*')->from('booking')->where('id', $res[$i]->book_id);
        $booking = $CI->db->get()->row();
        if ($booking->status == 0 || $booking->canceled == 1)
            unset($res[$i]);
    }

    return $res;
}

function createDateRangeArraygivenStamp($strDateFrom = NULL, $strDateTo = NULL, $unix = FALSE, $stamp = 'Y-m-d H:m:s') {
    // takes two dates formatted as YYYY-MM-DD and creates an
    // inclusive array of the dates between the from and to dates.
    // could test validity of dates here but I'm already doing
    // that in the main script

    $aryRange = [];

    $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
    $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

    if ($iDateTo >= $iDateFrom) {
        array_push($aryRange, $unix ? $iDateFrom : date($stamp, $iDateFrom)); // first entry
        while ($iDateFrom < $iDateTo) {
            $iDateFrom += 86400; // add 24 hours
            array_push($aryRange, $unix ? $iDateFrom : date($stamp, $iDateFrom));
        }
    }
    return $aryRange;
}

function getSupItemsPerproduct($table, $product_id, $book_id, $product_SupID = null) {
    $CI = &get_instance();
    $today = date('Y-m-d H:i:s');
    $CI->db->select('*')->from('booking_items');
    $CI->db->where('book_id', $book_id);
    $CI->db->where('product_id', $product_id);
    $CI->db->where('b_from >=', $today);
    $book_data = $CI->db->get()->result();
    $days = array();
    $ava_items = array();
    $r = 0;
    $ava_items = available_sup_items($product_SupID);

    if (!empty($book_data)) {
        foreach ($book_data as $date) {
            if ($date->rent_term == 1)
                $days = createDateRangeArraygivenStamp($date->b_from, $date->b_to); // get the time range for this product
            else if ($date->rent_term == 2) {
                $days[0] = $date->b_from;
                $days[1] = $date->b_to;
            }

            foreach ($days as $day) {
                $booked_prod = booked_products($product_id, $day);
                $ava_count = count($ava_items);
                $booked_count = count($booked_prod);
                if ($booked_count > 0) {
                    foreach ($booked_prod as $one) {
                        $CI->db->select('id')->from($table);
                        $CI->db->where('product_id', $one->product_id);
                        $CI->db->where('book_id', $one->book_id);
                        $delivery_items = $CI->db->get()->result();
                        $deliv_id = array_column($delivery_items, "id");
                        /*  $r = 0;
                          foreach ($delivery_items as $item) {
                          if (($item->id + $one->book_id == $item->book_id) && ($item->product_id == $one->product_id)) {
                          $deliv_id[$r] = $item->id;
                          $r++;
                          }
                          } */

                        //if ($product_SupID != null) {

                        /*  $CI->db->select('id')->from($table)->where('book_id', $deliv_bookID)->where('product_id', $one->product_id);
                          $res = $CI->db->get()->result();
                          $arr = array_column($res, "id"); */
                        $CI->db->select('sup_id')->from('booking_sup_items_delivery');
                        $CI->db->where_in('delivery_id', $deliv_id ? $deliv_id : [NULL]);
                        $CI->db->where('deleted !=', 1);
                        $CI->db->where('sup_id !=', null);
                        $CI->db->where('product_sup_id', $product_SupID);
                        $used_item = $CI->db->get()->result();
                        /*  } else {
                          $CI->db->select('sup_id')->from($table)->where('id', $deliv_id)->where('product_id', $one->product_id);
                          $CI->db->where('sup_id !=', null);
                          $used_item = $CI->db->get()->result();
                          } */
                        if (!empty($used_item)) {
                            foreach ($used_item as $item) {
                                $i = 0;
                                foreach ($ava_items as $ava) {// for($i =0 ; $i<count($ava_items); $i++)
                                    if ($ava->id == $item->sup_id)
                                        unset($ava_items[$i]);
                                    $i++;
                                }
                            }
                        }
                        //check used items in sup delivery temp
                        $CI->db->select('sup_id')->from('sup_items_temp');
                        $CI->db->where('book_id', $book_id);
                        $CI->db->where('sup_id !=', null);
                        $CI->db->where('product_sup_id', $product_SupID);
                        $used_item = $CI->db->get()->result();

                        if (!empty($used_item)) {
                            foreach ($used_item as $item) {
                                $i = 0;
                                foreach ($ava_items as $ava) {// for($i =0 ; $i<count($ava_items); $i++)
                                    if ($ava->id == $item->sup_id)
                                        unset($ava_items[$i]);
                                    $i++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return $ava_items;
}

function available_sup_items($product_id) {
    $CI = &get_instance();
    /* $CI->db->select('id');
      $CI->db->from('products_sup');
      $CI->db->where('product_id', $product_id);
      $CI->db->where('deleted !=', 1);
      $product_SupID = $CI->db->get()->row('id'); */
    $CI->db->select('*');
    $CI->db->from('product_sup_items');
    $CI->db->where('product_id', $product_id);
    $CI->db->where('damaged != ', 1);
    $CI->db->where('exist', 1);
    $CI->db->where('missing !=', 1);
    $CI->db->where('deleted !=', 1);
    return $CI->db->get()->result();
}

function get_SN($item_id) {
    $CI = &get_instance();
    $CI->db->select('id, serial_number SN')->from('product_items');
    $CI->db->where('id', $item_id);
    $CI->db->where('deleted !=', 1);
    $CI->db->where('sold !=', 1);
    //$CI->db->where('damaged !=', 1);
    //$CI->db->where('exist !=', 0);
    $res = $CI->db->get()->row('SN');
    return $res;
}

function get_sup_SN($sup_item_id) {
    $CI = &get_instance();
    $CI->db->select('serial_number SN')->from('product_sup_items');
    $CI->db->where('id', $sup_item_id);
    $CI->db->where('deleted !=', 1);
    $res = $CI->db->get()->row('SN');
    //print_r($res);
    //echo $sup_item_id;
    return !empty($res) ? $res : '';
}

function user_locations($user_id) {
    $CI = &get_instance();
    $CI->db->select("user_locations.*, locations.n_" . lc() . " as lname");
    $CI->db->from('user_locations');
    $CI->db->join('locations', 'locations.id = user_locations.location_id', 'left');
    $CI->db->where('user_locations.user_id', $user_id);
    $CI->db->where("locations.deleted !=", 1);
    return $CI->db->get()->result();
}

function get_sup_items($product_id) {
    $CI = &get_instance();
    $res = array();
    $CI->db->select('*')->from('products_sup')
            ->where('product_id', $product_id);
    $CI->db->where('deleted !=', 1);
    //$CI->db->where('damaged !=', 1);
    //$CI->db->where('missing !=', 1);
    // $CI->db->where('exist !=', 0);
    $res = $CI->db->get()->result();
    return $res;
}

function get_all_sup() {
    $CI = &get_instance();
    $res = array();

    $CI->db->select('*')->from('product_sup_items');
    $CI->db->where('deleted !=', 1);
    $res = $CI->db->get()->result();
    return $res;
}

function get_shutter_count($deliver_id) {
    $CI = &get_instance();
    $CI->db->select('shutter_count')->from('booking_items_delivery')
            ->where('id', $deliver_id);
    return $CI->db->get()->row('shutter_count');
}

function is_camera($product_id) {
    $CI = &get_instance();
    $CI->db->select('category_id')->from('product_categories')
            ->where('product_id', $product_id);
    $categ_id = $CI->db->get()->row('category_id');
    if (!empty($categ_id)) {
        $CI->db->select('parent, is_camera')->from('categories')
                ->where('id', $categ_id);
        $res = $CI->db->get()->row();
        if ($res->parent == 0) {
            if ($res->is_camera == 1)
                return 1;
            else {
                return 0;
            }
        } else {
            $CI->db->select('is_camera')->from('categories')
                    ->where('id', $res->parent);
            $is_cam = $CI->db->get()->row('is_camera');
            if ($is_cam == 1)
                return 1;
            else
                return 0;
        }
    } else
        return 0;
}

function get_items_products() {
    $CI = &get_instance();
    $CI->db->select('*')->from('product_items');
    $CI->db->where('deleted !=', 1);
    $CI->db->where('sold !=', 1);
    // $CI->db->where('exist !=', 0);
    return $CI->db->get()->result();
}

function get_studio_technicans() {
    $CI = &get_instance();
    $CI->db->select('*')->from('products');
    $CI->db->where('deleted !=', 1);
    $CI->db->where('under_maintenance !=', 1);
    $CI->db->where('type ', 1);
    $CI->db->or_where('type ', 2);
    return $CI->db->get()->result();
}

function get_from_to($book_id, $product_id) {
    $CI = &get_instance();
    $CI->db->select('*')->from('booking_items');
    $CI->db->where('book_id', $book_id);
    $CI->db->where('product_id', $product_id);
    $res = $CI->db->get()->row();
    return !empty($res) ? $res : null;
}

function get_client_penalty() {
    $CI = &get_instance();
    $CI->db->select('damaged_items.*, p.n_en as pname')->from('damaged_items')->where('client_id', cid());
    $CI->db->join('products p', 'p.id = damaged_items.product_id');
    return $CI->db->get()->result();
}

function get_accounting($accounting_id) {
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('accounting_types');
    $CI->db->where('id', $accounting_id);
    return $CI->db->get()->row();
}

function get_usage_data($product_id) {
    //get total usage for this product
    $CI = &get_instance();
    $count_all = 0;
    $CI->db->select('*');
    $CI->db->from('product_items');
    $CI->db->where('product_id', $product_id);
    $CI->db->where('deleted !=', 1);
    $CI->db->where('sold !=', 1);
    $items = $CI->db->get()->result();
    $data['count'] = count($items);
    foreach ($items as $item) {
        $CI->db->select('*');
        $CI->db->from('booking_items_delivery');
        $CI->db->where('item_id', $item->id);
        $count_rented = $CI->db->count_all_results();
        $count_all += $count_rented;
    }
    $data['all_rented'] = $count_all;
    $data['pname'] = $CI->db->select('n_en')->from('products')->where('id', $product_id)->get()->row('n_en');
    return $data;
}

function get_clients() {
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('clients');
    $CI->db->where('deleted !=', 1);
    $CI->db->where('status', 1);
    return $CI->db->get()->result();
}

function get_total() {
    $CI = &get_instance();
    $CI->db->select('total');
    $CI->db->from('booking');
    $CI->db->where('client_id', cid());
    $CI->db->where('deleted !=', 1);
    $CI->db->where('canceled !=', 1);
    $CI->db->where('status !=', 0);
    $CI->db->order_by('id', "desc");
    $CI->db->limit(1);
    return $CI->db->get()->row('total');
}

function get_reviews_total($product_id) {
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('reviews');
    $CI->db->where('product_id', $product_id);
    $CI->db->where('deleted !=', 1);
    $count = $CI->db->count_all_results();
    return !empty($count) ? $count : 0;
}

function get_reviews($product_id) {
    $CI = &get_instance();
    $CI->db->select('r.client_id,r.product_id,r.description,r.rating,r.timestamp,c.n_en as client_name_en,c.n_ar as client_name_ar');
    $CI->db->from('reviews as r');
    $CI->db->where('r.product_id', $product_id);
    $CI->db->where('r.deleted !=', 1);
    $CI->db->join('clients c', 'c.id = r.client_id');
    $res = $CI->db->get()->result();
    return !empty($res) ? $res : null;
}

function get_related_products($product_id) {
    $CI = &get_instance();
    $result = array();
    $CI->db->select('category_id');
    $CI->db->from('product_categories');
    $CI->db->where('product_id', $product_id);
    $categ_id = $CI->db->get()->row('category_id');
    if (!empty($categ_id)) {
        $CI->db->select('product_id');
        $CI->db->from('product_categories');
        $CI->db->where('category_id', $categ_id);
        $product_id = $CI->db->get()->result();
        $arr = array_column($product_id, "product_id");
        $CI->db->select('*');
        $CI->db->from('products');
        $CI->db->where_in('id', $arr);
        $CI->db->where('deleted != ', 1);
        $CI->db->order_by('rand()');
        $CI->db->limit(5);
        $result = $CI->db->get()->result();
    }
    return $result;
}

function get_slider() {
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('products');
    $CI->db->where('deleted !=', 1);
    $CI->db->where('slider', 1);
    //$CI->db->limit(3);
    return $CI->db->get()->result();
}

function get_top_seller() {
    $CI = &get_instance();
    $i = 0;
    $max = array();
    $count = array();
    $data = array();
    $CI->db->select('id');
    $CI->db->from('products');
    $CI->db->where('deleted !=', 1);
    $product_id = $CI->db->get()->result();
    if (!empty($product_id)) {
        foreach ($product_id as $one) {
            $CI->db->select('*');
            $CI->db->from('booking_items');
            $CI->db->where('product_id', $one->id);
            $c_res = $CI->db->count_all_results();
            if ($c_res != 0) {
                $count[$i]['count'] = $c_res;
                $count[$i]['id'] = $one->id;
                $i++;
            }
        }
        $c = array_column($count, 'count');
        $id = array_column($count, 'id');
        $r = 0;
        $p = 0;
        for ($n = 0; $n < count($c); $n++) {
            $max = max($c);
            $r = 0;
            foreach ($c as $a) {
                if ($a == $max) {
                    $data[$p] = $id[$r];
                    $p++;
                    unset($c[$r]);
                    break;
                }
                $r++;
            }
        }
    }
    if (!empty($data)) {
        return get_seller_product($data);
    } else {
        return $data;
    }
}

function get_seller_product($data) {
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('products');
    $CI->db->where_in('id', $data);
    return $CI->db->get()->result();
}

function get_top_rated() {
    $CI = &get_instance();
    $CI->db->select('reviews.*, p.*');
    $CI->db->from('reviews');
    $CI->db->where('reviews.deleted !=', 1);
    $CI->db->where('reviews.rating >=', 3);
    $CI->db->join('products p', 'p.id = reviews.product_id');
    return $CI->db->get()->result();
}

function get_table_info($table, $id = 0) {
    $CI = &get_instance();
    $CI->db->select("*");
    $CI->db->from($table);
    $CI->db->where("deleted !=", 1);
    $CI->db->where("id", $id);
    return $CI->db->get()->row();
}

function get_sup_delivery($prep_id, $product_supID, $book_id) {
    $CI = &get_instance();
    $CI->db->select('id');
    $CI->db->from('items_delivery_temp');
    $CI->db->where('prep_id', $prep_id);
    $CI->db->limit(1);
    $deliver_id = $CI->db->get()->row('id');
    $CI->db->where('delivery_id', $deliver_id);
    $CI->db->where('book_id', $book_id);
    $CI->db->where('product_sup_id', $product_supID);
    return $CI->db->get('sup_items_temp')->row();
}

function get_delivery_sup_items($deliver_id) {
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('booking_sup_items_delivery');
    $CI->db->where('delivery_id', $deliver_id);
    $CI->db->where('sup_id !=', null);
    $CI->db->where('deleted !=', 1);
    $res = $CI->db->get()->result();
    return $res;
}

function delivery_sup_items_count($book_id) {
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('booking_sup_items_delivery');
    $CI->db->where('book_id', $book_id);
    $CI->db->where('sup_id !=', null);
    $CI->db->where('deleted !=', 1);
    $res = $CI->db->get()->result();
    $i = 0;
    $data = array();
    foreach ($res as $one) {
        if ($i == 0) {

            $CI->db->select('*');
            $CI->db->from('booking_sup_items_delivery');
            $CI->db->where('delivery_id', $one->delivery_id);
            $CI->db->where('book_id', $one->book_id);
            $CI->db->where('product_sup_id', $one->product_sup_id);
            $CI->db->where('deleted !=', 1);
            $CI->db->where('sup_id !=', null);
            $count = $CI->db->count_all_results();
            $data[$i]['id'] = $one->id;
            $data[$i]['deliv_id'] = $one->delivery_id;
            $data[$i]['sup_id'] = $one->sup_id;
            $data[$i]['book_id'] = $one->book_id;
            $data[$i]['productSupID'] = $one->product_sup_id;
            $data[$i]['count'] = $count;
            $i++;
        }
        if (($one->delivery_id == $data[$i - 1]['deliv_id']) && ($one->book_id == $data[$i - 1]['book_id']) &&
                ($one->product_sup_id == $data[$i - 1]['productSupID'])) {
            continue;
        } else {
            $CI->db->select('*');
            $CI->db->from('booking_sup_items_delivery');
            $CI->db->where('delivery_id', $one->delivery_id);
            $CI->db->where('book_id', $one->book_id);
            $CI->db->where('product_sup_id', $one->product_sup_id);
            $CI->db->where('deleted !=', 1);
            $CI->db->where('sup_id !=', null);
            $count = $CI->db->count_all_results();
            $data[$i]['id'] = $one->id;
            $data[$i]['deliv_id'] = $one->delivery_id;
            $data[$i]['sup_id'] = $one->sup_id;
            $data[$i]['book_id'] = $one->book_id;
            $data[$i]['productSupID'] = $one->product_sup_id;
            $data[$i]['count'] = $count;
            $i++;
        }
    }
    return $data;
}

function get_branch_by_id($id) {
    $CI = &get_instance();
    $CI->db->select('*')->from('locations')->where('id', $id)->where('deleted !=', 1);
    return $CI->db->get()->row();
}

function get_product_item_by_id($id) {
    $CI = &get_instance();
    $CI->db->select('*')->from('product_items')->where('id', $id)->where('deleted !=', 1)->where('sold !=', 1);
    return $CI->db->get()->row();
}

function get_rating($product_id) {
    $CI = &get_instance();
    $rating = 0;
    $CI->db->select('rating');
    $CI->db->from('reviews');
    $CI->db->where('client_id', cid());
    $CI->db->where('deleted !=', 1);
    $CI->db->where('product_id', $product_id);
    $CI->db->limit(1);
    $rating = $CI->db->get()->row('rating');
    return !empty($rating) ? $rating : 0;
}

function get_cart_voucher($client_id) {
    $CI = &get_instance();
    $CI->db->select("cart.*");
    $CI->db->select("vouchers.amount");
    $CI->db->from('cart');
    $CI->db->join('vouchers', 'vouchers.id = cart.voucher_id');
    $CI->db->where('cart.client_id', $client_id);
    $CI->db->where('cart.parent', 0);
    return $CI->db->get()->row();
}

function get_voucher($voucher) {
    $CI = &get_instance();
    $CI->db->select("vouchers.*");
    $CI->db->from('vouchers');
    $CI->db->where('voucher_number', $voucher);
    $CI->db->where('deleted !=', 1);
    return $CI->db->get()->row();
}

function build_categories_menu($parent = NULL) {
    $CI = &get_instance();
    $query = $CI->db->select('*')->from('categories')->where('parent', $parent)->where('deleted !=', 1)->get()->result();
    $html = "";
    if (!empty($query)) {
        $html .= "<ul style='width:100% !important;'>";
        foreach ($query as $itemId) {
            $sub_child = $CI->db->select('*')->from('categories')->where('parent', $itemId->id)->where('deleted !=', 1)->get()->result();
            if (empty($sub_child)) {
                $html .= "<li><a href='" . base_url() . "Shop/product/" . $itemId->id . "'>" . $itemId->{'n_' . lc()} . "</a></li>";
            } else {
                $html .= " <li class='parent'>"
                        . "<a href='#'>" . $itemId->{'n_' . lc()} . "</a>"
                        . "<ul class='customeField' style='width:100% !important;'>"
                        . build_categories_menu($itemId->id)
                        . "</ul>";
            }
        }
        $html .= "</ul>";
    }
    return $html;
}
