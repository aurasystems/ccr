<?php

function get_level_functions() {
    $functions = [
        "access_levels" => ['a' => 0, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 5, 'd' => 0, 'w' => 5],
        "users" => ['a' => 0, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 5, 'd' => 0, 'w' => 5],
        "settings" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 0, 'e' => 5, 'd' => 5, 'w' => 5],
        "email_settings" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 0, 'e' => 5, 'd' => 5, 'w' => 5],
        "sms_settings" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 0, 'e' => 5, 'd' => 5, 'w' => 5],
        "newsletters_settings" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 0, 'e' => 5, 'd' => 5, 'w' => 5],
        "clients" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 0, 'd' => 0, 'w' => 5],
        "locations" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 5, 'd' => 0, 'w' => 5],
        "brands" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 5, 'd' => 0, 'w' => 5],
        "categories" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 5, 'd' => 0, 'w' => 5],
        "decline_reasons" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 5, 'd' => 0, 'w' => 5],
        "check_types" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 5, 'd' => 0, 'w' => 5],
        "products" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 5, 'd' => 0, 'w' => 5],
        "studios" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 5, 'd' => 0, 'w' => 5],
        "technicians_types" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 5, 'd' => 0, 'w' => 5],
        "stock" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 5, 'd' => 0, 'w' => 5],
        "technicians" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 5, 'd' => 0, 'w' => 5],
        "import" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 5, 'e' => 5, 'd' => 5, 'w' => 5],
        "payroll" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 5, 'e' => 5, 'd' => 5, 'w' => 5],
        "failed_msgs_rpt" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 5, 'e' => 5, 'd' => 5, 'w' => 5],
        "equip_rep" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 5, 'e' => 5, 'd' => 5, 'w' => 5],
        "acc_rep" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 5, 'e' => 5, 'd' => 5, 'w' => 5],
        "ecomm_rep" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 5, 'e' => 5, 'd' => 5, 'w' => 5],
        "booking" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 0, 'd' => 0, 'w' => 5],
        "equipment_test" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 0, 'e' => 5, 'd' => 5, 'w' => 5],
        "booking_preparation" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 0, 'e' => 5, 'd' => 5, 'w' => 5],
        "maintenance" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 5, 'd' => 0, 'w' => 5],
        "missing_items" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 0, 'e' => 5, 'd' => 5, 'w' => 5],
        "damaged_items" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 0, 'e' => 5, 'd' => 5, 'w' => 5],
        "wallet" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 0, 'e' => 5, 'd' => 5, 'w' => 5],
        "blog" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 0, 'd' => 0, 'w' => 5],
        "blog_categories" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 5, 'd' => 0, 'w' => 5],
        "accounting" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 5, 'd' => 0, 'w' => 5],
        "questions_types" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 5, 'd' => 0, 'w' => 5],
        "surveys" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 0, 'd' => 0, 'w' => 5],
        "newsletters" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 0, 'd' => 0, 'w' => 5],
        "vouchers" => ['a' => 5, 'v' => 0, 'c' => 0, 'u' => 0, 'e' => 0, 'd' => 0, 'w' => 5],
        "about_page" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 0, 'e' => 5, 'd' => 5, 'w' => 5],
        "privacy_page" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 0, 'e' => 5, 'd' => 5, 'w' => 5],
        "how_to_rent_page" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 0, 'e' => 5, 'd' => 5, 'w' => 5],
        "missing_acc" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 0, 'e' => 5, 'd' => 5, 'w' => 5],
        "damaged_acc" => ['a' => 5, 'v' => 0, 'c' => 5, 'u' => 0, 'e' => 5, 'd' => 5, 'w' => 5],

    ];
    return $functions;
}

function check_p($module, $permission) {
    $CI = &get_instance();
    $permissions = $CI->session->userdata("permissions");
    $p = !empty($permissions[$module][$permission]) ? $permissions[$module][$permission] : NULL;
    if (!$p || $p == 200) {
        redirect(base_url("admin/General/permission_error"));
    }
}

function get_p($module, $permission = 'v') {
    $CI = &get_instance();
    $permissions = $CI->session->userdata("permissions");
    $p = !empty($permissions[$module][$permission]) ? $permissions[$module][$permission] : NULL;
    return !$p || ($p && $p != 200) ? $p : 0;
}

function or_p($arr) {
    $CI = &get_instance();
    $permissions = $CI->session->userdata("permissions");
    $p = FALSE;
    foreach ($arr as $module => $permission) {
        $perm = !empty($permissions[$module][$permission]) ? $permissions[$module][$permission] : NULL;
        if ($perm && $perm != 200) {
            $p = TRUE;
            break;
        }
    }
    if (!$p) {
        redirect(base_url("admin/General/permission_error"));
    }
}

function or_pb($arr = [], $handle = FALSE, $md = 'v') {
    $CI = &get_instance();
    $permissions = $CI->session->userdata("permissions");
    $p = FALSE;
    foreach ($arr as $module) {
        $perm = !empty($permissions[$module][$md]) ? $permissions[$module][$md] : NULL;
        if ($perm && $perm != 200) {
            $p = TRUE;
            break;
        }
    }
    if ($handle && !$p) {
        redirect(base_url("admin/General/permission_error"));
    }
    return $p;
}

function and_p($arr) {
    $CI = &get_instance();
    $permissions = $CI->session->userdata("permissions");
    $p = true;
    foreach ($arr as $module => $permission) {
        $perm = !empty($permissions[$module][$permission]) ? $permissions[$module][$permission] : NULL;
        if (!$perm || $perm == 200) {
            $p = FALSE;
            break;
        }
    }
    if (!$p) {
        redirect(base_url("admin/General/permission_error"));
    }
}

function get_companies_access() {
    $CI = &get_instance();
    return $CI->session->userdata("companies");
}

function get_branches_access() {
    $CI = &get_instance();
    return $CI->session->userdata("branches");
}

function load_permissions($level_id) {
    $CI = &get_instance();
    $perm = $CI->db->where("level_id", $level_id)->get("level_functions")->row();
    $perm = !empty($perm->permissions) ? unserialize($perm->permissions) : [];
    $functions = get_level_functions();
    $permissions = [];
    foreach ($functions as $function => $pr) {
        $p = [
            'a' => !empty($perm[$function]['a']) ? (int) $perm[$function]['a'] : (!empty($pr['a']) ? $pr['a'] : 0),
            'v' => !empty($perm[$function]['v']) ? (int) $perm[$function]['v'] : (!empty($pr['v']) ? $pr['v'] : 0),
            'c' => !empty($perm[$function]['c']) ? (int) $perm[$function]['c'] : (!empty($pr['c']) ? $pr['c'] : 0),
            'u' => !empty($perm[$function]['u']) ? (int) $perm[$function]['u'] : (!empty($pr['u']) ? $pr['u'] : 0),
            'e' => !empty($perm[$function]['e']) ? (int) $perm[$function]['e'] : (!empty($pr['e']) ? $pr['e'] : 0),
            'd' => !empty($perm[$function]['d']) ? (int) $perm[$function]['d'] : (!empty($pr['d']) ? $pr['d'] : 0),
            'w' => !empty($perm[$function]['w']) ? (int) $perm[$function]['w'] : (!empty($pr['w']) ? $pr['w'] : 0),
        ];
        $permissions[$function]['a'] = ($p['a'] && $p['a'] != 5) ? $p['a'] : (!$p['a'] ? 0 : 200);
        $permissions[$function]['v'] = ($p['v'] && $p['v'] != 5) ? $p['v'] : (!$p['v'] ? 0 : 200);
        $permissions[$function]['c'] = ($p['c'] && $p['c'] != 5) ? $p['c'] : (!$p['c'] ? 0 : 200);
        $permissions[$function]['u'] = ($p['u'] && $p['u'] != 5) ? $p['u'] : (!$p['u'] ? 0 : 200);
        $permissions[$function]['e'] = ($p['e'] && $p['e'] != 5) ? $p['e'] : (!$p['e'] ? 0 : 200);
        $permissions[$function]['d'] = ($p['d'] && $p['d'] != 5) ? $p['d'] : (!$p['d'] ? 0 : 200);
        $permissions[$function]['w'] = ($p['w'] && $p['w'] != 5) ? $p['w'] : (!$p['w'] ? 0 : 200);
    }
    $CI->session->set_userdata("permissions", $permissions);
}

function load_locations($user_id) {
    $CI = &get_instance();
    $locations = $CI->db->where("user_id", $user_id)->get("user_locations")->result();
    $locations = $locations ? ids($locations, 'location_id') : [0];
    $CI->session->set_userdata("locations", $locations);
}
