<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front();
        date_default_timezone_set('Africa/Cairo');
        $this->load->model('Account_model');
        $this->load->model('admin/Booking_model');
    }

    function index() {
        
    }

    function profile() {
        if ((cid() != "")) {
            $id = cid();
            $data['title'] = lang('my_profile');
            $data['one'] = $this->Global_model->get_data_by_id('clients', $id);
            $this->load->view("front/planet/pages/profile", $data);
        } else {
            redirect('Home');
        }
    }

    function update_info() {
        if ((cid() != "")) {
            $id = cid();
            $one = $this->Global_model->get_data_by_id('clients', $id);
            $this->load->library('form_validation');
            $this->form_validation->set_rules("n_en", lang("name_en"), "trim|required");
            $this->form_validation->set_rules("n_ar", lang("name_ar"), "trim|required");
            $this->form_validation->set_rules("phone", lang("phone"), "trim|required");
            if ($this->input->post('national_id') != $one->national_id) {
                $this->form_validation->set_rules("national_id", lang("national_id"), "trim|required|min_length[14]|max_length[14]|is_unique[clients.national_id]");
            }
            if ($this->input->post('email') != $one->email) {
                $this->form_validation->set_rules('email', lang("email"), 'trim|required|valid_email|is_unique[clients.email]');
            }
            $this->form_validation->set_rules('password', lang("password"), 'trim|matches[confirm_password]|min_length[4]|max_length[32]');
            $this->form_validation->set_rules('confirm_password', lang("confirm_password"), 'trim|matches[password]');
            if ($this->form_validation->run() == FALSE) {
                $this->profile();
            } else {
                $data = [
                    'n_en' => $this->input->post('n_en'),
                    'n_ar' => $this->input->post('n_ar'),
                    'phone' => $this->input->post('phone'),
                    'national_id' => $this->input->post('national_id'),
                    'email' => $this->input->post('email'),
                    'address_en' => $this->input->post('address_en')
                ];
                if ($this->input->post('password') != "" && $this->input->post('confirm_password') != "") {
                    $data['password'] = do_hash($this->input->post('password'), 'md5');
                }
                $this->Global_model->global_update("clients", $id, $data);
                $client = $this->Global_model->get_data_by_id('clients', $id);
                if ($client) {
                    //add user data to session
                    $new_data = [
                        'client_id' => $client->id,
                        'client_email' => $client->email,
                        'client_name_en' => $client->n_en,
                        'client_name_ar' => $client->n_ar,
                        'logged_in' => TRUE
                    ];
                    $this->session->set_userdata($new_data);
                }
                $this->session->set_flashdata("success", lang("data_submitted_successfully"));
                redirect(base_url("Account/profile"));
            }
        } else {
            redirect('Home');
        }
    }

    function logout() {
        if ((cid() != "")) {
            $new_data = [
                'logged_in' => FALSE,
                'client_id' => '',
                'client_email' => '',
                'client_name_en' => '',
                'client_name_ar' => ''
            ];
            $this->session->unset_userdata($new_data);
            $this->session->sess_destroy();
            redirect('Home');
        } else {
            redirect('Home');
        }
    }

    function booking_cancellation($booking_id = NULL) {
        if ((cid() != "")) {
            $now = date("Y-m-d H:i:s");
            $cancel = FALSE;
            $client_id = cid();
            if (!empty($booking_id)) {
                $client = $this->Global_model->get_data_by_id('clients', $client_id);
                $booking = $this->Global_model->get_data_by_id('booking', $booking_id);
                if (!empty($booking)) {
                    $settings = settings('all', 3);
                    if (!empty($settings)) {
                        $cancellation_time_limit = $settings->free_cancellation_time_limit;
                        if (!empty($cancellation_time_limit)) {
                            // Get booking datetime and increase it by this limit, if result bigger than now then continue to cancellation
                            $result = date("Y-m-d H:i:s", strtotime("+" . $cancellation_time_limit . " minutes", $booking->timestamp));
                            if ($result >= $now) {
                                $cancel = TRUE;
                            }
                        } else {
                            $cancel = TRUE;
                        }
                    } else {
                        $cancel = TRUE;
                    }
                    if ($cancel == TRUE) {
                        $booking_items = $this->Booking_model->booking_items($booking_id);
                        if (!empty($booking_items)) {
                            foreach ($booking_items as $item) {
                                $booking_waiting = $this->Booking_model->get_booking_waiting($item->product_id);
                                if (!empty($booking_waiting)) {
                                    $notification = $settings->notification_type;
                                    $email = $booking_waiting->email;
                                    $phone = $booking_waiting->phone;
                                    $msg = lang('your_request') . ' (' . $booking_waiting->pname . ') ' . lang('is_available_now');
                                    if ($notification == 1 || $notification == 3) {
                                        // send email notification
                                        $data = [
                                            'to' => $email,
                                            'subject' => lang("product_request"),
                                            'message' => $msg
                                        ];
                                        send_email($data);
                                    }
                                    if ($notification == 2 || $notification == 3) {
                                        // send sms notification
                                        $sent = send_sms($phone, $msg);
                                        if (!$sent) {
                                            // insert into failed msgs
                                            $failed_dt = [
                                                'client_id' => $booking_waiting->client_id,
                                                'phone' => $phone,
                                                'msg' => $msg
                                            ];
                                            $this->Global_model->global_insert('failed_messages', $failed_dt);
                                        }
                                    }
                                    $this->Global_model->global_update('booking_waiting_list', $booking_waiting->id, ['notified' => 1]);
                                }
                            }
                        }
                        $this->Global_model->global_update('booking', $booking_id, ["deleted" => 1]);
                        $this->Booking_model->delete_booking_items('booking_items', $booking_id);
                        $this->Booking_model->delete_booking_items('booking_items_delivery', $booking_id);
                        $this->Booking_model->delete_booking_items('booking_items_preparation', $booking_id);
                        $this->Booking_model->delete_booking_items('booking_items_return', $booking_id);
                        $this->session->set_flashdata("success", lang("booking_canceled_successfully"));
                        // later it will be redirected to client bookings
                        redirect(base_url("Account/profile"));
                    } else {
                        $this->session->set_flashdata("error", lang("no_booking_cancel"));
                        // later it will be redirected to client bookings
                        redirect(base_url("Account/profile"));
                    }
                } else {
                    redirect('Home');
                }
            } else {
                redirect('Home');
            }
        } else {
            redirect('Home');
        }
    }

    function rent_history() {
        if ((cid() != "")) {
            $id = cid();
            $data['title'] = lang('rent_history');
            $booking_data = array();
            $booking_data = $this->Account_model->get_booking($id);
            $i = 0;
            $cancel[$i] = FALSE;
            foreach ($booking_data as $one) {
                $now = date("Y-m-d H:i:s");
                $cancel[$i] = FALSE;
                $client_id = cid();
                $booking_id = $one->book_id;
                if (!empty($booking_id)) {
                    $client = $this->Global_model->get_data_by_id('clients', $client_id);
                    $booking = $this->Global_model->get_data_by_id('booking', $booking_id);
                    if (!empty($booking)) {
                        $settings = settings('all', 3);
                        if (!empty($settings)) {
                            $cancellation_time_limit = $settings->free_cancellation_time_limit;
                            if (!empty($cancellation_time_limit)) {
                                // Get booking datetime and increase it by this limit, if result bigger than now then continue to cancellation
                                $result = date("Y-m-d H:i:s", strtotime("+" . $cancellation_time_limit . " minutes", strtotime($booking->timestamp)));
                                if ($result >= $now) {
                                    $cancel[$i] = TRUE;
                                }
                            } else {
                                $cancel[$i] = TRUE;
                            }
                        } else {
                            $cancel[$i] = TRUE;
                        }
                    }
                }
                if ($cancel[$i] != TRUE)
                    $cancel[$i] = FALSE;
                $i++;
            }
            $data['booking'] = $booking_data;
            $data['can_cancel'] = $cancel;
            $this->load->view("front/planet/pages/rent_history", $data);
        } else {
            redirect('Home');
        }
    }

    function cancel_booking($booking_id) {
        $booking_items = $this->Booking_model->booking_items($booking_id);
        if (!empty($booking_items)) {
            foreach ($booking_items as $item) {
                $booking_waiting = $this->Booking_model->get_booking_waiting($item->product_id);
                if (!empty($booking_waiting)) {
                    $notification = $settings->notification_type;
                    $email = $booking_waiting->email;
                    $phone = $booking_waiting->phone;
                    $msg = lang('your_request') . ' (' . $booking_waiting->pname . ') ' . lang('is_available_now');
                    if ($notification == 1 || $notification == 3) {
                        // send email notification
                        $data = [
                            'to' => $email,
                            'subject' => lang("product_request"),
                            'message' => $msg
                        ];
                        send_email($data);
                    }
                    if ($notification == 2 || $notification == 3) {
                        // send sms notification
                        $sent = send_sms($phone, $msg);
                        if (!$sent) {
                            // insert into failed msgs
                            $failed_dt = [
                                'client_id' => $booking_waiting->client_id,
                                'phone' => $phone,
                                'msg' => $msg
                            ];
                            $this->Global_model->global_insert('failed_messages', $failed_dt);
                        }
                    }
                    $this->Global_model->global_update('booking_waiting_list', $booking_waiting->id, ['notified' => 1]);
                }
            }
        }
        $this->Global_model->global_update('booking', $booking_id, ["deleted" => 1]);
        $this->Booking_model->delete_booking_items('booking_items', $booking_id);
        $this->Booking_model->delete_booking_items('booking_items_delivery', $booking_id, true);
        $this->Booking_model->delete_booking_items('booking_items_preparation', $booking_id);
        $this->Booking_model->delete_booking_items('booking_items_return', $booking_id);
        $this->session->set_flashdata("success", lang("booking_canceled_successfully"));
        // later it will be redirected to client bookings
        redirect(base_url("Account/rent_history"));
    }

}
