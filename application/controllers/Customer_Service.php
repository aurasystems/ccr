<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer_Service extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
    }

    public function index()
    {
        $data['title'] = lang('Customer_Service');
        $this->load->view("front/pages/customer_service", $data);
    }



}
