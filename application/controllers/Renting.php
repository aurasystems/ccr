<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Renting extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
        $this->load->model('admin/Renting_model');
    }

    function index() {
        $settings = settings('all', 3);
        if (!empty($settings) && $settings->show_renting) {
            $data['title'] = lang('how_to_rent');
            $data['renting'] = $this->Renting_model->get_content();
            $this->load->view("front/planet/pages/how_to_rent", $data);
        } else {
            redirect(base_url('Home'));
        }
    }

}
