<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Terms_condition extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
    }

    public function index()
    {
        $data['title'] = lang('Terms_condition');
        $this->load->view("front/pages/terms_condition", $data);
    }



}
