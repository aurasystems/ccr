<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
        $this->load->model('Home_model');
        $this->load->library('user_agent');
        $this->load->library('pagination');
    }

    function index() {
        $data['title'] = lang('home');
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $config = array();
        $config["base_url"] = base_url() . "Home/index";
        $config["total_rows"] = $this->Home_model->count_featured_products();
        $config['cur_tag_open'] = '<a href="" class="active">';
        $config['cur_tag_close'] = '</a>';
        $config["per_page"] = 12;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $data["products"] = $this->Home_model->get_featured_products($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['cats'] = build_categories_menu(0);
        $data['settings'] = settings('all', 3);
        $data['sliders'] = get_slider();
        $data['brands'] = get_brands();
        $data['active'] = 'home';
        $this->load->view("front/planet/pages/home", $data);
    }

    public function category_menu() {
        // Select all entries from the menu table
        $query = $this->db->query("select * from categories order by parent");

        // Create a multidimensional array to contain a list of items and parents
        $cat = array(
            'items' => array(),
            'parents' => array()
        );
        // Builds the array lists with data from the menu table
        foreach ($query->result() as $cats) {
            // Creates entry into items array with current menu item id ie. $menu['items'][1]
            $cat['items'][$cats->id] = $cats;
            // Creates entry into parents array. Parents array contains a list of all items with children
            $cat['parents'][$cats->parent][] = $cats->id;
        }
        if ($cat) {
            $result = $this->build_category_menu(0, $cat);
            return $result;
        } else {
            return FALSE;
        }
    }

    // Menu builder function, parentId 0 is the root
    function build_category_menu($parent, $menu) {
        $html = "";
        $pointer_events = '';
        if (isset($menu['parents'][$parent])) {
            $html .= "<ul style='padding:0px;'>";
            foreach ($menu['parents'][$parent] as $itemId) {
                $categ_products = get_cat_product($menu['items'][$itemId]->id, false);
                if (empty($categ_products))
                    $pointer_events = "none";
                if (!isset($menu['parents'][$itemId])) {
                    $items = get_brands($menu['items'][$itemId]->id);
                    if (!empty($items)) {
                        $html .= "<li class='menu-item menu-item-has-children'><a style='pointer-events:" . $pointer_events . "' href='" . base_url() . "Shop/product/" . $menu['items'][$itemId]->id . "'>" . $menu['items'][$itemId]->{'n_' . lc()} . "</a>";
                        $html .= "<ul>";
                        foreach ($items as $item) {
                            $html .= "<li ><a class='parent' href='" . base_url() . "Shop/brand/" . $item->id . "'>" . $item->{'n_' . lc()} . "</a></li>";
                        }
                        $html .= "</ul>
                            </li>";
                    } else {
                        $html .= "<li class='menu-item menu-item-has-children'><a style='pointer-events:" . $pointer_events . "' href='" . base_url() . "Shop/product/" . $menu['items'][$itemId]->id . "'>" . $menu['items'][$itemId]->{'n_' . lc()} . "</a></li>";
                    }
                }
                if (isset($menu['parents'][$itemId])) {
                    $html .= " <li class='menu-item menu-item-has-children'><a style='pointer-events:" . $pointer_events . "' class='parent' href='" . base_url() . "Shop/product/" . $menu['items'][$itemId]->id . "'>" . $menu['items'][$itemId]->{'n_' . lc()} . "</a>";
                    $html .= $this->build_category_menu($itemId, $menu);
                    $html .= "</li>";
                }
            }
            $html .= "</ul>";
        }
        return $html;
    }

    function build_categories_menu($parent) {
        $query = $this->db->select('*')->from('categories')->where('parent', $parent)->where('deleted !=', 1)->get()->result();
        $html = "";
        if (!empty($query)) {
            $html .= "<ul style='width:100% !important;'>";
            foreach ($query as $itemId) {
                $sub_child = $this->db->select('*')->from('categories')->where('parent', $itemId->id)->where('deleted !=', 1)->get()->result();
                if (empty($sub_child)) {
                    $html .= "<li><a href='" . base_url() . "Shop/item_details/" . $itemId->id . "'>" . $itemId->{'n_' . lc()} . "</a></li>";
                } else {
                    $html .= " <li class='parent'>"
                            . "<a href='#'>" . $itemId->{'n_' . lc()} . "</a>"
                            . "<ul class='customeField' style='width:100% !important;'>"
                            . $this->build_categories_menu($itemId->id)
                            . "</ul>";
                }
            }
            $html .= "</ul>";
        }
        return $html;
    }

}
