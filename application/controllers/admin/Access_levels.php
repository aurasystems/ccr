<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Access_levels extends CI_Controller {

    private $code = NULL;
    private $idf = NULL;
    private $sh = NULL;

    public function __construct() {
        parent::__construct();
        construct_init();

        $this->code = 2;
        $this->idf = get_sys_idf($this->code);
        $this->sh = 'al';
        $this->load->model('admin/'.ucfirst($this->idf) . '_model');
        set_active($this->idf);
    }

    function index() {
        check_p($this->idf, "v");
        $data['title'] = lang('access_levels');
        $data['access_levels'] = get_data_list(['c' => $this->code]);
        $this->load->view("admin/pages/$this->idf/index", $data);
    }

    function add() {
        check_p($this->idf, "c");
        $this->form_validation->set_rules("n_en", lang("name_in") . lang("lang_en"), "trim|required")
                ->set_rules("n_ar", lang("name_in") . lang("lang_ar"), "trim|required")
                ->set_rules("private", lang("private_access"), "trim");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('add_level');
            $this->load->view('admin/pages/access_levels/add', $data);
        } else {
            $data = [
                "n_en" => $this->input->post("n_en"),
                "n_ar" => $this->input->post("n_ar"),
                "private" => $this->input->post("private") ? 1 : 0,
                "added_by" => uid()
            ];
            $level_id = $this->Global_model->global_insert("access_levels", $data);
            $this->Global_model->global_insert("level_functions", ["level_id" => $level_id]);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/Access_levels"));
        }
    }

    function edit($id = NULL) {
        check_p($this->idf, "u");
        $get = get_single_date(['c' => $this->code, 'i' => $id, 'chk' => TRUE]);
        $this->form_validation->set_rules("n_en", lang("name_in") . lang("lang_en"), "trim|required");
        $this->form_validation->set_rules("n_ar", lang("name_in") . lang("lang_ar"), "trim|required");
        $this->form_validation->set_rules("private", lang("private_access"), "trim");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('edit_level');
            $data['level'] = $get;
            $this->load->view('admin/pages/access_levels/edit', $data);
        } else {
            $data = [
                "n_en" => $this->input->post("n_en"),
                "n_ar" => $this->input->post("n_ar"),
                "private" => $this->input->post("private") ? 1 : 0
            ];
            $this->Global_model->global_update("access_levels", $id, $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/Access_levels"));
        }
    }

    function delete($id = NULL) {
        check_p($this->idf, "d");
        $level = get_single_date(['c' => $this->code, 'i' => $id, 'chk' => TRUE]);
        $exists = $this->Access_levels_model->check_level_users($level->id);
        if ($exists) {
            $this->session->set_flashdata("error", lang("users_assigned_level"));
            redirect(base_url("admin/Access_levels"));
        }
        $this->Access_levels_model->delete_functions($level->id);
        $this->Global_model->global_delete("access_levels", $level->id);
        $this->session->set_flashdata("success", lang("data_deleted_successfully"));
        redirect(base_url("admin/Access_levels"));
    }

     function permissions($level_id = NULL) {
       check_p($this->idf, "v");
       $level = get_single_date(['c' => 2, 'i' => $level_id, 'chk' => TRUE]);

       $perms = $this->Access_levels_model->get_level_permissions($level_id);
       check_param($perms, "resource");
       $permissions = [];
       $functions = get_level_functions();
       $perm = !empty($perms->permissions) ? unserialize($perms->permissions) : [];
       foreach ($functions as $function => $pr) {
           $p = [
               'a' => !empty($perm[$function]['a']) ? (int) $perm[$function]['a'] : (!empty($pr['a']) ? $pr['a'] : 0),
               'v' => !empty($perm[$function]['v']) ? (int) $perm[$function]['v'] : (!empty($pr['v']) ? $pr['v'] : 0),
               'c' => !empty($perm[$function]['c']) ? (int) $perm[$function]['c'] : (!empty($pr['c']) ? $pr['c'] : 0),
               'u' => !empty($perm[$function]['u']) ? (int) $perm[$function]['u'] : (!empty($pr['u']) ? $pr['u'] : 0),
               'e' => !empty($perm[$function]['e']) ? (int) $perm[$function]['e'] : (!empty($pr['e']) ? $pr['e'] : 0),
               'd' => !empty($perm[$function]['d']) ? (int) $perm[$function]['d'] : (!empty($pr['d']) ? $pr['d'] : 0),
               'w' => !empty($perm[$function]['w']) ? (int) $perm[$function]['w'] : (!empty($pr['w']) ? $pr['w'] : 0),
           ];
           $permissions[$function]['a'] = ($p['a'] && $p['a'] != 5) ? $p['a'] : (!$p['a'] ? 0 : 200);
           $permissions[$function]['v'] = ($p['v'] && $p['v'] != 5) ? $p['v'] : (!$p['v'] ? 0 : 200);
           $permissions[$function]['c'] = ($p['c'] && $p['c'] != 5) ? $p['c'] : (!$p['c'] ? 0 : 200);
           $permissions[$function]['u'] = ($p['u'] && $p['u'] != 5) ? $p['u'] : (!$p['u'] ? 0 : 200);
           $permissions[$function]['e'] = ($p['e'] && $p['e'] != 5) ? $p['e'] : (!$p['e'] ? 0 : 200);
           $permissions[$function]['d'] = ($p['d'] && $p['d'] != 5) ? $p['d'] : (!$p['d'] ? 0 : 200);
           $permissions[$function]['w'] = ($p['w'] && $p['w'] != 5) ? $p['w'] : (!$p['w'] ? 0 : 200);
       }
       
       if ($_SERVER["REQUEST_METHOD"] == "POST") {
           check_p("access_levels", "u");
           $updates = [];
           foreach ($permissions as $module => $arr) {
               $updates[$module] = [];
               foreach ($arr as $p => $v) {
                   if ($this->input->post($module . "_" . $p)) {
                        $updates[$module][$p] = (int) $this->input->post($module . "_" . $p);
                    } else {
                        $updates[$module][$p] = 0;
                    }
                }
            }
            $this->Access_levels_model->update_permissions($level_id, $updates);
            if ($this->session->user_level_id == $level_id) {
                load_permissions($level_id);
            }
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/Access_levels"));
        } else {
            $data['title'] = lang("permissions");
            $data['level_name'] = $perms->level_name;
            $data['permissions'] = $permissions;
            $this->load->view('admin/pages/access_levels/permissions', $data);
        }
    }

}
