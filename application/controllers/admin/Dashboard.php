<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->session->set_userdata("sidebar_active", "dashboard");
    }

    function index() {
        if (uid() != '') {
            $data = ['title' => lang('lang_dashboard')];
            $this->load->view('admin/pages/dashboard', $data);
        } else {
            $data = ['title' => lang('lang_sign_in')];
            $this->load->view("admin/pages/login", $data);
        }
    }

    public function account_settings() {
        $id = uid();
        check_param($id);
        $one = $this->Global_model->get_data_by_id("users", $id);
        check_param($one, "resource");
        if ($this->input->post('email') != $one->email) {
            $this->form_validation->set_rules('email', lang("email"), 'trim|required|valid_email|is_unique[users.email]');
        }
        $this->form_validation->set_rules('password', lang("password"), 'trim|matches[password_confirm]|min_length[4]|max_length[32]');
        $this->form_validation->set_rules('password_confirm', lang("password_confirm"), 'trim|matches[password]');
        if (!$this->form_validation->run()) {
            $data['title'] = lang('lang_account_settings');
            $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
            $data['one'] = $one;
            $this->load->view('admin/pages/profile', $data);
        } else {
            $data = ['email' => $this->input->post('email') ? $this->input->post('email') : $one->email];
            if ($this->input->post('password') != "" && $this->input->post('password_confirm') != "") {
                $data['password'] = do_hash($this->input->post('password'), 'md5');
            }
            $this->Global_model->global_update("users", $id, $data);
            $get_user = $this->Global_model->get_data_by_id('users', $id);
            if ($get_user) {
                //add user data to session
                $new_data = [
                    'user_id' => $get_user->id,
                    'user_email' => $get_user->email,
                    'user_name_en' => $get_user->n_en,
                    'user_name_ar' => $get_user->n_ar,
                    'user_level_id' => $get_user->level_id,
                    'logged_in' => TRUE
                ];
                $this->session->set_userdata($new_data);
            }
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/Dashboard/account_settings"));
        }
    }

    function Logout() {
        if ((uid() != "")) {
            $new_data = [
                'logged_in' => FALSE,
                'user_id' => '',
                'user_image' => '',
                'user_email' => '',
                'user_name_en' => '',
                'user_name_ar' => '',
                'permissions' => '',
                'customers' => '',
                'branches' => '',
                'language' => ''
            ];
            $this->session->unset_userdata($new_data);
            $this->session->sess_destroy();
            redirect('admin/Login');
        } else {
            redirect('admin/Login');
        }
    }

}
