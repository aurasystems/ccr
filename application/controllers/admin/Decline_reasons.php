<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Decline_reasons extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Clients_model');
        set_active("decline_reasons");
    }

    function index() {
        check_p("decline_reasons", "v");
        $data = ['title' => lang('decline_reasons')];
        $data['reasons'] = $this->Clients_model->get_data('decline_reason');
        $this->load->view("admin/pages/clients/decline_reasons", $data);
    }

    function add_reason() {
        $data['title'] = lang('add_reason');

        $this->form_validation->set_rules("n_en", lang("name_in") . lang("lang_en"), "trim|required")
                ->set_rules("n_ar", lang("name_in") . lang("lang_ar"), "trim|required");

        if ($this->form_validation->run()) {
            $n_en = $this->input->post('n_en');
            $n_ar = $this->input->post('n_ar');
            $flag_1 = $this->Global_model->isFieldExist('decline_reason', 'n_en', $n_en);
            $flag_2 = $this->Global_model->isFieldExist('decline_reason', 'n_ar', $n_ar);

            if ($flag_1 == false && $flag_2 == false) {
                $data = [
                    'n_en' => $this->input->post('n_en'),
                    'n_ar' => $this->input->post('n_ar')
                ];
                $this->Global_model->global_insert('decline_reason', $data);

                $this->session->set_flashdata('success', lang('reason_added_success'));
            } else
                $this->session->set_flashdata('error', lang('this_reason_already_added'));

            $this->load->view('admin/pages/clients/add_reason');
        } else {
            $this->load->view('admin/pages/clients/add_reason');
        }
    }

    function edit_reason() {

        $last = $this->uri->total_segments();
        $id = $this->uri->segment($last);

        $data['title'] = lang('edit_reason');
        $data['reason'] = $this->Clients_model->get_record($id, 'decline_reason');
        $this->load->view("admin/pages/clients/edit_reason", $data);

        $this->form_validation->set_rules("n_en", lang("name_in") . lang("lang_en"), "trim|required")
                ->set_rules("n_ar", lang("name_in") . lang("lang_ar"), "trim|required");

        if ($this->form_validation->run()) {

            $n_en = $this->input->post('n_en');
            $n_ar = $this->input->post('n_ar');
            if ($n_en != $data['reason'][0]->n_en || $data['reason'][0]->n_ar != $n_ar) {
                $flag_1 = $this->Global_model->isFieldExist('decline_reason', 'n_en', $n_en);
                $flag_2 = $this->Global_model->isFieldExist('decline_reason', 'n_ar', $n_ar);

                if ($flag_1 == false && $flag_2 == false) {
                    $data = [
                        'n_en' => $this->input->post('n_en'),
                        'n_ar' => $this->input->post('n_ar')
                    ];
                    $this->Global_model->global_update('decline_reason', $id, $data);

                    $this->session->set_flashdata('success', lang('reason_edited_success'));
                } else
                    $this->session->set_flashdata('error', lang('this_reason_already_added'));

                redirect(base_url("admin/Decline_reasons"));
            } else {
                redirect(base_url("admin/Decline_reasons"));
            }
        } else {
            $this->load->view('admin/pages/clients/edit_reason');
        }
    }

    function delete_reason() {
        $last = $this->uri->total_segments();
        $id = $this->uri->segment($last);

        $this->Clients_model->delete_record($id, 'decline_reason');
        $this->session->set_flashdata("success", lang("reason_del_success"));
        redirect(base_url("admin/Decline_reasons"));
    }

}
