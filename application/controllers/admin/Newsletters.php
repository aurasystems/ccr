<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Newsletters extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
        $this->load->model('admin/Newsletters_model');
    }

    function index() {
        check_p("newsletters", "v");
        set_active("newsletters");
        $data['title'] = lang('newsletters');
        $data['data'] = $this->Newsletters_model->get_data();
        $data['types'] = $this->Newsletters_model->get_data();
        $data['last_rn'] = get_last_run($data);
        $this->load->view("admin/pages/newsletters/index", $data);
    }

    function add() {
        check_p('newsletters', "c");
        set_active("add_newsletter");
        $this->form_validation->set_rules("subject", lang("subject"), "trim|required");
        $this->form_validation->set_rules("mail_body", lang("mail_body"), "trim|required");
        $this->form_validation->set_rules("run_every", lang("run_every"), "trim|required");
        $this->form_validation->set_rules("unit", lang("unit"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('add_newsletter');
            $this->load->view('admin/pages/newsletters/add', $data);
        } else {
            if ($this->input->post('run_every') < 1) {
                $this->session->set_flashdata("error", lang("check_count"));
                redirect(base_url('admin/Newsletters/add'));
            }
            $data = [
                'subject' => $this->input->post('subject'),
                'mail_body' => $this->input->post('mail_body'),
                'run_every' => $this->input->post('run_every'),
                'unit' => $this->input->post('unit'),
                'last_run' => get_date(),
            ];
            if ($_FILES['userfile']['name'] != '') {
                $config['upload_path'] = './uploads/newsletters';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $this->load->library("upload", $config);
                if ($this->upload->do_upload()) {
                    $upload_data = $this->upload->data();
                    $data['img'] = $upload_data['file_name'];
                } else {
                    $this->session->set_flashdata("error", lang("img_could_not_be_uploaded"));
                    redirect(base_url('admin/Newsletters/add'));
                }
            }
            $inserted_id = $this->Global_model->global_insert('newsletters', $data);
            if ($inserted_id) {
                $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            }
            redirect(base_url('admin/Newsletters'));
        }
    }

    function edit($id = NULL) {
        check_p('newsletters', "u");
        set_active("newsletters");
        check_param($id);
        $newsletter = $this->Newsletters_model->get_info($id);
        check_param($newsletter, "resource");
        $this->form_validation->set_rules("subject", lang("subject"), "trim|required");
        $this->form_validation->set_rules("mail_body", lang("mail_body"), "trim|required");
        $this->form_validation->set_rules("run_every", lang("run_every"), "trim|required");
        $this->form_validation->set_rules("unit", lang("unit"), "trim|required");
        if (!$this->form_validation->run()) {
            $final_tags = '';
            $data['title'] = lang('edit_newsletter');
            $data['newsletter'] = $newsletter;
            $data['id'] = $id;
            $this->load->view('admin/pages/newsletters/edit', $data);
        } else {
            if ($this->input->post('run_every') < 1) {
                $this->session->set_flashdata("error", lang("check_count"));
                redirect(base_url('admin/Newsletters/edit/' . $id));
            }
            $data = [
                'subject' => $this->input->post('subject'),
                'mail_body' => $this->input->post('mail_body'),
                'run_every' => $this->input->post('run_every'),
                'unit' => $this->input->post('unit'),
            ];
            if ($_FILES['userfile']['name'] != '') {
                $config['upload_path'] = './uploads/newsletters';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $this->load->library("upload", $config);
                if ($this->upload->do_upload()) {
                    $upload_data = $this->upload->data();
                    $data['img'] = $upload_data['file_name'];
                } else {
                    $this->session->set_flashdata("error", lang("img_could_not_be_uploaded"));
                    redirect(base_url('admin/Newsletters/edit/' . $id));
                }
            }
            $this->Global_model->global_update('newsletters', $id, $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/Newsletters'));
        }
    }

    function set_status($id = NULL, $flag = NULL) {
        check_p("newsletters", "e");
        check_param($id);
        if ($flag) {
            $this->Newsletters_model->set_newsletter_deactivate($id, ["status" => 0]);
            $this->Global_model->global_update('newsletters', $id, ["status" => 1]);
            $this->session->set_flashdata("success", lang("news_act_success"));
        } else {
            $this->Global_model->global_update('newsletters', $id, ["status" => 0]);
            $this->session->set_flashdata("success", lang("news_deact_success"));
        }
        redirect(base_url('admin/Newsletters'));
    }

    function delete_newsletter($id = NULL) {
        check_p("newsletters", "d");
        check_param($id);
        $this->Global_model->global_update('newsletters', $id, ["deleted" => 1]);
        $this->session->set_flashdata("success", lang("newsletter_del_success"));
        redirect(base_url('admin/Newsletters'));
    }

    function assignees($id = NULL) {
        check_p("newsletters", "v");
        set_active("newsletters");
        check_param($id);
        $one = $this->Newsletters_model->get_info($id);
        check_param($one, "resource");
        $data['title'] = lang('assignees');
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['id'] = $id;
        $data['clients'] = $this->Newsletters_model->get_newsletter_clients($id);
        $data['one'] = $one;
        $data['now'] = date("Y-m-d H:i:s");
        $this->load->view("admin/pages/newsletters/assignees", $data);
    }

    function add_assignees($id = NULL) {
        check_param($id);
        $one = $this->Newsletters_model->get_info($id);
        if (!empty($this->input->post('assignees')[0])) {
            $post_assignees = $this->input->post('assignees')[0];
            $assignees = explode(",", $post_assignees);
            foreach ($assignees as $assignee) {
                if (!empty($assignee)) {
                    // Check if client already exists
                    $check = $this->Newsletters_model->check_newsletter_client($id, $assignee);
                    if (empty($check)) {
                        $data = [
                            'news_l_id' => $id,
                            'client_id' => $assignee
                        ];
                        $this->Global_model->global_insert('newsletter_assignees', $data);
                    }
                }
            }
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
        }
        redirect(base_url('admin/Newsletters/assignees/' . $id));
    }

    function search_client_name() {
        $text = $this->input->post('term');
        $result = $this->Newsletters_model->get_clients_by_search($text);
        if ($result) {
            echo json_encode($result);
        } else {
            echo json_encode(FALSE);
        }
    }

    function delete_assignee($id = NULL, $news_id = NULL) {
        check_p("newsletters", "d");
        check_param($id);
        check_param($news_id);
        $this->Global_model->global_delete('newsletter_assignees', $id);
        $this->session->set_flashdata("success", lang("assignee_del_success"));
        redirect(base_url('admin/Newsletters/assignees/' . $news_id));
    }

    function delete_all_assignee($id = NULL) {
        check_p("newsletters", "d");
        check_param($id);
        $this->Newsletters_model->delete_all_assignee($id);
        $this->session->set_flashdata("success", lang("assignees_del_success"));
        redirect(base_url('admin/Newsletters/assignees/' . $id));
    }

}
