<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sms_settings extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Sms_settings_model');
        set_active("sms_settings");
    }

    function index() {
        check_p("sms_settings", "v");
        $data = ['title' => lang('sms_settings')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['sms_settings'] = $this->Sms_settings_model->get_sms_settings();
        $this->load->view("admin/pages/settings/sms_settings", $data);
    }

    function update() {
        check_p("sms_settings", "u");
        $this->load->library('form_validation');
        //$this->form_validation->set_rules('url', lang("url"), 'trim');
        $this->form_validation->set_rules('username', lang("username"), 'trim');
        $this->form_validation->set_rules('password', lang("password"), 'trim');
        $this->form_validation->set_rules('sender_id', lang("sender_id"), 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $data = [
                //'url' => $this->input->post('url') == '' ? NULL : $this->input->post('url'),
                'username' => $this->input->post('username') == '' ? NULL : $this->input->post('username'),
                'password' => $this->input->post('password') == '' ? NULL : $this->input->post('password'),
                'sender_id' => $this->input->post('sender_id') == '' ? NULL : $this->input->post('sender_id')
            ];
            $this->Sms_settings_model->update_settings($data);
            $this->session->unset_userdata('sms_settings');
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/Sms_settings"));
        }
    }

}
