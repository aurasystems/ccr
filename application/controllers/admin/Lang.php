<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lang extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    function _remap($param) {
        $this->switchLang($param);
    }

    function switchLang($language = '') {
        $language = $language != NULL && in_array($language, array('arabic', 'english')) ? $language : 'english';
        $this->session->set_userdata('language', $language);
        if (uid()) {
            $this->Global_model->global_update('users', uid(), ['language' => $language]);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

}
