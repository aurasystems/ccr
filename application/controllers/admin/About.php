<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class About extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/About_model');
        set_active("about_page");
    }

    function index() {
        check_p("about_page", "v");
        set_active("about_page");
        $data = ['title' => lang('about_page')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['about'] = $this->About_model->get_content();
        $this->load->view("admin/pages/other/about", $data);
    }

    function update() {
        check_p("about_page", "u");
        $this->load->library('form_validation');
        $this->form_validation->set_rules('content', lang("content"), 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $about = $this->About_model->get_content();
            if (!empty($about)) {
                // Update
                $data = [
                    'content' => (!empty($this->input->post('content'))) ? $this->input->post('content') : NULL
                ];
                $this->About_model->update_content($data);
            } else {
                // Insert
                $data = [
                    'content' => (!empty($this->input->post('content'))) ? $this->input->post('content') : NULL,
                    'type' => 1
                ];
                $inserted = $this->Global_model->global_insert('pages', $data);
            }

            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/About"));
        }
    }

}
