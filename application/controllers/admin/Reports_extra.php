<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports_extra extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Reports_extra_model', 'rpt');
        set_active("reports");
    }

    function index() {
        
    }

    function total_expenses_rpt() {
        check_p("equip_rep", "v");
        set_active("equip_rep");
        $data['title'] = lang('total_expensed_s_n');
        $this->load->view("admin/pages/reports/equipment/total_expenses", $data);
    }

    function gen_total_expenses_rpt() {
        $data = array();
        $scanned_SN = $this->input->post('scan_SN');
        if (get_p('equip_rep', 'v')) {
            $data = $this->rpt->get_expenses($scanned_SN);
        }
        echo json_encode($data);
    }

    function coupon_codes_rpt() {
        check_p("ecomm_rep", "v");
        set_active("ecomm_rep");
        $data['title'] = lang('coupon_codes');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');
        $this->load->view("admin/pages/reports/ecommerce/coupon_codes", $data);
    }

    function gen_coupon_codes_rpt() {
        $output = [];
        if (get_p('ecomm_rep', 'v')) {
            $data = [
                'date_from' => $this->input->post('date_from'),
                'date_to' => $this->input->post('date_to')
            ];
            if (!empty($data['date_from']) && !empty($data['date_to'])) {
                $from = $data['date_from'];
                $to = $data['date_to'];
            } else {
                $from = date('Y-m-d');
                $to = date('Y-m-d');
            }
            $output = $this->rpt->get_vouchers($from, $to);
           
        }

       
        echo json_encode($output);
    }

    function get_client_name()
    {
        $client_id = $this->input->post('client_id');
        $client_data = $this->Global_model->get_data_by_id('clients', $client_id);
        echo json_encode($client_data->n_en);
    }

    function total_number_of_rental_rpt() {
        check_p("equip_rep", "v");
        set_active("equip_rep");
        $data['title'] = lang('total_trans_num_s_n');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');
        $this->load->view("admin/pages/reports/equipment/total_trans_num_s_n", $data);
    }

    function gen_total_number_of_rental_rpt_scan() {
        set_active("equip_rep");
        $output = [];
        if (get_p('equip_rep', 'v')) {
           $SN = $this->input->post('scan_SN');
           $data = $this->rpt->get_total_transaction($SN);
           if(!$data['flag'])
           {
               $output['flag'] = false;
               $output['msg']  = lang('no_data_for_this_SN');
           }
           else if($data['flag'])
           {
                $output['flag']       = true;
                $output['num_trans']  = $data['num_trans'];
                $output['num_days']   = $data['num_days'];
                $output['pname']      = $data['pname'];
                $output['num_hrs']    = $data['num_hrs'];
                $output['SN']         = $data['SN'];
           }
        }
        echo json_encode($output);
    }

    function gen_total_number_of_rental_rpt() {
        $output = [];
        if (get_p('equip_rep', 'v')) {
            $data = [
                'date_from' => $this->input->post('date_from'),
                'date_to' => $this->input->post('date_to')
            ];
            if (!empty($data['date_from']) && !empty($data['date_to'])) {
                $from = $data['date_from'];
                $to = $data['date_to'];
            } else {
                $from = date('Y-m-d');
                $to = date('Y-m-d');
            }
            $output = $this->rpt->get_total_trans_period($from, $to);
           
        }
        echo json_encode($output);
    }

    function total_s_n_inv_rpt() {
        check_p("equip_rep", "v");
        set_active("equip_rep");
        $data['title'] = lang('total_s_n_inv');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');
        $this->load->view("admin/pages/reports/equipment/total_s_n_inv", $data);
    }

    function gen_total_s_n_inv_rpt() {
        $output = [];
        if (get_p('equip_rep', 'v')) {
            $data = [
                'date_from' => $this->input->post('date_from'),
                'date_to' => $this->input->post('date_to')
            ];
            if (!empty($data['date_from']) && !empty($data['date_to'])) {
                $from = $data['date_from'];
                $to = $data['date_to'];
            } else {
                $from = date('Y-m-d');
                $to = date('Y-m-d');
            }
            $type   = $this->input->post('type');
            $output = $this->rpt->get_total_sn_inv($from, $to, $type);
           
        }
        echo json_encode($output);
    }

    function basket_size_rpt()
    {
        check_p("ecomm_rep", "v");
        set_active("ecomm_rep");
        $data['title'] = lang('avg_basket_size');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');
        $this->load->view("admin/pages/reports/ecommerce/basket_size", $data);
    }

    function gen_basket_size_rpt() {
        $output = [];
        if (get_p('equip_rep', 'v')) {
            $data = [
                'date_from' => $this->input->post('date_from'),
                'date_to' => $this->input->post('date_to')
            ];
            if (!empty($data['date_from']) && !empty($data['date_to'])) {
                $from = $data['date_from'];
                $to = $data['date_to'];
            } else {
                $from = date('Y-m-d');
                $to = date('Y-m-d');
            }
            $output = $this->rpt->get_avg_basket($from, $to);
           
        }
        echo json_encode($output);
    }

    function total_revenue_sn_rpt()
    {
        check_p("equip_rep", "v");
        set_active("equip_rep");
        $data['title'] = lang('total_revenue_sn_rpt');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');

        $this->load->view("admin/pages/reports/equipment/total_revenue", $data);
    }

    function gen_total_revenue_sn_rpt() {
        $output = [];
        $filter = 0;
        if (get_p('equip_rep', 'v')) {
            $data = [
                'date_from' => $this->input->post('date_from'),
                'date_to' => $this->input->post('date_to')
            ];
            if (!empty($data['date_from']) && !empty($data['date_to'])) {
                $from = $data['date_from'];
                $to = $data['date_to'];
            } else {
                $from = date('Y-m-d');
                $to = date('Y-m-d');
            }
            $for = $this->input->post('for');
            if($for == 1)
            $filter = $this->input->post('item');
            else if($for == 2)
            $filter = $this->input->post('brand');
            else if($for == 3)
            $filter = $this->input->post('categ');
            $output = $this->rpt->get_total_revenue($from, $to, $for, $filter);
           
        }
        $output['for']  = $for;
        $output['filter'] = $filter;
       echo json_encode($output);
    }

    function indivdual_transaction($from, $to, $for, $filter)
    {
        $output = [];
        $form = date('Y-m-d');
        $to = date('Y-m-d');
        
        $output = $this->rpt->get_individual_trans($from, $to, $for, $filter);
                   
        $data['result'] = $output;
        $this->load->view("admin/pages/reports/equipment/individual_transaction", $data);
    }

    function profit_anal_s_n_rpt()
    {
        check_p("equip_rep", "v");
        set_active("equip_rep");
        $data['title'] = lang('profit_anal_s_n');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');

        $this->load->view("admin/pages/reports/equipment/profit_anal_s_n", $data);
    }

    function gen_profit_anal_s_n_rpt() {
        $output = [];
        $filter = 0;
        if (get_p('equip_rep', 'v')) {
            $SN     = $this->input->post('SN');
            $output = $this->rpt->get_revenue_vs_expense($SN);
           
        }
       echo json_encode($output);
    }

    function op_eff_rpt()
    {
        check_p("equip_rep", "v");
        set_active("equip_rep");
        $data['title'] = lang('op_eff');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');

        $this->load->view("admin/pages/reports/equipment/op_eff", $data);
    }

    function gen_op_eff_rpt() {
        $output = [];
        $filter = 0;
        if (get_p('equip_rep', 'v')) {
            $SN     = $this->input->post('SN');
            $output = $this->rpt->get_op_eff($SN);
           
        }
       echo json_encode($output);
    }
}
