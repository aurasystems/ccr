<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Booking extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        date_default_timezone_set('Africa/Cairo');
        $this->load->model('admin/Booking_model');
        set_active("booking");
    }

    function index() {
        
    }

    function pending_bookings() {
        check_p("booking", "v");
        set_active("pending_bookings");
        $data['title'] = lang('pending_bookings');
        $data['decline_reasons'] = $this->Global_model->get_active_data('decline_reason');
        $data['pending'] = $this->Booking_model->get_pending_orders();
        $data['current_url'] = $this->uri->uri_string();
        $this->load->view("admin/pages/bookings/pending_orders", $data);
    }

    function approved_bookings() {
        check_p("booking", "v");
        set_active("approved_bookings");
        $data['title'] = lang('approved_bookings');
        $data['approved'] = $this->Booking_model->get_approved_orders();
        $this->load->view("admin/pages/bookings/approved_orders", $data);
    }

    function declined_bookings() {
        check_p("booking", "v");
        set_active("declined_bookings");
        $data['title'] = lang('declined_bookings');
        $data['declined'] = $this->Booking_model->get_declined_orders();
        $this->load->view("admin/pages/bookings/declined_orders", $data);
    }

    function decline_order() {
        check_p("booking", "e");
        $id = $this->input->post('book_id');
        check_param($id);
        $one = $this->Global_model->get_data_by_id('booking', $id);
        check_param($one, "resource");
        $this->form_validation->set_rules("reason", lang("decline_reason"), "trim|required");
        if (!$this->form_validation->run()) {
            $this->pending_bookings();
        } else {
            if ($this->input->post('reason') == '') {
                $this->session->set_flashdata("error", lang("select_d_r"));
                redirect(base_url('admin/Booking/pending_bookings'));
            }
            $data = [
                'decline_reason' => $this->input->post('reason'),
                "status" => 3,
            ];
            $res = $this->Global_model->global_update('booking', $id, $data);
            $client_data = $this->Global_model->get_data_by_id('clients', $one->client_id);
            // check for email/sms notifications to be send
            $settings = settings('all', 3);
            if (!empty($settings)) {
                $notification = $settings->notification_type;
                $email = $client_data->email;
                $phone = $client_data->phone;
                $msg = $settings->unapp_order_text;
                if ($notification == 1 || $notification == 3) {
                    // send email notification
                    $data = [
                        'to' => $email,
                        'subject' => lang("declined_order"),
                        'message' => $msg
                    ];
                    send_email($data);
                }
                if ($notification == 2 || $notification == 3) {
                    // send sms notification
                    $sent = send_sms($phone, $msg);
                    if (!$sent) {
                        // insert into failed msgs
                        $failed_dt = [
                            'client_id' => $client_data->id,
                            'phone' => $phone,
                            'msg' => $msg
                        ];
                        $this->Global_model->global_insert('failed_messages', $failed_dt);
                    }
                }
            }
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/Booking/pending_bookings'));
        }
    }

    function approve_order($id = NULL) {
        check_p("booking", "e");
        $one = $this->Global_model->get_data_by_id('booking', $id);
        check_param($one, "resource");
        if ($one) {
            $res = $this->Global_model->global_update('booking', $id, ["status" => 1]);
            $client_data = $this->Global_model->get_data_by_id('clients', $one->client_id);
            // check for email/sms notifications to be send
            $settings = settings('all', 3);
            if (!empty($settings)) {
                $notification = $settings->notification_type;
                $email = $client_data->email;
                $phone = $client_data->phone;
                $msg = $settings->app_order_text;
                if ($notification == 1 || $notification == 3) {
                    // send email notification
                    $data = [
                        'to' => $email,
                        'subject' => lang("approved_order"),
                        'message' => $msg
                    ];
                    send_email($data);
                }
                if ($notification == 2 || $notification == 3) {
                    // send sms notification
                    $sent = send_sms($phone, $msg);
                    if (!$sent) {
                        // insert into failed msgs
                        $failed_dt = [
                            'client_id' => $client_data->id,
                            'phone' => $phone,
                            'msg' => $msg
                        ];
                        $this->Global_model->global_insert('failed_messages', $failed_dt);
                    }
                }
            }
            $this->session->set_flashdata("success", lang("order_approved_success"));
            redirect(base_url('admin/Booking/pending_bookings'));
        } else {
            $error = lang("data_not_found");
            $this->session->set_flashdata("error", $error);
            redirect(base_url('admin/Booking/pending_bookings'));
        }
    }

    function delete_booking($id = NULL, $flag = 0) {
        check_p('booking', "d");
        $one = $this->Global_model->get_data_by_id('booking', $id);
        check_param($one, "resource");
        if ($one) {
            if ($flag == 0) {
                $url = 'admin/Booking/pending_bookings';
            } elseif ($flag == 1) {
                $url = 'admin/Booking/approved_bookings';
            } elseif ($flag == 2) {
                $url = 'admin/Booking/declined_bookings';
            }
            // 	Get from booking waiting list and check for first one not notified
            $booking_items = $this->Booking_model->booking_items($id);
            if (!empty($booking_items)) {
                foreach ($booking_items as $item) {
                    $booking_waiting = $this->Booking_model->get_booking_waiting($item->product_id);
                    if (!empty($booking_waiting)) {
                        // check for email/sms notifications to be send
                        $settings = settings('all', 3);
                        if (!empty($settings)) {
                            $notification = $settings->notification_type;
                            $email = $booking_waiting->email;
                            $phone = $booking_waiting->phone;
                            $msg = lang('your_request') . ' (' . $booking_waiting->pname . ') ' . lang('is_available_now');
                            if ($notification == 1 || $notification == 3) {
                                // send email notification
                                $data = [
                                    'to' => $email,
                                    'subject' => lang("product_request"),
                                    'message' => $msg
                                ];
                                send_email($data);
                            }
                            if ($notification == 2 || $notification == 3) {
                                // send sms notification
                                $sent = send_sms($phone, $msg);
                                if (!$sent) {
                                    // insert into failed msgs
                                    $failed_dt = [
                                        'client_id' => $booking_waiting->client_id,
                                        'phone' => $phone,
                                        'msg' => $msg
                                    ];
                                    $this->Global_model->global_insert('failed_messages', $failed_dt);
                                }
                            }
                            $this->Global_model->global_update('booking_waiting_list', $booking_waiting->id, ['notified' => 1]);
                        }
                    }
                }
            }
            $this->Global_model->global_update('booking', $id, ["deleted" => 1]);
            $this->Booking_model->delete_booking_items('booking_items', $id);
            $this->Booking_model->delete_booking_items('booking_items_delivery', $id);
            $this->Booking_model->delete_booking_items('booking_items_preparation', $id);
            $this->Booking_model->delete_booking_items('booking_items_return', $id);
            $this->session->set_flashdata("success", lang("data_deleted_successfully"));
            redirect(base_url($url));
        } else {
            $error = lang("data_not_found");
            $this->session->set_flashdata("error", $error);
            redirect(base_url($url));
        }
    }

    function update_booking_date_to() {
        check_p('booking', "u");
        $postData = $this->input->post();
        $id = $postData['id'];
        $b_from = $postData['b_from'];
        $b_to = $postData['b_to'];
        $b_qty = $postData['b_qty'];
        $one = $this->Global_model->get_data_by_id('booking_items', $id);
        $booking = $this->Global_model->get_data_by_id('booking', $one->book_id);
        if ($booking && $one) {
            // 	Availability check
            $available = checkAvailability(NULL, $one->product_id, $b_qty, $b_from, $b_to);
//            echo $available['msg'];
//            die;
            if ($available['flag']) {
                $this->Global_model->global_update('booking_items', $id, ["b_from" => $b_from, "b_to" => $b_to]);
                $this->session->set_flashdata('success', lang('booking_updated_successfully'));
                echo json_encode(TRUE);
            } elseif (!$available['flag']) {
                $this->session->set_flashdata("error", $available['msg']);
                echo json_encode(FALSE);
            }
        } else {
            $this->session->set_flashdata("error", lang('data_not_found'));
            echo json_encode(FALSE);
        }
    }

    function booking_items($id = NULL) {
        check_param($id);
        $one = $this->Global_model->get_data_by_id('booking', $id);
        check_param($one, "resource");
        $data['title'] = lang('booking_items');
        $data['id'] = $id;
        $data['items'] = $this->Booking_model->booking_items($id);
        $data['one'] = $one;
        $data['now'] = date("Y-m-d H:i:s");
        $this->load->view("admin/pages/bookings/items", $data);
    }

    function delete_booking_item($id = NULL, $booking_id) {
        check_p('booking', "d");
        $booking = $this->Global_model->get_data_by_id('booking', $booking_id);
        $one = $this->Global_model->get_data_by_id('booking_items', $id);
        check_param($booking, "resource");
        check_param($one, "resource");
        if ($booking && $one) {
            $this->Global_model->global_delete('booking_items', $id);
            // 	Get from booking waiting list and check for first one not notified
            $booking_waiting = $this->Booking_model->get_booking_waiting($one->product_id);
            if (!empty($booking_waiting)) {
                // check for email/sms notifications to be send
                $settings = settings('all', 3);
                if (!empty($settings)) {
                    $notification = $settings->notification_type;
                    $email = $booking_waiting->email;
                    $phone = $booking_waiting->phone;
                    $msg = lang('your_request') . ' (' . $booking_waiting->pname . ') ' . lang('is_available_now');
                    if ($notification == 1 || $notification == 3) {
                        // send email notification
                        $data = [
                            'to' => $email,
                            'subject' => lang("product_request"),
                            'message' => $msg
                        ];
                        send_email($data);
                    }
                    if ($notification == 2 || $notification == 3) {
                        // send sms notification
                        $sent = send_sms($phone, $msg);
                        if (!$sent) {
                            // insert into failed msgs
                            $failed_dt = [
                                'client_id' => $booking_waiting->client_id,
                                'phone' => $phone,
                                'msg' => $msg
                            ];
                            $this->Global_model->global_insert('failed_messages', $failed_dt);
                        }
                    }
                    $this->Global_model->global_update('booking_waiting_list', $booking_waiting->id, ['notified' => 1]);
                }
            }
            $this->session->set_flashdata("success", lang("data_deleted_successfully"));
            redirect(base_url('admin/Booking/booking_items/' . $booking_id));
        } else {
            $error = lang("data_not_found");
            $this->session->set_flashdata("error", $error);
            redirect(base_url('admin/Booking/booking_items/' . $booking_id));
        }
    }

    function booking_add_product($id = NULL) {
        check_param($id);
        $one = $this->Global_model->get_data_by_id('booking', $id);
        check_param($one, "resource");
        if ($one->status != 3) {
            $data['title'] = lang('booking_items');
            $data['id'] = $id;
            $data['items'] = $this->Booking_model->booking_items($id);
            $data['categories'] = $this->Booking_model->get_distinct_categories();
            $data['one'] = $one;
            $this->load->view("admin/pages/bookings/add_product", $data);
        } else {
            redirect(base_url('admin/Booking/booking_items/' . $id));
        }
    }

    function booking_check_product($booking_id = NULL, $product_id = NULL) {
        check_param($booking_id);
        check_param($product_id);
        $booking = $this->Global_model->get_data_by_id('booking', $booking_id);
        check_param($booking, "resource");
        $product = $this->Global_model->get_data_by_id('products', $product_id);
        check_param($product, "resource");
        $data['title'] = lang('booking_items');
        $data['booking_id'] = $booking_id;
        $data['product_id'] = $product_id;
        $data['booking'] = $booking;
        $data['product'] = $product;
        $this->load->view("admin/pages/bookings/check_product", $data);
    }

    function check_product_availability($booking_id = NULL, $product_id = NULL) {
        check_param($booking_id);
        check_param($product_id);
        $booking = $this->Global_model->get_data_by_id('booking', $booking_id);
        check_param($booking, "resource");
        $product = $this->Global_model->get_data_by_id('products', $product_id);
        check_param($product, "resource");
        $this->form_validation->set_rules("quantity", lang("quantity"), "trim|required|numeric");
        if (!empty($product) && $product->rent_term == 1) {
            // day
            $this->form_validation->set_rules("b_from", lang("b_from"), "trim|required|callback_valid_dt_b_from");
            $this->form_validation->set_rules("b_to", lang("b_to"), "trim|required|callback_valid_dt_b_to");
        } elseif (!empty($product) && $product->rent_term == 2) {
            // hour
            $this->form_validation->set_rules("date", lang("date"), "trim|required|callback_valid_date");
            $this->form_validation->set_rules("b_time_from", lang("b_time_from"), "trim|required");
            $this->form_validation->set_rules("b_time_to", lang("b_time_to"), "trim|required");
        }
        if (!$this->form_validation->run()) {
            $this->booking_check_product($booking_id, $product_id);
        } else {
            $location = $booking->branch;
            $quantity = $this->input->post('quantity');
            if (!empty($product) && $product->rent_term == 1) {
                $b_from = date('Y-m-d H:i:s', strtotime($this->input->post('b_from')));
                $b_to = date('Y-m-d H:i:s', strtotime($this->input->post('b_to')));
            } elseif (!empty($product) && $product->rent_term == 2) {
                $time_from = date('H:i:s', strtotime($this->input->post('b_time_from')));
                $time_to = date('H:i:s', strtotime($this->input->post('b_time_to')));
                $b_from = $this->input->post('date') . ' ' . $time_from;
                $b_to = $this->input->post('date') . ' ' . $time_to;
            }
            $available = checkAvailability(NULL, $product_id, $quantity, $b_from, $b_to);
//            echo $available['msg'];
//            die;
            if ($available['flag']) {
                // add this product to current booking
                $data = [
                    'book_id' => $booking_id,
                    'product_id' => $product_id,
                    'b_from' => $b_from,
                    'b_to' => $b_to,
                    'rent_price' => $product->rent_price,
                    'quantity' => $quantity,
                    'rent_term' => $product->rent_term
                ];
                $this->Global_model->global_insert('booking_items', $data);
                $this->session->set_flashdata("success", $available['msg']);
                redirect(base_url('admin/Booking/booking_items/' . $booking_id));
            } elseif (!$available['flag']) {
                $this->session->set_flashdata("error", $available['msg']);
                redirect(base_url('admin/Booking/booking_items/' . $booking_id));
            }
        }
    }

    function valid_dt_b_from() {
        if (date('Y-m-d H:i', strtotime($this->input->post('b_from'))) == $this->input->post('b_from')) {
            return TRUE;
        } else {
            $this->form_validation->set_message('valid_dt_b_from', $this->lang->line('check_date_format'));
            return FALSE;
        }
    }

    function valid_dt_b_to() {
        if (date('Y-m-d H:i', strtotime($this->input->post('b_to'))) == $this->input->post('b_to')) {
            return TRUE;
        } else {
            $this->form_validation->set_message('valid_dt_b_to', $this->lang->line('check_date_format'));
            return FALSE;
        }
    }

    function valid_date() {
        if (date('Y-m-d', strtotime($this->input->post('date'))) == $this->input->post('date')) {
            return TRUE;
        } else {
            $this->form_validation->set_message('valid_date', $this->lang->line('check_date_format'));
            return FALSE;
        }
    }

    function valid_time_b_from() {
        if (date('H:i:s', strtotime($this->input->post('b_time_from'))) == $this->input->post('b_time_from')) {
            return TRUE;
        } else {
            $this->form_validation->set_message('valid_time_b_from', $this->lang->line('check_time_format'));
            return FALSE;
        }
    }

    function valid_time_b_to() {
        if (date('H:i:s', strtotime($this->input->post('b_time_to'))) == $this->input->post('b_time_to')) {
            return TRUE;
        } else {
            $this->form_validation->set_message('valid_time_b_to', $this->lang->line('check_time_format'));
            return FALSE;
        }
    }

    function stock_check() {
        $booking_id = $this->input->post('booking_id');
        $product_id = $this->input->post('product_id');
        if (empty($booking_id) && empty($product_id)) {
            $this->session->set_flashdata('error', lang('error_occured'));
            redirect('admin/Booking/booking_items/' . $booking_id);
        }
        $result = $this->Booking_model->stock_check($booking_id, $product_id);
        echo json_encode($result);
    }

    function missing_items() {
        check_p("missing_items", "v");
        set_active("missing_items");
        $settings = settings('all', 3);
        $data['title'] = lang('missing_items');
        $data['items'] = $this->Booking_model->get_missing_items();
        foreach ($data['items'] as $item) {
            $one = $this->Booking_model->get_booking_item($item->book_id, $item->product_id);
            if ($one) {
                $item->b_from = $one->b_from;
                $item->b_to = $one->b_to;
                if (!empty($settings) && !empty($settings->time_for_missing)) {
                    $result_time = date('Y-m-d H:i:s', strtotime('+' . $settings->time_for_missing . ' hour', strtotime($one->b_to)));
                } else {
                    $result_time = $item->timestamp;
                }
                $item->return_time = $result_time;
            }
        }
        $this->load->view("admin/pages/booking_return/missing_items", $data);
    }

    function set_item_returned($id) {
        check_p("missing_items", "u");
        check_param($id);
        $one = $this->Global_model->get_data_by_id('booking_items_return', $id);
        check_param($one, "resource");
        // get item data
        // Later price was added to client account will be removed if item returned...
        $item = get_product_item_by_id($one->item_id);
        $this->Global_model->global_update('booking_items_return', $id, ["missing" => 0]);
        $this->Global_model->global_update('product_items', $item->id, ["missing" => 0]);
        $this->session->set_flashdata('success', lang('item_set_returned'));
        redirect(base_url() . 'admin/Booking/missing_items');
    }

    function damaged_items() {
        check_p("damaged_items", "v");
        set_active("damaged_items");
        $data['title'] = lang('damaged_items');
        $data['items'] = $this->Booking_model->get_damaged_items();
        $this->load->view("admin/pages/booking_return/damaged_items", $data);
    }

    function add_to_maintenance() {
        $today = date('Y-m-d');
        $return_id = $this->input->post('return_id');
        $product_id = $this->input->post('product_id');
        $item_id = $this->input->post('item_id');
        $m_from = $this->input->post('m_from');
        $m_to = $this->input->post('m_to');
        $one = $this->Global_model->get_data_by_id('booking_items_return', $return_id);
        if (empty($return_id) && empty($one) && empty($product_id) && empty($item_id)) {
            $this->session->set_flashdata('error', lang('error_occured'));
            redirect('admin/Booking/damaged_items');
        }
        if (empty($m_from) && empty($m_to)) {
            $this->session->set_flashdata('error', lang('select_date_range'));
            redirect('admin/Booking/damaged_items');
        }
        if ($m_from > $m_to) {
            $this->session->set_flashdata('error', lang('check_date_range'));
            redirect('admin/Booking/damaged_items');
        }
        if ($m_from < $today) {
            $this->session->set_flashdata('error', lang('no_old_date'));
            redirect('admin/Booking/damaged_items');
        }
        $data = [
            'product_id' => $product_id,
            'item_id' => $item_id,
            'm_from' => $m_from,
            'm_to' => $m_to
        ];
        $inserted = $this->Global_model->global_insert('maintenance', $data);
        if ($inserted) {
            $this->Global_model->global_update('booking_items_return', $return_id, ["added_maintenance" => 1]);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect('admin/Booking/damaged_items');
        } else {
            $this->session->set_flashdata('error', lang('error_occured'));
            redirect('admin/Booking/damaged_items');
        }
    }

    function missing_acc() {
        check_p("missing_acc", "v");
        set_active("missing_acc");
        $settings = settings('all', 3);
        $data['title'] = lang('missing_acc');
        $data['items'] = $this->Booking_model->get_missing_acc();
        $this->load->view("admin/pages/booking_return/missing_acc", $data);
    }

    function set_acc_returned($id) {
        check_p("missing_acc", "u");
        check_param($id);
        $one = $this->Global_model->get_data_by_id('booking_sup_items_return', $id);
        check_param($one, "resource");
        // get item data
        // Later price was added to client account will be removed if item returned...
        $item = $this->Global_model->get_data_by_id('product_sup_items', $one->sup_id);
        $this->Global_model->global_update('booking_sup_items_return', $id, ["missing" => 0]);
        $this->Global_model->global_update('product_sup_items', $item->id, ["missing" => 0]);
        $this->session->set_flashdata('success', lang('item_set_returned'));
        redirect(base_url() . 'admin/Booking/missing_acc');
    }

    function damaged_acc() {
        check_p("damaged_acc", "v");
        set_active("damaged_acc");
        $data['title'] = lang('damaged_acc');
        $data['items'] = $this->Booking_model->get_damaged_acc();
        $this->load->view("admin/pages/booking_return/damaged_acc", $data);
    }

    function add_acc_to_maintenance() {
        $today = date('Y-m-d');
        $return_id = $this->input->post('return_id');
        $item_id = $this->input->post('item_id');
        $m_from = $this->input->post('m_from');
        $m_to = $this->input->post('m_to');
        $one = $this->Global_model->get_data_by_id('booking_sup_items_return', $return_id);
        $p_sub_item = $this->Global_model->get_data_by_id('product_sup_items', $item_id);
        if (empty($return_id) && empty($one) && empty($item_id) && empty($p_sub_item)) {
            $this->session->set_flashdata('error', lang('error_occured'));
            redirect('admin/Booking/damaged_acc');
        }
        if (empty($m_from) && empty($m_to)) {
            $this->session->set_flashdata('error', lang('select_date_range'));
            redirect('admin/Booking/damaged_acc');
        }
        if ($m_from > $m_to) {
            $this->session->set_flashdata('error', lang('check_date_range'));
            redirect('admin/Booking/damaged_acc');
        }
        if ($m_from < $today) {
            $this->session->set_flashdata('error', lang('no_old_date'));
            redirect('admin/Booking/damaged_acc');
        }
        $data = [
            'product_id' => (!empty($p_sub_item->product_id)) ? $p_sub_item->product_id : NULL,
            'sup_id' => $item_id,
            'm_from' => $m_from,
            'm_to' => $m_to
        ];
        $inserted = $this->Global_model->global_insert('maintenance', $data);
        if ($inserted) {
            $this->Global_model->global_update('booking_sup_items_return', $return_id, ["added_maintenance" => 1]);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect('admin/Booking/damaged_acc');
        } else {
            $this->session->set_flashdata('error', lang('error_occured'));
            redirect('admin/Booking/damaged_acc');
        }
    }

}
