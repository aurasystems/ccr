<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Salary extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Salary_model');
        set_active("payroll");
    }

    function index() {
        check_p("payroll", "v");
        $data = ['title' => lang('payroll')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['locations'] = get_data_list(['c' => 6]);
        $this->load->view("admin/pages/user/salary", $data);
    }

    function calculate_salary() {
        check_p("payroll", "v");
        $this->form_validation->set_rules("n_en", lang('name_in') . lang('lang_en'), "trim|required")
                ->set_rules("n_ar", lang('name_in') . lang('lang_ar'), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('payroll');
            $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
            $this->load->view("admin/pages/user/salary", $data);
        } else {
            
        }
    }

    function get_users_salaries() {
        $data = $this->input->post();
        if ($data['location'] == '' || $data['year_month'] == '') {
            $this->session->set_flashdata('error', $this->lang->line('spec_loc_month'));
            redirect('admin/Salary');
        }
        $result = $this->Salary_model->get_users_salaries($data);
        if ($result) {
            echo json_encode($result);
        } else {
            echo json_encode(FALSE);
        }
    }

    function get_user_attendance($user_id = NULL, $branch_id = NULL, $year_month = NULL) {
        check_p("payroll", "v");
        check_param($user_id);
        check_param($year_month);
        check_param($branch_id);
        $user = $this->Global_model->get_data_by_id('users', $user_id);
        $location = $this->Global_model->get_data_by_id('locations', $branch_id);
        check_param($user, "resource");
        check_param($location, "resource");
        if (date('Y-m', strtotime($year_month)) != $year_month) {
            $this->session->set_flashdata('error', $this->lang->line('check_y_m'));
            redirect('admin/Salary');
        }
        $data = ['title' => lang('user_attendance')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['attendance'] = $this->Salary_model->get_user_attendance($user_id, $branch_id, $year_month);
        $data['user_id'] = $user_id;
        $data['year_month'] = $year_month;
        $data['user'] = $user;
        $data['location'] = $location;
        $this->load->view("admin/pages/user/attendance", $data);
    }

}
