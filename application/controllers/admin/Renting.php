<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Renting extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Renting_model');
        set_active("how_to_rent_page");
    }

    function index() {
        check_p("how_to_rent_page", "v");
        set_active("how_to_rent_page");
        $data = ['title' => lang('privacy_page')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['renting'] = $this->Renting_model->get_content();
        $this->load->view("admin/pages/other/renting", $data);
    }

    function update() {
        check_p("how_to_rent_page", "u");
        $this->load->library('form_validation');
        $this->form_validation->set_rules('content', lang("content"), 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $about = $this->Renting_model->get_content();
            if (!empty($about)) {
                // Update
                $data = [
                    'content' => (!empty($this->input->post('content'))) ? $this->input->post('content') : NULL
                ];
                $this->Renting_model->update_content($data);
            } else {
                // Insert
                $data = [
                    'content' => (!empty($this->input->post('content'))) ? $this->input->post('content') : NULL,
                    'type' => 3
                ];
                $inserted = $this->Global_model->global_insert('pages', $data);
            }

            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/Renting"));
        }
    }

}
