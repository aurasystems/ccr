<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Studios extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Studios_model');
        set_active("studios");
    }

    function index() {
        check_p("studios", "v");
        $data['title'] = lang('studios');
        $data['studios'] = $this->Studios_model->get_data();
        $this->load->view("admin/pages/studios/index", $data);
    }

    function add() {
        check_p('studios', "c");
        set_active("add_studio");
        $this->form_validation->set_rules("n_en", lang("name_en"), "trim|required");
        $this->form_validation->set_rules("n_ar", lang("name_ar"), "trim|required");
        $this->form_validation->set_rules("code", lang("code"), "trim|required");
        $this->form_validation->set_rules("location", lang("location"), "trim|required");
        $this->form_validation->set_rules("rent_price", lang("rent_price"), "trim|required|numeric");
        $this->form_validation->set_rules("rent_term", lang("rent_term"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('add_studio');
            $data['locations'] = get_data_list(['c' => 6]);
            $this->load->view('admin/pages/studios/add', $data);
        } else {
            $data = [
                'n_en' => $this->input->post('n_en'),
                'n_ar' => $this->input->post('n_ar'),
                'code' => $this->input->post('code'),
                'location_id' => $this->input->post('location'),
                'description' => $this->input->post('description'),
                'overview' => $this->input->post('overview'),
                'rent_price' => $this->input->post('rent_price'),
                'rent_term' => ($this->input->post('rent_term') == 1) ? 1 : 2,
                'type' => 1,
            ];
            if ($_FILES['userfile']['name'] != '') {
                $config['upload_path'] = './uploads/studios';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $this->load->library("upload", $config);
                if ($this->upload->do_upload()) {
                    $upload_data = $this->upload->data();
                    $data['img'] = $upload_data['file_name'];
                } else {
                    $this->session->set_flashdata("error", lang("img_could_not_be_uploaded"));
                    redirect(base_url('admin/Studios/add'));
                }
            }
            $this->Global_model->global_insert('products', $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/Studios'));
        }
    }

    function edit($id = NULL) {
        check_p('studios', "u");
        set_active("edit_studio");
        check_param($id);
        $one = $this->Studios_model->get_info($id);
        check_param($one, "resource");
        $this->form_validation->set_rules("n_en", lang("name_en"), "trim|required");
        $this->form_validation->set_rules("n_ar", lang("name_ar"), "trim|required");
        $this->form_validation->set_rules("code", lang("code"), "trim|required");
        $this->form_validation->set_rules("location", lang("location"), "trim|required");
        $this->form_validation->set_rules("rent_price", lang("rent_price"), "trim|required|numeric");
        $this->form_validation->set_rules("rent_term", lang("rent_term"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('edit_studio');
            $data['one'] = $one;
            $data['id'] = $id;
            $data['locations'] = get_data_list(['c' => 6]);
            $this->load->view('admin/pages/studios/edit', $data);
        } else {
            $data = [
                'n_en' => $this->input->post('n_en'),
                'n_ar' => $this->input->post('n_ar'),
                'code' => $this->input->post('code'),
                'location_id' => $this->input->post('location'),
                'description' => $this->input->post('description'),
                'overview' => $this->input->post('overview'),
                'rent_price' => $this->input->post('rent_price'),
                'rent_term' => ($this->input->post('rent_term') == 1) ? 1 : 2
            ];
            if ($_FILES["userfile"]["name"] != "") {
                $config['upload_path'] = './uploads/studios';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $this->load->library("upload", $config);
                if ($this->upload->do_upload()) {
                    $upload_data = $this->upload->data();
                    $data['img'] = $upload_data['file_name'];
                    unlink("./uploads/studios/$one->img");
                } else {
                    $this->session->set_flashdata("error", lang("img_could_not_be_uploaded"));
                    redirect(base_url('admin/Studios/edit/' . $id));
                }
            }
            $this->Global_model->global_update('products', $id, $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/Studios'));
        }
    }

    function delete_studio($id = NULL) {
        check_p('studios', "d");
        $one = $this->Global_model->get_data_by_id('products', $id);
        check_param($one, "resource");
        if ($one) {
            $this->Global_model->global_update('products', $id, ["deleted" => 1]);
            $this->session->set_flashdata("success", lang("data_deleted_successfully"));
            redirect(base_url('admin/Studios'));
        } else {
            $error = lang("data_not_found");
            $this->session->set_flashdata("error", $error);
            redirect(base_url('admin/Studios'));
        }
    }

    function items($id = NULL) {
        check_param($id);
        $one = $this->Studios_model->get_info($id);
        check_param($one, "resource");
        $data['title'] = lang('items');
        $data['id'] = $id;
        $data['items'] = $this->Global_model->get_data_by_colID('product_items', 'product_id', $id);
        $data['studio'] = $one;
        $this->load->view("admin/pages/studios/items", $data);
    }

    function add_item($id = NULL) {
        check_param($id);
        $one = $this->Studios_model->get_info($id);
        check_param($one, "resource");
        $this->form_validation->set_rules("n_en", lang("name_en"), "trim|required");
        $this->form_validation->set_rules("n_ar", lang("name_ar"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('add_item');
            $data['id'] = $id;
            $data['studio'] = $this->Studios_model->get_info($id);
            $this->load->view("admin/pages/studios/add_item", $data);
        } else {
            $data = [
                'product_id' => $id,
                'n_en' => $this->input->post('n_en'),
                'n_ar' => $this->input->post('n_ar'),
                'cost_price' => $this->input->post('cost_price'),
                'description' => $this->input->post('description')
            ];
            $this->Global_model->global_insert('product_items', $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/Studios/items/$id"));
        }
    }

    function edit_item($id = NULL) {
        check_param($id);
        $one = get_product_item_by_id($id);
        check_param($one, "resource");
        $product = $this->Studios_model->get_info($one->product_id);
        check_param($product, "resource");
        $this->form_validation->set_rules("n_en", lang("name_en"), "trim|required");
        $this->form_validation->set_rules("n_ar", lang("name_ar"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('edit_item');
            $data['one'] = $one;
            $data['studio'] = $product;
            $data['id'] = $id;
            $this->load->view('admin/pages/studios/edit_item', $data);
        } else {
            $data = [
                'n_en' => $this->input->post('n_en'),
                'n_ar' => $this->input->post('n_ar'),
                'cost_price' => $this->input->post('cost_price'),
                'description' => $this->input->post('description')
            ];
            $this->Global_model->global_update('product_items', $id, $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/Studios/items/$one->product_id"));
        }
    }

    function delete_item($id = NULL) {
        check_param($id);
        $one = get_product_item_by_id($id);
        check_param($one, "resource");
        $product = $this->Studios_model->get_info($one->product_id);
        check_param($product, "resource");
        if ($one) {
            $this->Global_model->global_update('product_items', $id, ["deleted" => 1]);
            $this->session->set_flashdata("success", lang("data_deleted_successfully"));
            redirect(base_url("admin/Studios/items/$one->product_id"));
        } else {
            $error = lang("data_not_found");
            $this->session->set_flashdata("error", $error);
            redirect(base_url("admin/Studios/items/$one->product_id"));
        }
    }

}
