<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Survey extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Survey_model');
        $this->load->model('admin/Settings_model');
        set_active("surveys");
    }

    function index() {
        check_p("surveys", "v");
        set_active("surveys");
        $data = ['title' => lang('surveys')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['surveys'] = $this->Survey_model->get_surveys();
        $this->load->view("admin/pages/surveys/index", $data);
    }

    function delete_survey($id = NULL) {
        check_p("surveys", "d");
        check_param($id);
        $this->Global_model->global_update('surveys', $id, ["deleted" => 1]);
        $this->session->set_flashdata("success", lang("srv_del_success"));
        redirect(base_url('admin/Survey'));
    }

    function active_survey($id = NULL) {
        check_p("surveys", "e");
        check_param($id);
        $this->Survey_model->set_survey_deactivate($id, ["active" => 0]);
        $this->Global_model->global_update('surveys', $id, ["active" => 1]);
        $this->session->set_flashdata("success", lang("srv_act_success"));
        redirect(base_url('admin/Survey'));
    }

    function deactivate_survey($id = NULL) {
        check_p("surveys", "e");
        check_param($id);
        $this->Global_model->global_update('surveys', $id, ["active" => 0]);
        $this->session->set_flashdata("success", lang("srv_deact_success"));
        redirect(base_url('admin/Survey'));
    }

    function add_survey() {
        check_p("surveys", "c");
        set_active("questions_types");
        $this->form_validation->set_rules('title', lang('title'), 'trim|required');
        $this->form_validation->set_rules('count_to_send', lang('count_to_send'), 'trim|required|numeric');
        if (!$this->form_validation->run()) {
            $data = ['title' => lang('create_survey')];
            $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
            $this->load->view("admin/pages/surveys/add", $data);
        } else {
            if ($this->input->post('count_to_send') <= 0) {
                $this->session->set_flashdata("error", lang("check_count"));
                redirect('admin/add_survey');
            }
            $data = [
                'title' => $this->input->post('title'),
                'count' => $this->input->post('count_to_send'),
                'description' => $this->input->post('description')
            ];
            $this->Global_model->global_insert('surveys', $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect('admin/Survey');
        }
    }

    function edit_survey($id = NULL) {
        check_p("surveys", "u");
        check_param($id);
        $survey = $this->Global_model->get_data_by_id('surveys', $id);
        check_param($survey, "resource");
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules('count_to_send', lang('count_to_send'), 'trim|required|numeric');
        if (!$this->form_validation->run()) {
            $data = ['title' => lang('edit_survey')];
            $data['survey_id'] = $id;
            $data['survey_data'] = $this->Global_model->get_data_by_id('surveys', $id);
            $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
            $this->load->view("admin/pages/surveys/edit", $data);
        } else {
            if ($this->input->post('count_to_send') <= 0) {
                $this->session->set_flashdata("error", lang("check_count"));
                redirect('admin/edit_survey/'.$id);
            }
            $data = [
                'title' => $this->input->post('title'),
                'count' => $this->input->post('count_to_send'),
                'description' => $this->input->post('description')
            ];
            $this->Global_model->global_update('surveys', $id, $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect('admin/Survey');
        }
    }

    function get_questions($id = NULL) {
        check_p("surveys", "v");
        check_param($id);
        $survey = $this->Survey_model->get_survey_info($id);
        check_param($survey, "resource");
        $data = ['title' => lang('survey_questions')];
        $data['survey_id'] = $id;
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['label_att'] = array('class' => 'col-sm-4');
        $data['options_groups'] = $this->Settings_model->get_options_groups();
        $data['survey_data'] = $survey;
        $data['survey_questions'] = $this->Survey_model->get_related_survey_questions($id);
        $this->load->view("admin/pages/surveys/questions", $data);
    }

    function add_survey_question() {
        check_p("surveys", "c");
        $postData = $this->input->post();
        $insertArr['survey_id'] = $postData['survey_id'];
        $insertArr['question'] = $postData['question'];
        $insertArr['answer_type'] = $postData['answer_type'];
        $survey_data = $this->Global_model->get_data_by_id('surveys', $insertArr['survey_id']);
        if ($survey_data) {
            $this->Global_model->global_insert("survey_questions", $insertArr);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            echo json_encode(array('flag' => TRUE));
        } else {
            $this->session->set_flashdata("error", lang("survey_not_found"));
            echo json_encode(array('flag' => FALSE, 'msg' => $msg));
        }
    }

    function delete_question($survey_id = NULL, $id = NULL) {
        check_p("surveys", "d");
        check_param($survey_id);
        check_param($id);
        $this->Global_model->global_update('survey_questions', $id, ["deleted" => 1]);
        $this->session->set_flashdata("success", lang("que_del_success"));
        redirect(base_url('admin/Survey/get_questions/' . $survey_id));
    }

    function edit_survey_question() {
        check_p("surveys", "u");
        $postData = $this->input->post();
        $id = $postData['id'];
        $updateArr['question'] = $postData['ques_val'];
        $updateArr['answer_type'] = $postData['ans_type'];
        $this->Global_model->global_update('survey_questions', $id, $updateArr);
        $this->session->set_flashdata('success', lang('que_upd_success'));
        echo json_encode(TRUE);
    }

    function show_answers($id = NULL) {
        check_p("surveys", "v");
        check_param($id);
        $survey = $this->Survey_model->get_survey_info($id);
        check_param($survey, "resource");
        check_p("surveys", "v");
        $data = ['title' => lang('survey_answers')];
        $data['survey_id'] = $id;
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['survey_data'] = $survey;
        $data['survey_questions'] = $this->Survey_model->get_survey_questions($id);
        $data['options_groups'] = $this->Settings_model->get_options_groups();
        $this->load->view("admin/pages/surveys/show_answers", $data);
    }

    function get_survey_clients($id = NULL) {
        check_p("surveys", "v");
        check_param($id);
        $survey = $this->Survey_model->get_survey_info($id);
        check_param($survey, "resource");
        $data = ['title' => lang('clients_survey')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['survey_id'] = $id;
        $data['survey_data'] = $survey;
        $data['survey_clients'] = $this->Survey_model->get_survey_clients($id);
        $this->load->view("admin/pages/surveys/show_long_questions", $data);
    }

    function show_client_ans($survey_id = NULL, $client_id = NULL) {
        check_p("surveys", "v");
        check_param($survey_id);
        check_param($client_id);
        $survey_data = $this->Survey_model->get_survey_info($survey_id);
        check_param($survey_data, "resource");
        $client_data = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($client_data, "resource");
        $client_survey_b_t = $this->Survey_model->get_client_survey_b_t_submitted($survey_id, $client_id);
        $data = ['title' => lang('clients_answers')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['survey_id'] = $survey_id;
        $data['client_id'] = $client_id;
        $data['survey_data'] = $survey_data;
        $data['client_data'] = $client_data;
        $data['client_survey_b_t'] = $this->Survey_model->get_client_survey_b_t_submitted($survey_id, $client_id);
        $data['client_answers'] = $this->Survey_model->get_client_answers($survey_id, $client_id);
        $this->load->view("admin/pages/surveys/long_questions_answers", $data);
    }

    function edit_answers($survey_id = NULL, $client_id = NULL) {
        check_p("surveys", "v");
        check_param($survey_id);
        check_param($client_id);
        $survey_data = $this->Survey_model->get_survey_info($survey_id);
        $client_data = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($survey_data, "resource");
        check_param($client_data, "resource");
        $data = ['title' => lang('answers')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['survey_id'] = $survey_id;
        $data['client_id'] = $client_id;
        $data['client'] = $client_data;
        $check_exists_code = $this->Survey_model->get_survey_basic_by_client($survey_id, $client_id);
        if (!empty($check_exists_code)) {
            $data['survey_answers'] = $this->Survey_model->get_survey_answers_by_client($survey_id, $client_id);
            if ($data['survey_answers']) {
                $data['active_survey'] = $this->Survey_model->get_current_survey($survey_id);
                $this->load->view("admin/pages/surveys/client_survey", $data);
            } else {
                redirect('admin/Survey');
            }
        } else {
            redirect('admin/Survey');
        }
    }

    function submit_survey($survey_id = NULL, $client_id = NULL) {
        check_p("surveys", "u");
        check_param($survey_id);
        $survey_data = $this->Survey_model->get_survey_info($survey_id);
        $client_data = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($survey_data, "resource");
        check_param($client_data, "resource");
        $postData = $this->input->post();
        $client_survey_b_t = $this->Survey_model->get_survey_basic_by_client($survey_id, $client_id);
        if (!empty($client_survey_b_t)) {
            $this->Global_model->global_update('survey_client_b_t', $client_survey_b_t->id, ['submitted' => 1]);
        }
        foreach ($postData as $key => $val) {
            $survey_answer = $this->Survey_model->get_survey_question_by_client($survey_id, $client_id, $key);
            if ($survey_answer->answer_type != 0) {
                list($one, $two) = explode(',', $val);
                $this->Global_model->global_update('survey_answers', $survey_answer->id, ['answer_value' => $one, 'answer_name' => $two, 'submitted' => 1]);
            } else {
                $this->Global_model->global_update('survey_answers', $survey_answer->id, ['answer_value' => $val, 'answer_name' => $val, 'submitted' => 1]);
            }
        }
        $this->session->set_flashdata("success", lang("data_submitted_successfully"));
        redirect('admin/Survey/show_client_ans/' . $survey_id . '/' . $client_id);
    }
}
