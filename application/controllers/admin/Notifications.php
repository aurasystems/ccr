<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notifications extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init(NULL);
    }

    public function index() {
        if (uid() != '') {
            $data['title'] = lang('lang_notifications');
            $data['page_title'] = lang('lang_view_all_notifications');
            $data['items'] = read_notifications(['u' => uid()], TRUE, TRUE);
            $this->load->view('admin/pages/notifications', $data);
        } else {
            redirect('admin/Login');
        }
    }

    function set_seen() {
        if (uid() != '') {
            $ntf_id = $this->input->post('ntf_id');
            set_seen($ntf_id);
            echo json_encode(TRUE);
        } else {
            echo json_encode(FALSE);
        }
    }

    function get_ajax() {
        if (uid() != '') {
            $result = [];
            //$count = '';
            //$get_notifications = read_notifications(['u' => uid(), 'us' => TRUE], TRUE);
            $test_notifications = read_notifications(['t' => 40, 'us' => TRUE], TRUE);
            //$count_notifications = $get_notifications ? count($get_notifications) : '';
            $count_test_notifications = $test_notifications ? count($test_notifications) : '';
            $count = $count_notifications . '' . $count_test_notifications;
            $data = '';
            if ($test_notifications) {
                foreach ($test_notifications as $ntf) {
                    $data .= $this->load->view('admin/private/ntf_item', ['ntf' => $ntf], TRUE);
                }
            } else {
                $data .= $this->load->view('admin/private/no_ntf_item', [], TRUE);
            }
            $result = ['status' => 'success', 'count' => $count_test_notifications, 'html' => $data];
            echo json_encode($result);
        } else {
            echo json_encode(['status' => 'logout']);
        }
    }

}
