<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories extends CI_Controller {

    private $code = NULL;
    public $idf = NULL;
    public $idfs = NULL;
    private $sh = NULL;

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->code = 8;
        $this->idf = get_sys_idf($this->code);
        $this->idfs = 'category';
        $this->sh = 'cat';
        $this->load->model('admin/' . ucfirst($this->idf) . '_model', $this->sh);
        set_active($this->idf);
    }

    function index() {
        check_p($this->idf, "v");
        $data['title'] = lang($this->idf);
        $data[$this->idf] = get_data_list(['c' => $this->code]);
        $this->load->view("admin/pages/" . $this->idf . "/index", $data);
    }

    function sub($id = NULL) {
        check_p($this->idf, "v");
        $one = get_single_date(['c' => $this->code, 'i' => $id, 'chk' => TRUE]);
        $data['title'] = lang('sub_categories');
        $data['parent'] = $one;
        $data['sub'] = get_data_list(['c' => $this->code, 'p' => $id]);
        $this->load->view("admin/pages/" . $this->idf . "/sub", $data);
    }

    public function add() {
        check_p($this->idf, "c");
        $this->form_validation->set_rules("n_en", lang("name_en"), "trim|required");
        $this->form_validation->set_rules("n_ar", lang("name_ar"), "trim|required");
        $this->form_validation->set_rules("is_camera", lang("is_camera"), "trim");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('add_category');
            $data['parents'] = $this->Global_model->get_all_active('categories');
            $this->load->view('admin/pages/' . $this->idf . '/add', $data);
        } else {
            $data = [
                'n_en' => $this->input->post('n_en'),
                'n_ar' => $this->input->post('n_ar'),
                'description' => $this->input->post('description'),
                'parent' => $this->input->post('parent'),
                'is_camera' => $this->input->post("is_camera") ? 1 : 0
            ];
            if ($_FILES['userfile']['name'] != '') {
                $config['upload_path'] = './uploads/categories';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $this->load->library("upload", $config);
                if ($this->upload->do_upload()) {
                    $upload_data = $this->upload->data();
                    $data['img'] = $upload_data['file_name'];
                } else {
                    $this->session->set_flashdata("error", lang("img_could_not_be_uploaded"));
                    redirect(base_url('admin/' . ucfirst($this->idf) . '/add'));
                }
            }
            $this->Global_model->global_insert($this->idf, $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/' . ucfirst($this->idf)));
        }
    }

    public function edit($id = NULL) {
        check_p($this->idf, "u");
        $one = get_single_date(['c' => $this->code, 'i' => $id, 'chk' => TRUE]);
        $this->form_validation->set_rules("n_en", lang("name_en"), "trim|required");
        $this->form_validation->set_rules("n_ar", lang("name_ar"), "trim|required");
        $this->form_validation->set_rules("is_camera", lang("is_camera"), "trim");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('edit_category');
            $data['one'] = $one;
            $data['parents'] = get_data_list(['c' => $this->code]);
            $this->load->view('admin/pages/' . $this->idf . '/edit', $data);
        } else {
            $data = [
                'n_en' => $this->input->post('n_en'),
                'n_ar' => $this->input->post('n_ar'),
                'description' => $this->input->post('description'),
                'parent' => $this->input->post('parent'),
                'is_camera' => $this->input->post("is_camera") ? 1 : 0
            ];
            if ($_FILES["userfile"]["name"] != "") {
                $config['upload_path'] = './uploads/categories';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $this->load->library("upload", $config);
                if ($this->upload->do_upload()) {
                    $upload_data = $this->upload->data();
                    $data['img'] = $upload_data['file_name'];
                    unlink("./uploads/categories/$one->img");
                } else {
                    $this->session->set_flashdata("error", lang("img_could_not_be_uploaded"));
                    redirect(base_url('admin/' . ucfirst($this->idf) . '/edit/' . $id));
                }
            }
            $this->Global_model->global_update($this->idf, $id, $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/' . ucfirst($this->idf)));
        }
    }

    public function delete($id = NULL) {
        check_p($this->idf, "d");
        $one = get_single_date(['c' => $this->code, 'i' => $id, 'chk' => TRUE]);
        if ($one) {
            $sub = get_data_list(['c' => $this->code, 'p' => $id]);
            $sub_error = $sub ? lang("check_sub_category") : NULL;
            if ($sub_error) {
                $this->session->set_flashdata("error", $sub_error);
                redirect(base_url('admin/' . ucfirst($this->idf)));
            }
            $this->Global_model->global_update($this->idf, $id, ["deleted" => 1]);
            $this->session->set_flashdata("success", lang("data_deleted_successfully"));
            redirect(base_url('admin/' . ucfirst($this->idf)));
        } else {
            $error = lang("category_not_found");
            $this->session->set_flashdata("error", $error);
            redirect(base_url('admin/' . ucfirst($this->idf)));
        }
    }

    function categories() {
        $data = $this->cat->get_categories();
        //echo "<ul>";
        $this->fetch_menu($data);
        //echo "</ul>";
        die;
    }

    function fetch_menu($data) {
        foreach ($data as $menu) {
            echo "<li>" . $menu->{'n_' . lc()} . "</li>";
            if (!empty($menu->sub)) {
                echo "<ul>";
                $this->fetch_sub_menu($menu->sub);
                echo "</ul>";
            }
        }
    }

    function fetch_sub_menu($sub_menu) {
        foreach ($sub_menu as $menu) {
            echo "<li>" . $menu->{'n_' . lc()} . "</li>";
            if (!empty($menu->sub)) {
                echo "<ul>";
                fetch_sub_menu($menu->sub);
                echo "</ul>";
            }
        }
    }

}
