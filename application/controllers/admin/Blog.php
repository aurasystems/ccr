<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Blog extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Blog_model');
    }

    function index() {
        check_p("blog", "v");
        set_active("blog");
        $data['title'] = lang('blog');
        $data['blog'] = $this->Blog_model->get_data();
        $this->load->view("admin/pages/blog/index", $data);
    }

    function categories() {
        check_p("blog_categories", "v");
        set_active("blog_categories");
        $data['title'] = lang('blog_categories');
        $data['categories'] = $this->Blog_model->get_categories();
        $this->load->view("admin/pages/blog/categories", $data);
    }

    function add_category() {
        check_p("blog_categories", "c");
        $today = date('Y-m-d');
        $n_en = $this->input->post('n_en');
        $n_ar = $this->input->post('n_ar');
        if (empty($n_en) || empty($n_ar)) {
            $this->session->set_flashdata('error', lang('fields_required'));
            redirect('admin/Blog/categories');
        }
        $data = [
            'n_en' => $n_en,
            'n_ar' => $n_ar
        ];
        $inserted = $this->Global_model->global_insert('blog_categories', $data);
        if ($inserted) {
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect('admin/Blog/categories');
        } else {
            $this->session->set_flashdata('error', lang('error_occured'));
            redirect('admin/Blog/categories');
        }
    }

    function update_category() {
        check_p("blog_categories", "u");
        $postData = $this->input->post();
        $id = $postData['id'];
        $n_en = $postData['n_en'];
        $n_ar = $postData['n_ar'];
        $one = $this->Global_model->get_data_by_id('blog_categories', $id);
        if ($one) {
            $this->Global_model->global_update('blog_categories', $id, ["n_en" => $n_en, "n_ar" => $n_ar]);
            $this->session->set_flashdata('success', lang('booking_updated_successfully'));
            echo json_encode(TRUE);
        } else {
            $this->session->set_flashdata("error", lang('data_not_found'));
            echo json_encode(FALSE);
        }
    }

    function delete_category($id = NULL) {
        check_p("blog_categories", "d");
        check_param($id);
        $this->Global_model->global_update('blog_categories', $id, ["deleted" => 1]);
        $this->session->set_flashdata("success", lang("data_del_successfully"));
        redirect(base_url('admin/Blog/categories'));
    }

    function publish_blog($id = NULL) {
        check_p("blog", "e");
        check_param($id);
        $this->Global_model->global_update('blog', $id, ["status" => 1]);
        $this->session->set_flashdata("success", lang("blog_pub_success"));
        redirect(base_url('admin/Blog'));
    }

    function unpublish_blog($id = NULL) {
        check_p("blog", "e");
        check_param($id);
        $this->Global_model->global_update('blog', $id, ["status" => 0]);
        $this->session->set_flashdata("success", lang("blog_unpub_success"));
        redirect(base_url('admin/Blog'));
    }

    function delete_blog($id = NULL) {
        check_p("blog", "d");
        check_param($id);
        $this->Blog_model->delete_blog_tags($id);
        $this->Global_model->global_update('blog', $id, ["deleted" => 1]);
        $this->session->set_flashdata("success", lang("blog_del_success"));
        redirect(base_url('admin/Blog'));
    }

    function add() {
        check_p('blog', "c");
        set_active("add_blog");
        $this->form_validation->set_rules("category", lang("category"), "trim|required");
        $this->form_validation->set_rules("title", lang("title"), "trim|required");
        $this->form_validation->set_rules("content", lang("content"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('add_blog');
            $data['categories'] = $this->Blog_model->get_categories();
            $this->load->view('admin/pages/blog/add', $data);
        } else {
            if (empty($this->input->post('category'))) {
                $this->session->set_flashdata("error", lang("specify_category"));
                redirect(base_url('admin/Blog/add'));
            }
            $ptags = str_replace(" ", ",", $this->input->post('tags'));
            $tags = explode(",", $ptags);
            $data = [
                'category_id' => $this->input->post('category'),
                'title' => $this->input->post('title'),
                'content' => $this->input->post('content'),
                'created_by' => uid()
            ];
            if ($_FILES['userfile']['name'] != '') {
                $config['upload_path'] = './uploads/blogs';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $this->load->library("upload", $config);
                if ($this->upload->do_upload()) {
                    $upload_data = $this->upload->data();
                    $data['img'] = $upload_data['file_name'];
                    $this->Blog_model->resizeImage($upload_data['file_name'], 1650, 680);
                } else {
                    $this->session->set_flashdata("error", lang("img_could_not_be_uploaded"));
                    redirect(base_url('admin/Blog/add'));
                }
            }
            $inserted_id = $this->Global_model->global_insert('blog', $data);
            if ($inserted_id) {
                // Insert tags if found
                if (!empty($tags)) {
                    foreach ($tags as $tag) {
                        $dtage = [
                            'blog_id' => $inserted_id,
                            'tag' => $tag
                        ];
                        $this->Global_model->global_insert('blog_tags', $dtage);
                    }
                }
            }
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/Blog'));
        }
    }

    function edit($id = NULL) {
        check_p('blog', "u");
        set_active("edit_blog");
        check_param($id);
        $blog = $this->Blog_model->get_info($id);
        check_param($blog, "resource");
        $this->form_validation->set_rules("category", lang("category"), "trim|required");
        $this->form_validation->set_rules("title", lang("title"), "trim|required");
        $this->form_validation->set_rules("content", lang("content"), "trim|required");
        if (!$this->form_validation->run()) {
            $final_tags = '';
            $data['title'] = lang('edit_blog');
            $data['blog'] = $blog;
            $data['id'] = $id;
            $data['categories'] = $this->Blog_model->get_categories();
            $tags = $this->Blog_model->get_tags($id);
            if (!empty($tags)) {
                foreach ($tags as $tag) {
                    $final_tags .= $tag->tag . ',';
                }
            }
            $data['tags'] = rtrim($final_tags, ',');
            $this->load->view('admin/pages/blog/edit', $data);
        } else {
            if (empty($this->input->post('category'))) {
                $this->session->set_flashdata("error", lang("specify_category"));
                redirect(base_url('admin/Blog/edit/' . $id));
            }
            $ptags = str_replace(" ", ",", $this->input->post('tags'));
            $result_tags = explode(",", $ptags);
            $data = [
                'category_id' => $this->input->post('category'),
                'title' => $this->input->post('title'),
                'content' => $this->input->post('content')
            ];
            if ($_FILES['userfile']['name'] != '') {
                $config['upload_path'] = './uploads/blogs';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $this->load->library("upload", $config);
                if ($this->upload->do_upload()) {
                    $upload_data = $this->upload->data();
                    $data['img'] = $upload_data['file_name'];
                    $this->Blog_model->resizeImage($upload_data['file_name'], 1650, 680);
                } else {
                    $this->session->set_flashdata("error", lang("img_could_not_be_uploaded"));
                    redirect(base_url('admin/Blog/edit/' . $id));
                }
            }
            $this->Global_model->global_update('blog', $id, $data);
            // Empty tags and insert new
            $this->Blog_model->delete_blog_tags($id);
            if (!empty($result_tags)) {
                foreach ($result_tags as $single) {
                    if (!empty($single)) {
                        $dtage = [
                            'blog_id' => $id,
                            'tag' => $single
                        ];
                        $this->Global_model->global_insert('blog_tags', $dtage);
                    }
                }
            }
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/Blog'));
        }
    }

    function show_comments($blog_id = NULL) {
        check_param($blog_id);
        $one = $this->Global_model->get_data_by_id('blog', $blog_id);
        check_param($one, "resource");
        $data['title'] = lang('show_comments');
        $data['blog_id'] = $blog_id;
        $data['comments'] = $this->Blog_model->get_comments($blog_id);
        $data['one'] = $one;
        $this->load->view("admin/pages/blog/comments", $data);
    }

    function delete_comment($blog_id = NULL, $comment_id = NULL) {
        check_p("blog", "d");
        check_param($blog_id);
        check_param($comment_id);
        $this->Global_model->global_update('blog_comments', $comment_id, ["deleted" => 1]);
        $this->session->set_flashdata("success", lang("comm_del_success"));
        redirect(base_url('admin/Blog/show_comments/' . $blog_id));
    }

}
