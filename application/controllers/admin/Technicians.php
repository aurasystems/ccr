<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Technicians extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Technicians_model');
        set_active("technicians");
    }

    function index() {
        check_p("technicians", "v");
        $data['title'] = lang('technicians');
        $data['technicians'] = $this->Technicians_model->get_data();
        $this->load->view("admin/pages/technicians/index", $data);
    }

    function add() {
        check_p('technicians', "c");
        set_active("add_technician");
        $this->form_validation->set_rules("n_en", lang("name_en"), "trim|required");
        $this->form_validation->set_rules("n_ar", lang("name_ar"), "trim|required");
        $this->form_validation->set_rules("code", lang("code"), "trim|required");
        $this->form_validation->set_rules("location", lang("location"), "trim|required");
        $this->form_validation->set_rules("technician_type", lang("type"), "trim|required");
        $this->form_validation->set_rules("rent_price", lang("rent_price"), "trim|required|numeric");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('add_technician');
            $data['locations'] = get_data_list(['c' => 6]);
            $data['tech_types'] = get_data_list(['c' => 10]);
            $this->load->view('admin/pages/technicians/add', $data);
        } else {
            $data = [
                'n_en' => $this->input->post('n_en'),
                'n_ar' => $this->input->post('n_ar'),
                'code' => $this->input->post('code'),
                'location_id' => $this->input->post('location'),
                'description' => $this->input->post('description'),
                'overview' => $this->input->post('overview'),
                'rent_price' => $this->input->post('rent_price'),
                'rent_term' => 2,
                'type' => 2,
                'technician_type' => $this->input->post('technician_type'),
            ];
            if ($_FILES['userfile']['name'] != '') {
                $config['upload_path'] = './uploads/technicians';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $this->load->library("upload", $config);
                if ($this->upload->do_upload()) {
                    $upload_data = $this->upload->data();
                    $data['img'] = $upload_data['file_name'];
                } else {
                    $this->session->set_flashdata("error", lang("img_could_not_be_uploaded"));
                    redirect(base_url('admin/Technicians/add'));
                }
            }
            $this->Global_model->global_insert('products', $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/Technicians'));
        }
    }

    function edit($id = NULL) {
        check_p('technicians', "u");
        set_active("edit_technician");
        check_param($id);
        $one = $this->Technicians_model->get_info($id);
        check_param($one, "resource");
        $this->form_validation->set_rules("n_en", lang("name_en"), "trim|required");
        $this->form_validation->set_rules("n_ar", lang("name_ar"), "trim|required");
        $this->form_validation->set_rules("code", lang("code"), "trim|required");
        $this->form_validation->set_rules("location", lang("location"), "trim|required");
        $this->form_validation->set_rules("technician_type", lang("type"), "trim|required");
        $this->form_validation->set_rules("rent_price", lang("rent_price"), "trim|required|numeric");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('edit_technician');
            $data['one'] = $one;
            $data['id'] = $id;
            $data['locations'] = get_data_list(['c' => 6]);
            $data['tech_types'] = get_data_list(['c' => 10]);
            $this->load->view('admin/pages/technicians/edit', $data);
        } else {
            $data = [
                'n_en' => $this->input->post('n_en'),
                'n_ar' => $this->input->post('n_ar'),
                'code' => $this->input->post('code'),
                'location_id' => $this->input->post('location'),
                'description' => $this->input->post('description'),
                'overview' => $this->input->post('overview'),
                'rent_price' => $this->input->post('rent_price'),
                'technician_type' => $this->input->post('technician_type'),
            ];
            if ($_FILES["userfile"]["name"] != "") {
                $config['upload_path'] = './uploads/technicians';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $this->load->library("upload", $config);
                if ($this->upload->do_upload()) {
                    $upload_data = $this->upload->data();
                    $data['img'] = $upload_data['file_name'];
                    unlink("./uploads/technicians/$one->img");
                } else {
                    $this->session->set_flashdata("error", lang("img_could_not_be_uploaded"));
                    redirect(base_url('admin/Technicians/edit/' . $id));
                }
            }
            $this->Global_model->global_update('products', $id, $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/Technicians'));
        }
    }

    function delete_technician($id = NULL) {
        check_p('technicians', "d");
        $one = $this->Global_model->get_data_by_id('products', $id);
        check_param($one, "resource");
        if ($one) {
            $this->Global_model->global_update('products', $id, ["deleted" => 1]);
            $this->Technicians_model->delete_timetable($id);
            $this->session->set_flashdata("success", lang("data_deleted_successfully"));
            redirect(base_url('admin/Technicians'));
        } else {
            $error = lang("data_not_found");
            $this->session->set_flashdata("error", $error);
            redirect(base_url('admin/Technicians'));
        }
    }

    function timetable($technician_id = NULL) {
        check_param($technician_id);
        $technician = $this->Technicians_model->get_info($technician_id);
        check_param($technician, "resource");
        $data['title'] = lang('timetable');
        $data['technician_id'] = $technician_id;
        $hours = $this->Technicians_model->get_technician_timetable($technician_id);
        $hours_array = [];
        foreach ($hours as $one) {
            $hours_array[$one->day] = new stdClass();
            $hours_array[$one->day]->time_from = $one->time_from;
            $hours_array[$one->day]->time_to = $one->time_to;
            $hours_array[$one->day]->day_off = $one->day_off;
        }
        $data['hours'] = $hours_array;
        $data['technician'] = $technician;
        $this->load->view('admin/pages/technicians/timetable', $data);
    }

    function insert_timetable($technician_id) {
        check_param($technician_id);
        $technician = $this->Technicians_model->get_info($technician_id);
        check_param($technician, "resource");
        for ($i = 1; $i <= 7; $i++) {
            $data = [
                'technician_id' => $technician_id,
                'day' => $i, // 1 (for Monday) through 7 (for Sunday)
                'time_from' => $this->input->post("time_from$i") ? date('H:i a', strtotime($this->input->post("time_from$i"))) : NULL,
                'time_to' => $this->input->post("time_to$i") ? date('H:i a', strtotime($this->input->post("time_to$i"))) : NULL,
                'day_off' => 0
            ];
            if ($this->input->post("day_off$i")) {
                $data['day_off'] = $this->input->post("day_off$i");
            }
            $this->Technicians_model->insert_timetable($data);
        }
        $this->session->set_flashdata("success", lang("data_submitted_successfully"));
        redirect(base_url("admin/Technicians/timetable/$technician_id"));
    }

    function edit_timetable($technician_id) {
        check_param($technician_id);
        $technician = $this->Technicians_model->get_info($technician_id);
        check_param($technician, "resource");
        for ($i = 1; $i <= 7; $i++) {
            $data = [
                'time_from' => $this->input->post("time_from$i") ? date('H:i a', strtotime($this->input->post("time_from$i"))) : NULL,
                'time_to' => $this->input->post("time_to$i") ? date('H:i a', strtotime($this->input->post("time_to$i"))) : NULL,
                'day_off' => 0
            ];
            if ($this->input->post("day_off$i")) {
                $data['day_off'] = $this->input->post("day_off$i");
            }
            $this->Technicians_model->update_timetable($technician_id, $i, $data);
        }
        $this->session->set_flashdata('msg', lang('data_submitted_successfully'));
        redirect(base_url("admin/Technicians/timetable/$technician_id"));
    }

}
