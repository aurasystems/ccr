<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Access_levels_model');
        set_active("users");
    }

    function index() {
        check_p("users", "v");
        $data = ['title' => lang('accounts')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['users'] = get_data_list(['c' => 1]);
        $this->load->view("admin/pages/user/index", $data);
    }

    function add() {
        check_p("users", "c");
        set_active("add_user");
        $this->form_validation->set_rules("access_level", lang("access_level"), "trim|required")
                ->set_rules("n_en", lang('name_in') . lang('lang_en'), "trim|required")
                ->set_rules("n_ar", lang('name_in') . lang('lang_ar'), "trim|required")
                ->set_rules('email', lang("email"), 'trim|required|valid_email|is_unique[users.email]')
                ->set_rules('attendance_id', lang("attendance_id"), 'trim|required|is_unique[users.attendance_id]')
                ->set_rules('password', lang("password"), 'trim|matches[password_confirm]|min_length[4]|max_length[32]')
                ->set_rules('password_confirm', lang("password_confirm"), 'trim|matches[password]');
        if (!$this->form_validation->run()) {
            $data['title'] = lang('add_account');
            $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form', 'autocomplete' => 'off'];
            $data['access_levels'] = get_data_list(['c' => 2]);
            $this->load->view("admin/pages/user/add", $data);
        } else {
            $acc_level = $this->input->post('access_level');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $acc_level_info = get_single_date(['c' => 2, 'i' => $acc_level, 'chk' => TRUE]);
            if ($acc_level_info) {
                $perms = $this->Access_levels_model->get_level_permissions($acc_level);
                if ($perms) {
                    //$ntf_type = (int) substr($perms->rcv_ntf, 0, 1);
                    $data = [
                        'level_id' => $acc_level,
                        'attendance_id' => $this->input->post('attendance_id'),
                        'notes' => $this->input->post('notes'),
                        'job_start_date' => ($this->input->post('job_start_date') != null) ? $this->input->post('job_start_date') : NULL,
                        'n_en' => $this->input->post('n_en'),
                        'n_ar' => $this->input->post('n_ar'),
                        'email' => $email,
                        'password' => do_hash($password, 'md5'),
                        //"ntf" => $ntf_type,
                        'added_by' => uid(),
                        "deleted" => 0,
                    ];
                    $user_id = $this->Global_model->global_insert("users", $data);
                    if ($user_id) {
                        // insert payroll
                        $sdata = [
                            'user_id' => $user_id,
                            'amount' => ($this->input->post('salary') != null) ? $this->input->post('salary') : 0
                        ];
                        $this->Global_model->global_insert("user_payroll", $sdata);
                    }
                    $this->session->set_flashdata("success", lang("data_submitted_successfully"));
                    if (settings('email_set_pass', 1)) {
                        $this->load->model('admin/Email_notifications_model');
                        $this->Email_notifications_model->send_account_info($email, $password);
                    }
                }
            }
            redirect(base_url("admin/User" . (!get_p("users", "v") ? '/add' : '')));
        }
    }

    public function edit($id = NULL) {
        check_p("users", "u");
        $one = get_single_date(['c' => 1, 'i' => $id, 'chk' => TRUE]);
        $this->form_validation->set_rules("access_level", lang("access_level"), "trim|required")
                ->set_rules("n_en", lang('name_in') . lang('lang_en'), "trim|required")
                ->set_rules("n_ar", lang('name_in') . lang('lang_ar'), "trim|required");
        if ($this->input->post('attendance_id') != $one->attendance_id) {
            $this->form_validation->set_rules('attendance_id', lang("attendance_id"), 'trim|required|is_unique[users.attendance_id]');
        }
        if ($this->input->post('email') != $one->email) {
            $this->form_validation->set_rules('email', lang("email"), 'trim|required|valid_email|is_unique[users.email]');
        }
        $this->form_validation->set_rules('password', lang("password"), 'trim|matches[password_confirm]|min_length[4]|max_length[32]')
                ->set_rules('password_confirm', lang("password_confirm"), 'trim|matches[password]');
        $this->load->model('admin/Users_model');
        if (!$this->form_validation->run()) {
            $data['title'] = lang('edit_account');
            $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form', 'autocomplete' => 'off'];
            $data['one'] = $one;
            $data['user_level'] = $this->Global_model->get_data_by_id('access_levels', $one->level_id);
            $data['id'] = $id;
            $data['access_levels'] = get_data_list(['c' => 2]);
            $this->load->view('admin/pages/user/edit', $data);
        } else {
            $acc_level = $this->input->post('access_level');
            $acc_level_info = $this->Global_model->get_data_by_id('access_levels', $acc_level);
            if ($acc_level_info) {
                $perms = $this->Access_levels_model->get_level_permissions($acc_level);
                if ($perms) {
                    //$ntf_type = (int) substr($perms->rcv_ntf, 0, 1);
                    $data = [
                        'level_id' => $acc_level,
                        'n_en' => $this->input->post('n_en'),
                        'n_ar' => $this->input->post('n_ar'),
                        'email' => $this->input->post('email'),
                        'attendance_id' => $this->input->post('attendance_id'),
                        'notes' => $this->input->post('notes'),
                        'job_start_date' => ($this->input->post('job_start_date') != null) ? $this->input->post('job_start_date') : NULL,
                        'job_end_date' => ($this->input->post('job_end_date') != null) ? $this->input->post('job_end_date') : NULL
                    ];
                    if ($this->input->post('password') != "" && $this->input->post('password_confirm') != "") {
                        $data['password'] = do_hash($this->input->post('password'), 'md5');
                    }
                    $this->Global_model->global_update("users", $id, $data);
                    // check for salary in payroll
                    $user_payroll = $this->Users_model->get_user_payroll($id);
                    if ($user_payroll) {
                        // update
                        $sdata = ['amount' => ($this->input->post('salary') != null) ? $this->input->post('salary') : 0];
                        $this->Global_model->global_update("user_payroll", $user_payroll->id, $sdata);
                    } else {
                        // insert
                        $indata = [
                            'user_id' => $id,
                            'amount' => ($this->input->post('salary') != null) ? $this->input->post('salary') : 0
                        ];
                        $this->Global_model->global_insert("user_payroll", $indata);
                    }
                    if ($id == uid()) {
                        $get_user = $this->Global_model->get_data_by_id('users', $id);
                        if ($get_user) {
                            //add user data to session
                            $new_data = [
                                'user_id' => $get_user->id,
                                'user_email' => $get_user->email,
                                'user_name_en' => $get_user->n_en,
                                'user_name_ar' => $get_user->n_ar,
                                'user_level_id' => $get_user->level_id,
                                'logged_in' => TRUE
                            ];
                            $this->session->set_userdata($new_data);
                        }
                        load_permissions($acc_level);
                        load_locations(uid());
                    }
                    $this->session->set_flashdata("success", lang("data_submitted_successfully"));
                }
            }
            redirect(base_url("admin/User"));
        }
    }

    function delete($id = NULL) {
        check_p("users", "d");
        $user_info = get_single_date(['c' => 1, 'i' => $id, 'chk' => TRUE]);
        $this->Global_model->global_update("users", $id, ["deleted" => 1, "email" => '', "deleted_email" => $user_info->email, "level_id" => NULL]);
        $this->session->set_flashdata("success", lang("data_deleted_successfully"));
        redirect(base_url("admin/User"));
    }

    function location() {
        $data = ['title' => lang('location')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['locations'] = $this->Global_model->get_data_not_deleted('locations');
        $last = $this->uri->total_segments();
        $user_id = $this->uri->segment($last);
        $data['user_location'] = user_locations($user_id);
        $this->form_validation->set_rules('city', lang("password"), 'trim|required');
        $cur_time = date('Y-m-d H:i:s');
        if ($this->form_validation->run()) {
            $branch_id = $this->input->post('city');
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');
            $is_tmp = $this->input->post('temp');
            $is_exist = $this->Access_levels_model->isFieldExist('user_locations', 'user_id', $user_id);
            if ($is_tmp == 1) {
                $this->form_validation->set_rules('start_date', lang("start_date"), 'trim|required|valid_date');
                $this->form_validation->set_rules('end_date', lang("end_date"), 'trim|required|valid_date');
                if (!$this->form_validation->run()) {
                    $this->session->set_flashdata("error", lang("you_must_fill_date_fields"));
                    $this->load->view("admin/pages/user/add_location", $data);
                } else {
                    if ($end_date < $start_date) {
                        $this->session->set_flashdata("error", lang("start_end_date_check"));
                        redirect(base_url("admin/User/location/$user_id"));
                    }
                    $data = [
                        'user_id' => $user_id,
                        'location_id' => $branch_id,
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                        'is_temp' => $is_tmp,
                    ];
                    if ($is_exist)
                        $this->Global_model->global_update_by_colName('user_locations', 'user_id', $user_id, $data);
                    else if (!$is_exist)
                        $this->Global_model->global_insert('user_locations', $data);
                    $this->session->set_flashdata("success", lang("location_added_succeffully"));
                }
            } else if ($is_tmp == 0) {
                $is_tmp = 0;
                $data = [
                    'user_id' => $user_id,
                    'location_id' => $branch_id,
                    'is_temp' => $is_tmp,
                    'start_date' => NULL,
                    'end_date' => NULL,
                ];
                if ($is_exist == 1)
                    $this->Global_model->global_update_by_colName('user_locations', 'user_id', $user_id, $data);
                else if (!$is_exist)
                    $this->Global_model->global_insert('user_locations', $data);
                $this->session->set_flashdata("success", lang("location_added_succeffully"));
            }
            redirect(base_url("admin/User/location/$user_id"));
        } else {
            $this->load->view("admin/pages/user/add_location", $data);
        }
    }

}
