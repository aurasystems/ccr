<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init(TRUE);
        date_default_timezone_set('Africa/Cairo');
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '2048M');
        $this->load->model('admin/Check_types_model', 'chktypes');
        $this->load->model('admin/Maintenance_model', 'maint');
        $this->load->model('admin/Booking_model', 'book');
        $this->load->model('admin/Newsletters_model', 'news');
        $this->load->model('admin/Clients_model', 'clients');
    }

    function index() {
        
    }

    function equipment_test() {
        $today = date('Y-m-d H:i:s');
        // Get all check types and check for last run
        $check_types = $this->chktypes->get_check_types();
        if (!empty($check_types)) {
            foreach ($check_types as $type) {
                // Get check type items
                $check_type_items = $this->chktypes->get_check_type_items($type->id);
                // Get product with related check type, then get related product items
                $products = $this->chktypes->get_products_wiz_check_type($type->id);
                // Check for last run with current datetime
                if ($type->unit == 1) {
                    // Run hourly
                    $checkTime = date('Y-m-d H:i:s', strtotime('+' . $type->run_every . ' hour', strtotime($type->last_run)));
                } elseif ($type->unit == 2) {
                    // Run daily
                    $checkTime = date('Y-m-d H:i:s', strtotime('+' . $type->run_every . ' day', strtotime($type->last_run)));
                } elseif ($type->unit == 3) {
                    // Run weekly
                    $checkTime = date('Y-m-d H:i:s', strtotime('+' . ($type->run_every * 7) . ' day', strtotime($type->last_run)));
                }
                if ($today >= $checkTime) {
                    if ($check_type_items) {
                        // Update last run in check types table
                        $this->Global_model->global_update('check_types', $type->id, ['last_run' => $today]);
                        // Insert into equipment test
                        $equip_data = [
                            'chk_type_id' => $type->id,
                            'datetime' => $today,
                            'completed' => 0
                        ];
                        $test_id = $this->Global_model->global_insert('equipment_test', $equip_data);
                        if ($test_id) {
                            $ntf_data = ['t' => 40, 'r' => $test_id];
                            notify($ntf_data);
                            foreach ($products as $product) {
                                $product_items = $this->chktypes->get_product_items($product->id);
                                foreach ($product_items as $item) {
                                    foreach ($check_type_items as $ctitem) {
                                        // Insert into equipment test log
                                        $equip_chks_data = [
                                            'test_id' => $test_id,
                                            'chk_t_i_id' => $ctitem->id,
                                            'p_id' => $product->id,
                                            'p_item_id' => $item->id,
                                            'is_checked' => 0
                                        ];
                                        $this->Global_model->global_insert('equipment_test_checks', $equip_chks_data);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $this->dsp(['status' => 200]);
    }

    function update_maintenance() {
        $is_maintenance_on = $this->maint->get_next_maint();
        if (!empty($is_maintenance_on)) {
            foreach ($is_maintenance_on as $one) {
                if ($one->product_id != null && $one->item_id == null) {//tech or studio
                    $this->global_model->global_update('products', $one->product_id, array('damaged' => 1));
                } else if ($one->item_id != null && $one->product_id == null) {
                    $data = array(
                        'damaged' => 1,
                        'is_exist' => 0,
                    );
                    //update cost for client
                    $this->global_model->global_update('product_items', $one->item_id, $data);
                }
                $this->global_model->global_update('maintenance', $one->id, array('is_applied' => 1));
            }
        }
    }

    function update_delivery_temp()
    {
        $today = date('Y-m-d H:i:s');
        $timestamp = $this->maint->get_delivery_temp_time('items_delivery_temp');
        if(!empty($timestamp))
        {
            foreach($timestamp as $one)
            {
                $date1 = $today;
                $date2 = $one->timestamp;
                $diff = abs(strtotime($date2) - strtotime($date1));
                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                if($days >= 2)
                $this->Global_model->global_delete('items_delivery_temp',$one->id);
            }
        }
        $timestamp = $this->maint->get_delivery_temp_time('sup_items_temp');
        if(!empty($timestamp))
        {
            foreach($timestamp as $one)
            {
                $date1 = $today;
                $date2 = $one->timestamp;
                $diff = abs(strtotime($date2) - strtotime($date1));
                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                if($days >= 2)
                $this->Global_model->global_delete('sup_items_temp',$one->id);
            }
        }
    }

    function check_missing_items() {
        $today = date('Y-m-d H:i:s');
        // Get all missing items
        $settings = settings('all', 3);
        $items = $this->book->get_all_missing_items();
        if (!empty($items)) {
            foreach ($items as $item) {
                $booking = $this->Global_model->get_data_by_id('booking', $item->book_id);
                $pitem_data = get_product_item_by_id($item->item_id);
                // Get timestamp and time for missing from settings and compare
                if (!empty($settings) && !empty($settings->time_for_missing)) {
                    $result_time = date('Y-m-d H:i:s', strtotime('+' . $settings->time_for_missing . ' hour', strtotime($item->timestamp)));
                } else {
                    $result_time = $item->timestamp;
                }
                if ($today > $result_time) {
                    $wallet = [
                        'client_id' => $booking->client_id,
                        'booking_id' => $item->book_id,
                        'item_id' => $item->item_id,
                        'due_to' => 2,
                        'amount' => (-1) * $pitem_data->cost_price,
                    ];
                    $inserted = $this->Global_model->global_insert('wallet', $wallet);
                    $this->Global_model->global_update('booking_items_return', $item->id, ['missing_pen_applied' => 1]);
                    // set item as lost
                    $this->Global_model->global_update('product_items', $item->item_id, ["missing" => 2]);
                }
            }
        }
        $this->dsp(['status' => 200]);
    }

    function send_newsletter() {
        $today = date('Y-m-d H:i:s');
        // Get active newsletter and check for last run
        $newsletter = $this->news->get_active_news();
        if (!empty($newsletter)) {
            // Get related clients to send (all or spcific)
            $clients = (!empty($this->news->get_newsletter_clients($newsletter->id))) ? $this->news->get_newsletter_clients($newsletter->id) : $this->clients->get_approved_limit_data();
//            print_r($clients);
//            die;
            // Check for last run with current datetime
            if ($newsletter->unit == 1) {
                // Run hourly
                $checkTime = date('Y-m-d H:i:s', strtotime('+' . $newsletter->run_every . ' hour', strtotime($newsletter->last_run)));
            } elseif ($newsletter->unit == 2) {
                // Run daily
                $checkTime = date('Y-m-d H:i:s', strtotime('+' . $newsletter->run_every . ' day', strtotime($newsletter->last_run)));
            } elseif ($newsletter->unit == 3) {
                // Run weekly
                $checkTime = date('Y-m-d H:i:s', strtotime('+' . ($newsletter->run_every * 7) . ' day', strtotime($newsletter->last_run)));
            }
            if ($today >= $checkTime) {
                if (!empty($clients)) {
                    // Update last run in check types table
                    $this->Global_model->global_update('newsletters', $newsletter->id, ['last_run' => $today]);
                    foreach ($clients as $client) {
                        // Email client with newsletter
                        $data = [
                            'to' => $client->email,
                            'subject' => $newsletter->subject,
                            'message' => $newsletter->mail_body
                        ];
                        send_email($data);
                    }
                }
            }
        }
        $this->dsp(['status' => 200]);
    }

    protected function dsp($results) {
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($results, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }

}
