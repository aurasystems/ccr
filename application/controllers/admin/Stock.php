<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Stock extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Stock_model');
        set_active("stock");
    }

    function index() {
        check_p("stock", "v");
        $data['title'] = lang('stock');
        $data['branches'] = $this->Stock_model->get_data();
        $this->load->view("admin/pages/stock/index", $data);
    }

    function view_products() {
        $last = $this->uri->total_segments();
        $location_id = $this->uri->segment($last);
        check_param($location_id);
        $branch_data = get_branch_by_id($location_id);
        check_param($branch_data, "resource");
        $branch_name = get_branch($location_id, 'locations');
        $data['title'] = lang('branch_products');
        $data['branch_name'] = $branch_name;
        $data['location_id'] = $location_id;
        $data['branch_prod'] = get_products($location_id);
        $data['location_products'] = $this->Stock_model->get_location_products($location_id);
        $this->load->view("admin/pages/stock/view_products", $data);
    }

    function view_items() {
        $last = $this->uri->total_segments();
        $product_id = $this->uri->segment($last);
        $location_id = $this->uri->segment($last - 1);
        check_param($location_id);
        $branch_data = get_branch_by_id($location_id);
        check_param($branch_data, "resource");
        $product_name = get_branch($product_id, 'products');
        $data['title'] = lang('items');
        $data['product_name'] = $product_name;
        $data['product_id'] = $product_id;
        $data['location_id'] = $location_id;
        $data['items'] = $this->Stock_model->get_items('product_items', $location_id, $product_id);
        $this->load->view("admin/pages/stock/view_items", $data);
    }

    function transfer_item() {
        $last = $this->uri->total_segments();
        $item_id = $this->uri->segment($last);
        //$product_id = $this->uri->segment($last - 1);
        $item = get_product_item_by_id($item_id);
        if (!empty($item)) {
            $data['title'] = lang('transfer_loc');
            $data['item_name'] = get_branch($item->product_id, 'products');
            $data['locations'] = $this->Stock_model->get_locations('locations', $item->product_id, $item->location_id);
            $this->form_validation->set_rules("location_id", lang("location"), "trim|required");
            if ($this->form_validation->run()) {
                if (empty($this->input->post('location_id'))) {
                    $this->session->set_flashdata('error', lang('select_location'));
                    redirect(base_url("admin/Stock/transfer_item/$item_id"));
                }
                $log_transfer = [
                    'prod_id' => $item->product_id,
                    'item_id' => $item_id,
                    'from_branch' => $item->location_id,
                    'to_branch' => $this->input->post('location_id'),
                    'tr_by' => uid(),
                ];
                $this->Global_model->global_insert('items_transfer', $log_transfer);
                $data = [
                    'location_id' => $this->input->post('location_id'),
                ];
                $this->Global_model->global_update('product_items', $item_id, $data);
                $this->session->set_flashdata('success', lang('item_transfered'));
                redirect(base_url("admin/Stock/view_items/$item->location_id/$item->product_id"));
            } else {
                $this->load->view("admin/pages/stock/transfer_to_branch", $data);
            }
        } else {
            redirect('admin/Dashboard');
        }
    }

}
