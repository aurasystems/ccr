<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Locations extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Locations_model');
        set_active("locations");
    }

    function index() {
        check_p("locations", "v");
        $data = ['title' => lang('locations')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['locations'] = get_data_list(['c' => 6]);
        $this->load->view("admin/pages/locations/index", $data);
    }

    function add() {
        check_p("locations", "c");
        set_active("add_location");
        $this->form_validation->set_rules("n_en", lang('name_in') . lang('lang_en'), "trim|required")
                ->set_rules("n_ar", lang('name_in') . lang('lang_ar'), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('add_location');
            $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
            $this->load->view("admin/pages/locations/add", $data);
        } else {
            $data = [
                'n_en' => $this->input->post('n_en'),
                'n_ar' => $this->input->post('n_ar'),
                'lat' => $this->input->post('lat'),
                'lon' => $this->input->post('lon'),
                'added_by' => uid(),
                "deleted" => 0,
            ];
            $user_id = $this->Global_model->global_insert("locations", $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/Locations" . (!get_p("locations", "v") ? '/add' : '')));
        }
    }

    public function edit($id = NULL) {
        check_p("locations", "u");
        $one = get_single_date(['c' => 6, 'i' => $id, 'chk' => TRUE]);
        $this->form_validation->set_rules("n_en", lang('name_in') . lang('lang_en'), "trim|required")
                ->set_rules("n_ar", lang('name_in') . lang('lang_ar'), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('edit_location');
            $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
            $data['one'] = $one;
            $data['id'] = $id;
            $this->load->view('admin/pages/locations/edit', $data);
        } else {
            $data = [
                'n_en' => $this->input->post('n_en'),
                'n_ar' => $this->input->post('n_ar'),
                'lat' => $this->input->post('lat'),
                'lon' => $this->input->post('lon')
            ];
            $this->Global_model->global_update("locations", $id, $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/Locations"));
        }
    }

    function delete($id = NULL) {
        check_p("locations", "d");
        $user_info = get_single_date(['c' => 6, 'i' => $id, 'chk' => TRUE]);
        $this->Global_model->global_update("locations", $id, ["deleted" => 1]);
        $this->Locations_model->delete_timetable($id);
        $this->session->set_flashdata("success", lang("data_deleted_successfully"));
        redirect(base_url("admin/Locations"));
    }

    function timetable($location_id = NULL) {
        check_param($location_id);
        $location = $this->Locations_model->get_info($location_id);
        check_param($location, "resource");
        $data['title'] = lang('timetable');
        $data['location_id'] = $location_id;
        $hours = $this->Locations_model->get_location_timetable($location_id);
        $hours_array = [];
        foreach ($hours as $one) {
            $hours_array[$one->day] = new stdClass();
            $hours_array[$one->day]->time_from = $one->time_from;
            $hours_array[$one->day]->time_to = $one->time_to;
            $hours_array[$one->day]->day_off = $one->day_off;
        }
        $data['hours'] = $hours_array;
        $data['location'] = $location;
        $this->load->view('admin/pages/locations/timetable', $data);
    }

    function insert_timetable($location_id) {
        check_param($location_id);
        $location = $this->Locations_model->get_info($location_id);
        check_param($location, "resource");
        for ($i = 1; $i <= 7; $i++) {
            $data = [
                'location_id' => $location_id,
                'day' => $i, // 1 (for Monday) through 7 (for Sunday)
                'time_from' => $this->input->post("time_from$i") ? date('H:i a', strtotime($this->input->post("time_from$i"))) : NULL,
                'time_to' => $this->input->post("time_to$i") ? date('H:i a', strtotime($this->input->post("time_to$i"))) : NULL,
                'day_off' => 0
            ];
            if ($this->input->post("day_off$i")) {
                $data['day_off'] = $this->input->post("day_off$i");
            }
            $this->Locations_model->insert_timetable($data);
        }
        $this->session->set_flashdata("success", lang("data_submitted_successfully"));
        redirect(base_url("admin/Locations/timetable/$location_id"));
    }

    function edit_timetable($location_id) {
        check_param($location_id);
        $location = $this->Locations_model->get_info($location_id);
        check_param($location, "resource");
        for ($i = 1; $i <= 7; $i++) {
            $data = [
                'time_from' => $this->input->post("time_from$i") ? date('H:i a', strtotime($this->input->post("time_from$i"))) : NULL,
                'time_to' => $this->input->post("time_to$i") ? date('H:i a', strtotime($this->input->post("time_to$i"))) : NULL,
                'day_off' => 0
            ];
            if ($this->input->post("day_off$i")) {
                $data['day_off'] = $this->input->post("day_off$i");
            }
            $this->Locations_model->update_timetable($location_id, $i, $data);
        }
        $this->session->set_flashdata('msg', lang('data_submitted_successfully'));
        redirect(base_url("admin/Locations/timetable/$location_id"));
    }

}
