<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Technicians_types extends CI_Controller {

    private $code = NULL;
    public $idf = NULL;
    private $sh = NULL;

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->code = 10;
        $this->idf = get_sys_idf($this->code);
        $this->sh = 'tech_type';
        $this->load->model('admin/' . ucfirst($this->idf) . '_model', $this->sh);
        set_active($this->idf);
    }

    function index() {
        check_p($this->idf, "v");
        $data['title'] = lang($this->idf);
        $data[$this->idf] = get_data_list(['c' => $this->code]);
        $this->load->view("admin/pages/" . $this->idf . "/index", $data);
    }

    function add() {
        check_p($this->idf, "c");
        $this->form_validation->set_rules("n_en", lang("name_en"), "trim|required");
        $this->form_validation->set_rules("n_ar", lang("name_ar"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('add_type');
            $data['parents'] = get_data_list(['c' => $this->code]);
            $this->load->view('admin/pages/' . $this->idf . '/add', $data);
        } else {
            $data = [
                'n_en' => $this->input->post('n_en'),
                'n_ar' => $this->input->post('n_ar')
            ];
            $this->Global_model->global_insert($this->idf, $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/' . ucfirst($this->idf)));
        }
    }

    public function edit($id = NULL) {
        check_p($this->idf, "u");
        $one = get_single_date(['c' => $this->code, 'i' => $id, 'chk' => TRUE]);
        $this->form_validation->set_rules("n_en", lang("name_en"), "trim|required");
        $this->form_validation->set_rules("n_ar", lang("name_ar"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('edit_type');
            $data['one'] = $one;
            $data['parents'] = get_data_list(['c' => $this->code]);
            $this->load->view('admin/pages/' . $this->idf . '/edit', $data);
        } else {
            $data = [
                'n_en' => $this->input->post('n_en'),
                'n_ar' => $this->input->post('n_ar')
            ];
            $this->Global_model->global_update($this->idf, $id, $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/' . ucfirst($this->idf)));
        }
    }

    public function delete($id = NULL) {
        check_p($this->idf, "d");
        $one = get_single_date(['c' => $this->code, 'i' => $id, 'chk' => TRUE]);
        if ($one) {
            $this->Global_model->global_update($this->idf, $id, ["deleted" => 1]);
            $this->session->set_flashdata("success", lang("data_deleted_successfully"));
            redirect(base_url('admin/' . ucfirst($this->idf)));
        } else {
            $error = lang("category_not_found");
            $this->session->set_flashdata("error", $error);
            redirect(base_url('admin/' . ucfirst($this->idf)));
        }
    }

}
