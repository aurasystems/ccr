<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Settings_model');
        set_active("settings");
    }

    function index() {
        check_p("settings", "v");
        set_active("settings");
        $data = ['title' => lang('settings')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['settings'] = $this->Settings_model->get_settings();
        $this->load->view("admin/pages/settings/settings", $data);
    }

    function update() {
        check_p("settings", "u");
        $this->load->library('form_validation');
        $this->form_validation->set_rules('no_show_penalty', lang("no_show_penalty"), 'trim|numeric');
        $this->form_validation->set_rules('free_cancellation_time_limit', lang("free_cancellation_time_limit"), 'trim|numeric');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $data = [
                'no_show_penalty' => $this->input->post('no_show_penalty') == '' ? NULL : $this->input->post('no_show_penalty'),
                'app_reg_text' => $this->input->post('app_reg_text') == '' ? NULL : $this->input->post('app_reg_text'),
                'unapp_reg_text' => $this->input->post('unapp_reg_text') == '' ? NULL : $this->input->post('unapp_reg_text'),
                'app_order_text' => $this->input->post('app_order_text') == '' ? NULL : $this->input->post('app_order_text'),
                'unapp_order_text' => $this->input->post('unapp_order_text') == '' ? NULL : $this->input->post('unapp_order_text'),
                'phone' => $this->input->post('phone') == '' ? NULL : $this->input->post('phone'),
                'email' => $this->input->post('email') == '' ? NULL : $this->input->post('email'),
                'address' => $this->input->post('address') == '' ? NULL : $this->input->post('address'),
                'longitude' => $this->input->post('longitude') == '' ? NULL : $this->input->post('longitude'),
                'latitude' => $this->input->post('latitude') == '' ? NULL : $this->input->post('latitude'),
                'fb_url' => $this->input->post('fb_url') == '' ? NULL : $this->input->post('fb_url'),
                'insta_url' => $this->input->post('insta_url') == '' ? NULL : $this->input->post('insta_url'),
                'free_cancellation_time_limit' => $this->input->post('free_cancellation_time_limit') == '' ? NULL : $this->input->post('free_cancellation_time_limit'),
                'time_for_missing' => $this->input->post('time_for_missing') == '' ? NULL : $this->input->post('time_for_missing'),
                'notification_type' => ($this->input->post('notification_type') == 1) ? ($this->input->post('notification_type')) : (($this->input->post('notification_type') == 2) ? ($this->input->post('notification_type')) : (($this->input->post('notification_type') == 3) ? ($this->input->post('notification_type')) : 0)),
                'booking_cash_limit' => ($this->input->post('booking_cash_limit')) ? $this->input->post('booking_cash_limit') : 0,
                'booking_credit_limit' => ($this->input->post('booking_credit_limit')) ? $this->input->post('booking_credit_limit') : 0,
                'show_blog' => ($this->input->post('show_blog')) ? $this->input->post('show_blog') : 0,
                'show_about' => ($this->input->post('show_about')) ? $this->input->post('show_about') : 0,
                'show_privacy' => ($this->input->post('show_privacy')) ? $this->input->post('show_privacy') : 0,
                'show_renting' => ($this->input->post('show_renting')) ? $this->input->post('show_renting') : 0,
                'send_survey_type' => ($this->input->post('send_survey_type')) ? $this->input->post('send_survey_type') : 0
            ];
            if ($_FILES['userfile']['name'] != '') {
                $config['upload_path'] = './uploads/settings';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '10000';
                $config['max_width'] = '5000';
                $config['max_height'] = '5000';
                $this->load->library("upload", $config);
                if ($this->upload->do_upload()) {
                    $upload_data = $this->upload->data();
                    $data['logo'] = $upload_data['file_name'];
                } else {
                    $this->session->set_flashdata("error", lang("img_could_not_be_uploaded"));
                    redirect(base_url('admin/Settings'));
                }
            }
            $this->Settings_model->update_settings($data);
            $this->session->unset_userdata('settings');
            $this->session->set_flashdata("success", lang("sett_updated_successfully"));
            redirect(base_url("admin/Settings"));
        }
    }

    function questions_types() {
        check_p("questions_types", "v");
        set_active("questions_types");
        $data = ['title' => lang('questions_types')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['options_groups'] = $this->Settings_model->get_options_groups();
        $this->load->view("admin/pages/options_groups/index", $data);
    }

    function add_question_type() {
        check_p("questions_types", "c");
        set_active("questions_types");
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        if (!$this->form_validation->run()) {
            $data['title'] = lang('add_question_type');
            $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
            $this->load->view("admin/pages/options_groups/add", $data);
        } else {
            $options_groups = $this->input->post('options_groups');
            $options_groups_count = count($options_groups);
            if ($options_groups_count <= 1) {
                $this->session->set_flashdata("error", lang("two_items_op_group"));
                redirect('admin/Settings/add_question_type');
            }
            if ($options_groups_count > 1) {
                for ($i = 0; $i < $options_groups_count; $i++) {
                    if ($options_groups[$i] == '' || $options_groups[$i] == NULL) {
                        $this->session->set_flashdata("error", lang("add_item_op_group"));
                        redirect('admin/Settings/add_question_type');
                    }
                }
            }
            $data = [
                'name' => $this->input->post('title')
            ];
            $inserted_id = $this->Global_model->global_insert('options_groups', $data);
            for ($i = 0; $i < $options_groups_count; $i++) {
                if ($options_groups[$i] != '' || $options_groups[$i] != NULL) {
                    $op_group_items = [
                        'op_group_id' => $inserted_id,
                        'name' => $options_groups[$i],
                        'value' => $i + 1
                    ];
                    $this->Global_model->global_insert('options_groups_items', $op_group_items);
                }
            }
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect('admin/Settings/questions_types');
        }
    }

    function edit_question_type($op_id = NULL) {
        check_p("questions_types", "u");
        check_param($op_id);
        set_active("questions_types");
        $one = $this->Settings_model->get_op($op_id);
        check_param($one, "resource");
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        if (!$this->form_validation->run()) {
            $data['title'] = lang('edit_question_type');
            $data['op_id'] = $op_id;
            $data['op_group_data'] = $one;
            $data['op_gp_items'] = $this->Settings_model->get_option_group_items($op_id);
            $this->load->view("admin/pages/options_groups/edit", $data);
        } else {
            $options_groups = $this->input->post('options_groups');
            $options_groups_count = count($options_groups);
            if ($options_groups_count <= 1) {
                $this->session->set_flashdata("error", lang("two_items_op_group"));
                redirect('admin/Settings/edit_question_type/' . $op_id);
            }
            if ($options_groups_count > 1) {
                for ($i = 0; $i < $options_groups_count; $i++) {
                    if ($options_groups[$i] == '' || $options_groups[$i] == NULL) {
                        $this->session->set_flashdata("error", lang("add_item_op_group"));
                        redirect('admin/Settings/edit_question_type/' . $op_id);
                    }
                }
            }
            $data = [
                'name' => $this->input->post('title')
            ];
            $this->Global_model->global_update('options_groups', $op_id, $data);
            $this->Settings_model->empty_op_gp_items($op_id);
            for ($i = 0; $i < $options_groups_count; $i++) {
                if ($options_groups[$i] != '' || $options_groups[$i] != NULL) {
                    $op_group_items = array(
                        'op_group_id' => $op_id,
                        'name' => $options_groups[$i],
                        'value' => $i + 1
                    );
                    $this->Global_model->global_insert('options_groups_items', $op_group_items);
                }
            }
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect('admin/Settings/questions_types');
        }
    }

    function delete_question_type($id = NULL) {
        check_p("questions_types", "d");
        check_param($id);
        $this->Global_model->global_update('options_groups', $id, ["deleted" => 1]);
        $this->session->set_flashdata("success", lang("qt_del_success"));
        redirect(base_url('admin/Settings/questions_types'));
    }

}
