<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vouchers extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
        $this->load->model('admin/Voucher_model');
    }

    function index() {
        check_p('vouchers', "v");
        set_active("vouchers");
        $data['title'] = lang('vouchers');
        $data['vouchers'] = $this->Global_model->get_data_not_deleted('vouchers');
        $this->load->view("admin/pages/vouchers/index", $data);
    }

    function add() {
        check_p('vouchers', "c");
        set_active("add_voucher");
        $this->form_validation->set_rules("voucher_number", lang("voucher_name"), "trim|required|is_unique[vouchers.voucher_number]")
                ->set_rules("amount", lang("amount"), "trim|required")
                ->set_rules("client_id", lang("client"), "trim|required")
                ->set_rules("v_from", lang("v_from"), "trim|required")
                ->set_rules("v_to", lang("v_to"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('add_voucher');
            $this->load->view("admin/pages/vouchers/add", $data);
        } else {
            $data = [];
            $data['voucher_number'] = $this->input->post('voucher_number');
            $data['amount'] = $this->input->post('amount');
            $data['client_id'] = $this->input->post('client_id');
            $data['type'] = $this->input->post('type');
            $data['v_from'] = $this->input->post('v_from');
            $data['v_to'] = $this->input->post('v_to');
            $data['uses_total'] = $this->input->post('uses_total');
            $data['uses_remain'] = $this->input->post('uses_total');
            $this->Global_model->global_insert('vouchers', $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/Vouchers/add'));
        }
    }

    function edit($voucher_id) {
        check_p('vouchers', "u");
        check_param($voucher_id);
        $this->form_validation->set_rules("voucher_number", lang("voucher_number"), "trim|required")
                ->set_rules("amount", lang("amount"), "trim|required")
                ->set_rules("client_id", lang("client"), "trim|required")
                ->set_rules("v_from", lang("v_from"), "trim|required")
                ->set_rules("v_to", lang("v_to"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('add_voucher');
            $data['info'] = get_record('id', $voucher_id, 'vouchers');
            $this->load->view("admin/pages/vouchers/edit", $data);
        } else {
            $data = [];
            $data['voucher_number'] = $this->input->post('voucher_number');
            $data['amount'] = $this->input->post('amount');
            $data['client_id'] = $this->input->post('client_id');
            $data['type'] = $this->input->post('type');
            $data['v_from'] = $this->input->post('v_from');
            $data['v_to'] = $this->input->post('v_to');
            $data['uses_total'] = $this->input->post('uses_total');
            $this->Global_model->global_update('vouchers', $voucher_id, $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/Vouchers'));
        }
    }

    function delete($voucher_id) {
        check_p('vouchers', "d");
        check_param($voucher_id);
        $this->Voucher_model->delete($voucher_id, 'products');
        $this->session->set_flashdata("success", lang("voucher_del_successfully"));
        redirect(base_url("admin/Vouchers"));
    }

}
