<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Maintenance extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
        $this->load->model('admin/Maintenance_model');
    }

    function index() {
        check_p("maintenance", "v");
        set_active("maintenance");
        $data['title'] = lang('maintenance');
        $this->load->view("admin/pages/maintenance/index", $data);
    }

    function add() {
        check_p('maintenance', "c");
        set_active("add_maintenance");
        $today = date('Y-m-d');
        $item_id = '';
        $product_id = '';
        $sup_id = '';
        $data['title'] = lang('add') .' '. lang('maintenance');
        $this->form_validation->set_rules("maintanance_type", lang("maintanance_type"), "trim|required");
       
        if (!$this->form_validation->run()) {
            $this->load->view("admin/pages/maintenance/add", $data);
        } else {
            $maint_type = $this->input->post('maintanance_type');
            if ($maint_type == 1)
                $this->form_validation->set_rules("tec_stud", lang("tec_stud"), "trim|required");
            else if ($maint_type == 2)
                $this->form_validation->set_rules("item", lang("item"), "trim|required");
            else if ($maint_type == 3)
                $this->form_validation->set_rules("sup_item", lang("sup_item"), "trim|required");
            $this->form_validation->set_rules("m_from", lang("m_from"), "trim|required");
            $this->form_validation->set_rules("m_to", lang("m_to"), "trim|required");
            $sup_id = $this->input->post('sup_item');
            $product_id = $this->input->post('tec_stud');
            $item_id = $this->input->post('item');
            if ($this->form_validation->run()) {
                $m_from = $this->input->post('m_from');
                $m_to = $this->input->post('m_to');
                if ($m_from <= $today) {
                    $update_product['damaged'] = 1;
                    if ($maint_type == 2) {
                        $update_product['exist'] = 0;
                        $this->Global_model->global_update('product_items', $item_id, $update_product);
                    } else if ($maint_type == 1){
                        $this->Global_model->global_update('products', $product_id, $update_product);
                    }
                    else if ($maint_type == 3){
                        $update_product['exist'] = 0;
                        $this->Global_model->global_update('product_sup_items', $sup_id, $update_product);                    }

                    $data['is_applied'] = 1;
                }
                if ($maint_type == 1) {
                    $data = array(
                        'product_id' => $product_id,
                        'm_from' => $m_from,
                        'm_to' => $m_to,
                    );
                    $this->Maintenance_model->check_booked_studioTech($product_id, $m_from, $m_to);
                    $maint_id =  $this->Maintenance_model->insert_maint('maintenance', $data);
                    $this->Maintenance_model->check_client_penalty($product_id, $maint_id, false, true);
                } else if ($maint_type == 2) {
                    $data = array(
                        'item_id' => $item_id,
                        'm_from' => $m_from,
                        'm_to' => $m_to,
                    );
                    
                    $this->Maintenance_model->check_booked_items($item_id, $m_from, $m_to);
                    $maint_id = $this->Maintenance_model->insert_maint('maintenance', $data);
                    $this->Maintenance_model->check_client_penalty($item_id, $maint_id);
                }else if ($maint_type == 3) {
                    $data = array(
                        'sup_id' => $sup_id,
                        'm_from' => $m_from,
                        'm_to' => $m_to,
                    );
                  
                    $maint_id = $this->Maintenance_model->insert_maint('maintenance', $data);
                    $this->Maintenance_model->check_client_penalty($sup_id, $maint_id, true);
                }
                $this->session->set_flashdata("success", lang("maintenance") . lang("added_success"));
                redirect(base_url('admin/Maintenance/add'));
            } else
                $this->load->view("admin/pages/maintenance/add", $data);
        }
    }

    function scan() {
        $scanned_id = $this->input->post('scan_value');
        // echo $scanned_id;
        $scanned_data = $this->Maintenance_model->get_scan_data($scanned_id);
        echo json_encode($scanned_data);
    }

    function update_applied() {
        $today = date('Y-m-d');
        $id = $this->input->post('id');
        $product_id = $this->input->post('product_id');
        $item_id = $this->input->post('item_id');
        $sup_id = $this->input->post('sup_id');
        $cost = $this->input->post('cost');
        $client_cost = $this->input->post('client_cost');
        $m_to = $this->Maintenance_model->get_data($id);

        if ($m_to > $today)
            $m_to = $today;
        if ($product_id != null)
            $this->Global_model->global_update('products', $product_id, array('under_maintenance' => 0));
        else if ($item_id != null)
            $this->Global_model->global_update('product_items', $item_id, array('damaged' => 0, 'exist' => 1));
        else if ($sup_id != null)
            $this->Global_model->global_update('product_sup_items', $sup_id, array('damaged' => 0, 'exist' => 1));
        $this->Global_model->global_update('maintenance', $id, array('is_applied' => 1, 'm_to' => $m_to));
        if(!empty($client_cost))
        {
        $this->Maintenance_model->update_cost('damaged_items', $id, $client_cost);
        }
        $expense_cost = (int)$cost - (int)$client_cost;
        $this->Maintenance_model->update_transaction($expense_cost, $id);
    }

    function update_not_applied() {
        $today = date('Y-m-d');
        $id = $this->input->post('id');
        $product_id = $this->input->post('product_id');
        $item_id = $this->input->post('item_id');
        $sup_id = $this->input->post('sup_id');

        if ($product_id != null)
            $this->Global_model->global_update('products', $product_id, array('under_maintenance' => 1));
        else if ($item_id != null)
            $this->Global_model->global_update('product_items', $item_id, array('damaged' => 1, 'exist' => 0));
        else if ($sup_id != null)
            $this->Global_model->global_update('product_sup_items', $sup_id, array('damaged' => 1, 'exist' => 0));
        $this->Global_model->global_update('maintenance', $id, array('is_applied' => 0));
    }

    function update_dates() {
        $id = $this->input->post('id');
        $m_from = $this->input->post('m_from');
        $m_to = $this->input->post('m_to');
        $product_id = $this->input->post('product_id');
        $item_id = $this->input->post('item_id');

        $data = array(
            'm_from' => $m_from,
            'm_to' => $m_to,
            'is_applied' => 0,
        );
        if ($product_id != null)
            $this->Global_model->global_update('products', $product_id, array('under_maintenance' => 1));
        else if ($item_id != null)
            $this->Global_model->global_update('product_items', $item_id, array('damaged' => 1, 'exist' => 0));
        $this->Global_model->global_update('maintenance', $id, $data);
        if ($product_id != null)
            $this->Maintenance_model->check_booked_studioTech($product_id, $m_from, $m_to);
        else if ($item_id != null)
            $this->Maintenance_model->check_booked_items($item_id, $m_from, $m_to);
    }

    function get_damaged()
    {
        $maint_id = $this->input->post('id');
        $related_to_client = $this->Maintenance_model->is_maint_related_client($maint_id);
        if($related_to_client)
        echo true;
        else 
        echo false;
    }

}
