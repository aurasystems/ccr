<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Privacy extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Privacy_model');
        set_active("privacy_page");
    }

    function index() {
        check_p("privacy_page", "v");
        set_active("privacy_page");
        $data = ['title' => lang('privacy_page')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['privacy'] = $this->Privacy_model->get_content();
        $this->load->view("admin/pages/other/privacy", $data);
    }

    function update() {
        check_p("privacy_page", "u");
        $this->load->library('form_validation');
        $this->form_validation->set_rules('content', lang("content"), 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $about = $this->Privacy_model->get_content();
            if (!empty($about)) {
                // Update
                $data = [
                    'content' => (!empty($this->input->post('content'))) ? $this->input->post('content') : NULL
                ];
                $this->Privacy_model->update_content($data);
            } else {
                // Insert
                $data = [
                    'content' => (!empty($this->input->post('content'))) ? $this->input->post('content') : NULL,
                    'type' => 2
                ];
                $inserted = $this->Global_model->global_insert('pages', $data);
            }

            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/Privacy"));
        }
    }

}
