<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Email_settings extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Email_settings_model');
        set_active("email_settings");
    }

    function index() {
        check_p("email_settings", "v");
        $data = ['title' => lang('email_sett')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['email_settings'] = $this->Email_settings_model->get_email_settings();
        $this->load->view("admin/pages/settings/email_settings", $data);
    }

    function update() {
        check_p("email_settings", "u");
        $this->load->library('form_validation');
        $this->form_validation->set_rules('smtp_host', lang("lang_smtp_host"), 'trim');
        $this->form_validation->set_rules('host_mail', lang("lang_host_mail"), 'trim');
        $this->form_validation->set_rules('smtp_port', lang("lang_smtp_port"), 'trim');
        $this->form_validation->set_rules('smtp_user', lang("lang_smtp_user"), 'trim');
        $this->form_validation->set_rules('smtp_pass', lang("lang_smtp_pass"), 'trim');
        $this->form_validation->set_rules('prefix_text', lang("prefix_text"), 'trim');
        $this->form_validation->set_rules('postfix_text', lang("postfix_text"), 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $data = [
                'smtp_host' => $this->input->post('smtp_host') == '' ? NULL : $this->input->post('smtp_host'),
                'host_mail' => $this->input->post('host_mail') == '' ? NULL : $this->input->post('host_mail'),
                'smtp_port' => $this->input->post('smtp_port') == '' ? NULL : $this->input->post('smtp_port'),
                'smtp_user' => $this->input->post('smtp_user') == '' ? NULL : $this->input->post('smtp_user'),
                'smtp_pass' => $this->input->post('smtp_pass') == '' ? NULL : $this->input->post('smtp_pass'),
                'prefix_text' => $this->input->post('prefix_text') == '' ? NULL : $this->input->post('prefix_text'),
                'postfix_text' => $this->input->post('postfix_text') == '' ? NULL : $this->input->post('postfix_text'),
            ];
            $this->Email_settings_model->update_settings($data);
            $this->session->unset_userdata('email_settings');
            $this->session->set_flashdata("success", lang("email_sett_updated_successfully"));
            redirect(base_url("admin/Email_settings"));
        }
    }

}
