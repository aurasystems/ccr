<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Reports_model', 'rpt');
        $this->load->model('admin/Booking_model');
        set_active("reports");
    }

    function index() {
        
    }

    function equip_rep() {
        check_p("equip_rep", "v");
        set_active("equip_rep");
        $data['title'] = lang('equip_rep');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');
        $this->load->view("admin/pages/reports/equipment/index", $data);
    }

    function acc_rep() {
        check_p("acc_rep", "v");
        set_active("acc_rep");
        $data['title'] = lang('acc_rep');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');
        $this->load->view("admin/pages/reports/accounting/index", $data);
    }

    function ecomm_rep() {
        check_p("ecomm_rep", "v");
        set_active("ecomm_rep");
        $data['title'] = lang('ecomm_rep');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');
        $this->load->view("admin/pages/reports/ecommerce/index", $data);
    }

    function failed_msgs_rpt() {
        check_p("failed_msgs_rpt", "v");
        set_active("failed_msgs_rpt");
        $data['title'] = lang('failed_msgs_rpt');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');
        $this->load->view("admin/pages/reports/failed_messages", $data);
    }

    function gen_failed_msgs_rpt() {
        $results = [];
        if (get_p('failed_msgs_rpt', 'v')) {
            $data = [
                'date_from' => $this->input->post('date_from'),
                'date_to' => $this->input->post('date_to')
            ];
            if (!empty($data['date_from']) && !empty($data['date_to'])) {
                $from = $data['date_from'];
                $to = $data['date_to'];
            } else {
                $from = date('Y-m-d');
                $to = date('Y-m-d');
            }
            $this->load->model('admin/Datatable_model', 'datatable');
            $this->datatable->set_table('failed_messages fm');
            $column_order = ['fm.id', 'fm.client_id', 'fm.phone', 'fm.msg', 'fm.timestamp']; //set column field database for datatable orderable
            $column_search = ['clients.n_en', 'clients.n_ar', 'fm.phone', 'fm.msg']; //set column field database for datatable searchable 
            $this->datatable->set_column_order($column_order);
            $this->datatable->set_column_search($column_search);
            $tp = $this->rpt->get_failed_msgs($from, $to);
            $this->datatable->set_select($tp['select']);
            $this->datatable->set_join($tp['join']);
            $this->datatable->set_filter($tp['filter']);
            $this->datatable->set_order(['fm.id' => 'asc']);
            $list = $this->datatable->get_datatables();
            $data = array();
            $no = (int) $this->input->post('start');
            foreach ($list as $dt) {
                $no++;
                $row = [];
                $row[] = $no;
                $row[] = $dt->client;
                $row[] = $dt->phone;
                $row[] = $dt->msg;
                $row[] = date('Y-m-d g:i A', strtotime($dt->timestamp));
                $data[] = $row;
            }
        }


        $output = array(
            "draw" => (int) $this->input->post('draw'),
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function equipment_transfer_rpt() {
        check_p("equip_rep", "v");
        set_active("equip_rep");
        $data['title'] = lang('equip_transfer');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');
        $this->load->view("admin/pages/reports/equipment/equip_transfer", $data);
    }

    function gen_equipment_transfer_rpt() {
        $results = [];
        if (get_p('equip_rep', 'v')) {
            $data = [
                'date_from' => $this->input->post('date_from'),
                'date_to' => $this->input->post('date_to')
            ];
            if (!empty($data['date_from']) && !empty($data['date_to'])) {
                $from = $data['date_from'];
                $to = $data['date_to'];
            } else {
                $from = date('Y-m-d');
                $to = date('Y-m-d');
            }
            $this->load->model('admin/Datatable_model', 'datatable');
            $this->datatable->set_table('items_transfer trns');
            $column_order = ['trns.id', 'products.n_' . lc(), 'product_items.serial_number', 'l1.n_' . lc(), 'l2.n_' . lc(), 'users.n_' . lc(), 'trns.timestamp']; //set column field database for datatable orderable
            $column_search = ['products.n_' . lc(), 'product_items.serial_number']; //set column field database for datatable searchable 
            $this->datatable->set_column_order($column_order);
            $this->datatable->set_column_search($column_search);
            $tp = $this->rpt->get_transfered_equips($from, $to);
            $this->datatable->set_select($tp['select']);
            $this->datatable->set_join($tp['join']);
            $this->datatable->set_filter($tp['filter']);
            $this->datatable->set_order(['trns.id' => 'asc']);
            $list = $this->datatable->get_datatables();
            $data = array();
            $no = (int) $this->input->post('start');
            foreach ($list as $dt) {
                $no++;
                $row = [];
                $row[] = $no;
                $row[] = $dt->product;
                $row[] = $dt->serial_number;
                $row[] = $dt->b1name;
                $row[] = $dt->b2name;
                $row[] = $dt->uname;
                $row[] = date('Y-m-d g:i A', strtotime($dt->timestamp));
                $data[] = $row;
            }
        }


        $output = array(
            "draw" => (int) $this->input->post('draw'),
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function unique_customers_rpt() {
        check_p("ecomm_rep", "v");
        set_active("ecomm_rep");
        $data['title'] = lang('unique_customers');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');
        $this->load->view("admin/pages/reports/ecommerce/unique_customers", $data);
    }

    function gen_unique_customers_rpt() {
        $results = [];
        if (get_p('ecomm_rep', 'v')) {
            $data = [
                'date_from' => $this->input->post('date_from'),
                'date_to' => $this->input->post('date_to')
            ];
            if (!empty($data['date_from']) && !empty($data['date_to'])) {
                $from = $data['date_from'];
                $to = $data['date_to'];
            } else {
                $from = date('Y-m-d');
                $to = date('Y-m-d');
            }
            $this->load->model('admin/Datatable_model', 'datatable');
            $this->datatable->set_table('booking');
            $column_order = ['clients.n_' . lc()]; //set column field database for datatable orderable
            $column_search = ['clients.n_' . lc()]; //set column field database for datatable searchable 
            $this->datatable->set_column_order($column_order);
            $this->datatable->set_column_search($column_search);
            $tp = $this->rpt->get_unique_customers($from, $to);
            $this->datatable->set_select($tp['select']);
            $this->datatable->set_join($tp['join']);
            $this->datatable->set_filter($tp['filter']);
            $this->datatable->set_order(['clients.n_' . lc() => 'asc']);
            $list = $this->datatable->get_datatables();
            $count = 0;
            $data = array();
            $no = (int) $this->input->post('start');
            foreach ($list as $dt) {
                $count++;
                $no++;
                $row = [];
                $row[] = '<a target="_blank" href="' . base_url() . 'admin/Clients/dashboard/' . $dt->client_id . '">' . $dt->cname . '</a>';
                $data[] = $row;
            }
        }


        $output = array(
            "draw" => (int) $this->input->post('draw'),
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
            "count" => $count,
        );
        //output to json format
        echo json_encode($output);
    }

    function sold_equip() {
        check_p("equip_rep", "v");
        set_active("equip_rep");
        $data['title'] = lang('sold_equip');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');
        $this->load->view("admin/pages/reports/equipment/sold_equip", $data);
    }

    function gen_sold_equip_rpt() {
        $results = [];
        if (get_p('equip_rep', 'v')) {
            $this->load->model('admin/Datatable_model', 'datatable');
            $this->datatable->set_table('product_items');
            $column_order = [NULL, 'products.n_' . lc(), 'product_items.serial_number', 'locations.n_' . lc()]; //set column field database for datatable orderable
            $column_search = ['products.n_' . lc(), 'product_items.serial_number', 'locations.n_' . lc()]; //set column field database for datatable searchable 
            $this->datatable->set_column_order($column_order);
            $this->datatable->set_column_search($column_search);
            $tp = $this->rpt->get_sold_equip();
            $this->datatable->set_select($tp['select']);
            $this->datatable->set_join($tp['join']);
            $this->datatable->set_filter($tp['filter']);
            $this->datatable->set_order(['product_items.id' => 'asc']);
            $list = $this->datatable->get_datatables();
            $data = array();
            $no = (int) $this->input->post('start');
            foreach ($list as $dt) {
                $no++;
                $row = [];
                $row[] = $no;
                $row[] = $dt->product;
                $row[] = $dt->serial_number;
                $row[] = $dt->bname;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => (int) $this->input->post('draw'),
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function damaged_equip() {
        check_p("equip_rep", "v");
        set_active("equip_rep");
        $data['title'] = lang('damaged_equips');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');
        $this->load->view("admin/pages/reports/equipment/damaged_equip", $data);
    }

    function gen_damaged_equip_rpt() {
        $results = [];
        if (get_p('equip_rep', 'v')) {
            $this->load->model('admin/Datatable_model', 'datatable');
            $this->datatable->set_table('damaged_items');
            $column_order = [NULL, 'clients.n_' . lc(), 'product_items.serial_number', 'damaged_items.cost']; //set column field database for datatable orderable
            $column_search = ['clients.n_' . lc(), 'product_items.serial_number']; //set column field database for datatable searchable 
            $this->datatable->set_column_order($column_order);
            $this->datatable->set_column_search($column_search);
            $tp = $this->rpt->get_damaged_equip();
            $this->datatable->set_select($tp['select']);
            $this->datatable->set_join($tp['join']);
            $this->datatable->set_filter($tp['filter']);
            $this->datatable->set_order(['damaged_items.id' => 'asc']);
            $list = $this->datatable->get_datatables();
            $data = array();
            $no = (int) $this->input->post('start');
            foreach ($list as $dt) {
                $no++;
                $row = [];
                $row[] = $no;
                $row[] = $dt->client;
                $row[] = $dt->item;
                $row[] = $dt->cost;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => (int) $this->input->post('draw'),
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function theft_equip() {
        check_p("equip_rep", "v");
        set_active("equip_rep");
        $data['title'] = lang('theft_equips');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');
        $this->load->view("admin/pages/reports/equipment/theft_equip", $data);
    }

    function gen_theft_equip_rpt() {
        $data = array(
            'date_from' => $this->input->post('date_from'),
            'date_to' => $this->input->post('date_to')
        );
        $items = $this->rpt->get_missing_items($data);
        foreach ($items as $item) {
            $one = $this->Booking_model->get_booking_item($item->book_id, $item->product_id);
            if ($one) {
                $item->b_from = $one->b_from;
                $item->b_to = $one->b_to;
                if (!empty($settings) && !empty($settings->time_for_missing)) {
                    $result_time = date('Y-m-d H:i:s', strtotime('+' . $settings->time_for_missing . ' hour', strtotime($one->b_to)));
                } else {
                    $result_time = $item->timestamp;
                }
                $item->return_time = $result_time;
            }
        }
        if (!empty($items)) {
            echo json_encode($items);
        } else {
            echo json_encode(false);
        }
    }

    function num_of_trans_made_rpt() {
        check_p("ecomm_rep", "v");
        set_active("ecomm_rep");
        $data['title'] = lang('num_of_trans_made');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');
        $this->load->view("admin/pages/reports/ecommerce/num_of_trans_made", $data);
    }

    function gen_num_of_trans_made_rpt() {
        $results = [];
        if (get_p('ecomm_rep', 'v')) {
            $data = [
                'date_from' => $this->input->post('date_from'),
                'date_to' => $this->input->post('date_to')
            ];
            if (!empty($data['date_from']) && !empty($data['date_to'])) {
                $from = $data['date_from'];
                $to = $data['date_to'];
            } else {
                $from = date('Y-m-d');
                $to = date('Y-m-d');
            }
            $this->load->model('admin/Datatable_model', 'datatable');
            $this->datatable->set_table('booking');
            $column_order = []; //set column field database for datatable orderable
            $column_search = []; //set column field database for datatable searchable 
            $this->datatable->set_column_order($column_order);
            $this->datatable->set_column_search($column_search);
            $tp = $this->rpt->get_num_of_trans($from, $to);
            $this->datatable->set_select($tp['select']);
            $this->datatable->set_join($tp['join']);
            $this->datatable->set_filter($tp['filter']);
            //$this->datatable->set_order(['booking.id' => 'asc']);
            $list = $this->datatable->get_datatables();
            $data = array();
            $no = (int) $this->input->post('start');
            foreach ($list as $dt) {
                $no++;
                $row = [];
                $row[] = $dt->count;
                $data[] = $row;
            }
        }


        $output = array(
            "draw" => (int) $this->input->post('draw'),
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function most_selling_items_rpt() {
        check_p("ecomm_rep", "v");
        set_active("ecomm_rep");
        $data['title'] = lang('most_selling_items');
        $data['from'] = date('Y-m-d');
        $data['to'] = date('Y-m-d');
        $this->load->view("admin/pages/reports/ecommerce/most_selling_items", $data);
    }

    function gen_most_selling_items_rpt() {
        $results = [];
        if (get_p('ecomm_rep', 'v')) {
            $data = [
                'date_from' => $this->input->post('date_from'),
                'date_to' => $this->input->post('date_to')
            ];
            if (!empty($data['date_from']) && !empty($data['date_to'])) {
                $from = $data['date_from'];
                $to = $data['date_to'];
            } else {
                $from = date('Y-m-d');
                $to = date('Y-m-d');
            }
            $this->load->model('admin/Datatable_model', 'datatable');
            $this->datatable->set_table('booking_items_delivery');
            $column_order = []; //set column field database for datatable orderable
            $column_search = []; //set column field database for datatable searchable 
            $this->datatable->set_column_order($column_order);
            $this->datatable->set_column_search($column_search);
            $tp = $this->rpt->get_most_selling_items($from, $to);
//            $this->datatable->set_select($tp['select']);
//            $this->datatable->set_join($tp['join']);
//            $this->datatable->set_filter($tp['filter']);
            //$this->datatable->set_order(['booking.id' => 'asc']);
            $list = $this->datatable->get_datatables();
            //var_dump($tp);die;
            $data = array();
            $no = (int) $this->input->post('start');
            if ($tp) {
                //$no++;
                $row = [];
                $row[] = $tp->serial_number;
                $row[] = $tp->count;
                $row[] = $tp->revenue;
                $data[] = $row;
            }
        }


        $output = array(
            "draw" => (int) $this->input->post('draw'),
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

}
