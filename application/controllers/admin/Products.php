<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        date_default_timezone_set('Africa/Cairo');
        $this->load->model('admin/Products_model');
        set_active("products");
    }

    function index() {
        check_p("products", "v");
        $data['title'] = lang('products');
        $data['products'] = $this->Global_model->get_data_not_deleted('products');
        $this->load->view("admin/pages/products/index", $data);
    }

    function add() {
        check_p('products', "c");
        set_active("add_product");
        $data['title'] = lang('add_product');
        $data['check_types'] = $this->Global_model->get_data_not_deleted('check_types');
        $data['locations'] = $this->Global_model->get_data_not_deleted('locations');
        $data['brands'] = $this->Global_model->get_data_not_deleted('brands');
        $data['categories'] = $this->Products_model->get_categories();
        $this->load->view("admin/pages/products/add_product", $data);
        $this->form_validation->set_rules("n_en", lang("n_en"), "trim|required")
                ->set_rules("n_ar", lang("n_ar"), "trim|required")
                ->set_rules("code", lang("code"), "trim|required")
                ->set_rules("rent_price", lang("rent_price"), "trim|required")
                ->set_rules("rent_term", lang("rent_term"), "trim|required")
                ->set_rules("slider", lang("slider"), "trim|required")
                ->set_rules("featured", lang("featured"), "trim|required")
                ->set_rules("category", lang("category"), "trim|required")
                ->set_rules("brand", lang("brand"), "trim|required");
        if ($this->form_validation->run()) {
            $code = $this->input->post('code');
            $is_exist = $this->Global_model->isFieldExist('products', 'code', $code);
            if ($is_exist) {
                $this->session->set_flashdata("error", lang("code_num_must_be_unique"));
                redirect(base_url("admin/Products/add"));
            }
            $dt = $this->input->post();
            $data = [];
            foreach ($dt as $k => $v):
                $data[$k] = $v;
            endforeach;
            unset($data['userfile']);
            $data['deleted'] = 0;
            $category_id = $data['category'];
            $brand_id = $data['brand'];
            unset($data['category']);
            unset($data['brand']);
            if ($_FILES['userfile']['name'] != '') {
                $config['upload_path'] = './uploads/products';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $this->load->library("upload", $config);
                if ($this->upload->do_upload()) {
                    $upload_data = $this->upload->data();
                    $data['img'] = $upload_data['file_name'];
                    if ($data['slider'] == 1) {
                        $this->Products_model->resizeImage($upload_data['file_name'], 770, 450, true);
                    }
                    $this->Products_model->resizeImage($upload_data['file_name'], 280, 265);
                } else {
                    $this->session->set_flashdata("error", lang("img_could_not_be_uploaded"));
                    redirect(base_url('admin/Products/add'));
                }
            }
            $this->Products_model->insert_product($category_id, $brand_id, $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/Products'));
        }
    }

    function items() {
        check_p("products", "v");
        $last = $this->uri->total_segments();
        $productID = $this->uri->segment($last);
        $data['title'] = lang('items');
        $data['items'] = $this->Global_model->get_data_by_colID('product_items', 'product_id', $productID);
        $data['product_name'] = $this->Products_model->get_record_name('products', $productID);
        $this->load->view("admin/pages/products/items", $data);
    }

    function add_item() {
        check_p('products', "c");
        $now = date('Y-m-d');
        $last = $this->uri->total_segments();
        $productID = $this->uri->segment($last);
        $pord_name = $this->input->post('prod_name');
        // Check for category if is_camera type => for shutter count check
        $data['product_cat'] = $this->Products_model->get_product_cat($productID);
        $data['title'] = lang('add_item');
        $data['product_name'] = $this->Products_model->get_record_name('products', $productID);
        $data['locations'] = get_data_list(['c' => 6]);
        $this->load->view("admin/pages/products/add_item", $data);
        $this->form_validation->set_rules("serial_number", lang("serial_number"), "trim|required");
        if ($this->form_validation->run()) {
            $serial_number = $this->input->post('serial_number');
            $location = $this->input->post('location_id');
            $is_exist = $this->Global_model->isFieldExist('product_items', 'serial_number', $serial_number);
            if ($is_exist) {
                $this->session->set_flashdata("error", lang("ser_num_must_be_unique"));
                redirect(base_url("admin/Products/add_item/$productID"));
            }
            if (empty($location)) {
                $this->session->set_flashdata("error", lang("select_location"));
                redirect(base_url("admin/Products/add_item/$productID"));
            }
            if (!empty($this->input->post('warranty_end_date')) && $this->input->post('warranty_end_date') < $now) {
                $this->session->set_flashdata("error", lang("no_old_date"));
                redirect(base_url("admin/Products/add_item/$productID"));
            }
            if (!empty($this->input->post('cost_price')) && $this->input->post('cost_price') < 1) {
                $this->session->set_flashdata("error", lang("chk_price"));
                redirect(base_url("admin/Products/add_item/$productID"));
            }
            if (!empty($this->input->post('shutter_count')) && $this->input->post('shutter_count') < 0) {
                $this->session->set_flashdata("error", lang("chk_shutter_count"));
                redirect(base_url("admin/Products/add_item/$productID"));
            }
            $data = [
                'product_id' => $productID,
                'location_id' => $location,
                'serial_number' => $serial_number,
                'cost_price' => $this->input->post('cost_price'),
                'warranty_company' => $this->input->post('warranty_company'),
                'warranty_end_date' => !empty($this->input->post('warranty_end_date')) ? $this->input->post('warranty_end_date') : NULL,
                'shutter_count' => !empty($this->input->post('shutter_count')) ? $this->input->post('shutter_count') : NULL,
                "deleted" => 0,
            ];
            $this->Global_model->global_insert('product_items', $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/Products/items/$productID"));
        }
    }

    function edit_item() {
        check_p('products', "u");
        $now = date('Y-m-d');
        $last = $this->uri->total_segments();
        $productID = $this->uri->segment($last);
        $itemID = $this->uri->segment($last - 1);
        $empty = '';
        // Check for category if is_camera type => for shutter count check
        $data_s['product_cat'] = $this->Products_model->get_product_cat($productID);
        $data_s['title'] = lang('edit_item');
        $data_s['product_name'] = $this->Products_model->get_record_name('products', $productID);
        $data_s['item'] = $this->Products_model->get_record($itemID, 'product_items');
        $data_s['locations'] = get_data_list(['c' => 6]);
        if ($data_s['item'] == null)
            $empty = 1;
        $data_s['empty'] = $empty;
        $this->form_validation->set_rules("serial_number", lang("serial_number"), "trim|required");
        if (!empty($data_s['item'])) {
            if ($this->form_validation->run()) {
                $dt = $this->input->post();
                $data = [];
                foreach ($dt as $k => $v):
                    $data[$k] = $v;
                endforeach;
                $is_exist = $this->Global_model->isFieldExist('product_items', 'serial_number', $data['serial_number']);
                if ($is_exist && $data['serial_number'] != $data_s['item'][0]->serial_number) {
                    $this->session->set_flashdata("error", lang("ser_num_must_be_unique"));
                    redirect(base_url("admin/Products/edit_item/$itemID/$productID"));
                }
                if (empty($data['location_id'])) {
                    $this->session->set_flashdata("error", lang("select_location"));
                    redirect(base_url("admin/Products/edit_item/$itemID/$productID"));
                }
                if (!empty($data['cost_price']) && $data['cost_price'] < 1) {
                    $this->session->set_flashdata("error", lang("chk_price"));
                    redirect(base_url("admin/Products/edit_item/$itemID/$productID"));
                }
                if (!empty($data['warranty_end_date']) && $data['warranty_end_date'] < $now) {
                    $this->session->set_flashdata("error", lang("no_old_date"));
                    redirect(base_url("admin/Products/edit_item/$itemID/$productID"));
                }
                if (!empty($data['shutter_count']) && $data['shutter_count'] < 0) {
                    $this->session->set_flashdata("error", lang("chk_shutter_count"));
                    redirect(base_url("admin/Products/edit_item/$itemID/$productID"));
                }
                $data['warranty_end_date'] = !empty($this->input->post('warranty_end_date')) ? $this->input->post('warranty_end_date') : NULL;
                $data['shutter_count'] = !empty($this->input->post('shutter_count')) ? $this->input->post('shutter_count') : NULL;
                $this->Global_model->global_update('product_items', $itemID, $data);
                $this->session->set_flashdata('success', lang('data_submitted_successfully'));
                redirect(base_url("admin/Products/items/$productID"));
            } else {
                $this->load->view("admin/pages/products/edit_item", $data_s);
            }
        } else {
            redirect(base_url("admin/Products"));
        }
    }

    function delete_item() {
        check_p('products', "d");
        $last = $this->uri->total_segments();
        $productID = $this->uri->segment($last);
        $id = $this->uri->segment($last - 1);
        $this->Products_model->delete_record($id, 'product_items');
        $this->session->set_flashdata("success", lang("item_del_successfully"));
        redirect(base_url("admin/Products/items/$productID"));
    }

    function edit_product() {
        check_p('products', "u");
        set_active("edit_product");
        $last = $this->uri->total_segments();
        $productID = $this->uri->segment($last);
        $empty = '';
        $data_s['title'] = lang('edit_product');
        $data_s['product_name'] = $this->Products_model->get_record_name('products', $productID);
        $data_s['check_types'] = $this->Global_model->get_data_not_deleted('check_types');
        $data_s['locations'] = $this->Global_model->get_data_not_deleted('locations');
        $data_s['item'] = $this->Products_model->get_record($productID, 'products');
        $data_s['categories'] = $this->Products_model->get_categories();
        $data_s['brands'] = $this->Products_model->get_brands();
        $data_s['product_brand'] = $this->Products_model->get_product_brand($productID);
        $data_s['product_cat'] = $this->Products_model->get_product_cat($productID);
        if ($data_s['item'] == null)
            $empty = 1;
        $data_s['empty'] = $empty;
        $this->form_validation->set_rules("code", lang("code"), "trim|required");
        if (!$empty) {
            if ($this->form_validation->run()) {
                $dt = $this->input->post();
                $data = [];
                foreach ($dt as $k => $v):
                    $data[$k] = $v;
                endforeach;
                unset($data['userfile']);
                $category_id = $data['category'];
                $brand_id = $data['brand'];
                unset($data['category']);
                unset($data['brand']);
                $img = $this->input->post('userfile');
                $is_exist = $this->Global_model->isFieldExist('products', 'code', $data['code']);
                if ($is_exist && $data['code'] != $data_s['item'][0]->code) {
                    $this->session->set_flashdata("error", lang("code_num_must_be_unique"));
                    redirect(base_url("admin/Products/edit_product/$productID"));
                }
                $data['deleted'] = 0;
                if ($_FILES['userfile']['name'] != '') {
                    $config['upload_path'] = './uploads/products';
                    $config['allowed_types'] = 'jpg|png|jpeg';
                    $this->load->library("upload", $config);
                    if ($this->upload->do_upload()) {
                        $upload_data = $this->upload->data();
                        $data['img'] = $upload_data['file_name'];
                        unlink("./uploads/products/$img");
                        if ($data['slider'] == 1) {
                            $this->Products_model->resizeImage($upload_data['file_name'], 770, 450, true);
                        }
                        $this->Products_model->resizeImage($upload_data['file_name'], 280, 265);
                    } else {
                        $this->session->set_flashdata("error", lang("img_could_not_be_uploaded"));
                        redirect(base_url("admin/Products/edit_product/$productID"));
                    }
                }
                $this->Products_model->products_update($productID, $category_id, $brand_id, $data);
                $this->session->set_flashdata('success', lang('data_submitted_successfully'));
                redirect(base_url("admin/Products"));
            } else
                $this->load->view("admin/pages/products/edit_product", $data_s);
        } else if ($empty) {
            redirect(base_url("admin/Products"));
        }
    }

    function delete_product() {
        check_p('products', "d");
        $last = $this->uri->total_segments();
        $productID = $this->uri->segment($last);
        $this->Products_model->delete_record($productID, 'products');
        $this->session->set_flashdata("success", lang("product_del_successfully"));
        redirect(base_url("admin/Products"));
    }

    function supplementary() {
        check_p('products', "v");
        $last = $this->uri->total_segments();
        $productID = $this->uri->segment($last);
        $data['title'] = lang('supp_pro');
        $data['product_id'] = $productID;
        $data['products'] = $this->Global_model->get_data_not_deleted('products_sup', $productID, 'product_id');
        $data['product_name'] = $this->Products_model->get_record_name('products', $productID);
        $this->load->view("admin/pages/products/products_sup", $data);
    }

    function add_supplementary() {
        check_p('products', "c");
        $last = $this->uri->total_segments();
        $productID = $this->uri->segment($last);
        $data['title'] = lang('add_supp_pro');
        $data['check_types'] = $this->Global_model->get_data_not_deleted('check_types');
        $this->load->view("admin/pages/products/add_sup_product", $data);
        $this->form_validation->set_rules("n_en", lang("n_en"), "trim|required")
                ->set_rules("n_ar", lang("n_ar"), "trim|required")
                ->set_rules("code", lang("code"), "trim|required")
                ->set_rules("rent_price", lang("rent_price"), "trim|required")
                ->set_rules("rent_term", lang("rent_term"), "trim|required")
                ->set_rules("check_type", lang("check_type"), "trim|required")
                ->set_rules("featured", lang("featured"), "trim|required");
        if ($this->form_validation->run()) {
            $code = $this->input->post('code');
            $is_exist = $this->Global_model->isFieldExist('products_sup', 'code', $code);
            if ($is_exist) {
                $this->session->set_flashdata("error", lang("code_num_must_be_unique"));
                redirect(base_url("admin/Products/add_supplementary/$productID"));
            }
            $dt = $this->input->post();
            $data = [];
            foreach ($dt as $k => $v):
                $data[$k] = $v;
            endforeach;
            unset($data['userfile']);
            $data['product_id'] = $productID;
            $data['deleted'] = 0;
            if ($_FILES['userfile']['name'] != '') {
                $config['upload_path'] = './uploads/products';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $this->load->library("upload", $config);
                if ($this->upload->do_upload()) {
                    $upload_data = $this->upload->data();
                    $data['img'] = $upload_data['file_name'];
                    $this->Products_model->resizeImage($upload_data['file_name'], 280, 265);
                } else {
                    $this->session->set_flashdata("error", lang("img_could_not_be_uploaded"));
                    redirect(base_url("admin/Products/add_supplementary/$productID"));
                }
            }
            $this->Global_model->global_insert('products_sup', $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/Products/supplementary/$productID"));
        }
    }

    function edit_supplementary() {
        check_p('products', "u");
        $last = $this->uri->total_segments();
        $id = $this->uri->segment($last);
        $productID = $this->uri->segment($last - 1);
        $empty = '';
        $data_s['title'] = lang('edit_supp_pro');
        $data_s['product_name'] = $this->Products_model->get_record_name('products', $productID);
        $data_s['check_types'] = $this->Global_model->get_data_not_deleted('check_types');
        $data_s['item'] = $this->Products_model->get_record($id, 'products_sup');
        if ($data_s['item'] == null)
            $empty = 1;
        $data_s['empty'] = $empty;
        $this->form_validation->set_rules("code", lang("code"), "trim|required");
        if (!$empty) {
            if ($this->form_validation->run()) {
                $dt = $this->input->post();
                $data = [];
                foreach ($dt as $k => $v):
                    $data[$k] = $v;
                endforeach;
                unset($data['userfile']);
                $img = $this->input->post('userfile');
                $is_exist = $this->Global_model->isFieldExist('products_sup', 'code', $data['code']);
                if ($is_exist && $data['code'] != $data_s['item'][0]->code) {
                    $this->session->set_flashdata("error", lang("code_num_must_be_unique"));
                    redirect(base_url("admin/Products/edit_supplementary/$productID/$id"));
                }
                $data['deleted'] = 0;
                if ($_FILES['userfile']['name'] != '') {
                    $config['upload_path'] = './uploads/products';
                    $config['allowed_types'] = 'jpg|png|jpeg';
                    $this->load->library("upload", $config);
                    if ($this->upload->do_upload()) {
                        $upload_data = $this->upload->data();
                        $data['img'] = $upload_data['file_name'];
                        $this->Products_model->resizeImage($upload_data['file_name'], 280, 265);
                        unlink("./uploads/supplementary_products/$img");
                    } else {
                        $this->session->set_flashdata("error", lang("img_could_not_be_uploaded"));
                        redirect(base_url("admin/Products/edit_supplementary/$productID/$id"));
                    }
                }
                $this->Global_model->global_update('products_sup', $id, $data);
                $this->session->set_flashdata('success', lang('data_submitted_successfully'));
                redirect(base_url("admin/Products/supplementary/$productID"));
            } else
                $this->load->view("admin/pages/products/edit_sup_product", $data_s);
        } else if ($empty) {
            redirect(base_url("admin/Products/supplementary/$productID"));
        }
    }

    function delete_supplementary($id = NULL, $product_id = NULL) {
        check_p('products', "d");
//        $last = $this->uri->total_segments();
//        $productID = $this->uri->segment($last);
        $this->Products_model->delete_record($id, 'products_sup');
        $this->session->set_flashdata("success", lang("product_del_successfully"));
        redirect(base_url("admin/Products/supplementary/$product_id"));
    }

    function supplementary_items() {
        check_p('products', "v");
        $last = $this->uri->total_segments();
        $product_id = $this->uri->segment($last);
        $id = $this->uri->segment($last - 1);
        $data['title'] = lang('supp_items');
        $data['items'] = $this->Global_model->get_data_by_colID('product_sup_items', 'product_id', $product_id);
        $data['product_name'] = $this->Products_model->get_record_name('products_sup', $id);
        // $data['count'] = $this->Global_model->get_total('products_sup','product_is', $product_id);
        $this->load->view("admin/pages/products/supplementary_items", $data);
    }

    function add_supplementary_item() {
        check_p('products', "c");
        $now = date('Y-m-d');
        $last = $this->uri->total_segments();
        $productID = $this->uri->segment($last);
        $pord_name = $this->input->post('prod_name');
        $data['title'] = lang('add_supp_item');
        $data['product_name'] = $this->Products_model->get_record_name('products_sup', $productID);
        $this->form_validation->set_rules("serial_number", lang("serial_number"), "trim|required");
        if ($this->form_validation->run()) {
            $serial_number = $this->input->post('serial_number');
            $is_exist = $this->Global_model->isFieldExist('product_sup_items', 'serial_number', $serial_number);
            if ($is_exist) {
                $this->session->set_flashdata("error", lang("ser_num_must_be_unique"));
                redirect(base_url("admin/Products/items/add_supplementary_item/$productID"));
            }
            if (!empty($this->input->post('warranty_end_date')) && $this->input->post('warranty_end_date') < $now) {
                $this->session->set_flashdata("error", lang("no_old_date"));
                redirect(base_url("admin/Products/supplementary_items/$productID"));
            }
            $data = [
                'product_id' => $productID,
                'serial_number' => $serial_number,
                'cost_price' => $this->input->post('cost_price'),
                'warranty_company' => $this->input->post('warranty_company'),
                'warranty_end_date' => $this->input->post('warranty_end_date'),
                "deleted" => 0,
            ];
            $this->Global_model->global_insert('product_sup_items', $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/Products/supplementary_items/$productID"));
        } else
            $this->load->view("admin/pages/products/add_sup_item", $data);
    }

    function edit_supplementary_item() {
        check_p('products', "u");
        $now = date('Y-m-d');
        $last = $this->uri->total_segments();
        $itemID = $this->uri->segment($last);
        $productID = $this->uri->segment($last - 1);
        $empty = '';
        $data_s['title'] = lang('edit_supp_item');
        $data_s['product_name'] = $this->Products_model->get_record_name('products_sup', $productID);
        $data_s['item'] = $this->Products_model->get_record($itemID, 'product_sup_items');
        if ($data_s['item'] == null)
            $empty = 1;
        $data_s['empty'] = $empty;
        $this->form_validation->set_rules("serial_number", lang("serial_number"), "trim|required");
        if (!$empty) {
            if ($this->form_validation->run()) {
                $dt = $this->input->post();
                $data = [];
                foreach ($dt as $k => $v):
                    $data[$k] = $v;
                endforeach;
                $is_exist = $this->Global_model->isFieldExist('product_sup_items', 'serial_number', $data['serial_number']);
                if ($is_exist && $data['serial_number'] != $data_s['item'][0]->serial_number) {
                    $this->session->set_flashdata("error", lang("ser_num_must_be_unique"));
                    redirect(base_url("admin/Products/edit_supplementary_item/$productID/$itemID"));
                }
                if (!empty($data['warranty_end_date']) && $data['warranty_end_date'] < $now) {
                    $this->session->set_flashdata("error", lang("no_old_date"));
                    redirect(base_url("admin/Products/edit_supplementary_item/$productID/$itemID"));
                }
                $this->Global_model->global_update('product_sup_items', $itemID, $data);
                $this->session->set_flashdata('success', lang('data_submitted_successfully'));
                redirect(base_url("admin/Products/supplementary_items/$itemID/$productID"));
            } else
                $this->load->view("admin/pages/Products/edit_sup_item", $data_s);
        } else if ($empty) {
            redirect(base_url("admin/Products/supplementary_items/$productID"));
        }
    }

    function delete_supplementary_item() {
        check_p('products', "d");
        $last = $this->uri->total_segments();
        $productID = $this->uri->segment($last);
        $id = $this->uri->segment($last - 1);
        $this->Products_model->delete_record($id, 'product_sup_items');
        $this->session->set_flashdata("success", lang("item_del_successfully"));
        redirect(base_url("admin/Products/supplementary_items/$productID"));
    }

    function reviews($id = NULL) {
        check_p("products", "v");
        check_param($id);
        $one = $this->Products_model->get_info('products', $id);
        check_param($one, "resource");
        $data['title'] = lang('reviews');
        $data['id'] = $id;
        $data['reviews'] = $this->Products_model->get_reviews($id);
        $data['one'] = $one;
        $this->load->view("admin/pages/products/reviews", $data);
    }

    function delete_review($product_id = NULL, $review_id = NULL) {
        check_p("products", "d");
        check_param($product_id);
        check_param($review_id);
        $this->Global_model->global_update('reviews', $review_id, ["deleted" => 1]);
        $this->session->set_flashdata("success", lang("review_del_success"));
        redirect(base_url('admin/Products/reviews/' . $product_id));
    }

    function mark_item_as_sold($id = NULL, $product_id = NULL) {
        check_p("products", "u");
        check_param($id);
        check_param($product_id);
        $this->Global_model->global_update('product_items', $id, ["sold" => 1]);
        $this->session->set_flashdata("success", lang("item_sold"));
        redirect(base_url('admin/Products/items/' . $product_id));
    }

}
