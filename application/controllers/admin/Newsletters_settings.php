<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Newsletters_settings extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Newsletters_settings_model');
        set_active("newsletters_settings");
    }

    function index() {
        check_p("newsletters_settings", "v");
        $data = ['title' => lang('newsletters_settings')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['newsletters_settings'] = $this->Newsletters_settings_model->get_newsletters_settings();
        $this->load->view("admin/pages/settings/newsletters_settings", $data);
    }

    function update() {
        check_p("newsletters_settings", "u");
        $this->load->library('form_validation');
        $this->form_validation->set_rules('public_key', lang("public_key"), 'trim');
        $this->form_validation->set_rules('private_key', lang("private_key"), 'trim');
        $this->form_validation->set_rules('list_id', lang("list_id"), 'trim');
        $this->form_validation->set_rules('api_url', lang("api_url"), 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $data = [
                'public_key' => $this->input->post('public_key') == '' ? NULL : $this->input->post('public_key'),
                'private_key' => $this->input->post('private_key') == '' ? NULL : $this->input->post('private_key'),
                'list_id' => $this->input->post('list_id') == '' ? NULL : $this->input->post('list_id'),
                'api_url' => $this->input->post('api_url') == '' ? NULL : $this->input->post('api_url')
            ];
            $this->Newsletters_settings_model->update_settings($data);
            $this->session->unset_userdata('newsletters_settings');
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/Newsletters_settings"));
        }
    }

}
