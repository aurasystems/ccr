<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Check_types extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Check_types_model');
        $this->load->model('admin/Clients_model');
        set_active("check_types");
    }

    function index() {
        check_p("check_types", "v");
        set_active("check_types");

        $data['title'] = lang('check_types');
        $data['types'] = $this->Clients_model->get_data('check_types');
        $data['last_rn'] = get_last_run($data);
        $this->load->view("admin/pages/check_types/index", $data);
    }

    function add_type() {
        $data['title'] = lang('add_type');
        $this->load->view("admin/pages/check_types/add_type", $data);
        $this->form_validation->set_rules("n_en", lang("name_in") . lang("lang_en"), "trim|required")
                ->set_rules("n_ar", lang("name_in") . lang("lang_ar"), "trim|required")
                ->set_rules("run_every", lang("run_every"), "trim|required")
                ->set_rules("unit", lang("unit"), "trim|required");
        if ($this->form_validation->run()) {
            $n_en = $this->input->post('n_en');
            $exist = $this->Global_model->isFieldExist('check_types', 'n_en', $n_en);
            if (!$exist) {
                $data = [
                    'n_en' => $n_en,
                    'n_ar' => $this->input->post('n_ar'),
                    'run_every' => $this->input->post('run_every'),
                    'last_run' => get_date(),
                    'unit' => $this->input->post('unit'),
                ];
                $res = $this->Global_model->global_insert('check_types', $data);
                if ($res) {
                    $this->session->set_flashdata('success', lang('type_added_success'));
                    redirect(base_url("admin/Check_types"));
                }
            } else {
                $this->session->set_flashdata('error', lang('this_type_exist'));
                redirect(base_url("admin/Check_types/add_type"));
            }
        }
    }

    function edit_type() {
        $last = $this->uri->total_segments();
        $id = $this->uri->segment($last);
        $data['title'] = lang('edit_type');
        $data['type'] = $this->Clients_model->get_record($id, 'check_types');
        $this->load->view("admin/pages/check_types/edit_type", $data);
        $this->form_validation->set_rules("n_en", lang("name_in") . lang("lang_en"), "trim|required")
                ->set_rules("n_ar", lang("name_in") . lang("lang_ar"), "trim|required")
                ->set_rules("run_every", lang("run_every"), "trim|required")
                ->set_rules("unit", lang("unit"), "trim|required");
        if ($this->form_validation->run()) {
            $n_en = $this->input->post('n_en');
            $n_ar = $this->input->post('n_ar');
            $run_every = $this->input->post('run_every');
            $unit = $this->input->post('unit');
            $flag_1 = false;
            $flag_2 = false;
            if ($n_en != $data['type'][0]->n_en || $data['type'][0]->n_ar != $n_ar) {
                $flag_1 = $this->Global_model->isFieldExist('check_types', 'n_en', $n_en);
                $flag_2 = $this->Global_model->isFieldExist('check_types', 'n_ar', $n_ar);
            }
            if ($flag_1 == false && $flag_2 == false) {
                $data = [
                    'n_en' => $this->input->post('n_en'),
                    'n_ar' => $this->input->post('n_ar'),
                    'run_every' => $this->input->post('run_every'),
                    'unit' => $this->input->post('unit'),
                ];
                $this->Global_model->global_update('check_types', $id, $data);
                $this->session->set_flashdata('success', lang('type_edited_success'));
                redirect(base_url("admin/Check_types"));
            } else {
                $this->session->set_flashdata('error', lang('this_type_already_added'));
                redirect(base_url("admin/Check_types/edit_type/$id"));
            }
        }
    }

    function delete_type() {
        $last = $this->uri->total_segments();
        $id = $this->uri->segment($last);

        $this->Check_types_model->delete_items($id, 'check_items');
        $this->Clients_model->delete_record($id, 'check_types');
        $this->session->set_flashdata("success", lang("type_del_successfully"));
        redirect(base_url("admin/Check_types"));
    }

    function items() {
        $last = $this->uri->total_segments();
        $typeId = $this->uri->segment($last);

        $data['title'] = lang('items');
        $data['items'] = $this->Global_model->get_data_by_colID('check_items', 'type_id', $typeId);
        $data['type_name'] = $this->Check_types_model->get_record_name('check_types', $typeId);
        $this->load->view("admin/pages/check_types/items", $data);
    }

    function add_item() {
        $typeID = $this->uri->segment($this->uri->total_segments());

        $data['title'] = lang('add_item');
        $this->load->view("admin/pages/check_types/add_item", $data);

        $this->form_validation->set_rules("n_en", lang("name_in") . lang("lang_en"), "trim|required")
                ->set_rules("n_ar", lang("name_in") . lang("lang_ar"), "trim|required");

        if ($this->form_validation->run()) {
            $n_en = $this->input->post('n_en');
            $exist = $this->Global_model->isFieldExist('check_items', 'n_en', $n_en);
            if (!$exist) {
                $data = [
                    'n_en' => $n_en,
                    'n_ar' => $this->input->post('n_ar'),
                    'type_id' => $typeID
                ];

                $res = $this->Global_model->global_insert('check_items', $data);
                if ($res) {
                    $this->session->set_flashdata('success', lang('item_added_success'));
                    redirect(base_url("admin/Check_types/items/$typeID"));
                }
            } else {
                $this->session->set_flashdata('error', lang('this_item_exist'));
                redirect(base_url("admin/Check_types/add_item"));
            }
        }
    }

    function edit_item() {
        $last = $this->uri->total_segments();
        $typeID = $this->uri->segment($last);
        $id = $this->uri->segment($last - 1);

        $data['title'] = lang('edit_item');
        $data['item'] = $this->Clients_model->get_record($id, 'check_items');
        $this->load->view("admin/pages/check_types/edit_item", $data);

        $this->form_validation->set_rules("n_en", lang("name_in") . lang("lang_en"), "trim|required")
                ->set_rules("n_ar", lang("name_in") . lang("lang_ar"), "trim|required");

        if ($this->form_validation->run()) {
            $n_en = $this->input->post('n_en');
            $n_ar = $this->input->post('n_ar');
            if ($n_en != $data['item'][0]->n_en || $data['item'][0]->n_ar != $n_ar) {
                $flag_1 = $this->Global_model->isFieldExist('check_items', 'n_en', $n_en);
                $flag_2 = $this->Global_model->isFieldExist('check_items', 'n_ar', $n_ar);

                if ($flag_1 == false || $flag_2 == false) {
                    $data = [
                        'n_en' => $this->input->post('n_en'),
                        'n_ar' => $this->input->post('n_ar')
                    ];
                    $this->Global_model->global_update('check_items', $id, $data);

                    $this->session->set_flashdata('success', lang('item_edited_success'));
                } else
                    $this->session->set_flashdata('error', lang('this_item_already_added'));

                redirect(base_url("admin/Check_types/edit_item/$id/$typeID"));
            } else {
                redirect(base_url("admin/Check_types/items/$typeID"));
            }
        }
    }

    function delete_item() {
        $last = $this->uri->total_segments();
        $typeID = $this->uri->segment($last);
        $id = $this->uri->segment($last - 1);

        $this->Clients_model->delete_record($id, 'check_items');
        $this->session->set_flashdata("success", lang("item_del_successfully"));
        redirect(base_url("admin/Check_types/items/$typeID"));
    }

}
