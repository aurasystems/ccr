<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Booking_preparation extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('admin/Booking_model');
        set_active("booking_preparation");
    }

    function index() {
        set_active("booking_preparation");
        $data['title'] = lang('booking_preparation');
        $data['booked_items'] = $this->Booking_model->get_booked_data_ordered();
        //print_r($data['booked_items']);
        $this->load->view("admin/pages/booking_preparation/index", $data);
    }

    function items($booking_id = null) {
        $book_data = $this->Global_model->get_data_by_id('booking', $booking_id);
        if (!empty($book_data)) {
            $data['title'] = lang('booking_items');
            $data['bookCN'] = $this->Booking_model->get_client_name($booking_id);
            $data['items'] = $this->Booking_model->get_prep_items($booking_id);
            $data['book_id'] = $booking_id;
            $data['returned'] = $book_data->returned;
            $this->load->view("admin/pages/booking_preparation/items", $data);
        } else {
            redirect(base_url('admin/Dashboard'));
        }
    }

    function add_items() {
        $book_id = $this->input->post('book_id');
        $item_id = $this->input->post('item_id');
        $prepID = $this->input->post('prepID');
        $prodID = $this->input->post('prodID');
        $this->Global_model->global_update('booking_items_preparation', $prepID, array('item_id' => $item_id));
        $items = $this->Booking_model->get_prep_items($book_id);
        $output = '';


        $i = 1;
        foreach ($items as $one) {
            $SN = '';
            if ($one->item_id != null)
                $SN = get_SN($one->item_id);
            else
                $SN = '';
            $loc = '';
            $output .= '
                    <tr>
                        <td>' . $i++ . '</td>
                        <td>' . $one->pname . '</td>
                        <td id="update-SN' . $one->id . '">' . $SN . '</td>
                        <td>
                            <div class="btn-group">
                            <select style="width:200px" data-Bookid="' . $book_id . '" data-prodID=' . $one->product_id . ' data-prepID="' . $one->id . '" name="item-' . $one->id . '" class="dropD">
                            <option  selected  value="' . lang("choose_option") . '" disabled>' . lang("choose_option") . '</option>
                            ';
            $related_items = getItemsPerproduct('booking_items_preparation', $one->product_id, $book_id);
            foreach ($related_items as $item) {
                $loc = get_branch_by_id($item->location_id);
                $output .= '
                            <option  value="' . $item->id . '">' . $item->serial_number . '(' . $loc->{'n_' . lc()} . ')</option>
                                ';
            }
            if ($SN == null)
                $disp = 'none';
            else
                $disp = '';
            $output .= '         </select>
                            </div>
                            <div class="btn-group" style="display:' . $disp . '">
                            <button title="' . lang("remove_assigned_item") . '" onclick="removeAssignItem(' . $book_id . ',' . $one->product_id . ',' . $one->id . ')" type="button" class="close" aria-label="Close"><i style="color:red" class="fa fa-times"></i></button>
                            </div>
                        </td>
                    </tr>
                     ';
        }

        $this->Booking_model->addTodelivery($prepID, $book_id, $prodID, $item_id);
        echo json_encode($output);
    }

    function add_delivery_items() {
        $book_id = $this->input->post('book_id');
        $item_id = $this->input->post('item_id');
        $prep_id = $this->input->post('prep_id');
        $prodID = $this->input->post('prodID');
        $cname = $this->input->post('cname');
        $is_sup = $this->input->post('is_sup');

        //echo $book_id .' '. $item_id .' '. $deliver_id .' '. $prodID .' '. $cname .' '. $is_sup ;
        //print_r('');
        if ($is_sup == true) {
            $temp_deliv_record = get_record('prep_id', $prep_id, 'items_delivery_temp', false);
            $sup_data = $this->Global_model->get_data_by_id('product_sup_items', $item_id);
            $product_sup = $this->Global_model->get_data_by_id('products_sup', $sup_data->product_id);
            $deliver_sup = array(
                'sup_id' => $item_id,
                'product_sup_id' => $product_sup->id,
                'delivery_id' => $temp_deliv_record[0]->id,
                'book_id' => $book_id,
            );
            $sup_delivery_data = $this->Booking_model->get_sup_delivery_tmp($temp_deliv_record[0]->id, $product_sup->id, $book_id);
            if (!empty($sup_delivery_data))
                $this->Global_model->global_update('sup_items_temp', $sup_delivery_data->id, ['sup_id' => $item_id]);
            else
                $this->Global_model->global_insert('sup_items_temp', $deliver_sup);
        } else if ($is_sup == false) {

            $temp_deliv_record = get_record('prep_id', $prep_id, 'items_delivery_temp', false);
            $this->Global_model->global_update('items_delivery_temp', $temp_deliv_record[0]->id, array('item_id' => $item_id));
            $this->Global_model->global_update('booking_items_preparation', $prep_id, array('item_id' => $item_id));
        }
        $items = $this->Booking_model->get_prep_items($book_id);
        // print_r($items);
        $output = '';
        //echo $prodID;
        //print_r($cname);
        $i = 1;
        foreach ($items as $one) {
            $SN = '';
            $disp = '';
            if ($one->item_id != null)
                $SN = get_SN($one->item_id);
            else
                $SN = ''; //print_r($SN); 
            $loc = '';
            $output .= '
                <tr>
                    <td>' . $i++ . '</td>
                    <td>' . $one->pname . '</td>
                    <td id="update-SN' . $one->id . '">' . $SN . '</td>
                    <td>
                        <div class="btn-group">
                        <select style="width:200px" data-Bookid="' . $book_id . '" data-prodID="' . $one->product_id . '" data-cname="' . $cname . '" data-isSup=0 data-deliverID="' . $one->id . '" name="item-' . $one->id . '" class="dropD">
                        <option  selected  value="' . lang("choose_option") . '" disabled>' . lang("choose_option") . '</option>
                        ';
            $related_items = getItemsPerproduct('booking_items_delivery', $one->product_id, $book_id, true);
            foreach ($related_items as $item) {
                $loc = get_branch_by_id($item->location_id);
                $output .= '
                        <option  value="' . $item->id . '">' . $item->serial_number . '(' . $loc->{'n_' . lc()} . ')</option>
                            ';
            }
            if ($SN == null)
                $disp = 'none';

            $output .= '         </select>
                        </div>
                        <div class="btn-group" style="display:' . $disp . '">
                        <button title="' . lang("remove_assigned_item") . '" onclick="removeAssignItem(' . $book_id . ',' . $one->product_id . ',' . $one->id . ')" type="button" class="close" aria-label="Close"><i style="color:red" class="fa fa-times"></i></button>
                        </div>
                    </td>
                    <td>
                    ';
            $shutter_count = get_shutter_count($one->id);
            $output .= '     <input type="text" class="shutter_c" data-prev="' . $shutter_count . '" data-id="' . $one->id . '" value=' . $shutter_count . '>
                    </td>';
            $output .= ' <td style=" justify-content: space-between;">
                    <fieldset>';
            $supItems = get_sup_items($one->product_id);
            if (!empty($supItems)) {
                foreach ($supItems as $itemSup) {
                    $count_of_sup = assignedSups_perSupProduct($book_id, $one->id, $itemSup->id);
                    $delivery_sup = get_sup_delivery($one->id, $itemSup->id, $book_id);
                    $SN_sup = '';
                    $display = '';
                    if (!empty($delivery_sup)) {
                        if ($delivery_sup->sup_id != null)
                            $SN_sup = get_sup_SN($delivery_sup->sup_id);
                    }
                    if ($SN_sup == null)
                        $display = 'none';
                    $output .= '                   <p><strong>' . lang('product_name') . ': </strong>' . $itemSup->n_en . '<i class="fa fa-times"></i> ' . $count_of_sup . '</p>
                                     <p id="updateSup-SN' . $delivery_sup->sup_id . '"> <strong>' . lang('assigned_item_SN') . ': </strong> ' . $SN_sup . '</p>
                                     <p>
                                         <div class="btn-group">
                                         <select style="width:200px" data-Bookid="' . $book_id . '" data-prodID=' . $one->product_id . ' data-cname="' . $cname . '" data-isSup="true" data-deliverID="' . $one->id . '" name="item-' . $one->id . '" class="dropD">
                                         <option  selected  value="' . lang("choose_option") . '" disabled>' . lang("choose_option") . '</option>';
                    $related_sup_items = getSupItemsPerproduct('booking_items_delivery', $one->product_id, $book_id, $itemSup->id);
                    if (!empty($related_sup_items)) {
                        foreach ($related_sup_items as $item_sup) {
                            $output .= '          <option  value="' . $item_sup->id . '">' . $item_sup->serial_number . '</option>';
                        }
                    }
                    $output .= '                         </select>
                                             </div>
                                             <div>';
                    if ($count_of_sup > 1) {
                        $output .= '        <button style="opacity: 0.5;" title="' . lang("remove_extra_sup") . '" onclick="remove_extra(' . $book_id . ',' . $one->id . ', ' . $itemSup->id . ')" type="button" class="close" aria-label="Close"><i style="color:red; opacity: 4.5;" class="fa fa-minus"></i></button>
                                               ';
                    }
                    if (!empty($related_sup_items) && $SN_sup != null) {

                        $output .= '          <button title="' . lang("extra_sup") . '" onclick="add_extra(' . $book_id . ',' . $one->id . ', ' . $itemSup->id . ',' . end($related_sup_items)->id . ')" type="button" class="close" aria-label="Close"><i style="color:green" class="fa fa-plus"></i></button>
                                             </div>';
                    }
                    $output .= '          <div id="update_supSN' . $one->id . '" class="btn-group" style="display:' . $display . '">
                                             <button title="' . lang("remove_assigned_item") . '" onclick="removeAssignSupItem(' . $book_id . ',' . $one->product_id . ',' . $one->id . ', ' . $itemSup->id . ')" type="button" class="close" aria-label="Close"><i style="color:red" class="fa fa-times"></i></button>
                                             </div>
                                         </p>';
                }
            }

            $output .= '           </fieldset>
                    </td>
                </tr>
                 ';
        }
        echo json_encode($output);
    }

    function scan() {
        $book_id = $this->input->post('book_id');
        $book_data = $this->Global_model->get_data_by_id('booking', $book_id);
        $scan_SN = $this->input->post('scan_SN');
        $items = $this->Booking_model->getItemsAvailable_relatedToBookID($book_id, 'booking_items_preparation');
        $data = [];
        if (!empty($book_data) && !$book_data->returned) {
            $prep_id;
            $data['flag'] = false;
            $data['options'] = '';
            if ($items['flag']) {
                foreach ($items['available_items'] as $item) {
                    if ($item->SN == $scan_SN) {
                        $prep_id = $this->Booking_model->book_scanned_item($book_id, $item->id, 'booking_items_preparation');
                        //$this->Global_model->global_update('booking_items_preparation',$prep_id, array('item_id' => $item->id));
                        $items = $this->Booking_model->get_prep_items($book_id);

                        $data['flag'] = true;
                        $data['prep_id'] = $prep_id;

                        $i = 1;
                        foreach ($items as $one) {
                            $disp = '';
                            if ($one->item_id != null)
                                $SN = get_SN($one->item_id);
                            else
                                $SN = '';
                            $loc = '';
                            $data['options'] .= '
                                <tr>
                                    <td>' . $i++ . '</td>
                                    <td>' . $one->pname . '</td>
                                    <td id="update-SN' . $one->id . '">' . $SN . '</td>
                                    <td>
                                        <div class="btn-group">
                                        <select style="width:200px" data-Bookid="' . $book_id . '" data-prodID=' . $one->product_id . ' data-prepID="' . $one->id . '" name="item-' . $one->id . '" class="dropD">
                                        <option  selected  value="' . lang("choose_option") . '" disabled>' . lang("choose_option") . '</option>
                                        ';
                            $related_items = getItemsPerproduct('booking_items_preparation', $one->product_id, $book_id);
                            foreach ($related_items as $item) {
                                $loc = get_branch_by_id($item->location_id);
                                $data['options'] .= '
                                        <option  value="' . $item->id . '">' . $item->serial_number . '(' . $loc->{'n_' . lc()} . ')</option>
                                            ';
                            }
                            if ($SN == null)
                                $disp = 'none';
                            $data['options'] .= '         </select>
                                        </div>
                                        <div class="btn-group" style="display:' . $disp . '">
                                        <button title="' . lang("remove_assigned_item") . '" onclick="removeAssignItem(' . $book_id . ',' . $one->product_id . ',' . $one->id . ')" type="button" class="close" aria-label="Close"><i style="color:red" class="fa fa-times"></i></button>
                                        </div>
                                    </td>
                                </tr>
                                 ';
                        }
                        $prodID = $this->Booking_model->get_prodID($prep_id);
                        $this->Booking_model->addTodelivery($prep_id, $book_id, $prodID, $item->id);
                        break;
                    }
                }
                if (!$data['flag']) {
                    $data['flag'] = false;
                    $data['msg'] = 'this item unavailable';
                }
            } else {
                $data['flag'] = false;
                $data['msg'] = $items['msg'];
            }
        }
        echo json_encode($data);
    }

    function delivery_items_scan() {
        $book_id = $this->input->post('book_id');
        $scan_SN = $this->input->post('scan_SN');
        // $items = $this->Booking_model->getItemsAvailable_relatedToBookID($book_id, 'booking_items_preparation');
        $items = $this->Booking_model->getItemsAvailable_relatedToBookID_delivery($book_id, 'booking_items_preparation', $scan_SN);
        $data = array();
        $is_added = false;
        $data['flag'] = false;
        $data['options'] = '';
        $data['msg'] = '';
        if ($items['flag']) { //if available items in products
            //   print_r($items);
            if ($items['is_sup'] == 1)
                $is_added = $this->Booking_model->book_scanned_item_delivery($book_id, $items['available_items']->id, $items['available_items']->product_id, 'booking_items_preparation', true);
            else if ($items['is_sup'] == 0)
                $is_added = $this->Booking_model->book_scanned_item_delivery($book_id, $items['available_items']->id, $items['available_items']->product_id, 'booking_items_preparation');
            $updated_items = $this->Booking_model->get_prep_items($book_id);
            $cname = $this->Booking_model->get_client_name($book_id);
            if ($is_added == false) {
                $data['flag'] = false;
                if ($items['is_sup'])
                    $data['msg'] = lang("all_items_assigned_sup_remove");
                else if ($items['is_sup'])
                    $data['msg'] = lang("all_items_assigned_remove");
            } else
                $data['flag'] = true;

            $i = 1;
            //print_r($updated_items);
            foreach ($updated_items as $one) {
                if ($one->item_id != null)
                    $SN = get_SN($one->item_id);
                else
                    $SN = '';
                $loc = '';
                $data['options'] .= '
                                <tr>
                                    <td>' . $i++ . '</td>
                                    <td>' . $one->pname . '</td>
                                    <td id="update-SN' . $one->id . '">' . $SN . '</td>
                                    <td>
                                        <div class="btn-group">
                                        <select style="width:200px" data-Bookid="' . $book_id . '" data-prodID="' . $one->product_id . '" data-cname="' . $cname . '" data-isSup=0 data-deliverID="' . $one->id . '" name="item-' . $one->id . '" class="dropD">
                                        <option  selected  value="' . lang("choose_option") . '" disabled>' . lang("choose_option") . '</option>
                                        ';
                $related_items = getItemsPerproduct('booking_items_preparation', $one->product_id, $book_id, true);
                foreach ($related_items as $item) {
                    $loc = get_branch_by_id($item->location_id);
                    $data['options'] .= '
                                        <option  value="' . $item->id . '">' . $item->serial_number . '(' . $loc->{'n_' . lc()} . ')</option>
                                            ';
                }
                if ($SN == null)
                    $disp = '';
                else
                    $disp = 'none';
                $data['options'] .= '         </select>
                                        </div>
                                        <div class="btn-group" style="display:' . $disp . '">
                                        <button title="' . lang("remove_assigned_item") . '" onclick="removeAssignItem(' . $book_id . ',' . $one->product_id . ',' . $one->id . ')" type="button" class="close" aria-label="Close"><i style="color:red" class="fa fa-times"></i></button>
                                        </div>
                                    </td>
                                    <td>
                                    ';
                $shutter_count = get_shutter_count($one->id);
                $data['options'] .= '     <input type="text" class="shutter_c" data-prev="' . $shutter_count . '" data-id="' . $one->id . '" value=' . $shutter_count . '>
                                    </td>';
                $data['options'] .= ' <td style=" justify-content: space-between;">
                                 <fieldset>';
                $supItems = get_sup_items($one->product_id);
                if (!empty($supItems)) {
                    foreach ($supItems as $itemSup) {
                        $count_of_sup = assignedSups_perSupProduct($book_id, $one->id, $itemSup->id);
                        $delivery_sup = get_sup_delivery($one->id, $itemSup->id, $book_id);
                        $SN_sup = '';
                        $display = '';
                        if (!empty($delivery_sup)) {
                            if ($delivery_sup->sup_id != null)
                                $SN_sup = get_sup_SN($delivery_sup->sup_id);
                        }
                        if ($SN_sup == null)
                            $display = 'none';

                        $data['options'] .= '                   <p><strong>' . lang('product_name') . ': </strong>' . $itemSup->n_en . '<i class="fa fa-times"></i> ' . $count_of_sup . ' </p>
                                                  <p id="updateSup-SN' . $delivery_sup->sup_id . '"> <strong>' . lang('assigned_item_SN') . ': </strong> ' . $SN_sup . '</p>
                                                  <p>
                                                      <div class="btn-group">
                                                      <select style="width:200px" data-Bookid="' . $book_id . '" data-prodID=' . $one->product_id . ' data-cname="' . $cname . '" data-isSup="true" data-deliverID="' . $one->id . '" name="item-' . $one->id . '" class="dropD">
                                                      <option  selected  value="' . lang("choose_option") . '" disabled>' . lang("choose_option") . '</option>';
                        $related_sup_items = getSupItemsPerproduct('booking_items_delivery', $one->product_id, $book_id, $itemSup->id);
                        if (!empty($related_sup_items)) {
                            foreach ($related_sup_items as $item_sup) {
                                $data['options'] .= '      <option  value="' . $item_sup->id . '">' . $item_sup->serial_number . '</option>';
                            }
                        }
                        $data['options'] .= '        </select>
                                                 </div>
                                                 <div>';
                        if ($count_of_sup > 1) {
                            $data['options'] .= '        <button style="opacity: 0.5;" title="' . lang("remove_extra_sup") . '" onclick="remove_extra(' . $book_id . ',' . $one->id . ', ' . $itemSup->id . ', ' . $cname . ')" type="button" class="close" aria-label="Close"><i style="color:red; opacity: 4.5;" class="fa fa-minus"></i></button>
                                                    ';
                        }
                        if (!empty($related_sup_items) && $SN_sup != null) {

                            $data['options'] .= '          <button title="' . lang("extra_sup") . '" onclick="add_extra(' . $book_id . ',' . $one->id . ', ' . $itemSup->id . ',' . end($related_sup_items)->id . ', ' . $cname . ')" type="button" class="close" aria-label="Close"><i style="color:green" class="fa fa-plus"></i></button>
                                                 </div>';
                        }
                        $data['options'] .= '          <div id="update_supSN' . $one->id . '" class="btn-group" style="display:' . $display . '">
                                                 <button title="' . lang("remove_assigned_item") . '" onclick="removeAssignSupItem(' . $book_id . ',' . $one->product_id . ',' . $one->id . ', ' . $itemSup->id . ')" type="button" class="close" aria-label="Close"><i style="color:red" class="fa fa-times"></i></button>
                                                 </div>
                                             </p>';
                    }
                }

                $data['options'] .= '           </fieldset>
                                 </td>
                             </tr>
                              ';
            }
        } else {
            $data['flag'] = false;
            $data['msg'] = $items['msg'];
        }

        echo json_encode($data);
    }

    function add_extra_sup($book_id, $prep_id, $product_supID, $sup_id) {
        $item_deliv_temp = get_record('prep_id', $prep_id, 'items_delivery_temp', false);
        $deliver_sup = array(
            'sup_id' => $sup_id,
            'product_sup_id' => $product_supID,
            'delivery_id' => $item_deliv_temp[0]->id,
            'book_id' => $book_id,
        );
        $this->Global_model->global_insert('sup_items_temp', $deliver_sup);
        redirect(base_url('admin/Booking_preparation/delivery_items/' . $book_id));
    }

    function remove_extra_sup($book_id, $prep_id, $product_supID) {
        $this->Booking_model->remove_extraSup($book_id, $prep_id, $product_supID);
        redirect(base_url('admin/Booking_preparation/delivery_items/' . $book_id));
    }

    function remove_sup($deliver_supID, $book_id, $client_id) {
        $this->Booking_model->remove_delivery_sup($deliver_supID);
        redirect(base_url('admin/Booking_preparation/invoice_printing/' . $book_id . '/' . $client_id));
    }

    function delete_item($book_id, $prodID, $prep_id) {
        $this->Global_model->global_update('booking_items_preparation', $prep_id, array('item_id' => null));
        redirect(base_url('admin/Booking_preparation/items/' . $book_id));
    }

    function delete_deliver_item($book_id, $prodID, $prep_id) {
        $book_data = $this->Global_model->get_data_by_id('booking', $book_id);
        $client_data = $this->Global_model->get_data_by_id('clients', $book_data->client_id);
        $cname = $client_data->n_en;
        $temp_deliv_record = get_record('prep_id', $prep_id, 'items_delivery_temp', false);
        $this->Global_model->global_update('items_delivery_temp', $temp_deliv_record[0]->id, array('item_id' => null));
        $this->Global_model->global_update('booking_items_preparation', $prep_id, array('item_id' => null));
        redirect(base_url('admin/Booking_preparation/delivery_items/' . $book_id . '/' . $cname));
    }

    function delete_sup_item($book_id, $prodID, $prep_id, $product_supID) {
        $book_data = $this->Global_model->get_data_by_id('booking', $book_id);
        $client_data = $this->Global_model->get_data_by_id('clients', $book_data->client_id);
        $cname = $client_data->n_en;
        $temp_deliv_record = get_record('prep_id', $prep_id, 'items_delivery_temp', false);
        $this->Booking_model->update_sup_delivery('sup_items_temp', $temp_deliv_record[0]->id, $product_supID, $book_id);
        redirect(base_url('admin/Booking_preparation/delivery_items/' . $book_id . '/' . $cname));
    }

    function delivery() {
        set_active("booking_delivery");
        $data['title'] = lang('booking_delivery');
        $this->load->view("admin/pages/booking_preparation/delivery", $data);
    }

    function delivery_scan() {
        $scan_num = $this->input->post('scan_SN');
        $data = $this->Booking_model->getDeliveryData($scan_num);
        // print_r($data);
        if (!empty($data))
            echo json_encode($data);
        else
            echo json_encode(array());
    }

    function delivery_items($book_id) {
        set_active("booking_delivery");
        $book_data = $this->Global_model->get_data_by_id('booking', $book_id);
        $client_data = $this->Global_model->get_data_by_id('clients', $book_data->client_id);
        $data['title'] = lang('booking_delivery');
        $data['cname'] = $client_data->n_en;
        $data['items'] = $this->Booking_model->get_prep_items($book_id);
        if (empty($data['items'])) {
            $this->session->set_flashdata('error', $cname . lang('this_booking_has_no_items'));
            redirect('admin/Booking_preparation/delivery');
        }
        $data['book_id'] = $book_id;
        $this->Booking_model->add_to_delivery_temp($data['items'], $book_id);
        $this->load->view("admin/pages/booking_preparation/delivery_items", $data);
    }

    function update_shutter_count() {
        $prep_id = $this->input->post('prep_id');
        $new_count = $this->input->post('new_count');
        $temp_deliv_record = get_record('prep_id', $prep_id, 'items_delivery_temp', false);
        $this->Global_model->global_update('items_delivery_temp', $temp_deliv_record[0]->id, array('shutter_count' => $new_count));
        echo true;
    }

    function return() {
        //check_p("booking_return", "v");
        set_active("booking_return");
        $data['title'] = lang('booking_return');
        $this->load->view("admin/pages/booking_return/index", $data);
    }

    function booked_data() {
        $scan_num = $this->input->post('scan_SN');
        $data = $this->Booking_model->getDeliveryData($scan_num);
        if (!empty($data))
            echo json_encode($data);
        else
            echo json_encode(array());
    }

    function return_data($id) {
        set_active("booking_return");
        $data['title'] = lang('booking_return');
        $data['book_id'] = $id;
        $book_data = $this->Global_model->get_data_by_id('booking', $id);
        $client_id = $book_data->client_id;
        $client_data = $this->Global_model->get_data_by_id('clients', $client_id);
        $data['returned'] = $book_data->returned;
        $data['cname'] = $client_data->n_en;
        $data['items'] = $this->Booking_model->get_client_items($id);
        //print_r($data['items']);die;
        if (!empty($data['items'])) {
            $this->load->view("admin/pages/booking_return/data", $data);
        } else {
            redirect(base_url('admin/Booking_preparation/return'));
        }
    }

    function check_return($book_id) {
        set_active("booking_return");
        $book_data = $this->Global_model->get_data_by_id('booking', $book_id);
        $this->Global_model->global_update('booking', $book_id, ['returned' => 1]);
        $client_id = $book_data->client_id;
        $client_data = $this->Global_model->get_data_by_id('clients', $client_id);
        $cname = $client_data->n_en;
        $data = $this->Booking_model->get_client_items($book_id);
        foreach ($data as $one) {
            $missing = $this->input->post('missing-' . $one['id']) == 1 ? 1 : 0;
            $damaged = $this->input->post('damaged-' . $one['id']) == 1 ? 1 : 0;
            $ret_shutter_count = $this->input->post('ret_shutter_count-' . $one['id']);
            if ($ret_shutter_count != null) {
                $is_excceded_shutter = $this->Booking_model->calculate_shutter($ret_shutter_count, $one['shutter_count'], $one['id'], $book_id, $one['product_id'], $one['item_id']);
                if ($is_excceded_shutter) {
                    //to add penalty 
                }
            }
            $update_ret = array(
                'book_id' => $book_id,
                'product_id' => $one['product_id'],
                'item_id' => $one['item_id'],
                //'sup_id' => $one['sup_id'],
                'missing' => $missing,
                'damaged' => $damaged,
            );
            $return_id = $this->Booking_model->insert_returned_data('booking_items_return', $update_ret);
            $delivery_sup = get_delivery_sup_items($one['id']);
            if (!empty($delivery_sup)) {
                foreach ($delivery_sup as $sup) {
                    $sup_missing = $this->input->post('missing-' . $sup->sup_id) == 1 ? 1 : 0;
                    $sup_damaged = $this->input->post('damaged-' . $sup->sup_id) == 1 ? 1 : 0;
                    $update_sup_ret = array(
                        'book_id' => $book_id,
                        'product_sup_id' => $sup->product_sup_id,
                        'return_id' => $return_id,
                        'sup_id' => $sup->sup_id,
                        'missing' => $sup_missing,
                        'damaged' => $sup_damaged,
                    );
                    $sup_return_id = $this->Booking_model->insert_sup_returned_data('booking_sup_items_return', $update_sup_ret);
                    if ($sup_damaged == 1 && $sup->sup_id != null)
                        $this->Global_model->global_update('product_sup_items', $sup->sup_id, ['damaged' => 1, 'exist' => 0]);
                    if ($sup_missing == 1 && $sup->sup_id != null)
                        $this->Global_model->global_update('product_sup_items', $sup->sup_id, ['missing' => 1, 'exist' => 0]);
                }
            }
            if ($damaged == 1 && $one['item_id'] != null) {
                $this->Global_model->global_update('product_items', $one['item_id'], ['damaged' => 1, 'exist' => 0]);
            }
            if ($missing == 1 && $one['item_id'] != null) {
                $this->Global_model->global_update('product_items', $one['item_id'], ['missing' => 1, 'exist' => 0]);
            }
            $this->session->set_flashdata('success', $cname . lang('ret_data_updated'));
            send_active_survey($client_id);
        }
        $data['title'] = lang('booking_return');
        $this->load->view("admin/pages/booking_return/index", $data);
    }

    function invoice() {
        set_active("invoice_printing");
        $data['title'] = lang('invoice_printing');
        $data['booking'] = $this->Booking_model->get_ready_clients();
        $this->load->view("admin/pages/invoice_printing/index", $data);
    }

    function invoice_printing($book_id, $client_id) {
        set_active("invoice_printing");
        $data['title'] = lang('invoice_printing');
        $data['booking_items'] = $this->Booking_model->client_booked_data($book_id, $client_id);
        $data['booking'] = $this->Booking_model->get_ready_clients($book_id);
        $data['client_id'] = $client_id;
        $data['book_id'] = $book_id;
        $this->load->view("admin/pages/invoice_printing/invoice_data", $data);
    }

    function capture_img() {
        $filename = md5(time()) . '.jpg';
        $filepath = 'uploads/invoice_scan/';
        //read the raw POST data and save the file with file_put_contents()
        $result = file_put_contents($filepath . $filename, file_get_contents('php://input'));

        if (!$result) {
            print "ERROR: Failed to write data to $filename, check permissions\n";
            exit();
        } else {
            $book_id = $this->Booking_model->get_bookID();
            $data = array(
                'client_id' => cid(),
                'book_id' => $book_id,
                'document' => $filename,
            );
            $this->Global_model->global_insert('invoice_scanning', $data);
        }
        echo $filename;
    }

    function upload_invoice($book_id, $client_id) {
        $filename = '';
        if ($_FILES['userfile']['name'] != '') {
            $config['upload_path'] = './uploads/invoice_scan/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
            $config['max_size'] = '10000';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $config['encrypt_name'] = TRUE;
            $this->lang->load('upload');
            $this->load->library('upload', $config);
            if ($this->upload->do_upload()) {
                $upload_data = $this->upload->data();
                $dt['photo'] = $upload_data['file_name'];
                $filename = $_FILES['userfile']['name'];
                $this->session->set_flashdata('success', lang('uploaded_success'));
            }
        }
        if ($filename != '') {
            $data = array(
                'client_id' => $client_id,
                'book_id' => $book_id,
                'document' => $filename,
            );
            $this->Global_model->global_insert('invoice_scanning', $data);
        }
        redirect(base_url('admin/Booking_preparation/invoice_printing/' . $book_id . '/' . $client_id . ''));
    }

    function update_delivery_action_old() {
        $is_paid = $this->input->post('is_paid'); //1 for delivered and paid, 2 for unpaid
        $client_id = $this->input->post('client_id'); //1 for delivered and paid, 2 for unpaid
        $book_id = $this->input->post('book_id'); //1 for delivered and paid, 2 for unpaid
        if ($is_paid != 0)
            $this->Booking_model->update_transaction($is_paid, $client_id, $book_id);
        if ($is_paid == 1) {
            $count = $this->Booking_model->booked_count($book_id, $client_id);
            $record = $this->Booking_model->get_record('booked_products_counts', 'book_id', $book_id, $client_id);
            if (!empty($record))
                $this->Global_model->global_update('booked_products_counts', $record->id, ['items_count' => $count, 'client_id' => $client_id]);
            else
                $this->Global_model->global_insert('booked_products_counts', ['book_id' => $book_id, 'items_count' => $count, 'client_id' => $client_id]);

            $sup_data = delivery_sup_items_count($book_id);
            if (!empty($sup_data)) {
                foreach ($sup_data as $one) {
                    $record = $this->Booking_model->get_record('booked_sup_products_counts', 'book_id', $book_id, $client_id, $one['productSupID']);
                    if (!empty($record))
                        $this->Global_model->global_update('booked_sup_products_counts', $record->id, ['items_count' => $one['count']]);
                    else
                        $this->Global_model->global_insert('booked_sup_products_counts', ['sup_product_id' => $one['productSupID'], 'book_id' => $book_id, 'items_count' => $one['count'], 'client_id' => $client_id]);
                }
            }
        }
        //insert in return table
        $delivery_data = $this->Booking_model->get_delivery_items($book_id);
        if (!empty($delivery_data)) {
            $is_exist = get_record('book_id', $book_id, 'booking_items_return', false);
            if (empty($is_exist)) {
                foreach ($delivery_data as $item) {
                    $return_data = [
                        'book_id' => $book_id,
                        'product_id' => $item->product_id,
                        'item_id' => $item->item_id,
                    ];
                    $return_id = $this->Global_model->global_insert('booking_items_return', $return_data);
                    $delivery_sup_data = $this->Booking_model->get_delivery_sup_items($item->id);
                    if (!empty($delivery_sup_data)) {
                        foreach ($delivery_sup_data as $sup_item) {
                            $return_sup_data = [
                                'book_id' => $book_id,
                                'product_sup_id' => $sup_item->product_sup_id,
                                'sup_id' => $sup_item->sup_id,
                                'return_id' => $return_id,
                            ];
                            $this->Global_model->global_insert('booking_sup_items_return', $return_sup_data);
                        }
                    }
                }
            }
        }
    }

    function update_delivery_action() {
        $is_paid = $this->input->post('is_paid'); //1 for delivered and paid, 2 for unpaid
        $client_id = $this->input->post('client_id'); //1 for delivered and paid, 2 for unpaid
        $book_id = $this->input->post('book_id'); //1 for delivered and paid, 2 for unpaid
        if ($is_paid != 0)
            $this->Booking_model->update_transaction($is_paid,
                    $client_id, $book_id);
        if ($is_paid == 1) {
            $count = $this->Booking_model->booked_count($book_id,
                    $client_id);
            $record = $this->Booking_model->get_record('booked_products_counts', 'book_id', $book_id, $client_id);
            if (!empty($record))
                $this->Global_model->global_update('booked_products_counts',
                        $record->id, ['items_count' => $count, 'client_id' => $client_id]);
            else
                $this->Global_model->global_insert('booked_products_counts', ['book_id'
                    => $book_id, 'items_count' => $count, 'client_id' => $client_id]);

            $sup_data = delivery_sup_items_count($book_id);
            if (!empty($sup_data)) {
                foreach ($sup_data as $one) {
                    $record = $this->Booking_model->get_record('booked_sup_products_counts',
                            'book_id', $book_id, $client_id, $one['productSupID']);
                    if (!empty($record))
                        $this->Global_model->global_update('booked_sup_products_counts',
                                $record->id, ['items_count' => $one['count']]);
                    else
                        $this->Global_model->global_insert('booked_sup_products_counts',
                                ['sup_product_id' => $one['productSupID'], 'book_id' => $book_id, 'items_count' => $one['count'], 'client_id' => $client_id]);
                }
            }
        }
        //insert in return table
        $delivery_data = $this->Booking_model->client_booked_data($book_id,
                $client_id); //$this->Booking_model->get_delivery_items($book_id);
        if (!empty($delivery_data)) {
            $is_exist = get_record('book_id', $book_id, 'booking_items_return', false);
            if (empty($is_exist)) {
                foreach ($delivery_data as $item) {
                    $return_data = [
                        'book_id' => $book_id,
                        'product_id' => $item['product_id'],
                        'item_id' => $item['item_id'],
                    ];
                    $return_id = $this->Global_model->global_insert('booking_items_return',
                            $return_data);
                    $delivery_sup_data = $this->Booking_model->get_delivery_sup_items($item->id);
                    if (!empty($delivery_sup_data)) {
                        foreach ($delivery_sup_data as $sup_item) {
                            $return_sup_data = [
                                'book_id' => $book_id,
                                'product_sup_id' => $sup_item->product_sup_id,
                                'sup_id' => $sup_item->sup_id,
                                'return_id' => $return_id,
                            ];

                            $this->Global_model->global_insert('booking_sup_items_return',
                                    $return_sup_data);
                        }
                    }
                }
            }
        }
    }

    function ready_to_deliver($book_id) {
        $items = $this->Booking_model->get_delivery_temp($book_id);
        if (!empty($items)) {
            foreach ($items as $one) {
                $record = $this->Booking_model->get_delivery_record($book_id, $one->prep_id, $one->product_id);
                if (!empty($record)) {
                    $item_delivery = [
                        'item_id' => $one->item_id,
                        'shutter_count' => $one->shutter_count,
                    ];
                    $this->Global_model->global_update('booking_items_delivery', $record->id, $item_delivery);
                    $sup_items = $this->Booking_model->get_sup_delivery_temp($book_id, $one->id);
                    if (!empty($sup_items)) {
                        foreach ($sup_items as $sup) {
                            $sup_record = $this->Booking_model->get_sup_delivery_record($book_id, $record->id, $sup->product_sup_id);
                            $sup_delivery = [
                                'sup_id' => $sup->sup_id,
                            ];
                            $this->Global_model->global_update('booking_sup_items_delivery', $sup_record->id, $sup_delivery);
                        }
                    }
                } else {
                    $item_delivery = [
                        'book_id' => $book_id,
                        'prep_id' => $one->prep_id,
                        'product_id' => $one->product_id,
                        'item_id' => $one->item_id,
                        'shutter_count' => $one->shutter_count,
                    ];
                    $deliver_id = $this->Global_model->global_insert('booking_items_delivery', $item_delivery);
                    $sup_items = $this->Booking_model->get_sup_delivery_temp($book_id, $one->id);
                    if (!empty($sup_items)) {
                        foreach ($sup_items as $sup) {
                            $sup_delivery = [
                                'book_id' => $book_id,
                                'delivery_id' => $deliver_id,
                                'product_sup_id' => $sup->product_sup_id,
                                'sup_id' => $sup->sup_id,
                            ];
                            $this->Global_model->global_insert('booking_sup_items_delivery', $sup_delivery);
                        }
                    }
                }
            }
        }
        set_active("invoice_printing");
        $book_data = $this->Global_model->get_data_by_id('booking', $book_id);
        $data['title'] = lang('invoice_printing');
        $data['booking_items'] = $this->Booking_model->client_booked_data($book_id, $book_data->client_id);
        $data['booking'] = $this->Booking_model->get_ready_clients($book_id);
        $data['client_id'] = $book_data->client_id;
        $data['book_id'] = $book_id;
        $this->load->view("admin/pages/invoice_printing/invoice_data", $data);
    }

}
