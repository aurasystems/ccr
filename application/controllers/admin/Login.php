<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin/Login_model');
        construct_init(TRUE);
    }

    public function index() {
        if ((uid() != "")) {
            redirect('admin/Dashboard');
        } else {
            $data['title'] = lang('lang_sign_in');
            $this->load->view("admin/pages/login", $data);
        }
    }

    public function do_login() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $email = $this->input->post('email');
            $password = do_hash($this->input->post('password'), 'md5');
            $check_login_result = $this->Login_model->check_login($email, $password);
            if ($check_login_result) {
                redirect('admin/Dashboard');
            } else {
                $this->session->set_flashdata('msg', 'Wrong email or password');
                $this->index();
            }
        }
    }

    function reset_password() {
        if ((uid() != "")) {
            redirect('admin/Dashboard');
        } else {
            $data = ['title' => lang('reset_password')];
            $this->load->view("admin/pages/forget_password", $data);
        }
    }

    function do_reset_password() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        if ($this->form_validation->run() == FALSE) {
            $this->reset_password();
        } else {
            $settings = settings('all', 1);
            $email = $this->input->post('email');
            $check_user = $this->Global_model->check_email('users', $email);
            if ($check_user) {
                // user is exists
                if ((!empty($settings->smtp_host)) && (!empty($settings->smtp_port)) && (!empty($settings->smtp_user)) && (!empty($settings->smtp_pass))) {
                    $this->Global_model->send_reset_link($email);
                    redirect('admin/Login/password_reset_success');
                }
                else {
                    $this->session->set_flashdata('error', lang('error_occured'));
                    redirect('admin/Login/reset_password');
                }
                
            } else {
                // user not exists
                $this->session->set_flashdata('error', lang('email_not_found'));
                $this->reset_password();
            }
        }
    }

    function password_reset_success() {
        if ((uid() != "")) {
            redirect('admin/Dashboard');
        } else {
            $data['title'] = lang('password_recover_success');
            $this->load->view("admin/pages/password_recover_success", $data);
        }
    }

    function reset_my_password($token) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if (!$this->form_validation->run()) {
            $data = ['title' => lang('reset_password'), "token" => $token];
            $this->load->view("admin/pages/reset_my_password", $data);
        } else {
            //verify token
            $token = explode("_", $token);
            $id = $token[0];
            $user = $this->Global_model->get_data_by_id("users", $id);
            if ($user && substr($user->password, 0, 5) == $token[1]) {
                $data = ["password" => md5($this->input->post("password"))];
                $this->Global_model->global_update("users", $id, $data);
                redirect('admin/Login');
            } else {
                // user not exists
                $this->session->set_flashdata('msg', 'This reset link is invalid or has expired');
                redirect('admin/Login/reset_password');
            }
        }
    }

}
