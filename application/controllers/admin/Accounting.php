<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Accounting extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
        $this->load->model('admin/Accounting_model');
    }

    function index() {
        check_p('accounting', "v");
        set_active("accounting");
        $this->store_active_records();
        $data['title'] = lang('accounting');
        $data['income'] = $this->Accounting_model->get_total_accounting(1); //type 1 income
        $data['expense'] = $this->Accounting_model->get_total_accounting(2); //type 2 expense
        $data['income_dt'] = $this->Accounting_model->get_trans_data(true);
        $data['expense_dt'] = $this->Accounting_model->get_trans_data();
        $this->load->view("admin/pages/accounting/index", $data);
    }

    function store_active_records() {
        $data = [
            'id' => '1',
            'n_en' => 'income',
            'n_ar' => 'الداخل',
            'parent' => 0,
            'type' => 1,
            'editable' => 0,
        ];
        $income = $this->Global_model->get_data_by_id('accounting_types', 1);
        if (empty($income))
            $this->db->insert('accounting_types', $data);
        $data = [
            'id' => '2',
            'n_en' => 'expense',
            'n_ar' => 'المدفوعات',
            'parent' => 0,
            'type' => 2,
            'editable' => 0,
        ];
        $expense = $this->Global_model->get_data_by_id('accounting_types', 2);
        if (empty($expense))
            $this->db->insert('accounting_types', $data);
        $data = [
            'id' => '3',
            'n_en' => 'maintenance',
            'n_ar' => 'الصيانة',
            'parent' => 2,
            'type' => 0,
            'editable' => 0,
        ];
        $maintenance = $this->Global_model->get_data_by_id('accounting_types', 3);
        if (empty($maintenance))
            $this->db->insert('accounting_types', $data);
        $data = [
            'id' => '4',
            'n_en' => 'booking',
            'n_ar' => 'المشتريات',
            'parent' => 1,
            'type' => 0,
            'editable' => 0,
        ];
        $booking = $this->Global_model->get_data_by_id('accounting_types', 4);
        if (empty($booking))
            $this->db->insert('accounting_types', $data);
    }

    function add() {
        check_p('accounting', "c");
        set_active("add_type");
        $this->store_active_records();
        $this->form_validation->set_rules("n_en", lang("name_en"), "trim|required");
        $this->form_validation->set_rules("n_ar", lang("name_ar"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('add_type');
            $data['parents'] = $this->Accounting_model->get_types();
            $this->load->view("admin/pages/accounting/add", $data);
        } else {
            $data = [
                'n_en' => $this->input->post('n_en'),
                'n_ar' => $this->input->post('n_ar'),
                'parent' => $this->input->post('parent'),
                'type' => $this->input->post('parent') == 0 ? $this->input->post('type') : 0,
                'editable' => 1,
            ];

            $this->Global_model->global_insert('accounting_types', $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/Accounting/add'));
        }
    }

    function types() {
        check_p('accounting', "v");
        set_active("types");
        $this->store_active_records();
        $data['title'] = lang('types');
        $data['types'] = $this->Accounting_model->get_types();
        $this->load->view("admin/pages/accounting/types", $data);
    }

    function sub($parent) {
        check_p('accounting', "v");
        check_param($parent);
        $data['title'] = lang('subcategories');
        $data['sub'] = $this->Accounting_model->get_sub($parent);
        $data['parent'] = $this->Global_model->get_data_by_id('accounting_types', $parent);
        $this->load->view("admin/pages/accounting/sub_types", $data);
    }

    function edit($id) {
        check_p('accounting', "u");
        check_param($id);
        $this->form_validation->set_rules("n_en", lang("name_en"), "trim|required");
        $this->form_validation->set_rules("n_ar", lang("name_ar"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('edit');
            $data['data'] = $this->Global_model->get_data_by_id('accounting_types', $id);
            $data['parents'] = $this->Accounting_model->get_types();
            $this->load->view("admin/pages/accounting/edit", $data);
        } else {
            $data = [
                'n_en' => $this->input->post('n_en'),
                'n_ar' => $this->input->post('n_ar'),
                'parent' => $this->input->post('parent'),
                'type' => $this->input->post('parent') == 0 ? $this->input->post('type') : 0,
            ];

            $this->Global_model->global_update('accounting_types', $id, $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/Accounting/edit/' . $id));
        }
    }

    function add_transaction() {
        check_p('accounting', "c");
        set_active("add_transaction");
        $this->store_active_records();
        $type_id = 0;
        $this->form_validation->set_rules("type", lang("type"), "trim|required");
        $this->form_validation->set_rules("cost", lang("cost"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = lang('add_transaction');
            $data['types'] = $this->Global_model->get_data_by_colID('accounting_types', 'parent', 0);
            $data['sub_income'] = $this->Accounting_model->get_sub(null, true);
            $data['sub_expense'] = $this->Accounting_model->get_sub();
            $this->load->view("admin/pages/accounting/add_transaction", $data);
        } else {
            if ($this->input->post('type') == 1) {
                if ($this->input->post('sub_n') == 0)
                    $type_id = $this->input->post('type');
                else
                    $type_id = $this->input->post('sub_n');
            } else if ($this->input->post('type') == 2) {
                if ($this->input->post('sub_e') == 0)
                    $type_id = $this->input->post('type');
                else
                    $type_id = $this->input->post('sub_e');
            }


            $data = [
                'type_id' => $type_id,
                'cost' => $this->input->post('cost'),
                'description' => $this->input->post('description'),
            ];
            $this->Global_model->global_insert('transaction', $data);
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/Accounting/add_transaction'));
        }
    }

    function delete($id) {
        check_p('accounting', "d");
        check_param($id);
        $one = $this->Accounting_model->get_category($id);
        if (!empty($one)) {
            $sub = $this->Accounting_model->get_sub($one->id);
            $sub_error = !empty($sub) ? lang("check_sub_category") : NULL;
            if ($sub_error) {
                $this->session->set_flashdata("error", $sub_error);
                redirect(base_url('admin/accounting/types'));
            }
            $this->Global_model->global_update('accounting_types', $id, ["deleted" => 1]);
            $this->Accounting_model->delete_relatedTicket($id);
            $this->session->set_flashdata("success", lang("data_deleted_successfully"));
            redirect(base_url('admin/accounting/types'));
        } else {
            $error = lang("category_not_found");
            $this->session->set_flashdata("error", $error);
            redirect(base_url('admin/accounting/types'));
        }
    }

    function products() {
        check_p('accounting', "v");
        set_active("products_stat");
        $data['title'] = lang('products_stat');
        $data['products'] = $this->Accounting_model->get_table_data('products');
        $this->load->view("admin/pages/accounting/products", $data);
    }

    function items($product_id, $rent_price) {
        check_p('accounting', "v");
        check_param($product_id);
        check_param($rent_price);
        $data['title'] = lang('items');
        $data['items'] = $this->Accounting_model->get_items_data($product_id, $rent_price);
        $this->load->view("admin/pages/accounting/items", $data);
    }

    function sup_items($product_id, $rent_price) {
        check_p('accounting', "v");
        check_param($product_id);
        check_param($rent_price);
        $data['title'] = lang('sup_items');
        $data['items'] = $this->Accounting_model->get_sup_items_data($product_id, $rent_price);
        $this->load->view("admin/pages/accounting/items", $data);
    }

    function products_usage() {
        check_p('accounting', "v");
        set_active("products_usage");
        $data['title'] = lang('products');
        $data['products'] = $this->Accounting_model->get_table_data('products');
        $this->load->view("admin/pages/accounting/products_usage", $data);
    }

    function items_usage($product_id) {
        check_p('accounting', "v");
        check_param($product_id);
        $data['title'] = lang('usage');
        $data['usage'] = $this->Accounting_model->get_usage_data($product_id);
        $this->load->view("admin/pages/accounting/items_usgae", $data);
    }

    function transaction() {
        check_p('accounting', "v");
        set_active("transaction");
        $data['title'] = lang('transaction');
        $this->load->view("admin/pages/accounting/transaction", $data);
    }

    function fetch_transaction() {
        $type = $this->input->post('type');
        $data['trans'] = $this->Accounting_model->get_transaction_data($type);
        $data['fetch'] = $this->Accounting_model->get_client_data($data['trans'], $type);
        echo json_encode($data);
    }

    function get_voucher_code() {
        check_p('accounting', "v");
        $v_id = $this->input->post('voucher_id');
        $res = $this->Global_model->get_data_by_id('vouchers', $v_id);
        echo $res->voucher_number;
    }

}
