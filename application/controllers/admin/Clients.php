<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clients extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        date_default_timezone_set('Africa/Cairo');
        $this->load->model('admin/Clients_model');
        $this->load->model('admin/Settings_model');
        $this->load->model('admin/Survey_model');
        set_active("clients");
    }

    function waiting_approve() {
        check_p("clients", "v");
        set_active("waiting_approve");
        $data['title'] = lang('waiting_approve');
        $data['clients_data'] = $this->Clients_model->get_waiting_approve('clients');
        $data['reasons'] = $this->Clients_model->get_data('decline_reason');
        $this->load->view('admin/pages/clients/waiting_approve', $data);
    }

    function approved() {
        check_p("clients", "v");
        set_active("approved_clients");
        $data['title'] = lang('approved_clients');
        $data['clients_data'] = $this->Clients_model->get_approved('clients');
        $data['reasons'] = $this->Clients_model->get_data('decline_reason');
        $this->load->view('admin/pages/clients/approved', $data);
    }

    function unapproved() {
        check_p("clients", "v");
        set_active("unapproved_clients");
        $data['title'] = lang('unapproved_clients');
        $data['clients_data'] = $this->Clients_model->get_unapproved_clients('clients');
        $this->load->view('admin/pages/clients/unapproved', $data);
    }

    function edit() {
        check_p("clients", "u");
        set_active("edit_client");
        $last = $this->uri->total_segments();
        $id = $this->uri->segment($last);
        check_param($id);
        $flag = true; //to make sure this page is visited with client id
        $data['title'] = lang('edit_account');
        $data['client_data'] = $this->Clients_model->get_record($id, 'clients');
        if ($data['client_data'][0] == null)
            $flag = false;
        $data['flag'] = $flag;
        $this->load->view('admin/pages/clients/edit', $data);
        $this->form_validation->set_rules("n_en", lang("name_in") . lang("lang_en"), "trim|required");
        $this->form_validation->set_rules("n_ar", lang("name_in") . lang("lang_ar"), "trim|required");
        $this->form_validation->set_rules("address_en", lang("address_en") . lang("lang_ar"), "trim|required");
        $this->form_validation->set_rules("address_ar", lang("address_ar") . lang("lang_ar"), "trim|required");
        $this->form_validation->set_rules("phone", lang("phone"), "trim|required");
        $this->form_validation->set_rules("birthday", lang("birthday"), "trim|required");
        $this->form_validation->set_rules("gender", lang("gender"), "trim|required");
        $this->form_validation->set_rules("type", lang("type"), "trim|required");
        if ($flag)
            if ($this->input->post('email') != $data['client_data'][0]->email)
                $this->form_validation->set_rules("email", lang("email"), "valid_email|is_unique[clients.email]|trim|required");
            else {
                $this->form_validation->set_rules("email", lang("email"), "valid_email|trim|required");
            }
        if ($this->input->post('national_id') != $data['client_data'][0]->national_id)
            $this->form_validation->set_rules("national_id", lang("national_id"), "trim|required|is_unique[clients.national_id]|min_length[14]|max_length[14]|numeric");
        else {
            $this->form_validation->set_rules("national_id", lang("national_id"), "trim|required|min_length[14]|max_length[14]|numeric");
        }
        $this->form_validation->set_rules("password", lang("password"), "matches[confirm_password]|trim|min_length[4]|max_length[32]");
        $this->form_validation->set_rules("confirm_password", lang("confirm_password"), "matches[password]|trim|min_length[4]|max_length[32]");
        if ($this->form_validation->run()) {
            $data = $this->input->post();
            if ($data['gender'] == '') {
                $this->session->set_flashdata('error', lang("specify_gender"));
                redirect(base_url("admin/Clients/edit/$id"));
            }
            if ($data['type'] == '') {
                $this->session->set_flashdata('error', lang("specify_account"));
                redirect(base_url("admin/Clients/edit/$id"));
            }
            if ($data['type'] == 1 && $data['corp_n'] == '') {
                $this->session->set_flashdata('error', lang("specify_corp_name"));
                redirect(base_url("admin/Clients/edit/$id"));
            }
            $dt = [];
            foreach ($data as $k => $v):
                $dt[$k] = $v;
            endforeach;
            if (!empty($dt['password']) && !empty($dt['confirm_password'])) {
                $dt['password'] = do_hash($dt['password'], 'md5');
            } else {
                unset($dt['password']);
            }
            unset($dt['confirm_password']);
            if ($dt['type'] == 2) {
                $dt['corp_n'] = NULL;
            }
            if ($this->Global_model->global_update("clients", $id, $dt))
                $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url("admin/Clients/edit/$id"));
        }
    }

    function delete_client($id = NULL, $flag = NULL) {
        check_p("clients", "d");
        check_param($id);
        if ($flag == 0) {
            // redirect to waiting approve
            $redirect = 'admin/Clients/waiting_approve';
        } elseif ($flag == 1) {
            // redirect to approve
            $redirect = 'admin/Clients/approved';
        } elseif ($flag == 2) {
            // redirect to unapprove
            $redirect = 'admin/Clients/unapproved';
        } else {
            // redirect to approve
            $redirect = 'admin/Clients/approved';
        }
        $this->Clients_model->delete_account($id);
        $this->session->set_flashdata("success", lang("client_del_successfully"));
        redirect(base_url($redirect));
    }

    function add_client() {
        check_p("clients", "c");
        set_active("add_client");
        $data['title'] = lang('add_client');
        $this->form_validation->set_rules("n_en", lang("name_in") . lang("lang_en"), "trim|required")
                ->set_rules("n_ar", lang("name_in") . lang("lang_ar"), "trim|required")
                ->set_rules("national_id", lang("national_id"), "trim|required|is_unique[clients.national_id]|min_length[14]|max_length[14]|numeric")
                ->set_rules("birthday", lang("birthday") . lang("lang_ar"), "trim|required")
                ->set_rules("address_en", lang("address_en") . lang("lang_en"), "trim|required")
                ->set_rules("address_ar", lang("address_ar") . lang("lang_ar"), "trim|required")
                ->set_rules("phone", lang("phone"), "trim|required")
                ->set_rules("type", lang("type"), "trim|required")
                ->set_rules("email", lang("email"), "trim|required|valid_email|is_unique[clients.email]")
                ->set_rules("password", lang("password"), "matches[confirm_password]|trim|required|min_length[4]|max_length[32]")
                ->set_rules("confirm_password", lang("confirm_password"), "matches[password]|trim|required|min_length[4]|max_length[32]")
                ->set_rules("gender", lang("gender"), "trim|required");
        if (!$this->form_validation->run()) {
            $this->load->view('admin/pages/clients/create', $data);
        } else {
            $data = $this->input->post();
            if ($data['gender'] == '') {
                $this->session->set_flashdata('error', lang("specify_gender"));
                redirect(base_url() . 'admin/Clients/add_client');
            }
            if ($data['type'] == '') {
                $this->session->set_flashdata('error', lang("specify_account"));
                redirect(base_url() . 'admin/Clients/add_client');
            }
            if ($data['type'] == 1 && $data['corp_n'] == '') {
                $this->session->set_flashdata('error', lang("specify_corp_name"));
                redirect(base_url() . 'admin/Clients/add_client');
            }
            $dt = [];
            foreach ($data as $k => $v):
                $dt[$k] = $v;
            endforeach;
            unset($dt['confirm_password']);
            $dt['password'] = md5($this->input->post('password'));
            $dt['gender'] = $this->input->post('gender');
            $dt['type'] = $this->input->post("type");
            $dt['status'] = '0';
            $dt['added_by'] = uid();
            $res = $this->Global_model->global_insert('clients', $dt);
            if (!$res) {
                $this->session->set_flashdata('error', lang('error_occured'));
                redirect(base_url() . 'admin/Clients/add_client');
            } else if ($res) {
                $this->session->set_flashdata('success', lang('data_submitted_successfully'));
                redirect(base_url() . 'admin/Clients/add_client');
            }
        }
    }

    function approve_client($id) {
        check_p("clients", "e");
        check_param($id);
        $one = $this->Global_model->get_data_by_id('clients', $id);
        check_param($one, "resource");
        $res = $this->Clients_model->approve_client($id);
        if ($res) {
            // check for email/sms notifications to be send
            $settings = settings('all', 3);
            if (!empty($settings)) {
                $notification = $settings->notification_type;
                $email = $one->email;
                $phone = $one->phone;
                $msg = $settings->app_reg_text;
                if ($notification == 1 || $notification == 3) {
                    // send email notification
                    $data = [
                        'to' => $email,
                        'subject' => lang("approved_reg"),
                        'message' => $msg
                    ];
                    send_email($data);
                }
                if ($notification == 2 || $notification == 3) {
                    // send sms notification
                    $sent = send_sms($phone, $msg);
                    if (!$sent) {
                        // insert into failed msgs
                        $failed_dt = [
                            'client_id' => $id,
                            'phone' => $phone,
                            'msg' => $msg
                        ];
                        $this->Global_model->global_insert('failed_messages', $failed_dt);
                    }
                }
            }
            $this->session->set_flashdata('success', lang('data_submitted_successfully'));
        } else {
            $this->session->set_flashdata('error', lang('error_occured'));
        }
        redirect(base_url() . 'admin/Clients/waiting_approve');
    }

    function unapprove_client() {
        check_p("clients", "e");
        $last = $this->uri->total_segments();
        $id = $this->uri->segment($last);
        check_param($id);
        $one = $this->Global_model->get_data_by_id('clients', $id);
        check_param($one, "resource");
        $res = $this->Clients_model->unapprove_client($id);
        if ($res) {
            // check for email/sms notifications to be send
            $settings = settings('all', 3);
            if (!empty($settings)) {
                $notification = $settings->notification_type;
                $email = $one->email;
                $phone = $one->phone;
                $msg = $settings->unapp_reg_text;
                if ($notification == 1 || $notification == 3) {
                    // send email notification
                    $data = [
                        'to' => $email,
                        'subject' => lang("unapproved_reg"),
                        'message' => $msg
                    ];
                    send_email($data);
                }
                if ($notification == 2 || $notification == 3) {
                    // send sms notification
                    $sent = send_sms($phone, $msg);
                    if (!$sent) {
                        // insert into failed msgs
                        $failed_dt = [
                            'client_id' => $id,
                            'phone' => $phone,
                            'msg' => $msg
                        ];
                        $this->Global_model->global_insert('failed_messages', $failed_dt);
                    }
                }
            }
            $this->session->set_flashdata('success', lang('unapproved_success'));
            redirect(base_url() . 'admin/Clients/get_clients');
        }
    }

    function limits() {
        check_p("clients", "e");
        $this->form_validation->set_rules("cash_limit", lang("cash_limit"), "trim|required")
                ->set_rules("credit_limit", lang("credit_limit"), "trim|required")
                ->set_rules("flag_1", lang("flag_1"), "trim|required")
                ->set_rules("flag_2", lang("flag_2"), "trim|required");
        if ($this->form_validation->run()) {
            $limit_data = [
                'cash_limit' => $this->input->post('cash_limit'),
                'credit_limit' => $this->input->post('credit_limit'),
                'flag_1' => $this->input->post('flag_1'),
                'flag_2' => $this->input->post('flag_2')
            ];
            $id = $this->input->post('id');
            $this->Global_model->global_update("clients", $id, $limit_data);
            $this->approve_client($id);
            $this->session->set_flashdata('success', lang('data_submitted_successfully'));
            redirect(base_url() . 'admin/Clients/waiting_approve');
        } else {
            $this->session->set_flashdata('error', lang('error_occured'));
            redirect(base_url() . 'admin/Clients/waiting_approve');
        }
    }

    function decline_client_old() {
        $last = $this->uri->total_segments();
        $reason = $this->uri->segment($last);
        $id = $this->uri->segment($last - 1);
        $email = $this->Clients_model->get_email($id, 'clients');
        $declined_data = [
            'decline_reason' => $reason,
            'deleted' => 1,
            'deleted_email' => $email,
            'email' => ''
        ];
        $res = $this->Global_model->global_update('clients', $id, $declined_data);
        if ($res) {
            $one = $this->Global_model->get_data_by_id('clients', $id);
            // check for email/sms notifications to be send
            $settings = settings('all', 3);
            if (!empty($settings)) {
                $notification = $settings->notification_type;
                $phone = $one->phone;
                $email = $one->email;
                $msg = $settings->unapp_reg_text;
                if ($notification == 1 || $notification == 3) {
                    // send email notification
                    $data = [
                        'to' => $email,
                        'subject' => lang("unapproved_reg"),
                        'message' => $msg
                    ];
                    send_email($data);
                }
                if ($notification == 2 || $notification == 3) {
                    // send sms notification
                    $sent = send_sms($phone, $msg);
                    if (!$sent) {
                        // insert into failed msgs
                        $failed_dt = [
                            'client_id' => $id,
                            'phone' => $phone,
                            'msg' => $msg
                        ];
                        $this->Global_model->global_insert('failed_messages', $failed_dt);
                    }
                }
            }
            $this->session->set_flashdata('success', lang('declined_success'));
            redirect(base_url() . 'admin/Clients/get_clients');
        }
    }

    function decline_client($flag = NULL) {
        check_p("clients", "e");
        $client_id = $this->input->post('client_id');
        check_param($client_id);
        $one = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($one, "resource");
        if ($flag == 0) {
            // redirect to waiting approve
            $redirect = 'admin/Clients/waiting_approve';
        } else {
            // redirect to approve
            $redirect = 'admin/Clients/approved';
        }
        $this->form_validation->set_rules("reason", lang("decline_reason"), "trim|required");
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata("error", lang("select_d_r"));
            redirect(base_url() . $redirect);
        } else {
            $data = [
                'decline_reason' => $this->input->post('reason'),
                "status" => 2,
            ];
            $res = $this->Global_model->global_update('clients', $client_id, $data);
            if ($res) {
                // check for email/sms notifications to be send
                $settings = settings('all', 3);
                if (!empty($settings)) {
                    $notification = $settings->notification_type;
                    $phone = $one->phone;
                    $email = $one->email;
                    $msg = $settings->unapp_reg_text;
                    if ($notification == 1 || $notification == 3) {
                        // send email notification
                        $data = [
                            'to' => $email,
                            'subject' => lang("unapproved_reg"),
                            'message' => $msg
                        ];
                        send_email($data);
                    }
                    if ($notification == 2 || $notification == 3) {
                        // send sms notification
                        $sent = send_sms($phone, $msg);
                        if (!$sent) {
                            // insert into failed msgs
                            $failed_dt = [
                                'client_id' => $id,
                                'phone' => $phone,
                                'msg' => $msg
                            ];
                            $this->Global_model->global_insert('failed_messages', $failed_dt);
                        }
                    }
                }
                $this->session->set_flashdata('success', lang('data_submitted_successfully'));
                redirect(base_url() . $redirect);
            } else {
                $this->session->set_flashdata('error', lang('error_occured'));
                rredirect(base_url() . $redirect);
            }
        }
    }

    function approve_this_client($id) {
        check_p("clients", "e");
        check_param($id);
        $one = $this->Global_model->get_data_by_id('clients', $id);
        check_param($one, "resource");
        $res = $this->Clients_model->approve_client($id);
        if ($res) {
            $this->session->set_flashdata('success', lang('data_submitted_successfully'));
        } else {
            $this->session->set_flashdata('error', lang('error_occured'));
        }
        redirect(base_url() . 'admin/Clients/unapproved');
    }

    function dashboard($client_id = NULL) {
        check_param($client_id);
        $client = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($client, "resource");
        $data['title'] = lang('dashboard');
        $data['client_id'] = $client_id;
        $data['active'] = 'dashboard';
        $data['client'] = $client;
        $data['bookings_count'] = $this->Clients_model->get_bookings($client_id, TRUE);
        $data['bookings'] = $this->Clients_model->get_bookings($client_id, FALSE);
        $data['last_booking'] = $this->Clients_model->get_last_booking($client_id);
        $data['most_rented'] = $this->Clients_model->most_rented($client_id);

        $this->load->view('admin/pages/clients/dashboard', $data);
    }

    function old_bookings($client_id = NULL) {
        check_param($client_id);
        $client = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($client, "resource");
        $data['title'] = lang('old_bookings');
        $data['client_id'] = $client_id;
        $data['active'] = 'old_bookings';
        $data['client'] = $client;
        $data['bookings'] = $this->Clients_model->old_bookings($client_id);
        $this->load->view('admin/pages/clients/old_bookings', $data);
    }

    function today_bookings($client_id = NULL) {
        check_param($client_id);
        $client = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($client, "resource");
        $data['title'] = lang('today_bookings');
        $data['client_id'] = $client_id;
        $data['active'] = 'today_bookings';
        $data['client'] = $client;
        $data['bookings'] = $this->Clients_model->today_bookings($client_id);
        $this->load->view('admin/pages/clients/today_bookings', $data);
    }

    function coming_bookings($client_id = NULL) {
        check_param($client_id);
        $client = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($client, "resource");
        $data['title'] = lang('coming_bookings');
        $data['client_id'] = $client_id;
        $data['active'] = 'coming_bookings';
        $data['client'] = $client;
        $data['bookings'] = $this->Clients_model->coming_bookings($client_id);
        $this->load->view('admin/pages/clients/coming_bookings', $data);
    }

    function no_shows($client_id = NULL) {
        check_param($client_id);
        $client = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($client, "resource");
        $data['title'] = lang('no_shows');
        $data['client_id'] = $client_id;
        $data['active'] = 'no_shows';
        $data['client'] = $client;
        $data['bookings'] = $this->Clients_model->no_shows_bookings($client_id);
        $data['settings'] = settings('all', 3);
        $this->load->view('admin/pages/clients/no_shows', $data);
    }

    function invoice($booking_id = NULL, $client_id = NULL) {
        check_param($booking_id);
        check_param($client_id);
        $booking = $this->Global_model->get_data_by_id('booking', $booking_id);
        $client = $this->Global_model->get_data_by_id('clients', $client_id);
        $client_booking = $this->Clients_model->get_booking_row($booking_id, $client_id);
        check_param($booking, "resource");
        check_param($client, "resource");
        check_param($client_booking, "resource");
        $data['title'] = lang('no_shows');
        $data['booking_id'] = $booking_id;
        $data['client_id'] = $client_id;
        $data['booking'] = $booking;
        $data['client'] = $client;
        $data['booking_items'] = $this->Clients_model->booking_items($booking_id);
        $data['settings'] = settings('all', 3);
        $this->load->view('admin/pages/clients/invoice', $data);
    }

    function items_with_maintenance($client_id = NULL) {
        check_param($client_id);
        $client = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($client, "resource");
        $data['title'] = lang('maintenance_on_rented_items');
        $data['client_id'] = $client_id;
        $data['active'] = 'maintenance';
        $data['client'] = $client;
        $data['items_maint'] = $this->Clients_model->items_with_maintenance($client_id);
        $this->load->view('admin/pages/clients/maintenance', $data);
    }

    function wallet($client_id = NULL) {
        check_param($client_id);
        $client = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($client, "resource");
        $data['title'] = lang('penalties');
        $data['client_id'] = $client_id;
        $data['active'] = 'wallet';
        $data['client'] = $client;
        $data['penalties'] = $this->Clients_model->get_penalties($client_id);
        $this->load->view('admin/pages/clients/wallet', $data);
    }

    function penalties_history($client_id = NULL, $booking_id = NULL, $item_id = NULL) {
        check_param($client_id);
        check_param($booking_id);
        check_param($item_id);
        $client = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($client, "resource");
        $booking = $this->Global_model->get_data_by_id('booking', $booking_id);
        check_param($booking, "resource");
        $item = get_product_item_by_id($item_id);
        check_param($item, "resource");
        $data['title'] = lang('penalty_history');
        $data['client_id'] = $client_id;
        $data['active'] = 'wallet';
        $data['client'] = $client;
        $data['penalties'] = $this->Clients_model->get_penalties_history($client_id, $booking_id, $item_id);
        $this->load->view('admin/pages/clients/wallet_history', $data);
    }

    function settle_penalty() {
        check_p('wallet', "u");
        $postData = $this->input->post();
        $client_id = $postData['client_id'];
        $booking_id = $postData['booking_id'];
        $item_id = $postData['item_id'];
        $sum = $postData['sum'];
        $due_to = $postData['due_to'];
        if (empty($client_id) || empty($booking_id) || empty($item_id) || empty($sum)) {
            $this->session->set_flashdata('error', lang('error_occured'));
            echo json_encode(FALSE);
        }
        $client = $this->Global_model->get_data_by_id('clients', $client_id);
        $booking = $this->Global_model->get_data_by_id('booking', $booking_id);
        $item = get_product_item_by_id($item_id);
        if ($client && $booking && $item) {
            // 	Get sum
            $total_penalties = $this->Clients_model->get_total_penalties($client_id, $booking_id, $item_id);
            if ($total_penalties < 0) {
                $new = (-1) * ($total_penalties);
                $data = [
                    'client_id' => $client_id,
                    'booking_id' => $booking_id,
                    'item_id' => $item_id,
                    'due_to' => $due_to,
                    'amount' => $new,
                ];
                $inserted = $this->Global_model->global_insert('wallet', $data);
                if ($inserted) {
                    $this->session->set_flashdata('success', lang('data_submitted_successfully'));
                    echo json_encode(TRUE);
                }
            } else {
                $this->session->set_flashdata("error", lang('error_occured'));
                echo json_encode(FALSE);
            }
        } else {
            $this->session->set_flashdata("error", lang('data_not_found'));
            echo json_encode(FALSE);
        }
    }

    function surveys($client_id = NULL) {
        check_param($client_id);
        $client = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($client, "resource");
        $data['title'] = lang('surveys');
        $data['client_id'] = $client_id;
        $data['active'] = 'surveys';
        $data['client'] = $client;
        $data['related_surveys'] = $this->Clients_model->get_related_surveys($client_id);
        $this->load->view('admin/pages/clients/surveys', $data);
    }

    function survey_history($id = NULL, $survey_id = NULL, $client_id = NULL) {
        check_param($id);
        check_param($client_id);
        check_param($survey_id);
        $client = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($client, "resource");
        $survey = $this->Survey_model->get_survey_info($survey_id);
        check_param($survey, "resource");
        $data['title'] = lang('surveys');
        $data['client_id'] = $client_id;
        $data['active'] = 'surveys';
        $data['client'] = $client;
        $data['survey_id'] = $survey_id;
        $data['history'] = $this->Clients_model->get_survey_history($survey_id, $client_id);
        $this->load->view('admin/pages/clients/surveys_history', $data);
    }

    function show_client_ans($id = NULL, $survey_id = NULL, $client_id = NULL) {
        check_param($id);
        check_param($survey_id);
        check_param($client_id);
        $survey_data = $this->Survey_model->get_survey_info($survey_id);
        check_param($survey_data, "resource");
        $client_data = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($client_data, "resource");
        $client_survey_b_t = $this->Clients_model->get_client_survey_b_t_submitted($id, $survey_id, $client_id);
        $data = ['title' => lang('clients_answers')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['id'] = $id;
        $data['survey_id'] = $survey_id;
        $data['client_id'] = $client_id;
        $data['survey_data'] = $survey_data;
        $data['client_data'] = $client_data;
        $data['client_survey_b_t'] = $this->Clients_model->get_client_survey_b_t_submitted($id, $survey_id, $client_id);
        $data['client_answers'] = $this->Clients_model->get_client_answers($id, $survey_id, $client_id);
        $this->load->view("admin/pages/clients/long_questions_answers", $data);
    }

    function edit_answers($id = NULL, $survey_id = NULL, $client_id = NULL) {
        check_param($id);
        check_param($survey_id);
        check_param($client_id);
        $survey_data = $this->Survey_model->get_survey_info($survey_id);
        $client_data = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($survey_data, "resource");
        check_param($client_data, "resource");
        $data = ['title' => lang('answers')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['id'] = $id;
        $data['survey_id'] = $survey_id;
        $data['client_id'] = $client_id;
        $data['client'] = $client_data;
        $check_exists_code = $this->Clients_model->get_survey_basic_by_client($id, $survey_id, $client_id);
        if (!empty($check_exists_code)) {
            $data['survey_answers'] = $this->Clients_model->get_survey_answers_by_client($id, $survey_id, $client_id);
            if ($data['survey_answers']) {
                $data['active_survey'] = $this->Survey_model->get_current_survey($survey_id);
                $this->load->view("admin/pages/clients/client_survey", $data);
            } else {
                redirect('admin/Survey');
            }
        } else {
            redirect('admin/Survey');
        }
    }

    function submit_survey($id = NULL, $survey_id = NULL, $client_id = NULL) {
        check_param($id);
        check_param($survey_id);
        check_param($client_id);
        $survey_data = $this->Survey_model->get_survey_info($survey_id);
        $client_data = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($survey_data, "resource");
        check_param($client_data, "resource");
        $postData = $this->input->post();
        $client_survey_b_t = $this->Clients_model->get_survey_basic_by_client($id);
        if (!empty($client_survey_b_t)) {
            $this->Global_model->global_update('survey_client_b_t', $client_survey_b_t->id, ['submitted' => 1]);
        }
        foreach ($postData as $key => $val) {
            $survey_answer = $this->Clients_model->get_survey_question_by_client($id, $survey_id, $client_id, $key);
            if ($survey_answer->answer_type != 0) {
                list($one, $two) = explode(',', $val);
                $this->Global_model->global_update('survey_answers', $survey_answer->id, ['answer_value' => $one, 'answer_name' => $two, 'submitted' => 1]);
            } else {
                $this->Global_model->global_update('survey_answers', $survey_answer->id, ['answer_value' => $val, 'answer_name' => $val, 'submitted' => 1]);
            }
        }
        $this->session->set_flashdata("success", lang("data_submitted_successfully"));
        redirect('admin/Clients/show_client_ans/' . $id . '/' . $survey_id . '/' . $client_id);
    }

    function transactions($client_id = NULL) {
        check_param($client_id);
        $client = $this->Global_model->get_data_by_id('clients', $client_id);
        check_param($client, "resource");
        $data['title'] = lang('transactions');
        $data['client_id'] = $client_id;
        $data['active'] = 'transactions';
        $data['client'] = $client;
        $data['transactions'] = $this->Clients_model->transactions($client_id);
        $this->load->view('admin/pages/clients/transactions', $data);
    }

}
