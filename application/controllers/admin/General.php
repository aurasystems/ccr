<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class General extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init(1);
        $this->session->set_userdata("sidebar_active", "");
    }

    function permission_error() {
        $data['title'] = lang('error');
        $data['heading'] = lang('permission_error');
        $data['message'] = lang('no_permission');
        $this->load->view('errors/error', $data);
    }

    function clear() {
        $this->session->sess_destroy();
    }

    function session() {
        echo "<pre>";
        print_r($this->session->userdata());
        echo "</pre>";
    }

    function error($error = NULL) {
        $heading = lang('heading_missing_param');
        $message = lang('error_missing_param');
        if ($error) {
            $heading = lang("heading_$error");
            $message = lang("error_$error");
        }
        $data['title'] = lang('error');
        $data['heading'] = $heading;
        $data['message'] = $message;
        $this->load->view('errors/error', $data);
    }

    function search_document() {
        $this->load->helper('search');
        $dt = ['q' => $this->input->post('r')];
        $result = search_document($dt);
        echo json_encode($result);
    }

    function search_client() {
        $this->load->helper('search');
        $dt = ['q' => $this->input->post('q')];
        $result = search_client($dt);
        echo json_encode($result);
    }

    function search_branch() {
        $company = $this->input->post('company');
        $result = $company ? get_data_list(['c' => 4, 'cm' => $this->input->post('company')]) : [];
        echo json_encode($result);
    }

    function get_client_info() {
        $client_id = $this->input->post('client_id');
        $result = $this->Global_model->get_data_by_id('clients', $client_id);
        echo json_encode($result ? $result : FALSE);
    }

    function get_sub_brand() {
        $sub_brands = get_data_list(['c' => 1, 'p' => $this->input->post('brand')]);
        echo json_encode($sub_brands ? $sub_brands : FALSE);
    }

}
