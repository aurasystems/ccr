<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Equipment_test extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        date_default_timezone_set('Africa/Cairo');
        $this->load->model('admin/Equipment_test_model', 'equipment');
        set_active("booking");
    }

    function index() {
        check_p("equipment_test", "v");
        set_active("equipment_test");
        $data['title'] = lang('current_equip_test');
        $data['tests'] = $this->equipment->get_equipment_tests();
        $this->load->view("admin/pages/equipment_test/index", $data);
    }

    function completed() {
        check_p("equipment_test", "v");
        set_active("com_equipment_test");
        $data['title'] = lang('com_equip_test');
        $data['tests'] = $this->equipment->get_completed_tests();
        $this->load->view("admin/pages/equipment_test/completed", $data);
    }

    function get_tests($id = NULL) {
        check_p("equipment_test", "v");
        set_active("equipment_test");
        check_param($id);
        $one = $this->equipment->get_equipment_test($id);
        check_param($one, "resource");
        if ($one->completed == 0) {
            $result = [];
            $data['title'] = lang('test_p_items');
            $data['id'] = $id;
            $p_items = $this->equipment->get_product_items($id);
            if (!empty($p_items)) {
                for ($i = 0; $i < count($p_items); $i++) {
                    $chk_items = $this->equipment->get_check_type_items($id, $p_items[$i]->p_item_id);
                    if (!in_array($p_items[$i]->p_item_id, $result)) {
                        $result[$p_items[$i]->p_item_id . '-' . $p_items[$i]->p_serial] = $chk_items;
                    }
                }
            }
            $data['result'] = $result;
            $data['one'] = $one;
            $data['now'] = date("Y-m-d H:i:s");
            $this->load->view("admin/pages/equipment_test/items", $data);
        } else {
            redirect(base_url('admin/Equipment_test'));
        }
    }

    function equipment_checked() {
        check_p('equipment_test', "u");
        set_active("equipment_test");
        $post = $this->input->post();
        if (!empty($post['test'])) {
            $chk_items = $this->equipment->get_check_type_items($post['test'], $post['pro_item_id']);
            if (!empty($chk_items)) {
                for ($i = 0; $i < count($chk_items); $i++) {
                    if (isset($chk_items[$i]->id)) {
                        //echo $chk_items[$i]->id;
                        $chkitem = 'chkitem_' . '' . $chk_items[$i]->id;
                        if (isset($post[$chkitem])) {
                            $row = $this->Global_model->get_data_by_id('equipment_test_checks', $chk_items[$i]->id);
                            if ($row->is_checked == 0) {
                                $this->Global_model->global_update('equipment_test_checks', $chk_items[$i]->id, ['is_checked' => $post[$chkitem], 'checked_by' => uid()]);
                            }
                        }
                    }
                }
            }
            // Check for all is checked to mark test is completed
            $items_not_checked = $this->equipment->check_type_items_checked($post['test']);
            if ($items_not_checked == 0) {
                // Update test is completed
                $this->Global_model->global_update('equipment_test', $post['test'], ['completed' => 1]);
            }
            $this->session->set_flashdata("success", lang("data_submitted_successfully"));
            redirect(base_url('admin/Equipment_test/get_tests/' . $post['test']));
        } else {
            redirect(base_url('admin/Equipment_test'));
        }
    }

    function completed_tests($id = NULL) {
        check_p("equipment_test", "v");
        set_active("com_equipment_test");
        check_param($id);
        $one = $this->equipment->get_equipment_test($id);
        check_param($one, "resource");
        if ($one->completed == 1) {
            $result = [];
            $data['title'] = lang('com_equip_test');
            $data['id'] = $id;
            $p_items = $this->equipment->get_product_items($id);
            if (!empty($p_items)) {
                for ($i = 0; $i < count($p_items); $i++) {
                    $chk_items = $this->equipment->get_check_type_items($id, $p_items[$i]->p_item_id);
                    if (!in_array($p_items[$i]->p_item_id, $result)) {
                        $result[$p_items[$i]->p_item_id . '-' . $p_items[$i]->p_serial] = $chk_items;
                    }
                }
            }
            $data['result'] = $result;
            $data['one'] = $one;
            $data['now'] = date("Y-m-d H:i:s");
            $this->load->view("admin/pages/equipment_test/completed_items", $data);
        } else {
            redirect(base_url('admin/Equipment_test/completed'));
        }
    }

}
