<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "/third_party/spout/src/Spout/Autoloader/autoload.php";

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class Import extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        date_default_timezone_set('Africa/Cairo');
        $this->load->model('admin/Import_model');
        set_active("import");
    }

    function index() {
        check_p("import", "v");
        $data['title'] = lang('attendance');
        $data['attendance'] = $this->Import_model->get_data();
        $this->load->view("admin/pages/import/index", $data);
    }

    function import() {
        check_p("import", "c");
        set_active("import_data");
        $data = ['title' => lang('attendance_import')];
        $data['attributes'] = ['id' => 'import_form', 'class' => 'form-horizontal', 'role' => 'form'];
        $this->load->view("admin/pages/import/import", $data);
    }

    function import_data() {
        check_p("import", "c");
        if ($_FILES['userfile']['name'] == '') {
            $this->session->set_flashdata('warning', lang('select_file'));
            redirect('admin/Import/import');
        } else {
            // Average days to pay report
            $path = 'uploads/db/';
            require_once APPPATH . "/third_party/Classes/PHPExcel.php";
            require_once APPPATH . "/third_party/Classes/ChunkReadFilter.php";
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'xlsx|xls';
            $config['remove_spaces'] = TRUE;
            $config['overwrite'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload()) {
                $data = ['title' => lang('attendance_import')];
                $data['attributes'] = ['class' => 'form-horizontal', 'class' => 'form-horizontal', 'role' => 'form'];
                $data['error'] = $this->upload->display_errors();
                $this->load->view("admin/pages/import/import", $data);
            } else {
                $data = array('upload_data' => $this->upload->data());
            }
            if (!empty($data['upload_data']['file_name'])) {
                $import_xls_file = $data['upload_data']['file_name'];
            } else {
                $import_xls_file = 0;
            }
            $inputFileName = $path . $import_xls_file;
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                /**  Create a new Reader of the type defined in $inputFileType  * */
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objReader->setReadDataOnly(false);
                $objPHPExcel = $objReader->load($inputFileName);
                $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(NULL, TRUE, TRUE, FALSE);
                $data = array_map('array_filter', $allDataInSheet);
                if (!empty($data[0])) {
                    unset($allDataInSheet[0]);
                    $avgs = [];
                    $inserted_id = $this->Global_model->global_insert('process_code', ['timestamp' => date('Y-m-d H:i:s')]);
                    for ($i = 1; $i <= count($allDataInSheet); $i++) {
                        // check for attendance id in users
                        $one = $this->Import_model->get_user_by_att_id($allDataInSheet[$i][0]);
                        if ($one) {
                            $dt = [];
                            $dt['user_id'] = $one->id;
                            $dt['type'] = ($allDataInSheet[$i][1] == 'checkin') ? 1 : 2;
                            $dt['attend_date'] = date("Y-m-d H:i:s", strtotime($allDataInSheet[$i][2]));
                            $dt['process_id'] = $inserted_id;
                            $insertdata[] = $dt;
                        }
                    }
                    if (!empty($insertdata)) {
                        $this->db->insert_batch('attendance', $insertdata);
                        $this->session->set_flashdata('msg', lang('data_submitted_successfully'));
                    }
                } else {
                    $this->session->set_flashdata('warning', lang('no_data'));
                    redirect('admin/Import/import');
                }
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' . $e->getMessage());
            }
            redirect('admin/Import/import');
        }
    }

}
