<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contact extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
    }

    function index() {
        $data['title'] = lang('contact_us');
        $data['settings'] = settings('all', 3);
        $data['active'] = 'contact';
        $this->load->view("front/planet/pages/contact", $data);
    }

    function send() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', lang("name"), 'trim|required');
        $this->form_validation->set_rules('email', lang("email"), 'trim|required');
        $this->form_validation->set_rules('message', lang("msg"), 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $settings = settings('all', 3);
            if (!empty($settings)) {
                $to = $settings->email;
                $from = $this->input->post('email');
                $msg = $this->input->post('message');
                $name = $this->input->post('name');
            }
            $data = [
                'from' => $from,
                'to' => $to,
                'subject' => lang("new_msg").' ( '.$name.' ) ',
                'message' => $msg
            ];
            guest_email($data);
            $this->session->set_flashdata("success", lang("msg_sent"));
            redirect(base_url('Contact'));
        }
    }

}
