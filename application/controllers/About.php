<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class About extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
        $this->load->model('admin/About_model');
    }

    function index() {
        $data['title'] = lang('about_us');
        $data['about'] = $this->About_model->get_content();
        $data['active'] = 'about';
        $this->load->view("front/planet/pages/about", $data);
    }

}
