<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shop extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
        $this->load->model('Shop_model');
        $this->load->library('user_agent');
        $this->load->library('pagination');
    }

    function index() {
        $settings = settings('all', 3);
        $data['title'] = lang('shop');
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $config = array();
        $config["base_url"] = base_url() . "Shop/index";
        $config["total_rows"] = $this->Shop_model->count_products();
        $config['cur_tag_open'] = '<a href="" class="active">';
        $config['cur_tag_close'] = '</a>';
        $config["per_page"] = 12;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $data["products"] = $this->Shop_model->get_products_data($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['cats'] = build_categories_menu(0);
        $data['brands'] = get_brands();
        $data['active'] = 'shop';
        $this->load->view("front/planet/pages/shop", $data);
    }

    function product($category_id = NULL) {
        $data['title'] = lang('Product_list');
        $data['cats'] = build_categories_menu(0);
        $data['brands'] = get_brands();
        $data['all_count'] = brand_prod_count();
        $data['items'] = get_cat_product($category_id, false);
        $data['categ'] = $this->category_menu();
        $data['active'] = 'shop';
        $this->load->view("front/planet/pages/product_items", $data);
    }

    function brand($brand_id = NULL) {
        $data['title'] = lang('brand_list');
        $data['cats'] = build_categories_menu(0);
        $data['brands'] = get_brands();
        $data['all_count'] = brand_prod_count();
        $data['brand'] = get_table_info('brands', $brand_id);
        $data['items'] = get_cat_product($brand_id, true);
        $data['categ'] = $this->category_menu();
        $data['active'] = 'shop';
        $this->load->view("front/planet/pages/product_items", $data);
    }

    public function category_menu() {
        // Select all entries from the menu table
        $query = $this->db->query("select * from categories order by parent");
        // Create a multidimensional array to contain a list of items and parents
        $cat = array(
            'items' => array(),
            'parents' => array()
        );
        // Builds the array lists with data from the menu table
        foreach ($query->result() as $cats) {
            // Creates entry into items array with current menu item id ie. $menu['items'][1]
            $cat['items'][$cats->id] = $cats;
            // Creates entry into parents array. Parents array contains a list of all items with children
            $cat['parents'][$cats->parent][] = $cats->id;
        }
        if ($cat) {
            $result = $this->build_category_menu(0, $cat);
            return $result;
        } else {
            return FALSE;
        }
    }

    // Menu builder function, parentId 0 is the root
    function build_category_menu($parent, $menu) {
        $html = "";
        $pointer_events = '';
        if (isset($menu['parents'][$parent])) {
            $html .= "<ul style='padding:0px;'>";
            foreach ($menu['parents'][$parent] as $itemId) {
                $categ_products = get_cat_product($menu['items'][$itemId]->id, false);
                if (empty($categ_products))
                    $pointer_events = "none";
                if (!isset($menu['parents'][$itemId])) {
                    $items = get_brands($menu['items'][$itemId]->id);
                    if (!empty($items)) {
                        $html .= "<li class='menu-item menu-item-has-children'><a style='pointer-events:" . $pointer_events . "' href='" . base_url() . "Shop/product/" . $menu['items'][$itemId]->id . "'>" . $menu['items'][$itemId]->{'n_' . lc()} . "</a>";
                        $html .= "<ul>";
                        foreach ($items as $item) {
                            $html .= "<li ><a class='parent' href='" . base_url() . "Shop/brand/" . $item->id . "'>" . $item->{'n_' . lc()} . "</a></li>";
                        }
                        $html .= "</ul>
                            </li>";
                    } else {
                        $html .= "<li class='menu-item menu-item-has-children'><a style='pointer-events:" . $pointer_events . "' href='" . base_url() . "Shop/product/" . $menu['items'][$itemId]->id . "'>" . $menu['items'][$itemId]->{'n_' . lc()} . "</a></li>";
                    }
                }
                if (isset($menu['parents'][$itemId])) {
                    $html .= " <li class='menu-item menu-item-has-children'><a style='pointer-events:" . $pointer_events . "' class='parent' href='" . base_url() . "Shop/product/" . $menu['items'][$itemId]->id . "'>" . $menu['items'][$itemId]->{'n_' . lc()} . "</a>";
                    $html .= $this->build_category_menu($itemId, $menu);
                    $html .= "</li>";
                }
            }
            $html .= "</ul>";
        }
        return $html;
    }

    function item_details($product_id = NULL) {
        if (!empty($product_id)) {
            $product = $this->Global_model->get_data_by_id('products', $product_id);
            if (!empty($product)) {
                $item = get_record('id', $product_id, 'products');
                $data['title'] = lang('item_details');
                $data['product'] = $product;
                $data['cat_n'] = get_cat_name($product_id);
                $data['rev_count'] = get_reviews_total($product_id);
                $data['related_prods'] = get_related_products($product_id);
                $data['reviews'] = get_reviews($product_id);
                $data['active'] = 'shop';
                $this->load->view("front/planet/pages/item_details", $data);
            } else {
                redirect(base_url('Home'));
            }
        } else {
            redirect(base_url('Home'));
        }
    }

    function check_availabilty($product_id = NULL) {
        if (!empty(cid())) {
            if (!empty($product_id)) {
                $product = $this->Global_model->get_data_by_id('products', $product_id);
                if (!empty($product)) {
                    $today = date('Y-m-d');
                    $data['reserve'] = false;
                    $this->form_validation->set_rules("quantity", lang("quantity"), "trim|required|numeric");
                    if (!empty($product) && $product->rent_term == 1) {
                        // day
                        $this->form_validation->set_rules("b_from", lang("b_from"), "trim|required");
                        $this->form_validation->set_rules("b_to", lang("b_to"), "trim|required");
                    } else if (!empty($product) && $product->rent_term == 2) {
                        // hour
                        $this->form_validation->set_rules("date", lang("date"), "trim|required");
                        $this->form_validation->set_rules("b_time_from", lang("b_time_from"), "trim|required");
                        $this->form_validation->set_rules("b_time_to", lang("b_time_to"), "trim|required");
                    }
                    if (!$this->form_validation->run()) {
                        $data['title'] = lang('add_to_cart');
                        $data['reserve'] = false;
                        $data['product'] = $product;
                        $data['active'] = 'shop';
                        $this->load->view("front/planet/pages/booking", $data);
                    } else {
                        $quantity = $this->input->post('quantity');
                        if (!empty($product) && $product->rent_term == 1) {
                            // day
                            if ($this->input->post('b_from') < $today) {
                                $this->session->set_flashdata("error", lang("no_old_dates_allowed"));
                                redirect(base_url('Shop/check_availabilty/' . $product_id));
                            }
                            if ($this->input->post('b_to') < $this->input->post('b_from')) {
                                $this->session->set_flashdata("error", lang("check_dates"));
                                redirect(base_url('Shop/check_availabilty/' . $product_id));
                            }
                        }
                        if (!empty($product) && $product->rent_term == 2) {
                            // hour
                            if ($this->input->post('date') < $today) {
                                $this->session->set_flashdata("error", lang("no_old_dates_allowed"));
                                redirect(base_url('Shop/check_availabilty/' . $product_id));
                            }
                            if ($this->input->post('b_time_to') <= $this->input->post('b_time_from')) {
                                $this->session->set_flashdata("error", lang("check_times"));
                                redirect(base_url('Shop/check_availabilty/' . $product_id));
                            }
                        }
                        if (!empty($product) && $product->rent_term == 1) {
                            $b_from = date('Y-m-d H:i:s', strtotime($this->input->post('b_from')));
                            $b_to = date('Y-m-d H:i:s', strtotime($this->input->post('b_to')));
                        } elseif (!empty($product) && $product->rent_term == 2) {
                            $time_from = date('H:i:s', strtotime($this->input->post('b_time_from')));
                            $time_to = date('H:i:s', strtotime($this->input->post('b_time_to')));
                            $b_from = $this->input->post('date') . ' ' . $time_from;
                            $b_to = $this->input->post('date') . ' ' . $time_to;
                        }
                        $cart_quantity = $this->Shop_model->get_cart_quantity($product_id, $b_from, $b_to);
                        $total_qty = $quantity + $cart_quantity;
                        $available = checkAvailability('', $product_id, $total_qty, $b_from, $b_to);
                        if ($available['flag']) {
                            // $book_id = $this->Shop_model->check_booking($b_from, $b_to);
                            $cart_root = $this->Shop_model->get_cart_root();
                            if (!empty($cart_root)) {
                                $dt = array(
                                    'product_id' => $product_id,
                                    'quantity' => $quantity,
                                    'b_from' => $b_from,
                                    'b_to' => $b_to,
                                    'total_cost' => $quantity * $product->rent_price,
                                    'parent' => $cart_root->id,
                                    'added' => date('Y-m-d H:i:s'),
                                );
                                $this->Global_model->global_insert('cart', $dt);
                            } else {
                                $dt = array(
                                    'client_id' => cid(),
                                    'parent' => 0,
                                    'added' => date('Y-m-d H:i:s'),
                                );
                                $parent = $this->Global_model->global_insert('cart', $dt);
                                $dt = array(
                                    'product_id' => $product_id,
                                    'quantity' => $quantity,
                                    'b_from' => $b_from,
                                    'b_to' => $b_to,
                                    'total_cost' => $quantity * $product->rent_price,
                                    'parent' => $parent,
                                    'added' => date('Y-m-d H:i:s'),
                                );
                                $this->Global_model->global_insert('cart', $dt);
                            }
                            $total = $this->Shop_model->get_total_price();
                            // echo $total;
                            $this->session->set_flashdata("success", $available['msg']);
                            redirect(base_url('Shop/check_availabilty/' . $product_id));
                        } else if (!$available['flag']) {
                            $this->session->set_flashdata("error", $available['msg']);
                            redirect(base_url('Shop/waiting_list/' . $product_id));
                        }
                    }
                } else {
                    redirect(base_url('Home'));
                }
            } else {
                redirect(base_url('Home'));
            }
        } else {
            redirect(base_url('Home'));
        }
    }

    function waiting_list($product_id = NULL) {
        if (!empty(cid())) {
            if (!empty($product_id)) {
                $product = $this->Global_model->get_data_by_id('products', $product_id);
                if (!empty($product)) {
                    $time_from = date('H:i:s', strtotime($this->input->post('b_time_from')));
                    $time_to = date('H:i:s', strtotime($this->input->post('b_time_to')));
                    $b_from = $this->input->post('date') . ' ' . $time_from;
                    $b_to = $this->input->post('date') . ' ' . $time_to;
                    $data['title'] = lang('add_to_waiting_list');
                    $data['product'] = $product;
                    $data['reserve'] = true;
                    $data['product_id'] = $product_id;
                    $data['rent_term'] = $product->rent_term;
                    $data['active'] = 'shop';
                    $this->load->view("front/planet/pages/booking", $data);
                } else {
                    redirect(base_url('Home'));
                }
            } else {
                redirect(base_url('Home'));
            }
        } else {
            redirect(base_url('Home'));
        }
    }

    function add_waiting_list($product_id = null, $rent_term = null) {
        if ($rent_term == 1) {
            $b_from = date('Y-m-d H:i:s', strtotime($this->input->post('b_from')));
            $b_to = date('Y-m-d H:i:s', strtotime($this->input->post('b_to')));
        } elseif ($rent_term == 2) {
            $time_from = date('H:i:s', strtotime($this->input->post('b_time_from')));
            $time_to = date('H:i:s', strtotime($this->input->post('b_time_to')));
            $b_from = $this->input->post('date') . ' ' . $time_from;
            $b_to = $this->input->post('date') . ' ' . $time_to;
        }
        // Check if added before
        $added = $this->Shop_model->check_waiting_list(cid(), $product_id, $b_from, $b_to);
        if ($added) {
            $this->session->set_flashdata("error", lang('added_to_wl'));
        } else {
            $data = array(
                'client_id' => cid(),
                'product_id' => $product_id,
                'b_from' => $b_from,
                'b_to' => $b_to,
            );
            $res = $this->Global_model->global_insert('booking_waiting_list', $data);
            $this->session->set_flashdata("success", lang('added_to_waiting_list'));
        }
        redirect(base_url('Shop/check_availabilty/' . $product_id));
    }

    function add_review($product_id) {
        $this->form_validation->set_rules("message", lang("message"), "trim|required");
        if (!$this->form_validation->run()) {
            redirect(base_url('Shop/item_details/' . $product_id));
        } else {
            $data['description'] = $this->input->post('message');
            //$data['rating'] = $this->input->post('rating');
            $data['product_id'] = $product_id;
            $data['client_id'] = cid();
            $this->Global_model->global_insert('reviews', $data);
            $this->session->set_flashdata("success", lang('review_added'));
            redirect(base_url('Shop/item_details/' . $product_id));
        }
    }

    function add_rating() {
        $product_id = $this->input->post('product_id');
        $rating = $this->input->post('rating');
        $this->Shop_model->update_rating($product_id, $rating);
    }

}
