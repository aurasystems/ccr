<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
        $this->load->model('Search_model');
    }

    function index($query = NULL) {
        $search_wiz = (!empty($this->input->post('category'))) ? $this->input->post('category') : NULL;
        $text = $this->input->post('keyword');
        $query = !empty($text) ? $text : $query;
        if ($query) {
            $this->load->model('Search_model');
            $results = $this->Search_model->search_products_by($search_wiz, $text);
            if ($results) {
                shuffle($results);
            }
            $data = [
                'title' => lang('search_results'),
                'keyword' => $query,
                'items' => $results
            ];
            $this->load->view('front/planet/pages/search', $data);
        } else {
            redirect(base_url("Home"));
        }
    }

    function search_products() {
        $text = $this->input->post('term');
        $search_wiz = (!empty($this->input->post('search_wiz'))) ? $this->input->post('search_wiz') : NULL;
        $result = $this->Search_model->search_products_by($search_wiz, $text);
        echo json_encode($result);
    }

}
