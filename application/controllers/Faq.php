<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Faq extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
    }

    function index() {
        $data['title'] = lang('faq');
        $this->load->view("front/pages/faq", $data);
    }

}
