<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Our_Services extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
    }

    public function index()
    {
        $data['title'] = lang('Our_Services');
        $this->load->view("front/pages/our_services", $data);
    }



}
