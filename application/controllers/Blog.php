<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Blog extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
        $this->load->model('Blog_model');
        $this->load->helper("url");
        $this->load->library('pagination');
    }

    function index() {
        $settings = settings('all', 3);
        if (!empty($settings) && $settings->show_blog) {
            $data['title'] = lang('blogs');
            $data['tags'] = $this->Blog_model->get_all_tags();
            $categories = $this->Blog_model->get_categories();
            if (!empty($categories)) {
                foreach ($categories as $cat) {
                    $blogs = $this->Blog_model->count_blog_by_category($cat->id);
                    if (!empty($blogs)) {
                        $cat->count = $blogs;
                    } else {
                        $cat->count = 0;
                    }
                }
            }
            $data['categories'] = $categories;
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $config = array();
            $config["base_url"] = base_url() . "Blog/index";
            $config["total_rows"] = $this->Blog_model->count_blogs();
            $config['cur_tag_open'] = '<a href="" class="active">';
            $config['cur_tag_close'] = '</a>';
            $config["per_page"] = 3;
            $config["uri_segment"] = 3;
            $this->pagination->initialize($config);
            $data["blogs"] = $this->Blog_model->get_data('', $config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();
            $data['cats'] = $this->build_categories_menu(0);
            $data['active'] = 'blog';
            $this->load->view("front/planet/pages/blogs", $data);
        } else {
            redirect(base_url('Home'));
        }
    }

    function get_details($blog_id = NULL) {
        $settings = settings('all', 3);
        if (!empty($settings) && $settings->show_blog) {
            $data['title'] = lang('blog_details');
            $data['blog_id'] = $blog_id;
            $data['blog'] = $this->Blog_model->get_info($blog_id);
            $data['tags'] = $this->Blog_model->get_tags($blog_id);
            $data['count_comments'] = $this->Blog_model->count_comments($blog_id);
            $data['comments'] = $this->Blog_model->get_comments($blog_id);
            $categories = $this->Blog_model->get_categories();
            if (!empty($categories)) {
                foreach ($categories as $cat) {
                    $blogs = $this->Blog_model->count_blog_by_category($cat->id);
                    if (!empty($blogs)) {
                        $cat->count = $blogs;
                    } else {
                        $cat->count = 0;
                    }
                }
            }
            $data['categories'] = $categories;
            $data['active'] = 'blog';
            $this->load->view("front/planet/pages/blog_details", $data);
        } else {
            redirect(base_url('Home'));
        }
    }

    function get_blog_by_category($category_id = NULL) {
        $settings = settings('all', 3);
        if (!empty($settings) && $settings->show_blog) {
            $data['title'] = lang('blog_by_cat');
            $data['category_id'] = $category_id;
            $data['category'] = $this->Global_model->get_data_by_id('blog_categories', $category_id);
            //$data['blogs'] = $this->Blog_model->get_data($category_id);
            $data['tags'] = $this->Blog_model->get_all_tags();
            $categories = $this->Blog_model->get_categories();
            if (!empty($categories)) {
                foreach ($categories as $cat) {
                    $blogs = $this->Blog_model->count_blog_by_category($cat->id);
                    if (!empty($blogs)) {
                        $cat->count = $blogs;
                    } else {
                        $cat->count = 0;
                    }
                }
            }
            $data['categories'] = $categories;
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $config = array();
            $config["base_url"] = base_url() . "Blog/get_blog_by_category/$category_id";
            $config["total_rows"] = $this->Blog_model->count_blog_by_category($category_id);
            $config['cur_tag_open'] = '<a href="" class="active">';
            $config['cur_tag_close'] = '</a>';
            $config["per_page"] = 3;
            $config["uri_segment"] = 4;
            $this->pagination->initialize($config);
            $data["blogs"] = $this->Blog_model->get_data($category_id, $config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();
            $data['active'] = 'blog';
            $this->load->view("front/planet/pages/blog_by_category", $data);
        } else {
            redirect(base_url('Home'));
        }
    }

    function get_blog_by_tag($tag = NULL) {
        $settings = settings('all', 3);
        if (!empty($settings) && $settings->show_blog) {
            $data['title'] = lang('blog_by_tag');
            $data['tag'] = $tag;
            //$data['blogs'] = $this->Blog_model->get_blog_by_tag($tag);
            $data['tags'] = $this->Blog_model->get_all_tags();
            $categories = $this->Blog_model->get_categories();
            if (!empty($categories)) {
                foreach ($categories as $cat) {
                    $blogs = $this->Blog_model->count_blog_by_category($cat->id);
                    if (!empty($blogs)) {
                        $cat->count = $blogs;
                    } else {
                        $cat->count = 0;
                    }
                }
            }
            $data['categories'] = $categories;
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $config = array();
            $config["base_url"] = base_url() . "Blog/get_blog_by_tag/$tag";
            $config["total_rows"] = $this->Blog_model->count_blog_by_tag($tag);
            $config['cur_tag_open'] = '<a href="" class="active">';
            $config['cur_tag_close'] = '</a>';
            $config["per_page"] = 3;
            $config["uri_segment"] = 4;
            $this->pagination->initialize($config);
            $data["blogs"] = $this->Blog_model->get_blog_by_tag($tag, $config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();
            $data['active'] = 'blog';
            $this->load->view("front/planet/pages/blog_by_tag", $data);
        } else {
            redirect(base_url('Home'));
        }
    }

    function add_comment($blog_id) {
        if (!empty(cid())) {
            if (!empty($blog_id)) {
                $blog = $this->Global_model->get_data_by_id('blog', $blog_id);
                if (!empty($blog)) {
                    $this->load->library('form_validation');
                    $this->form_validation->set_rules('comment', lang("password"), 'trim|required');
                    if (!$this->form_validation->run()) {
                        $this->get_details($blog_id);
                    } else {
                        $data = [
                            'blog_id' => $blog_id,
                            'cid' => cid(),
                            'comment' => $this->input->post('comment')
                        ];
                        $res = $this->Global_model->global_insert('blog_comments', $data);
                        if ($res) {
                            $this->session->set_flashdata('success', lang("comm_submitted_successfully"));
                            redirect(base_url('Blog/get_details/' . $blog_id));
                        } else {
                            $this->session->set_flashdata('error', lang("error_occured"));
                            redirect(base_url('Blog/get_details/' . $blog_id));
                        }
                    }
                }
            }
        }
    }

    function build_categories_menu($parent) {
        $query = $this->db->select('*')->from('categories')->where('parent', $parent)->where('deleted !=', 1)->get()->result();
        $html = "";
        if (!empty($query)) {
            $html .= "<ul>";
            foreach ($query as $itemId) {
                $sub_child = $this->db->select('*')->from('categories')->where('parent', $itemId->id)->where('deleted !=', 1)->get()->result();
                if (empty($sub_child)) {
                    $html .= "<li><a href='" . base_url() . "Shop/product/" . $itemId->id . "'>" . $itemId->{'n_' . lc()} . "</a></li>";
                } else {
                    $html .= " <li class='parent'>"
                            . "<a href='#'>" . $itemId->{'n_' . lc()} . "</a>"
                            . "<ul class='customeField'>"
                            . $this->build_categories_menu($itemId->id)
                            . "</ul>";
                }
            }
            $html .= "</ul>";
        }
        return $html;
    }

}
