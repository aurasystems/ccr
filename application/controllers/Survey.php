<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Survey extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
        $this->load->model('admin/Survey_model');
        $this->load->model('admin/Settings_model');
    }

    function index() {
        
    }

    function get_survey($survey_id = NULL, $code = NULL) {
        check_param($survey_id);
        check_param($code);
        $survey_data = $this->Survey_model->get_survey_info($survey_id);
        check_param($survey_data, "resource");
        $data = ['title' => lang('answers')];
        $data['attributes'] = ['class' => 'form-horizontal', 'role' => 'form'];
        $data['survey_id'] = $survey_id;
        $data['code'] = $code;
        $check_exists_code = $this->Survey_model->get_client_survey_basic($survey_id, $code);
        if (!empty($check_exists_code)) {
            $data['survey_answers'] = $this->Survey_model->get_survey_answers($survey_id, $code);
            if (!empty($data['survey_answers'])) {
                $data['active_survey'] = $this->Survey_model->get_current_survey($survey_id);
                $this->load->view("admin/pages/surveys/survey", $data);
            } else {
                redirect('Survey/survey_submitted');
            }
        } else {
            redirect('Survey/survey_submitted');
        }
    }

    function submit_survey($survey_id = NULL, $code = NULL) {
        check_param($survey_id);
        $survey_data = $this->Survey_model->get_survey_info($survey_id);
        check_param($survey_data, "resource");
        $postData = $this->input->post();
        $client_survey_b_t = $this->Survey_model->get_client_survey_b_t($survey_id, $code);
        if ($client_survey_b_t) {
            $this->Global_model->global_update('survey_client_b_t', $client_survey_b_t->id, ['submitted' => 1]);
        }
        foreach ($postData as $key => $val) {
            $survey_answer = $this->Survey_model->get_survey_question_by_code($survey_id, $code, $key);
            if ($survey_answer->answer_type != 0) {
                list($one, $two) = explode(',', $val);
                $this->Global_model->global_update('survey_answers', $survey_answer->id, ['answer_value' => $one, 'answer_name' => $two, 'submitted' => 1]);
            } else {
                $this->Global_model->global_update('survey_answers', $survey_answer->id, ['answer_value' => $val, 'answer_name' => $val, 'submitted' => 1]);
            }
        }
        redirect('Survey/survey_submitted');
    }

    function survey_submitted() {
        $data = ['title' => lang('survey_done')];
        $data['submitted_survey'] = lang('submitted_survey');
        $this->load->view("admin/pages/surveys/survey_submitted", $data);
    }

}
