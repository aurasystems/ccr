<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Privacy extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
        $this->load->model('admin/Privacy_model');
    }

    function index() {
        $settings = settings('all', 3);
        if (!empty($settings) && $settings->show_privacy) {
            $data['title'] = lang('privacy_policy');
            $data['privacy'] = $this->Privacy_model->get_content();
            $this->load->view("front/planet/pages/privacy", $data);
        } else {
            redirect(base_url('Home'));
        }
    }

}
