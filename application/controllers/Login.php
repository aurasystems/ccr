<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
        $this->load->model('Login_model');
    }

    function index() {
        if ((cid())) {
            redirect('Home');
        } else {
            $data['title'] = lang('login');
            $this->load->view("front/planet/pages/login", $data);
        }
    }

    function do_login() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $email = $this->input->post('email');
            $password = do_hash($this->input->post('password'), 'md5');
            $check_login_result = $this->Login_model->check_login($email, $password);
            if ($check_login_result) {
                redirect('Home');
            } else {
                $this->session->set_flashdata('error', lang('wrong_email_pass'));
                redirect('Login');
            }
        }
    }

    function reset_password() {
        if ((cid() != "")) {
            redirect('Home');
        } else {
            $data = ['title' => lang('reset_password')];
            $this->load->view("front/planet/pages/forget_password", $data);
        }
    }

    function do_reset_password() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        if ($this->form_validation->run() == FALSE) {
            $this->reset_password();
        } else {
            $email = $this->input->post('email');
            $check_user = $this->Global_model->check_email('clients', $email);
            if ($check_user) {
                // client is exists
                send_reset_link($email);
                redirect('Login/password_reset_success');
            } else {
                // user not exists
                $this->session->set_flashdata('error', lang('email_not_found'));
                $this->reset_password();
            }
        }
    }

    function password_reset_success() {
        if ((cid() != "")) {
            redirect('Home');
        } else {
            $data['title'] = lang('password_recover_success');
            $this->load->view("front/planet/pages/password_recover_success", $data);
        }
    }

    function reset_my_password($token) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if (!$this->form_validation->run()) {
            $data = ['title' => lang('reset_password'), "token" => $token];
            $this->load->view("front/planet/pages/reset_my_password", $data);
        } else {
            //verify token
            $token = explode("_", $token);
            $id = $token[0];
            $user = $this->Global_model->get_data_by_id("clients", $id);
            if ($user && substr($user->password, 0, 5) == $token[1]) {
                $data = ["password" => md5($this->input->post("password"))];
                $this->Global_model->global_update("clients", $id, $data);
                $this->session->set_flashdata('success', lang('pass_reset'));
                redirect('Login');
            } else {
                // user not exists
                $this->session->set_flashdata('error', lang('invalid_link'));
                redirect('Login/reset_password');
            }
        }
    }

}
