<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cart extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
        $this->load->model('Shop_model');
        $this->load->library('user_agent');
        $this->load->library('pagination');
    }

    function index() {
        $data['title'] = lang('Shop_cart');
        $data['items'] = get_cart();
        $data['active'] = 'cart';
        $this->load->view("front/planet/pages/cart", $data);
    }

    function delete_cart($cart_id, $product_id) {
        if ($cart_id != 'null')
            delete_cart($cart_id);
        else
            delete_cart();
        redirect($this->agent->referrer());
    }

    function show_cart() {
        $dt['items'] = get_cart();
        echo json_encode($dt);
    }

    function increment() {
        $cart_id = $this->input->post('cart_id');
        $product_id = $this->input->post('product_id');
        $count = get_quantity(cid(), $cart_id);
        $cart_data = $this->Global_model->get_data_by_id('cart', $cart_id);
        //$items_data = $this->Shop_model->get_items_data($cart_data->id, $product_id);
        $is_available = checkAvailability(null, $product_id, $count + 1, $cart_data->b_from, $cart_data->b_to);
        //print_r($is_available);
        if (!$is_available['flag']) {
            $sub_total = get_total_count(cid());
            $dataArray = array(
                'count' => $count,
                'sub_total' => $sub_total,
                'not_ava' => true,
                'msg' => $is_available['msg'],
            );
        } else if ($is_available['flag']) {
            $count++;
            $this->Shop_model->inc_dec_quantity($cart_id, $count);
            $sub_total = get_total_count(cid());
            $dataArray = array(
                'count' => $count,
                'sub_total' => $sub_total,
                'not_ava' => false,
            );
        }
        echo json_encode($dataArray);
    }

    function decrement() {
        $cart_id = $this->input->post('cart_id');
        $product_id = $this->input->post('product_id');
        $count = get_quantity(cid(), $cart_id);
        $count--;
        if ($count == 0)
            return;
        $this->Shop_model->inc_dec_quantity($cart_id, $count, true);
        $sub_total = get_total_count(cid());
        $dataArray = array(
            'count' => $count,
            'sub_total' => $sub_total
        );
        echo json_encode($dataArray);
    }

}
