<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Register extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front(TRUE);
        $this->load->model('Register_model');
        $this->load->model('Global_model');
    }

    function index() {
        if ((cid())) {
            redirect('Home');
        } else {
            $data['title'] = lang('register');
            $this->load->view("front/pages/register", $data);
        }
    }

//business name field
    function validation() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules("n_en", lang("name_en"), "trim|required");
        $this->form_validation->set_rules("n_ar", lang("name_ar"), "trim|required");
        $this->form_validation->set_rules("phone", lang("phone"), "trim|required");
        $this->form_validation->set_rules('email', lang("email"), 'required|is_unique[clients.email]|valid_email');
        $this->form_validation->set_rules("national_id", lang("national_id"), "trim|required|min_length[14]|max_length[14]|is_unique[clients.national_id]");
        $this->form_validation->set_rules('password', lang("password"), 'trim|required|matches[confirm_password]|min_length[4]|max_length[32]');
        $this->form_validation->set_rules('confirm_password', lang("confirm_password"), 'trim|matches[password]');
        $this->form_validation->set_rules('gender', lang('gender'), 'required');
        $this->form_validation->set_rules('type', lang('type'), 'required');
        $this->form_validation->set_rules('term_cond', lang('term_cond'), 'required');
        if ($this->form_validation->run()) {
            $data = $this->input->post();
            if ($data['gender'] == '') {
                $this->session->set_flashdata('error', lang("specify_gender"));
                redirect(base_url('Register'));
            }
            if ($data['type'] == '') {
                $this->session->set_flashdata('error', lang("specify_account"));
                redirect(base_url('Register'));
            }
            if ($data['term_cond'] == 0) {
                $this->session->set_flashdata('error', lang("term_cond_req"));
                redirect(base_url('Register'));
            }
            if ($data['type'] == 1 && $data['corp_n'] == '') {
                $this->session->set_flashdata('error', lang("specify_corp_name"));
                redirect(base_url('Register'));
            }
            $data = [
                'n_en' => $this->input->post('n_en'),
                'n_ar' => $this->input->post('n_ar'),
                'phone' => $this->input->post('phone'),
                'national_id' => $this->input->post('national_id'),
                'email' => $this->input->post('email'),
                'address_en' => $this->input->post('address_en'),
                'address_ar' => $this->input->post('address_ar'),
                'password' => do_hash($this->input->post('password'), 'md5'),
                'gender' => $this->input->post('gender'),
                'type' => $this->input->post('type'),
                'corp_n' => ($this->input->post('type') == 1) ? $this->input->post('corp_n') : NULL,
            ];
            $res = $this->Global_model->global_insert('clients', $data);
            if (!$res) {
                $this->session->set_flashdata('error', lang("unable_to_reg"));
                redirect(base_url('Register'));
            } else if ($res) {
                $msg = lang('reg_confirm');
                $client = $this->Global_model->get_data_by_id('clients', $res);
                $email_data = [
                    'to' => $client->email,
                    'subject' => 'Account Confirmation',
                    'message' => $msg
                ];
                send_email($email_data);
                $this->session->set_flashdata('success', lang("reg_success"));
                redirect(base_url('Register'));
            }
        } else {
            $this->index();
        }
    }
}
