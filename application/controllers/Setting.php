<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Setting extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front();
        $this->load->model('Crud_model');
    }

    public function index() {

        if ((uid())) {
            redirect('Setting');
        } else {
            $data['title'] = lang('lang_sign_in');
            $this->load->view("front/pages/login", $data);
        }
    }

    function update() {
        if ((uid() != "")) {
            $this->form_validation->set_rules('address_en', 'add_en');
            $this->form_validation->set_rules('address_ar', 'add_ar');
            $this->form_validation->set_rules('password', 'Password');
            $this->form_validation->set_rules('confPassword', 'confirmation', 'required|matches[password]');
            if ($this->form_validation->run()) {
                $data = $this->input->post();
                $dt = [];
                foreach ($data as $k => $v):
                    $dt[$k] = $v;
                endforeach;
                unset($dt['confPassword']);
                $dt['password'] = md5('password');

                $res = $this->Global_model->global_update('clients', $uid, $dt);
            } else
                redirect(base_url() . 'profile');
        } else {
            $data['title'] = lang('lang_sign_in');
            $this->load->view("login", $data);
        }
    }

    function delete() {
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('confPassword', 'confirmation', 'required|matches[password]');

        if ($this->form_validation->run()) {
            $email = $this->input->post('email');
            $pass = $this->input->post('password');

            if ($this->Crud_model->soft_delete($email, $pass)) {
                $this->session->set_flashdata('account-delete-success', 'deleted succefflly');
                redirect(base_url() . 'login');
            } else {
                $this->session->set_flashdata('wrong-account-details', 'wrong email or password');
                redirect(base_url() . 'profile');
            }
        } else
            redirect(base_url() . 'profile');
    }

}
