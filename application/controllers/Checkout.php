<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Checkout extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init_front();
        $this->load->model('Shop_model');
    }

    public function index() {
        $data['title'] = lang('checkout');
        $data['items'] = get_cart();
        $data['locations'] = $this->Global_model->get_data_not_deleted('locations');
        $data['settings'] = settings('all', 3);
        $data['active'] = 'checkout';
        $this->load->view("front/planet/pages/checkout", $data);
    }

    function availability() {
        $booking_items = $this->Shop_model->get_cart_data();
        $unavailable_products = array();
        $i = 0;
        $products_name = '';
        if (!empty($booking_items)) {
            foreach ($booking_items as $item) {
                $available = checkAvailability(null, $item->product_id, $item->quantity, $item->b_from, $item->b_to);
                if (!$available['flag']) {
                    $unavailable_products[$i] = $item->product_id;
                    $i++;
                }
            }
            if (count($unavailable_products) > 0) {
                //print_r($unavailable_products);
                $data['title'] = lang("shop_cart");
                $data['items'] = get_cart(cid());
                $products_data = $this->Shop_model->get_prodcts($unavailable_products);
                $products_name .= $available['msg'] . ', ';
                $products_name .= $products_data[0]->n_en;
                for ($n = 1; $n < count($products_data); $n++) {
                    $products_name .= ', ' . $products_data[$n]->n_en;
                }
                $this->session->set_flashdata("error", $products_name . lang("unavailable") . lang("please_remove") . lang("it_first"));
                $this->load->view("front/pages/shop_cart", $data);
            }
        }
    }

    function checkBeforePayment() {
        $booking_items = $this->Shop_model->get_cart_data();
        $unavailable_products = array();
        $i = 0;
        $products_name = '';
        if (!empty($booking_items)) {
            foreach ($booking_items as $item) {
                $available = checkAvailability(null, $item->product_id, $item->quantity, $item->b_from, $item->b_to);
                if (!$available['flag']) {
                    $unavailable_products[$i] = $item->product_id;
                    $i++;
                }
            }
            if (count($unavailable_products) > 0) {
                //print_r($unavailable_products);
                $data['title'] = lang("shop_cart");
                $data['items'] = get_cart();
                $products_data = $this->Shop_model->get_prodcts($unavailable_products);
                $products_name .= $products_data[0]->n_en;
                for ($n = 1; $n < count($products_data); $n++) {
                    $products_name .= ', ' . $products_data[$n]->n_en;
                }
                $this->session->set_flashdata("error", $products_name . lang("unavailable") . lang("please_remove") . lang("it_first"));
                $this->load->view("front/pages/shop_cart", $data);
            } else
                redirect(base_url('Checkout'));
        } else
            redirect(base_url('Checkout'));
    }

    function check_all() {
        $booking_items = $this->Shop_model->get_cart_data();
        $unavailable_products = array();
        $i = 0;
        $products_name = '';
        $flag = true;
        if (!empty($booking_items)) {
            foreach ($booking_items as $item) {
                $available = checkAvailability(null, $item->product_id, $item->quantity, $item->b_from, $item->b_to);
                if (!$available['flag']) {
                    $unavailable_products[$i] = $item->product_id;
                    $i++;
                }
            }
            if (count($unavailable_products) > 0) {
                $data['title'] = lang("shop_cart");
                $data['items'] = get_cart();
                $products_data = $this->Shop_model->get_prodcts($unavailable_products);
                $products_name .= $products_data[0]->n_en;
                for ($n = 1; $n < count($products_data); $n++) {
                    $products_name .= ', ' . $products_data[$n]->n_en;
                }
                $flag = false;
            }
        }
        if ($flag == false) {
            $this->session->set_flashdata("error", $products_name . lang("unavailable") . lang("please_remove") . lang("it_first"));
            $this->load->view("front/pages/shop_cart", $data);
        } else {
            $branch = $this->input->post('location_id');
            $settings = settings('all', 3);
            $book_id = 0;
            $total = 0;
            $f_b_from = 0;
            $l_b_to = 0;
            $i = 1;
            $status = 0;
            $voucher_id = 0;
            $payment_type = 0;
            $coupons_msg = '';
            $credit = false;
            $client_data = $this->Shop_model->get_record_not_deleted('clients', cid(), 'id');
            if (!empty($settings) && ($settings->booking_cash_limit == 1) && ($settings->booking_credit_limit == 0)) {//if cash only
                $is_exceeded = $this->Shop_model->is_cash_exceeded($client_data->cash_limit);
                if ($is_exceeded) {
                    $status = 0;
                } else {
                    $status = 2;
                }
            } else if (!empty($settings) && ($settings->booking_cash_limit == 0) && ($settings->booking_credit_limit == 1)) {//if credit only
                $is_exceeded = $this->Shop_model->is_credit_exceeded($client_data->credit_limit);
                if ($is_exceeded) {
                    $status = 0;
                } else {
                    $status = 2;
                    $credit = true;
                }
            } else if (!empty($settings) && ($settings->booking_cash_limit == 1) && ($settings->booking_credit_limit == 1)) {//if credit and cash
                $payment_type = $this->input->post('payment_method');
                $credit = false;
                if ($payment_type == 1) {
                    $is_exceeded = $this->Shop_model->is_cash_exceeded($client_data->cash_limit);
                } else if ($payment_type == 2) {
                    $credit = true;
                    $is_exceeded = $this->Shop_model->is_credit_exceeded($client_data->credit_limit);
                }
                if ($is_exceeded) {
                    $status = 0;
                } else {
                    $status = 2;
                }
            } else {
                $status = 2;
            }
            $new_booking = [
                'client_id' => cid(),
                'cash_limit' => $client_data->cash_limit,
                'credit_limit' => $client_data->credit_limit,
                'status' => $status,
                'branch' => $branch,
            ];
            $book_id = $this->Global_model->global_insert('booking', $new_booking);
            $cart_data = $this->Shop_model->get_cart_data();
            $cart_root = $this->Global_model->get_data_by_id('cart', $cart_data[0]->parent);
            $voucher_id = $cart_root->voucher_id;
            // add products to booking_items
            foreach ($cart_data as $one) {
                if ($i == 1) {
                    $f_b_from = $one->b_from;
                    $l_b_to = $one->b_to;
                }
                $data = [
                    'book_id' => $book_id,
                    'product_id' => $one->product_id,
                    'b_from' => $one->b_from,
                    'b_to' => $one->b_to,
                    'rent_price' => $one->rent_price,
                    'quantity' => $one->quantity,
                    'rent_term' => $one->rent_term
                ];
                $this->Global_model->global_insert('booking_items', $data);
                $data_pre = [
                    'book_id' => $book_id,
                    'product_id' => $one->product_id,
                ];
                $this->Global_model->global_insert('booking_items_preparation', $data_pre);
                $total += $one->total_cost;
                if ($one->b_from < $f_b_from)
                    $f_b_from = $one->b_from;
                if ($one->b_to > $l_b_to)
                    $l_b_to = $one->b_to;
                $i++;
            }
            $this->Global_model->global_update('booking', $book_id, ['voucher_id' => $voucher_id, 'total' => $total, 'b_from' => $f_b_from, 'b_to' => $l_b_to]);
            $book_data = $this->Global_model->get_data_not_deleted('booking', $book_id, 'id');
            if ($credit == false && $status == 2)
                $this->Shop_model->update_transaction($book_data[0]->total, $book_data[0]->transaction_id, $book_id);
            else if ($credit == true && $status == 2)
                $this->Shop_model->update_transaction($book_data[0]->total, $book_data[0]->transaction_id, $book_id, true);

            if ($voucher_id != 0) {//check for discount
                $voucher_data = $this->Global_model->get_data_by_id('vouchers', $voucher_id);
                $validate = $this->Shop_model->update_voucher_validation($voucher_data->voucher_number);
                if ($validate['flag']) {
                    if ($validate['type'] == 2) {//percentage
                        $sub_total = $total;
                        $amount = ($sub_total * $validate['amount']) / 100;
                        $sub_total -= $amount;
                        $this->Shop_model->update_booking($validate['id'], $sub_total, $amount, $book_id);
                    } else if ($validate['type'] == 1) {//amount
                        $sub_total = $total;
                        $sub_total -= $validate['amount'];
                        $this->Shop_model->update_booking($validate['id'], $sub_total, $validate['amount'], $book_id);
                    }
                    $coupons_msg = lang("coupon_applied");
                } else if (!$validate['flag']) {
                    $coupons_msg = $validate['msg'];
                }
            }

            delete_cart(null, -1);
            if ($status == 0) {
                if ($payment_type == 1)//cash exceeded
                    $this->session->set_flashdata("error", lang("your_total") . lang("exceeded_the") . lang("cash_limit") . lang("please_wait_approval") . $coupons_msg);
                else if ($payment_type == 2)//credit exceeded
                    $this->session->set_flashdata("error", lang("your_total") . lang("exceeded_the") . lang("credit_limit") . lang("please_wait_approval") . $coupons_msg);
            } else if ($status == 2)// auto approved
                $this->session->set_flashdata("success", lang("your_order_succedd") . $coupons_msg);

            redirect(base_url('Checkout'));
        }
    }

    function coupon_validation() {
        $this->form_validation->set_rules("coupon_code", lang("coupon_number"), "trim|required");
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata("error", lang("you_must_enter") .' '. lang("coupon_number"));
            redirect(base_url('Checkout'));
        } else {
            $voucher_number = $this->input->post('coupon_code');
            $validate = $this->Shop_model->coupoun_vaidation($voucher_number);
            if ($validate['flag']) {
                $this->Shop_model->update_cart($validate['id']);
                $this->session->set_flashdata("success", $validate['msg']);
                redirect(base_url('Checkout'));
            } else if (!$validate['flag']) {
                $this->session->set_flashdata("error", $validate['msg']);
                redirect(base_url('Checkout'));
            }
            //to be cont after voucher
            //redirect(base_url('Checkout'));
        }
        redirect(base_url('Checkout'));
    }

    function remove_voucher($voucher_id) {
        if (!empty($voucher_id)) {
            $voucher_number = $this->input->post('coupon_code');
            $validate = $this->Shop_model->coupoun_vaidation($voucher_number);
            if ($validate['flag']) {
                $cart_data = $this->Global_model->get_data_by_id('cart', $voucher_id);
                if (!empty($cart_data)) {
                    $this->Shop_model->update_cart_voucher($voucher_id);
                    $this->session->set_flashdata("success", lang('voucher_removed'));
                    redirect(base_url('Checkout'));
                } else {
                    $this->session->set_flashdata("error", lang('voucher_not_found'));
                    redirect(base_url('Checkout'));
                }
            } else if (!$validate['flag']) {
                $this->session->set_flashdata("error", $validate['msg']);
                redirect(base_url('Checkout'));
            }
        } else {
            $this->session->set_flashdata("error", lang('voucher_not_found'));
            redirect(base_url('Checkout'));
        }
    }

}
